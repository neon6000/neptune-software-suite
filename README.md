# Neptune Software Suite #

This software is a Work In Progress and intended for Research only and not General Use.
Use or reference at your own risk.

The Neptune Software Suite is a set of software components, documentation,
and designs for the Neptune Project. The Neptune Project is an ongoing research project.
The goal is a self hosting and independent operating system.

## Neptune Build Environment (NBE) ##

The NBE is the tool chain and associated files needed to build the source tree. The toolchain
can either be built using another C-compatible tool set (such as MSVC) or grab the latest copy
of the executables from somewhere. Set up $(NBE_ROOT) and run nbuild.

### Debugging ###

NBOOT and NEXEC attempt to connect to NDBG if it is detected at the start. If detected, they will trigger
a breakpoint allowing you to issue commands through NDBG. This also occurs if a bugcheck is detected.

## Neptune boot loader ##

When targetting EFI, you can just use NBOOT.EFI. When targetting BIOS firmware, prepend NSTART
to NBOOT.EXE to produce NBOOT, then install BOOTSECT to the disk device. In both cases, add NBOOT.LST
to the same directory. Please reference NBOOT/NBOOT.C for a sample NBOOT.LST.

