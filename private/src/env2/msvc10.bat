::
::	msvc10.bat
::		Sets up Neptune Build Environment variables in MSVC command shell
::
::	Copyright (c) BrokenThorn Entertainment Co
::

@title Neptune Build Environment
@if defined NEPTUNE_ENV goto :startEnvironment
@set DBGMSG=1
@set NBE_ENVDIR=%~dp0
@set _NROOT=%~dp0..\os
@set NBE_BINDIR=%~dp0..\bin
@set NBE_HOST=NBE_HOST_WINDOWS
@set SDK=%~dp0..\..\..\public\sdk
@PATH=%PATH%;"%~dp0"
:startEnvironment:
prompt nbe^>
@call %comspec% /k "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" x86

