::
::	msvc8.bat
::		Sets up Neptune Build Environment variables in MSVC command shell
::
::	Copyright (c) BrokenThorn Entertainment Co
::

@title Neptune Build Environment
@if defined NEPTUNE_ENV goto :startEnvironment
@set NBE_BUILD_PATH=C:\Documents and Settings\Michael\Desktop\PROJECTS\Neptune\src\env2
@set NBE_ENVDIR=..\env2
@set NBE_SRCDIR=..\os
@set NBE_BINDIR=..\bin
@set NBE_HOST=NBE_HOST_WINDOWS
:startEnvironment:
@call %comspec% /k "C:\Program Files\Microsoft Visual Studio 8\VC\vcvarsall.bat" x86
