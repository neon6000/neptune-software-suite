
# NBE Build path
NBE_BUILD_PATH=~/Desktop/b/Neptune/c/env
export NBE_BUILD_PATH;

# Other paths. These must be relative
NBE_ENVDIR=../env
NBE_OBJDIR=../obj
NBE_SRCDIR=../src
NBE_BINDIR=../bin

# Set host operating system
NBE_HOST=NBE_HOST_LINUX

export NBE_ENVDIR
export NBE_OBJDIR
export NBE_SRCDIR
export NBE_BINDIR
export NBE_HOST

# Run nbuild. It will find path using NEPTUNE_BUILD_PATH, set and export the real NEPTUNE_CURRENT_BUILD.
# nbuild

bash

