
#=================================================================
#                   * * * USER DEFINED * * *
#
#	Projects that use the NBE must define the following
#	constants in the respective make files. It is recommended
#	to define the constants using "var_namr = value" rather
#	then "var_name := value" for compatibility with NMAKE.
#
#    * * * All Neptune Project Software must follow this. * * *
#
#=================================================================
# PROJECT_LANG
#	PROJECT_LANG_ENGLISH
#	PROJECT_LANG_SPANISH
#	PROJECT_LANG_JAPANESE
#	PROJECT_LANG_KOREAN
#-----------------------------------------------------------------
# PROJECT_SRC
#	List of source file paths.
# Example :
#	PROJECT_SRC = $(NBE_SRCDIR)/$(PROJECT_DIR)/file_name.c
#-----------------------------------------------------------------
# PROJECT_NAME
#	Name of project. This will be used for the output image name.
#	By default, this is the same name as the parent directory.
#-----------------------------------------------------------------
# PROJECT_SCRIPT
#	Name of LD script file in curent directory.
#-----------------------------------------------------------------
# PROJECT_DEF
#	Path to project specific DEF file that defines custom
#	build settings specific to the project.
#-----------------------------------------------------------------
# PROJECT_TARGET
#	PROJECT_TARGET_SHARED
#	PROJECT_TARGET_STATIC
#-----------------------------------------------------------------
# PROJECT_SYSTEM
#	PROGRAM_SYSTEM_GUI
#	PROGRAM_SYSTEM_DRIVER
#	PROGRAM_SYSTEM_NATIVE
#	PROGRAM_SYSTEM_EFI
#-----------------------------------------------------------------
# PROJECT_DEBUG
#	If defined, enables DEBUG build.
#=================================================================
# NBE Defined. Do not modify.
#=================================================================
# NBE_HOST
#	NBE_HOST_WINDOWS
#	NBE_HOST_LINUX
#	NBE_HOST_NEPTUNE
#-----------------------------------------------------------------
# PROJECT_DIR
#	Directory path to current project.
#-----------------------------------------------------------------
# NBE_ARCH
#	NBE_ARCH_IA32
#	NBE_ARCH_IA64
#==================================================================

# Example of use :
#  make -f gnu-linux-x64.mak c/b
# builds /src/c/b project.

# OS utilities.

include $(NBE_BUILD_PATH)/utils-linux-x64.def

#
# NBE Script defined constants :
#
# NBE_HOST
# NBE_BUILD_PATH
# NBE_ENVDIR
# NBE_OBJDIR
# NBE_SRCDIR
# NBE_BINDIR
#

# The following must be set by NBUILD

PROJECT_DIR = c/b

# Project defined makefile.

include $(NBE_SRCDIR)/$(PROJECT_DIR)/makefile

# Include default settings.

include $(NBE_BUILD_PATH)/as-linux-x64.def
include $(NBE_BUILD_PATH)/gcc-linux-x64.def
include $(NBE_BUILD_PATH)/ld-linux-x64.def

# Include project defined build settings.

ifdef PROJECT_DEF
include $(PROJECT_DEF)
endif

# Project object files

PROJECT_OBJ := $(PROJECT_SRC:.c=.o)

# Configure settings to prepare for builds

ifdef PROJECT_SCRIPT
	LDFLAGS :=  $(NBE_SRCDIR)/$(PROJECT_DIR)/$(PROJECT_SCRIPT) $(LDFLAGS)
endif

# Build targets.

# .c -> .o
$(NBE_OBJDIR)/$(PROJECT_DIR)/%.o : $(NBE_SRCDIR)/$(PROJECT_DIR)/%.c
	-$(MKDIR) $(NBE_OBJDIR)/$(PROJECT_DIR)
	$(CC) $(CFLAGS) -o $@ $<

# .o -> .a
$(PROJECT_DIR) : $(PROJECT_OBJ)
	$(LD) $(LDFLAGS) -o $(NBE_BINDIR)/$(PROJECT_NAME).out $(LDLIBS) $(PROJECT_OBJ)

# build all targets
all : $(NBE_BINDIR)/$(PROJECT_DIR)

clean :
	$(ECHO) clean command issued.
