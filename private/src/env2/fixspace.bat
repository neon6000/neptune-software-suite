@echo off
rem
rem fixspace.bat - Removes redundant spaces in string list.
rem
rem Example of use:
rem    	for /f "tokens=*" %a in ('fixspace $(INCLUDES)') do set INCPATH=%a
rem
SET x=%*
FOR /F "tokens=*" %%i IN ('ECHO %x%') DO SET y=%%i
ECHO %y%
