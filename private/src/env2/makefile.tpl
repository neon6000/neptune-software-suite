!if 0

	makefile

		Template file for a project makefile.

	Copyright BrokenThorn Entertainment, Co.

!endif

#
# This is where the output program file will be built. Paths
# can be relative or absolute. The program will be built
# in $(TARGETPATH)/$(TARGETCPU)
#
TARGETPATH = ..\..\..\..\public\sdk\lib

#
# This is the name and program type. The environment will
# output the file as $(TARGETNAME).$(TARGETEXT) depending
# on TARGETTYPE.
#
TARGETNAME = hello
TARGETTYPE = DYNLINK

#
# This is a list of libraries to import.
#
TARGETLIBS =

#
# Semicolon (;) separate list of include paths.
#
INCLUDES = ../nexec

#
# List of common source files.
#
SOURCES = main.c

#
# Architecture specific source files. Set $(TARGETCPU)
# to control the architecture type.
#
X86_SOURCES = x86/test.c x86/foo.asm

#
# List of object files. $(O) is the object file
# directory. This is always current directory/$(TARGETCPU).
#
OBJECTS = $(O)\foo.obj $(O)\main.obj

#
# Include the master makefile.
#
!INCLUDE $(NBE_ENVDIR)\makefile.def
