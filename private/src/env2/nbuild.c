/**
 * Neptune Build Utility.
 * Copyright (c) BrokenThorn Entertainment Co.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <malloc.h>
#include <time.h>
#include <sys/stat.h>
#ifdef __GNUC__
#include <unistd.h>
#include <dirent.h>
#endif
#ifdef _MSC_VER
#include <io.h>
#endif

/* This component implements the NBUILD utility. */

/* Standard definitios. */
#define IN
#define OUT
#define TRUE 1
#define FALSE 0
#define INLINE inline

/*
===========================================

	* * * PRIVATE DECLARATIONS * * *

===========================================
*/

/* Standard types. */
typedef unsigned int BOOL;
typedef unsigned int status_t;
typedef unsigned int index_t;
#ifdef _MSC_VER
typedef status_t size_t;
#endif

/**
* NBE support host types.
*/
typedef enum _nbeHostType {
  NBE_HOST_LINUX = 0,
  NBE_HOST_WINDOWS,
  NBE_HOST_NEPTUNE,
  NBE_HOST_UNKNOWN
}nbeHostType;

/**
* NBE supported hosts.
*/
struct _tagNbeHosts {
  nbeHostType t;
  char* s;
}_nbeHosts[] = {
   {NBE_HOST_LINUX, "NBE_HOST_LINUX"},
   {NBE_HOST_WINDOWS, "NBE_HOST_WINDOWS" },
   {NBE_HOST_NEPTUNE, "NBE_HOST_NEPTUNE" },
   {NBE_HOST_UNKNOWN, "NBE_HOST_UNKNOWN" }
};

/**
* Project Environment Block
*/
typedef struct _nbePEB {
   nbeHostType host;
   char*       env;
   char*       obj;
   char*       src;
   char*       bin;
}nbePEB;

typedef enum _nbeArchType {
  NBE_ARCH_I386,
  NBE_ARCH_X64
}nbeArchType;

typedef enum _nbeFwType {
  NBE_FW_BIOS,
  NBE_FW_UEFI,
  NBE_FW_OFW
}nbeFwType;

/**
* NBE error codes.
*/
typedef enum nbeErrorCode {
   NBE_STAT_SUCCESS         = 0,
   NBE_STAT_ERROR_ENV,
   NBE_STAT_ERROR_EXEC,
   NBE_STAT_ERROR_MAKEFILE,
   NBE_STAT_ERROR_DEBUG,
   NBE_STAT_ERROR_PARAM,
   NBE_STAT_ERROR_MAX
}nbeErrorCode;

/**
* NBE error descriptor.
*/
typedef struct _nbeError {
   nbeErrorCode code;
   char* s;
}nbeError;

/**
* NBE Build Target.
*/
typedef struct _nbeTarget {
   char* name;
   struct _nbeTarget* next;
}nbeTarget;

/**
* NBE Build Target List.
*/
typedef struct _nbeTargetList {
   nbeTarget* first;
   nbeTarget* last;
}nbeTargetList;

/**
* Program Paramater Block
*/
typedef struct _nbePPB {
   size_t         cpuCores;
   BOOL           forceRebuild;
   BOOL           buildDependents;
   BOOL           buildAll;
   BOOL           buildDebug;
   BOOL           install;
   nbeArchType    target;
   nbeFwType      fw;
   nbeTargetList  targetListHead;
}nbePPB;

/* max number of directory enteries in DIRS file. */
#define NBE_DIRS_MAX_SIZE 32

/**
* NBE DIRS File Descriptor.
*/
typedef struct _nbeDIRS {
   int   count;
   char* name[NBE_DIRS_MAX_SIZE];
}nbeDIRS;

/* Error list. */

nbeError _errors[] = {
 {NBE_STAT_SUCCESS,        "Success"},
 {NBE_STAT_ERROR_ENV,      "Environment variable '%s' missing or malformed"},
 {NBE_STAT_ERROR_EXEC,     "The system failed to find or execute make"},
 {NBE_STAT_ERROR_MAKEFILE, "Project makefile missing or invalid"},
 {NBE_STAT_ERROR_DEBUG,    "Debug error detected"},
 {NBE_STAT_ERROR_PARAM,    "The paramater '%s' is malformed"}
};

typedef enum _nbeParameterFlag {
  NBE_PARAM_FW = 0,
  NBE_PARAM_H,
  NBE_PARAM_F,
  NBE_PARAM_A,
  NBE_PARAM_I,
  NBE_PARAM_C,
  NBE_PARAM_D,
  NBE_PARAM_T,
  NBE_PARAM_MAX
}nbeParameterFlag;

static struct _nbeParameter {
   nbeParameterFlag flag;
   char*            name;
   char*            desc;
}_nbeParameterList [] = {
   {NBE_PARAM_FW,"-fw", "Set target firmware (For Firmware dependent software)" },
   {NBE_PARAM_H,"-h", "Display help information" },
   {NBE_PARAM_F,"-f", "Force rebuild of all dependents" },
   {NBE_PARAM_A,"-a", "Force rebuild of all projects" },
   {NBE_PARAM_I,"-i", "Runs install command on all projects" },
   {NBE_PARAM_C,"-c", "Enables multiple cores (if environment supports)" },
   {NBE_PARAM_D,"-d", "Enables debug build" },
   {NBE_PARAM_T,"-t", "Set target architecture" }
};

/*
===========================================

	* * * PRIVATE METHDOS * * *

===========================================
*/

/**
* Get host type from host name.
* \param s Host name
* \ret Host type or NBE_HOST_UNKNOWN if not supported
*/
nbeHostType NbeGetHost (IN char* s) {

   index_t c;

   /* get host type. */
   for (c=0; c<NBE_HOST_UNKNOWN; c++) {
      if (strcmp (s, _nbeHosts [c].s) == 0) {

         /* return host type. */
         return _nbeHosts [c].t;
      }
   }

   /* host name not supported. */
   return NBE_HOST_UNKNOWN;
}

/**
* Raise NBE specific error.
* \param code Error code
* \param ... Error specific additional information
*/
void NbeError (IN unsigned int code, IN ...) {

   va_list args;
   char    buf[512];

   /* sanity check. */
   if (code > NBE_STAT_ERROR_MAX)
      code = NBE_STAT_ERROR_DEBUG;

   /* parse argument list. */
   va_start (args, code);
   vsnprintf(buf, 512, _errors[code].s, args);
   va_end (args);

   /* write error. */
   fprintf(stderr, "*** NBUILD Error [0x%x] %s\n\r", code<<7, buf);
}

/**
*  Read NBE environment parameter block.
*  \param Program enviromnent block
*  \ret TRUE on success, FALSE otherwise/home/mwt5175/Desktop/b/Neptune/c/env/nbuild.out
*/
BOOL NbeGetPEB (OUT nbePEB* out) {

   char* host;

   /* initialize environment block. */
   host = getenv("NBE_HOST");
   out->env  = getenv("NBE_ENVDIR");
   out->obj  = getenv("NBE_OBJDIR");
   out->src  = getenv("NBE_SRCDIR");
   out->bin  = getenv("NBE_BINDIR");

   if (host == NULL) {
      return FALSE;
   }
   out->host  = NbeGetHost (host);
   return TRUE;
}

#ifdef __GNUC__

/**
* Executes make on a project to build.
* \param projectName Pointer to project name
* \ret TRUE if success, FALSE on errr
*/
BOOL NbeExecMake (IN char* projectName) {

	BOOL     result;
	pid_t    pid;
	status_t status;
	char*    argv[4];

	/* assume failure. */
	result = FALSE;

	/* prepare to call make. */
	argv[0] = "-f";
	argv[1] = "gnu-makefile";
	argv[2] = projectName;
	argv[3] = NULL;

	/* create new process. */
	pid = fork();
	if (pid == 0) {

		/* execute make. */
		status = execvp("make", argv);
		perror("*** Failed to execute make ");
		result = FALSE;
	}
	else if (pid < 0) {

		/* failed to create process. */
		perror("*** Failed to create process ");
		result = FALSE;
	}else{

		/* make executed. */
		if (status != -1)
			result = TRUE;

		/* wait for make. */
		wait(pid);
	}

	/* return result. */
	return result;
}

#endif

/**
* Initialize build target list.
* \param in Target list to initialize
* \ret TRUE if success, FALSE if error
*/
INLINE BOOL NbeInitializeTargetList (IN nbeTargetList* in) {
   if (!in)
      return FALSE;
   in->first = NULL;
   in->last = NULL;
   return TRUE;
}

/**
* Free target list resources.
* \param in Target list to free.
*/
void NbeFreeTargetList (IN nbeTargetList* in) {

   /* Just loop through targets and free memory. */
   nbeTarget* target = in->first;
   while (target) {
      if (target->name)
         free (target->name);
      target=target->next;
   }
}

/**
* Adds new target to target list.
* \param in Input target list
* \param in Target name to add
* \ret TRUE if success, FALSE if not
*/
BOOL NbeAddTarget (IN nbeTargetList* in, IN char* target) {

   nbeTarget* p;

   /* Append target to end of list and adjust pointers. */
   if (in->first == NULL) {
      p = (nbeTarget*) malloc (sizeof(nbeTarget));
      if (!p)
         return FALSE;
      p->name = strdup(target);
      p->next = 0;
      in->first = p;
      in->last = in->first;
   }else{
      p = (nbeTarget*) malloc(sizeof(nbeTarget));
      if(!p)
         return FALSE;
      p->name = strdup(target);
      p->next = 0;
      in->last->next = p;
      in->last = p;
   }
   return TRUE;
}

/**
* Get time stamp object.
* \param path Path name of file object
* \ret Time object
*/
INLINE BOOL NbeGetLastModified (IN time_t* out, IN char* path) {

   struct stat s;
   int         result;

#ifdef _MSC_VER
   result = _stat(path,&s);
#else
   result = stat(path,&s);
#endif

   if (result == -1)
      return FALSE;
   *out = s.st_mtime;
   return TRUE;
}

/**
* Check that a file can be opened for reading.
* \param fname Pointer to file name path string
* \ret TRUE if file can be opened, FALSE if not
*/
INLINE BOOL NbeFileCheck (IN char* fname) {
   FILE* file = fopen(fname,"r");
   if(file) {
      fclose(file);
      return TRUE;
   }
   return FALSE;
}

/**
* Parse NBE DIRS file.
* \param fpath Pointer to file path string
* \param out Pointer to NBE DIRS descriptor
* \ret TRUE if success, FALSE if error
*/
BOOL NbeParseFileDIRS (IN char* fpath, OUT nbeDIRS* out) {

   FILE*   file;
   size_t  c;
   size_t  s;
   char    line [512];

  /* open DIRS. */
  file = fopen(fpath, "r");
  if (!file)
     return FALSE;

   /* start counting at 0. */
   c = 0;

   /* begin reading. */
   while (!feof (file)) {

      if ( fgets(line,512,file) ) {

          /* ignore comments. */
          if (line[0] == '#' || line[0]==';')
             continue;

          /* get size. */
          s = strlen (line);

          /* remove trailing newlines. */
          if (line[s-1]=='\n')
             line[s-1]='\0';

          /* ignore empty lines. */
          if (s-1 == 0)
             continue;

          /* read in our string. */
    	  out->name[c++] = strdup(line);
      }
   }
   out->count = c;

   /* return success. */
   fclose(file);
   return TRUE;
}

/**
* Build target list.
* \param path Pointer to initial path string
*/
void NbeBuildTargetList (IN char* path) {

  nbeDIRS  dirs;
  size_t   c;
  char*    s;
  char     current[256];

  /*
  if (NbeGetLastModified($(NBE_SRCDIR)/path) < NbeGetLastModified$(NBE_BINDIR)/path))
    // Nothing to do.
    return;
  */

  /* read DIRS file. */
  sprintf(current, ".%s/dirs", path);
  if ( NbeParseFileDIRS(current, &dirs) ) {

     /* for each directory. */
     for(c = 0; c < dirs.count; c++) {

        /* add targets from the directory. */
        s = (char*) malloc (strlen(path) + strlen(dirs.name[c])+1);
        sprintf(s, "%s/%s",path, dirs.name[c]);
        NbeBuildTargetList(s);
        free(s);
     }
  }

  /* test if makefile exists. */
  sprintf(current, ".%s/makefile", path);
  if (NbeFileCheck(current)) {

      /* add new target. */
      printf("\n\rAdding target : '%s'\n\r", current);
  }
}

/**
* Build source directory.
* \param pbp Pointer to the Program Parameter Block
* \param peb Pointer to the Program Environment Block
* \ret TRUE on success, FALSE otherwise
*/
BOOL NbeBuild (IN nbePPB* ppb, IN nbePEB* peb) {

   /* For every build target in target list. */

      /* setenv (PROJECT_DIR, target_list [c].name ) */

      /* NbeExecMake( target_list [c].name ); */

}

nbeParameterFlag NbeGetParameter (IN char* s) {

  size_t c;

  for (c=0; c<NBE_PARAM_MAX; c++) {
     if (strncmp (s, _nbeParameterList [c].name, strlen(_nbeParameterList [c].name)) == 0)
         return _nbeParameterList [c].flag;
   }
   return NBE_PARAM_MAX;
}

void NbeVersion(void) {
   printf("\n\rNeptune Build Utility (Version 2.0)");
   printf("\n\rCopyright BrokenThorn Entertainment, Co. All Rights Reserved.");
}

void NbeHelp (void) {

   size_t c;

   printf("\n\n\rOptions :\n\r");
   for (c=0; c<NBE_PARAM_MAX; c++)
      printf("\n\r%s\t%s", _nbeParameterList[c].name, _nbeParameterList[c].desc);
   printf("\n\n\rThis component is part of the Neptune Project.");
}

/**
* Parse Program Paramater Block
* \param out PPB descriptor
* \param argc Argument count
* \param argv Arguments
* \ret TRUE if success, FALSE if error
*/
BOOL NbeParsePPB (IN nbePPB* out, IN int argc, IN char** argv) {

  size_t c;
  nbeParameterFlag flag;

  /* initialize. */
  out->cpuCores        = 1;
  out->target          = NBE_ARCH_I386;
  out->forceRebuild    = FALSE;
  out->buildDependents = FALSE;
  out->install         = FALSE;
  out->buildDebug      = FALSE;
  NbeInitializeTargetList (&out->targetListHead);

  /* scan program parameters. */
  NbeVersion ();
  for (c=1; c<argc; c++) {
    flag = NbeGetParameter (argv[c]);
    switch(flag) {

       /* Set cpu cores. Cannot test with MAKE since its not supported. */
       case NBE_PARAM_C:
          break;

       /* set target architecture. */
       case NBE_PARAM_T: {

          char* a;

          strtok (argv[c], "=");
          a = strtok(NULL, "=");

          if (!a) {
             NbeError(NBE_STAT_ERROR_PARAM, "-t");
             break;
          }

          if(strcmp(a, "i386") == 0 )
             out->target = NBE_ARCH_I386;
          else if (strcmp(a, "x64") == 0 )
             out->target = NBE_ARCH_X64;
          else
             NbeError(NBE_STAT_ERROR_PARAM, "-t");
          break;
       }

       /* set firmware. */
       case NBE_PARAM_FW: {

          char* a;

          strtok (argv[c], "=");
          a = strtok(NULL, "=");

          if (!a) {
             NbeError(NBE_STAT_ERROR_PARAM, "-fw");
             break;
          }

          if(strcmp(a, "bios") == 0 )
             out->fw = NBE_FW_BIOS;
          else if (strcmp(a, "uefi") == 0 )
             out->fw = NBE_FW_UEFI;
          else if (strcmp(a, "ofw") == 0 )
             out->fw = NBE_FW_OFW;
          else
             NbeError(NBE_STAT_ERROR_PARAM, "-fw");
          break;
       }

       /* Simple options. */
       case NBE_PARAM_H: NbeHelp();                   break;
       case NBE_PARAM_F: out->forceRebuild    = TRUE; break;
       case NBE_PARAM_A: out->buildDependents = TRUE; break;
       case NBE_PARAM_I: out->install         = TRUE; break;
       case NBE_PARAM_D: out->buildDebug      = TRUE; break;

       /* Add target to target list. */
       default:
         NbeAddTarget (&out->targetListHead, argv[c]);
         printf("\n\rNbeParsePPB -> Adding Target : '%s'", argv[c]);
    }    
  }

   return TRUE;
}

/**
* Entry point.
* \param argc Argument count
* \param argv Argument list
* \ret EXIT_SUCCESS or EXIT_FAILURE status
*/
int main(IN int argc, IN char** argv) {

  status_t   result;
  nbePEB     peb;
  nbePPB     ppb;

  /* parse program paramater block. */
  NbeParsePPB (&ppb, argc, argv);

  /* assume success. */
  result = EXIT_SUCCESS;

  /* get environment block. */
  NbeGetPEB (&peb);

  /* verify enviromnent. */
  if (peb.bin == NULL) {
     NbeError(NBE_STAT_ERROR_ENV, "NBE_BINDIR");
     result = EXIT_FAILURE;
  }
  if (peb.src == NULL) {
     NbeError(NBE_STAT_ERROR_ENV, "NBE_SRCDIR");
     result = EXIT_FAILURE;
  }
  if (peb.env == NULL) {
     NbeError(NBE_STAT_ERROR_ENV, "NBE_ENVDIR");
     result = EXIT_FAILURE;
  }

  /* if no object directory specified, set it to src. */
  if (peb.obj == NULL)
      peb.obj = peb.src;

  /* if no host specified, assume Neptune. */
  if (peb.host == NBE_HOST_UNKNOWN)
      peb.host = NBE_HOST_NEPTUNE;

  /* bail out if error condition is set. */
  if (result == EXIT_FAILURE)
     return result;

  if ( NbeBuild  (&ppb, &peb))
     return EXIT_SUCCESS;

  return EXIT_FAILURE;
}

