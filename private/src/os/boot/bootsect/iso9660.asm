;*********************************************
;	stage1.asm
;		- iso9660 VBR
;
;   Copyright BrokenThorn Entertainment Co
;*********************************************

bits 16
org 0

jmp 0x7c0:main

;
;	biggest file tested with: 316416 bytes
;

;
;	location for readSectors
;
%define LOAD_SEG    0x800
%define LOAD_OFFSET 0

;=================================================;
;                                                 ;
; IBM/MS INT 13 Extensions check                  ;
; Return:                                         ;
;	AX = 0 [false] or 1 [true]                ;
;                                                 ;
;=================================================;

int13Check:
	mov	ah, 0x41			; query for extension support
	mov	bx, 0x55aa
	mov	dl, [bootDrive]
	int	0x13
	jc	.noExtension			; if CF set, interrupt not avilable
	cmp	bx, 0xaa55
	jne	.noExtension			; if bx is not 0xaa55, its not installed
	mov	ax, 1
	ret
.noExtension:
	mov	ax, 0
	ret

;=================================================;
;                                                 ;
; IBM/MS INT 13 Extensions Disk Address Packet    ;
;                                                 ;
;=================================================;

struc diskAddressPacket
	.size		resb 1
	.reserved	resb 1
	.transferCount	resw 1
	.bufferOff      resw 1
	.bufferSeg      resw 1
	.lbaLo		resd 1
	.lbaHi		resd 1
endstruc

addressPacket:
istruc diskAddressPacket
	at diskAddressPacket.size,          db 16
	at diskAddressPacket.reserved,      db 0
	at diskAddressPacket.transferCount, dw 0
	at diskAddressPacket.bufferOff,     dw 0
	at diskAddressPacket.bufferSeg,     dw 0
	at diskAddressPacket.lbaLo,         dd 0
	at diskAddressPacket.lbaHi,         dd 0
iend

;=================================================;
;                                                 ;
; Load sectors                                    ;
; Input:                                          ;
;	EBX = sector LBA                          ;
;	DI = number of sectors                    ;
; Output:                                         ;
;       Sectors loaded to LOAD_SEG:LOAD_OFFSET    ;
;                                                 ;
;=================================================;

readSectors:
	xor		dx, dx
	mov		cx, LOAD_OFFSET
	mov		si, LOAD_SEG
.loop:
	mov             word [addressPacket + diskAddressPacket.transferCount], 1
	mov		word [addressPacket + diskAddressPacket.bufferSeg], si
	;
	;	load sector
	;
.loadSector:
	push		si
	push		dx
	mov		word [addressPacket + diskAddressPacket.bufferOff], cx
	mov		dword [addressPacket + diskAddressPacket.lbaLo], ebx
	mov		dword [addressPacket + diskAddressPacket.lbaHi], 0
	mov		ah, 0x42
	mov		dl, [bootDrive]
	mov		si, addressPacket
	int		0x13
	pop		dx
	pop		si
	jc		.error
.next:
	inc		dx			; next sector
	add		cx, word [sectorSize]	; next offset
	inc		ebx			; next LBA
	dec		di			; loop counter
	;
	;	adjust to a new segment if needed
	;
	cmp		cx, 0xf800		; 0xf800 is last 2048 byte offset before rollover to 0x10000
	jne		.ready
	add		si, 0x1000		; go to next segment and let cx overflow to 0
.ready:
	cmp		di, 0
	jnz		.loop			; loop
.done:
	ret
.error:
	call		restart			; bail out

;=================================================;
;                                                 ;
; Display error and restart                       ;
;                                                 ;
;=================================================;

restart:
	mov     si, msgError			; display error
	call    puts
	mov     si, msgReboot			; display reboot message
	call    puts
	xor     ax, ax
	int     0x16				; wait for keypress
	int	0x19				; reboot
	cli
	hlt

;=================================================;
;                                                 ;
; iso9660 structures                              ;
;                                                 ;
;=================================================;

;
;	all directory structores are evenly aligned, with padding in between when needed
;
struc directory
	.size		resb 1
	.attrSize	resb 1
	.extentLoc	resd 2			; LBA location, both-endian format
	.extentSize	resd 2			; size of extent, both-endian format
	.dateTime	resb 7			; date and time
	.flags		resb 1			; file flags
	.unitSize	resb 1			; unit size for files recorded in interleaved mode
	.gapSize	resb 1			; interleave gap size for files recorded in interleaved mode
	.extentVolume	resd 1			; volume sequence number; volume extent is recorded on
	.fileNameSize	resb 1			; length of file name
	; variable size, file name. String is ';'-terminated followed by file ID in ASCII coded decimal ('1')
endstruc

;
;	directory entry flag bits
;
%define FLAGS_FILE_HIDDEN      1 ; bit 0
%define FLAGS_FILE_DIR         2 ; bit 1
%define FLAGS_FILE_ASSOC       3 ; bit 2
%define FLAGS_FILE_EXTATTRREC  4 ; bit 3, extended attribute record contains info
%define FLAGS_FILE_PERMISSIONS 5 ; bit 4
%define FLAGS_FILE_SPAN        8 ; bit 7

;
;	primary volume descriptor
;
struc primVolDescr
	.type                resb 1    ; always 1
	.id                  resb 5    ; always 'CD001'
	.version             resb 1    ; always 1
	.unused              resb 1
	.systemId            resb 32
	.volumeId            resb 32
	.unused2             resb 8
	.volSpaceSize        resb 8
	.unused3             resb 32
	.volumeSetSize	     resb 4
	.volumeSeqNum	     resb 4
	.logicalBlockSize    resb 4
	.pathTableSoze       resb 8
	.locPathTableL       resb 4
	.locOptPathTableL    resb 4
	.locPathTableM       resb 4
	.locOptPathTableM    resb 4
	.rootDirectory       resb 34   ; actual directory structure
	.volumeSetId         resb 128
	.publisherId         resb 128
	.dataPreparerId      resb 128
	.applicationId       resb 128
	.copyrightFileId     resb 38
	.abstractFileId      resb 36
	.bibiolgraphicFileId resb 37
	.volCreateDateTime   resb 17
	.volModDateTime      resb 17
	.volExpDateTime      resb 17   ; expiration date/time
	.volEffDateTime      resb 17   ; effective date/time
	.strucVersion        resb 1
	.unused4             resb 1
endstruc

;=================================================;
;                                                 ;
; Load file from iso9660 filesystem               ;
; Return:                                         ;
;	AX = 0 [false] 1 [true]                   ;
;                                                 ;
;=================================================;

readFile:
	;
	;	read primary volume descriptor
	;
	mov	ebx, 16				; sector 16 is first volume descriptor
	mov	di, 1
	call	readSectors
	mov	ax, LOAD_SEG			; switch to load segment
	mov	ds, ax
	;
	;	check for invalid structure
	;
	cmp	byte [LOAD_OFFSET + primVolDescr.type], 1
	jne	.error
	cmp	dword [LOAD_OFFSET + primVolDescr.id], 'CD00'
	jne	.error
	;
	;	get starting directory
	;
	mov	bx, LOAD_OFFSET + primVolDescr.rootDirectory
	mov	ebx, [bx + directory.extentLoc]

	;
	;	fixme: reads 1 sector of root directory; should calculate number of sectors
	;
.readDirectory:
	mov	di, 1						; read 1 sector for now
	mov	ax, 0x7c0					; go back to our program segment
	mov	ds, ax
	call	readSectors					; read directory
	;
	;	search directory
	;
	mov	ax, LOAD_SEG					; switch to load segment for reading
	mov	ds, ax
	mov	bx, LOAD_OFFSET					; bx = current directory entry
.search:
	;
	;	watch for last entry
	;
	mov	ax, [bx + directory.size]
	cmp	ax, 0
	je	.error						; file not found
	;
	;	compare entry name
	;
	movzx	cx, byte [bx + directory.fileNameSize]
	mov	di, imageName
	lea	si, [bx + directory.fileNameSize + 1]
	repe	cmpsb
	jz	.found
	;
	;	go to next entry
	;
	add	bx, [bx + directory.size]			; go to next entry
	jmp	.search

.found:
	;
	;	sector count = (extentSize / sectorSize) + 1
	;
	xor	edx, edx
	mov	eax, [bx + directory.extentSize]		; size of file in bytes
	movzx	ecx, word [es:sectorSize]
	div	ecx
	inc	eax
	;
	;	load file or directory. directory entry is pointed by bx
	;
.loadFile:
	mov	ebx, [bx + directory.extentLoc]			; sector of file
	mov	di, ax						; number of sectors
	mov	ax, 0x7c0					; go back to our program segment
	mov	ds, ax
	call	readSectors

.success:
	mov	ax, 1
	ret

.error:
	mov	ax, 0x7c0
	mov	ds, ax
	mov     ax, 0
	ret

;=================================================;
;                                                 ;
; Display string                                  ;
;                                                 ;
;=================================================;

puts:
	mov     ax, 0x0e0d
	mov     bx, 0x07
.next:       
	int     0x10
	lodsb
	test	al,al
	jnz     .next
	ret

;=================================================;
;                                                 ;
; Entry point                                     ;
;                                                 ;
;=================================================;

main:
	cli
	mov	ax, 0x7c0			;we are at 0x7c0:0
	mov	ss, ax
	mov	ds, ax
	mov	es, ax
	mov	fs, ax
	mov	gs, ax
	mov	sp, 0
	sti
	mov	[bootDrive], dl			;save boot drive
	call	int13Check			;perform int 13h install check
	cmp	ax, 1
	je	.readLoader
	;
	;	fixme: should fall back to another disk reading method
	;
	mov	si, msgNoExtension		;extensions not available, cant continue
	call	puts
	xor     ax, ax
	int     0x16				;wait for keypress
	int	0x19				;reboot
	cli
	hlt

.readLoader:
	call	readFile			;load boot loader
	cmp     ax, 0
	je      .error

.execute:
	mov	dl, [bootDrive]			;execute boot loader
	push	LOAD_SEG
	push	LOAD_OFFSET
	retf

.error:
	call restart				;restart system

;=================================================;
;                                                 ;
; Variables                                       ;
;                                                 ;
;=================================================;

msgReboot	db 10, "Press any key to reboot", 13, 10, 0
msgError	db "nboot is missing or corrupt", 13, 0
msgNoExtension	db "Required BIOS services not available", 13, 0

bootDrive	db 0
sectorSize	dw 2048
imageName	db "NBOOT;1"

times 2048 - ($-$$) db 0			;must be 2048 bytes
