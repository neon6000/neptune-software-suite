
;*********************************************
;	Boot1.asm
;		- Boot strap
;
;   This file is part of the MOS project
;   Copyright 2008 BrokenThorn Entertainment Co
;*********************************************

; fat16 stage 1 version info
%define VERSION_MINOR 1
%define VERSION_MAJOR 0

; stage 1 signiture
%define FAT12_STAGE1_SIGNITURE 0xaa55

; offset of the end of the bios paramater block
%define FAT12_STAGE1_BPBEND		0x3e

; bios hdd flag
%define FAT12_STAGE1_BIOS_HD_FLAG	0x80

; windows nt magic number offset
%define FAT12_STAGE1_WINDOWS_NT_MAGIC	0x1b8

; offset of the start of the parition table
%define FAT12_STAGE1_PARTSTART 0x1be

; offset of the end of the parition table
%define FAT12_STAGE1_PARTEND 0x1fe

; offset of stack
%define FAT12_STAGE1_STACKSEG		0x2000

; filesystem
%define BOOT_FSYS 0

; 16 bit real mode code
bits	16

; we set segments later
org		0

; jump over bios paramater block
jmp	main

bpbOEM				DB "MOS     "
bpbBytesPerSector:  	DW 512
bpbSectorsPerCluster: 	DB 1
bpbReservedSectors: 	DW 1
bpbNumberOfFATs: 	DB 2
bpbRootEntries: 	DW 224
bpbTotalSectors: 	DW 2880
bpbMedia: 			DB 0xf0  ;; 0xF1
bpbSectorsPerFAT: 	DW 9
bpbSectorsPerTrack: 	DW 18
bpbHeadsPerCylinder: 	DW 2
bpbHiddenSectors: 		DD 0
bpbTotalSectorsBig:     DD 0
bsDriveNumber: 	        DB 0
bsUnused: 				DB 0
bsExtBootSignature: 	DB 0x29
bsSerialNumber:	        DD 0xa0a1a2a3
bsVolumeLabel: 	        DB "MOS FLOPPY "
bsFileSystem: 	        DB "FAT16   "

; end of bios paramater block. declare data

absoluteSector db 0x00
absoluteHead   db 0x00
absoluteTrack  db 0x00

;-------------------------------------------
;	Prints a string
;	DS=>SI: 0 terminated string
;-------------------------------------------

Print:
			lodsb
			or	al, al
			jz	.done
			mov	ah, 0eh
			int	10h
			jmp	Print
	.done
			ret

;--------------------------------------------
; Convert CHS to LBA
; LBA = (cluster - 2) * sectors per cluster
;--------------------------------------------

ClusterLBA:
          sub     ax, 0x0002                          ; zero base cluster number
          xor     cx, cx
          mov     cl, BYTE [bpbSectorsPerCluster]     ; convert byte to word
          mul     cx
          add     ax, WORD [datasector]               ; base data sector
          ret

;-------------------------------------------
; Convert LBA to CHS
; AX=>LBA Address to convert
;
; absolute sector = (logical sector / sectors per track) + 1
; absolute head   = (logical sector / sectors per track) MOD number of heads
; absolute track  = logical sector / (sectors per track * number of heads)
;
;-------------------------------------------

LBACHS:
          xor     dx, dx                              ; prepare dx:ax for operation
          div     WORD [bpbSectorsPerTrack]           ; calculate
          inc     dl                                  ; adjust for sector 0
          mov     BYTE [absoluteSector], dl
          xor     dx, dx                              ; prepare dx:ax for operation
          div     WORD [bpbHeadsPerCylinder]          ; calculate
          mov     BYTE [absoluteHead], dl
          mov     BYTE [absoluteTrack], al
          ret

;---------------------------------------------
; Reads a series of sectors
; CX=>Number of sectors to read
; AX=>Starting sector
; ES:BX=>Buffer to read to
;---------------------------------------------

ReadSectors:
     .MAIN
          mov     di, 0x0005                          ; five retries for error
     .SECTORLOOP
          push    ax
          push    bx
          push    cx
          call    LBACHS                              ; convert starting sector to CHS
          mov     ah, 0x02                            ; BIOS read sector
          mov     al, 0x01                            ; read one sector
          mov     ch, BYTE [absoluteTrack]            ; track
          mov     cl, BYTE [absoluteSector]           ; sector
          mov     dh, BYTE [absoluteHead]             ; head
          mov     dl, BYTE [bsDriveNumber]            ; drive
          int     0x13                                ; invoke BIOS
          jnc     .SUCCESS                            ; test for read error
          xor     ax, ax                              ; BIOS reset disk
          int     0x13                                ; invoke BIOS
          dec     di                                  ; decrement error counter
          pop     cx
          pop     bx
          pop     ax
          jnz     .SECTORLOOP                         ; attempt to read again
          int     0x18
     .SUCCESS
          mov     si, msgProgress
          call    Print
          pop     cx
          pop     bx
          pop     ax
          add     bx, WORD [bpbBytesPerSector]        ; queue next buffer
          inc     ax                                  ; queue next sector
          loop    .MAIN                               ; read next sector
          ret

;---------------------------------------
;	Bootloader Entry Point
;---------------------------------------

;0x00007E00 - 0x0009FFFF - Unused

main:
          cli								; disable interrupts
          mov     ax, 0x07C0				; setup registers to point to our segment
          mov     ds, ax
          mov     es, ax
          mov     fs, ax
          mov     gs, ax

;          mov     ax, 0x0000				; set the stack
;          mov     ss, ax
;		  mov	  sp, 0x7B00				;0x500-0x7bff is free

		mov		ax, 0x7e00					;512 bytes for stack
		mov		ss, ax
		mov		sp, 512

          sti								; restore interrupts

          mov     [bootdrive], dl
          cmp     dl, 0x80                  ; find what we are booting from
          jb      BOOT_FLOPPY
          mov     ah, 0x08
          xor     di, di                    ; set es:di=0:0 to prevent possible bios error
          mov     es, di
          int     0x13
          jc      FAILURE
          mov     al, cl					; maximum sector count
          and     al, 00111111b
          cbw
          mov     word[bpbSectorsPerTrack], ax  ; save Sectors Per Track
          mov     bl, dh                        ; bl = maximum Head number
          mov     bh, ah
          inc     bx
          mov     word[bpbHeadsPerCylinder], bx	; SAVE Heads Per Cylinder
          jmp     BOOT_FLOPPY

FAILURE:
          mov     si, msgFailure
          call    Print
          mov     ah, 0x00
          int     0x16                                ; await keypress
          int     0x19                                ; warm boot computer

;0x8000

BOOT_FLOPPY:

     LOAD_ROOT:

     ; compute size of root directory and store in "cx"
     
          xor     cx, cx
          xor     dx, dx
          mov     ax, 0x0020                           ; 32 byte directory entry
          mul     WORD [bpbRootEntries]                ; total size of directory
          div     WORD [bpbBytesPerSector]             ; sectors used by directory
          xchg    ax, cx

     ; compute location of root directory and store in "ax"
     
          mov     al, BYTE [bpbNumberOfFATs]            ; number of FATs
          mul     WORD [bpbSectorsPerFAT]               ; sectors used by FATs
          add     ax, WORD [bpbReservedSectors]         ; adjust for bootsector
          mov     WORD [datasector], ax                 ; base of root directory
          add     WORD [datasector], cx

     ; read root directory into memory (7C00:0200)
     
          mov     bx, 0x0200                            ; copy root dir above bootcode
          call    ReadSectors

     ; browse root directory for binary image
          mov     cx, WORD [bpbRootEntries]             ; load loop counter
          mov     di, 0x0200                            ; locate first root entry
     .LOOP:
          push    cx
          mov     cx, 0x000B                            ; eleven character name
          mov     si, ImageName                         ; image name to find
          push    di
     rep  cmpsb                                         ; test for entry match
          pop     di
          je      LOAD_FAT
          pop     cx
          add     di, 0x0020                            ; queue next directory entry
          loop    .LOOP
          jmp     FAILURE

     LOAD_FAT:

          mov     dx, WORD [di + 0x001A]
          mov     WORD [cluster], dx                  ; file's first cluster
          xor     ax, ax
          mov     al, BYTE [bpbNumberOfFATs]          ; number of FATs
          mul     WORD [bpbSectorsPerFAT]             ; sectors used by FATs
          mov     cx, ax
          mov     ax, WORD [bpbReservedSectors]       ; adjust for bootsector
          mov     bx, 0x0200                          ; copy FAT above bootcode
          call    ReadSectors
;          mov     ax, 0x0050
		mov		ax, 0x0800
          mov     es, ax                              ; destination for image
          mov     bx, 0x0000                          ; destination for image
          push    bx

     LOAD_IMAGE:
          mov     ax, WORD [cluster]                  ; cluster to read
          pop     bx                                  ; buffer to read into
          call    ClusterLBA                          ; convert cluster to LBA
          xor     cx, cx
          mov     cl, BYTE [bpbSectorsPerCluster]     ; sectors to read
          call    ReadSectors
          push    bx
          mov     ax, WORD [cluster]                  ; identify current cluster
          mov     cx, ax                              ; copy current cluster
          mov     dx, ax                              ; copy current cluster
          shr     dx, 0x0001                          ; divide by two
          add     cx, dx                              ; sum for (3/2)
          mov     bx, 0x0200                          ; location of FAT in memory
          add     bx, cx                              ; index into FAT
          mov     dx, WORD [bx]                       ; read two bytes from FAT
          test    ax, 0x0001
          jnz     .ODD_CLUSTER   
     .EVEN_CLUSTER:
         and     dx, 0000111111111111b               ; take low twelve bits
         jmp     .DONE 
     .ODD_CLUSTER:
          shr     dx, 0x0004                          ; take high twelve bits
     .DONE:
          mov     WORD [cluster], dx                  ; store new cluster
          cmp     dx, 0x0FF0                          ; test for end of file
          jb      LOAD_IMAGE
     DONE:
          mov     si, msgCRLF
          call    Print
          mov     dl, [bootdrive]
          mov     dh, BOOT_FSYS
		  push    WORD 0x0800
          push    WORD 0x0000
          retf										; execute krnldr

bootdrive   db 0
datasector  dw 0x0000
cluster     dw 0x0000
ImageName   db "NBOOT      "
msgCRLF     db 0x0D, 0x0A, 0x00
msgProgress db ".", 0x00
msgFailure  db 0x0D, 0x0A, "Boot failed", 0x0D, 0x0A,0x00

TIMES 510-($-$$) DB 0
DW FAT12_STAGE1_SIGNITURE
