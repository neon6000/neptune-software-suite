;*********************************************
;	stage1.asm
;		- fat32 VBR
;
;   Copyright BrokenThorn Entertainment Co
;*********************************************

;largest file tested with: 309K 50MB image

;FTRFS theoritical file system
;http://www.reddit.com/r/osdev/comments/rh7zy/time_for_someone_to_start_working_on_ftrfs/

;partcopy bootsect.bin 0 3 C.IMG 0 
;partcopy bootsect.bin 5a 1a6 C.IMG 5a

section .text
bits 16
org 0

jmp short main
nop
;
;	bios paramater block area is from offset 0 to 0x5a
;	actual code starts at offset 0x5a. Formatting software
;	installs the BPB in this area
;
times 0x5a db 0

;
;	nboot is loaded to 0x800:0
;
%define LOAD_SEG    0
%define LOAD_OFFSET 0x8000

;=================================================;
;                                                 ;
; Entry point                                     ;
;                                                 ;
;=================================================;

main:
	cli
	push 0x7c0
	push .fix_cs
	retf						; jump to 0x7c0:fix_cs
.fix_cs:
	mov	ax, 0x7c0				; set segments
	mov	es, ax
	mov	ds, ax
	mov	fs, ax
	mov	gs, ax
	mov	ax, 0
	mov	ss, ax
	mov	sp, 0x2000				; ss:sp = stack top at 0x2000
	mov	bp, 0x7c00				; ss:bp = bios param block
	sti
	;
	; first_data_sector = fat_boot->reserved_sector_count + (fat_boot->table_count * fat_boot->table_size_16);
	;
	movzx	edx, byte [bp + biosParamBlock.fatCount]
	mov	eax, dword [bp + biosParamBlock.fatSize32]
	imul	edx
	movzx	edx, word [bp + biosParamBlock.resSectors]
	add	eax, edx
	mov	[dataCluster], eax
	call	loadFile				; load boot loader
.execute:
    push LOAD_SEG
    push LOAD_OFFSET
    retf

;=================================================;
;                                                 ;
;  Fat32 structures                               ;
;                                                 ;
;=================================================;

;
;	Bios Paramater Block
;
struc biosParamBlock
	.jump             resb 3             ; jump instruction
	.ident            resb 8             ; oem identifier
        .bps              resw 1             ; bytes per sector
	.spc              resb 1             ; sectors per cluster
	.resSectors       resw 1             ; reserved sector count
	.fatCount         resb 1             ; number of FATs
	.dirEntryCount    resw 1             ; number of directory enteries
	.sectorCount      resw 1             ; number of sectors in logical volume
	.mediaDescr       resb 1             ; media descriptor byte
	.fatSectorCount   resw 1             ; sectors per FAT (fat12/16 only)
	.trackSectorCount resw 1             ; sectors per track
	.headCount        resw 1             ; number of heads
	.hiddenSectCount  resd 1             ; number of hidden sectors
	.sectorCountBig   resd 1             ; only set if sectorCount field is 0 (more then 65535 sectors)
	;
	;	extended boot record for fat32
	;
	.fatSize32        resd 1             ; Sectors per FAT. The size of the FAT in sectors
	.flags            resw 1             ; flags
	.fatVersion       resw 1             ; FAT version number
	.rootCluster      resd 1             ; cluster number of root directory. Often 2
	.fsInfoCluster    resw 1             ; cluster number of FSinfo structure
	.bootCluster      resw 1             ; cluster number of backed up boot sector
	.reserved         resb 12
	.driveNum         resb 1             ; drive number, identical to the values returned by the BIOS
	.winNTflags       resb 1             ; flags in Windows NT
	.signiture        resb 1             ; must be 0x28 or 0x29
	.volumeId         resd 1             ; serial number
	.label            resb 11            ; volume label string padded with spaces
	.systemId         resb 8             ; system idenitifier string always "FAT32   "
endstruc

;
;	Directory entry
;
struc directoryEntry
	.name            resb 11             ; entry name
	.attrib          resb 1              ; attributes
	.winNT           resb 1              ; windows NT reserved
	.createTimeSec   resb 1              ; in tenths of a second
	.createTime      resw 1
	.createDate      resw 1
	.lastAccess      resw 1
	.firstClusterHi  resw 1              ; first cluster of entry
	.lastModify      resw 1
	.lastModifyDate  resw 1
	.firstClusterLo  resw 1              ; first cluster of entry
	.size            resd 1              ; size of file
endstruc

;=================================================;
;                                                 ;
; Display error and restart                       ;
;                                                 ;
;=================================================;

restart:
	mov     si, msgError			; display error
	call    puts
	xor     ax, ax
	int     0x16				; wait for keypress
	int	0x19				; reboot
	cli
	hlt

;=================================================;
;                                                 ;
; Display string                                  ;
;                                                 ;
;=================================================;

puts:
	mov     ax, 0x0e0d
	mov     bx, 0x07
.next:
	int     0x10
	lodsb
	test	al,al
	jnz     .next
	ret

;=================================================;
;                                                 ;
; Read sectors                                    ;
; Input:                                          ;
;	EAX = sector LBA                          ;
;	DI = number of sectors                    ;
;	ES:BX = destination                       ;
; Output:                                         ;
;       Sectors loaded to ES:BX                   ;
;	ES:BX = destination+size                  ;
;                                                 ;
;=================================================;

readSectors:
	push		dx
.loop:
	push		eax
	;
	;	push disk address packet on stack
	;
	push		word 0			; hi 32 bits lba
	push		word 0
	push		eax			; low 32 bits lba
	push		es			; segment
	push		bx			; offset
	push		word 1			; number of sectors
	push		word 16			; size of address packet
	;
	;	load sector
	;
	mov		si, sp			; ds:si points to packet
	mov		dl, [bp + biosParamBlock.driveNum]
	push		ds
	mov		ax, ss
	mov		ds, ax
	mov		ah, 0x42
	int		0x13
	jc		.error
	pop		ds
	add		sp, 16			; pop stack
	pop		eax
.next:
	add		bx, word [bp + biosParamBlock.bps]
	jnc		.ready			; watch for overflow

.nextSegment:
	mov		dx, es
	add		dh, 0x10		; go to next segment
	mov		es, dx
.ready:
	inc		eax			; next LBA
	dec		di			; loop counter
	jnz		.loop			; loop
.done:
	pop		dx
	ret
.error:
	pop		ds
;;	add		sp, 16			; dont need to pop stack since we are restarting
;;	pop		ax
	call		restart			; bail out

;=================================================;
;                                                 ;
; Read cluster                                    ;
; Input:                                          ;
;	EAX = cluster                             ;
;	ES:BX = destination                       ;
; Output:                                         ;
;	ES:BX pointer to cluster                  ;
;	EAX = next cluster in chain               ;
;                                                 ;
;=================================================;

readCluster:
	;
	;	sector = (bsSecsPerClust * (cluster# - 2)) + datastart
	;
	push	eax				; save cluster number
	sub	eax, 2
	movzx	cx, byte [bp + biosParamBlock.spc] ; cx = sectors per cluster
	imul	cx
	add	eax, dword [dataCluster]	; eax = sector
	;
	;	read cluster
	;
	mov	di, cx				; sectors per cluster
	call	readSectors			; load to ES:BX
	;
	;	fat_offset = active_cluster * 4
	;
	pop	eax				; restore cluster number
	shl	eax, 2				; multiply by 4
	mov	ecx, eax			; ecx = fat offset
	;
	;	calculate cluster size
	;
	mov	ax, [bp + biosParamBlock.bps] ; cluster size = bytes per sector*sectors per cluster
	movzx	dx, byte [bp + biosParamBlock.spc]
	imul	dx				; ax = cluster size
	push	ebx
	movzx	ebx, ax				; bx = cluster size
	;
	;	fat_sector = first_fat_sector + (fat_offset / cluster_size)
	;
	xor	edx, edx
	mov	eax, ecx
	idiv	ebx
	movzx	ebx, word [bp + biosParamBlock.resSectors]
	add	eax, ebx			; eax = sector
	pop	ebx
	push	ebx				; save es:bx
	push	es
	mov	bx, 0x7e0			; read fat sector to 0x7e00
	mov	es, bx
	xor	ebx, ebx
	mov	di, 1				; 1 sector
	call	readSectors			; eax = sector lba of fat
	;
	;	get next cluster in chain
	;
	mov	ebx, edx
	mov	eax, dword [es:ebx]
	pop	es
	pop	ebx
	ret

;=================================================;
;                                                 ;
; Load file                                       ;
;                                                 ;
;=================================================;

;
;	fat32 FAT EOF cluster ID
;
%define FAT_LAST_CLUSTER  0x0FFFFFF8

loadFile:
	;
	;	load root directory
	;
	mov	ax, LOAD_SEG
	mov	es, ax
	mov	bx, LOAD_OFFSET				; es:bx = destination
	mov	eax, [bp + biosParamBlock.rootCluster]
.loadRoot:
	call	readCluster
	add	dword [rootEntryCount], 32
	cmp	eax, FAT_LAST_CLUSTER			; check if this is the last cluster
	jl	.loadRoot
	;
	;	search for file
	;
	xor	eax, eax
	mov	bx, LOAD_OFFSET				; bx points to first directory entry
.search:
	cmp	byte [es:bx], 0				; if entry name begins with 0 or 0xe5, skip
	je	.next
	cmp	byte [es:bx], 0xe5
	je	.next
.compare:
	mov	si, imageName				; compare name
	mov	di, bx
	mov	cx, 11
	repe	cmpsb
	jz	.found
.next:
	add	bx, 32					; go to next entry
	inc	eax
	cmp	eax, [rootEntryCount]
	jne	.search

.not_found:
	call	restart
.found:
	;
	;	load file
	;
	mov	ax, word [es:bx + directoryEntry.firstClusterHi]
	shl	eax, 16
	mov	ax, word [es:bx + directoryEntry.firstClusterLo]
	mov	bx, LOAD_OFFSET				; es:bx = destination
.nextCluster:
	call	readCluster
	cmp	eax, FAT_LAST_CLUSTER			; check if this is the last cluster
	jl	.nextCluster
.success:
	ret

;=================================================;
;                                                 ;
; Variables                                       ;
;                                                 ;
;=================================================;

imageName   db "NBOOT      "            ; 11 byte padded name
msgError    db "Unable to load nboot.", 13, 10, 0
dataCluster dd 0
rootEntryCount dd 0

times 510-$+$$ db 0			; image must be 512 bytes
dw 0xaa55				; boot signiture
