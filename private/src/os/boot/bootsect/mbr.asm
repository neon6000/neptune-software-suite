
;*********************************************
;	mbr.asm
;		- Master Boot Record
;
;   Copyright BrokenThorn Entertainment Co
;*********************************************

%include "mbr.inc"

bits 16

org		0

;=================================================;
;                                                 ;
; Relocate ourself to 0:600                       ;
;                                                 ;
;=================================================;

main:

	cli
	mov		ax, 0x7c0				;we are at 0x7c0:0
	mov		ss, ax
	mov		ds, ax
	mov		fs, ax
	mov		gs, ax
	mov		sp, 0x7d0
	sti

	xor		si, si					;relocate ourself to 0:RELOC_ADDR
	push		word 0
	pop		es
	mov		di, RELOC_ADDR
	mov		cx, 256					;256 words=512 bytes
	cld
	repnz
	movsw
	jmp		0:RELOC_ADDR+.relocated			;jump to relocated code

;=================================================;
;                                                 ;
; Prepare to read partition table                 ;
;                                                 ;
;=================================================;

.relocated:

; relocated at 0:0x600

	cli
	mov		ax, 0x60				;we are at 0x60:0
	mov		ss, ax
	mov		ds, ax
	mov		fs, ax
	mov		gs, ax
	mov		sp, 0x9ff
	sti
	mov		[bios_disk], dl				;save disk number passed from BIOS
	mov		si, message
	call		puts

	mov		si, partition_table
	mov		cx, MBR_NUM_ENTRIES
	mov		bx, MBR_NUM_ENTRIES			;for second loop later on

;=================================================;
;                                                 ;
; Find an active partition                        ;
;                                                 ;
;=================================================;

.loop:

	cmp		byte [si], MBR_ENTRY_ACTIVE		; find a valid parition entry
	je		.found_active

	cmp		byte [si], MBR_ENTRY_INACTIVE		; its not active, test if its inactive
	jne		.found_inactive				; its neither, invalid table entry

	add		si, MBR_ENTRY_SIZE			; inactive entry, move to next one

	dec		bx
	loopz		.loop

	mov		si, none
	call		puts
	call		halt

;=================================================;
;                                                 ;
; Invalid partition found, stop boot              ;
;                                                 ;
;=================================================;

.found_inactive:

	mov		si, invalid
	call		puts
	call		halt

;=================================================;
;                                                 ;
; Check for more then 1 active partitions         ;
;                                                 ;
;=================================================;

.found_active:

	; save partition info for later
	mov		dh, 0
	mov		dl, [si+mbr_entry.m_startHead]
	mov		cx, [si+mbr_entry.m_startSect]

	; save boot table entry for later
	mov		bp, si

	; make sure there are no other active paritions
.loop2:

	add		si, MBR_ENTRY_SIZE
	dec		bx
	jc		read_boot			; end of table

	cmp		byte [si], MBR_ENTRY_ACTIVE
	je		.found_inactive
	jz		.loop2

;=================================================;
;                                                 ;
; Load partition boot sector to 0x7c0:0           ;
;                                                 ;
;=================================================;

; already set:
;;	dl == [si+mbr_entry.m_startHead]
;;	cx == [si+mbr_entry.m_startSect]

read_boot:

	mov     	di, 0x0005			; five retries for error
.loop:
	mov		ax, 0x7c0
	mov		es, ax
	mov		bx, 0x0				; es:bx=0:0x7c00
	mov		ax, 0x0201			; read boot sector
	push		dx
	mov			dh, dl ;head number
	mov			dl, 0x80 ;drive number
	int		0x13				; cl and dl are already set, read sector
	pop		dx

	jnc		.done 
	xor		ax, ax				; reset disk
	int		0x13
	dec		di
	jnz		.loop				; try again
	mov		si, error			; 5 times failed, bail out
	call		puts
	call		halt

;=================================================;
;                                                 ;
; Check for a valid boot signiture                ;
;                                                 ;
;=================================================;

.done:

	mov		ax, 0x7c0
	mov		es, ax
	mov		di, 0x1fe
	cmp		word [es:di], BOOT_SIG	; insure boot sig is there
	je		.found
	jmp .found
	mov		si, no_os
	call		puts
	call		halt

;=================================================;
;                                                 ;
; Jump to boot code                               ;
;                                                 ;
;=================================================;

.found:

	; restore boot entry and call boot code
	mov		si, bp
	mov		dl, [bios_disk]
	jmp		0:0x7c00

;=================================================;
;                                                 ;
; Halt boot process                               ;
;                                                 ;
;=================================================;

halt:
	cli
	hlt

;=================================================;
;                                                 ;
; Display a string                                ;
;                                                 ;
;=================================================;

puts:
	mov     ax, 0x0e0d
	mov     bx, 0x07
.next:       
	int     0x10
	lodsb
	test	al,al
	jnz     .next
	ret

;=================================================;
;                                                 ;
; Data Area                                       ;
;                                                 ;
;=================================================;

message db "Loading...",13,10,0
error db "Error loading operating system.",13,10,0
invalid db "Invalid partition table.", 13, 10, 0
none	db "No paritition table.", 13, 10, 0
no_os	db "No operating system found.", 13, 10, 0
bios_disk db 0

;=================================================;
;                                                 ;
; Partition Table Area and Boot Signiture         ;
;                                                 ;
;=================================================;

; partition table must be at 0:0x07be
; fill the rest of the sector with 0's

times 0x1be-($-$$) db 0

partition_table:

%rep MBR_NUM_ENTRIES
   istruc mbr_entry
      at mbr_entry.m_boot,	db MBR_ENTRY_INACTIVE
      at mbr_entry.m_startHead,	db 0
      at mbr_entry.m_startSect,	dw 0
      at mbr_entry.m_id,	db 0
      at mbr_entry.m_endHead,	db 0
      at mbr_entry.m_endSect,	dw 0
      at mbr_entry.m_partSect,	dd 0
      at mbr_entry.m_partSize,	dd 0
   iend
%endrep

dw BOOT_SIG
