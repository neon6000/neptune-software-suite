
;*********************************************
;	mbr.inc
;		- Master Boot Record
;
;   Copyright BrokenThorn Entertainment Co
;*********************************************

; address to load boot sector to
%define BOOT_SECT_ADDR	0x7c00

; our relocation addr
%define RELOC_ADDR		0x600

; boot signiture
%define BOOT_SIG		0xAA55

; valid values for mbr_entry.m_boot
%define MBR_ENTRY_ACTIVE	0x80
%define MBR_ENTRY_INACTIVE	0

; number of entries allowed
%define MBR_NUM_ENTRIES		4
%define MBR_ENTRY_SIZE		16	;must be 16 bytes

; mbr boot entry format

struc mbr_entry
   .m_boot		resb   1
   .m_startHead	resb   1
   .m_startSect	resw   1	;first 6 bits: starting sector, last 10 bits: starting cylinder
   .m_id		resb   1
   .m_endHead	resb   1
   .m_endSect	resw   1	;first 6 bits: starting sector, last 10 bits: starting cylinder
   .m_partSect	resd   1	;paritions starting lba
   .m_partSize	resd   1
endstruc
