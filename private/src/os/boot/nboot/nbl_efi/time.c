/************************************************************************
*
*	EFI NBL Time Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "efi.h"

/* pointer to system table. */
STATIC efiSystemTable* _st = 0;

/* self reference. */
STATIC nblDriverObject* _self = 0;

/* real time clock device object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiRtcDriverObject;
STATIC nblDeviceObject _rtc = {
	NBL_DEVICE_OTHER_CONTROLLER,
	NBL_DEVICE_CONTROLLER_CLASS,
	0, &_nEfiRtcDriverObject, 0, 0, "rtc", 0
};

/* returns EFI system table. */
STATIC efiSystemTable* EfiRtcGetSystemTable (void) {

	nblDriverObject* fm;
	nblMessage       m;

	/* get EFI system table pointer. */
	if (_st)
		return _st;

	/* open firmware efi driver. */
	fm = BlrOpenDriver ("root/fw");
	if (!fm) {
		/* this is fatal. */
		BlrPrintf("\n\rRTC Can't get system table.");
		return NULL;
	}

	/* call driver. */
	m.type = NBL_FW_EFI_SERVICES;
	if (fm->request(&m) == NBL_STAT_SUCCESS)
		_st = (efiSystemTable*) m.retval;
	return _st;
}

/* gets time. */
STATIC status_t EfiRtcGetTime (OUT nblTime* clock) {

	efiTime time;
	efiRuntimeServices* rs;

	if (! _st)
		return NBL_STAT_DRIVER;

	if (! clock)
		return NBL_STAT_INVALID_ARGUMENT;

	rs = _st->runtime;
	if ( EFI_CALL(rs->getTime,2, &time, 0) != NBL_STAT_SUCCESS)
		return NBL_STAT_HARDWARE_ERROR;

	clock->year       = time.year;
	clock->month      = time.month;
	clock->day        = time.day;
	clock->hour       = time.hour;
	clock->minute     = time.minute;
	clock->second     = time.second;
	clock->nanosecond = time.nanosecond;
	clock->timezone   = time.timezone;
	clock->daylight   = time.daylight;

	return NBL_STAT_SUCCESS;
}

/* sets time. */
STATIC status_t EfiRtcSetTime (IN nblTime* clock) {

	efiTime time;
	efiRuntimeServices* rs;

	if (! _st)
		return NBL_STAT_DRIVER;

	if (! clock)
		return NBL_STAT_INVALID_ARGUMENT;

	time.year       = clock->year;
	time.month      = clock->month;
	time.day        = clock->day;
	time.hour       = clock->hour;
	time.minute     = clock->minute;
	time.second     = clock->second;
	time.nanosecond = clock->nanosecond;
	time.timezone   = clock->timezone;
	time.daylight   = clock->daylight;

	rs = _st->runtime;
	if ( EFI_CALL(rs->setTime,1, &time) != NBL_STAT_SUCCESS)
		return NBL_STAT_HARDWARE_ERROR;

	return NBL_STAT_SUCCESS;
}

/* driver entry point. */
status_t EfiRtcEntryPoint (IN nblDriverObject* self, IN int dependencyCount,
						 IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	nblMessage rpb;

	_self = self;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	/* create rtc device. */
	rpb.source = self;
	rpb.type   = NBL_DEV_REGISTER;
	rpb.NBL_DEV_REGISTER_OBJ = &_rtc;
	if (dm->request (&rpb) != NBL_STAT_SUCCESS)
		return NBL_STAT_DEPENDENCY;

	/* get efi boot services table and return success. */
	if (! EfiRtcGetSystemTable () )
		return NBL_STAT_DEPENDENCY;

	return NBL_STAT_SUCCESS;
}

/* stall system. */
STATIC status_t EfiRtcStall (IN efi_uintn ms) {

	EFI_CALL(_st->boottime->stall, 1, ms);
	return NBL_STAT_SUCCESS;
}

/* driver requests. */
status_t EfiRtcRequest (IN nblMessage* rpb) {
	switch (rpb->type) {
		case NBL_RTC_GETTIME:
			return EfiRtcGetTime((nblTime*) rpb->NBL_RTC_GETTIME_DESCR);
		case NBL_RTC_SETTIME:
			return EfiRtcSetTime((nblTime*) rpb->NBL_RTC_SETTIME_DESCR);
		case NBL_RTC_STALL:
			return EfiRtcStall ( (efi_uintn) rpb->NBL_RTC_STALL_MS );
		default:
			return NBL_STAT_UNSUPPORTED;
	};
}

#define NBL_EFI_RTC_VERSION 1

/* EFI driver object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiRtcDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_EFI_RTC_VERSION,
	NBL_DRV_LOAD_CORE,
	NBL_DRV_CLASS_OTHER,
	"rtc",
	EfiRtcEntryPoint,
	EfiRtcRequest,
	1,
	"dm"
};

/* For now. This is called directly by NBOOT
for BIOS and UEFI builds. */

PRIVATE unsigned int RtcGetTicks() {

	nblTime time;
	EfiRtcGetTime(&time);
	return time.second * 1000;
}

