/************************************************************************
*
*	EFI memory driver.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "efi.h"

/* The memory driver component implements the EFI memory device object.
Note that this driver may be called before its entry point in order
to initialize the heap. */

/* helper macros. */
#define BYTES_TO_PAGES(bytes)	(((bytes) + 0xfff) >> 12)
#define PAGES_TO_BYTES(pages)	((pages) << 12)

/* The minimum and maximum heap size.  */
#define MIN_HEAP_SIZE	0x100000
#define MAX_HEAP_SIZE	(1600 * 0x100000)

/* The size of a memory map obtained from the firmware. This must be
   a multiplier of 4KB.  */
#define MEMORY_MAP_SIZE	0x3000

/* next memory descriptor. */
#define MM_NEXT_DESCR(descr,size) ((efiMemoryDesc*)((char*) descr + size))

/* pointer to system table. */
STATIC efiSystemTable* _st = 0;

/* self reference. */
STATIC nblDriverObject* _self = 0;

/* memory device object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiMmDriverObject;
STATIC nblDeviceObject _mm = {
	NBL_DEVICE_MEMORY_UNIT,
	NBL_DEVICE_MEMORY_CLASS,
	0, &_nEfiMmDriverObject, 0, 0, "mm", 0
};

/* returns EFI system table. */
STATIC efiSystemTable* EfiMmGetSystemTable (void) {

	nblDriverObject* fm;
	nblMessage       m;

	/* get EFI system table pointer. */
	if (_st)
		return _st;

	/* open firmware efi driver. */
	fm = BlrOpenDriver ("root/fw");
	if (!fm) {
		/* this is fatal. */
		BlrPrintf("\n\rCan't get system table.");
		return NULL;
	}

	/* call driver. */
	m.type = NBL_FW_EFI_SERVICES;
	if (fm->request(&m) == NBL_STAT_SUCCESS)
		_st = (efiSystemTable*) m.retval;
	return _st;
}

/* allocates pages at specified address. */
STATIC void* EfiMmAllocatePages(IN addr_t phys, IN uint32_t pages) {

	efiBootServices* bs;
	efi_status_t result;
	int type;

	if (! EfiMmGetSystemTable())
		return NULL;

	bs   = _st->boottime;
	type = EFI_ALLOCATE_ADDRESS;

	if (phys == 0)
		type = EFI_ALLOCATE_ANY_PAGES;

	result = EFI_CALL (bs->allocatePages, 4, type, EFI_LOADER_DATA, pages, &phys);
	if (result != EFI_SUCCESS)
		return NULL;
	return (void*) phys;
}

/* frees pages at specified address. */
STATIC void EfiMmFreePages(IN addr_t phys, IN uint32_t pages) {

	efiBootServices* bs;

	if (! EfiMmGetSystemTable()) return;
	bs = _st->boottime;

	EFI_CALL (bs->freePages, 2, phys, pages);
}

/* get memory map. */
STATIC efi_status_t EfiMmGetMemoryMap (IN OUT efi_uintn* size,IN OUT efiMemoryDesc* memoryMap,
	OUT OPTIONAL efi_uintn* mapKey, OUT efi_uintn* descriptorSize, OUT OPTIONAL efi_uint32_t* descriptorVersion) {

	efi_uintn    key;
	efi_uint32_t version;
	efiBootServices* bs;

	if (! EfiMmGetSystemTable()) return NBL_STAT_DRIVER;
	bs = _st->boottime;

	/* these are optional. */
	if (!mapKey)
		mapKey = &key;
	if (!descriptorVersion)
		descriptorVersion = &version;

	return EFI_CALL (bs->getMemoryMap, 5, size, memoryMap, mapKey, descriptorSize, descriptorVersion);
}

/* get total number of pages. */
STATIC efi_uint64_t EfiMmGetPageCount (IN efiMemoryDesc* mmap, IN efi_uintn descSize, IN efiMemoryDesc* end) {

	efiMemoryDesc* desc;
	efi_uint64_t count = 0;

	for (desc = mmap; desc < end; desc = MM_NEXT_DESCR(desc,descSize))
		count += desc->numPages;
	return count;
}

/* print memory map. */
STATIC void EfiMmPrintMap(IN efiMemoryDesc* map, efi_uintn descrSize, efiMemoryDesc* end) {

	efiMemoryDesc* desc;
	int i = 0;

	BlrPrintf("\n\rEfiMmPrintMap: ");
	for (desc = map; desc < end; desc = MM_NEXT_DESCR(desc, descrSize)) {
		BlrPrintf("\n\r%i type=%x phys=%llx virt=%llx", ++i, desc->type, desc->physAddr, desc->virtAddr);
		BlrPrintf(" pages=%llu attr=%llx", desc->numPages, desc->attribute);
	}
}

/* get memory map. */
STATIC void EfiMmGetMap (void) {

	efiMemoryDesc* mmap;
	efiMemoryDesc* end;
	efi_status_t   status;
	efi_uintn      size;
	efi_uintn      descriptorSize;

	size = MEMORY_MAP_SIZE;

	mmap = (efiMemoryDesc*) EfiMmAllocatePages (0, BYTES_TO_PAGES(MEMORY_MAP_SIZE));
	if (!mmap) {
		BlrPrintf("\n\rEfiGetMap: Cannot allocate memory.");
		return;
	}

	status = EfiMmGetMemoryMap (&size, mmap, 0, &descriptorSize, 0);
	switch(status) {
		case EFI_BUFFER_TOO_SMALL:
			BlrPrintf("\n\rEFI_BUFFER_TOO_SMALL"); return;
		case EFI_INVALID_PARAMETER:
			BlrPrintf("\n\rEFI_INVALID_PARAMETER"); return;
	};

	end = MM_NEXT_DESCR (mmap, size);

	EfiMmPrintMap (mmap, descriptorSize, end);

}

/* driver entry point. */
status_t EfiMmEntryPoint (IN nblDriverObject* self, IN int dependencyCount,
						 IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	nblMessage rpb;
	STATIC _initialize = FALSE;

	if (_initialize)
		return NBL_STAT_SUCCESS;

	_self = self;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

//	EfiMmGetMap();

	/* create memory device. */
	rpb.source = self;
	rpb.type   = NBL_DEV_REGISTER;
	rpb.NBL_DEV_REGISTER_OBJ = &_mm;
	if (dm->request (&rpb) != NBL_STAT_SUCCESS)
		return NBL_STAT_DEPENDENCY;

	_initialize = TRUE;
	return NBL_STAT_SUCCESS;
}

status_t NBL_CALL EfiMmInit (NBL_MM_INIT_FUNC func) {

	STATIC addr_t _base        = NULL;
	STATIC BOOL   _initialized = FALSE;

	if (!func)
		return NBL_STAT_INVALID_ARGUMENT;

	if (_initialized) {
		func (_base, MIN_HEAP_SIZE, 0);
		return NBL_STAT_SUCCESS;
	}

	_base = (addr_t) EfiMmAllocatePages(0, BYTES_TO_PAGES(MIN_HEAP_SIZE));
	if (! _base)
		return NBL_STAT_MEMORY;

	func (_base,  MIN_HEAP_SIZE, 0);

	_initialized = TRUE;
	return NBL_STAT_SUCCESS;
}

/* driver requests. */
status_t EfiMmRequest (IN nblMessage* rpb) {
	rettype_t r = 0;
	rpb->retval = 0;
	switch (rpb->type) {
		case NBL_MM_ALLOC:
			r = (rettype_t) EfiMmAllocatePages(rpb->NBL_MM_ALLOC_ADDRESS, rpb->NBL_MM_ALLOC_SIZE);
			break;
		case NBL_MM_FREE:
			EfiMmFreePages(rpb->NBL_MM_FREE_ADDRESS,rpb->NBL_MM_FREE_SIZE);
			break;
		case NBL_MM_GETMAP:
			EfiMmGetMap();
			break;
		case NBL_MM_INIT:
			r = (rettype_t) EfiMmInit ( (NBL_MM_INIT_FUNC) rpb->NBL_MM_INIT_FUNCTION);
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_EFI_MM_VERSION 1

/* EFI driver object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiMmDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_EFI_MM_VERSION,
	NBL_DRV_LOAD_CORE,
	NBL_DRV_CLASS_FW,
	"mm",
	EfiMmEntryPoint,
	EfiMmRequest,
	1,
	"dm"
};
