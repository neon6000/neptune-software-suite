/************************************************************************
*
*	NBL EFI Firmware Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nbl.h"
#include "efi.h"

efiSystemTable*  _st;
STATIC efi_handle_t     _imageHandle;
STATIC nblDriverObject* _self;

/**
*	EfiExit
*
*	Description : Return back to the firmware.
*
*	Input : status - EFI Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void EfiExit (IN EFI_STATUS status) {

	EFI_CALL (_st->boottime->exit, 4, _imageHandle, status, 0, 0);
}

/**
*	EfiReset
*
*	Description : Reset system.
*
*	Input : status - EFI Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void EfiReset (IN EFI_STATUS status) {

	EFI_CALL (_st->runtime->resetSystem, 4, EFI_RESET_COLD, status, 0, NULL);
}

/**
*	EfiEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	_self = self;
	return NBL_STAT_SUCCESS;
}

/**
*	EfiRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiRequest (IN nblMessage* rpb) {

	rettype_t r;
	rpb->retval = 0;
	r = 0;
	switch (rpb->type) {
		case NBL_FW_EXIT:
			EfiExit (rpb->NBL_FW_EXIT_STATUS);
			break;
		case NBL_FW_RESET:
			EfiReset (rpb->NBL_FW_RESET_STATUS);
			break;
		case NBL_FW_EFI_SERVICES:
			r = (rettype_t) _st;
			break;
		case NBL_FW_EFI_IMAGEHANDLE:
			r = (rettype_t) _imageHandle;
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_EFI_VERSION 1

/* EFI driver object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_EFI_VERSION,
	NBL_DRV_LOAD_CORE,
	NBL_DRV_CLASS_FW,
	"fw",
	EfiEntryPoint,
	EfiRequest,
	0
};

/**
*	BlFwStartup
*
*	Description : Firmware driver initialization. This is called by EFI.
*
*	Input : imageHandle - EFI supplied image handle.
*           st - EFI supplied system table.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
efi_status_t BlFwStartup (efi_handle_t imageHandle, efiSystemTable* st) {

	_st = st;
	_imageHandle = imageHandle;

	/* call boot application. */
	if (BlStartBootApplication () == NBL_STAT_SUCCESS)
		return EFI_SUCCESS;
	return EFI_LOAD_ERROR;
}
