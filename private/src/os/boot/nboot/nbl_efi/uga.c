/************************************************************************
*
*	NBL EFI Graphics Output Protocol (GOP) Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "efi.h"

/* pointer to system table. */
STATIC efiSystemTable* _st = 0;

/* self reference. */
STATIC nblDriverObject* _self = 0;

/* protocol GUIDs. */
STATIC nblGUID _ugaIoGUID   = UGA_IO_PROTOCOL;
STATIC nblGUID _ugaDrawGUID = UGA_DRAW_PROTOCOL;

/* protocols. */
STATIC efiUgaDraw* _ugaDraw = NULL;
STATIC efiUgaIo*   _ugaIo   = NULL;

/* display device object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiUgaDriverObject;
STATIC nblDeviceObject _ugaDeviceObject = {
	NBL_DEVICE_DISPLAY_CONTROLLER,
	NBL_DEVICE_CONTROLLER_CLASS,
	0, &_nEfiUgaDriverObject, 0, 0, "uga", 0
};

/* returns EFI system table. */
STATIC efiSystemTable* EfiUgaGetSystemTable (void) {

	nblDriverObject* fm;
	nblMessage       m;

	/* get EFI system table pointer. */
	if (_st)
		return _st;

	/* open firmware efi driver. */
	fm = BlrOpenDriver ("root/fw");
	if (!fm)
		return NULL;

	/* get EFI boot services. */
	m.type = NBL_FW_EFI_SERVICES;
	if (fm->request(&m) == NBL_STAT_SUCCESS)
		_st = (efiSystemTable*) m.retval;

	return _st;
}

/* open protocol. */
STATIC void* EfiUgaLocateProtocol (IN nblGUID* protocol) {

	void*    protocolInterface;
	status_t status;

	status = EFI_CALL (_st->boottime->locateProtocol,3,protocol,NULL,&protocolInterface);

	if (status != EFI_SUCCESS) {
		BlrPrintf("\n\rUGA OPEN CODE: %x", status);
		return NULL;
	}

	return protocolInterface;
}

STATIC status_t EfiUgaSetMode (IN uint32_t mode) {

	EFI_CALL (_ugaDraw->setMode, 5, _ugaDraw, 800, 600, 32, 60);

	return 0;
}

STATIC status_t EfiUgaQueryModes (void) {

	EfiUgaSetMode(0);

	return NBL_STAT_SUCCESS;
}

/* driver entry point. */
status_t EfiUgaEntryPoint (IN nblDriverObject* self, IN int dependencyCount,
						 IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	_self = self;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	/* get efi boot services table and return success. */
	if (! EfiUgaGetSystemTable () )
		return NBL_STAT_DEPENDENCY;

	_ugaDraw = EfiUgaLocateProtocol(&_ugaDrawGUID);
	if (!_ugaDraw) {
		BlrPrintf("\n\rUGA DRAW FAIL");
		return 0;
	}

	EfiUgaQueryModes ();

	return NBL_STAT_SUCCESS;
}

/* driver requests. */
status_t EfiUgaRequest (IN nblMessage* rpb) {
	rettype_t r;
	rpb->retval = 0;
	switch (rpb->type) {



		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_EFI_UGA_VERSION 1

/* EFI driver object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiUgaDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_EFI_UGA_VERSION,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_VIDEO,
	"uga",
	EfiUgaEntryPoint,
	EfiUgaRequest,
	1,
	"dm"
};
