/************************************************************************
*                                                                       *
*	efi.h																*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

#ifndef EFI_H
#define EFI_H

#pragma pack(push,1)

/* basic types. */
typedef unsigned int efi_uint_t;
typedef uint8_t   efi_uint8_t;
typedef uint16_t  efi_uint16_t;
typedef uint32_t  efi_uint32_t;
typedef uint64_t  efi_uint64_t;
typedef uint32_t  efi_uintn;
typedef int8_t    efi_int8_t;
typedef int16_t   efi_int16_t;
typedef int32_t   efi_int32_t;
typedef int64_t   efi_int64_t;
typedef int8_t    efi_char8_t;
typedef int16_t   efi_char16_t;
typedef int32_t   efi_char32_t;
typedef int64_t   efi_char64_t;
typedef uint32_t  efi_status_t;
typedef char      efi_bool_t;
typedef void      VOID;
typedef VOID *    efi_handle_t;
typedef uint64_t  efi_ptr64_t;
typedef int       EFI_STATUS;
typedef uint64_t  efi_lba_t;
typedef uint32_t  efi_physical_addr_t;

#define _cdecl    EFIAPI

/* Modifiers for EFI Runtime and Boot Services. */
#define IN
#define OUT
#define OPTIONAL

/* Set the upper bit to indicate EFI Error. */
#define EFI_MAX_BIT               0x80000000
#define EFIERR(a)                 (EFI_MAX_BIT | (a))
#define EFIWARN(a)                (a)

/* error codes. */
#define EFI_SUCCESS               0
#define EFI_LOAD_ERROR            EFIERR (1)
#define EFI_INVALID_PARAMETER     EFIERR (2)
#define EFI_UNSUPPORTED           EFIERR (3)
#define EFI_BAD_BUFFER_SIZE       EFIERR (4)
#define EFI_BUFFER_TOO_SMALL      EFIERR (5)
#define EFI_NOT_READY             EFIERR (6)
#define EFI_DEVICE_ERROR          EFIERR (7)
#define EFI_WRITE_PROTECTED       EFIERR (8)
#define EFI_OUT_OF_RESOURCES      EFIERR (9)
#define EFI_VOLUME_CORRUPTED      EFIERR (10)
#define EFI_VOLUME_FULL           EFIERR (11)
#define EFI_NO_MEDIA              EFIERR (12)
#define EFI_MEDIA_CHANGED         EFIERR (13)
#define EFI_NOT_FOUND             EFIERR (14)
#define EFI_ACCESS_DENIED         EFIERR (15)
#define EFI_NO_RESPONSE           EFIERR (16)
#define EFI_NO_MAPPING            EFIERR (17)
#define EFI_TIME_OUT              EFIERR (18)
#define EFI_NOT_STARTED           EFIERR (19)
#define EFI_ALREADY_STARTED       EFIERR (20)
#define EFI_ABORTED               EFIERR (21)
#define EFI_ICMP_ERROR            EFIERR (22)
#define EFI_TFTP_ERROR            EFIERR (23)
#define EFI_PROTOCOL_ERROR        EFIERR (24)
#define EFI_INCOMPATIBLE_VERSION  EFIERR (25)
#define EFI_SECURITY_VIOLATION    EFIERR (26)
#define EFI_CRC_ERROR             EFIERR (27)

/* warning codes. */
#define EFI_WARN_UNKNOWN_GLYPH    EFIWARN (1)
#define EFI_WARN_DELETE_FAILURE   EFIWARN (2)
#define EFI_WARN_WRITE_FAILURE    EFIWARN (3)
#define EFI_WARN_BUFFER_TOO_SMALL EFIWARN (4)

/* EFI Specification Revision information. */
#define EFI_SPECIFICATION_MAJOR_REVISION  1
#define EFI_SPECIFICATION_MINOR_REVISION  10

/* Memory types: */
#define EFI_RESERVED_TYPE                0
#define EFI_LOADER_CODE                  1
#define EFI_LOADER_DATA                  2
#define EFI_BOOT_SERVICES_CODE           3
#define EFI_BOOT_SERVICES_DATA           4
#define EFI_RUNTIME_SERVICES_CODE        5
#define EFI_RUNTIME_SERVICES_DATA        6
#define EFI_CONVENTIONAL_MEMORY          7
#define EFI_UNUSABLE_MEMORY              8
#define EFI_ACPI_RECLAIM_MEMORY          9
#define EFI_ACPI_MEMORY_NVS             10
#define EFI_MEMORY_MAPPED_IO            11
#define EFI_MEMORY_MAPPED_IO_PORT_SPACE 12
#define EFI_PAL_CODE                    13
#define EFI_MAX_MEMORY_TYPE             14

/* Attribute values: */
#define EFI_MEMORY_UC                  ((u64)0x0000000000000001ULL)    /* uncached */
#define EFI_MEMORY_WC                  ((u64)0x0000000000000002ULL)    /* write-coalescing */
#define EFI_MEMORY_WT                  ((u64)0x0000000000000004ULL)    /* write-through */
#define EFI_MEMORY_WB                  ((u64)0x0000000000000008ULL)    /* write-back */
#define EFI_MEMORY_WP                  ((u64)0x0000000000001000ULL)    /* write-protect */
#define EFI_MEMORY_RP                  ((u64)0x0000000000002000ULL)    /* read-protect */
#define EFI_MEMORY_XP                  ((u64)0x0000000000004000ULL)    /* execute-protect */
#define EFI_MEMORY_RUNTIME             ((u64)0x8000000000000000ULL)    /* range requires runtime mapping */
#define EFI_MEMORY_DESCRIPTOR_VERSION  1

/* pages. */
#define EFI_PAGE_SHIFT                  12
#define EFI_PAGE_SIZE                   (1UL << EFI_PAGE_SHIFT)

/* allocation types. */
#define EFI_ALLOCATE_ANY_PAGES          0
#define EFI_ALLOCATE_MAX_ADDRESS        1
#define EFI_ALLOCATE_ADDRESS            2
#define EFI_MAX_ALLOCATE_TYPE           3

/* defines for time services. */
#define EFITIME_ADJUST_DAYLIGHT         1
#define EFITIME_IN_DAYLIGHT             2
#define EFI_UNSPECIFIED_TIMEZONE        0x07ff

/* EFI ResetSystem defines */
#define EFI_RESET_COLD 0
#define EFI_RESET_WARM 1
#define EFI_RESET_SHUTDOWN 2

/* EFI runtime services table */
#define EFI_RUNTIME_SERVICES_SIGNATURE ((uint64_t)0x5652453544e5552ULL)
#define EFI_RUNTIME_SERVICES_REVISION  0x00010000

/* EFI PCI IO attibutes. */
#define EFI_PCI_IO_ATTRIBUTE_ISA_MOTHERBOARD_IO   0x0001
#define EFI_PCI_IO_ATTRIBUTE_ISA_IO               0x0002
#define EFI_PCI_IO_ATTRIBUTE_VGA_PALETTE_IO       0x0004
#define EFI_PCI_IO_ATTRIBUTE_VGA_MEMORY           0x0008
#define EFI_PCI_IO_ATTRIBUTE_VGA_IO               0x0010
#define EFI_PCI_IO_ATTRIBUTE_IDE_PRIMARY_IO       0x0020
#define EFI_PCI_IO_ATTRIBUTE_IDE_SECONDARY_IO     0x0040
#define EFI_PCI_IO_ATTRIBUTE_MEMORY_WRITE_COMBINE 0x0080
#define EFI_PCI_IO_ATTRIBUTE_IO                   0x0100
#define EFI_PCI_IO_ATTRIBUTE_MEMORY               0x0200
#define EFI_PCI_IO_ATTRIBUTE_BUS_MASTER           0x0400
#define EFI_PCI_IO_ATTRIBUTE_MEMORY_CACHED        0x0800
#define EFI_PCI_IO_ATTRIBUTE_MEMORY_DISABLE       0x1000
#define EFI_PCI_IO_ATTRIBUTE_EMBEDDED_DEVICE      0x2000
#define EFI_PCI_IO_ATTRIBUTE_EMBEDDED_ROM         0x4000
#define EFI_PCI_IO_ATTRIBUTE_DUAL_ADDRESS_CYCLE   0x8000
#define EFI_PCI_IO_ATTRIBUTE_ISA_IO_16            0x10000
#define EFI_PCI_IO_ATTRIBUTE_VGA_PALETTE_IO_16    0x20000
#define EFI_PCI_IO_ATTRIBUTE_VGA_IO_16            0x40000

/* EFI system table. */
#define EFI_SYSTEM_TABLE_SIGNATURE                ((u64)0x5453595320494249ULL)
#define EFI_2_30_SYSTEM_TABLE_REVISION            ((2 << 16) | (30))
#define EFI_2_20_SYSTEM_TABLE_REVISION            ((2 << 16) | (20))
#define EFI_2_10_SYSTEM_TABLE_REVISION            ((2 << 16) | (10))
#define EFI_2_00_SYSTEM_TABLE_REVISION            ((2 << 16) | (00))
#define EFI_1_10_SYSTEM_TABLE_REVISION            ((1 << 16) | (10))
#define EFI_1_02_SYSTEM_TABLE_REVISION            ((1 << 16) | (02))

/* EFI Configuration Table and GUID definitions. */
#define NULL_GUID \
    EFI_GUID( 0x00000000, 0x0000, 0x0000, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 )
#define MPS_TABLE_GUID    \
    EFI_GUID( 0xeb9d2d2f, 0x2d88, 0x11d3, 0x9a, 0x16, 0x0, 0x90, 0x27, 0x3f, 0xc1, 0x4d )
#define ACPI_TABLE_GUID    \
    EFI_GUID( 0xeb9d2d30, 0x2d88, 0x11d3, 0x9a, 0x16, 0x0, 0x90, 0x27, 0x3f, 0xc1, 0x4d )
#define ACPI_20_TABLE_GUID    \
    EFI_GUID( 0x8868e871, 0xe4f1, 0x11d3, 0xbc, 0x22, 0x0, 0x80, 0xc7, 0x3c, 0x88, 0x81 )
#define SMBIOS_TABLE_GUID    \
    EFI_GUID( 0xeb9d2d31, 0x2d88, 0x11d3, 0x9a, 0x16, 0x0, 0x90, 0x27, 0x3f, 0xc1, 0x4d )
#define SAL_SYSTEM_TABLE_GUID    \
    EFI_GUID( 0xeb9d2d32, 0x2d88, 0x11d3, 0x9a, 0x16, 0x0, 0x90, 0x27, 0x3f, 0xc1, 0x4d )
#define HCDP_TABLE_GUID \
    EFI_GUID( 0xf951938d, 0x620b, 0x42ef, 0x82, 0x79, 0xa8, 0x4b, 0x79, 0x61, 0x78, 0x98 )
#define EFI_GLOBAL_VARIABLE_GUID \
    EFI_GUID( 0x8be4df61, 0x93ca, 0x11d2, 0xaa, 0x0d, 0x00, 0xe0, 0x98, 0x03, 0x2b, 0x8c )
#define UV_SYSTEM_TABLE_GUID \
    EFI_GUID( 0x3b13a7d4, 0x633e, 0x11dd, 0x93, 0xec, 0xda, 0x25, 0x56, 0xd8, 0x95, 0x93 )

#define EFI_BLOCK_IO_INTERFACE_REVISION 0x00010000

#define SERIAL_IO_PROTOCOL \
  {0xbb25cf6f, 0xf1d4, 0x11d2, \
	{ 0x9a, 0x0c, 0x00, 0x90, 0x27, 0x3f, 0xc1, 0xfd } \
  }

#define UGA_IO_PROTOCOL \
  {0x61a4d49e, 0x6f68, 0x4f1b, \
	{ 0xb9, 0x22, 0xa8, 0x6e, 0xed, 0xb,  0x7, 0xa2 } \
  }

#define UGA_DRAW_PROTOCOL \
  {0x982c298b, 0xf4fa, 0x41cb, \
	{ 0xb8, 0x38, 0x77, 0xaa, 0x68, 0x8f, 0xb8, 0x39 } \
  }

#define GOP_PRTOCOL \
  {0x9042a9de, 0x23dc, 0x4a38, \
	{ 0x96, 0xfb, 0x7a, 0xde, 0xd0, 0x80, 0x51, 0x6a } \
  }

#define DISK_IO_PROTOCOL	\
  { 0xce345171, 0xba0b, 0x11d2, \
    { 0x8e, 0x4f, 0x00, 0xa0, 0xc9, 0x69, 0x72, 0x3b } \
  }

#define BLOCK_IO_PROTOCOL	\
  { 0x964e5b21, 0x6459, 0x11d2, \
    { 0x8e, 0x39, 0x00, 0xa0, 0xc9, 0x69, 0x72, 0x3b } \
  }

#define DEVICE_PATH_PROTOCOL	\
  { 0x09576e91, 0x6d3f, 0x11d2, \
    { 0x8e, 0x39, 0x00, 0xa0, 0xc9, 0x69, 0x72, 0x3b } \
  }

/* EFI file modes. */
#define EFI_FILE_MODE_READ      0x0000000000000001
#define EFI_FILE_MODE_WRITE     0x0000000000000002
#define EFI_FILE_MODE_CREATE    0x8000000000000000

/* EFI variable attributes. */
#define EFI_VARIABLE_NON_VOLATILE       0x0000000000000001
#define EFI_VARIABLE_BOOTSERVICE_ACCESS 0x0000000000000002
#define EFI_VARIABLE_RUNTIME_ACCESS     0x0000000000000004
#define EFI_VARIABLE_HARDWARE_ERROR_RECORD 0x0000000000000008
#define EFI_VARIABLE_AUTHENTICATED_WRITE_ACCESS 0x0000000000000010
#define EFI_VARIABLE_TIME_BASED_AUTHENTICATED_WRITE_ACCESS 0x0000000000000020
#define EFI_VARIABLE_APPEND_WRITE       0x0000000000000040

/* EFI locate handle properties. */
#define EFI_LOCATE_ALL_HANDLES                  0
#define EFI_LOCATE_BY_REGISTER_NOTIFY           1
#define EFI_LOCATE_BY_PROTOCOL                  2

/* EFI device path information. */
#define EFI_DEV_HW                      0x01
#define  EFI_DEV_PCI                             1
#define  EFI_DEV_PCCARD                          2
#define  EFI_DEV_MEM_MAPPED                      3
#define  EFI_DEV_VENDOR                          4
#define  EFI_DEV_CONTROLLER                      5
#define EFI_DEV_ACPI                    0x02
#define   EFI_DEV_BASIC_ACPI                     1
#define   EFI_DEV_EXPANDED_ACPI                  2
#define EFI_DEV_MSG                     0x03
#define   EFI_DEV_MSG_ATAPI                      1
#define   EFI_DEV_MSG_SCSI                       2
#define   EFI_DEV_MSG_FC                         3
#define   EFI_DEV_MSG_1394                       4
#define   EFI_DEV_MSG_USB                        5
#define   EFI_DEV_MSG_USB_CLASS                 15
#define   EFI_DEV_MSG_I20                        6
#define   EFI_DEV_MSG_MAC                       11
#define   EFI_DEV_MSG_IPV4                      12
#define   EFI_DEV_MSG_IPV6                      13
#define   EFI_DEV_MSG_INFINIBAND                 9
#define   EFI_DEV_MSG_UART                      14
#define   EFI_DEV_MSG_VENDOR                    10
#define EFI_DEV_MEDIA                   0x04
#define   EFI_DEV_MEDIA_HARD_DRIVE               1
#define   EFI_DEV_MEDIA_CDROM                    2
#define   EFI_DEV_MEDIA_VENDOR                   3
#define   EFI_DEV_MEDIA_FILE                     4
#define   EFI_DEV_MEDIA_PROTOCOL                 5
#define EFI_DEV_BIOS_BOOT               0x05
#define EFI_DEV_END_PATH                0x7F
#define EFI_DEV_END_PATH2               0xFF
#define   EFI_DEV_END_INSTANCE                  0x01
#define   EFI_DEV_END_ENTIRE                    0xFF

/* open protocol. */
#define EFI_OPEN_PROTOCOL_BY_HANDLE_PROTOCOL  0x00000001
#define EFI_OPEN_PROTOCOL_GET_PROTOCOL        0x00000002
#define EFI_OPEN_PROTOCOL_TEST_PROTOCOL       0x00000004
#define EFI_OPEN_PROTOCOL_BY_CHILD_CONTROLLER 0x00000008
#define EFI_OPEN_PROTOCOL_BY_DRIVER           0x00000010
#define EFI_OPEN_PROTOCOL_EXCLUSIVE           0x00000020

typedef struct efiGUID {
	uint8_t b[16];
}efiGUID;

typedef struct efiIPv4 {
	uint8_t addr[4];
}efiIPv4;

typedef struct efiIPv6 {
	uint8_t addr[16];
}efiIPv6;

typedef struct efiMACAddress {
	uint8_t addr[32];
}efiMACAddress;

typedef struct efiTableHeader {
	uint64_t signature;
	uint32_t revision;
	uint32_t headerSize;
	uint32_t crc32;
	uint32_t reserved;
}efiTableHeader;

typedef struct efiMemoryDesc {
        uint32_t type;
        uint64_t physAddr;
        uint64_t virtAddr;
        uint64_t numPages;
        uint64_t attribute;
} efiMemoryDesc;

typedef struct efiTime {
        uint16_t year;
        uint8_t  month;
		uint8_t  day;
        uint8_t  hour;
        uint8_t  minute;
		uint8_t  second;
        uint8_t  pad1;
        uint32_t nanosecond;
        int16_t  timezone;
        uint8_t  daylight;
        uint8_t  pad2;
} efiTime;

typedef struct efiTimeCap {
	uint32_t resolition;
	uint32_t accuracy;
	uint8_t  setsToZero;
} efiTimeCap;

typedef struct efiRuntimeServices {
	efiTableHeader hdr;
	void*          getTime;
	void*          setTime;
	void*          getWakeupTime;
	void*          setWakeupTime;
	void*          setVirtualAddressMap;
	void*          convertPointer;
	void*          getVariable;
	void*          getNextVariable;
	void*          getNextHighMonoCount;
	void*          resetSystem;
}efiRuntimeServices;

typedef struct efiDevicePath {
	uint8_t        type;
	uint8_t        subType;
	uint8_t        length[2];
}efiDevicePath;

typedef struct efiPciDevicePath {
	efiDevicePath  header;
	uint8_t        function;
	uint8_t        device;
}efiPciDevicePath;

typedef struct efiPccardDevicePath {
	efiDevicePath  header;
	uint8_t        functionNumber;
}efiPccardDevicePath;

typedef struct efiMemmapDevicePath {
	efiDevicePath  header;
	uint32_t       memoryType;
	efi_physical_addr_t startingAddress;
	efi_physical_addr_t endingAddress;
}efiMemmapDevicePath;

typedef struct efiVendorDevicePath {
	efiDevicePath  header;
	nblGUID        guid;  /* compatible with EFI. */
}efiVendorDevicePath;

typedef struct efiControllerDevicePath {
	efiDevicePath  header;
	uint32_t       controllerNumber;
}efiControllerDevicePath;

typedef struct efiAcpiHidDevicePath {
	efiDevicePath  header;
	uint32_t       hid;
	uint32_t       uid;
}efiAcpiHidDevicePath;

typedef struct efiAcpiExtendedHidDevicePath {
	efiDevicePath  header;
	uint32_t       hid;
	uint32_t       uid;
	uint32_t       cid;
}efiAcpiExtendedHidDevicePath;

typedef struct efiAdrDevicePath {
	efiDevicePath  header;
	uint32_t       adr;
}efiAdrDevicePath;

typedef struct efiAtapiDevicePath {
	efiDevicePath  header;
	uint8_t        primarySecondary;
	uint8_t        slaveMaster;
	uint16_t       lun;
}efiAtapiDevicePath;

typedef struct efiScsiDevicePath {
	efiDevicePath  header;
	uint16_t       pun;
	uint16_t       lun;
}efiScsiDevicePath;

typedef struct efiFiberChannelDevicePath {
	efiDevicePath  header;
	uint32_t       reserved;
	uint64_t       wwn;
	uint64_t       lun;
}efiFiberChannelDevicePath;

typedef struct efiF1394DevicePath {
	efiDevicePath  header;
	uint32_t       reserved;
	uint64_t       guid;
}efiF1394DevicePath;

typedef struct efiUsbDevicePath {
	efiDevicePath  header;
	uint8_t        parentPortNumber;
	uint8_t        interfaceNumber;
}efiUsbDevicePath;

typedef struct efiClassDevicePath {
	efiDevicePath  header;
	uint16_t       vendorId;
	uint16_t       productId;
	uint8_t        deviceClass;
	uint8_t        deviceSubClass;
	uint8_t        deviceProtocol;
}efiClassDevicePath;

typedef struct efiWwidDevicePath {
	efiDevicePath  header;
	uint16_t       interfaceNumber;
	uint16_t       vendorId;
	uint16_t       productId;
}efiWwidDevicePath;

typedef struct efiLogicalUnitDevicePath {
	efiDevicePath  header;
	uint8_t        lun;
}efiLogicalUnitDevicePath;

typedef struct efiSataDevicePath {
	efiDevicePath  header;
	uint16_t       hbaPortNumber;
	uint16_t       portMultiplierPortNumber;
	uint16_t       lun;
}efiSataDevicePath;

typedef struct efiI2ODevicePath {
	efiDevicePath  header;
	uint32_t       tid;
}efiI2ODevicePath;

typedef struct efiMACAddrDevicePath {
	efiDevicePath  header;
	efiMACAddress  macAddress;
	uint8_t        ifType;
}efiMACAddrDevicePath;

typedef struct efiIPv4DevicePath {
	efiDevicePath  header;
	efiIPv4        localIpAddress;
	efiIPv4        remoteIpAddress;
	uint16_t       localPort;
	uint16_t       remotePort;
	uint16_t       protocol;
	efi_bool_t     staticIpAddress;
}efiIPv4DevicePath;

typedef struct efiIPv6DevicePath {
	efiDevicePath  header;
	efiIPv6        localIpAddress;
	efiIPv6        remoteIpAddress;
	uint16_t       localPort;
	uint16_t       remotePort;
	uint16_t       protocol;
	efi_bool_t     staticIpAddress;
}efiIPv6DevicePath;

typedef struct efiInfiniBandDevicePath {
	efiDevicePath  header;
	uint32_t       resourceFlags;
	uint8_t        portGid[16];
	uint64_t       serviceId;
	uint64_t       targetPortId;
	uint64_t       deviceId;
}efiInfiniBandDevicePath;

typedef struct efiUrtDevicePath {
	efiDevicePath  header;
	uint32_t       reserved;
	uint64_t       baudRate;
	uint8_t        dataBits;
	uint8_t        parity;
	uint8_t        stopBits;
}efiUrtDevicePath;

typedef struct efiUrtFlowControlDevicePath {
	efiDevicePath  header;
	nblGUID        guid;
	uint32_t       flowControlMap;
}efiUrtFlowControlDevicePath;

typedef struct efiSasDevicePath {
	efiDevicePath  header;
	nblGUID        guid;
	uint32_t       reserved;
	uint64_t       sasAddress;
	uint64_t       lun;
	uint16_t       deviceTopology;
	uint16_t       relativeTargetPort;
}efiSasDevicePath;

typedef struct efiIScsiDevicePath {
	efiDevicePath  header;
	uint16_t       networkProtocol;
	uint16_t       loginOption;
	uint64_t       lun;
	uint16_t       targetPortalGroupTag;
	// char        iSCSI target name
}efiIScsiDevicePath;

typedef struct efiVLanDevicePath {
	efiDevicePath  header;
	uint16_t       vlanId;
}efiVLanDevicePath;

typedef struct efiHardDiskDevicePath {
	efiDevicePath  header;
	uint32_t       partitionNumber;
	uint64_t       partitionStart;
	uint64_t       partitionSize;
	uint8_t        signature[16];
	uint8_t        mbrType;
	uint8_t        signatureType;
}efiHardDiskDevicePath;

typedef struct efiCdRomDevicePath {
	efiDevicePath  header;
	uint32_t       bootEntry;
	uint64_t       partitionStart;
	uint64_t       partitionEnd;
}efiCdRomDevicePath;

typedef struct efiFilePathDevicePath {
	efiDevicePath  header;
	wchar_t        pathName[1];
}efiFilePathDevicePath;

typedef struct efiMediaProtocolDevicePath {
	efiDevicePath  header;
	nblGUID        protocol;
}efiMediaProtocolDevicePath;

typedef struct efiFwVolFilePathDevicePath {
	efiDevicePath  header;
	nblGUID        fvFileName;
}efiFwVolFilePathDevicePath;

typedef struct efiFwVolDevicePath {
	efiDevicePath  header;
	nblGUID        fvName;
}efiFwVolDevicePath;

typedef struct efiMediaRelativeOffsetRangeDevicePath {
	efiDevicePath  header;
	uint32_t       reserved;
	uint64_t       startingOffset;
	uint64_t       endingOffset;
}efiMediaRelativeOffsetRangeDevicePath;

/* Booting non-EFI operating systems. */
typedef struct efiBbsDevicePath {
	efiDevicePath  header;
	uint16_t       deviceType;
	uint16_t       statusFlag;
	char           string[1];
}efiBbsDevicePath;

typedef union efiDevicePaths {
	efiDevicePath                         header;
	efiPciDevicePath                      pci;
	efiPccardDevicePath                   pccard;
	efiMemmapDevicePath                   memmap;
	efiVendorDevicePath                   vendor;
	efiControllerDevicePath               controller;
	efiAcpiHidDevicePath                  acpiHid;
	efiAcpiExtendedHidDevicePath          acpiExtendedHid;
	efiAdrDevicePath                      adr;
	efiAtapiDevicePath                    atapi;
	efiScsiDevicePath                     scsi;
	efiFiberChannelDevicePath             fiberChannel;
	efiF1394DevicePath                    f1394;
	efiUsbDevicePath                      usb;
	efiClassDevicePath                    class;
	efiWwidDevicePath                     wwid;
	efiLogicalUnitDevicePath              logicalUnit;
	efiSataDevicePath                     sata;
	efiI2ODevicePath                      i2o;
	efiMACAddrDevicePath                  mac;
	efiIPv4DevicePath                     ipv4;
	efiIPv6DevicePath                     ipv6;
	efiInfiniBandDevicePath               infiniBand;
	efiUrtDevicePath                      urt;
	efiUrtFlowControlDevicePath           urtFlowControl;
	efiSasDevicePath                      sas;
	efiIScsiDevicePath                    iScsi;
	efiVLanDevicePath                     vlan;
	efiHardDiskDevicePath                 hardDisk;
	efiCdRomDevicePath                    cdRom;
	efiFilePathDevicePath                 filePath;
	efiMediaProtocolDevicePath            mediaProtocol;
	efiFwVolFilePathDevicePath            fwVolFilePath;
	efiFwVolDevicePath                    fwVol;
	efiMediaRelativeOffsetRangeDevicePath mediaRelativeOffsetRange;
	efiBbsDevicePath                      bbs;
}efiDevicePaths;

typedef union efiDevicePathsPtr {
	efiDevicePath                         *header;
	efiPciDevicePath                      *pci;
	efiPccardDevicePath                   *pccard;
	efiMemmapDevicePath                   *memmap;
	efiVendorDevicePath                   *vendor;
	efiControllerDevicePath               *controller;
	efiAcpiHidDevicePath                  *acpiHid;
	efiAcpiExtendedHidDevicePath          *acpiExtendedHid;
	efiAdrDevicePath                      *adr;
	efiAtapiDevicePath                    *atapi;
	efiScsiDevicePath                     *scsi;
	efiFiberChannelDevicePath             *fiberChannel;
	efiF1394DevicePath                    *f1394;
	efiUsbDevicePath                      *usb;
	efiClassDevicePath                    *class;
	efiWwidDevicePath                     *wwid;
	efiLogicalUnitDevicePath              *logicalUnit;
	efiSataDevicePath                     *sata;
	efiI2ODevicePath                      *i2o;
	efiMACAddrDevicePath                  *mac;
	efiIPv4DevicePath                     *ipv4;
	efiIPv6DevicePath                     *ipv6;
	efiInfiniBandDevicePath               *infiniBand;
	efiUrtDevicePath                      *urt;
	efiUrtFlowControlDevicePath           *urtFlowControl;
	efiSasDevicePath                      *sas;
	efiIScsiDevicePath                    *iScsi;
	efiVLanDevicePath                     *vlan;
	efiHardDiskDevicePath                 *hardDisk;
	efiCdRomDevicePath                    *cdRom;
	efiFilePathDevicePath                 *filePath;
	efiMediaProtocolDevicePath            *mediaProtocol;
	efiFwVolFilePathDevicePath            *fwVolFilePath;
	efiFwVolDevicePath                    *fwVol;
	efiMediaRelativeOffsetRangeDevicePath *mediaRelativeOffsetRange;
	efiBbsDevicePath                      *bbs;
}efiDevicePathsPtr;

/*** protocols ***/

typedef struct efiBlockIoMedia {
	uint32_t         mediaId;
	efi_bool_t       removableMedia;
	efi_bool_t       mediaPresent;
	efi_bool_t       logicalPartition;
	efi_bool_t       readOnly;
	efi_bool_t       writeCaching;
	uint32_t         blockSize;
	uint32_t         ioAlign;
	efi_lba_t        lastBlock;
}efiBlockIoMedia;

typedef struct efiBlockIo {
	uint64_t         revision;
	efiBlockIoMedia* media;
	void*            reset;
	void*            readBlocks;
	void*            writeBlocks;
	void*            flushBlocks;
}efiBlockIo;

typedef enum efiParityType {
	DefaultParity,
	NoParity,
	EvenParity,
	OddParity,
	MarkParity,
	SpaceParity
}efiParityType;

typedef enum efiStopBitsType {
	DefaultStopBits,
	OneStopBit,
	OneFiveStopBits, // 1.5 stop bits.
	TwoStopBits
}efiStopBitsType;

typedef struct efiSerialIoMode {
	uint32_t         controlMask;
	uint32_t         timeout;
	uint64_t         baudRate;
	uint32_t         receiveFifoDepth;
	uint32_t         dataBits;
	uint32_t         parity;
	uint32_t         stopBits;
}efiSerialIoMode;

typedef struct efiSerialIo {
	uint32_t         revision;
	void*            reset;
	void*            setAttributes;
	void*            setControl;
	void*            getControl;
	void*            write;
	void*            read;
	efiSerialIoMode* mode;
}efiSerialIo;

typedef struct efiDiskIo {
	uint64_t         revision;
	void*            diskRead;
	void*            diskWrite;
}efiDiskIo;

typedef enum {
  PixelRedGreenBlueReserved8BitPerColor,
  PixelBlueGreenRedReserved8BitPerColor,
  PixelBitMask,
  PixelBltOnly,
  PixelFormatMax
}efiGopPixelFormat;

typedef struct efiPixelBitMask {
	uint32_t redMask;
	uint32_t greenMask;
	uint32_t blueMask;
	uint32_t reservedMask;
}efiPixelBitMask;

typedef struct efiGopModeInfo {
	uint32_t          version;
	uint32_t          horzResolution;
	uint32_t          vertResolution;
	efiGopPixelFormat pixelFormat;
	efiPixelBitMask   pixelInformation;
	uint32_t          pixelsPerScanLine;
}efiGopModeInfo;

typedef struct efiGopMode {
	uint32_t            maxMode;
	uint32_t            mode;
	efiGopModeInfo*     info;
	efi_uintn           sizeOfInfo;
	efi_physical_addr_t fbBase;
	efi_uintn           fbSize;
}efiGopMode;

typedef struct efiGop {
	void*            queryMode;
	void*            setMode;
	void*            blt;
	efiGopMode*      mode;
}efiGop;

typedef struct efiUgaPixel {
	uint8_t          blue;
	uint8_t          green;
	uint8_t          red;
	uint8_t          reserved;
}efiUgaPixel;

typedef enum efiUgaBltOp {
	EfiUgaVideoFill,
	EfiUgaVideoToBltBuffer,
	EfiUgaBltBufferToVideo,
	EfiUgaVideoToVideo,
	EfiUgaBltMax 
}efiUgaBltOp;

typedef struct efiUgaDraw {
	void*            getMode;
	void*            setMode;
	void*            blt;
}efiUgaDraw;

typedef struct efiUgaIo {
	void*            createDevice;
	void*            deleteDevice;
	void*            dispatchService;
}efiUgaIo;

typedef struct efiBootServices {
	efiTableHeader hdr;
    void*          raiseTpl;
    void*          restoreTpl;
    void*          allocatePages;
    void*          freePages;
    void*          getMemoryMap;
    void*          allocatePool;
    void*          freePool;
	void*          createEvent;
	void*          setTimer;
	void*          waitForEvent;
	void*          signalEvent;
	void*          closeEvent;
	void*          checkEvent;
	void*          installProtocolInterface;
	void*          reinstallProtocolInterface;
	void*          uninstallProtocolInterface;
	void*          handleProtocol;
	void*          _reserved;
	void*          registerProtocolNotify;
	void*          locateHandle;
	void*          locateDevicePath;
	void*          installConfigurationTable;
	void*          loadImage;
	void*          startImage;
	void*          exit;
	void*          unloadImage;
	void*          exitBootServices;
	void*          getNextMonotonicCount;
	void*          stall;
	void*          setWatchdogTimer;
	void*          connectController;
	void*          disconnectController;
	void*          openProtocol;
	void*          closeProtocol;
	void*          openProtocolInformation;
	void*          protocolsPerHandle;
	void*          locateHandleBuffer;
	void*          locateProtocol;
	void*          installMultipleProtocolInterfaces;
	void*          uninstallMultipleProtocolInterfaces;
	void*          calculateCrc32;
	void*          copyMem;
	void*          setMem;
	void*          createEventEx;
}efiBootServices;

typedef struct efiInputKey {
	uint16_t       scanCode;
	char16_t       unicodeChar;
} efiInputKey;

typedef struct efiSimpleTextInputProtocol {
	void*          reset;
	void*          readKeyStroke;
	void*          waitForKey; /* EFI_EVENT to use with WaitForEvent. */
} efiSimpleTextInputProtocol;

typedef struct efiSimpleTextOutputMode {
	efi_int32_t    maxMode;
	efi_int32_t    mode;
	efi_int32_t    attributes;
	efi_int32_t    cursorCol;
	efi_int32_t    cursorRow;
	efi_bool_t     cursorVisable;
}efiSimpleTextOutputMode;

typedef struct efiSimpleTextOutputProtocol {
	void*          reset;
	void*          outputString;
	void*          testString;
	void*          queryMode;
	void*          setMode;
	void*          setAttribute;
	void*          clearScreen;
	void*          setCursorPosition;
	void*          enableCursor;
	efiSimpleTextOutputMode* mode;
} efiSimpleTextOutputProtocol;

typedef struct _efiSystemTable {
	efiTableHeader               hdr;
	unsigned long                fwVender;
	uint32_t                     fwRevision;
	unsigned long                conInHandle;
	efiSimpleTextInputProtocol*  conInProtocol;
	unsigned long                conOutHandle;
	efiSimpleTextOutputProtocol* conOutProtocol;
	unsigned long                stderrHandle;
	unsigned long                stderrProtocol;
	efiRuntimeServices*          runtime;
	efiBootServices*             boottime;
	unsigned long                tableCount;
	unsigned long                tables;
}efiSystemTable;

/* invoke EFI function. EFI_CALL(function,param_count,params) */
#define EFI_CALL(f,n,...) ((int(*)())f) (__VA_ARGS__)

#pragma pack(pop)

#endif
