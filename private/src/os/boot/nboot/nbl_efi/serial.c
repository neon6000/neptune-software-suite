/************************************************************************
*
*	EFI serial driver.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "efi.h"

/* pointer to system table. */
STATIC efiSystemTable* _st = NULL;

/* self reference. */
STATIC nblDriverObject* _self = NULL;

/* image handle. */
STATIC efi_handle_t _image = 0;

/* serial protocol. */
STATIC efiSerialIo* _serial = NULL;

static nblGUID _serialIoGUID     = SERIAL_IO_PROTOCOL;

/* set control bits. */
#define EFI_SERIAL_CLEAR_TO_SEND                0x0010
#define EFI_SERIAL_DATA_SET_READY               0x0020
#define EFI_SERIAL_RING_INDICATE                0x0040
#define EFI_SERIAL_CARRIER_DETECT               0x0080
#define EFI_SERIAL_REQUEST_TO_SEND              0x0002
#define EFI_SERIAL_DATA_TERMINAL_READY          0x0001
#define EFI_SERIAL_INPUT_BUFFER_EMPTY           0x0100
#define EFI_SERIAL_OUTPUT_BUFFER_EMPTY          0x0200
#define EFI_SERIAL_HARDWARE_LOOPBACK_ENABLE     0x1000
#define EFI_SERIAL_SOFTWARE_LOOPBACK_ENABLE     0x2000
#define EFI_SERIAL_HARDWARE_FLOW_CONTROL_ENABLE 0x4000 

/* serial device object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiSerialDriverObject;
STATIC nblDeviceObject _serialDeviceObject = {
	NBL_DEVICE_SERIAL_CONTROLLER,
	NBL_DEVICE_CONTROLLER_CLASS,
	0, &_nEfiSerialDriverObject, 0, 0, "serial", 0
};


/* returns EFI system table. */
STATIC efiSystemTable* EfiSerialGetSystemTable (void) {

	nblDriverObject* fm;
	nblMessage       m;

	/* get EFI system table pointer. */
	if (_st)
		return _st;

	/* open firmware efi driver. */
	fm = BlrOpenDriver ("root/fw");
	if (!fm)
		return NULL;

	/* get EFI boot services. */
	m.type = NBL_FW_EFI_SERVICES;
	if (fm->request(&m) == NBL_STAT_SUCCESS)
		_st = (efiSystemTable*) m.retval;

	/* get image handle. */
	m.type = NBL_FW_EFI_IMAGEHANDLE;
	if (fm->request(&m) == NBL_STAT_SUCCESS)
		_image = (efi_handle_t*) m.retval;

	return _st;
}

/* open protocol. */
STATIC void* EfiSerialLocateProtocol (IN nblGUID* protocol) {

	void*    protocolInterface;
	status_t status;

	status = EFI_CALL (_st->boottime->locateProtocol,3,protocol,NULL,&protocolInterface);

	if (status != EFI_SUCCESS) {
		BlrPrintf("\n\rGOP OPEN CODE: %x", status);
		return NULL;
	}

	return protocolInterface;
}

/* reset device. */
STATIC BOOL EfiSerialReset () {
	if (!_serial)
		return FALSE;
	if (EFI_CALL (_serial->reset, 1, _serial) != EFI_SUCCESS)
		return FALSE;
	return TRUE;
}

/* write data to device. */
STATIC uint32_t EfiSerialWrite (IN uint32_t size, IN void* buffer) {

	efi_uintn s = size;
	BlrPrintf("\n\rEfiSerialWrite size=%i buf=%x _serial=%x", size, buffer,_serial);
	if (!buffer || size == 0)
		return 0;
	EFI_CALL (_serial->write, 3, _serial, &s, buffer);
	return s;
}

/* read data to device. */
STATIC uint32_t EfiSerialRead (IN uint32_t size, IN void* buffer) {

	efi_uintn s = size;
	if (!buffer || size == 0)
		return 0;
	EFI_CALL (_serial->read, 3, _serial, &s, buffer);
	return s;
}

/* driver entry point. */
status_t EfiSerialEntryPoint (IN nblDriverObject* self, IN int dependencyCount,
						 IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	nblMessage rpb;

	_self = self;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	if (!EfiSerialGetSystemTable ())
		return NBL_STAT_DEPENDENCY;

	/* get protocol. */
	_serial = EfiSerialLocateProtocol(&_serialIoGUID);
	if (!_serial)
		return NBL_STAT_UNSUPPORTED;

	/* create serial device. */
	rpb.source = self;
	rpb.type   = NBL_DEV_REGISTER;
	rpb.NBL_DEV_REGISTER_OBJ = &_serialDeviceObject;
	if (dm->request (&rpb) != NBL_STAT_SUCCESS)
		return NBL_STAT_DEPENDENCY;

	return NBL_STAT_SUCCESS;
}

/* driver requests. */
status_t EfiSerialRequest (IN nblMessage* rpb) {
	rettype_t r = 0;
	rpb->retval = 0;
	switch (rpb->type) {
		case NBL_SERIAL_READ:
			r = (rettype_t) EfiSerialRead(rpb->NBL_SERIAL_READ_SIZE,(void*) rpb->NBL_SERIAL_READ_BUFFER);
			break;
		case NBL_SERIAL_WRITE:
			r = (rettype_t) EfiSerialWrite(rpb->NBL_SERIAL_WRITE_SIZE,(void*)rpb->NBL_SERIAL_WRITE_BUFFER);
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_EFI_SERIAL_VERSION 1

/* EFI driver object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiSerialDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_EFI_SERIAL_VERSION,
	NBL_DRV_LOAD_CORE,
	NBL_DRV_CLASS_CHAR,
	"serial",
	EfiSerialEntryPoint,
	EfiSerialRequest,
	1,
	"dm"
};
