/************************************************************************
*
*	NBL EFI Disk Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "efi.h"

#define LOGICAL_SECTOR_SIZE 512

/* pointer to system table. */
STATIC efiSystemTable* _st = 0;

/* self reference. */
STATIC nblDriverObject* _self = 0;

/* image handle. */
STATIC efi_handle_t _image = 0;

static nblGUID _diskIoGUID     = DISK_IO_PROTOCOL;
static nblGUID _blockIoGUID    = BLOCK_IO_PROTOCOL;
static nblGUID _devicePathGUID = DEVICE_PATH_PROTOCOL;


#define EFI_DEVICE_PATH_TYPE(dp)		((dp)->type & 0x7f)
#define EFI_DEVICE_PATH_SUBTYPE(dp)	((dp)->subType)
#define EFI_DEVICE_PATH_LENGTH(dp)		\
  ((dp)->length[0] | ((uint16_t) ((dp)->length[1]) << 8))

/* The End of Device Path nodes.  */
#define EFI_END_DEVICE_PATH_TYPE			(0xff & 0x7f)

#define EFI_END_ENTIRE_DEVICE_PATH_SUBTYPE		0xff
#define EFI_END_THIS_DEVICE_PATH_SUBTYPE		0x01

#define EFI_END_ENTIRE_DEVICE_PATH(dp)	\
  (EFI_DEVICE_PATH_TYPE (dp) == EFI_END_DEVICE_PATH_TYPE \
   && (EFI_DEVICE_PATH_SUBTYPE (dp) \
       == EFI_END_ENTIRE_DEVICE_PATH_SUBTYPE))

#define EFI_NEXT_DEVICE_PATH(dp)	\
  ((efiDevicePath *) ((char *) (dp) \
      + EFI_DEVICE_PATH_LENGTH (dp)))

/* driver specific disk data. */
typedef struct efiDiskDeviceObjectExt {
	efi_handle_t   handle;
	efiDevicePath* devicePath;
	efiDevicePath* lastDevicePath;
	efiBlockIo*    blockIo;
	efiDiskIo*     diskIo;
	struct efiDiskDeviceObjectExt* next;
}efiDiskDeviceObjectExt;

/**
*	EfiDiskNameDevice
*
*	Description : Give a name to a disk device.
*
*	Input : ext - Ignored.
*
*	Return value : Pointer to allocated memory containing next sequence name or NULL.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE char* EfiDiskNameDevice (IN efiDiskDeviceObjectExt* ext) {

	STATIC int i = 0;
	char* name;

	name = BlrAlloc (10);
	if (!name)
		return NULL;

	BlrSPrintf (name, "disk(%i)", i);

	i++;
	return name;
}

STATIC size_t EfiDiskRead (IN nblDeviceObject* disk, IN size_t sector,
						   IN size_t off, IN size_t size, OUT void* buffer);

/**
*	EfiDiskRegisterDevice
*
*	Description : Register disk device.
*
*	Input : ext - Pointer to disk device object extended block.
*
*	Return value : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiDiskRegisterDevice (IN efiDiskDeviceObjectExt* ext) {

	nblDeviceObject* disk;
	nblDriverObject* dm;
	char*            name;
	nblMessage       m;

	int c;
	char buf[32];

	c = 0;
	BlrZeroMemory(buf,32);

	if (!ext)
		return NBL_STAT_INVALID_ARGUMENT;

	dm = BlrOpenDriver ("root/dm");
	if (!dm)
		return NBL_STAT_DRIVER;

	disk = BlrAlloc (sizeof (nblDeviceObject));
	if (!disk)
		return NBL_STAT_MEMORY;

	name = EfiDiskNameDevice (ext);

	disk->type            = NBL_DEVICE_DISK_CONTROLLER;
	disk->devclass        = NBL_DEVICE_CONTROLLER_CLASS;
	disk->characteristics = 0;
	disk->driverObject    = _self;
	disk->attached        = NULL;
	disk->next            = NULL;
	disk->name            = name;
	disk->ext             = ext;

	m.source = _self;
	m.type   = NBL_DEV_REGISTER;
	m.NBL_DEV_REGISTER_OBJ = disk;
	dm->request (&m);

	return NBL_STAT_SUCCESS;
}

/**
*	EfiDiskGetSystemTable
*
*	Description : Get EFI System Table.
*
*	Return value : Pointer to system table or NULL.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE efiSystemTable* EfiDiskGetSystemTable (void) {

	nblDriverObject* fm;
	nblMessage       m;

	/* get EFI system table pointer. */
	if (_st)
		return _st;

	/* open firmware efi driver. */
	fm = BlrOpenDriver ("root/fw");
	if (!fm)
		return NULL;

	/* get EFI boot services. */
	m.type = NBL_FW_EFI_SERVICES;
	if (fm->request(&m) == NBL_STAT_SUCCESS)
		_st = (efiSystemTable*) m.retval;

	/* get image handle. */
	m.type = NBL_FW_EFI_IMAGEHANDLE;
	if (fm->request(&m) == NBL_STAT_SUCCESS)
		_image = (efi_handle_t*) m.retval;

	return _st;
}

/**
*	EfiDiskOpenProtocol
*
*	Description : Open EFI Protocol Interface.
*
*	Input : handle - Handle to EFI image.
*           protocol - Pointer to GUID of protocol.
*           attributes - Requested attributes.
*
*	Return value : Pointer to interface object or NULL.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void* EfiDiskOpenProtocol (IN efi_handle_t handle, IN nblGUID* protocol, IN efi_uint32_t attributes) {

	void*    protocolInterface;
	status_t status;

	status = EFI_CALL (_st->boottime->openProtocol, 6, handle, protocol, &protocolInterface,
		_image, (efi_handle_t) 0, attributes);

	if (status != EFI_SUCCESS) {
		BlrPrintf("\n\rOPEN CODE: %x", status);
		return NULL;
	}

	return protocolInterface;
}

/**
*	EfiDiskLocateHandles
*
*	Description : Returns an array of handles that supports the requested protocol.
*
*	Input : searchType - Search criteria.
*           protocol - Pointer to GUID of protocol.
*           handleCount - OUT Pointer to receive number of handles in returned array.
*
*	Return value : Pointer to interface handles or NULL.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE efi_handle_t* EfiDiskLocateHandles (IN uint32_t searchType, IN nblGUID* protocol, OUT uint32_t* handleCount) {

	uint32_t      size;
	efi_handle_t* buffer;
	efi_status_t  status;

	size = 8 * sizeof (efi_handle_t);
	buffer = BlrAlloc (size);
	if (!buffer)
		return NULL;

	BlrZeroMemory(buffer,size);

	status = EFI_CALL (_st->boottime->locateHandle, 5, searchType, protocol, 0, &size, buffer);
	if (status == EFI_BUFFER_TOO_SMALL) {

		BlrFree (buffer);

		buffer = BlrAlloc (size);
		if (!buffer)
			return NULL;

		BlrZeroMemory(buffer,size);

		status = EFI_CALL (_st->boottime->locateHandle, 5, searchType, protocol, 0, &size, buffer);
	}

	if (status != EFI_SUCCESS) {
		BlrFree (buffer);
		return NULL;
	}

	*handleCount = size / sizeof (efi_handle_t);
	return buffer;
}

struct {
	uint8_t* data;
	uint32_t base;
	uint32_t end;
}CACHE;

/**
*	EfiDiskRead
*
*	Description : Reads data from a disk device.
*
*	Input : disk - Disk device object.
*           sector - Starting sector to read from.
*           off - Starting offset from sector.
*           size - Size in bytes of output buffer.
*           buffer - Pointer to memory to retrieve data.
*
*	Return value : Number of bytes read.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE size_t EfiDiskRead (IN nblDeviceObject* disk, IN size_t sector, IN size_t off, IN size_t size, OUT void* buffer) {

	efi_status_t status;
	uint32_t     base;

	/* make sure we created this device. */
	if (disk->driverObject != _self)
		return 0;

	if (!CACHE.data) {
		CACHE.data = (uint8_t*) BlrAlloc (1024);
		CACHE.base = 0;
		CACHE.end  = 0;
	}

	base = sector * LOGICAL_SECTOR_SIZE + off;

	if (base < CACHE.base || base + size > CACHE.end) {

		efiDiskDeviceObjectExt* ext;
		efiDiskIo*   dio;
		efiBlockIo*  bio;
		uint32_t     media;

		ext   = (efiDiskDeviceObjectExt*) disk->ext;
		dio   = ext->diskIo;
		bio   = ext->blockIo;
		media = bio->media->mediaId;

		status = EFI_CALL (dio->diskRead, 5, dio, media,
			(efi_uint64_t) base,
			(efi_uintn) 1024, CACHE.data);

		CACHE.base = base;
		CACHE.end = CACHE.base + 1024;
	}

	base -= CACHE.base;
	BlrCopyMemory (buffer, &CACHE.data [base], size);

	if (status != NBL_STAT_SUCCESS)
		return 0;

	return size;
}

/**
*	EfiDiskWrite
*
*	Description : Writes data to a disk device.
*
*	Input : disk - Disk device object.
*           sector - Starting sector to read from.
*           off - Starting offset from sector.
*           size - Size in bytes of output buffer.
*           buffer - Pointer to memory to retrieve data.
*
*	Return value : Number of bytes read.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE size_t EfiDiskWrite (IN nblDeviceObject* disk,IN size_t sector, IN size_t off, IN size_t size, OUT void* buffer) {

	efiDiskDeviceObjectExt* ext;
	efiDiskIo*   dio;
	efiBlockIo*  bio;
	uint32_t     media;
	efi_status_t status;

	/* make sure we created this device. */
	if (disk->driverObject != _self)
		return 0;

	ext   = (efiDiskDeviceObjectExt*) disk->ext;
	dio   = ext->diskIo;
	bio   = ext->blockIo;
	media = bio->media->mediaId;

	status = EFI_CALL (dio->diskWrite, 5, dio, media,
		(efi_uint64_t) sector * LOGICAL_SECTOR_SIZE + off,
		(efi_uintn) size, buffer);

	if (status != EFI_SUCCESS)
		return 0;

	return size;
}

/**
*	EfiDiskGetDevicePath
*
*	Description : Returns EFI device path.
*
*	Input : handle - Handle
*
*	Return value : Pointer to EFI device path.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE efiDevicePath* EfiDiskGetDevicePath (IN efi_handle_t handle) {

	return (efiDevicePath*) EfiDiskOpenProtocol(handle,
		&_devicePathGUID,EFI_OPEN_PROTOCOL_GET_PROTOCOL);
}

/**
*	EfiDiskLastDevicePath
*
*	Description : Locate next device path.
*
*	Input : dp - Pointer to current device path.
*
*	Return value : Pointer to next device path.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE efiDevicePath* EfiDiskLastDevicePath (IN efiDevicePath* dp) {

  efiDevicePath *next, *p;

  if (EFI_END_ENTIRE_DEVICE_PATH (dp))
    return 0;

  for (p = (efiDevicePath *) dp, next = EFI_NEXT_DEVICE_PATH (p);
       !EFI_END_ENTIRE_DEVICE_PATH (next);
       p = next, next = EFI_NEXT_DEVICE_PATH (next))
    ;

  return p;
}

/**
*	EfiMakeDevices
*
*	Description : Register disk device objects.
*
*	Return value : NULL (Ignored.)
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE efiDiskDeviceObjectExt* EfiMakeDevices (void) {

	efi_handle_t* handles;
	efi_uint32_t  handleCount;
	efi_handle_t* handle;

	/* locate handles that support disk i/o protocol. */
	handles = EfiDiskLocateHandles (EFI_LOCATE_BY_PROTOCOL, &_diskIoGUID, &handleCount);

	if (! handles)
		return NULL;

	for (handle = handles; handleCount--; handle++) {

		efiBlockIo*             bio;
		efiDiskIo*              dio;
		efiDevicePath*          dp;
		efiDevicePath*          ldp;
		efiDiskDeviceObjectExt* ext;

		/* open disk and block i/o protocols. */
		bio = EfiDiskOpenProtocol (*handle, &_blockIoGUID,EFI_OPEN_PROTOCOL_GET_PROTOCOL);
		dio = EfiDiskOpenProtocol (*handle, &_diskIoGUID, EFI_OPEN_PROTOCOL_GET_PROTOCOL);

		/* skip devices that we don't support. */
		if (!bio || !dio)
			continue;

		/* open device path. */
		dp = EfiDiskGetDevicePath (*handle);
		if (!dp)
			continue;

		ldp = EfiDiskLastDevicePath(dp);
		if (!ldp)
			continue;

		/* allocate disk device. */
		ext = BlrAlloc (sizeof (efiDiskDeviceObjectExt));
		if (!ext) {
			/* this is fatal so bail out. */
			BlrFree (handles);
			return NULL;
		}

		/* initialize device. */
		ext->handle     = *handle;
		ext->blockIo    = bio;
		ext->diskIo     = dio;
		ext->devicePath = dp;

		/* do we need these anymore? */
		ext->lastDevicePath = EfiDiskLastDevicePath(dp);
		ext->next           = NULL;

		/* register device. */
		EfiDiskRegisterDevice (ext);
	}

	/* no longer need buffer. */
	BlrFree (handles);

	return 0;
}

/**
*	EfiEnumerateDevices
*
*	Description : Enumerate disk devices.
*
*	Return value : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiEnumerateDevices (IN nblDriverObject* dm) {

	efiDiskDeviceObjectExt* devices;
	
	devices = EfiMakeDevices ();
	if (!devices)
		return NBL_STAT_DEVICE;

	return NBL_STAT_SUCCESS;
}

/**
*	EfiDiskEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiDiskEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	_self = self;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	/* get efi boot services table and return success. */
	if (! EfiDiskGetSystemTable () )
		return NBL_STAT_DEPENDENCY;

	/* enumerate disk devices. */
	return EfiEnumerateDevices (dm);
}

/**
*	EfiDiskRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiDiskRequest (IN nblMessage* rpb) {
	rettype_t r;
	rpb->retval = 0;
	switch (rpb->type) {
		case NBL_DISK_READ:
			r = EfiDiskRead (rpb->NBL_DISK_READ_DEV,rpb->NBL_DISK_READ_SECTOR,
				rpb->NBL_DISK_READ_OFFSET,rpb->NBL_DISK_READ_SIZE, (void*) rpb->NBL_DISK_READ_BUFFER);
			break;
		case NBL_DISK_WRITE:
			r = EfiDiskWrite (rpb->NBL_DISK_WRITE_DEV,rpb->NBL_DISK_WRITE_SECTOR,
				rpb->NBL_DISK_WRITE_OFFSET,rpb->NBL_DISK_WRITE_SIZE,(void*) rpb->NBL_DISK_WRITE_BUFFER);
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_EFI_DISK_VERSION 1

/* EFI driver object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiDiskDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_EFI_DISK_VERSION,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_OTHER,
	"disk",
	EfiDiskEntryPoint,
	EfiDiskRequest,
	1,
	"dm"
};
