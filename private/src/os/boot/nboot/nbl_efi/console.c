/************************************************************************
*
*	EFI console driver.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "efi.h"

#define BUILD_STD_EFI 1

extern NBL_DRIVER_OBJECT_DEFINE _nStdDriverObject;

extern efiSystemTable*  _st;

/* convert multibyte char string to wide char string. */
STATIC void mbtows (char* mb, wchar_t* wc, size_t c) {
	unsigned int s = BlrStrLength(mb);
	unsigned int i;
	for (i=0;i<s;i++) {
		BlrMbtowc (wc,mb,c);
		wc++;
		mb++;
	}
	*wc=0;
}

/*** Console Input Device. ***/

NBL_DRIVER_OBJECT_DEFINE _nEfiConInDriverObject;

nblFile nbl_in = {
	0,0,0,&_nEfiConInDriverObject,0,0,0,0,0,0,0,0
};

/**
*	EfiInputReset
*
*	Description : Reset input device.
*
*	Input : ExtendedVerification (Ignored.)
*
*	Return value : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
STATIC status_t EfiInputReset (IN BOOL ExtendedVerification) {

	efi_status_t result;

	result = EFI_CALL (_st->conInProtocol->reset,2,_st->conInProtocol, ExtendedVerification);

	if (result != EFI_SUCCESS)
		return NBL_STAT_DEVICE;
	return NBL_STAT_SUCCESS;
}

/**
*	EfiInputReadKeyStroke
*
*	Description : Test if a key has been pressed. This is a blocking call.
*
*	Return value : Scan Code if key was pressed, 0 otherwise.
*
*	Execution Mode : Kernel Mode.
*/
STATIC uint32_t EfiInputReadKeyStroke (void) {

	efi_status_t result;
	efiInputKey  key;

//	while (1) {

		result = EFI_CALL (_st->conInProtocol->readKeyStroke, 2, _st->conInProtocol, &key);

		if (result == EFI_NOT_READY)
			return 0;
		else if (result == EFI_DEVICE_ERROR)
			return 0;
		else {
			if (key.unicodeChar == 0)
				return (uint32_t)key.scanCode;
			else
				return (uint32_t)key.unicodeChar;
		}
//	}
}

/**
*	EfiInputRead
*
*	Description : Reads a block of data from the keyboard.
*
*	Input: file - Input File Stream (must be "conin" device.)
*          size - Size (in bytes) of buffer.
*          buffer - Pointer to buffer to receive data.
*
*	Return value : Number of characters read.
*
*	Execution Mode : Kernel Mode.
*/
STATIC uint32_t EfiInputRead (IN nblFile* file, IN size_t size, OUT char* buffer) {

	efi_status_t result;
	efiInputKey  key;
	uint32_t c;

	/* make sure we are associated with file object. */
	if (file->driver != &_nEfiConInDriverObject)
		return 0;

	c = 0;
	while (size) {

		wchar_t s[2];
		s[1] = 0;

		/* get next key. */
		result = EFI_CALL (_st->conInProtocol->readKeyStroke, 2, _st->conInProtocol, &key);
		if (result == EFI_NOT_READY)
			continue;

		if (result == EFI_DEVICE_ERROR)
			return c;

		/* print it to stdout. */
		s[0] = key.unicodeChar;
		BlrPrints ((char*) s);

		/* break if newline or carriage return. */
		if (key.unicodeChar == 0x000a || key.unicodeChar == 0x000d){
//			buffer [c++] = (char) key.unicodeChar;
			break;
		}

		buffer [c++] = (char) key.unicodeChar;
		--size;
	}

	buffer [c] = 0;
	return c;
}

STATIC nblDeviceObject _conin;

/**
*	EfiInputOpen
*
*	Description : Reads a block of data from the keyboard.
*
*	Input: path (ignored.)
*          dev - Must be the conin device object created by this driver.
*
*	Return value : Character Code returned.
*
*	Execution Mode : Standard In File Pointer or NULL.
*/
PRIVATE nblFile* EfiInputOpen (IN char* path, IN nblDeviceObject* dev) {

	if (dev != &_conin)
		return 0;

	nbl_stdin = &nbl_in;
	return nbl_stdin;
}

/**
*	EfiConInRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiConInRequest (IN nblMessage* rpb) {
	rettype_t r;
	r = 0;
	switch (rpb->type) {
		case NBL_CONIN_RESET:
			r = (rettype_t) EfiInputReset (FALSE);
			break;
		case NBL_CONIN_READKEY:
			r = (rettype_t) EfiInputReadKeyStroke ();
			break;
		case NBL_FS_READ:
			r = (rettype_t) EfiInputRead ((nblFile*) rpb->NBL_FS_READ_FILE,
				rpb->NBL_FS_READ_SIZE, (char*) rpb->NBL_FS_READ_BUFFER);
			break;
		case NBL_FS_OPEN:
			r = (rettype_t) EfiInputOpen ((char*) rpb->NBL_FS_OPEN_PATH,
				(nblDeviceObject*) rpb->NBL_FS_OPEN_DEVICE);
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

/* console output device object. */
STATIC nblDeviceObject _conin = {
	NBL_DEVICE_TERMINAL_PERIPHERAL,
	NBL_DEVICE_PERIPHERAL_CLASS,
	0, &_nEfiConInDriverObject,
	&_nEfiConInDriverObject, 0, "conin", 0
};

/**
*	EfiConInEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiConInEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	nblMessage rpb;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	/* create console device. */
	rpb.source = self;
	rpb.type   = NBL_DEV_REGISTER;
	rpb.NBL_DEV_REGISTER_OBJ = &_conin;
	if (dm->request (&rpb) != NBL_STAT_SUCCESS)
		return NBL_STAT_DEPENDENCY;

	return NBL_STAT_SUCCESS;
}

/* EFI driver object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiConInDriverObject = {
	NBL_DRIVER_MAGIC,
	0,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_CON,
	"conin",
	EfiConInEntryPoint,
	EfiConInRequest,
	1,
	"dm"
};

/*** Console Output Device. ***/

NBL_DRIVER_OBJECT_DEFINE _nEfiConOutDriverObject;

nblFile nbl_out = {
	0,0,0,&_nEfiConOutDriverObject,0,0,0,0,0,0,0,0
};

/**
*	EfiOutputReset
*
*	Description : Resets console output device.
*
*	Input : ExtendedVerification (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiOutputReset (IN BOOL ExtendedVerification) {

	efi_status_t result;

	result = EFI_CALL (_st->conOutProtocol->reset,2,_st->conOutProtocol, ExtendedVerification);

	if (result != EFI_SUCCESS)
		return NBL_STAT_DEVICE;
	return NBL_STAT_SUCCESS;
}

/**
*	EfiOutputString
*
*	Description : Display string to console device.
*
*	Input : str - String to display.
*
*	Output : Status code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiOutputString (IN efi_char16_t* str) {

	efi_status_t result;

	result = EFI_CALL (_st->conOutProtocol->testString, 2, _st->conOutProtocol, str);

	if (result == EFI_SUCCESS)
		result = EFI_CALL (_st->conOutProtocol->outputString, 2, _st->conOutProtocol, str);

	if (result != EFI_SUCCESS)
		return NBL_STAT_DEVICE;
	return NBL_STAT_SUCCESS;
}

/**
*	EfiOutputQueryMode
*
*	Description : Returns the console mode number. The mode number
*   has the following format:
*
*   +------+-------+
*   | cols |  rows |
*   +------+-------+
*   12     8       0
*
*	Input : mode (Ignored.)
*
*	Output : Mode number.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE uint16_t EfiOutputQueryMode (IN efi_uintn mode) {

	efi_status_t result;
	efi_uintn cols;
	efi_uintn rows;

	rows = mode & 0xff;
	cols = mode >> 8;

	if (!rows ||  !cols)
		return NBL_STAT_INVALID_ARGUMENT;

	result = EFI_CALL (_st->conOutProtocol->queryMode, 4, _st->conOutProtocol, mode, cols, rows);

	if (result != EFI_SUCCESS)
		return NBL_STAT_DEVICE;

	return (cols << 8) | rows;
}

/**
*	EfiOutputSetMode
*
*	Description : Returns the console mode number. The mode number
*   has the following format:
*
*   +------+-------+
*   | cols |  rows |
*   +------+-------+
*   12     8       0
*
*	Input : mode - Mode number in the above format.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiOutputSetMode (IN efi_uintn mode) {

	efi_status_t result;
	result = EFI_CALL (_st->conOutProtocol->setMode, 2, _st->conOutProtocol, mode);

	if (result != EFI_SUCCESS)
		return NBL_STAT_DEVICE;
	return NBL_STAT_SUCCESS;
}

/**
*	EfiOutputGetMode
*
*	Description : Returns VGA video mode number.
*
*	Output : Current VGA mode number.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE uint32_t EfiOutputGetMode (void) {

	return _st->conOutProtocol->mode->mode;
}

/**
*	EfiOutputSetAttribute
*
*	Description : Set current text mode attribute byte.
*
*	Input : foreground - Foregound color.
*           background - Background color.
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiOutputSetAttribute (IN efi_uintn foreground, IN efi_uintn background) {

	efi_status_t result;
	efi_uintn col;

	col = (foreground & 0x7f) | ((background & 0x7f) << 4);

	result = EFI_CALL (_st->conOutProtocol->setAttribute, 2, _st->conOutProtocol, col);

	if (result != EFI_SUCCESS)
		return NBL_STAT_DEVICE;
	return NBL_STAT_SUCCESS;
}

/**
*	EfiOutputGetAttributes
*
*	Description : Returns current attribute byte.
*
*	Output : Current attribute byte.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE uint32_t EfiOutputGetAttributes (void) {

	return _st->conOutProtocol->mode->attributes;
}

/**
*	EfiOutputClearScreen
*
*	Description : Clear the console device.
*
*	Input : foreground - Foreground color.
*           background - Background color.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiOutputClearScreen (IN efi_uintn foreground, IN efi_uintn background) {

	efi_status_t result;

	if (EfiOutputSetAttribute (foreground, background) != NBL_STAT_SUCCESS)
		return NBL_STAT_DEVICE;

	result = EFI_CALL (_st->conOutProtocol->clearScreen, 1, _st->conOutProtocol);

	if (result != EFI_SUCCESS)
		return NBL_STAT_DEVICE;
	return NBL_STAT_SUCCESS;
}

/**
*	EfiOutputSetXY
*
*	Description : Set text mode cursor location.
*
*	Input : col - Column to set cursor to.
*           row - Row to set cursor to.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiOutputSetXY (IN efi_uintn col, IN efi_uintn row) {

	efi_status_t result;

	result = EFI_CALL (_st->conOutProtocol->setCursorPosition,3,
		_st->conOutProtocol,col,row);

	if (result != EFI_SUCCESS)
		return NBL_STAT_DEVICE;
	return NBL_STAT_SUCCESS;
}

/**
*	EfiOutputGetXY
*
*	Description : Returns text mode cursor location.
*
*	Output : Cursor location in the following format:
*
*   +-----+-----+
^   | col | row |
^   +-----+-----+
*   16    8     0
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE uint16_t EfiOutputGetXY (void) {

	return (_st->conOutProtocol->mode->cursorCol << 8) | _st->conOutProtocol->mode->cursorRow;
}

/**
*	EfiOutputEnableCursor
*
*	Description : Enble hardware cursor.
*
*	Input : enable - TRUE to enable or FALSE to disable.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiOutputEnableCursor (IN BOOL enable) {

	if (EFI_CALL (_st->conOutProtocol->enableCursor,2,_st->conOutProtocol,enable) != EFI_SUCCESS)
		return NBL_STAT_DEVICE;
	return NBL_STAT_SUCCESS;
}

/**
*	EfiOutputGetCursorState
*
*	Description : Returns hardware cursor state.
*
*	Output : TRUE if enabled, FALSE otherwise.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE BOOL EfiOutputGetCursorState (void) {

	return _st->conOutProtocol->mode->cursorVisable;
}

/**
*	EfiOutputPrint
*
*	Description : Write string to console device.
*
*	Input : in - Input string to write.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiOutputPrint (IN char* in) {

	efi_status_t result;
	wchar_t      s[64];

	mbtows(in,s,64);

	result = EFI_CALL (_st->conOutProtocol->outputString,2,_st->conOutProtocol,s);
	if (result != EFI_SUCCESS)
		return NBL_STAT_DRIVER;
	return NBL_STAT_SUCCESS;
}

/**
*	EfiOutputWrite
*
*	Description : Write string to file device object.
*
*	Input : file - Input file device.
*           size - Size in bytes of output buffer.
*           buffer - Output buffer.
*
*	Output : Number of bytes written.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE uint32_t EfiOutputWrite (IN nblFile* file, IN size_t size, OUT char* buffer) {

	/* only write if we own the object. */
	if (file->driver != &_nEfiConOutDriverObject)
		return 0;

	/* write to "file" and return length. */
	if (EfiOutputPrint (buffer) != NBL_STAT_SUCCESS)
		return 0;
	return BlrStrLength (buffer);
}

STATIC nblDeviceObject _conout;

/**
*	EfiOutputOpen
*
*	Description : Open console driver.
*
*	Input : path (Ignored.)
*           dev - Must point to console device object created by this driver.
*
*	Output : Pointer to Standard Out Device File.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE nblFile* EfiOutputOpen (IN char* path, IN nblDeviceObject* dev) {

	if (dev != &_conout)
		return 0;

	nbl_stdout = &nbl_out;
	return nbl_stdout;
}

/**
*	EfiConOutRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiConOutRequest (IN nblMessage* rpb) {
	rettype_t r;
	r = 0;
	switch (rpb->type) {
		case NBL_CONOUT_RESET:
			r = (rettype_t) EfiOutputReset (FALSE);
			break;
		case NBL_CONOUT_CURSOR:
			r = (rettype_t) EfiOutputEnableCursor ((BOOL) rpb->NBL_CONOUT_CURSOR_ENABLE);
			break;
		case NBL_CONOUT_GETCURSOR:
			r = (rettype_t) EfiOutputGetCursorState ();
			break;
		case NBL_CONOUT_GETXY:
			r = (rettype_t) EfiOutputGetXY ();
			break;
		case NBL_CONOUT_SETXY:
			r = (rettype_t) EfiOutputSetXY 
				((efi_uintn) rpb->NBL_CONOUT_SET_X, (efi_uintn) rpb->NBL_CONOUT_SET_Y);
			break;
		case NBL_CONOUT_CLRSCR:
			r = (rettype_t) EfiOutputClearScreen
				((efi_uintn) rpb->NBL_CONOUT_CLRSCR_FCOLOR, (efi_uintn) rpb->NBL_CONOUT_CLRSCR_BCOLOR);
			break;
		case NBL_CONOUT_ATTRIB:
			r = (rettype_t) EfiOutputSetAttribute
				((efi_uintn) rpb->NBL_CONOUT_ATTRIB_FCOLOR, (efi_uintn) rpb->NBL_CONOUT_ATTRIB_BCOLOR);
			break;
		case NBL_CONOUT_GETATTRIB:
			r = (rettype_t) EfiOutputGetAttributes ();
			break;
		case NBL_CONOUT_SETMODE:
			r = (rettype_t) EfiOutputSetMode ((efi_uintn) rpb->NBL_CONOUT_SETMODE_MODE);
			break;
		case NBL_CONOUT_GETMODE:
			r = (rettype_t) EfiOutputGetMode ();
			break;
		case NBL_CONOUT_PRINT:
			r = (rettype_t) EfiOutputPrint ((char*) rpb->NBL_CONOUT_PRINT_STR);
			break;
		case NBL_FS_WRITE:
			r = (rettype_t) EfiOutputWrite ((nblFile*) rpb->NBL_FS_WRITE_FILE,
				rpb->NBL_FS_WRITE_SIZE, (char*) rpb->NBL_FS_WRITE_BUFFER);
			break;
		case NBL_FS_OPEN:
			r = (rettype_t) EfiOutputOpen ((char*) rpb->NBL_FS_OPEN_PATH,
				(nblDeviceObject*) rpb->NBL_FS_OPEN_DEVICE);
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

/* console output device object. */
STATIC nblDeviceObject _conout = {
	NBL_DEVICE_TERMINAL_PERIPHERAL,
	NBL_DEVICE_PERIPHERAL_CLASS,
	0, &_nEfiConOutDriverObject,
	&_nEfiConOutDriverObject, 0, "conout", 0
};

/**
*	EfiConOutEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiConOutEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	nblMessage rpb;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	/* create console device. */
	rpb.source = self;
	rpb.type   = NBL_DEV_REGISTER;
	rpb.NBL_DEV_REGISTER_OBJ = &_conout;
	if (dm->request (&rpb) != NBL_STAT_SUCCESS)
		return NBL_STAT_DEPENDENCY;

	/* redirect standard streams to console devices. */
	nbl_stdout = &nbl_out;

	return NBL_STAT_SUCCESS;
}

/* EFI driver object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiConOutDriverObject = {
	NBL_DRIVER_MAGIC,
	0,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_CON,
	"conout",
	EfiConOutEntryPoint,
	EfiConOutRequest,
	1,
	"dm"
};
