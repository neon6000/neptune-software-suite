/************************************************************************
*
*	NBL EFI Debug Support.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/*
	This component implements the Debug Port interface for EFI builds.
*/

/*
	Note that we can't test this. Under Virtual Box, InstallExceptionHandler
	return EFI_ALREADY_STARTED. If we install NULL first and then install
	the handler, they both return 0 - however when we force an exception to be
	thrown for testing, the handler is never called.

	Same happens under QEMU, both use the EDK II UEFI. Until we can get
	a workable environment, this driver just returns NOT_SUPPORTED.
*/

#include <nbl.h>
#include "efi.h"

/* This protocol provides services to allow the debug agent to register callback functions
that are called either periodically or when specific processor exceptions occur. */
#define EFI_DEBUG_SUPPORT_PROTOCOL_GUID \
 {0x2755590C, 0x6F3C, 0x42FA, \
 {0x9E, 0xA4, 0xA3, 0xBA, 0x54, 0x3C, 0xDA, 0x25}}

/* Debug port GUID. */
#define EFI_DEBUGPORT_PROTOCOL_GUID \
	{0xEBA4E8D2, 0x3858, 0x41EC, \
	{0xA2, 0x81, 0x26, 0x47, 0xBA, 0x96, 0x60, 0xD0}}

nblGUID _efiDebugSupportProtocol = EFI_DEBUG_SUPPORT_PROTOCOL_GUID;
nblGUID _efiDebugPortProtocol = EFI_DEBUGPORT_PROTOCOL_GUID;

/*** Exceptions ***/

/* Generic exception type. */
typedef efi_uint_t EFI_EXCEPTION_TYPE;

/* IA32 exception types. */
#define EXCEPT_IA32_DIVIDE_ERROR 0
#define EXCEPT_IA32_DEBUG 1
#define EXCEPT_IA32_NMI 2
#define EXCEPT_IA32_BREAKPOINT 3
#define EXCEPT_IA32_OVERFLOW 4
#define EXCEPT_IA32_BOUND 5
#define EXCEPT_IA32_INVALID_OPCODE 6
#define EXCEPT_IA32_DOUBLE_FAULT 8
#define EXCEPT_IA32_INVALID_TSS 10
#define EXCEPT_IA32_SEG_NOT_PRESENT 11
#define EXCEPT_IA32_STACK_FAULT 12
#define EXCEPT_IA32_GP_FAULT 13
#define EXCEPT_IA32_PAGE_FAULT 14
#define EXCEPT_IA32_FP_ERROR 16
#define EXCEPT_IA32_ALIGNMENT_CHECK 17
#define EXCEPT_IA32_MACHINE_CHECK 18
#define EXCEPT_IA32_SIMD 19

/* Machine type definitions. */
#define IMAGE_FILE_MACHINE_I386 0x014C
#define IMAGE_FILE_MACHINE_X64 0x8664
#define IMAGE_FILE_MACHINE_IA64 0x0200
#define IMAGE_FILE_MACHINE_EBC 0x0EBC
#define IMAGE_FILE_MACHINE_ARMTHUMB_MIXED 0xAA64
#define IMAGE_FILE_MACHINE_AARCH64 0xAA64

/* Instruction Set Architecture. */
typedef enum {
	IsaIa32 = IMAGE_FILE_MACHINE_I386,
	IsaX64 = IMAGE_FILE_MACHINE_X64,
	IsaIpf = IMAGE_FILE_MACHINE_IA64,
	IsaEbc = IMAGE_FILE_MACHINE_EBC,
	IsaArm = IMAGE_FILE_MACHINE_ARMTHUMB_MIXED,
	IsaAArch64 = IMAGE_FILE_MACHINE_AARCH64
} efiInstructionSetArchitecture;

// FXSAVE_STATE - FP / MMX / XMM registers
typedef struct {
	efi_uint16_t Fcw;
	efi_uint16_t Fsw;
	efi_uint16_t Ftw;
	efi_uint16_t Opcode;
	efi_uint32_t Eip;
	efi_uint16_t Cs;
	efi_uint16_t Reserved1;
	efi_uint32_t DataOffset;
	efi_uint16_t Ds;
	efi_uint8_t Reserved2[10];
	efi_uint8_t St0Mm0[10], Reserved3[6];
	efi_uint8_t St1Mm1[10], Reserved4[6];
	efi_uint8_t St2Mm2[10], Reserved5[6];
	efi_uint8_t St3Mm3[10], Reserved6[6];
	efi_uint8_t St4Mm4[10], Reserved7[6];
	efi_uint8_t St5Mm5[10], Reserved8[6];
	efi_uint8_t St6Mm6[10], Reserved9[6];
	efi_uint8_t St7Mm7[10], Reserved10[6];
	efi_uint8_t Xmm0[16];
	efi_uint8_t Xmm1[16];
	efi_uint8_t Xmm2[16];
	efi_uint8_t Xmm3[16];
	efi_uint8_t Xmm4[16];
	efi_uint8_t Xmm5[16];
	efi_uint8_t Xmm6[16];
	efi_uint8_t Xmm7[16];
	efi_uint8_t Reserved11[14 * 16];
} efiFxSaveStateIA32;

// System context for IA-32 processors
typedef struct {
	efi_uint32_t ExceptionData; // ExceptionData is
	// additional data pushed
	// on the stack by some
	// types of IA-32
	// exceptions
	efiFxSaveStateIA32 FxSaveState;
	efi_uint32_t Dr0, Dr1, Dr2, Dr3, Dr6, Dr7;
	efi_uint32_t Cr0, Cr1 /* Reserved */, Cr2, Cr3, Cr4;
	efi_uint32_t Eflags;
	efi_uint32_t Ldtr, Tr;
	efi_uint32_t Gdtr[2], Idtr[2];
	efi_uint32_t Eip;
	efi_uint32_t Gs, Fs, Es, Ds, Cs, Ss;
	efi_uint32_t Edi, Esi, Ebp, Esp, Ebx, Edx, Ecx, Eax;
} efiSystemContextIA32;

typedef union {
	efiSystemContextIA32*    SystemContextIa32;
//	efiSystemContextEBC*     SystemContextEbc;
//	efiSystemContextX64*     SystemContextX64;
//	efiSystemContextIPF*     SystemContextIpf;
//	efiSystemContextARM*     SystemContextArm;
//	efiSystemContextAARCH64* SystemContextAArch64;
} efiSystemContext;

typedef struct _efiDebugSupportProtocol efiDebugSupportProtocol;

typedef VOID       (*EFI_PERIODIC_CALLBACK)          (IN OUT efiSystemContext SystemContext);
typedef EFI_STATUS (*EFI_GET_MAXIMUM_PROCESSOR_INDEX) (IN efiDebugSupportProtocol *This, OUT efi_uint_t *MaxProcessorIndex);
typedef EFI_STATUS (*EFI_GET_MAXIMUM_PROCESSOR_INDEX) (IN efiDebugSupportProtocol *This, OUT efi_uint_t *MaxProcessorIndex);
typedef EFI_STATUS (*EFI_REGISTER_PERIODIC_CALLBACK)  (IN efiDebugSupportProtocol *This, IN efi_uint_t ProcessorIndex,
	IN EFI_PERIODIC_CALLBACK PeriodicCallback);
typedef VOID       (*EFI_EXCEPTION_CALLBACK) (IN EFI_EXCEPTION_TYPE ExceptionType, IN OUT efiSystemContext SystemContext);
typedef EFI_STATUS(*EFI_REGISTER_EXCEPTION_CALLBACK) (IN efiDebugSupportProtocol *This, IN efi_uint_t ProcessorIndex,
	IN EFI_EXCEPTION_CALLBACK ExceptionCallback, IN EFI_EXCEPTION_TYPE ExceptionType);
typedef EFI_STATUS (*EFI_INVALIDATE_INSTRUCTION_CACHE) (IN efiDebugSupportProtocol *This, IN efi_uint_t ProcessorIndex,
	IN VOID *Start, IN efi_uint64_t Length);

typedef struct _efiDebugSupportProtocol {
	efiInstructionSetArchitecture Isa;
	EFI_GET_MAXIMUM_PROCESSOR_INDEX GetMaximumProcessorIndex;
	EFI_REGISTER_PERIODIC_CALLBACK RegisterPeriodicCallback;
	EFI_REGISTER_EXCEPTION_CALLBACK RegisterExceptionCallback;
	EFI_INVALIDATE_INSTRUCTION_CACHE InvalidateInstructionCache;
} efiDebugSupportProtocol;

/*** Debug Port ***/

typedef struct _efiDebugPortProtocol efiDebugPortProtocol;

typedef EFI_STATUS (*EFI_DEBUGPORT_RESET) (IN efiDebugPortProtocol *This);
typedef EFI_STATUS(*EFI_DEBUGPORT_WRITE) (IN efiDebugPortProtocol *This, IN efi_uint32_t Timeout,
	IN OUT efi_uint_t *BufferSize, IN VOID *Buffer);
typedef EFI_STATUS(*EFI_DEBUGPORT_READ) (IN efiDebugPortProtocol *This, IN efi_uint32_t Timeout,
	IN OUT efi_uint_t *BufferSize, OUT VOID *Buffer);
typedef EFI_STATUS(*EFI_DEBUGPORT_POLL) (IN efiDebugPortProtocol *This);

typedef struct _efiDebugPortProtocol {
	EFI_DEBUGPORT_RESET Reset;
	EFI_DEBUGPORT_WRITE Write;
	EFI_DEBUGPORT_READ Read;
	EFI_DEBUGPORT_POLL Poll;
} efiDebugPortProtocol;

/* pointer to system table. */
STATIC efiSystemTable* _st = 0;

/* image handle. */
STATIC efi_handle_t _image = 0;

efiDebugSupportProtocol* _debug;
efiDebugPortProtocol* _debugport;

PRIVATE efiSystemTable* EfiDebugGetSystemTable(void) {

	nblDriverObject* fm;
	nblMessage       m;

	/* get EFI system table pointer. */
	if (_st)
		return _st;

	/* open firmware efi driver. */
	fm = BlrOpenDriver("root/fw");
	if (!fm)
		return NULL;

	/* get EFI boot services. */
	m.type = NBL_FW_EFI_SERVICES;
	if (fm->request(&m) == NBL_STAT_SUCCESS)
		_st = (efiSystemTable*)m.retval;

	/* get image handle. */
	m.type = NBL_FW_EFI_IMAGEHANDLE;
	if (fm->request(&m) == NBL_STAT_SUCCESS)
		_image = (efi_handle_t*)m.retval;

	return _st;
}

EFI_STATUS EfiExceptionHandler(IN efiDebugSupportProtocol *This, IN efi_uint_t ProcessorIndex,
	IN EFI_EXCEPTION_CALLBACK ExceptionCallback, IN EFI_EXCEPTION_TYPE ExceptionType) {

	while (TRUE) {

		BlrPrintf("Exception %x", ExceptionType);

	}
}

void EfiInitDebugSupport(void) {

	uint32_t processorIndex;
	efi_status_t status;

	processorIndex = 0;

	status = EFI_CALL(_st->boottime->locateProtocol, 3, &_efiDebugSupportProtocol, NULL, &_debug);

	BlrPrintf("\n\rstatus: %x debug: %x", status, _debug);

	BlrPrintf("\n\r--Installing--");

	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_DEBUG);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_NMI);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_BREAKPOINT);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_OVERFLOW);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_BOUND);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_INVALID_OPCODE);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_DOUBLE_FAULT);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_INVALID_TSS);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_SEG_NOT_PRESENT);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_STACK_FAULT);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_GP_FAULT);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_PAGE_FAULT);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_FP_ERROR);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_ALIGNMENT_CHECK);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_MACHINE_CHECK);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, NULL, EXCEPT_IA32_SIMD);

	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_DEBUG);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_NMI);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_BREAKPOINT);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_OVERFLOW);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_BOUND);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_INVALID_OPCODE);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_DOUBLE_FAULT);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_INVALID_TSS);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_SEG_NOT_PRESENT);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_STACK_FAULT);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_GP_FAULT);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_PAGE_FAULT);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_FP_ERROR);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_ALIGNMENT_CHECK);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_MACHINE_CHECK);
	EFI_CALL(_debug->RegisterExceptionCallback, 4, _debug, processorIndex, EfiExceptionHandler, EXCEPT_IA32_SIMD);

	BlrPrintf("\n\r--- Done ---");

}

void EfiInitDebugPort(void) {

	efi_status_t status;

	BlrPrintf("\n\r--Debug Port Support--");

	BlrPrintf("\n\r--- Locate Protocol ---");

	status = EFI_CALL(_st->boottime->locateProtocol, 3, &_efiDebugPortProtocol, NULL, &_debugport);

	BlrPrintf("\n\rstatus: %x debug: %x", status, _debugport);
}

STATIC void DebugRead(IN size_t size, OUT char* buffer) {

}

STATIC void DebugWrite(IN size_t size, IN char* buffer) {

}

/**
*	DebugEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
STATIC status_t DebugEntryPoint(IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	return NBL_STAT_UNSUPPORTED;

#if 0
	/* get efi boot services table and return success. */
	if (!EfiDebugGetSystemTable())
		return NBL_STAT_DEPENDENCY;

	EfiInitDebugSupport();
	EfiInitDebugPort();

	return NBL_STAT_SUCCESS;
#endif
}

/**
*	DebugRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
STATIC status_t DebugRequest(IN nblMessage* rpb) {

	rettype_t r;

	rpb->retval = 0;
	r = 0;

	switch (rpb->type) {
	case NBL_DEBUG_READ:
		DebugRead(rpb->NBL_DEBUG_READ_SIZE, rpb->NBL_DEBUG_READ_BUFFER);
		break;
	case NBL_DEBUG_WRITE:
		DebugWrite(rpb->NBL_DEBUG_WRITE_SIZE, rpb->NBL_DEBUG_WRITE_BUFFER);
		break;
	default:
		return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

/*
	Driver version number.
*/
#define NBL_EFI_VERSION 1

/*
	Firmware Driver object.
*/
PUBLIC
NBL_DRIVER_OBJECT_DEFINE _nEfiDebugDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_EFI_VERSION,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_OTHER,
	"debug",
	DebugEntryPoint,
	DebugRequest,
	0
};
