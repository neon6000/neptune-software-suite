/************************************************************************
*
*	NBL EFI Graphics Output Protocol (GOP) Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "efi.h"

/* pointer to system table. */
STATIC efiSystemTable* _st = 0;

/* self reference. */
STATIC nblDriverObject* _self = 0;

/* GOP protocol GUID. */
STATIC nblGUID _gopGUID = GOP_PRTOCOL;

/* GOP protocol. */
STATIC efiGop* _gop = NULL;

/* display device object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiGopDriverObject;
STATIC nblDeviceObject _gopDeviceObject = {
	NBL_DEVICE_DISPLAY_CONTROLLER,
	NBL_DEVICE_CONTROLLER_CLASS,
	0, &_nEfiGopDriverObject, 0, 0, "video", 0
};

STATIC uint32_t EfiGopCountBits (IN uint32_t val) {

	uint32_t c;

	for (c = 31; c >= 0; c--)
		if (val & (1 << c))
			return c + 1;

	return 0;
}

STATIC uint32_t BlEfiGopGetShift (IN uint32_t value) {

	uint32_t c = 1;
	uint32_t r = 0;

	while (!(value & c)) {
		c <<= 1;
		r++;
	}
	return r;
}

/* EFI mode info to NBL mode info. */
STATIC BOOL EfiGopToNblFormat (IN uint32_t mode, IN efiGopModeInfo* in, OUT nblVideoMode* out) {

	uint32_t mask;

	out->colors = 255;
	out->width  = in->horzResolution;
	out->height = in->vertResolution;
	out->pitch  = in->pixelsPerScanLine;
	out->mode   = mode;

	out->format.amask = in->pixelInformation.reservedMask;
	out->format.bmask = in->pixelInformation.blueMask;
	out->format.gmask = in->pixelInformation.greenMask;
	out->format.rmask = in->pixelInformation.redMask;

	switch(in->pixelFormat) {
		case PixelRedGreenBlueReserved8BitPerColor:

			out->format.bpp = 32;
			out->format.bytesPerPixel = 4;

			out->format.rshift = 0;
			out->format.gshift = 8;
			out->format.bshift = 16;
			out->format.ashift = 24;

			break;

		case PixelBlueGreenRedReserved8BitPerColor:

			out->format.bpp = 32;
			out->format.bytesPerPixel = 4;

			out->format.bshift = 0;
			out->format.gshift = 8;
			out->format.rshift = 16;
			out->format.ashift = 24;

			break;

		case PixelBitMask:

			/* need to count bpp from the bit mask field. */

			/* make sure none of the fields overlap. */
			if ((in->pixelInformation.redMask      & in->pixelInformation.blueMask)
				|| (in->pixelInformation.redMask   & in->pixelInformation.greenMask)
				|| (in->pixelInformation.redMask   & in->pixelInformation.reservedMask)
				|| (in->pixelInformation.greenMask & in->pixelInformation.blueMask)
				|| (in->pixelInformation.greenMask & in->pixelInformation.reservedMask)
				|| (in->pixelInformation.blueMask  & in->pixelInformation.reservedMask)) {

					return FALSE;
			}

			/* combine mask. */
			mask = in->pixelInformation.redMask | in->pixelInformation.greenMask
				| in->pixelInformation.blueMask | in->pixelInformation.reservedMask;

			out->format.bpp = EfiGopCountBits (mask);
			out->format.bytesPerPixel = out->format.bpp / 8;

			/** need to calculate these too. */

			out->format.bshift = BlEfiGopGetShift(out->format.bmask);
			out->format.gshift = BlEfiGopGetShift(out->format.gmask);
			out->format.rshift = BlEfiGopGetShift(out->format.rmask);
			out->format.ashift = BlEfiGopGetShift(out->format.amask);

			break;

		case PixelBltOnly:
		default:

			/* no physical frame buffer. */
			out->format.bpp    = 0;
			out->format.ashift = 0;
			out->format.bshift = 0;
			out->format.gshift = 0;
			out->format.rshift = 0;
			out->format.bytesPerPixel = 0;

			break;
	};

	return TRUE;
}

/* returns EFI system table. */
STATIC efiSystemTable* EfiGopGetSystemTable (void) {

	nblDriverObject* fm;
	nblMessage       m;

	/* get EFI system table pointer. */
	if (_st)
		return _st;

	/* open firmware efi driver. */
	fm = BlrOpenDriver ("root/fw");
	if (!fm)
		return NULL;

	/* get EFI boot services. */
	m.type = NBL_FW_EFI_SERVICES;
	if (fm->request(&m) == NBL_STAT_SUCCESS)
		_st = (efiSystemTable*) m.retval;

	return _st;
}

/* open protocol. */
STATIC void* EfiGopLocateProtocol (IN nblGUID* protocol) {

	void*    protocolInterface;
	status_t status;

	status = EFI_CALL (_st->boottime->locateProtocol,3,protocol,NULL,&protocolInterface);

	if (status != EFI_SUCCESS) {
		BlrPrintf("\n\rGOP OPEN CODE: %x", status);
		return NULL;
	}

	return protocolInterface;
}

/* query video modes. */
STATIC status_t EfiGopQueryModes (IN NBL_QUERY_MODE_CALLBACK callback, IN void* ex) {

	uint32_t      mode;
	efi_status_t  status;
	nblVideoMode  modeInfo;

	if (!_gop)
		return NBL_STAT_DRIVER;

	for (mode = 0; mode < _gop->mode->maxMode; mode++) {

		efi_uintn       infoSize;
		efiGopModeInfo* info;

		info = NULL;
		status = EFI_CALL (_gop->queryMode, 4, _gop, mode, &infoSize, &info);

		if (status != EFI_SUCCESS)
			continue;

		EfiGopToNblFormat(mode, info, &modeInfo);

		if (!callback(&modeInfo, ex))
			break;
	}

	return NBL_STAT_SUCCESS;
}

/* driver entry point. */
status_t EfiGopEntryPoint (IN nblDriverObject* self, IN int dependencyCount,
						 IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	nblMessage m;
	_self = self;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	/* get efi boot services table and return success. */
	if (! EfiGopGetSystemTable () )
		return NBL_STAT_DEPENDENCY;

	/* get protocol. */
	_gop = EfiGopLocateProtocol(&_gopGUID);
	if (!_gop)
		return NBL_STAT_UNSUPPORTED;

	/* create device. */
	m.source = self;
	m.type   = NBL_DEV_REGISTER;
	m.NBL_DEV_REGISTER_OBJ = &_gopDeviceObject;
	dm->request (&m);

	return NBL_STAT_SUCCESS;
}

STATIC struct {
	nblVideoMode mode;
	efi_physical_addr_t lfb;
	efi_uintn lfsize;
}_efiGopFb;

/* set video mode. */
STATIC status_t EfiGopSetVideoMode (IN uint32_t width, IN uint32_t height, IN uint32_t depth) {

	uint32_t      mode;
	efi_status_t  status;
	nblVideoMode  modeInfo;

	if (!_gop)
		return NBL_STAT_DRIVER;

	for (mode = 0; mode < _gop->mode->maxMode; mode++) {

		efi_uintn       infoSize;
		efiGopModeInfo* info;

		info = NULL;

		status = EFI_CALL (_gop->queryMode, 4, _gop, mode, &infoSize, &info);
		if (status != EFI_SUCCESS)
			continue;

		EfiGopToNblFormat(mode, info, &modeInfo);

		if (width == modeInfo.width && height == modeInfo.height
			&& depth == modeInfo.format.bpp) {
				break;
		}
	}

	if (mode == _gop->mode->maxMode)
		return NBL_STAT_UNSUPPORTED;

	if ( EFI_CALL (_gop->setMode, 2, _gop, mode) != EFI_SUCCESS)
		return NBL_STAT_DEVICE;

	BlrCopyMemory (&_efiGopFb.mode, &modeInfo, sizeof (nblVideoMode));
	_efiGopFb.lfb = _gop->mode->fbBase;
	_efiGopFb.lfsize = _gop->mode->fbSize;

	return NBL_STAT_SUCCESS;
}

/* gets video mode info. */
STATIC status_t EfiGopGetVideoMode (OUT nblVideoMode* mode) {

	return 0;
}

/* create new surface object. */
status_t EfiGopCreateSurface (IN OUT nblSurface* out) {

	/* assume primary surface for now. */

	out->width = _efiGopFb.mode.width;
	out->height = _efiGopFb.mode.height;
	out->pitch = _efiGopFb.mode.pitch;
	BlrCopyMemory(&out->format, &_efiGopFb.mode.format, sizeof (nblPixelFormat));
	out->pixels = (void*) _efiGopFb.lfb;

	return 0;
}

/* driver requests. */
status_t EfiGopRequest (IN nblMessage* rpb) {
	rettype_t r;
	rpb->retval = 0;
	switch (rpb->type) {
		case NBL_VIDEO_SETMODE:
			r =(rettype_t) EfiGopSetVideoMode (rpb->NBL_VIDEO_SETMODE_WIDTH,
				rpb->NBL_VIDEO_SETMODE_HEIGHT, rpb->NBL_VIDEO_SETMODE_DEPTH);
			break;
		case NBL_VIDEO_GETMODE:
			r = (rettype_t) EfiGopGetVideoMode ((nblVideoMode*) rpb->NBL_VIDEO_GETMODE_OUT);
			break;
		case NBL_VIDEO_QUERY:
			r = (rettype_t) EfiGopQueryModes ((NBL_QUERY_MODE_CALLBACK) rpb->NBL_VIDEO_QUERY_CALL,
				(void*) rpb->NBL_VIDEO_QUERY_EX);
			break;
		case NBL_VIDEO_CREATESURFACE:
			r = (rettype_t) EfiGopCreateSurface((nblSurface*) rpb->NBL_VIDEO_CREATESURFACE_IN);
			break;
		case NBL_VIDEO_GETPALETTE:
		case NBL_VIDEO_SETPALETTE:
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_EFI_GOP_VERSION 1

/* EFI driver object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiGopDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_EFI_GOP_VERSION,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_VIDEO,
	"gop",
	EfiGopEntryPoint,
	EfiGopRequest,
	1,
	"dm"
};
