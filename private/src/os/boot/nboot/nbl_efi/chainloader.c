/************************************************************************
*
*	chainloader.c - EFI Chainloader
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "efi.h"

#define SECTOR_SIZE 512

extern efiSystemTable*  _st;

#define EFI_DEVICE_PATH_TYPE(dp)		((dp)->type & 0x7f)

#define EFI_DEVICE_PATH_SUBTYPE(dp)	((dp)->subType)

#define EFI_DEVICE_PATH_LENGTH(dp)		\
  ((dp)->length[0] | ((uint16_t) ((dp)->length[1]) << 8))

/* The End of Device Path nodes.  */
#define EFI_END_DEVICE_PATH_TYPE			(0xff & 0x7f)

#define EFI_END_ENTIRE_DEVICE_PATH_SUBTYPE		0xff
#define EFI_END_THIS_DEVICE_PATH_SUBTYPE		0x01

#define EFI_END_ENTIRE_DEVICE_PATH(dp)	\
  (EFI_DEVICE_PATH_TYPE (dp) == EFI_END_DEVICE_PATH_TYPE \
   && (EFI_DEVICE_PATH_SUBTYPE (dp) \
       == EFI_END_ENTIRE_DEVICE_PATH_SUBTYPE))

#define EFI_NEXT_DEVICE_PATH(dp)	\
  ((efiDevicePath *) ((char *) (dp) \
                               + EFI_DEVICE_PATH_LENGTH (dp)))

/**
*	mbtows
*
*	Description : Convert multibyte character string to wide character string.
*
*	Input : mb - Multibyte string to convert.
*           wc - OUT Output Wide Character buffer.
*           c - Size of output buffer in characters.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void mbtows (IN char* mb, OUT wchar_t* wc, IN size_t c) {
	unsigned int s = BlrStrLength(mb);
	unsigned int i;
	for (i=0;i<s;i++) {
		BlrMbtowc (wc,mb,c);
		wc++;
		mb++;
	}
	*wc=0;
}

/**
*	strchr
*
*	Description : Returns first occurrence of character in string.
*
*	Input : s - String to convert.
*           c - Character to search for.
*
*	Output : Pointer in s to location of c or NULL.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE char* strchr (IN const char *s, IN int c) {
	const char ch = c;
    for ( ; *s != ch; s++)
        if (*s == '\0')
            return NULL;
    return (char *)s;
}

/**
*	strrchr
*
*	Description : Returns last occurrence of character in string.
*
*	Input : s - String to convert.
*           c - Character to search for.
*
*	Output : Pointer in s to location of c or NULL.
*
*	Execution Mode : Kernel Mode.
*/
STATIC char* strrchr (IN const char* s, IN int c) {
	char *rtnval = 0;
	do {
	if (*s == c)
		rtnval = (char*) s;
	} while (*s++);
	return (rtnval);
}

/**
*	GetImageHandle
*
*	Description : Returns handle to EFI image.
*
*	Output : Handle to EFI image or NULL.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE efi_handle_t GetImageHandle (void) {

	nblMessage       rpb;
	nblDriverObject* fw;

	fw = BlrOpenDriver("root/fw");
	if (!fw) {
		return NULL;
	}
	rpb.type = NBL_FW_EFI_IMAGEHANDLE;
	if (fw->request)
		return (efi_handle_t) fw->request(&rpb);
	return NULL;
}

/**
*	Boot
*
*	Description : Execute an EFI boot application
*
*	Input : imageHandle - Handle of EFI boot application to execute.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t Boot (IN efi_handle_t imageHandle) {

	efi_uintn     exitDataSize;
	efi_char16_t* exitData;
	efi_status_t  status;

	status = EFI_CALL(_st->boottime->startImage,3,imageHandle,&exitDataSize, &exitData);
	if (status != EFI_SUCCESS) {

		char* errorData;

		errorData = BlrAlloc (exitDataSize);
		if (errorData) {

			mbtows (errorData, exitData, exitDataSize);
			BlrError("\n\r Info : '%s'", errorData);

			BlrFree (errorData);
			return NBL_STAT_DEVICE;
		}
	}
	return NBL_STAT_SUCCESS;
}

/**
*	CopyFilePath
*
*	Description : Convert an NBL path name to EFI path name.
*
*	Input : fp - OUT EFI file path object.
*           str - Input file path.
*           len - Length in characters of the input file path.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void CopyFilePath (OUT efiFilePathDevicePath *fp, IN const char *str, IN efi_uint16_t len) {

	efi_char16_t* p;
	efi_uint16_t size;

	size = len * sizeof (efi_char16_t) + sizeof (efiFilePathDevicePath);

	fp->header.type      = EFI_DEV_MEDIA;
	fp->header.subType   = EFI_DEV_MEDIA_FILE;
	fp->header.length[0] = size & 0xff;
	fp->header.length[1] = size >> 8;

	for (p = fp->pathName; len > 0; len--, p++, str++)
		*p = (efi_char16_t) (*str == '/' ? '\\' : *str);
}

/**
*	BuildDevicePath
*
*	Description : Convert NBL device path to EFI device path.
*
*	Input : file - NBL device path.
*           dp - OUT EFI device path.
*
*	Output : Pointer to device path or NULL.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE efiDevicePath* BuildDevicePath (IN char* file, OUT efiDevicePath* dp) {

	efiDevicePath* fp;
	efiDevicePath* d;
	size_t   size;
	char*    pathStart;
	char*    pathEnd;

	size = 0;
	d    = dp;

	pathStart = strchr(file, ':');
	if (!pathStart)
		pathStart = file;
	else
		pathStart++;

	pathEnd = strrchr(file, '/');
	if (!pathEnd)
		return NULL;

	while (TRUE) {

		size += EFI_DEVICE_PATH_LENGTH (d);
		if (EFI_END_ENTIRE_DEVICE_PATH (d))
			break;
		d = EFI_NEXT_DEVICE_PATH (d);
	}

	/* allocate memory for file path */
	fp = (efiDevicePath*) BlrAlloc (size + ((BlrStrLength(pathStart) + 1)
		* sizeof(efi_char16_t)) + sizeof (efiFilePathDevicePath) * 2);
	if (!fp) {
		return NULL;
	}
	BlrCopyMemory (fp, dp, size);

	/* fill file path for the directory */
	d = (efiDevicePath*) ((char*) fp + ((char*) d - (char*) dp));
	CopyFilePath ((efiFilePathDevicePath*) d, pathStart, pathEnd - pathStart);

	/* fill file path for the file */
	d = EFI_NEXT_DEVICE_PATH(d);
	CopyFilePath ((efiFilePathDevicePath*) d, pathEnd + 1, BlrStrLength (pathEnd + 1));

	/* fill end of device path nodes */
	d  = EFI_NEXT_DEVICE_PATH(d);
	d->type      = EFI_END_DEVICE_PATH_TYPE;
	d->subType   = EFI_END_ENTIRE_DEVICE_PATH_SUBTYPE;
	d->length[0] = sizeof(*d);
	d->length[1] = 0;

	/* return file path */
	return fp;
}

/**
*	Chainload
*
*	Description : Chainload a specified device.
*
*	Input : file - NBL device path.
*
*	Output : Pointer to device path or NULL.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t Chainload (IN char* file) {

	efi_handle_t          callerImage;
	efi_handle_t          imageHandle;
	efiDevicePath*        fp;
	efiDevicePath         dp;
	efi_status_t          status;

	/* get handle to our own image */
	callerImage = GetImageHandle ();
	if (!callerImage) {
		BlrError("\n\rNo image handle");
		return NBL_STAT_DRIVER;
	}

	/* build efi device path */
	fp = BuildDevicePath (file, &dp);

	/* load image. We let EFI allocate and map the image into memory. */
	status = EFI_CALL(_st->boottime->loadImage,6,FALSE,callerImage,fp,NULL,0,&imageHandle);

	if (status != EFI_SUCCESS) {
		if (status == EFI_NOT_FOUND) {
			BlrError("\n\rUnable to locate image", file);
			return NBL_STAT_DRIVER;
		}else if (status == EFI_LOAD_ERROR) {
			BlrError("\n\rInvalid or corrupt image", file);
			return NBL_STAT_DRIVER;
		}
		else{
			BlrError("\n\rDevice or resource error");
			return NBL_STAT_DEVICE;
		}
	}

	return NBL_STAT_SUCCESS;
}

/**
*	EfiChainloaderEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiChainloaderEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	/* nothing to do. */
	return NBL_STAT_SUCCESS;
}

/**
*	EfiChainloaderRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t EfiChainloaderRequest (IN nblMessage* rpb) {
	rettype_t r;
	rpb->retval = 0;
	switch (rpb->type) {
		case NBL_CHAINLOADER_BOOT:
			r = Chainload ((char*) rpb->NBL_CHAINLOADER_BOOT_DEVICE);
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_EFI_CHAINLOADER_VERSION 1

/* Chainloader driver object. */
NBL_DRIVER_OBJECT_DEFINE _nEfiChainloaderDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_EFI_CHAINLOADER_VERSION,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_OTHER,
	"chainloader",
	EfiChainloaderEntryPoint,
	EfiChainloaderRequest,
	0
};
