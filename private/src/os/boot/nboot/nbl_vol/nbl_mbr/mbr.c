/************************************************************************
*
*	mbr.c - Master Boot Record Parition Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>

#pragma pack(push, 1)
typedef struct partition {
	uint8_t  active;
	uint8_t  startChs[3];
	uint8_t  systemId;
	uint8_t  endChs[3];
	uint32_t lba;
	uint32_t size;
}partition;
#pragma pack(pop)

typedef struct nblVolumeDeviceObject {
	nblDeviceObject* disk;
	partition part;
}nblVolumeDeviceObject;

STATIC nblDriverObject* _self;

/* read volume. */
STATIC size_t BlVolumeRead (IN nblDeviceObject* disk, IN size_t sector, IN size_t off, IN size_t size, OUT void* buffer) {

	nblVolumeDeviceObject* ex;
	nblMessage m;

	if(disk->driverObject!=_self)
		return 0; /* not a volume device object. */

	ex = disk->ext;
	if (!ex->disk)
		return 0; /* no disk associated with volume. */

	/* send the request. */
	m.source = _self;
	m.type = NBL_DISK_READ;
	m.NBL_DISK_READ_SECTOR = sector + ex->part.lba;
	m.NBL_DISK_READ_SIZE = size;
	m.NBL_DISK_READ_BUFFER = (uint32_t) buffer;
	m.NBL_DISK_READ_OFFSET = off;
	m.NBL_DISK_READ_DEV = ex->disk;
	ex->disk->driverObject->request(&m);
	return (size_t) m.retval;
}

/* write volume. */
STATIC size_t BlVolumeWrite (IN nblDeviceObject* disk, IN size_t sector, IN size_t off, IN size_t size, OUT void* buffer) {

	nblVolumeDeviceObject* ex;
	nblMessage m;

	if(disk->driverObject!=_self)
		return 0; /* not a volume device object. */

	ex = disk->ext;
	if (!ex->disk)
		return 0; /* no disk associated with volume. */

	/* send the request. */
	m.source = _self;
	m.type = NBL_DISK_WRITE;
	m.NBL_DISK_WRITE_SECTOR = sector + ex->part.lba;
	m.NBL_DISK_WRITE_SIZE = size;
	m.NBL_DISK_WRITE_BUFFER = (uint32_t) buffer;
	m.NBL_DISK_WRITE_OFFSET = off;
	m.NBL_DISK_WRITE_DEV = ex->disk;
	ex->disk->driverObject->request(&m);
	return (size_t) m.retval;
}

/* scan volumes. */
STATIC BOOL BlVolumeScan (IN nblDeviceObject* disk) {

	partition patitionList[4];
	nblDriverObject* dm;
	nblMessage m;
	int count;

	dm = BlrOpenDriver("root/dm");
	if (!dm)
		return FALSE; /* this is fatal. */

	m.source = _self;
	m.type = NBL_DISK_READ;
	m.NBL_DISK_READ_SECTOR = 0;
	m.NBL_DISK_READ_SIZE = sizeof(partition) * 4;
	m.NBL_DISK_READ_BUFFER = (uint32_t) patitionList;
	m.NBL_DISK_READ_OFFSET = 0x01be;
	m.NBL_DISK_READ_DEV = disk;
	disk->driverObject->request(&m);
	if (!m.retval)
		return FALSE;

	for (count = 0; count < 4; count++) {

		/* create volume. */
		nblDeviceObject* dev;
		nblVolumeDeviceObject* volume;
		char* name;

		/* skip inactive partitions. */
		if (patitionList [count].active != 0x80)
			continue;

		dev = BlrAlloc (sizeof (nblDeviceObject));
		volume = BlrAlloc (sizeof (nblVolumeDeviceObject));
		name = BlrAlloc ( BlrStrLength (disk->name) + 9 + 2 + 1 );
			/*  strlen(name) + strlen("partition") + "()" + NULL */

		if (!dev)    goto on_error;
		if (!volume) goto on_error;
		if (!name)   goto on_error;

		/* initialize volume info. */
		volume->disk = disk;
		BlrCopyMemory ( &volume->part, &patitionList [count], sizeof (partition) );

		/* build device name. All device names have form "device_name(n)partition(m)" */
		{
			int c = BlrStrLength(disk->name);
			BlrCopyMemory(name,disk->name,c);
			BlrCopyMemory(name+c,"partition(0)", 13);
			name[c + 12 - 2] = count + '0';
		}

		/* create volume device object. */
		dev->type            = NBL_DEVICE_DISK_CONTROLLER;
		dev->type            = NBL_DEVICE_CONTROLLER_CLASS;
		dev->characteristics = 0;
		dev->driverObject    = _self;
		dev->attached        = 0;
		dev->next            = 0;
		dev->name            = name;
		dev->ext            = volume;

		/* create volume. */
		m.source = _self;
		m.type   = NBL_DEV_REGISTER;
		m.NBL_DEV_REGISTER_OBJ = dev;
		dm->request (&m);

		continue;

	on_error:

		BlrFree(name);
		BlrFree(volume);
		BlrFree(dev);
		return FALSE;
	}

	return TRUE;
}

status_t MbrEntryPoint (IN nblDriverObject* self, IN int dependencyCount,
						 IN nblDriverObject* dependencies[]) {

	_self=self;
	return NBL_STAT_SUCCESS;
}

status_t MbrRequest (IN nblMessage* rpb) {
	rettype_t r;
	rpb->retval = 0;
	switch (rpb->type) {
		case NBL_DISK_SCAN:
			r =  (rettype_t) BlVolumeScan (rpb->NBL_DISK_READ_DEV);
			break;
		case NBL_DISK_READ:
			r = (rettype_t) BlVolumeRead (rpb->NBL_DISK_READ_DEV,rpb->NBL_DISK_READ_SECTOR,
				rpb->NBL_DISK_READ_OFFSET,rpb->NBL_DISK_READ_SIZE, (void*) rpb->NBL_DISK_READ_BUFFER);
			break;
		case NBL_DISK_WRITE:
			r = (rettype_t) BlVolumeWrite (rpb->NBL_DISK_WRITE_DEV,rpb->NBL_DISK_WRITE_SECTOR,
				rpb->NBL_DISK_WRITE_OFFSET,rpb->NBL_DISK_WRITE_SIZE, (void*) rpb->NBL_DISK_WRITE_BUFFER);
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_MBR_VERSION 1

NBL_DRIVER_OBJECT_DEFINE _nmbrDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_MBR_VERSION,
	NBL_DRV_LOAD_CORE,
	NBL_DRV_CLASS_BLOCK,
	"mbr",
	MbrEntryPoint,
	MbrRequest,
	0
};
