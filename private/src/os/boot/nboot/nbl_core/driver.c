/************************************************************************
*
*	Neptune Boot Library Driver Services
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>

/*
	This module implements driver scanning and initialization facilities.
*/

/*
The .drv section has the following format :

+-------------------------------------------------+
| _DrvEntryPoint | DRIVER_OBJECT[] | _DevEntryEnd |
+-------------------------------------------------+
*/
__declspec(allocate(".drv$a")) int _DevEntryStart = 0;
__declspec(allocate(".drv$z")) int _DevEntryEnd   = 0;

/* Max driver count. */
#define nblDriverObject_MAX 32

/* Global driver list. */
unsigned int    _driverCount = 0;
nblDriverObject _drivers[nblDriverObject_MAX];

/**
*	Locate driver object given a name
*	\param name Driver name
*	\param Pointer to driver or 0 on error
*/
nblDriverObject* _BlFwGetDriverObject (IN char* name) {
	unsigned int c;
	if (name) {
		for (c=0; c<_driverCount; c++) {
			if (BlrStrCompare (_drivers[c].name,name) == 0)
				return &_drivers[c];
		}
	}
	return 0;
}

/**
*	Initialize driver object list.
*	\param Valid driver objects in global driver section.
*	\ret Count of drivers initialized.
*/
size_t _BlFwInitDriverList (void) {

	unsigned char* devStart;
	unsigned char* devEnd;

	/* initialize start and end */
	devStart = (unsigned char*) &_DevEntryStart;
	devEnd   = (unsigned char*) &_DevEntryEnd;

	/* skip past _DevEntryStart */
	devStart += 4;

	/* scan device object list */
	for ( ; devStart < devEnd; devStart++) {

		/* watch for end of driver list. */
		if (_driverCount > nblDriverObject_MAX) {
			break;
		}

		if (*devStart) {

			nblDriverObject* driver;
			unsigned int       magic;

			/* test if this is a valid entry */
			magic = *(unsigned int*) devStart;
			if (magic != NBL_DRIVER_MAGIC)
				continue;

			/* get the driver object */
//			devStart - sizeof(unsigned int);
			driver = (nblDriverObject*) devStart;

			/* test if this is a valid object */
			if (BlrStrLength (driver->name) > 31)
				continue;
			if (driver->dependencyCount > 10)
				continue;

			/* initialize driver list */
			BlrCopyMemory (&_drivers[_driverCount++], driver, sizeof(nblDriverObject));

			/* jump past this entry and continue scanning */
			(unsigned char*)devStart += sizeof(nblDriverObject);
			devStart--;
		}
	}

	return _driverCount;
}

/*
	Visited list
*/

#define NBL_FW_MAX_VISITED_LIST 32

/* visited list */
nblDriverObject*  _visited[NBL_FW_MAX_VISITED_LIST];
static unsigned int _lastVisited = 0;

/**
*	Clear visited list.
*/
void _BlFwClearVisited () {
	unsigned int c;
	for (c=0; c < NBL_FW_MAX_VISITED_LIST; c++)
		_visited [c] = 0;
	_lastVisited = 0;
}

/**
*	Add node to visited list.
*	\param node Driver object to add
*	\ret TRUE if success, FALSE on error.
*/
BOOL _BlFwAddVisited (IN nblDriverObject* node) {
	if (_lastVisited > NBL_FW_MAX_VISITED_LIST-1) {
		return FALSE;
	}
	_visited[_lastVisited++] = node;
	return TRUE;
}

/**
*	Test if node has already been visited.
*	\param node Driver object.
*	\ret TRUE if in visited list, FALSE if not.
*/
BOOL _BlFwIsVisited (IN nblDriverObject* node) {
	unsigned int c;
	for (c=0; c < NBL_FW_MAX_VISITED_LIST; c++) {
		if (node == _visited[c])
			return TRUE;
	}
	return FALSE;
}

/**
*	Give a parent a child.
*	\param parent Parent object.
*	\param child Child object.
*	\ret TRUE if success, FALSE on error.
*/
BOOL _BlFwAddDriverNode (IN OUT nblDriverObject* parent, IN nblDriverObject* child) {

	if (!parent || !child)
		return FALSE;

	if (parent->node.childCount < 10) {
		parent->node.children [parent->node.childCount++] = (nblDriverObject *) child;
		child->node.parent = (nblDriverObject *) parent;
		return TRUE;
	}
	return FALSE;
}

/**
*	Build driver tree.
*	\param node Root driver object.
*/
void _BlFwInitDriverTree (IN nblDriverObject* node) {

/* max size of queue. */
#define BL_FW_QUEUE_MAX 32

	nblDriverObject* queue[BL_FW_QUEUE_MAX];
	unsigned int       head;
	unsigned int       tail;
	nblDriverObject*   root;

	/* get root driver object. */
	root = (nblDriverObject*) BlRootDriverObject ();

	/* breadth first search. */

	/* initialize queue */
	head = -1;
	tail = -1;

	/* visit current node */
	_BlFwAddVisited (node);
	
	/* and enqueue it */
	queue [ ++tail % BL_FW_QUEUE_MAX ] = node;

	while (head != tail) {
		nblDriverObject* parent;
		nblDriverObject* child;
		unsigned int       dependCount;

		/* deque */
		parent = queue [++head % BL_FW_QUEUE_MAX];

		if (parent->dependencyCount == 0) {
			_BlFwAddDriverNode (root, parent);
		}

		for (dependCount = 0; dependCount < parent->dependencyCount; dependCount++) {
			child = _BlFwGetDriverObject (parent->dependencies [dependCount].name);
			_BlFwAddDriverNode (child, parent);
			if (!_BlFwIsVisited(child)) {

				/* visit current node */
				_BlFwAddVisited (child);
				
				/* and enqueue it */
				queue [ ++tail % BL_FW_QUEUE_MAX ] = child;
			}
		}
	}
}

/**
*	Initialize driver.
*	\param node Pointer to driver object to initialize.
*/
void _BlFwInitDriver (IN nblDriverObject* node) {

	unsigned int c;
	status_t s;
	nblDriverObject* dependencies[nblDriverObject_MAX];

	/* build dependency list. */
	for (c=0; c<node->dependencyCount; c++) {
		dependencies[c] = _BlFwGetDriverObject (node->dependencies[c].name );
	}

	/* call entry point. */
	if (node->entryPoint) {

		/* If no dependencies, give it root. */
		if (node->dependencyCount == 0) {

			dependencies [0] = BlRootDriverObject ();
			s = node->entryPoint( node, 1, dependencies );
		}
		else {

			s = node->entryPoint( node, node->dependencyCount, dependencies );
		}
	}
}

/**
*	Initialize drivers.
*	\param node Root driver object.
*/
void _BlFwInitDrivers (IN nblDriverObject* node) {

	nblDriverObject    dependencies[NBL_DRIVER_DEPEND_MAX];
	unsigned int       dependCount = 0;
	nblDriverObject*   root;

	/* get root driver. */
	root = (nblDriverObject*) BlRootDriverObject();

	/* test if node has been visited */
	if (_BlFwIsVisited(node))
		return;

	/* clear dependency list. */
	BlrZeroMemory (dependencies, sizeof(nblDriverObject) * NBL_DRIVER_DEPEND_MAX);

	/* for all dependencies */
	for (dependCount = 0; dependCount < node->dependencyCount; dependCount++) {

		nblDriverObject* dependency;

		/* if already visited, skip it */
		if (_BlFwIsVisited(node))
			continue;

		/* initialize dependency */
		dependency = _BlFwGetDriverObject ( node->dependencies[dependCount].name );
		_BlFwInitDrivers (dependency);

		/* if dependency has no associated driver object, assign it to root. */
		if (!dependency)
			dependency = root;

		/* copy to dependency list */
		BlrCopyMemory (&dependencies[dependCount],dependency, sizeof(nblDriverObject));
	}

	/* visit node */
	_BlFwAddVisited (node);

	/* initialize driver. */
	_BlFwInitDriver (node);
}

/**
*	Call driver entry points.
*	\param loadType Load type of drivers to call
*/
void NBL_CALL BlCallDriverEntryPoints (nblDriverLoadType loadType) {
	unsigned int c;
	for (c = 0; c < _driverCount; c++) {
		if (_drivers[c].loadType == loadType) {
			_BlFwInitDrivers (&_drivers[c]);
		}
	}
}

/**
*	Initialize driver objects.
*	\ret TRUE if success, FALSE if error.
*/
BOOL NBL_CALL BlInitializeDrivers (void) {

	unsigned int     c;
	nblDriverObject* root;

	/* initialize driver list. */
	if (! _BlFwInitDriverList() ) {
		return FALSE;
	}

	/* clear visited list to prepare for next search. */
	_BlFwClearVisited();

	/* no drivers should have children yet. */
	for (c = 0; c < _driverCount; c++) {
		_drivers[c].node.childCount = 0;
	}

	/* initialize root. */
	root = (nblDriverObject*) BlRootDriverObject ();
	root->node.childCount = 0;

	/* build driver tree. */
	for (c = 0; c < _driverCount; c++) {
		if (!_BlFwIsVisited(&_drivers[c])) {
			_BlFwInitDriverTree (&_drivers[c]);
		}
	}

	/* clear visited list. */
	_BlFwClearVisited();

	/* initialize drivers. */
//	for (c = 0; c < _driverCount; c++) {
//		_BlFwInitDrivers (&_drivers[c]);
//	}

	/* clear visited list to prepare for next search. */
	_BlFwClearVisited();
	return TRUE;
}

/* Root driver object. */
nblDriverObject _root = {
	NBL_DRIVER_MAGIC,
	0,
	NBL_DRV_LOAD_CORE,
	NBL_DRV_CLASS_ROOT,
	"root",
	0,
	0,
	0
};

/**
*	Get root driver service.
*	\ret Handle to driver root device.
*/
nblDriverObject* NBL_CALL BlRootDriverObject (void) {

	return &_root;
}

BOOL BlDriverIterate (NBL_DRV_ITERATE_HOOK_FUNC hook, void* ex) {

	unsigned int i;

	for (i = 0; i < _driverCount; i++) {
		if (hook (&_drivers[i], ex) == TRUE) {
			return TRUE;
		}
	}
	return FALSE;
}
