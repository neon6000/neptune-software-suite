/************************************************************************
*
*	Support multiboot standard.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* Multiboot applications can call NBOOT or NBOOT.EXE directly. */

#include "nbl.h"

/* load base must be at 1MB. */
#define   LOADBASE                    0x100000
#define   ALIGN                       0x400
#define   HEADER_ADDRESS              LOADBASE+ALIGN
#define   MULTIBOOT_HEADER_MAGIC      0x1BADB002
#define   MULTIBOOT_HEADER_FLAGS      0x00010003
#define   STACK_SIZE                  0x4000    
#define   CHECKSUM                    -(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS)

/* relocated base address. */
#define   RELBASE                     0x20000

typedef struct _MULTIBOOT_INFO {
   uint32_t magic;
   uint32_t flags;
   uint32_t checksum;
   uint32_t headerAddr;
   uint32_t loadAddr;
   uint32_t loadEndAddr;
   uint32_t bssEndAddr;
   uint32_t entryPoint;
   uint32_t modType;
   uint32_t width;
   uint32_t height;
   uint32_t depth;
}MULTIBOOT_INFO, *PMULTIBOOT_INFO;

extern void _BlFwStartup (void);

PRIVATE
void
_BlMbRelocate (void);

PRIVATE
void
_BlMbStartup (void);

/**
*	_BlMbRelocate
*
*	Relocates NBOOT.
*/
PRIVATE
void
_BlMbRelocate (void) {

	/* must be position independent. */

}

/**
*	BlMbStartup
*
*	Multiboot Entry Point.
*/
PRIVATE
void
_BlMbStartup (void) {

	/* must be position independent. */

	/* relocate NBOOT. */
	_BlMbRelocate ();

	/* call firmware entry point. */
	_BlFwStartup ();
}

/* multiboot information header. This must be
4k aligned and < 8k within the NBOOT image. This
section will always meet that criteria. */

#pragma code_seg(".a$0") __declspec(allocate(".a$0"))
MULTIBOOT_INFO _MultibootInfo = {
   MULTIBOOT_HEADER_MAGIC,
   MULTIBOOT_HEADER_FLAGS,
   CHECKSUM,
   HEADER_ADDRESS,
   LOADBASE,
   0, /* load end address */
   0, /* bss end address */
   _BlMbStartup
};
#pragma comment(linker, "/merge:.text=.a")
