
#include <nbl.h>


//https://msdn.microsoft.com/en-us/library/windows/desktop/ms724934(v=vs.85).aspx


#define EFI_SUCCESS             0
#define EFI_LOAD_ERROR          ( 1 | (1UL << (BITS_PER_LONG-1)))
#define EFI_INVALID_PARAMETER   ( 2 | (1UL << (BITS_PER_LONG-1)))
#define EFI_UNSUPPORTED         ( 3 | (1UL << (BITS_PER_LONG-1)))
#define EFI_BAD_BUFFER_SIZE     ( 4 | (1UL << (BITS_PER_LONG-1)))
#define EFI_BUFFER_TOO_SMALL    ( 5 | (1UL << (BITS_PER_LONG-1)))
#define EFI_NOT_FOUND           (14 | (1UL << (BITS_PER_LONG-1)))

typedef unsigned long efi_status_t;
typedef uint8_t efi_bool_t;
typedef uint16_t efi_char16_t;               /* UNICODE character */

typedef struct {
	uint8_t b[16];
} nblEfiGUID;

typedef struct {
	uint64_t signature;
	uint32_t revision;
	uint32_t headersize;
	uint32_t crc32;
	uint32_t reserved;
} nblEfiTableHeader;

typedef struct {
	uint32_t type;
	uint32_t pad;
	uint64_t phys_addr;       
	uint64_t virt_addr;
	uint64_t num_pages;
	uint64_t attribute;
} nblEfiMemoryDescr;

typedef int (*efi_freemem_callback_t) (uint64_t start, uint64_t end, void *arg);

typedef struct {
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	uint8_t pad1;
	uint32_t nanosecond;
	sint16_t timezone;
	uint8_t daylight;
	uint8_t pad2;
} nblEfiTimeDescr;

typedef struct {
	uint32_t resolution;
	uint32_t accuracy;
	uint8_t sets_to_zero;
} nblEfiTimeCaps;

typedef struct {
	nblEfiTableHeader hdr;
	unsigned long get_time;
	unsigned long set_time;
	unsigned long get_wakeup_time;
	unsigned long set_wakeup_time;
	unsigned long set_virtual_address_map;
	unsigned long convert_pointer;
	unsigned long get_variable;
	unsigned long get_next_variable;
	unsigned long set_variable;
	unsigned long get_next_high_mono_count;
	unsigned long reset_system;
} nblEfiRuntimeServices;

typedef efi_status_t efi_get_time_t (nblEfiTimeDescr *tm, nblEfiTimeCaps *tc);

typedef efi_status_t efi_set_time_t (nblEfiTimeDescr *tm);

typedef efi_status_t efi_get_wakeup_time_t (efi_bool_t *enabled, efi_bool_t *pending,
                                     nblEfiTimeDescr *tm);

typedef efi_status_t efi_set_wakeup_time_t (efi_bool_t enabled, nblEfiTimeDescr *tm);

typedef efi_status_t efi_get_variable_t (efi_char16_t *name, nblEfiGUID *vendor, uint32_t *attr,
                                  unsigned long *data_size, void *data);

typedef efi_status_t efi_get_next_variable_t (unsigned long *name_size, efi_char16_t *name,
                                     nblEfiGUID *vendor);

typedef efi_status_t efi_set_variable_t (efi_char16_t *name, nblEfiGUID *vendor, 
                                  unsigned long attr, unsigned long data_size, 
                                  void *data);

typedef efi_status_t efi_get_next_high_mono_count_t (uint32_t *count);

typedef void efi_reset_system_t (int reset_type, efi_status_t status,
                          unsigned long data_size, efi_char16_t *data);

typedef efi_status_t efi_set_virtual_address_map_t (unsigned long memory_map_size,
                                         unsigned long descriptor_size,
                                         uint32_t descriptor_version,
                                         nblEfiMemoryDescr *virtual_map);

typedef struct {
	nblEfiGUID guid;
	unsigned long table;
} nblEfiConfigTable;

#define EFI_SYSTEM_TABLE_SIGNATURE ((u64)0x5453595320494249ULL)

typedef struct {
	 nblEfiTableHeader      hdr;
	 wchar_t*               fwVender;
	 uint32_t               fwRevision;
	 handle_t               consoleInHandle;
	 handle_t               consoleIn;
	 handle_t               consoleOutHandle;
	 handle_t               consoleOut;
	 handle_t               standardErrorHandle;
	 handle_t               standardError;
	 nblEfiRuntimeServices* runtimeServices;
	 unsigned long          bootServices;
	 unsigned long          NumberOfTableEntries;
	 unsigned long          ConfigurationTable;
} nblEfiSystemTable;

struct nblEfiMemoryMap {
	void *phys_map;
	void *map;
	void *map_end;
	int nr_map;       
	unsigned long desc_version;
	unsigned long desc_size;
};

extern struct efi {
	nblEfiSystemTable *systab;      /* EFI system table */
	unsigned long mps;              /* MPS table */
	unsigned long acpi;             /* ACPI table  (IA64 ext 0.71) */
	unsigned long acpi20;           /* ACPI table  (ACPI 2.0) */
	unsigned long smbios;           /* SM BIOS table */
	unsigned long sal_systab;       /* SAL system table */
	unsigned long boot_info;        /* boot info table */
	unsigned long hcdp;             /* HCDP table */
	unsigned long uga;              /* UGA table */
	unsigned long uv_systab;        /* UV system table */
	efi_get_time_t *get_time;
	efi_set_time_t *set_time;
	efi_get_wakeup_time_t *get_wakeup_time;
	efi_set_wakeup_time_t *set_wakeup_time;
	efi_get_variable_t *get_variable;
	efi_get_next_variable_t *get_next_variable;
	efi_set_variable_t *set_variable;
	efi_get_next_high_mono_count_t *get_next_high_mono_count;
	efi_reset_system_t *reset_system;
	efi_set_virtual_address_map_t *set_virtual_address_map;
} efi;

extern int BlMain (void);

NBL_API void NBL_CALL BlFwEntryPoint (IN handle_t imageBase, IN nblEfiSystemTable* fw) {


	BlMain ();
}

