/************************************************************************
*
*	Implements exit services.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nbl.h"

/* Very simple queue for exit callbacks. */

#define NBL_EXIT_QUEUE_SIZE 10

STATIC NBL_EXIT_FUNC _nblExitQueue [ NBL_EXIT_QUEUE_SIZE ];

STATIC index_t _nblExitQueueIndex = 0;

STATIC NBL_EXIT_FUNC _nblExitQueueTop (void) {
	if (_nblExitQueueIndex == 0)
		return 0;
	return _nblExitQueue [_nblExitQueueIndex-1];
}

STATIC status_t _nblExitQueuePush (NBL_EXIT_FUNC p) {
	if (_nblExitQueueIndex == NBL_EXIT_QUEUE_SIZE)
		return NBL_STAT_BUFFER_OVERFLOW;
	_nblExitQueue [_nblExitQueueIndex++] = p;
	return NBL_STAT_SUCCESS;
}

STATIC status_t _nblExitQueuePop (void) {
	if (_nblExitQueueIndex == 0) {
		if (_nblExitQueue [_nblExitQueueIndex] == 0)
			return NBL_STAT_BUFFER_UNDERFLOW;
		else
			return NBL_STAT_SUCCESS;
	}
	_nblExitQueue [--_nblExitQueueIndex] = 0;
	return NBL_STAT_SUCCESS;
}

STATIC BOOL _nblExitQueueIsEmpty (void) {
	return (_nblExitQueueIndex == 0);
}

/**
*	Registers function called at exit
*	\param p Pointer to function
*	\ret Status code
*/
NBL_API status_t NBL_CALL BlAtExit (IN NBL_EXIT_FUNC p) {

	/* add method and return status. */
	return _nblExitQueuePush(p);
}

/**
*	Exit boot library.
*/
void BlExit (void) {

	/* execute callback methods in reverse order. */
	while (! _nblExitQueueIsEmpty()) {
		NBL_EXIT_FUNC p = _nblExitQueueTop();
		if (p) p();
		_nblExitQueuePop();
	}
}

/**
*	Boot application entry point.
*	\param Pointer to NBLC Environment Block
*	\ret Sttus code
*/
extern status_t NBL_CALL BlMain (void);

/**
*	Starts boot application and library services
*	\ret Boot application result
*/
status_t BlStartBootApplication (void) {

	/* initialize NBLC driver tree and NBLR driver root. */
	BlInitializeDrivers ();
	BlrInitialize ( BlRootDriverObject () );

	/* initialize core drivers. */
	BlCallDriverEntryPoints (NBL_DRV_LOAD_CORE);

	/* create heap. */
	BlrInitializeHeap ();

	/* initialize core 2 drivers. */
	BlCallDriverEntryPoints (NBL_DRV_LOAD_CORE2);

	/* Call boot application. */
	if (BlMain () != NBL_STAT_SUCCESS) {

		/* Attempt to call debugger. */
		BlrDbgBreak ();
	}

	/* Call exit services. */
	BlExit ();
	return NBL_STAT_SUCCESS;
}

/* standard streams. */
nblFile* nbl_stdin  = NULL;
nblFile* nbl_stderr = NULL;
nblFile* nbl_stdout = NULL;
nblFile* nbl_wstdout = NULL;

/* Required by MSVC runtime. */
int _fltused = 1;
