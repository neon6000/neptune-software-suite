/************************************************************************
*
*	NBL Device Manager.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>

nblDeviceObject* _blDeviceListHead = 0;

/**
*	Iterate device list.
*	\param type Device type
*	\param hook Iterator hook
*/
STATIC void NBL_CALL BlDmIterate (IN int type, NBL_DEV_ITERATE_HOOK_FUNC hook, void* ex) {

	nblDeviceObject* cur = _blDeviceListHead;

	while (cur) {
		if ((cur->type == type) || (type==0xffffffff)) {
			if (hook (cur, ex) == TRUE)
				return;
		}
		cur = cur->next;
	}
}

/**
*	Unregister device
*	\param dev Pointer to device object
*/
STATIC void NBL_CALL BlDmUnregister (IN nblDeviceObject* dev) {

	nblDeviceObject* cur = _blDeviceListHead;

	/* head device. */
	if (cur == dev) {
		_blDeviceListHead = cur->next;
		return;
	}

	/* search for device. */
	while (cur->next) {

		/* remove it. */
		if (cur->next == dev) {
			cur->next = cur->next->next;
			return;
		}
		cur = cur->next;
	}
}

/**
*	Register device
*	\param dev Pointer to device object
*	\ret Status code
*/
STATIC status_t NBL_CALL BlDmRegister (IN nblDeviceObject* dev) {

	nblDeviceObject* cur = _blDeviceListHead;

	/* head device. */
	if (! _blDeviceListHead) {
		_blDeviceListHead = dev;
		dev->next = 0;
		return NBL_STAT_SUCCESS;
	}

	/* add to end. */
	while (cur->next)
		cur = cur->next;
	cur->next = dev;
	dev->next = 0;
	return NBL_STAT_SUCCESS;
}

/**
*	Open device
*	\param name Pointer to device name
*	\ret Pointer to device object
*/
STATIC nblDeviceObject* NBL_CALL BlDmOpen (IN char* name) {

	nblDeviceObject* cur = _blDeviceListHead;

	if ( cur == NULL) {
		return NULL;
	}

	/* head device. */
	if ( BlrStrCompare (name, cur->name) == 0) {
		return cur;
	}

	/* search for device. */
	while (cur->next) {

		if ( BlrStrCompare (name, cur->next->name) == 0) {

			/* Should we send an OPEN request to cur->next? */
			return cur->next;
		/*	return cur; */
		}
		cur = cur->next;
	}

	return 0;
}

/**
*	Close device
*	\param dev Pointer to device object
*/
STATIC void NBL_CALL BlDmClose (IN nblDeviceObject* dev) {

	/* Should we send a CLOSE request to dev? */
}

/**
*	Debugger entry point.
*	\param self Pointer to own device object
*	\param dependencyCount Number of dependencies
*	\param dependencies Dependency list
*/
status_t BlDmEntryPoint (IN nblDriverObject* self,IN int dependencyCount,IN nblDriverObject* dependencies[]) {

	return NBL_STAT_SUCCESS;
}

/**
*	Request callback.
*	\param rpb Request Paramater Block
*	\ret Status code
*/
status_t BlDmRequest (IN nblMessage* rpb) {

	switch (rpb->type) {
		case NBL_DEV_REGISTER:
			{
				status_t result = BlDmRegister ( (nblDeviceObject *) rpb->NBL_DEV_REGISTER_OBJ);
				rpb->NBL_DEV_REGISTER_STAT = result;
				return NBL_STAT_SUCCESS;
			}
		case NBL_DEV_UNREGISTER:
			{
				BlDmUnregister ( (nblDeviceObject *) rpb->NBL_DEV_UNREGISTER_OBJ);
				return NBL_STAT_SUCCESS;
			}
		case NBL_DEV_ITERATE:
			{
				BlDmIterate (rpb->NBL_DEV_ITERATE_TYPE, (NBL_DEV_ITERATE_HOOK_FUNC) rpb->NBL_DEV_ITERATE_HOOK,
					(void*) rpb->NBL_DEV_ITERATE_EX);
				return NBL_STAT_SUCCESS;
			}
		case NBL_DEV_OPEN:
			{
				nblDeviceObject* result = BlDmOpen ((char*) rpb->NBL_DEV_OPEN_NAME);
				rpb->NBL_DEV_OPEN_OUT = (int) result;
				return NBL_STAT_SUCCESS;
			}
		case NBL_DEV_CLOSE:
			{
				BlDmClose ( (nblDeviceObject *) rpb->NBL_DEV_CLOSE_OBJ);
				return NBL_STAT_SUCCESS;
			}
		default:
			return NBL_STAT_UNSUPPORTED;
	};
}

#define NBL_DM_VERSION 0

/**
*	Driver object.
*/
NBL_DRIVER_OBJECT_DEFINE _ndmDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_DM_VERSION,
	NBL_DRV_LOAD_CORE,
	NBL_DRV_CLASS_DM,
	"dm",
	BlDmEntryPoint,
	BlDmRequest,
	0
};

/**
*	Register device object.
*	\param dev Pointer to device object
*	\ret Status code.
*/
NBL_API status_t NBL_CALL BlrRegisterDevice (IN nblDeviceObject* dev) {

	nblMessage       rpb;
	nblDriverObject* dm;

	/* NBLR needs to open DM. */
	dm = BlrOpenDriver ("root/dm");
	if (!dm) {
		BlrError("BlrRegisterDevice: cannot open 'root/dm'");
		return NBL_STAT_DRIVER;
	}

	rpb.source = 0;
	rpb.type   = NBL_DEV_REGISTER;
	rpb.NBL_DEV_REGISTER_OBJ = dev;

	dm->request (&rpb);
	return rpb.NBL_DEV_REGISTER_STAT;
}

/**
*	Unregister device object.
*	\param dev Pointer to device object
*/
NBL_API void NBL_CALL BlrUnregisterDevice (IN nblDeviceObject* dev) {

	nblMessage       rpb;
	nblDriverObject* dm;

	/* NBLR needs to open DM. */
	dm = BlrOpenDriver ("root/dm");
	if (!dm) {
		BlrError("BlrUnregisterDevice: cannot open 'root/dm'");
		return;
	}

	rpb.source = 0;
	rpb.type   = NBL_DEV_UNREGISTER;
	rpb.NBL_DEV_UNREGISTER_OBJ = dev;

	dm->request (&rpb);
}

/**
*	Iterate through device objects.
*	\param type Device type
*	\param hook The method should return TRUE to stop iterating, FALSE to continue.
*/
NBL_API void NBL_CALL BlrDeviceIterate (IN int type, IN NBL_DEV_ITERATE_HOOK_FUNC hook, void* ex) {

	nblMessage       rpb;
	nblDriverObject* dm;

	/* NBLR needs to open DM. */
	dm = BlrOpenDriver ("root/dm");
	if (!dm) {
		BlrError("BlrDeviceIterate: cannot open 'root/dm'");
		return;
	}

	rpb.source = 0;
	rpb.type   = NBL_DEV_ITERATE;
	rpb.NBL_DEV_ITERATE_TYPE = type;
	rpb.NBL_DEV_ITERATE_HOOK = (int)hook;
	rpb.NBL_DEV_ITERATE_EX   = (int)ex;

	dm->request (&rpb);
}

/**
*	Open device object.
*	\param name Name of device
*	\ret Pointer to device object or NULL
*/
NBL_API nblDeviceObject* NBL_CALL BlrOpenDevice (IN char* name) {

	nblMessage       rpb;
	nblDriverObject* dm;

	if (! name )
		return NULL;

	/* NBLR needs to open DM. */
	dm = BlrOpenDriver ("root/dm");

//	BlrPrintf("\n\rBlrOpenDevice DM at %x ", dm);

	if (!dm) {
		BlrError("BlrOpenDevice: cannot open 'root/dm'");
		return NULL;
	}

	rpb.source = 0;
	rpb.type   = NBL_DEV_OPEN;
	rpb.NBL_DEV_OPEN_NAME = (uint32_t) name;

	dm->request (&rpb);

	return (nblDeviceObject*) rpb.NBL_DEV_OPEN_OUT;
}

/**
*	Close device object.
*	\param dev Pointer to device object
*/
NBL_API void NBL_CALL BlrCloseDevice (IN nblDeviceObject* dev) {

	nblMessage       rpb;
	nblDriverObject* dm;

	if (! dev )
		return;

	/* NBLR needs to open DM. */
	dm = BlrOpenDriver ("root/dm");
	if (!dm) {
		BlrError("BlrCloseDevice: cannot open 'root/dm'");
		return;
	}

	rpb.source = 0;
	rpb.type   = NBL_DEV_CLOSE;
	rpb.NBL_DEV_CLOSE_OBJ = dev;

	dm->request (&rpb);
}
