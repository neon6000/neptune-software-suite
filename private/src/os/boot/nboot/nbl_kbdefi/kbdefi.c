/************************************************************************
*
*	NBL EFI Keyboard Mapper Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* Maps EFI keyboard scancodes to NBL VK codes. */

#include <nbl.h>

/* need to somehow map NBL_VK_RETURN */

/* efi scan codes. */
enum {
	EFI_SC_NULL = 0,
	EFI_SC_UP,
	EFI_SC_DOWN,
	EFI_SC_RIGHT,
	EFI_SC_LEFT,
	EFI_SC_HOME,
	EFI_SC_END,
	EFI_SC_INSERT,
	EFI_SC_DELETE,
	EFI_SC_PGUP,
	EFI_SC_PGDOWN,
	EFI_SC_F1,
	EFI_SC_F2,
	EFI_SC_F3,	// same code (0xd) as ENTER key.
	EFI_SC_F4,
	EFI_SC_F5,
	EFI_SC_F6,
	EFI_SC_F7,
	EFI_SC_F8,
	EFI_SC_F9,
	EFI_SC_F10,
	EFI_SC_F11,
	EFI_SC_F12,
	EFI_SC_ESCAPE,
}EFI_SCANCODE;

/* maps virtual scan code to virtual key code.*/
STATIC
nblKey _vscToVkMap[] = {
	NBL_VK_EMPTY,
	NBL_VK_UP,
	NBL_VK_DOWN,
	NBL_VK_RIGHT,
	NBL_VK_LEFT,
	NBL_VK_HOME,
	NBL_VK_END,
	NBL_VK_INSERT,
	NBL_VK_DELETE,
	NBL_VK_PRIOR,
	NBL_VK_NEXT,
	NBL_VK_F1,
	NBL_VK_F2,
	NBL_VK_RETURN, // This also maps to	NBL_VK_F3.
	NBL_VK_F4,
	NBL_VK_F5,
	NBL_VK_F6,
	NBL_VK_F7,
	NBL_VK_F8,
	NBL_VK_F9,
	NBL_VK_F10,
	NBL_VK_F11,
	NBL_VK_F12,
	NBL_VK_ESCAPE, /* 24. */
	NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,
	NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,
	NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,
	NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,
	NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,
	NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,
	NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,
	0 /* 0x80. */
};

	nblKey  vk;
	uint8_t attributes;
	/*
		wch[0] => normal
		wch[1] => shift
		wch[2] => control
		wch[3] => special
	*/
	wchar_t wch[4];

/* maps virtual key code to wide-character name. */
STATIC
nblVkCharMap _vkCharMap[] = {

	/* alphabet */
	{'A', 0, {L'a', L'A'} },
	{'B', 0, {L'b', L'B'} },
	{'C', 0, {L'c', L'C'} },
	{'D', 0, {L'd', L'D'} },
	{'E', 0, {L'e', L'E'} },
	{'F', 0, {L'f', L'F'} },
	{'G', 0, {L'g', L'G'} },
	{'H', 0, {L'h', L'H'} },
	{'I', 0, {L'i', L'I'} },
	{'J', 0, {L'j', L'J'} },
	{'K', 0, {L'k', L'K'} },
	{'L', 0, {L'l', L'L'} },
	{'M', 0, {L'm', L'M'} },
	{'N', 0, {L'n', L'N'} },
	{'O', 0, {L'o', L'O'} },
	{'P', 0, {L'p', L'P'} },
	{'Q', 0, {L'q', L'Q'} },
	{'R', 0, {L'r', L'R'} },
	{'S', 0, {L's', L'S'} },
	{'T', 0, {L't', L'T'} },
	{'U', 0, {L'u', L'U'} },
	{'V', 0, {L'v', L'V'} },
	{'W', 0, {L'w', L'W'} },
	{'X', 0, {L'x', L'X'} },
	{'Y', 0, {L'y', L'Y'} },
	{'Z', 0, {L'z', L'Z'} },

	/* numbers. */
	{'1', 0, {L'1', L'!'} },
	{'2', 0, {L'2', L'@'} }, /* ctrl+2 generates NUL */
	{'3', 0, {L'3', L'#'} },
	{'4', 0, {L'4', L'$'} },
	{'5', 0, {L'5', L'%'} },
	{'6', 0, {L'6', L'^', 0, 0x1e /* RS */ } },
	{'7', 0, {L'7', L'&'} },
	{'8', 0, {L'8', L'*'} },
	{'9', 0, {L'9', L'('} },
	{'0', 0, {L'0', L')'} },

	/* specials. */
	{NBL_VK_OEM_PLUS,   0, {L'7', L'&'} },
	{NBL_VK_OEM_MINUS,  0, {L'-',L'_', 0, 0x1f /* US */ } },
	{NBL_VK_OEM1,       0, {L';', L':'} },
	{NBL_VK_OEM7,       0, {L'\'', L'\"'} },
	{NBL_VK_OEM3,       0, {L'`', L'~'} },
	{NBL_VK_OEM_COMMA,  0, {L',', L'<'} },
	{NBL_VK_OEM_PERIOD, 0, {L'.', L'>'} },
	{NBL_VK_OEM2,       0, {L'/', L'?'} },

	/* does not have shift states */
	{NBL_VK_TAB,      0,             {L'\t', L'\t', L'\t'} },
	{NBL_VK_ADD,      0,             {L'+', L'+',L'+'} },
	{NBL_VK_SUBTRACT, 0,             {L'-', L'-',L'-'} },
	{NBL_VK_DECIMAL,  0,             {L'.', L'.',L'.'} },
	{NBL_VK_MULTIPLY, 0,             {L'*', L'*',L'*'} },
	{NBL_VK_DIVIDE,   0,             {L'/', L'/', L'/'} },

	/* escapes */
	{NBL_VK_OEM4,     0,             {L'['  , L'{' , 0x1b /* ESC */ } },
	{NBL_VK_OEM5,     0,             {L'\\' , L'|' , 0x1c /* FS */  } },
	{NBL_VK_OEM6,     0,             {L']'  , L'}' , 0x1d /* GS */  } },
	{NBL_VK_102,      0,             {L'\\' , L'|' , 0x1c /* FS */  } },
	{NBL_VK_BACK,     0,             {0x8  , 0xb , 0x7F           } },
	{NBL_VK_ESCAPE,   0,             {0x1b , 0x1b, 0x1b           } },
	{NBL_VK_RETURN,   0,             {L'\r' , L'\r', L'\n'        } },
	{NBL_VK_SPACE,    0,             {' '  , ' ' , ' '            } },
	{NBL_VK_CANCEL,   0,             {0x3  , 0x3 , 0x3            } },
	{0,0}
};

/* maps scancodes to keynames. */
STATIC
nblVscStr _keynames[] = {
	{ EFI_SC_NULL,   L"Null" },
	{ EFI_SC_UP,     L"Up" },
	{ EFI_SC_DOWN,   L"Down" },
	{ EFI_SC_RIGHT,  L"Right" },
	{ EFI_SC_LEFT,   L"Left" },
	{ EFI_SC_HOME,   L"Home" },
	{ EFI_SC_END,    L"End" },
	{ EFI_SC_INSERT, L"Insert" },
	{ EFI_SC_DELETE, L"Delete" },
	{ EFI_SC_PGUP,   L"PageUp" },
	{ EFI_SC_PGDOWN, L"PageDown" },
	{ EFI_SC_F1,     L"F1" },
	{ EFI_SC_F2,     L"F2" },
	{ EFI_SC_F3,     L"F3" },
	{ EFI_SC_F4,     L"F4" },
	{ EFI_SC_F5,     L"F5" },
	{ EFI_SC_F6,     L"F6" },
	{ EFI_SC_F7,     L"F7" },
	{ EFI_SC_F8,     L"F8" },
	{ EFI_SC_F9,     L"F9" },
	{ EFI_SC_F10,    L"F10" },
	{ EFI_SC_F11,    L"F11" },
	{ EFI_SC_F12,    L"F12" },
	{ EFI_SC_ESCAPE, L"Escape" },
	{ 0, NULL }
};

STATIC
nblVscStr _keynamesEx[] = {
	{ 0, NULL },
};

STATIC
nblKeyTable _keymap = {
	_vkCharMap,
	_keynames,
	_keynamesEx,
	_vscToVkMap
};

STATIC
nblKeyTable*
KBDEFIGetTable (void) {
	/* returns keyboard map.*/
	return &_keymap;
}

STATIC
status_t
KBDEFIEntryPoint (IN nblDriverObject* self,
				 IN int dependencyCount,
				 IN nblDriverObject* dependencies[]) {

	/* nothing to do. */
	return NBL_STAT_SUCCESS;
}

/* request callback. */
STATIC
status_t
KBDEFIRequest (IN nblMessage* rpb) {

	rettype_t r;
	rpb->retval = 0;
	switch (rpb->type) {
		case NBL_KBDMAP_GET:
			r = (rettype_t) KBDEFIGetTable();
			break;
		default: return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_KBDEFI_VERSION 1

/* EFI keyboard map driver object. */
NBL_DRIVER_OBJECT_DEFINE _nKBDEFI = {
	NBL_DRIVER_MAGIC,
	NBL_KBDEFI_VERSION,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_OTHER,
	"kbdefi",
	KBDEFIEntryPoint,
	KBDEFIRequest,
	0
};
