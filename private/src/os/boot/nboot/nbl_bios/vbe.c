/************************************************************************
*
*	NBL EFI Graphics Output Protocol (GOP) Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "bios.h"

/*** PRIVATE DECLARATIONS ***/

/* self reference. */
STATIC nblDriverObject* _self = 0;

/* display device object. */
NBL_DRIVER_OBJECT_DEFINE _nBiosVbeDriverObject;
STATIC nblDeviceObject _vbeDeviceObject = {
	NBL_DEVICE_DISPLAY_CONTROLLER,
	NBL_DEVICE_CONTROLLER_CLASS,
	0, &_nBiosVbeDriverObject, 0, 0, "video", 0
};

/* VBE mode number */
typedef unsigned short modeNum;

/**
* VBE far pointer
* When functions are called via the real mode INT 10h software interrupt, a 
* vbeFarPtr' will be a real mode segment:offset style pointer to a memory location
* below the 1Mb system memory boundary
*
* When functions are called via the protected mode entry point, a `vbeFarPtr' will
* be a valid 16-bit protected mode selector:offset style pointer, the selector of
* which may point to the base of the protected mode BIOS image loaded in memory, user
* data passed in to the protected mode BIOS or to the information blocks passed in to
* the protected mode BIOS
*/
typedef uint32_t vbeFarPtr;

/**
* VBE palette entry
*/
typedef struct _VBE_PALETTE_ENTRY {
	uint8_t blue;
	uint8_t green;
	uint8_t red;
	uint8_t alignment;
}VBE_PALETTE_ENTRY, *PVBE_PALETTE_ENTRY;

/**
* VBE CRT Information Block
*/
typedef struct _VBE_CRT_INFO {
	uint16_t horizontalTotal;
	uint16_t horizontalSyncStart;
	uint16_t horizontalSyncEnd;
	uint16_t scanLineTotal;
	uint16_t verticalSyncStart;
	uint16_t verticalSyncEnd;
	uint8_t  flags;
	uint32_t pixelClock;
	uint16_t refreshRate; /* Must be pixel_clock / (HTotal * VTotal) */
	uint8_t  reserved[40];
}VBE_CRT_INFO, *PVBE_CRT_INFO;

/**
* VBE Flat Panel Information Block
*/
typedef struct _VBE_FLAT_PANEL_INFO {
	uint16_t horizontalSize;
	uint16_t berticalSize;
	uint16_t panelType;
	uint8_t  redBpp;
	uint8_t  greenBpp;
	uint8_t  blueBpp;
	uint8_t  reservedBpp;
	uint32_t reservedOffscreenMemSize;
	uint32_t reservedOffscreenMemPtr;
	uint8_t  reserved[14];
}VBE_FLAT_PANEL_INFO, *PVBE_FLAT_PANEL_INFO;

/**
*	vbe mode info block
*/
typedef struct _VBE_INFO_BLOCK {
	uint8_t  signiture[4];
	uint16_t version;
	uint32_t oemStrPtr;
	uint32_t caps;
	uint32_t videoModesPtr;
	uint16_t totalMemory;
	uint16_t oemSoftwareRev;
	uint32_t oemVenderNamePtr;
	uint32_t oemProductNamePtr;
	uint32_t oemProductRevPtr;
	uint8_t  reserved [222];
	uint8_t  oemData [256];
}VBE_INFO_BLOCK, *PVBE_INFO_BLOCK;

STATIC VBE_INFO_BLOCK _vbeControllerInfo;

/**
*	Vbe mode info block
*/
typedef struct _VBE_MODE_INFO {
	uint16_t modeAttrib;
	uint8_t winAttribA;
	uint8_t winAttribB;
	uint16_t winGranularity;
	uint16_t winSize;
	uint16_t winSegmentA;
	uint16_t winSegmentB;
	uint32_t winFuncPtr;
	uint16_t bytesPerScanLine;
	/**
	*	vbe 1.2 or higher fields
	*/
	uint16_t resolutionX;
	uint16_t resolutionY;
	uint8_t charSizeX;
	uint8_t charSizeY;
	uint8_t numberOfPlanes;
	uint8_t bitsPerPixel;
	uint8_t numberOfBanks;
	uint8_t memoryModel;
	uint8_t bankSize;
	uint8_t numberOfImagePages;
	uint8_t reserved;
	/**
	*	direct color fields
	*/
	uint8_t maskSizeRed;
	uint8_t fieldPositionRed;
	uint8_t maskSizeGreen;
	uint8_t fieldPositionGreen;
	uint8_t maskSizeBlue;
	uint8_t fieldPositionBlue;
	uint8_t maskSizeRsvd;
	uint8_t fieldPositionRsvd;
	uint8_t directColorModeInfo;
	/**
	*	vbe 2.0 and higher fields
	*/
	uint32_t physBasePtr;
	uint32_t reserved2;
	uint16_t reserved3;
	/**
	*	vbe 3.0 and higher fields
	*/
	uint16_t linBytesPerScanLine;
	uint8_t bnkNumberOfImagePages;
	uint8_t linNumberOfImagePages;
	uint8_t linRedMaskSize;
	uint8_t linRedFieldPosition;
	uint8_t linGreenMaskSize;
	uint8_t linGreenFieldPosition;
	uint8_t linBlueMaskSize;
	uint8_t linBlueFieldPosition;
	uint8_t linRsvdMaskSize;
	uint8_t linRsvdFieldPosition;
	uint32_t maxPixelClock;
	/**
	*	reserved, structure must be 256 bytes
	*/
	uint8_t filler [189];
}VBE_MODE_INFO, *PVBE_MODE_INFO;

/**
* Mode attributes (vbeModeInfo.modeAttrib) flags
*/
#define VBE_MODEATTR_SUPPORTED                 (1 << 0)
#define VBE_MODEATTR_RESERVED_1                (1 << 1)
#define VBE_MODEATTR_BIOS_TTY_OUTPUT_SUPPORT   (1 << 2)
#define VBE_MODEATTR_COLOR                     (1 << 3)
#define VBE_MODEATTR_GRAPHICS                  (1 << 4)
#define VBE_MODEATTR_VGA_COMPATIBLE            (1 << 5)
#define VBE_MODEATTR_VGA_WINDOWED_AVAIL        (1 << 6)
#define VBE_MODEATTR_LFB_AVAIL                 (1 << 7)
#define VBE_MODEATTR_DOUBLE_SCAN_AVAIL         (1 << 8)
#define VBE_MODEATTR_INTERLACED_AVAIL          (1 << 9)
#define VBE_MODEATTR_TRIPLE_BUF_AVAIL          (1 << 10)
#define VBE_MODEATTR_STEREO_AVAIL              (1 << 11)
#define VBE_MODEATTR_DUAL_DISPLAY_START        (1 << 12)

/**
* Memory model (vbeModeInfo.memoryModel) flags
*/
#define VBE_MEMORY_MODEL_TEXT           0x00
#define VBE_MEMORY_MODEL_CGA            0x01
#define VBE_MEMORY_MODEL_HERCULES       0x02
#define VBE_MEMORY_MODEL_PLANAR         0x03
#define VBE_MEMORY_MODEL_PACKED_PIXEL   0x04
#define VBE_MEMORY_MODEL_NONCHAIN4_256  0x05
#define VBE_MEMORY_MODEL_DIRECT_COLOR   0x06
#define VBE_MEMORY_MODEL_YUV            0x07

/**
*	vbe signiture
*/
#define VBE_INFOBLOCK_SIG "VESA"

/**
*	mode list terminator
*/
#define VBE_MODELIST_END 0xffff

/**
*	status codes
*/
#define VBE_STATUS_OK 0

/* Initial mode */
STATIC uint16_t _vbeInitialMode = 0;

/* Current mode. Initial is 3 as its PC startup text mode */
STATIC uint16_t _vbeCurrentMode = 3;

/* Mode list */
STATIC uint16_t* _vbeModeList = 0;

/* Current linear frame buffer. */
STATIC uint32_t _vbeCurrentLFB = 0;

PRIVATE void* real2pm (IN vbeFarPtr p);
PRIVATE int VbeGetModeInfo (IN int mode, OUT PVBE_MODE_INFO pModeInfo);
PRIVATE int VbeGetDisplayMode (OUT unsigned int* mode);
PRIVATE int VbeSetDisplayMode (IN unsigned int mode, OPTIONAL IN PVBE_CRT_INFO ctrlInfo);
PRIVATE int VbeGetSetDacPaletteWidth (IN int set, IN uint8_t* bitsPerCol);
PRIVATE int VbeSetMemoryWindow (IN uint32_t window, IN uint32_t loc);
PRIVATE int VbeGetMemoryWindow (IN uint32_t window, IN uint32_t* loc);
PRIVATE int VbeSetScanLineLength (IN int length);
PRIVATE int VbeGetScanLineLength (OUT int* length);
PRIVATE int VbeSetDisplayStart (IN int x, IN int y);
PRIVATE int VbeGetDisplayStart (OUT int* x, OUT int* y);
PRIVATE int VbeGetPModeInterface (OUT uint16_t* seg, OUT uint16_t* off, OUT uint16_t* length);
PRIVATE int VbeGetControlInfo (OUT PVBE_INFO_BLOCK buffer);
PRIVATE int VbeGetCapsDDC (OUT uint8_t* level);
PRIVATE int VbeGetFlatPanelInfo (OUT PVBE_FLAT_PANEL_INFO info);
PRIVATE int VbeDetectSupport (OUT PVBE_INFO_BLOCK buffer);
PRIVATE int BlVbeSetPalette (IN int numEnteries, IN int start, IN addr_t palette);
PRIVATE int BlVbeGetEDID (OUT addr_t edid);
PRIVATE void VbeToSurface (int mode, PVBE_MODE_INFO vbe, nblVideoMode* disp);
PRIVATE status_t VbeQueryModes (IN NBL_QUERY_MODE_CALLBACK callback, IN void* ex);
PRIVATE status_t BlVbeInitialize (void);
PRIVATE int BlVbeSetPage (int page);
PRIVATE status_t BlVbeSetMode (IN int width, IN int height, IN int bpp);
PRIVATE status_t BlVbeGetMode (IN nblVideoMode* out);
PRIVATE status_t BlVbeShutdown (void);
PRIVATE status_t VbeEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]);
PRIVATE status_t VbeSetVideoMode (IN uint32_t width, IN uint32_t height, IN uint32_t depth);
STATIC status_t VbeGetVideoMode (OUT nblVideoMode* mode);
PRIVATE status_t VbeCreateSurface (IN OUT nblSurface* out);
PRIVATE status_t VbeRequest (IN nblMessage* rpb);

/*** PRIVATE DEFINITIONS ***/

/**
*	real2pm
*
*	Description : Convert VBE SEG:OFF far ptr to protected mode ptr.
*
*	Input : p 16:16 Real Mode Far Pointer.
*
*	Output : 32 bit protected mode pointer.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void* real2pm (vbeFarPtr p) {

	return (void *) ((((unsigned long) p & 0xFFFF0000) >> 12UL)
		+ ((unsigned long) p & 0x0000FFFF));
}

/**
*	VbeGetModeInfo
*
*	Description : Return video mode information.
*
*	Input : mode - VBE mode number.
*           pModeInfo - OUT mode information.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeGetModeInfo (IN int mode, OUT PVBE_MODE_INFO pModeInfo) {

	INTR in, out;
	void* addr=0;

	in.eax.val = 0x4f01;
	in.ecx.val = mode;
	in.es = SEG ((unsigned int) pModeInfo);
	in.edi.val = OFFSET ( (unsigned int) pModeInfo);

	/* call BIOS INT 10h function 4f01*/
	io_services (0x10, &in, &out);
	return _ah(out.eax);
}

/**
*	VbeGetDisplayMode
*
*	Description : Return current display mode.
*
*	Input : mode - OUT display mode.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeGetDisplayMode (OUT unsigned int* mode) {

	INTR	in, out;

	/* call BIOS INT 10h function 4f03*/
	in.eax.val = 0x4f03;
	io_services (0x10, &in, &out);
	*mode = out.ebx.val & 0x00ff;
	return _ah (out.eax);
}

/**
*	VbeSetDisplayMode
*
*	Description : Set display mode.
*
*	Input : mode - VBE mode number to set.
*           ctrlInfo - Controller information block.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeSetDisplayMode (IN unsigned int mode, OPTIONAL IN PVBE_CRT_INFO ctrlInfo) {

	INTR in, out;

	in.eax.val = 0x4f02;
	in.ebx.val = mode;

	/* ES:DI->ctrlInfo */
	in.es = SEG ((int) ctrlInfo);
	in.edi.val = OFFSET ((int) ctrlInfo);

	/* call BIOS INT 10h function 4f02 */
	io_services (0x10, &in, &out);
	return _ah (out.eax);
}

/**
*	VbeGetSetDacPaletteWidth
*
*	Description : Set display mode.
*
*	Input : set - TRUE (1) sets the entry, FALSE (0) gets the entry.
*           bitsPerCol - Bits per color
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeGetSetDacPaletteWidth (IN int set, IN OUT uint8_t* bitsPerCol) {

	INTR	in, out;

	in.eax.val = 0x4f08;
	_bl (in.ebx) = (set) ? 1 : 0;
	_bh (in.ebx) = *bitsPerCol;

	/* call BIOS INT 10h function 4f08 */
	io_services (0x10, &in, &out);
	*bitsPerCol = _bh (out.ebx);
	return _ah (out.eax);
}

/**
*	VbeSetMemoryWindow
*
*	Description : Set display mode.
*
*	Input : window - Window number to set.
*           loc - Location in Granularity Units.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeSetMemoryWindow (IN uint32_t window, IN uint32_t loc) {

	INTR	in, out;

	in.eax.val = 0x4f05;
	//BL = window BH = 0 (00h select video memory window)
	in.ebx.val = window & 0x00ff;
	in.edx.val = loc;

	/* call BIOS INT 10h function 4f05 */
	io_services (0x10, &in, &out);
	return _ah (out.eax);
}

/**
*	VbeGetMemoryWindow
*
*	Description : Query memory window information.
*
*	Input : window - Window number to query.
*           loc - OUT Location in Granularity Units.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeGetMemoryWindow (IN uint32_t window, IN uint32_t* loc) {

	INTR	in, out;

	in.eax.val = 0x4f05;
	//BL = window BH = 1 (01h get video memory window)
	in.ebx.val = (window & 0x00ff) | 0x100;

	/* call BIOS INT 10h function 4f05 */
	io_services (0x10, &in, &out);
	*loc = out.edx.val & 0x00ff;
	return _ah (out.eax);
}

/**
*	VbeSetScanLineLength
*
*	Description : Set Scan Line length.
*
*	Input : length - Scan line length.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeSetScanLineLength (IN int length) {

	INTR	in, out;

	/* call BIOS INT 10h function 4f06 */
	in.eax.val = 0x4f06;
	in.ebx.val = 0;
	in.ecx.val = length & 0x00ff;
	io_services (0x10, &in, &out);
	return _ah (out.eax);
}

/**
*	VbeGetScanLineLength
*
*	Description : Get Scan Line length.
*
*	Input : length - OUT Scan line length.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeGetScanLineLength (OUT int* length) {

	INTR	in, out;

	/* call BIOS INT 10h function 4f06 */
	in.eax.val = 0x4f06;
	in.ebx.val = 1;
	io_services (0x10, &in, &out);
	*length = in.ebx.val & 0x00ff;
	return _ah (out.eax);
}

/**
*	VbeSetDisplayStart
*
*	Description : Set Display Window start.
*
*	Input : x - Horizontal Display Start.
*           y - Vertical Display Start.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeSetDisplayStart (IN int x, IN int y) {

	INTR	in, out;

	/* call BIOS INT 10h function 4f07 */
	in.eax.val = 0x4f07;
	in.ebx.val = 0x0080;
	in.ecx.val = x;
	in.edx.val = y;
	io_services (0x10, &in, &out);
	return _ah (out.eax);
}

/**
*	VbeGetDisplayStart
*
*	Description : Get Display Window start.
*
*	Input : x - OUT Horizontal Display Start.
*           y - OUT Vertical Display Start.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeGetDisplayStart (OUT int* x, OUT int* y) {

	INTR	in, out;

	/* call BIOS INT 10h function 4f07 */
	in.eax.val = 0x4f07;
	in.ebx.val = 0x0001;
	io_services (0x10, &in, &out);
	*x = out.ecx.val & 0x00ff;
	*y = out.edx.val & 0x00ff;
	return _ah (out.eax);
}

/**
*	VbeGetPModeInterface
*
*	Description : Get VBE Protected Mode Interface.
*
*	Input : seg - OUT 16:16 Real Mode Segment.
*           off - OUT 16:16 Real Mode Offset.
*           length - OUT Size of interface.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeGetPModeInterface (OUT uint16_t* seg, OUT uint16_t* off, OUT uint16_t* length) {

	INTR	in, out;

	/* call BIOS INT 10h function 4f0a */
	in.eax.val = 0x4f0a;
	in.ebx.val = 0;
	io_services (0x10, &in, &out);
	//ES:DI->PMode table
	*seg = out.es;
	*off = out.edi.val & 0x00ff;
	*length = out.ecx.val & 0x00ff;
	return _ah (out.eax);
}

/**
*	VbeGetControlInfo
*
*	Description : Get VBE Controller Information
*
*	Input : buffer - OUT Controller Information Block.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeGetControlInfo (OUT PVBE_INFO_BLOCK buffer) {

	INTR in, out;

	/* call BIOS INT 10h function 4f00 */
	in.eax.val = 0x4f00;
	//ES:DI->buffer
	in.es = SEG ((unsigned int) buffer);
	in.edi.val = (unsigned int) OFFSET ((uint32_t) buffer);
	io_services (0x10, &in, &out);
	return _ah(out.eax);
}

/**
*	VbeGetCapsDDC
*
*	Description : Get Display Data Channel capabilities (install check)
*
*	Input : level - OUT Level.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeGetCapsDDC (OUT uint8_t* level) {

	INTR in, out;

	/* call BIOS INT 10h function 4f15 */
	in.eax.val = 0x4f15;
	in.ebx.val = 0;
	in.ecx.val = 0;
	in.edi.val = 0;
	in.es = 0;
	io_services (0x10, &in, &out);
	*level = out.ebx.val & 0xff;
	return _ah(out.eax);
}

/**
*	VbeGetFlatPanelInfo
*
*	Description : Get Flat Panel Information.
*
*	Input : info - OUT Flat Panel information block.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeGetFlatPanelInfo (OUT PVBE_FLAT_PANEL_INFO info) {

	INTR in, out;

	/* call BIOS INT 10h function 4f11 */
	in.eax.val = 0x4f11;
	in.ebx.val = 1;
	//ES:DI->edid
	in.es = SEG( (int) info);
	in.edi.val = OFFSET ( (int) info);
	io_services (0x10, &in, &out);
	return _ah(out.eax);
}

/**
*	VbeDetectSupport
*
*	Description : Detect VBE Support.
*
*	Input : buffer - OUT VBE Information Block.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int VbeDetectSupport (OUT PVBE_INFO_BLOCK buffer) {
	/**
	* prepare buffer
	*/
	if (buffer) {
		BlrZeroMemory(buffer, sizeof(VBE_INFO_BLOCK));
		buffer->signiture[0] = 'V';
		buffer->signiture[1] = 'B';
		buffer->signiture[2] = 'E';
		buffer->signiture[3] = '2';
		if (VbeGetControlInfo (buffer) == VBE_STATUS_OK)
			return 1;
	}
	return 0;
}

/**
*	BlVbeSetPalette
*
*	Description : Set palette entries.
*
*	Input : numEnteries - Number of enteries to set.
*           start - Starting palette index.
*           palette - Address of palette data.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int BlVbeSetPalette (IN int numEnteries, IN int start, IN addr_t palette) {

	INTR in, out;

	/* call BIOS INT 10h function 4f09 */
	in.eax.val = 0x4f09;
	in.ebx.val = 0;
	in.ecx.val = numEnteries;
	in.edx.val = start;
	in.edi.val = (unsigned int) palette; // ES:DI=palette
	io_services (0x10, &in, &out);
	return _ah(out.eax);
}

/**
*	BlVbeSetPalette
*
*	Description : Read Extended Device Idenitification (EDID) information.
*
*	Input : edid - OUT Output buffer to write information to.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int BlVbeGetEDID (OUT addr_t edid) {

	INTR in, out;

	/* call BIOS INT 10h function 4f15 */
	in.eax.val = 0x4f15;
	in.ebx.val = 1;
	in.ecx.val = 0;
	in.edx.val = 0;
	//ES:DI->edid
	in.es = SEG( (int) edid);
	in.edi.val = OFFSET( (int) edid);
	io_services (0x10, &in, &out);
	return _ah(out.eax);
}

/**
*	VbeToSurface
*
*	Description : Convert VBE mode information to NBL Surface.
*
*	Input : mode - VBE mode number.
*           vbe - VBE mode information block.
*           disp - OUT Video mode information to populate.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void VbeToSurface (IN int mode, IN PVBE_MODE_INFO vbe, OUT nblVideoMode* disp) {

	int i = 0;

	disp->width      = vbe->resolutionX;
	disp->height     = vbe->resolutionY;
	disp->format.bpp = vbe->bitsPerPixel;

//	disp->data               = (uint8_t*) ((void*) vbe->physBasePtr);

	/* clear color masks */
	disp->format.rmask = 0;
	disp->format.gmask = 0;
	disp->format.bmask = 0;
	disp->format.amask = 0;

	/* set shift values */
	disp->format.rshift = vbe->linRedFieldPosition;
	disp->format.gshift = vbe->linGreenFieldPosition;
	disp->format.bshift = vbe->linBlueFieldPosition;
	disp->format.ashift = vbe->linRsvdFieldPosition;

	/* build the bit masks from mask size */
	for (i = 0; i < vbe->linRedMaskSize; i++)
		disp->format.rmask |= (1<<i);
	for (i = 0; i < vbe->linGreenMaskSize; i++)
		disp->format.gmask |= (1<<i);
	for (i = 0; i < vbe->linBlueMaskSize; i++)
		disp->format.bmask |= (1<<i);
	for (i = 0; i < vbe->maskSizeRsvd; i++)
		disp->format.amask |= (1<<i);

	/* set color bit masks */
	disp->format.rmask <<= vbe->linRedFieldPosition;
	disp->format.gmask <<= vbe->linGreenFieldPosition;
	disp->format.bmask <<= vbe->linBlueFieldPosition;
	disp->format.amask <<= vbe->fieldPositionRsvd;

	/* set pitch */
	if (_vbeControllerInfo.version >= 0x300)
		disp->pitch = vbe->linBytesPerScanLine;
	else
		disp->pitch = vbe->bytesPerScanLine;

#if 0
	switch (vbe->memoryModel) {
		case VBE_MEMORY_MODEL_TEXT:
			break;
		case VBE_MEMORY_MODEL_CGA:
			disp->type |= VIDEO_MODE_TYPE_CGA;
			break;
		case VBE_MEMORY_MODEL_HERCULES:
			disp->type |= VIDEO_MODE_TYPE_HERCULES | VIDEO_MODE_TYPE_1BIT_BITMAP;
			break;
		case VBE_MEMORY_MODEL_PLANAR:
			disp->type |= VIDEO_MODE_TYPE_PLANAR | VIDEO_MODE_TYPE_INDEX_COLOR;
			break;
		case VBE_MEMORY_MODEL_PACKED_PIXEL:
			disp->type |= VIDEO_MODE_TYPE_INDEX_COLOR;
			break;
		case VBE_MEMORY_MODEL_NONCHAIN4_256:
			disp->type |= VIDEO_MODE_TYPE_NONCHAIN4;
			break;
		case VBE_MEMORY_MODEL_DIRECT_COLOR:
			disp->type |= VIDEO_MODE_TYPE_RGB;
			break;
		case VBE_MEMORY_MODEL_YUV:
			disp->type |= VIDEO_MODE_TYPE_YUV;
			break;
		default:
			disp->type |= VIDEO_MODE_TYPE_UNKNOWN;
			break;
	};
#endif
}

/**
*	VbeQueryModes
*
*	Description : Query supported video modes.
*
*	Input : callback - Called on every found mode.
*           ex - Pointer to callee supplied arguments to pass into the callback.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t VbeQueryModes (IN NBL_QUERY_MODE_CALLBACK callback, IN void* ex) {

	VBE_INFO_BLOCK ctrlInfo;
	modeNum* modeList = 0;

	if (callback == NULL)
		return NBL_STAT_INVALID_ARGUMENT;

	/* get controller info */
	ctrlInfo.signiture[0]='V';
	ctrlInfo.signiture[1]='B';
	ctrlInfo.signiture[2]='E';
	ctrlInfo.signiture[3]='2';
	VbeGetControlInfo ( &ctrlInfo );

	/* check signiture */
	if (ctrlInfo.signiture[0]=='V' && ctrlInfo.signiture[1]=='E'
		&& ctrlInfo.signiture[2]=='S' && ctrlInfo.signiture[3]=='A') {

			unsigned int i;
			VBE_MODE_INFO modeInfo;
			nblVideoMode  mode;

			modeList = (modeNum*) ctrlInfo.videoModesPtr;

			for (i=0 ; ; i++) {

				if (modeList[i] == VBE_MODELIST_END)
					break;

				VbeGetModeInfo (modeList[i], &modeInfo);
				VbeToSurface   (modeList[i], &modeInfo, &mode);

				if (callback (&mode, ex) == TRUE)
					return NBL_STAT_SUCCESS;
			}

			return NBL_STAT_SUCCESS;
	}

	return NBL_STAT_DEVICE;
}

/**
*	BlVbeInitialize
*
*	Description : Initialize driver.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t BlVbeInitialize (void) {

	VBE_INFO_BLOCK vbeInfo;
	unsigned int numModes = 0;
	uint16_t* curMode = 0;

	if (VbeDetectSupport (&vbeInfo)==0) {

		return NBL_STAT_UNSUPPORTED;
	}
	if (vbeInfo.signiture[0] != 'V' && vbeInfo.signiture[1] != 'E'
		&& vbeInfo.signiture[2] != 'S' && vbeInfo.signiture[3] != 'A') {

			return NBL_STAT_DEVICE;
	}
	/*
		Some firmware store the video mode list in temporary memory
		so we need to get updated pointers by calling VbeDetectSupport
		and copying the mode list to local memory.
	*/
	curMode = (uint16_t*) real2pm (vbeInfo.videoModesPtr);
	while (*curMode != 0xffff) {

		curMode++;
		numModes++;
	}
	curMode = (uint16_t*) real2pm (vbeInfo.videoModesPtr);

	/* make a local copy of mode list */
	_vbeModeList = (uint16_t*) BlrAlloc ((numModes*2)+2);
	if (_vbeModeList == NULL) {

		return NBL_STAT_MEMORY;
	}
	BlrCopyMemory (_vbeModeList, curMode, (numModes*2)+2);

	/* get initial video mode */
	if (VbeGetDisplayMode ((unsigned int*) &_vbeInitialMode) != VBE_STATUS_OK) {

		if (_vbeModeList) {

			BlrFree (_vbeModeList);
			_vbeModeList = 0;
		}
		return NBL_STAT_DRIVER;
	}

	return NBL_STAT_SUCCESS;
}

/**
*	BlVbeShutdown
*
*	Description : Shutdown driver.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t BlVbeShutdown (void) {

	/* restore initial video mode */
	if (_vbeInitialMode) {
		VbeSetDisplayMode (_vbeInitialMode, 0);
	}

	/* release mode list.*/
	if (_vbeModeList) {
		BlrFree (_vbeModeList);
		_vbeModeList = 0;
	}

	return NBL_STAT_SUCCESS;
}

/**
*	BlVbeSetPage
*
*	Description : Set active page for page flipping.
*
*	Return : NBL_STAT_UNSUPPORTED.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int BlVbeSetPage (IN int page) {

	return NBL_STAT_UNSUPPORTED;

/*
	int startY;
	startY = _framebuffer.surface.height * page;

	if (VbeSetDisplayStart (0, startY) != VBE_STATUS_OK)
		return NBL_STAT_DEVICE;

	return NBL_STAT_SUCCESS;
*/
}

/**
*	BlVbeSetMode
*
*	Description : Set video mode.
*
*	Return : width - Horizontal size in pixels.
*            height - Vertical size in pixels.
*            bpp - Bits per pixel.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t BlVbeSetMode (IN int width, IN int height, IN int bpp) {

	VBE_MODE_INFO modeInfo;
	VBE_MODE_INFO bestModeInfo;
	uint16_t*     curMode;
	uint16_t      bestMode;

	curMode = _vbeModeList;
	BlrZeroMemory(&bestModeInfo, sizeof(VBE_MODE_INFO));

	if (width == 0 && height == 0) {
		/* fall back to 640x480 */
		width = 640;
		height = 480;
	}

	/* look for matching video mode */
	for (curMode = _vbeModeList; *curMode != 0xffff; curMode++) {

		if (VbeGetModeInfo (*curMode, &modeInfo) == VBE_STATUS_OK) {

			/* skip over modes that are not supported */
			if ((modeInfo.modeAttrib & 0x001) == 0)
				continue;

			/* skip over monochrome modes */
			if ((modeInfo.modeAttrib & VBE_MODEATTR_COLOR) == 0)
				continue;

			/* skip over non-graphical modes */
			if ((modeInfo.modeAttrib & VBE_MODEATTR_GRAPHICS) == 0)
				continue;

			/* skip over non-LFB modes */
			if ((modeInfo.modeAttrib & 0x80) == 0) //VBE_MODEATTR_LFB_AVAIL) == 0)
				continue;

//			if ((modeInfo.modeAttrib & 0x90) != 0x90)
//				continue;

			/* skip over non-supporting modes */
			if (modeInfo.bitsPerPixel != 8 && modeInfo.bitsPerPixel != 15
				&& modeInfo.bitsPerPixel != 16 && modeInfo.bitsPerPixel != 24
				&& modeInfo.bitsPerPixel != 32) {

					continue;
			}

			/* skip modes that do not match requested resolution */
			if (modeInfo.resolutionX != width && modeInfo.resolutionY != height)
				continue;

			/* compare bits per pixels if its valid */	
			if (bpp > 0) {

				/* try to find requested mode */
				if (modeInfo.bitsPerPixel == bpp) {

					bestMode = *curMode;
					BlrCopyMemory (&bestModeInfo, &modeInfo, sizeof (VBE_MODE_INFO));
					break;
				}
			}
			else {

				/* try to find the best mode with highest resolution */
				if (modeInfo.bitsPerPixel * modeInfo.resolutionX * modeInfo.resolutionY
					< bestModeInfo.bitsPerPixel * bestModeInfo.resolutionX * bestModeInfo.resolutionY) {

						continue;
				}

				/* set mode for use */
				bestMode = *curMode;
				BlrCopyMemory (&bestModeInfo, &modeInfo, sizeof (VBE_MODE_INFO));
			}
		}
	}

	if (bestMode) {

		VBE_MODE_INFO modeInfo2;

		/* enable LFB mode in all modes */
		if (bestMode >= 0x100)
			bestMode |= 1 << 14;

		/* get mode info */
		if (VbeGetModeInfo (bestMode, &modeInfo2) != VBE_STATUS_OK)
			return NBL_STAT_UNSUPPORTED;

		/* set display mode */
		if (VbeSetDisplayMode (bestMode, 0) != VBE_STATUS_OK)
			return NBL_STAT_UNSUPPORTED;

		/* set page 0 */
		VbeSetDisplayStart (0, 0);

		/* initialize rest of framebuffer structure */
		//VbeToSurface (bestMode, &modeInfo2, &_framebuffer.surface);

		/* set as current mode */
		_vbeCurrentMode = bestMode;

		/* set as current linear frame buffer. */
		_vbeCurrentLFB = modeInfo2.physBasePtr;
	}

	return NBL_STAT_SUCCESS;
}

/**
*	BlVbeGetMode
*
*	Description : Returns VBE mode number.
*
*	Input : out - OUT Current video mode information.
*
*	Return : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t BlVbeGetMode (IN nblVideoMode* out) {

	VBE_MODE_INFO mode;

	if (!out)
		return NBL_STAT_INVALID_ARGUMENT;

	/* get mode info */
	VbeGetModeInfo (_vbeCurrentMode, &mode);
	VbeToSurface (_vbeCurrentMode, &mode, out);	
	return TRUE;
}

/**
*	VbeEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t VbeEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	nblMessage       m;

	_self = self;
	dm    = dependencies[0];

	if (!dm)
		return NBL_STAT_DEPENDENCY;

	/* create device. */
	m.source = self;
	m.type   = NBL_DEV_REGISTER;
	m.NBL_DEV_REGISTER_OBJ = &_vbeDeviceObject;
	dm->request (&m);

	return BlVbeInitialize();
}

/**
*	VbeSetVideoMode
*
*	Description : Set video mode.
*
*	Return : width - Horizontal size in pixels.
*            height - Vertical size in pixels.
*            bpp - Bits per pixel.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t VbeSetVideoMode (IN uint32_t width, IN uint32_t height, IN uint32_t depth) {

	return BlVbeSetMode (width, height, depth);
}

/**
*	VbeGetVideoMode
*
*	Description : Returns VBE mode number.
*
*	Input : out - OUT Current video mode information.
*
*	Return : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t VbeGetVideoMode (OUT nblVideoMode* mode) {

	return BlVbeGetMode (mode);
}

/**
*	VbeCreateSurface
*
*	Description : Creates an NBL Surface Object for the current video mode.
*
*	Input : out - OUT Pointer to NBL Surface Object to retrieve information.
*
*	Return : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE STATIC status_t VbeCreateSurface (IN OUT nblSurface* out) {

	VBE_MODE_INFO mode;

	if (!out)
		return NBL_STAT_INVALID_ARGUMENT;

	if (!_vbeCurrentMode)
		return NBL_STAT_DRIVER;

	VbeGetModeInfo(_vbeCurrentMode, &mode);

	out->width  = mode.resolutionX;
	out->height = mode.resolutionY;
	out->pitch  = mode.bytesPerScanLine;
	out->pixels = (void*) _vbeCurrentLFB;

	out->format.bpp = mode.bitsPerPixel;
	out->format.bytesPerPixel = out->format.bpp / 8;
	out->format.palette       = NULL;

	out->format.rshift = mode.fieldPositionRed;
	out->format.gshift = mode.fieldPositionGreen;
	out->format.bshift = mode.fieldPositionBlue;
	out->format.ashift = mode.fieldPositionRsvd;

	out->format.rmask = mode.maskSizeRed;
	out->format.gmask = mode.maskSizeGreen;
	out->format.bmask = mode.maskSizeBlue;
	out->format.amask = mode.maskSizeRsvd;

	return NBL_STAT_SUCCESS;
}

/**
*	QueryProtectedModeInterface
*
*	Description : Get VBE Protected Mode Interface.
*
*	Input : seg - OUT 16:16 Real Mode Segment.
*           off - OUT 16:16 Real Mode Offset.
*           length - OUT Size of interface.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void QueryProtectedModeInterface(OUT uint16_t* seg, OUT uint16_t* off, OUT uint16_t* length) {

	VbeGetPModeInterface(seg, off, length);
}

/**
*	QueryCurrentModeNumber
*
*	Description : Returns current VBE mode number.
*
*	Output : Current VBE mode number.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE uint16_t QueryCurrentModeNumber(void) {

	return _vbeCurrentMode;
}

/**
*	QueryControlInfo
*
*	Description : Query controller information.
*
*	Input : OUT Output VBE Information Block.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void QueryControlInfo(OUT PVBE_INFO_BLOCK buffer) {

	VbeGetControlInfo(buffer);
}

/**
*	QueryModeInfo
*
*	Description : Query mode information.
*
*	Input : OUT Output VBE Mode Information Block.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void QueryModeInfo(OUT PVBE_MODE_INFO pModeInfo) {

	VbeGetModeInfo(_vbeCurrentMode, pModeInfo);
}

/**
*	VbeRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE STATIC status_t VbeRequest(IN nblMessage* rpb) {

	rettype_t r;

	rpb->retval = 0;
	switch (rpb->type) {
		case NBL_VIDEO_SETMODE:
			r =(rettype_t) VbeSetVideoMode (rpb->NBL_VIDEO_SETMODE_WIDTH,
				rpb->NBL_VIDEO_SETMODE_HEIGHT, rpb->NBL_VIDEO_SETMODE_DEPTH);
			break;
		case NBL_VIDEO_GETMODE:
			r = (rettype_t) VbeGetVideoMode ((nblVideoMode*) rpb->NBL_VIDEO_GETMODE_OUT);
			break;
		case NBL_VIDEO_QUERY:
			r = (rettype_t) VbeQueryModes ((NBL_QUERY_MODE_CALLBACK) rpb->NBL_VIDEO_QUERY_CALL,
				(void*) rpb->NBL_VIDEO_QUERY_EX);
			break;
		case NBL_VIDEO_CREATESURFACE:
			r = (rettype_t) VbeCreateSurface((nblSurface*) rpb->NBL_VIDEO_CREATESURFACE_IN);
			break;
		case VBE_QUERY_PMODE_INTERFACE:
			QueryProtectedModeInterface(
				(uint16_t*)rpb->VBE_QUERY_PMODE_INTERFACE_SEG,
				(uint16_t*)rpb->VBE_QUERY_PMODE_INTERFACE_OFF,
				(uint16_t*)rpb->VBE_QUERY_PMODE_INTERFACE_LENGTH);
			break;
		case VBE_QUERY_MODE:
			r = (rettype_t)QueryCurrentModeNumber();
			break;
		case VBE_QUERY_CONTROL_INFO:
			QueryControlInfo((PVBE_INFO_BLOCK)rpb->VBE_QUERY_CONTROL_INFO_OUT);
			break;
		case VBE_QUERY_MODE_INFO:
			QueryModeInfo((PVBE_MODE_INFO)rpb->VBE_QUERY_MODE_INFO_OUT);
			break;
		case NBL_VIDEO_GETPALETTE:
		case NBL_VIDEO_SETPALETTE:
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_BIOS_VBE_VERSION 1

/* VBE driver object. */
NBL_DRIVER_OBJECT_DEFINE _nBiosVbeDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_BIOS_VBE_VERSION,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_VIDEO,
	"vbe",
	VbeEntryPoint,
	VbeRequest,
	1,
	"dm"
};
