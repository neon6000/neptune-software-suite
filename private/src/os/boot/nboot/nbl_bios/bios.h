/************************************************************************
*
*	BIOS firmware definitions.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef BIOS_H
#define BIOS_H

#include <nbl.h>

/*
	This component defines definitions and constants used
	throughout all drivers defined in the BIOS module.

	The software must be loaded at an address suitable for
	executing real mode code.
*/

/* GDT Selectors for 32 bit protected mode. These are defined by NSTART. */
#define NULL_SEL       0
#define CODE_SEL       8
#define DATA_SEL       0x10

/* GDT Selectors for 16 bit protected mode. These are defined by NSTART. */
#define CODE_SEL_16BIT 0x18
#define DATA_SEL_16BIT 0x20

/*
	The mouse callback function is 16 bit real mode code. It stores
	the mouse coordinates at 0x500.
*/
#define MOUSE_X       0x500
#define MOUSE_Y       0x502
#define MOUSE_STAT    0x504

/**
*	Convert seg:off pointer to far ptr
*/
#define MK_FP(seg,off) ((seg * 16) + off)

/**
*	Convert far ptr to a seg:off pointer
*/
#define SEG(linear)    (linear / 16)
#define OFFSET(linear) (linear % 16)

#pragma pack(push,1)

/*
	IDT Entry Descriptor.
*/
typedef struct _IDTE {
	uint16_t baseLo;
	uint16_t selector;
	uint8_t  reserved;
	uint8_t  flags;
	uint16_t baseHi;
}IDTE, *PIDTE;

/*
	IDT Register Descriptor.
*/
typedef struct _IDTR {
	uint16_t size;
	uint32_t base;
}IDTR, *PIDTR;

/*
	BIOS Data Area
*/
typedef struct _BDA {
	struct {
		uint8_t     seconds;
		uint8_t     secondsAlarm;
		uint8_t     minutes;
		uint8_t     minutesAlarm;
		uint8_t     hours;
		uint8_t     hoursAlarm;
		uint8_t     dayOfWeek;
		uint8_t     dateDay;
		uint8_t     dateMonth;
		uint8_t     dateYear;
	}rtc;
	struct {
		uint8_t     a;
		uint8_t     b;
		uint8_t     c;
		uint8_t     d;
	}status;
	uint8_t     diagnosticStatus;
	uint8_t     shutdownStatus;
	uint8_t     floppyDiskDriveTypes;
	uint8_t     systemConfigurationSettings;
	uint8_t     hardDiskDriveTypes;
	uint8_t     typematicParameters;
	uint8_t     installedEquipment;
	uint8_t     baseMemoryLo;
	uint8_t     baseMemoryHi;
	uint8_t     extendedMemoryLo;
	uint8_t     extendedMemoryHi;
	uint8_t     hdisk0ExtendedType;
	uint8_t     hdisk1ExtendedType;
	struct {
		uint8_t     cylLo;
		uint8_t     cylHi;
		uint8_t     heads;
		uint8_t     precompCylLo;
		uint8_t     precompCylHi;
		uint8_t     control;
		uint8_t     landingZoneLo;
		uint8_t     landingZoneHi;
		uint8_t     sectors;
	}driveC;
	struct {
		uint8_t     cylLo;
		uint8_t     cylHi;
		uint8_t     heads;
		uint8_t     precompCylLo;
		uint8_t     precompCylHi;
		uint8_t     control;
		uint8_t     landingZoneLo;
		uint8_t     landingZoneHi;
		uint8_t     sectors;
	}driveD;
	uint8_t     systemOperationalFlag;
	uint8_t     cmosChecksumHi;
	uint8_t     cmosChecksumLo;
	uint8_t     actualExtendedMemoryLo;
	uint8_t     actualExtendedMemoryHi;
	uint8_t     centuryDateBCD;
	uint8_t     postInformationFlags;
	uint16_t    biosAndShadowOptionFlags;
	uint8_t     chipset;
	uint8_t     password;
	uint8_t     encryptedPassword[6];
	uint8_t     extendedCmosChecksumHi;
	uint8_t     extendedCmosChecksumLo;
	uint8_t     model;
	uint8_t     serial[6];
	uint8_t     crc;
	uint8_t     century;
	uint8_t     dateAlarm;
	uint8_t     extendedControl4A;
	uint8_t     extendedControl4B;
	uint8_t     _reserved1[2];
	uint8_t     rtcAddress1l;
	uint8_t     rtcAddress2;
	uint8_t     extendedRamAddressLo;
	uint8_t     extendedRamAddressHi;
	uint8_t     _reserved2;
	uint8_t     extendedRamDataPort;
	uint8_t     _reserved3[10];
}BDA, *PBDA;

#define BDA_BASE 0x400

#define GET_BDA ((PBDA)BDA_BASE)

typedef enum {
	MMAP_DESCR_AVAILABLE,
	MMAP_DESCR_RESERVED,
	MMAP_DESCR_ACPI_RECLAIM,
	MMAP_DESCR_ACPI_NVS
}MMAP_DESCRIPTOR_TYPE;

/**
*	Memory descriptor.
*/
typedef struct _MMAP_DESCRIPTOR {
	uint64_t          base;
	uint64_t          length;
	uint32_t          type;
	OPTIONAL uint32_t acpiAttributes;
}MMAP_DESCRIPTOR, *PMMAP_DESCRIPTOR;

/**
*	32 bit register
*/
typedef union _R32BIT {
	union _R16BIT {
		struct _R8BIT {
			unsigned char l;
			unsigned char h;
		}b;
		unsigned short val;
	}w;
	unsigned int val;
} R32BIT, *PR32BIT;

/**
*	Registers
*/
typedef struct _INTR {
	R32BIT eax;
	R32BIT ebx;
	R32BIT ecx;
	R32BIT edx;
	R32BIT esi;
	R32BIT edi;
	R32BIT ebp;
	R32BIT esp; //io_services does not allow setting of esp
	R32BIT eip; //io_services does not allow setting of eip
	uint16_t cs;
	uint16_t ds; //io_services sets this to 64k always
	uint16_t es;
	uint16_t ss; //io_services does not allow setting of ss
	uint16_t fs;
	uint16_t gs;
	uint16_t flags;
}INTR, *PINTR;

#pragma pack(pop)

/**
*	Register access for improved readability
*/

#define _ax(r) (r.w.val)
#define _al(r) (r.w.b.l)
#define _ah(r) (r.w.b.h)

#define _bx(r) (r.w.val)
#define _bl(r) (r.w.b.l)
#define _bh(r) (r.w.b.h)

#define _cx(r) (r.w.val)
#define _cl(r) (r.w.b.l)
#define _ch(r) (r.w.b.h)

#define _dx(r) (r.w.val)
#define _dl(r) (r.w.b.l)
#define _dh(r) (r.w.b.h)

#define _sp(r) (r.esp.w)
#define _bp(r) (r.ebp.w)
#define _si(r) (r.esi.w)
#define _di(r) (r.w)
#define _ip(r) (r.eip.w)

extern PUBLIC int __cdecl io_services (IN int function, IN PINTR in, IN PINTR out);
extern PUBLIC void chainload_rmode(IN int drive);
extern PUBLIC void BlPs2BiosDeviceHandler(IN uint16_t PO, IN uint8_t  xData, IN uint8_t  yData, IN uint16_t status);

#endif
