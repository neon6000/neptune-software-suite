/************************************************************************
*
*	EFI NBL Time Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "bios.h"

/*** PUBLIC DECLARATIONS ***/

typedef struct _nblTime2 {
	uint16_t year;
	uint8_t  month;
	uint8_t  day;
	uint8_t  hour;
	uint8_t  minute;
	uint8_t  second;
	uint32_t nanosecond;
	int16_t  timezone;
	uint8_t  daylight;
} nblTime2;

/*** PRIVATE DECLARATIONS ***/

/* real time clock device object. */
NBL_DRIVER_OBJECT_DEFINE _nBiosRtcDriverObject;
STATIC nblDeviceObject _rtc = {
	NBL_DEVICE_OTHER_CONTROLLER,
	NBL_DEVICE_CONTROLLER_CLASS,
	0, &_nBiosRtcDriverObject, 0, 0, "rtc", 0
};

PRIVATE unsigned int TimeGetTickCount ();
PRIVATE status_t RtcGetTime (OUT nblTime* clock);
PRIVATE status_t RtcSetTime (IN nblTime* clock);
PRIVATE status_t RtcEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]);
PRIVATE status_t RtcStall (IN uint32_t ms);
PRIVATE status_t RtcRequest (IN nblMessage* rpb);

/*** PRIVATE METHODS ***/

/**
*	RtcTick32
*
*	Description : Not needed anymore.
*/
PRIVATE void RtcTick32(void) {

}

/**
*	TimeGetTickCount
*
*	Description : Returns current RTC Tick Count.
*
*	Output : Current tick count.
*/
PRIVATE unsigned int TimeGetTickCount () {

	/* Offset 0x6C in BDA, updated by INT 1A. */
	uint32_t* data = (uint32_t*) 0x46c;
	return *data;
}

/**
*	TimeGetTickCount
*
*	Description : Returns current RTC Tick Count.
*
*	Output : Current tick count.
*/
PRIVATE unsigned int RtcGetTicks () {

	/* Offset 0x6C in BDA, updated by INT 1A. */
	uint32_t* data = (uint32_t*)0x46c;
	return *data;
}

/**
*	RtcGetTime
*
*	Description : Returns the current time and date.
*
*	Input : clock - Ignored.
*
*	Output : NBL_STAT_UNSUPPORTED.
*/
PRIVATE status_t RtcGetTime (OUT nblTime* clock) {

	return NBL_STAT_UNSUPPORTED;
}

/**
*	RtcSetTime
*
*	Description : Returns the current time and date.
*
*	Input : clock - Ignored.
*
*	Output : NBL_STAT_UNSUPPORTED.
*/
PRIVATE status_t RtcSetTime (IN nblTime* clock) {

	return NBL_STAT_UNSUPPORTED;
}

/**
*	RtcStall
*
*	Description : Halts the CPU for a given time.
*
*	Input : ms - Milliseconds to halt the system for.
*
*	Output : Status Code.
*/
PRIVATE status_t RtcStall (IN uint32_t ms) {

	unsigned int endTime;

	endTime = ((RtcGetTicks() * 18)) + ms;
	while (endTime > ((RtcGetTicks() * 18)))
		__asm rep nop

	return NBL_STAT_SUCCESS;
}

/**
*	RtcEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t RtcEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	nblMessage rpb;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	/* create rtc device. */
	rpb.source = self;
	rpb.type   = NBL_DEV_REGISTER;
	rpb.NBL_DEV_REGISTER_OBJ = &_rtc;
	dm->request (&rpb);

	return NBL_STAT_SUCCESS;
}

/**
*	RtcRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t RtcRequest (IN nblMessage* rpb) {

	switch (rpb->type) {
		case NBL_RTC_INSTALL:
			return NBL_STAT_SUCCESS;
		case NBL_RTC_GETTIME:
			return RtcGetTime((nblTime*) rpb->NBL_RTC_GETTIME_DESCR);
		case NBL_RTC_SETTIME:
			return RtcSetTime((nblTime*) rpb->NBL_RTC_SETTIME_DESCR);
		case NBL_RTC_STALL:
			return RtcStall ( (uint32_t) rpb->NBL_RTC_STALL_MS );
		default:
			return NBL_STAT_UNSUPPORTED;
	};
}

#define NBL_BIOS_RTC_VERSION 1

NBL_DRIVER_OBJECT_DEFINE _nBiosRtcDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_BIOS_RTC_VERSION,
	NBL_DRV_LOAD_CORE,
	NBL_DRV_CLASS_OTHER,
	"rtc",
	RtcEntryPoint,
	RtcRequest,
	1,
	"dm"
};
