/************************************************************************
*
*	BIOS Video Graphics Array (VGA) Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "bios.h"

unsigned char* _F8x8Ptr;
unsigned char* _F8x14Ptr;

int CharWidth = 8;
int CharHeight = 14;

int screen_width = 320;
unsigned char* off_screen = (unsigned char*) 0xa0000;

unsigned char* FontPtr;

/**
*	VgaDrawCharacter
*
*	Description : Renders a character using the default BIOS font.
*
*	Input : c Character)
*           x Horizontal pixel location
*           y Vertical pixel location
*           forecolor Foreground color
*           backcolor Background color
*
*	Output : The character will be rendered to video memory.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void VgaDrawCharacter (IN int c, IN int x, IN int y, IN int forecolour, IN int backcolour)
{
	unsigned char *p, *fnt;
	int width, height, adj;
	unsigned char mask;

	// make p point to the screen
	p = off_screen + y*screen_width + x;
	adj=screen_width - CharWidth;

	// make fnt point to the start of the character we want to draw.
	// characters are 1 byte wide, and height bytes tall
	fnt=FontPtr + c*CharHeight;

	height=CharHeight;
	while ( height-- )
		{
		width=CharWidth;
		mask=128; 			// bit mask: 10000000
		while ( width-- )		// assumes width of 8
			{
			if ( (*fnt) & mask )	 // is this bit set?
				{
				// draw pixel
				*p++=forecolour;
				}
			else
				{
				// draw background colour
				*p++=backcolour;
				}

			mask>>=1;  // shift mask to check next bit
			}

		p+=adj;  // next line on screen
		fnt++;   // next line of font
		}
}

/**
*	VgaTest
*
*	Description : VGA Tests. This should not be called outside of tests.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void VgaTest(void) {

	INTR in, out;

	_F8x8Ptr = 0;
	_F8x14Ptr = 0;

	in.eax.val = 0x1130; // 30 get info on current set, 11 character info
	in.ebx.val = 0x0300; // get 8x8 font info

	io_services(0x10,&in,&out);

	// es:bp points to 8x8 font
	_F8x8Ptr = (unsigned char*) MK_FP (out.es, out.ebp.w.val);

	in.eax.val = 0x1130; // 30 get info on current set, 11 character info
	in.ebx.val = 0x0200; // get 8x14 font info

	io_services(0x10,&in,&out);

	// es:bp points to 8x14 font
	_F8x14Ptr = (unsigned char*) MK_FP (out.es, out.ebp.w.val);

	// set mode 13h
	in.eax.val = 0x13;
	io_services(0x10,&in,&out);

	// draw background.
	FontPtr = _F8x14Ptr;

	// fonr tests.
	VgaDrawCharacter('a', 2, 2, 4, 5);
}
