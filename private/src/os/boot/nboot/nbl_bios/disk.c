/************************************************************************
*
*	disk.c - BIOS Disk Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "bios.h"

/* * * PRIVATE DEFINITIONS * * */

/* Self reference. */
nblDriverObject* _self;

#define SECTOR_SIZE 512

typedef struct _DISK_DEVICE_OBJECT {
	uint32_t NumberOfSectors;
	uint32_t BytesPerSector;
	uint32_t DiskNumber;
}DISK_DEVICE_OBJECT, *PDISK_DEVICE_OBJECT;

/*
	BIOS Disk Services Status Codes.
*/
typedef enum _DISK_STATUS {
   SUCCESSFUL_COMPLETION       =   0,
   INVALID_FUNCTION            =   1,
   ADDRESS_MARK_NOT_FOUND      =   2,
   DISK_WRITE_PROTECTED        =   3,
   SECTOR_NOT_FOUND            =   4,
   RESET_FAILED                =   5,
   DISK_CHANGED                =   6,
   DRIVE_PARM_ACTIVITY_FAILED  =   7,
   DMA_OVERRUN                 =   8,
   DATA_BOUNDARY_ERROR         =   9,
   BAD_SECTOR_DETECTED         =   0xa,
   BAD_TRACK_DETECTED          =   0xb,
   INVLID_MEDIA                =   0xc,
   INVALID_NUMBER_OF_SECTORS   =   0xd,
   CONTROL_DATA_ADDRESS_MARK   =   0xe,
   DMA_ARBITRATION_OUT_OF_RANGE=   0xf,
   ECC_ERROR                   =   0x10,
   DATA_ECC_CORRECTED          =   0x11,
   CONTROLLER_FAILURE          =   0x20,
   NO_MEDIA                    =   0x31,
   INVALID_DRIVE_TYPE_IN_CMOS  =   0x32,
   SEEK_FAILED                 =   0x40,
   NOT_READY                   =   0x80,
   HDD_NOT_READY               =   0xaa,
   EXT_NOT_LOCKED_IN_DRIVE     =   0xb0,
   EXT_LOCKED_IN_DRIVE         =   0xb1,
   EXT_NOT_REMOVABLE           =   0xb2,
   EXT_VOLUME_IN_USE           =   0xb3,
   EXT_LOCK_COUNT_EXCEEDED     =   0xb4,
   EXT_EJECT_REQUEST_FAILED    =   0xb5,
   EXT_READ_PROTECTED          =   0xb6,
   UNDEFINED_ERROR             =   0xbb,
   WRITE_FAULT                 =   0xcc,
   STATUS_REGISTER_ERROR       =   0xe0,
   SENSE_OPERATION_FAILED      =   0xff
}DISK_STATUS;

/**
*	BIOS Drive types.
*/
typedef enum _DRIVE_TYPE {
   FLOPPYDISK_0               =   0,
   FLOPPYDISK_1               =   1,
   HARDDISK_0                 =   0x80,
   HARDDISK_1                 =   0x81,
   DRIVE_TYPE_UNKOWN          =   0xff
}DRIVE_TYPE;

/**
*	BIOS Media types.
*/
typedef enum BIOS_MEDIA_TYPE {
   FDISK_360K                 =   1,
   FDISK_12M                  =   2,   //1.22mb
   FDISK_720K                 =   3,
   FDISK_144                  =   4,   //1.44mb
   FDISK_288                  =   6,   //2.88mb
   ATAPI_REMOVABLE_MEDIA      =   0x10
}BIOS_MEDIA_TYPE;

/**
*	Disk Address Packet.
*/
#pragma pack(push,1)
typedef struct _DAP {
	/* Size must be 0x10 or 0x18. */
	uint8_t size;
	uint8_t reserved;
	/* sectors to read, 0..127 (should be WORD?) */
	uint16_t numBlocksTransfer;
	uint16_t bufferOff;
	uint16_t bufferSeg;
	uint64_t startSector;
}DAP, *PDAP;
#pragma pack(pop)

PRIVATE unsigned int DiskGetType (IN uint8_t driveNum, OUT OPTIONAL unsigned int* driveType, OUT OPTIONAL unsigned int* numberOfSectors);
PRIVATE unsigned int DiskGetStatus (IN unsigned int driveNum);
PRIVATE unsigned int DiskWriteSectorsEx (IN unsigned int drive, IN uint64_t sector, IN uint16_t bufferSeg, IN uint16_t bufferOffset);
PRIVATE unsigned int DiskReadSectorsEx (IN unsigned int drive, IN uint64_t sector, IN uint16_t bufferSeg, IN uint16_t bufferOffset);
PRIVATE unsigned int DiskReadSectors (IN unsigned int cylNumber, IN unsigned int sectorNum, IN unsigned int  headNum, IN unsigned int  driveNum, OUT unsigned char* buffer);
PRIVATE STATIC unsigned int DiskReset (IN unsigned int disk);
PRIVATE unsigned int DiskGetParams (IN unsigned int disk,
			   OUT unsigned int* type,
			   OUT unsigned int* numCyl,
			   OUT unsigned int* SectorsPerTrack,
			   OUT unsigned int* maxHeads,
			   OUT unsigned int* numDisks,
			   OUT unsigned int* TotalSectors);
PRIVATE unsigned int DiskGetNumberOfFdd ();
PRIVATE unsigned int DiskGetNumberOfHdd ();
PRIVATE unsigned int DiskRawRead (IN unsigned int Drive, IN unsigned int Sector, OUT unsigned char* Buffer);
PRIVATE size_t DiskRead (IN nblDeviceObject* dev,
		  IN uint32_t sector,
		  IN uint32_t offset,
		  IN uint32_t size,
		  OUT void* buffer);
PRIVATE size_t DiskWrite (IN nblDeviceObject* dev,
		   IN uint32_t sector,
		   IN uint32_t offset,
		   IN uint32_t size,
		   IN void* buffer);
PRIVATE void DiskInitializeDevices (IN nblDriverObject* dm);
PRIVATE status_t BiosDiskEntryPoint (IN nblDriverObject* self,
					IN int dependencyCount,
					IN nblDriverObject* dependencies[]);
PRIVATE status_t BiosDiskRequest (IN nblMessage* rpb);

struct {
	uint8_t* data;
	uint32_t sector;
	uint32_t offset;
}CACHE;

/*** PRIVATE FUNCTIONS ***/

/**
*	BiosDiskEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t BiosDiskEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	_self = self;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	/* initialize cache. */
	BlrSetActiveHeap(ZONE_LOW);
	CACHE.data = (uint8_t*)BlrAlloc(SECTOR_SIZE);
	BlrSetActiveHeap(ZONE_NORMAL);
	CACHE.sector = 0xffffffff;
	CACHE.offset = 0;

	/* enumerate disk devices. */
	DiskInitializeDevices (dm);
	return 0;
}

/**
*	BiosDiskRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t BiosDiskRequest (IN nblMessage* rpb) {

	rettype_t r;
	rpb->retval = 0;
	switch (rpb->type) {
		case NBL_DISK_READ:
			r = DiskRead (rpb->NBL_DISK_READ_DEV,rpb->NBL_DISK_READ_SECTOR,
				rpb->NBL_DISK_READ_OFFSET,rpb->NBL_DISK_READ_SIZE, (void*) rpb->NBL_DISK_READ_BUFFER);
			break;
		case NBL_DISK_WRITE:
			r = DiskWrite (rpb->NBL_DISK_WRITE_DEV,rpb->NBL_DISK_WRITE_SECTOR,
				rpb->NBL_DISK_WRITE_OFFSET,rpb->NBL_DISK_WRITE_SIZE,(void*) rpb->NBL_DISK_WRITE_BUFFER);
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_BIOS_DISK_VERSION 1

/* Disk driver object. */
NBL_DRIVER_OBJECT_DEFINE _nBiosDiskDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_BIOS_DISK_VERSION,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_OTHER,
	"disk",
	BiosDiskEntryPoint,
	BiosDiskRequest,
	1,
	"dm"
};

/**
*	DiskGetType
*
*	Description : Get drive type information.
*
*	Input : driveNum - BIOS drive number
*           driveType - Drive type
*           numberOfSectors - Number of sectors
*
*	Output : TRUE if supported, FALSE if not.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE unsigned int DiskGetType (IN uint8_t driveNum, OUT OPTIONAL unsigned int* driveType, OUT OPTIONAL unsigned int* numberOfSectors) {

	INTR in;
	INTR out;

	/* call int 0x13 function 0x15 */
	in.ecx.val   = 0;
	_ah (in.eax) = 0x15;
	_dl (in.edx) = driveNum;
	_al (in.eax) = 0xff;
	in.ecx.val   = 0xffff;

	/* call BIOS. */
	io_services (0x13, &in, &out);

	/* set drive type. */
	if (driveType != NULL) {
		*driveType = _ah (out.eax);
	}

	/* set number of sectors. */
	if (numberOfSectors != NULL) {

		unsigned int sectors;
		
		sectors = _dx (out.edx);
		sectors = (sectors & ~0xffff0000) | _cx (in.ecx);

		*numberOfSectors = sectors;
	}

	/* status code is in AH. */
	return _ah (out.eax);
}

/**
*	DiskGetStatus
*
*	Description : Returns disk status code.
*
*	Input : driveNum - BIOS Device Number.
*
*	Output : Disk Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE unsigned int DiskGetStatus (IN unsigned int driveNum) {

	INTR in;
	INTR out;

	/* call int 0x13 function 1 and return status */
	in.ecx.val = 0;
	_ah (in.eax) = 1;
	_dl (in.edx) = (unsigned char)driveNum;

	/* call BIOS. */
	io_services (0x13, &in, &out);

	/* status code is in AH. */
	return _ah (out.eax);
}

/**
*	DiskWriteSectorsEx
*
*	Description : Write sectors to disk.
*
*	Input : drive - BIOS drive number
*           sector - Starting sector
*           bufferSeg - 16:16 Far Pointer Segment
*           bufferOffset - 16:16 Far Pointer Offset
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE unsigned int DiskWriteSectorsEx (IN unsigned int drive, IN uint64_t sector, IN uint16_t bufferSeg, IN uint16_t bufferOffset) {

	INTR in;
	INTR out;
	DAP  packet;

	/* set up packet for transfer. */
	packet.bufferSeg         = bufferSeg;
	packet.bufferOff         = bufferOffset;
	packet.numBlocksTransfer = 1;
	packet.reserved          = 0;
	packet.size              = 0x10;
	packet.startSector       = sector;

	/* call int 0x13 function 0x4300. */
	_ah (in.eax) = 0x43;
	_dl (in.edx) = drive;
	_al (in.eax) = 0;		/* write without verify */

	/*
		segments always set to 0x2000 in io_services to
		offset 64k linear base (0x2000 * 16 = 64k) so
		we need to subtract the 64k to get ds:es == packet.
	*/

	in.esi.val = (unsigned int) &packet - 0x20000;

	/* call BIOS. */
	io_services (0x13, &in, &out);

	/* return status code in AH. */
	return _ah (out.eax);
}

/**
*	DiskReadSectorsEx
*
*	Description : Read sectors from disk.
*
*	Input : drive - BIOS drive number
*           sector - Starting sector
*           bufferSeg - 16:16 Far Pointer Segment
*           bufferOffset - 16:16 Far Pointer Offset
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE unsigned int DiskReadSectorsEx (IN unsigned int drive, IN uint64_t sector, IN uint16_t bufferSeg, IN uint16_t bufferOffset) {

	/*
		DiskReadSectorsEx note:

		io_services sets DS=ES=0x20000. We need
		to set DS:SI->DAP so the DAP must be located
		somewhere >= 0x20000. That is,

		DS:SI=>0x2000:0=0x20000
		...
		DS:SI=>0x2000:0xFFFF=0x2FFFF.

		- We cannot use the stack since its < 0x20000.
		- We cannot use the heap since the DAP must be < 0x2FFFF to avoid overflow.

		To make things simple, we set DS:SI=>0x2000:0=0x20000 which overwrites the
		first 16 bytes of NBOOT.EXE.
	*/

	INTR     in;
	INTR     out;
	DAP*     packet = (DAP*) 0x20000;

	/* set up packet for transfer. */
	packet->size              = 16;
	packet->reserved          = 0;
	packet->numBlocksTransfer = 1;
	packet->bufferSeg         = bufferSeg;
	packet->bufferOff         = bufferOffset;
	packet->startSector       = sector;

	/* DS:SI => 0x2000:0 => 200000. */
	BlrZeroMemory(&in,sizeof(INTR));
	_si(in).val = 0;

	/* int 13h function 42h */
	_ah (in.eax) = 0x42;
	_dl (in.edx) = drive;

	io_services (0x13, &in, &out);

	/* status code in AH. */
	return _ah (out.eax);
}

/**
*	DiskReadSectors
*
*	Description : Read sectors from disk. This function is designed to work with CHS devices.
*
*	Input : cylNumber - Cylinder / Track Number
*           sectorNum - Starting sector
*           headNum - Head / Side Number
*           driveNum - Drive Number
*           buffer - Pointer to buffer to read sector into.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE unsigned int DiskReadSectors (IN unsigned int cylNumber, IN unsigned int sectorNum, IN unsigned int  headNum,
				 IN unsigned int  driveNum, OUT unsigned char* buffer) {

	INTR     in;
	INTR     out;
	uint16_t _ES;
	uint16_t _DS;
	uint16_t _SS;
	uint16_t _GS;
	uint16_t _FS;

	if ( buffer == NULL )
		return 0;

	/* This call might modify certain registers
	so we save them here. */
	_asm {
		mov	ax, es
		mov _ES, ax
		mov	ax, ds
		mov _DS, ax
		mov	ax, ss
		mov _SS, ax
		mov	ax, gs
		mov _GS, ax
		mov	ax, fs
		mov _FS, ax
	}

	/* int 0x13 function 2. */
	_ah (in.eax) = 2;
	_al (in.eax) = 1;	/* numSectors */
	_cl (in.ecx) = (unsigned char) sectorNum;
	_ch (in.ecx) = (unsigned char) cylNumber;
	_dh (in.edx) = (unsigned char) headNum;
	_dl (in.edx) = (uint8_t) driveNum;
	in.es        = SEG    ( (uint32_t) buffer);
	_bx (in.ebx) = OFFSET ( (uint32_t) buffer);

	/* call BIOS. */
	io_services (0x13, &in, &out);

	/* Now we restore the saved segments. */
	_asm {
		mov	ax, ES
		mov es, ax
		mov	ax, DS
		mov ds, ax
		mov	ax, SS
		mov ss, ax
		mov	ax, GS
		mov gs, ax
		mov	ax, FS
		mov fs, ax
	}

	/* status code is in AH.  */
	return _ah (out.eax);
}

/**
*	DiskReset
*
*	Description : Reset drive read/write head.
*
*	Input : disk - BIOS disk number.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE unsigned int DiskReset (IN unsigned int disk) {

	INTR     in;
	INTR     out;

	/* int 0x13 function 0. */
	_ah (in.eax) = 0;
	_dl (in.edx) = (unsigned char) disk;
	io_services (0x13, &in, &out);

	/* status code is in AH.  */
	return _ah (out.eax);
}

/**
*	DiskGetParams
*
*	Description : Returns disk device paramaters.
*
*	Input : disk - BIOS disk number.
*           type - OUT device type code.
*           numCyl - OUT number of cylinders / Tracks.
*           SectorsPerTrack - OUT number of sectors per track.
*           maxHeads - OUT total number of heads.
*           numDisks - OUT total number of BIOS supported disk devices.
*           TotalSectors - OUT total number of sectors on this disk.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE unsigned int DiskGetParams (IN unsigned int disk, OUT unsigned int* type, OUT unsigned int* numCyl,
			   OUT unsigned int* SectorsPerTrack, OUT unsigned int* maxHeads,
			   OUT unsigned int* numDisks, OUT unsigned int* TotalSectors) {

	INTR     in;
	INTR     out;

	/* int 0x13 function 8. */
	_ah (in.eax) = 8;
	_dl (in.edx) = (uint8_t) disk;
	in.es = 0;
	in.edi.w.val = 0;

	/* call BIOS. */
	io_services (0x13, &in, &out);

	/* return information. */
	*type            = _bl (out.ebx);
	*numCyl          = _ch (out.ecx) + 1;
	*SectorsPerTrack = _cl (out.ecx);
	*maxHeads        = _dh (out.edx) + 1;
	*numDisks        = _dl (out.edx);
	*TotalSectors    = (*SectorsPerTrack) * (*numCyl) * (*maxHeads);

	/* status is in AH. */
	return _ah (out.eax);
}

/**
*	DiskGetNumberOfFdd
*
*	Description : Returns number of BIOS supported Floppy Disk Drives.
*
*	Output : Number of devices found.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE unsigned int DiskGetNumberOfFdd () {

	INTR     in;
	INTR     out;

	/* int 0x13 function 8. */
	_ah (in.eax) = 8;
	_dl (in.edx) = 0;
	io_services (0x13, &in, &out);

	/* result is in DL. */
	return _dl (out.edx);
}

/**
*	DiskGetNumberOfHdd
*
*	Description : Returns number of BIOS supported Hard Disk Drives.
*
*	Output : Number of devices found.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE unsigned int DiskGetNumberOfHdd () {

	INTR     in;
	INTR     out;

	/* int 0x13 function 8. */
	_ah (in.eax) = 8;
	_dl (in.edx) = 0x80;
	io_services (0x13, &in, &out);

	/* result is in DL. */
	return _dl (out.edx);
}

/**
*	DiskRawRead
*
*	Description : Reads a sector from a disk device. This is desigened to work with CHS devices.
*
*	Input : Drive - BIOS Drive Number.
*           Sector - Starting sector.
*           Buffer - OUT Pointer to buffer to read into.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE unsigned int DiskRawRead (IN unsigned int Drive, IN unsigned int Sector, OUT unsigned char* Buffer) {

	DISK_STATUS stat;
	unsigned int DiskType;
	unsigned int NumCylinders;
	unsigned int NumSectors;
	unsigned int NumHeads;
	unsigned int TotalSectors;
	unsigned int AbsHead;
	unsigned int AbsCylinder;
	unsigned int AbsSector;
	unsigned int NumDisks;
	uint8_t      SectorData [SECTOR_SIZE+1];

	/* clear temporary buffer. */
	BlrZeroMemory (SectorData, SECTOR_SIZE);

	/* get disk information. */
	DiskGetParams (Drive, &DiskType, &NumCylinders, &NumSectors, &NumHeads, &NumDisks, &TotalSectors);

	/* if boot sector, attempt to read it. */
	if (Sector == 0) {

		stat = DiskReadSectors (0, 1, 0, Drive, SectorData);

		/* if reading failed, do not attempt to copy anything
		to output buffer. Just return code. */
		if (stat != SUCCESSFUL_COMPLETION)
			return stat;

		BlrCopyMemory (Buffer, SectorData, SECTOR_SIZE);
		return stat;
	}

	/* convert LBA to CHS. */
	AbsCylinder		= Sector / (NumHeads * NumSectors);
	AbsHead			= (Sector % (NumHeads * NumSectors)) / NumSectors;
	AbsSector		= (Sector % (NumHeads * NumSectors)) % NumSectors + 1;

	/* read sector. */
	stat = DiskReadSectors (AbsCylinder, AbsSector, AbsHead, Drive, SectorData);

	/* if success, copy it into output buffer. */
	if (stat == SUCCESSFUL_COMPLETION) {
		BlrCopyMemory (Buffer, SectorData, SECTOR_SIZE);
	}

	/* return status. */
	return stat;
}

/**
*	_DiskRead
*
*	Description : Reads a sector from a disk device.
*
*	Input : Drive - BIOS Drive Number.
*           Sector - Starting sector.
*           Buffer - OUT Pointer to buffer to read into.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE unsigned int _DiskRead(IN uint32_t diskNumber, IN uint32_t sector, IN addr_t memory) {

	unsigned int result = NOT_READY;

	do {

		/* read from device. */
		if (diskNumber < 0x80)
			result = DiskRawRead(diskNumber, sector, (unsigned char*) memory);
		else
			result = DiskReadSectorsEx(diskNumber, sector, SEG(memory), OFFSET(memory));

	} while (result == NOT_READY);

	return result;
}

/**
*	DiskRead
*
*	Description : Reads a sector from a disk device.
*
*	Input : dev - Disk Device Object
*           sector - Starting sector.
*           offset - OUT Offset from the starting sector.
*           size - Size in bytes to read from the offset.
*           buffer - OUT Output buffer for the data read.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE size_t DiskRead (IN nblDeviceObject* dev, IN uint32_t sector, IN uint32_t offset, IN uint32_t size, OUT void* buffer) {

	DISK_DEVICE_OBJECT* disk;
	unsigned char*      out;

	/* these should never be NULL. */
	if (dev == NULL || buffer == NULL)
		return 0;

	/* make sure we created the device object. */
	if (dev->driverObject != _self)
		return 0;

	/* initialize. */
	out                 = (unsigned char*) buffer;
	disk                = (PDISK_DEVICE_OBJECT) dev->ext;

	/* if offset > sector_size, recompute actual start sector. */
	while (offset > disk->BytesPerSector) {

		/* adjust. */
		sector++;
		offset -= disk->BytesPerSector;
	}

	if (offset + size > disk->BytesPerSector) {

		unsigned char temp[512*2];

		/* here we have to avoid the cache since this spans across sectors. */
		_DiskRead(disk->DiskNumber, sector, (addr_t) temp);
		_DiskRead(disk->DiskNumber, sector+1, (addr_t) (&temp[512]));

		BlrCopyMemory(buffer, &temp[offset], size);
	}
	else if (sector != CACHE.sector) {

		_DiskRead(disk->DiskNumber, sector, (addr_t) CACHE.data);

		CACHE.sector = sector;
		CACHE.offset = offset;

		BlrCopyMemory(buffer, &CACHE.data[offset], size);
	}
	else {

		BlrCopyMemory(buffer, &CACHE.data[offset], size);
	}
	return size;
}

/**
*	DiskWrite
*
*	Description : This function is not implemented.
*
*	Input : dev - Disk Device Object
*           sector - Starting sector.
*           offset - OUT Offset from the starting sector.
*           size - Size in bytes to read from the offset.
*           buffer - OUT Output buffer for the data read.
*
*	Output : BIOS Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE size_t DiskWrite (IN nblDeviceObject* dev, IN uint32_t sector, IN uint32_t offset, IN uint32_t size, IN void* buffer) {

   /* not yet supported. */
   DPRINT ("BIOS Driver does not support disk writing.");
   return 0;
}

/**
*	DiskRegisterDevice
*
*	Description : Register a new Disk Device Object.
*
*	Input : dm - Driver Manager.
*           ext - Disk Device Extended Block.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t DiskRegisterDevice (IN nblDriverObject* dm, IN PDISK_DEVICE_OBJECT ext) {

	STATIC int       i = 0;

	nblDeviceObject* disk;
	nblMessage       m;
	char*            name;

	if (!ext)
		return NBL_STAT_INVALID_ARGUMENT;

	disk = BlrAlloc (sizeof (nblDeviceObject));
	if (!disk)
		return NBL_STAT_MEMORY;

	name = BlrAlloc (10);
	if (!name) {
		BlrFree (disk);
		return NBL_STAT_MEMORY;
	}

	BlrZeroMemory (name, 10);
	BlrSPrintf (name, "disk(%i)", i);

	/* initialize device object. */
	disk->type            = NBL_DEVICE_DISK_CONTROLLER;
	disk->devclass        = NBL_DEVICE_CONTROLLER_CLASS;
	disk->characteristics = 0;
	disk->driverObject    = _self;
	disk->attached        = NULL;
	disk->next            = NULL;
	disk->name            = name;
	disk->ext             = ext;

	/* register it. */
	m.source = _self;
	m.type   = NBL_DEV_REGISTER;
	m.NBL_DEV_REGISTER_OBJ = disk;
	dm->request (&m);

	/* prepare for next device. */
	i++;
	return NBL_STAT_SUCCESS;
}

/**
*	DiskInitializeDevices
*
*	Description : Scans all disks in the system and registers device objects.
*
*	Input : dm - Driver Manager.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void DiskInitializeDevices (IN nblDriverObject* dm) {

	unsigned int fddCount;
	unsigned int hddCount;
	unsigned int i;
	unsigned int driveType;
	unsigned int sectorCount;
	unsigned int result;

	/* get number of drives */
	fddCount = DiskGetNumberOfFdd ();
	hddCount = DiskGetNumberOfHdd ();

	/* scan floppy drives */
	for (i = 0; i < fddCount; i++) {

		PDISK_DEVICE_OBJECT disk;

		/* get drive info */
		result = DiskGetType (i, &driveType, &sectorCount);

		/* floppy without change-line support (1), floppy with it (2) */
		if (result == 1 || result == 2) {

			/* allocate driver extended data. */
			disk = BlrAlloc (sizeof (DISK_DEVICE_OBJECT));
			if (disk == NULL) {
				return; /* this is fatal. */
			}

			/* initialize. */
			disk->BytesPerSector  = SECTOR_SIZE;
			disk->NumberOfSectors = sectorCount;
			disk->DiskNumber      = i;

			/* register device. */
			if (DiskRegisterDevice (dm, disk) != NBL_STAT_SUCCESS) {
				BlrFree(disk);
				return;	/* this is fatal. */
			}
		}
	}

	/* scan hard drives */
	for (i = 0; i < hddCount; i++) {

		PDISK_DEVICE_OBJECT disk;

		/* get drive info */
		result = DiskGetType (i + 0x80, &driveType, &sectorCount);

		/* hard disk */
		if (result == 3) {

			/* allocate driver extended data. */
			disk = BlrAlloc (sizeof (DISK_DEVICE_OBJECT));
			if (disk == NULL) {
				return; /* this is fatal. */
			}

			/* initialize. */
			disk->BytesPerSector  = SECTOR_SIZE;
			disk->NumberOfSectors = sectorCount;
			disk->DiskNumber      = i + 0x80;

			/* register device. */
			if (DiskRegisterDevice (dm, disk) != NBL_STAT_SUCCESS) {
				BlrFree(disk);
				return;	/* this is fatal. */
			}
		}
	}
}
