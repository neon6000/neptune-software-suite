/************************************************************************
*
*	idt.c - Interrupt Descriptor Table.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>

/*** PRIVATE Definitions **********************************/

typedef enum _HalDpl {
	DplRing0,
	DplRing1,
	DplRing2,
	DplRing3
}HalDpl;

/* IDT gate types. */
typedef enum _HalIdtGateType {
	TaskGate32Bit = 5,
	InterruptGate16Bit = 6,
	TrapGate16Bit = 7,
	InterruptGate32Bit = 0xe,
	TrapGate32Bit = 0xf
}HalIdtGateType;

/* IDT Entry. */
typedef struct _HalIdt {
	uint16_t base;
	uint16_t sel;
	uint8_t  reserved;
	struct {
		uint8_t gateType : 4;
		uint8_t storage : 1;
		uint8_t dpl : 2;
		uint8_t present : 1;
	}flags;
	uint16_t baseHi;
}HalIdt;

/* IDTR Descriptor. */
typedef struct _HalIdtr {
	uint16_t limit;
	uint32_t base;
}HalIdtr;


/* Global Interrupt Descriptor Table. */
#define KIDT_MAX 256
HalIdt  _kidt[KIDT_MAX];
HalIdtr _kidtr;

extern void i386LoadInterruptTable(addr_t idtr);

/**
*	Set IDTR register for current CPU
*/
PRIVATE void HalSetIdtr(void) {

	_kidtr.base = (uint32_t)&_kidt[0];
	_kidtr.limit = (sizeof(HalIdt)* KIDT_MAX) - 1;
	i386LoadInterruptTable(&_kidtr);
}

/**
*	Set IDT descriptor gate
*/
PRIVATE void IdtSetGate(IN index_t idx, IN addr_t irq,
	IN uint8_t sel, IN uint8_t gate, IN uint8_t dpl) {

	ASSERT(idx < KIDT_MAX);

	_kidt[idx].base = (irq & 0x0000ffff);
	_kidt[idx].baseHi = (irq & 0xffff0000) >> 16;
	_kidt[idx].flags.gateType = gate;
	_kidt[idx].flags.dpl = dpl;
	_kidt[idx].flags.storage = 0;
	_kidt[idx].sel = sel;
	_kidt[idx].reserved = 0;
	_kidt[idx].flags.present = 1;
}

/*** PUBLIC Definitions **********************************/


/**
*	Returns IDT gate descriptor.
*/
PUBLIC HalIdt* HalGetIdtGate(IN index_t idx) {

	ASSERT(idx < KIDT_MAX);
	return &_kidt[idx];
}

/**
*	Exception service routines.
*/
extern void i386DivideByZero(void);
extern void i386DebugException(void);
extern void i386NMIException(void);
extern void i386Breakpoint(void);
extern void i386Overflow(void);
extern void i386BoundException(void);
extern void i386InvalidOpcode(void);
extern void i386FPUNotAvailable(void);
extern void i386DoubleFault(void);
extern void i386CoprocessorSegment(void);
extern void i386InvalidTss(void);
extern void i386SegmentNotPresent(void);
extern void i386StackException(void);
extern void i386GeneralProtectionFault(void);
extern void i386PageFault(void);
extern void i386CoprocessorError(void);
extern void i386AlignmentCheck(void);
extern void i386MachineCheck(void);

/**
*	Initializes interrupt descriptor table.
*/
PUBLIC void HalInitializeIdt(void) {

	BlrZeroMemory(_kidt, sizeof(HalIdt)* KIDT_MAX);

	IdtSetGate(0, (addr_t)i386DivideByZero, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(1, (addr_t)i386DebugException, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(2, (addr_t)i386NMIException, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(3, (addr_t)i386Breakpoint, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(4, (addr_t)i386Overflow, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(5, (addr_t)i386BoundException, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(6, (addr_t)i386InvalidOpcode, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(7, (addr_t)i386FPUNotAvailable, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(8, (addr_t)i386DoubleFault, 8, TaskGate32Bit, DplRing0);
	IdtSetGate(9, (addr_t)i386CoprocessorSegment, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(10, (addr_t)i386InvalidTss, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(11, (addr_t)i386SegmentNotPresent, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(12, (addr_t)i386StackException, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(13, (addr_t)i386GeneralProtectionFault, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(14, (addr_t)i386PageFault, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(15, (addr_t)i386CoprocessorError, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(16, (addr_t)i386AlignmentCheck, 8, TrapGate32Bit, DplRing0);
	IdtSetGate(17, (addr_t)i386MachineCheck, 8, TrapGate32Bit, DplRing0);

	HalSetIdtr();
}
