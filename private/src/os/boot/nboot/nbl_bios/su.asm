;*******************************************************
;	su.asm
;		protected mode <> real mode interface module
;
;	Copyright BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

%ifndef __SU_ASM_INCLUDED
%define __SU_ASM_INCLUDED

bits 32

SECTION .text


extern _DbgExceptionHandler

;=== Exceptions ====================================

;
; Processor Generated Exceptions
;
global _i386DivideByZero
global _i386DebugException
global _i386NMIException
global _i386Breakpoint
global _i386Overflow
global _i386BoundException
global _i386InvalidOpcode
global _i386FPUNotAvailable
global _i386DoubleFault
global _i386CoprocessorSegment
global _i386InvalidTss
global _i386SegmentNotPresent
global _i386StackException
global _i386GeneralProtectionFault
global _i386PageFault
global _i386CoprocessorError
global _i386AlignmentCheck
global _i386MachineCheck

;===============================================================;
;    void i386ExceptionEntry ()                                 ;
;        -Exception point entry                                 ;
;                                                               ;
;    CALLING ENVIRONMENT: PROTECTED MODE                        ;
;===============================================================;

_i386ExceptionEntry:
	;
	; Call exception handler.
	;
	pushad
	mov eax, ss
	push eax	; we copy to eax in order to clear the high bytes of the stack element.
	mov eax, ds
	push eax
	mov eax, es
	push eax
	mov eax, fs
	push eax
	mov eax, gs
	push eax
	push dword esp		; pointer to stack frame.
	push dword 0		; nested exception flag.
	call _DbgExceptionHandler
	add esp, 8
	pop gs
	pop fs
	pop es
	pop ds
	pop ss
	popad
	;
	; ExceptionWithErrorCode and ExceptionWithoutErrorCode
	; push an error code and vector number on stack, so
	; we pop them now.
	;
	add esp, 8
	iret

;
; Calls exception handler. It is
; assumed CPU already pushed error code.
;
%macro ExceptionWithErrorCode 1
	push dword %1
	jmp _i386ExceptionEntry
%endmacro

;
; Calls exception handler with
; a NULL error code.
;
%macro ExceptionWithoutErrorCode 1
	push dword 0
	push dword %1
	jmp _i386ExceptionEntry
%endmacro

;
; Exceptions.
;
_i386DivideByZero:           ExceptionWithoutErrorCode(0)
_i386DebugException:         ExceptionWithoutErrorCode(1)
_i386NMIException:           ExceptionWithoutErrorCode(2)
_i386Breakpoint:             ExceptionWithoutErrorCode(3)
_i386Overflow:               ExceptionWithoutErrorCode(4)
_i386BoundException:         ExceptionWithoutErrorCode(5)
_i386InvalidOpcode:          ExceptionWithoutErrorCode(6)
_i386FPUNotAvailable:        ExceptionWithoutErrorCode(7)
_i386DoubleFault:            ExceptionWithErrorCode(8)
_i386CoprocessorSegment:     ExceptionWithoutErrorCode(9)
_i386InvalidTss:             ExceptionWithErrorCode(10)
_i386SegmentNotPresent:      ExceptionWithErrorCode(11)
_i386StackException:         ExceptionWithErrorCode(12)
_i386GeneralProtectionFault: ExceptionWithErrorCode(13)
_i386PageFault:	             ExceptionWithErrorCode(14)
_i386CoprocessorError:       ExceptionWithoutErrorCode(15)
_i386AlignmentCheck:         ExceptionWithoutErrorCode(16)
_i386MachineCheck:           ExceptionWithoutErrorCode(17)

;=== Interrupt Vector Table ================================

global _i386LoadInterruptTable

_i386LoadInterruptTable:

%define IDTR_POINTER [esp+4]

	mov ebx, IDTR_POINTER
	lidt [ebx]
	ret

;
; The following defines the constants required for operation. They
; are set up by NSTART when enabling protected mode.
;
%define NULL_SEL       0
%define CODE_SEL       8
%define DATA_SEL       0x10
%define CODE_SEL_16BIT 0x18
%define DATA_SEL_16BIT 0x20

;
; The following defines the protected mode base address and real
; mode segment base for NBOOT. These fields must reflect the
; actual image base address of NBOOT. If a change occurs, update
; the following fields as required.
;
%define NBOOT_IMAGE_BASE 0x20000
%define NBOOT_IMAGE_BASE_SEGMENT 0x2000

;===========================================;
;	idt descriptor
;===========================================;

struc idt_entry
   .m_baseLow	resw   1
   .m_selector	resw   1
   .m_reserved	resb   1
   .m_flags		resb   1
   .m_baseHi	resw   1
endstruc

;---------------------------------------------
;	idtr
;---------------------------------------------

struc idt_ptr
   .m_size         resw   1
   .m_base         resd   1
endstruc

;----------------------------------------------
;    regs structure
;----------------------------------------------

struc REGS2
   .m_eax		resd    1
   .m_ebx		resd    1
   .m_ecx		resd    1
   .m_edx		resd    1
   .m_esi		resd    1
   .m_edi		resd    1
   .m_ebp		resd	1
   .m_esp		resd	1
   .m_eip		resd	1
   .m_cs        resw    1
   .m_ds        resw    1
   .m_es        resw    1
   .m_ss        resw    1
   .m_fs		resw	1
   .m_gs		resw	1
   .m_flags     resw    1; 50 bytes
endstruc

REGS_LOCAL2:
istruc REGS2
   at REGS2.m_eax, dd 0
   at REGS2.m_ebx, dd 0
   at REGS2.m_ecx, dd 0
   at REGS2.m_edx, dd 0
   at REGS2.m_esi, dd 0
   at REGS2.m_edi, dd 0
   at REGS2.m_ebp, dd 0
   at REGS2.m_esp, dd 0
   at REGS2.m_eip, dd 0
   at REGS2.m_cs, dw 0
   at REGS2.m_ds, dw 0
   at REGS2.m_es, dw 0
   at REGS2.m_ss, dw 0
   at REGS2.m_fs, dw 0
   at REGS2.m_gs, dw 0
   at REGS2.m_flags,dw 0
iend

; protected mode stack location
stack2 dd 0

; real mode ivt pointer
ivt_ptr:
   istruc idt_ptr
      at idt_ptr.m_size, dw 0
      at idt_ptr.m_base, dd 0
   iend

; protected mode idt pointer
pmode_idt2:
   istruc idt_ptr
      at idt_ptr.m_size, dw 0
      at idt_ptr.m_base, dd 0
   iend

; protected mode gdt
pmode_gdt2:
      dw 0
      dd 0

is_error:
      dd 0

;===============================================================;
;    void io_services (int function, REGS* in, REGS* out)       ;
;        -16bit input/output services                           ;
;                                                               ;
;    int function: 16bit interrupt number to call               ;
;    REGS* in: input registers for interrupt call               ;
;    REGS* out: output registers from interrupt call            ;
;    CALLING ENVIRONMENT: PROTECTED MODE                        ;
;===============================================================;

global _io_services

_io_services:

	pushad
	mov dword [is_error], 0
	;
	; get local copy of REGS in parameter
	;
	cld

;; 	mov        esi, [esp + 32 + 8]

 	mov        esi, [esp + 32 + 8]
 	
 	mov        edi, REGS_LOCAL2
	mov        ecx, 13
	rep        movsd
	;
	; save protected mode stack and idtr
	;
 	mov        [stack2], esp
	cli
	sgdt       [pmode_gdt2]
	sidt       [pmode_idt2]
	;
 	; install real mode ivt
 	;
	mov        word [ivt_ptr + idt_ptr.m_size], 0xffff
	mov        dword [ivt_ptr + idt_ptr.m_base], 0
	lidt       [ivt_ptr]
	;
	; jump to 16 bit protected mode
	;
	jmp        CODE_SEL_16BIT:.pmode16

bits 16

.pmode16:
	;
	; go into real mode
	;
	cli
	mov	eax, cr0
	and	al, 0FEh		; clear the protected mode bit in cr0
 	and	eax, 0x7fffffff ; clear paging bit
	mov	cr0, eax
	;
	; COFF format doesnt support 16 bit relocations :p this is my way to fix that
	;
	mov	ebx, NBOOT_IMAGE_BASE_SEGMENT
	shl	ebx, 16
	mov	eax, .rmode - NBOOT_IMAGE_BASE
	mov	bx, ax
	;
	; jump to real mode
	;
	push dword ebx
	retf

.rmode:
	;
	; set all segments
	;
	mov		ax, NBOOT_IMAGE_BASE_SEGMENT
	mov		fs, ax
	mov		gs, ax
	mov		ds, ax
	mov		es, ax
;;	mov		ss, ax
	;
	; Adjust stack for real mode use.
	;
	mov     ax, 0
	mov     ss, ax
	;
	; set int number to call (self modifying code here)
	;
	mov		dl, [ esp + 32 + 4 ]
	mov		ebx, .call_service - NBOOT_IMAGE_BASE
	mov		byte [ ebx + 1 ], dl
	;
	; old code here for reference. We changed SS:SP=>0:original_sp
	; to avoid farther stack issues.
	;
;;	mov		dl, [ (esp - NBOOT_IMAGE_BASE) + 32 + 4 ]
;;	mov		ebx, .call_service - NBOOT_IMAGE_BASE
;;	mov		byte [ ebx + 1 ], dl
	;
	; set registers
	;
	mov		ebx, REGS_LOCAL2 - NBOOT_IMAGE_BASE
	mov		eax, [ebx+REGS2.m_eax]
	mov		ecx, [ebx+REGS2.m_ecx]
	mov		edx, [ebx+REGS2.m_edx]
	mov		esi, [ebx+REGS2.m_esi]
	mov		edi, [ebx+REGS2.m_edi]
	mov		ebp, [ebx+REGS2.m_ebp]
	mov		es, [ebx+REGS2.m_es]
	mov		fs, [ebx+REGS2.m_fs]
	mov		gs, [ebx+REGS2.m_gs]
	mov		ebx, [ebx+REGS2.m_ebx]
;;	mov		ds, [ebx+REGS2.m_ds]		;;dunno if itll work

;	push	word [ebx+REGS2.m_ds]
;	pop		ds
;;	sti

	;
	; invoke BIOS interrupt. Warning: self modifying code being used
	;
.call_service:
	int        0
	;
	; here we assume that if CF is set, its an error
	;
 	jnc        .cont
 	mov        ebx, is_error - NBOOT_IMAGE_BASE
	mov        byte [ebx], 1

.cont:
	;
	; save return registers
	;
	pushf
	push		edx
	mov		edx, REGS_LOCAL2 - NBOOT_IMAGE_BASE
	mov		[edx+REGS2.m_ecx], ecx
	mov		[edx+REGS2.m_eax], eax
	mov		[edx+REGS2.m_ebx], ebx
	mov		[edx+REGS2.m_esi], esi
	mov		[edx+REGS2.m_edi], edi
	mov		[edx+REGS2.m_ebp], ebp
	mov		[edx+REGS2.m_es], es
	mov		[edx+REGS2.m_fs], fs
	mov		[edx+REGS2.m_gs], gs
	pop		edx
	mov		ebx, REGS_LOCAL2 - NBOOT_IMAGE_BASE
	mov		[ebx+REGS2.m_edx], edx
	pop		word [ebx+REGS2.m_flags]
	;
	; disable interrups
	;
	cli
	;
	; enable protected mode
	;
	mov    eax, cr0
 	or     eax, 1
	mov    cr0, eax
	;
	; jump into protected mode
	;
	jmp	dword CODE_SEL:.return

bits 32

.return:
	;
 	; restore selectors and stack
	;
	mov        ax, DATA_SEL
	mov        fs, ax
	mov        gs, ax
	mov        ds, ax
	mov        es, ax
	mov        ss, ax
	mov        esp, [stack2]
	;
	; copy output paramater
	;
	cld
	mov        esi, REGS_LOCAL2
	mov        edi, [esp+32+12]
	mov        ecx, 13
	rep        movsd
	;
	; restore registers and idtr
	;
	popad
	lgdt    [pmode_gdt2]
	lidt    [pmode_idt2]
	;
	; enable paging
	;
;;	mov	eax, cr0
;;	or	eax, 0x80000000
;;	mov	cr0, eax
	;
	; set error code and return
	;
	mov		eax, [is_error]
;;	sti
	ret

;==============================================
;
; void _cdecl chainload_rmode (int drive)
; Goes into real mode and calls 0x7c0:0
; CALLING ENVIRONMENT: PROTECTED MODE
;
;==============================================

global _chainload_rmode

_chainload_rmode:
	;
	; save state
	;
	pushad
	;
	; install real mode ivt
	;
	mov        word [ivt_ptr+idt_ptr.m_size], 0xffff
	mov        dword [ivt_ptr+idt_ptr.m_base], 0
	lidt       [ivt_ptr]
	;
	; jump to 16 bit protected mode
	;
 	jmp        CODE_SEL_16BIT:.pmode16

bits 16

.pmode16:
	;
	; go into real mode
	;
	cli
	mov eax, cr0
	and al, 0FEh		; clear the protected mode bit in cr0
 	and	eax, 0x7fffffff ; clear paging bit
 	mov cr0, eax
	;
	; COFF format doesnt support 16 bit relocations :p this is my way to fix that
	;
	mov	ebx, NBOOT_IMAGE_BASE_SEGMENT
	shl	ebx, 16
	mov	eax, .rmode - NBOOT_IMAGE_BASE
	mov	bx, ax
	;
	; jump to real mode
	;
	push dword ebx
	retf

.rmode:
	;
	; set all segments
	;
	mov			ax, NBOOT_IMAGE_BASE_SEGMENT
	mov			fs, ax
	mov			gs, ax
	mov			ds, ax
	mov			es, ax
	mov			ss, ax
	;
	; get boot drive paramater(BIOS passes to us, bootsect might need it)
	;
	mov     dl, [(esp-NBOOT_IMAGE_BASE)+32+4]
	;
	; call 0x7c0:0 boot sector
	;
	push 0x7c0
	push 0
	retf
	;
	; fallback; if we get here bail out
	;
	cli
	hlt
	ret			; shouldnt get here

;==============================================
;
; void _cdecl BlPs2BiosDeviceHandler (uint16_t PO, uint8_t xData,
;                                     uint8_t yData, uint16_t status)
; Ps2 interrupt handler called by BIOS
; CALLING ENVIRONMENT: REAL MODE
;
;==============================================

;
; mouse scratchspace at 0x500
;
%define MOUSE_SCRATCH_SEG 0x50
%define MOUSE_SCRATCH_OFF_X 0
%define MOUSE_SCRATCH_OFF_Y 2
%define MOUSE_SCRATCH_STAT  4

global _BlPs2BiosDeviceHandler

; bp + sizeof (push bp) + sizeof (push cs, push ip) + loc of operand
%define PO     bp+6
%define xData  bp+6+2
%define yData  bp+6+4
%define status bp+6+6

_BlPs2BiosDeviceHandler:
	;
	; clear interrupts
	;
	cli
	;
	; set bp to top of stack
	;
	push		bp
	mov			bp, sp
	;
	; set DS
	;
	push		ds
	push		ax
	mov			ax, MOUSE_SCRATCH_SEG
	mov			ds, ax
	;
	; set status
	;
	mov ax, word [status]
	mov word [MOUSE_SCRATCH_STAT], ax
	;
	; set X
	;
	mov bx, word [MOUSE_SCRATCH_OFF_X]
	mov ax, word [xData]
	mov word [MOUSE_SCRATCH_OFF_X], ax
	;
	; set Y
	;
	mov ax, word [MOUSE_SCRATCH_OFF_Y]
	mov ax, word [yData]
	mov word [MOUSE_SCRATCH_OFF_Y], ax
	;
	; set interrupts and far return back to BIOS (note no IRET)
	;
	pop		ax
	pop		ds
	pop		bp
	sti
	retf

%endif
