/************************************************************************
*
*	BIOS memory driver.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "bios.h"

/* The memory driver component implements the BIOS memory
device object. */

/* Note system has 2 heaps. One for kernel+general allocations.
The second is for information that will be passed to the kernel,
which includes boot paramater block data structures and boot modules
that are loaded. */

/*** PRIVATE DECLARATIONS ***/

#define PAGE_SIZE    4096
#define PAGE_ALIGN   PAGE_SIZE
#define PFN_PER_BYTE 8 /* used with PFN alloctaor. */

/* helper macros. */
#define BYTES_TO_PAGES(bytes)	(((bytes) + 0xfff) >> 12)
#define PAGES_TO_BYTES(pages)	((pages) << 12)

/* The minimum and maximum heap size.  */
#define MIN_HEAP_SIZE	0x100000
#define MAX_HEAP_SIZE	(1600 * 0x100000)

#define MIN_HEAP_START  0x100000

/* ZONE_LOW. Please see bios.c memory layout before modifying! */
#define MIN_HEAP2_START 0xA000
#define MIN_HEAP2_END   0x20000
#define MIN_HEAP2_SIZE  MIN_HEAP2_END - MIN_HEAP2_START 

/* The size of a memory map obtained from the firmware. This must be
   a multiplier of 4KB.  */
#define MEMORY_MAP_SIZE	0x3000

/* self reference. */
STATIC nblDriverObject* _self = 0;

/* memory device object. */
NBL_DRIVER_OBJECT_DEFINE _nBiosMmDriverObject;
STATIC nblDeviceObject _mm = {
	NBL_DEVICE_MEMORY_UNIT,
	NBL_DEVICE_MEMORY_CLASS,
	0, &_nBiosMmDriverObject, 0, 0, "mm", 0
};

/*** PRIVATE DECLARATIONS ***/

PRIVATE void* MmAllocatePages (IN addr_t phys, IN uint32_t pages);
PRIVATE void MmFreePages(IN addr_t phys, IN uint32_t pages);
PRIVATE status_t MmEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]);
PRIVATE status_t MmInit (IN NBL_MM_INIT_FUNC func);
PRIVATE status_t MmRequest (IN nblMessage* rpb);

/*** PRIVATE DEFINITIONS ***/

/**
*	MmAllocatePages
*
*	Description : Allocates system pages.
*
*	Input : phys - Ignored.
*           pages - Ignored.
*
*	Output : Pointer to requested memory block.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void* MmAllocatePages (IN addr_t phys, IN uint32_t pages) {

	/* return requested pointer. */
	return (void*) phys;
}

/**
*	MmFreePages
*
*	Description : Frees system pages.
*
*	Input : phys - Ignored.
*           pages - Ignored.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void MmFreePages(IN addr_t phys, IN uint32_t pages) {

	/* nothing to do. */
	return;
}

/**
*	MmGetMapEntry
*
*	Description : Returns a memory map entry.
*
*	Input : memory - Memory Descriptor.
*           index - Memory Descriptor Index.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE size_t MmGetMapEntry(OUT MMAP_DESCRIPTOR* memory, IN index_t index) {

	INTR             in;
	INTR             out;

	in.eax.val = 0xe820;
	in.edx.val = 0x534D4150; /* "SMAP" */
	in.ebx.val = index;
	in.ecx.val = sizeof (MMAP_DESCRIPTOR);

	/* es:di->buffer */
	in.es      = SEG    ((addr_t) memory);
	in.edi.val = OFFSET ((addr_t) memory);

	/* int 0x15 function e820 */
	io_services(0x15,&in,&out);

	/* if ebx = 0, we are done. */
	if (out.ebx.val == 0)
		return 0;

	/* return number of actual bytes written. */
	return out.ecx.val;
}

/**
*	MmGetMap
*
*	Description : Returns the system memory map.
*
*	Input : memory - Output array of memory descriptors.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE size_t MmGetMap (OUT MMAP_DESCRIPTOR* memory) {

	MMAP_DESCRIPTOR tmp;
	index_t index = 0;
	size_t  memoryMapSize = 0;

	/* count number of entries. */
	while (MmGetMapEntry(&tmp, index))
		index++;
	index++;

	/* get amount of memory needed for memory map. */
	memoryMapSize = index * sizeof (MMAP_DESCRIPTOR);
	if (! memory)
		return memoryMapSize;

	/* and copy memory map to output buffer. */
	index = 0;
	while (MmGetMapEntry(&tmp, index)) {
		BlrCopyMemory(&memory[index], &tmp, sizeof(MMAP_DESCRIPTOR));
		index++;
	}
	BlrCopyMemory(&memory[index], &tmp, sizeof(MMAP_DESCRIPTOR));
	return memoryMapSize;
}

/**
*	MmEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t MmEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	nblMessage rpb;
	STATIC _initialize = FALSE;

	if (_initialize)
		return NBL_STAT_SUCCESS;

	_self = self;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	/* create memory device. */
	rpb.source = self;
	rpb.type   = NBL_DEV_REGISTER;
	rpb.NBL_DEV_REGISTER_OBJ = &_mm;
	if (dm->request (&rpb) != NBL_STAT_SUCCESS)
		return NBL_STAT_DEPENDENCY;

	_initialize = TRUE;
	return NBL_STAT_SUCCESS;
}

/**
*	MmInit
*
*	Description : Initialize firmware specific memory management services.
*
*	Input : func - NBL Core Library callback initialization function.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t MmInit (IN NBL_MM_INIT_FUNC func) {

	STATIC addr_t _base        = MIN_HEAP_START;

	if (!func)
		return NBL_STAT_INVALID_ARGUMENT;

	/* initialize heaps. */

	func (MIN_HEAP2_START, MIN_HEAP2_SIZE, ZONE_LOW);
	func (_base, MIN_HEAP_SIZE, ZONE_NORMAL);

	return NBL_STAT_SUCCESS;
}

/**
*	MmRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t MmRequest (IN nblMessage* rpb) {

	rettype_t r = 0;

	rpb->retval = 0;
	switch (rpb->type) {
		case NBL_MM_ALLOC:
			r = (rettype_t) MmAllocatePages (rpb->NBL_MM_ALLOC_ADDRESS, rpb->NBL_MM_ALLOC_SIZE);
			break;
		case NBL_MM_FREE:
			MmFreePages(rpb->NBL_MM_FREE_ADDRESS,rpb->NBL_MM_FREE_SIZE);
			break;
		case NBL_MM_GETMAP:
			r = MmGetMap((MMAP_DESCRIPTOR*) rpb->NBL_MM_GETMAP_ADDRESS);
			break;
		case NBL_MM_INIT:
			r = (rettype_t) MmInit ( (NBL_MM_INIT_FUNC) rpb->NBL_MM_INIT_FUNCTION);
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_BIOS_MM_VERSION 1

/* EFI driver object. */
NBL_DRIVER_OBJECT_DEFINE _nBiosMmDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_BIOS_MM_VERSION,
	NBL_DRV_LOAD_CORE,
	NBL_DRV_CLASS_FW,
	"mm",
	MmEntryPoint,
	MmRequest,
	1,
	"dm"
};
