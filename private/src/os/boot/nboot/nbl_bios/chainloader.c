/************************************************************************
*
*	chainloader.c - BIOS Chainloader
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "bios.h"

#define SECTOR_SIZE 512

/* we need a way to get actual number and bytesPerSector
of device for this to work with virtual devices. No way
around it. */

/* this must be compatible with BIOS disk driver */
typedef struct _DISK_DEVICE_OBJECT {
	uint32_t NumberOfSectors;
	uint32_t BytesPerSector;
	uint32_t DiskNumber;
}DISK_DEVICE_OBJECT, *PDISK_DEVICE_OBJECT;

/**
*	Chainload
*
*	Description : Attempts to chainload a boot program.
*
*	Input: devicePath - Device Path of boot program to execute.
*
*	Return value : Status.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t Chainload (IN char* devicePath) {

	nblDeviceObject*    disk;
	PDISK_DEVICE_OBJECT ex;

	/* get disk device. */
	disk = BlrOpenDevice(devicePath);

	if (disk == NULL)
		return NBL_STAT_DEVICE;
	ex = (PDISK_DEVICE_OBJECT) disk->ext;

	/* read boot sector to 0x7c00. Note that the
	device object may be virtual (such as a volume) so this
	works for partitions and virtual devices too. */
	BlrDiskRead (disk,0,0,SECTOR_SIZE,(void*) 0x7c00);

	/* make sure it has a valid boot signature. */
	if ( *((unsigned short*)0x7dfe) != 0xaa55)
		return NBL_STAT_DEVICE;

	/* call boot sector at 0x7c0:0. */
	chainload_rmode (ex->DiskNumber);

	/* if we ever get here, we are in trouble. */
	return NBL_STAT_DRIVER;
}

/**
*	BiosChainloaderEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t BiosChainloaderEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	/* nothing to do. */
	return NBL_STAT_SUCCESS;
}

/**
*	BiosChainloaderRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t BiosChainloaderRequest (IN nblMessage* rpb) {

	rettype_t r;
	rpb->retval = 0;
	switch (rpb->type) {
		case NBL_CHAINLOADER_BOOT:
			r = Chainload ((char*) rpb->NBL_CHAINLOADER_BOOT_DEVICE);
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_BIOS_CHAINLOADER_VERSION 1

/* Chainloader driver object. */
NBL_DRIVER_OBJECT_DEFINE _nBiosChainloaderDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_BIOS_CHAINLOADER_VERSION,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_OTHER,
	"chainloader",
	BiosChainloaderEntryPoint,
	BiosChainloaderRequest,
	0
};
