/************************************************************************
*
*	NBL BIOS Firmware Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "bios.h"

/*** PUBLIC definitions. ***/

PUBLIC void BlFwStartup (void);

/*** PRIVATE definitions. ***/

/**
*	APM Install Check Bit flags
*/
#define	APM_16BIT_PMODE     1
#define	APM_32BIT_PMODE     2
#define	APM_CPUIDLE_REDUCE  3
#define	APM_DISABLED        4
#define	APM_DISENGAGED      5

/**
*	System BIOS device ID
*/
#define APM_BIOS_DEVICE_ID 0

/**
*	APM 32 bit interface
*/
typedef struct _APM_INTERFACE_32 {
	uint16_t rmodeCodeSeg32;
	uint32_t entryPoint;
	uint16_t rmodeCodeSeg16;
	uint16_t rmodeDataSeg16;
	/**
	*	APM 1.1
	*/
	uint16_t codeSegLen;
	uint16_t dataSegLen;
}APM_INTERFACE_32, *PAPM_INTERFACE_32;

/**
*	APM 16 bit interface
*/
typedef struct _APM_INTERFACE_16 {
	uint16_t rmodeCodeSeg16;
	uint32_t entryPoint;
	uint16_t rmodeDataSeg16;
	/**
	*	APM 1.1
	*/
	uint16_t codeSegLen;
	uint16_t dataSegLen;
}APM_INTERFACE_16, *PAPM_INTERFACE_16;

PRIVATE void BiosExit (IN status_t status);
PRIVATE void BiosReset (IN status_t status);
PRIVATE void BiosShutdown (void);
PRIVATE status_t BiosEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]);
PRIVATE status_t BiosRequest (IN nblMessage* rpb);
/*
	Advanced Power Management (APM) Private Services.
*/
PRIVATE BOOL ApmInstallCheck ();
PRIVATE int ApmGetVersion ();
PRIVATE int ApmDisconnectInterface (IN int dev);
PRIVATE int ApmConnectBits32 (IN int dev, IN PAPM_INTERFACE_32);
PRIVATE int ApmConnectBits16 (IN int dev, IN PAPM_INTERFACE_16);
PRIVATE void ApmShutdownSystem ();

/**
*	ApmInstallCheck
*
*	Description : Checks if Advanced Power Management (APM) is
*	supported by the BIOS firmware.
*
*	Return value : TRUE if supported, FALSE if not.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE BOOL ApmInstallCheck () {

	INTR in;
	INTR out;

	/* check if APM installed */
	in.eax.val = 0x5300;
	in.ebx.val = 0;
	if (! io_services (0x15, &in, &out) )
		return FALSE;

	/* check PM signiture */
	if (out.ebx.w.val != 0x504D)
		return FALSE;

	if (out.ecx.val)
		return TRUE;

	return FALSE;
}

/**
*	ApmGetVersion
*
*	Description : Obtains Installed APM version number. This
*	method assumes APM is installed in the system.
*
*	Return value : Version number obtained from firmware.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int ApmGetVersion () {

	INTR in;
	INTR out;

	in.eax.val = 0x5300;
	in.ebx.val = 0;
	io_services (0x15, &in, &out);
	return _ax (out.eax);
}

/**
*	ApmGetVersion
*
*	Description : Disconnects APM device interface.
*
*	Return value : TRUE if success, FALSE if not.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int ApmDisconnectInterface (IN int dev) {

	INTR in;
	INTR out;

	in.eax.val = 0x5304;
	in.ebx.val = dev;
	return io_services (0x15, &in, &out);
}

/**
*	ApmConnectBits32
*
*	Description : Disconnects APM device interface.
*
*	Return value : TRUE if success, FALSE if not.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int ApmConnectBits32 (IN int dev, IN PAPM_INTERFACE_32 p) {

	INTR in;
	INTR out;

	if (p) {

		in.eax.val = 0x5303;
		in.ebx.val = dev;
		io_services (0x15, &in, &out);

		p->rmodeCodeSeg32 = _ax (out.eax);
		p->entryPoint     = _bx (out.ebx);
		p->rmodeCodeSeg16 = _cx (out.ecx);
		p->rmodeDataSeg16 = _dx (out.edx);
		p->codeSegLen     = 0; //_si (out.esi);
		p->dataSegLen     = 0; // _di (out.edi);
		return _ah (out.eax);
	}
	return 0;
}

/**
*	ApmConnectBits16
*
*	Description : Disconnects APM device interface.
*
*	Return value : TRUE if success, FALSE if not.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE int ApmConnectBits16 (IN int dev, IN PAPM_INTERFACE_16 p) {

	INTR in;
	INTR out;

	if (p) {
		in.eax.val = 0x5302;
		in.ebx.val = dev;
		io_services (0x15, &in, &out);

		p->rmodeCodeSeg16 = _ax (out.eax);
		p->entryPoint     = _bx (out.ebx);
		p->rmodeCodeSeg16 = _cx (out.ecx);
		p->codeSegLen     = 0; //_si (out.esi);
		p->dataSegLen     = 0; // _di (out.edi);

		//! error code
		return _ah (out.eax);
	}

	return 0;
}

/**
*	ApmShutdownSystem
*
*	Description : Attempts to use APM to shut down the system.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void ApmShutdownSystem () {

	INTR in;
	INTR out;

	/* test if APM is available */
	in.eax.val = 0x5300;
	in.ebx.val = 0;
	io_services (0x15, &in, &out);

	/* if bit 1 is set, 16bit APM interface supported */
	if ( (out.ecx.w.val & 1) == 1) {

		/* connect real mode interface */
		in.eax.val = 0x5301;
		in.ebx.val = 0;
		io_services (0x15, &in, &out);
		
		/* set power state */
		in.eax.val = 0x5307;
		in.ebx.val = 1;
		in.ecx.val = 3;
		io_services (0x15, &in, &out);
	}
}

/**
*	GetConfigurationTable
*
*	Description : Attempts to return the address of the System Configuration Table.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE addr_t GetConfigurationTable(void) {

	INTR in;
	INTR out;
	addr_t configTable;

	/* call int 15h function 0xc0 - GET CONFIGURATION. */
	in.eax.val = 0xc0;
	io_services(0x15, &in, &out);

	/* ES:BX -> ROM Config table. */
	configTable = out.es * 16 + _bx(out.ebx);
	return configTable;
}

/**
*	BiosShutdown
*
*	Description : Attempts to use APM to shut down the system.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void BiosShutdown (void) {

	ApmShutdownSystem ();
}

/**
*	BiosExit
*
*	Description : Calls the debugger if one is present
*	and halts execution.
*
*	Execution Mode : Kernel Mode.
*/
void BiosExit (IN status_t status) {

	_asm cli
	for(;;) {
		_asm mov eax, 0xc001babe
		_asm hlt	/* loop to trap NMI's */
	}
}

/**
*	BiosReset
*
*	Description : Resets the system and returns execution
*	to the BIOS firmware.
*
*	Input : status (ignored.)
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void BiosReset (IN status_t status) {

	INTR in;
	INTR out;

	/* call BIOS to soft reboot */
	in.eax.val = 0;
	io_services (0x19, &in, &out);
	/*
		the callee would not expect to return
		from this function. If the above fails,
		quit the program.
	*/
	BiosExit (0);
}

/**
*	BlFwInitFpu
*
*	Description : Initialize FPU and SSE support.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void BlFwInitFpu(void) {

	uint32_t cr4, cr0;

	cr0 = __readcr0();
	cr0 &= ~(1 << 2);  /* clear CR0.EM */
	cr0 |= (1 << 1);   /* set CR0.MP */
	cr0 |= (1 << 5);   /* set CR0.NE */
	__writecr0(cr0);

	cr4 = __readcr4();
	cr4 |= (1 << 9);   /* set CR4.OSFXSR */
	cr4 |= (1 << 10); /* set CR4.OSXMMEXCPT */
	__writecr4(cr4);
}

/**
*	BlFwStartup
*
*	Description : This method is called from NSTART. It is
*	responsible for calling NBLC.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void BlFwStartup(void) {

	status_t result;

	/* relocate stack. This needs to be a low enough
	address as to not cause problems with NBOOT, NSTART,
	or io_services */
	__asm mov esp, 0x7c00

	/* initialize floating point unit. Specifically
	needed for graphics. */
	BlFwInitFpu();

	/* call boot application. */
	result = BlStartBootApplication();

	/* never return. */
	BiosExit(result);
}

/**
*	BiosEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t BiosEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	return NBL_STAT_SUCCESS;
}

/**
*	BiosRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t BiosRequest (IN nblMessage* rpb) {

	rettype_t r;

	rpb->retval = 0;
	r = 0;

	switch (rpb->type) {
		case NBL_FW_EXIT:
			BiosExit (rpb->NBL_FW_EXIT_STATUS);
			break;
		case NBL_FW_RESET:
			BiosReset (rpb->NBL_FW_RESET_STATUS);
			break;
		case NBL_FW_SHUTDOWN:
			BiosShutdown ();
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

/*
	Driver version number.
*/
#define NBL_BIOS_VERSION 1

/*
	Firmware Driver object.
*/
PUBLIC
NBL_DRIVER_OBJECT_DEFINE _nBiosDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_BIOS_VERSION,
	NBL_DRV_LOAD_CORE,
	NBL_DRV_CLASS_FW,
	"fw",
	BiosEntryPoint,
	BiosRequest,
	0
};


/*
               NBOOT Memory Map for BIOS Builds

	              +------------------------+
                  |                        |
0 - 0x400         | Interrupt Vector Table | Reserved by BIOS.
                  |                        |
	              +------------------------+
                  |                        |
0x400 - 0x500     |     BIOS Data Area     | Reserved by BIOS.
                  |                        |
	              +------------------------+
                  |                        | The stack is used by protected and real
0x500 - 0x7c00    |       NBOOT Stack      | mode BIOS calls. io_services sets SS:SP=>0:existing_sp,
                  |                        | so stack must be < 64k.
	              +------------------------+
                  |                        | We keep NSTART in memory so we can fall back
0x8000 - 0xA000   |         NSTART         | on its platform specific exception handlers.
                  |                        | Although, perhaps we can just install our own?
	              +------------------------+
                  |                        |
0xA000 - 0x10000  |                        |
                  |                        |
	              +        ZONE_LOW        -+ NBOOT Heap.
                  |                        |
0x10000 - 0x20000 |                        |
                  |                        |
	              +------------------------+
                  |                        | First 16 bytes also used as SCRATCH space for Disk
0x20000 - 0x80000 |        NBOOT.EXE       | Address Packet.
                  |                        |
	              +------------------------+
                  |                        |
0x80000 - 0x9FBFF |   RAM (If available)   |
                  |                        |
	              +------------------------+
                  |                        | These are installed by NSTART in order to trap
0x9C000 -         |       Page Tables      | errors in early boot. It gets disabled or remapped
                  |                        | by NBOOT when booting.
	              +------------------------+
                  |                        |
0x9FC00 - 0x9FFFF |      Extended BDA      |
                  |                        |
	              +------------------------+
                  |                        |
0xA0000 - 0xFFFFF |          ROM           |
                  |                        |
	              +------------------------+
                  |                        |
0x100000          |      ZONE_NORMAL       | NBOOT Heap.
                  |                        |
	              +------------------------+
*/


/* Mode 12h Testing Ground ---------------------------- */

/* back end routines ---------------------- */

void BlSetModeMode12h(int mode) {

	INTR in, out;
	in.eax.val = mode & 0xff;
	io_services(0x10, &in, &out);
}

volatile uint8_t* _video_memory = (volatile uint8_t*)0xa0000;
const uint32_t _lfb_width = 640;
const uint32_t _lfb_height = 480;

PUBLIC INLINE void HalWritePort(IN uint16_t port, IN uint8_t data) {

	__outbyte(port,data);
}

void BlDrawPixel12h (IN uint32_t x, IN uint32_t y, IN int col) {

	uint8_t mask;
	uint32_t pixel;

	if (x >= _lfb_width)
		return;
	if (y >= _lfb_height)
		return;

	HalWritePort(0x3ce,5);/* graphics Mode */
	HalWritePort(0x3cf,2);/* write Mode 2 */ 

	pixel = y * 80 + (x / 8); 
	mask = 0x80 >> (x % 8);

	HalWritePort(0x3ce,8);
	HalWritePort(0x3cf,mask); /* set bit mask. */

	HalWritePort(0x3ce,3);
	HalWritePort(0x3cf,0); /* set pixel. */

	mask = _video_memory[pixel]; /* refresh latch registers. */
	_video_memory[pixel] = col;
}

uint8_t* _backbuffer;

/* higher level routines -------------------- */

void BlrDrawRect(IN uint32_t x, IN uint32_t y, IN uint32_t w, IN uint32_t h, IN int col) {

	uint32_t i;
	for (i = y; i < y + h; i++)
		BlrFillMemory(&_backbuffer[x+i*_lfb_width], col, w);
}

void BlBlitBackbuffer(void) {

	uint32_t y, x;
	for (y=0; y<_lfb_height;y++)
		for (x=0;x<_lfb_width;x++)
			BlDrawPixel12h(x,y, _backbuffer[x+y*_lfb_width]);
}

void  BlMode12hTest (void) {

	uint32_t sizeInBytes;

	/* 640x480x4 bits per pixel. Each pixel is half a byte. */
	sizeInBytes = _lfb_width * _lfb_height * 2;

	_backbuffer = BlrAlloc(sizeInBytes);
	if (!_backbuffer)
		BlFatalError("Not enough space for backbuffer.");
	BlrZeroMemory(_backbuffer, sizeInBytes);

	BlSetModeMode12h (0x12);

	/* draw rect to backbuffer and blit it --------- */

	BlrDrawRect(0,0,640,479,0x1);
	BlBlitBackbuffer();

	while(1)
		_asm pause;
}
