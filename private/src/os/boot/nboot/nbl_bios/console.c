/************************************************************************
*
*	BIOS console driver.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "bios.h"

/*** PRIVATE DECLARATIONS ***/

STATIC uint8_t _currentAttribute;
STATIC BOOL    _cursorEnable;

NBL_DRIVER_OBJECT_DEFINE _nBiosInDriverObject;
NBL_DRIVER_OBJECT_DEFINE _nBiosOutDriverObject;

STATIC nblFile nbl_in = {
	0,0,0,&_nBiosInDriverObject,0,0,0,0,0,0,0,0
};

STATIC nblFile nbl_out = {
	0,0,0,&_nBiosOutDriverObject,0,0,0,0,0,0,0,0
};

STATIC nblFile nbl_wout = {
	0, 0, 0, &_nBiosOutDriverObject, 0, 0, 0, 0, 0, 0, 0, 0
};

/* console output device object. */
STATIC nblDeviceObject _conin = {
	NBL_DEVICE_TERMINAL_PERIPHERAL,
	NBL_DEVICE_PERIPHERAL_CLASS,
	0, &_nBiosInDriverObject,
	&_nBiosInDriverObject, 0, "conin", 0
};

/* console output device object. */
STATIC nblDeviceObject _conout = {
	NBL_DEVICE_TERMINAL_PERIPHERAL,
	NBL_DEVICE_PERIPHERAL_CLASS,
	0, &_nBiosOutDriverObject,
	&_nBiosOutDriverObject, 0, "conout", 0
};

PRIVATE status_t InputReset (IN BOOL ExtendedVerification);
PRIVATE uint32_t InputReadKeyStroke (void);
PRIVATE uint32_t InputRead (IN nblFile* file, IN size_t size, OUT char* buffer);
PRIVATE nblFile* InputOpen (IN char* path, IN nblDeviceObject* dev);
PRIVATE status_t ConInRequest (IN nblMessage* rpb);
PRIVATE status_t ConInEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]);

PRIVATE status_t OutputReset (IN BOOL ExtendedVerification);
PRIVATE status_t OutputString (IN char* str);
PRIVATE uint16_t OutputQueryMode (IN uint32_t mode);
PRIVATE status_t OutputSetMode (IN uint32_t mode);
PRIVATE uint32_t OutputGetMode (void);
PRIVATE status_t OutputSetAttribute (IN uint32_t foreground, IN uint32_t background);
PRIVATE uint32_t OutputGetAttributes (void);
PRIVATE status_t OutputClearScreen (IN uint32_t foreground, IN uint32_t background);
PRIVATE status_t OutputSetXY (IN uint32_t col, IN uint32_t row);
PRIVATE uint16_t OutputGetXY (void);
PRIVATE status_t OutputEnableCursor (IN BOOL enable);
PRIVATE BOOL OutputGetCursorState (void);
status_t OutputPrint (IN char* in);
PRIVATE uint32_t OutputWrite (IN nblFile* file, IN size_t size, OUT char* buffer);
PRIVATE nblFile* OutputOpen (IN char* path, IN nblDeviceObject* dev);
PRIVATE status_t ConOutRequest (IN nblMessage* rpb);
PRIVATE status_t ConOutEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]);

/*** PRIVATE DEFINITIONS ***/

/**
*	InputReset
*
*	Description : Reset input device.
*
*	Input : ExtendedVerification (Ignored.)
*
*	Return value : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t InputReset (IN BOOL ExtendedVerification) {

	/* Nothing to do. */
	return NBL_STAT_SUCCESS;
}

/**
*	InputReadKeyStroke
*
*	Description : Test if a key has been pressed. This is a non-blocking call.
*
*	Return value : Scan Code if key was pressed, 0 otherwise.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE uint32_t InputReadKeyStroke (void) {

	INTR     in;
	INTR     out;
	uint32_t scanCode;

	scanCode = 0;

	/* int 0x16 function 1 */
	_ah (in.eax) = 1;
	io_services (0x16,&in,&out);
	scanCode = _ah (out.eax);

	/* if zero flag is clear, we got a keypress. */
	if (!(out.flags & 0x40)) {
		_ah (in.eax) = 0;
		io_services (0x16,&in,&out);
		return scanCode;
	}

	return 0;
}

/**
*	InputReadCharacter
*
*	Description : Reads a single character from keyboard. This is a blocking call.
*
*	Return value : Character Code returned.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE char InputReadCharacter (void) {

	INTR     in;
	INTR     out;
	uint32_t ch;

	/* int 0x16 function 0 */
	_ah (in.eax) = 0;
	io_services (0x16,&in,&out);

	ch = _al (out.eax);
	return ch;
}

/**
*	InputRead
*
*	Description : Reads a block of data from the keyboard.
*
*	Input: file - Input File Stream (must be "conin" device.)
*          size - Size (in bytes) of buffer.
*          buffer - Pointer to buffer to receive data.
*
*	Return value : Number of characters read.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE uint32_t InputRead (IN nblFile* file, IN size_t size, OUT char* buffer) {

	uint32_t i;

	/* make sure the file object was created by us. */
	if (file && file->driver != &_nBiosInDriverObject)
		return 0;

	if (buffer == NULL)
		return 0;

	for (i = 0; i < size - 1; i++) {

		/* get character from keyboard. */
		char ch = InputReadCharacter ();

		/* if character is not a printable character, bail out. */
		if (ch < ' ' || ch > '~')
			break;

		/* display it to stdout. */
		BlrPrintf("%c", ch);

		/* write it to output buffer. */
		buffer[i] = ch;
	}

	/* null terminate buffer and return actual size. */
	buffer [i] = NULL;
	return i;
}

/**
*	InputOpen
*
*	Description : Reads a block of data from the keyboard.
*
*	Input: path (ignored.)
*          dev - Must be the conin device object created by this driver.
*
*	Return value : Character Code returned.
*
*	Execution Mode : Standard In File Pointer or NULL.
*/
PRIVATE nblFile* InputOpen (IN char* path, IN nblDeviceObject* dev) {

	if (dev != &_conin)
		return NULL;

	nbl_stdin = &nbl_in;
	return nbl_stdin;
}

/**
*	BiosRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t ConInRequest (IN nblMessage* rpb) {

	rettype_t r = 0;

	switch (rpb->type) {
		case NBL_CONIN_RESET:
			r = (rettype_t) InputReset (FALSE);
			break;
		case NBL_CONIN_READKEY:
			r = (rettype_t) InputReadKeyStroke ();
			break;
		case NBL_FS_READ:
			r = (rettype_t) InputRead ((nblFile*) rpb->NBL_FS_READ_FILE,
				rpb->NBL_FS_READ_SIZE, (char*) rpb->NBL_FS_READ_BUFFER);
			break;
		case NBL_FS_OPEN:
			r = (rettype_t) InputOpen ((char*) rpb->NBL_FS_OPEN_PATH,
				(nblDeviceObject*) rpb->NBL_FS_OPEN_DEVICE);
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;

	return NBL_STAT_SUCCESS;
}

/**
*	ConInEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self - Pointer to Driver Object.
*           dependencyCount - Must be 1.
*           dependencies - Dependency array.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t ConInEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	nblMessage rpb;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	/* create console device. */
	rpb.source = self;
	rpb.type   = NBL_DEV_REGISTER;
	rpb.NBL_DEV_REGISTER_OBJ = &_conin;
	if (dm->request (&rpb) != NBL_STAT_SUCCESS)
		return NBL_STAT_DEPENDENCY;

	BlrZeroMemory(&nbl_in,sizeof(nblFile));
	nbl_in.driver = &_nBiosInDriverObject;

	nbl_stdin  = &nbl_in;

	return NBL_STAT_SUCCESS;
}

/* Driver object. */
NBL_DRIVER_OBJECT_DEFINE _nBiosInDriverObject = {
	NBL_DRIVER_MAGIC,
	0,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_CON,
	"conin",
	ConInEntryPoint,
	ConInRequest,
	1,
	"dm"
};

/*** Console Output Device ***/

/**
*	OutputReset
*
*	Description : Resets console output device.
*
*	Input : ExtendedVerification (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t OutputReset(IN BOOL ExtendedVerification) {

	/* Nothing to do. */
	return NBL_STAT_SUCCESS;
}

/**
*	Scroll
*
*	Description : Scrolls the screen.
*
*	Input : rows - Number of rows to scroll.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void Scroll (IN unsigned int rows) {

	uint16_t* VIDEO_MEMORY_BASE = (uint16_t*) 0xb8000;
	uint16_t* SCROLL_START = VIDEO_MEMORY_BASE + 80;

	uint32_t  regionSize = 25 * 80;

	BlrCopyMemory ((void*)  VIDEO_MEMORY_BASE, (void*) SCROLL_START, regionSize * sizeof (uint16_t));
	BlrZeroMemory (VIDEO_MEMORY_BASE + regionSize, 80 * sizeof (uint16_t));
}

/**
*	DisplayCharacter
*
*	Description : Displays character with attribute byte.
*
*	Input : c - Character to display.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE void DisplayCharacter (IN unsigned char c) {

	INTR     in;
	INTR     out;
	uint16_t pos;
	uint8_t  x;
	uint8_t  y;

	pos = OutputGetXY ();
	y = pos & 0xff;
	x = pos >> 8;

	if (c=='\n') {

		y++;
		OutputSetXY  (x,y);
		return;

	} else if (c=='\r') {

		x=0;
		OutputSetXY  (x,y);
		return;
	}

	if (x >= 79) {
		x = 0;
		y++;
	}

	if (y > 24) {
		Scroll (1);
		y = 24;
		OutputSetXY  (x,y);
	}

	_ah (in.eax) = 0x09;
	_al (in.eax) = c;
	_bh (in.ebx) = 0;
	_bl (in.ebx) = _currentAttribute;
	_cx (in.ecx) = 1;

	/* int 0x10 function 9 */
	io_services (0x10,&in,&out);

	OutputSetXY (x+1,y);
}

/**
*	OutputString
*
*	Description : Display string to console device.
*
*	Input : str - String to display.
*
*	Output : Status code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t OutputString (IN char* str) {

	size_t c;

	for (c = 0; c < BlrStrLength (str); c++)
		DisplayCharacter (str [c]);
	return NBL_STAT_SUCCESS;
}

/**
*	OutputQueryMode
*
*	Description : Returns the console mode number. The mode number
*   has the following format:
*
*   +------+-------+
*   | cols |  rows |
*   +------+-------+
*   12     8       0
*
*	Input : mode (Ignored.)
*
*	Output : Mode number.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE uint16_t OutputQueryMode (IN uint32_t mode) {

	uint8_t cols;
	uint8_t rows;

	/* offset into Bios Data Area */
	cols = *(uint8_t*) 0x44A;
	rows = *(uint8_t*) 0x484;

	return (cols << 8) | rows;
}

/**
*	OutputSetMode
*
*	Description : Returns the console mode number. The mode number
*   has the following format:
*
*   +------+-------+
*   | cols |  rows |
*   +------+-------+
*   12     8       0
*
*	Input : mode - Mode number in the above format.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t OutputSetMode (IN uint32_t mode) {

	INTR	in;
	INTR    out;

	/* we assume mode is text. */
	_ah (in.eax) = 0;
	_al (in.eax) = (unsigned char) mode;

	/* int 0x10 function 0. */
	io_services (0x10, &in, &out);

	return NBL_STAT_SUCCESS;
}

/**
*	OutputGetMode
*
*	Description : Returns VGA video mode number.
*
*	Output : Current VGA mode number.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE uint32_t OutputGetMode (void) {

	INTR	in;
	INTR    out;
	uint32_t mode;

	/* int 0x10 function 0x0f */
	_ah (in.eax) = 0x0f;
	io_services (0x10, &in, &out);

	mode = _al (out.eax);
	return mode;
}

/**
*	OutputSetAttribute
*
*	Description : Set current text mode attribute byte.
*
*	Input : foreground - Foregound color.
*           background - Background color.
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t OutputSetAttribute (IN uint32_t foreground, IN uint32_t background) {

	_currentAttribute = (background << 4) | foreground;
	return NBL_STAT_SUCCESS;
}

/**
*	OutputGetAttributes
*
*	Description : Returns current attribute byte.
*
*	Output : Current attribute byte.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE uint32_t OutputGetAttributes (void) {

	return _currentAttribute;
}

/**
*	OutputClearScreen
*
*	Description : Clear the console device.
*
*	Input : foreground - Foreground color.
*           background - Background color.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t OutputClearScreen (IN uint32_t foreground, IN uint32_t background) {

	INTR	in;
	INTR    out;

	in.ecx.val = 0;
	_ah (in.eax) = 6;
	_al (in.eax) = 0;
	_dl (in.edx) = 79;
	_dh (in.edx) = 24;
	_bh (in.ebx) = (unsigned char)_currentAttribute;

	/* int 0x10 function 6. */
	io_services (0x10, &in, &out);

	return NBL_STAT_SUCCESS;
}

/**
*	OutputSetXY
*
*	Description : Set text mode cursor location.
*
*	Input : col - Column to set cursor to.
*           row - Row to set cursor to.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
status_t OutputSetXY (IN uint32_t col, IN uint32_t row) {

	INTR	in;
	INTR    out;

	_ah (in.eax) = 2;
	_dh (in.edx) = row;
	_dl (in.edx) = col;
	_bh (in.ebx) = 0;

	/* int 0x10 function 2 */
	io_services (0x10, &in, &out);

	return NBL_STAT_SUCCESS;
}

/**
*	OutputGetXY
*
*	Description : Returns text mode cursor location.
*
*	Output : Cursor location in the following format:
*
*   +-----+-----+
^   | col | row |
^   +-----+-----+
*   16    8     0
*
*	Execution Mode : Kernel Mode.
*/
uint16_t OutputGetXY (void) {

	INTR	in;
	INTR    out;
	uint8_t row;
	uint8_t col;

	_ah (in.eax) = 3;
	_bh (in.ebx) = 0;

	/* int 0x10 function 3 */
	io_services (0x10, &in, &out);

	row = _dh (out.edx);
	col = _dl (out.edx);

	return (col << 8) | row;
}

/**
*	OutputEnableCursor
*
*	Description : Enables or Disables the hardware cursor.
*
*	Input : enable - TRUE to enable, FALSE to disable.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t OutputEnableCursor (IN BOOL enable) {

	INTR	in;
	INTR    out;

	/* int 0x10 function 1 */
	_ah (in.eax) = 1;
	in.ecx.val = (enable) ? 0x0607 : 0x2607;
	io_services (0x10, &in, &out);

	_cursorEnable = enable;

	return NBL_STAT_SUCCESS;
}

/**
*	OutputGetCursorState
*
*	Description : Returns current cursor state.
*
*	Output : TRUE if the hardware cursor is enabled, FALSE otherwise.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE BOOL OutputGetCursorState (void) {

	return _cursorEnable;
}

/**
*	OutputPrint
*
*	Description : Prints string to the console device.
*
*	Input : in String to display.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE STATIC status_t OutputPrint (IN char* in) {

	size_t c;

	for (c = 0; c < BlrStrLength (in); c++)
		DisplayCharacter (in [c]);

	return NBL_STAT_SUCCESS;
}

/**
*	OutputWidePrint
*
*	Description : Prints string to the console device.
*
*	Input : in String to display.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE STATIC status_t OutputWidePrint(IN wchar_t* in) {

	size_t c;

	for (c = 0; c < BlrWideStrLength(in); c++)
		DisplayCharacter(BlrWideCharToSingleiByte (in[c]));

	return NBL_STAT_SUCCESS;
}

/**
*	OutputWrite
*
*	Description : Prints string to a File Device Object.
*
*	Input : file - File Device Object. This should be nbl_stdout or nbl_wstdout.
*           size - Size in bytes of input buffer.
*           buffer - Input buffer.
*
*	Output : Status Code.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE uint32_t OutputWrite (IN nblFile* file, IN size_t size, OUT char* buffer) {

	if (file == nbl_stdout) { 

		if (OutputPrint(buffer) != NBL_STAT_SUCCESS)
			return 0;
		return BlrStrLength(buffer);
	}
	else if (file == nbl_wstdout) {

		wchar_t* real_buffer = (wchar_t*)buffer;

		if (OutputWidePrint(real_buffer) != NBL_STAT_SUCCESS)
			return 0;
		return BlrWideStrLength(real_buffer);
	}

	return 0;
}

/**
*	OutputOpen
*
*	Description : Open console device object.
*
*	Input : path - Ignored.
*           dev - Device Object created by this driver.
*
*	Output : Standard Out File Device Object.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE nblFile* OutputOpen (IN char* path, IN nblDeviceObject* dev) {

	if (dev != &_conout)
		return 0;

	nbl_stdout = &nbl_out;
	nbl_wstdout = &nbl_wout;
	return nbl_stdout;
}

/**
*	ConOutRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t ConOutRequest (IN nblMessage* rpb) {

	rettype_t r;

	uint32_t req2 = rpb->type;
	r = 0;

	switch (rpb->type) {
		case NBL_CONOUT_RESET:
			r = (rettype_t) OutputReset (FALSE);
			break;
		case NBL_CONOUT_CURSOR:
			r = (rettype_t) OutputEnableCursor ((BOOL) rpb->NBL_CONOUT_CURSOR_ENABLE);
			break;
		case NBL_CONOUT_GETCURSOR:
			r = (rettype_t) OutputGetCursorState ();
			break;
		case NBL_CONOUT_GETXY:
			r = (rettype_t) OutputGetXY ();
			break;
		case NBL_CONOUT_SETXY:
			r = (rettype_t) OutputSetXY 
				((uint32_t) rpb->NBL_CONOUT_SET_X, (uint32_t) rpb->NBL_CONOUT_SET_Y);
			break;
		case NBL_CONOUT_CLRSCR:
			r = (rettype_t) OutputClearScreen
				((uint32_t) rpb->NBL_CONOUT_CLRSCR_FCOLOR, (uint32_t) rpb->NBL_CONOUT_CLRSCR_BCOLOR);
			break;
		case NBL_CONOUT_ATTRIB:
			r = (rettype_t) OutputSetAttribute
				((uint32_t) rpb->NBL_CONOUT_ATTRIB_FCOLOR, (uint32_t) rpb->NBL_CONOUT_ATTRIB_BCOLOR);
			break;
		case NBL_CONOUT_GETATTRIB:
			r = (rettype_t) OutputGetAttributes ();
			break;
		case NBL_CONOUT_SETMODE:
			r = (rettype_t) OutputSetMode ((uint32_t) rpb->NBL_CONOUT_SETMODE_MODE);
			break;
		case NBL_CONOUT_GETMODE:
			r = (rettype_t) OutputGetMode ();
			break;
		case NBL_CONOUT_PRINT:
			r = (rettype_t) OutputPrint ((char*) rpb->NBL_CONOUT_PRINT_STR);
			break;
		case NBL_FS_WRITE:
			r = (rettype_t) OutputWrite ((nblFile*) rpb->NBL_FS_WRITE_FILE,
				rpb->NBL_FS_WRITE_SIZE, (char*) rpb->NBL_FS_WRITE_BUFFER);
			break;
		case NBL_FS_OPEN:
			r = (rettype_t) OutputOpen ((char*) rpb->NBL_FS_OPEN_PATH,
				(nblDeviceObject*) rpb->NBL_FS_OPEN_DEVICE);
			break;
		default:
			return NBL_STAT_UNSUPPORTED;
	};

	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

/**
*	ConOutEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
PRIVATE status_t ConOutEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	nblMessage rpb;

	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	/* create console device. */
	rpb.source = self;
	rpb.type   = NBL_DEV_REGISTER;
	rpb.NBL_DEV_REGISTER_OBJ = &_conout;
	if (dm->request (&rpb) != NBL_STAT_SUCCESS)
		return NBL_STAT_DEPENDENCY;

	BlrZeroMemory(&nbl_out, sizeof(nblFile));
	nbl_out.driver = &_nBiosOutDriverObject;

	/* default attribute is black background with white text */
	_currentAttribute = 0x7;

	/* default cursor state is enabled */
	_cursorEnable = TRUE;

	/* redirect standard streams to console devices. */
	nbl_stdout = &nbl_out;
	nbl_wstdout = &nbl_wout;

	return NBL_STAT_SUCCESS;
}

/* Console output driver object. */
NBL_DRIVER_OBJECT_DEFINE _nBiosOutDriverObject = {
	NBL_DRIVER_MAGIC,
	0,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_CON,
	"conout",
	ConOutEntryPoint,
	ConOutRequest,
	1,
	"dm"
};
