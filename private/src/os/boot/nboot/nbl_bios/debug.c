/************************************************************************
*
*	debug.c - BIOS Debug Port Services.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "bios.h"

/* For performance reasons, we avoid the standard bios int 0x14 services here.
This way, we don't need to switch to real mode for every character. Unless
we buffer the data perhaps? */

STATIC INLINE void _WritePort(IN uint16_t port, IN uint8_t data) {

	__outbyte(port, data);
}

STATIC INLINE uint8_t _ReadPort(IN uint16_t port) {

	return __inbyte(port);
}

#define COM1         0x3f8

#define DATAREG      0
#define INTREG       1
#define BAUDLOW      0
#define BAUDHIGH     1
#define INTFIFOCONT  2
#define LINECONT     3
#define MODEMCONT    4
#define LINESTAT     5
#define MODEMSTAT    6

STATIC int DbgSerialReceived(void) {

	return _ReadPort(COM1 + LINESTAT) & 1;
}

STATIC int DbgSerialTransmitEmpty(void) {

	return _ReadPort(COM1 + LINESTAT) & 0x20;
}

STATIC char DbgReadSerial(void) {

	while (!DbgSerialReceived())
		;
	return _ReadPort(COM1 + DATAREG);
}

STATIC void DbgWriteSerial(IN char c) {

	while (!DbgSerialTransmitEmpty())
		;
	_WritePort(COM1 + DATAREG, c);
}

STATIC void DebugRead(IN size_t size, OUT char* buffer) {

	int i;
	for (i = 0; i < size; i++)
		buffer[i] = DbgReadSerial();
}

STATIC void DebugWrite(IN size_t size, IN char* buffer) {

	int i;
	for (i = 0; i < size; i++)
		DbgWriteSerial(buffer[i]);
}

/**
*	DebugEntryPoint
*
*	Description : This is called by NBLC to initialize
*	device objects. Independent firmware drivers will
*	register required device objects so we do nothing here.
*
*	Input : self (Ignored.)
*           dependencyCount (Ignored.)
*           dependencies (Ignored.)
*
*	Output : NBL_STAT_SUCCESS.
*
*	Execution Mode : Kernel Mode.
*/
STATIC status_t DebugEntryPoint(IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	_WritePort(COM1 + DATAREG, 0);
	_WritePort(COM1 + LINECONT, 0x80);
	_WritePort(COM1 + BAUDLOW, 3);
	_WritePort(COM1 + BAUDHIGH, 0);
	_WritePort(COM1 + LINECONT, 3);
	_WritePort(COM1 + INTFIFOCONT, 0xe7);
	_WritePort(COM1 + MODEMCONT, 0x0b);

	return NBL_STAT_SUCCESS;
}

/**
*	DebugRequest
*
*	Description : This method services NBLC
*	and boot application requests for basic
*	firmware services.
*
*	Input : rpb - Pointer to the Request Paramater Block
*
*	Output : NBL_STAT_SUCCESS if the request type is supported.
*            NBL_STAT_UNSUPPORTED if the request type is not supported.
*
*	Execution Mode : Kernel Mode.
*/
STATIC status_t DebugRequest(IN nblMessage* rpb) {

	rettype_t r;

	rpb->retval = 0;
	r = 0;

	switch (rpb->type) {
	case NBL_DEBUG_READ:
		DebugRead(rpb->NBL_DEBUG_READ_SIZE, rpb->NBL_DEBUG_READ_BUFFER);
		break;
	case NBL_DEBUG_WRITE:
		DebugWrite(rpb->NBL_DEBUG_WRITE_SIZE, rpb->NBL_DEBUG_WRITE_BUFFER);
		break;
	default:
		return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

/*
Driver version number.
*/
#define NBL_BIOS_VERSION 1

/*
Firmware Driver object.
*/
PUBLIC
NBL_DRIVER_OBJECT_DEFINE _nBiosDebugDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_BIOS_VERSION,
	NBL_DRV_LOAD_CORE,
	NBL_DRV_CLASS_FW,
	"debug",
	DebugEntryPoint,
	DebugRequest,
	0
};
