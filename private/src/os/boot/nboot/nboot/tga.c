/************************************************************************
*
*	tga.c TGA file loading
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "gfx.h"

// http://www.dca.fee.unicamp.br/~martino/disciplinas/ea978/tgaffs.pdf
// http://nehe.gamedev.net/tutorial/loading_compressed_and_uncompressed_tga's/22001/

typedef enum _TGA_COLOR_MAP_TYPE {
	TGA_COLOR_MAP_TYPE_NONE = 0,
	TGA_COLOR_MAP_TYPE_VALID = 1
}TGA_COLOR_MAP_TYPE;

typedef enum _TGA_IMAGE_TYPE {
	TGA_IMAGE_TYPE_NONE = 0,
	TGA_IMAGE_TYPE_UNCOMPRESSED_COLOR_MAPPED = 1,
	TGA_IMAGE_TYPE_UNCOMPRESSED_TRUE_COLOR = 2,
	TGA_IMAGE_TYPE_UNCOMPRESSED_BACK_WHITE = 3,
	TGA_IMAGE_RLE_COLOR_MAPPED = 9,
	TGA_IMAGE_RLE_TRUE_COLOR = 10,
	TGA_IMAGE_RLE_BLACK_WHITE = 11
}TGA_IMAGE_TYPE;

typedef struct _blTgaHeader {
	uint8_t imageIdLength;
	uint8_t colorMapType;
	uint8_t imageType;
	struct {
		uint16_t firstEntryIndex;
		uint16_t colorMapLength;
		uint8_t colorMapEntrySize;
	}colorMapSpec;
	struct {
		uint16_t xOrigin;
		uint16_t yOrigin;
		uint16_t width;
		uint16_t height;
		uint8_t depth;
//		These bits are used to indicate the order in which
//			pixel data is transferred from the file to the screen.
//			Bit 4 is for left - to - right ordering and bit 5 is for
//			top - to - bottom ordering as shown below
		uint8_t descriptor;
	}imageSpec;
}blTgaHeader;

#define DIRECTION_UP_DOWN_MASK    0x32
#define DIRECTION_LEFT_RIGHT_MASK 0x16

// TGA file is flipped on Y axis.

STATIC BOOL tgaLoadImageIdField(IN nblFile* file, OUT nblSurface* out, IN blTgaHeader* header) {

	if (header->imageIdLength == 0)
		return FALSE;
	/* this is intentional for clarification. if imageIdLength > 0, we need to read it in.
	We don't quite support this yet though. */
	BlrPrintf("\n\rImageID length %u not supported", header->imageIdLength);
	return FALSE;
}

STATIC BOOL tgaLoadColorMapTable(IN nblFile* file, OUT nblSurface* out, IN blTgaHeader* header) {

	if (header->colorMapType == TGA_COLOR_MAP_TYPE_NONE)
		return FALSE;
	/* this is intentional for clarification. if colorMapType > 0, we need to read it in.
	We don't quite support this yet though. */
	BlrPrintf("\n\rColorMapType %u not supported", header->imageIdLength);
	return FALSE;
}

STATIC BOOL tgaLoadUncompressed(IN nblFile* file, OUT nblSurface* out, IN blTgaHeader* header) {

	uint32_t imageSize;

	imageSize = out->width * out->height * out->format.bytesPerPixel;

	tgaLoadImageIdField(file, out, header);
	tgaLoadColorMapTable(file, out, header);

	out->pixels = BlrAlloc(imageSize);

	if (BlrFsRead(file, imageSize, out->pixels) == 0) {
		BlrFree(out->pixels);
		return FALSE;
	}

	BlrPrintf("\n\rTGA Loaded");
	return TRUE;
}

typedef struct _blTgaImageRlePacket {
	uint8_t repetionCount;
	/* variable number of pixels depending on repetitionCount bit 7. */
	uint8_t value[1];
}blTgaImageRlePacket;

STATIC BOOL tgaLoadCompressed(IN nblFile* file, OUT nblSurface* out, IN blTgaHeader* header) {

	blTgaImageRlePacket packet;
	uint32_t imageSize;
	uint32_t pixelCount;
	uint32_t currentPixel;
	uint32_t currentByte;
	uint8_t* pixels;

	BlrPrintf("\n\r*** Compressed TGA");

	tgaLoadImageIdField(file, out, header);
	tgaLoadColorMapTable(file, out, header);

	imageSize = header->imageSpec.height * header->imageSpec.width * header->imageSpec.depth;

	out->pixels = BlrAlloc(imageSize);
	if (!out->pixels)
		BlrPrintf("\n\rAlloc fail");

	pixels = (uint8_t*)out->pixels;

	pixelCount = out->width * out->height;
	currentPixel = 0;
	currentByte = 0;

	do {

		uint32_t total;

		if (BlrFsRead(file, sizeof(uint8_t), &packet.repetionCount) == 0) {
			BlrFree(out->pixels);
			return FALSE;
		}

		// The lower 7 bits of the Repetition Count specify how many pixel values are represented by the packet
		total = (packet.repetionCount & 0x7f) + 1;

		/* RAW format. */
		if (packet.repetionCount < 128) {

			uint32_t span;

			span = total * out->format.bytesPerPixel;

			if (BlrFsRead(file, span, &pixels[currentByte]) == 0)
				return FALSE;

			currentByte += span;
			currentPixel += total - 1;
		}

		/* RLE format. */
		else{

			uint8_t pixel[4];
			uint32_t current;

			/* read single pixel. */
			if (BlrFsRead(file, out->format.bytesPerPixel, pixel) == 0)
				return FALSE;

			for (current = 0; current < total; current++) {
				BlrCopyMemory(&pixels[currentByte], pixel, out->format.bytesPerPixel);
				currentByte += out->format.bytesPerPixel;
			}
			currentPixel += total - 1;
		}

	} while (currentPixel < pixelCount);

	BlrPrintf("\n\rSuccess");

	return TRUE;
}

PUBLIC BOOL tgaLoad(IN char* filename, OUT nblSurface* out) {

	nblFile* file;
	blTgaHeader header;
	BOOL status;
	nblColor col = { 0, 0, 0, 0 };

	file = BlrFileOpen(filename);
	if (!file) {
		BlrPrintf("\n\rUnable to open file");
		return FALSE;
	}

	status = FALSE;

	if (BlrFsRead(file, sizeof(blTgaHeader), &header) != 0) {

		out->width = header.imageSpec.width;
		out->height = header.imageSpec.height;
		out->format.bpp = header.imageSpec.depth;

		if (out->width <= 0 || out->height <= 0 || (out->format.bpp != 24 && out->format.bpp != 32))
			return FALSE;

		out->format.bytesPerPixel = out->format.bpp / 8;
		out->pitch = out->width;

		out->format.palette = NULL;
		out->format.front = col;
		out->format.back = col;

		out->format.bmask = 0x0000ff;
		out->format.rmask = 0xff0000;
		out->format.gmask = 0x00ff00;

		out->format.amask = 0;
		if (out->format.bpp == 32)
			out->format.amask = 0xff000000;

		BlCreateSurface(out, out->width, out->height, out->format.bpp, out->format.rmask,
			out->format.gmask, out->format.bmask, out->format.amask);

		out->flags = NBL_SFLAGS_CAPS | NBL_SFLAGS_WIDTH | NBL_SFLAGS_HEIGHT | NBL_SFLAGS_PITCH
			| NBL_SFLAGS_DEPTH | NBL_SFLAGS_PIXELS | NBL_SFLAGS_CLIP;
		out->caps = NBL_SCAPS_SYSTEMMEMORY;

		out->clip = BL_RECT(0, 0, out->width, out->height);

		out->device = NULL;
		out->attached = NULL;
		out->ex = NULL;

		switch (header.imageType) {
			case TGA_IMAGE_TYPE_UNCOMPRESSED_COLOR_MAPPED:
			case TGA_IMAGE_TYPE_UNCOMPRESSED_TRUE_COLOR:
			case TGA_IMAGE_TYPE_UNCOMPRESSED_BACK_WHITE:
				status = tgaLoadUncompressed(file, out, &header);
				break;
			case TGA_IMAGE_RLE_COLOR_MAPPED:
			case TGA_IMAGE_RLE_TRUE_COLOR:
			case TGA_IMAGE_RLE_BLACK_WHITE:
				status = tgaLoadCompressed(file, out, &header);
				break;
		};
	}

	BlrFsClose(file);
	return status;
}

