/************************************************************************
*
*	gmenu.c - Graphics Menu
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "font.h"
#include "gfx.h"
#include "nboot.h"

/* Default background color. */
#define BACKGROUND_COLOR RGBA(100,100,255,255)

/* graphics font (if in graphics modes.) */
blFont           _font;

/* primary surface (if in graphics mode.) */
nblSurface       _primary;

extern nblDeviceObject* _conout;
struct _uiElement;

/* UI Element Controller. */
typedef void(*UIELEMENT_CONTROLLER)(struct _uiElement* self, int msg, uint32_t arg1, uint32_t arg2);

/* UI Element Message Types. */
#define WM_PAINT    1
#define WM_KEYDOWN  2

/* Helper macros. */
#define WIDTH(r)(r.right - r.left)
#define HEIGHT(r)(r.bottom - r.top)

#define BORDER_SIZE 5
#define TITLEBAR_SIZE 40



/*
	UI Elements are the basic building blocks of this UI.
	They can have an optional name for debugging, optional
	dirty flag to indicate that it needs to be redrawn, and an optional
	show/hide flag.

	The position field has two forms:

		(0, bottom, 0, right)

		This is what the element sees. The element itself must ignore
		the LEFT and TOP fields and must assume they are always 0.

		(<TOP>, bottom, <LEFT>, right)

		This is what the parent of the element sees. The TOP and LEFT
		part of the rectangle contain the relative position within
		its parent.

	The client field is currently not used.

	The background field contains the default background color to use.
	Note that opique elements should set the alpha component to 255.
	This helps with performance and prevents unecessary repaints.

	The controller must point to a callback function that is responsible
	for the element.
*/
typedef struct _uiElement {

	/* PUBLIC members. */

	wchar_t* name;
	BOOL     dirty;
	BOOL     show;
	nblColor background;
	UIELEMENT_CONTROLLER controller;

	/* Partially opaque members. Call functions to modify. */

	nblRect  position;
	nblRect  client;

	/* Opaque members. Do not access directly. */

	nblRect  redraw;
	struct _uiElement* parent;
	struct _uiElement* firstChild;
	struct _uiElement* nextChild;

}uiElement;

/* cast UI element to Generic UI element. */
#define TO_UIE(e) ((uiElement*)e)

/* cast Generic UI element to specific element. */
#define FROM_UIE(type,e) ((type*)e)

/* initialize element. */
PRIVATE void UiInitElement(IN uiElement* e, nblColor background, UIELEMENT_CONTROLLER controller) {

	e->background = background;
	e->controller = controller;
	e->parent     = NULL;
	e->firstChild = NULL;
	e->nextChild  = NULL;
	e->dirty      = TRUE;
	e->position = BL_RECT(0, 0, 0, 0);
	e->redraw = BL_RECT(0, 0, 0, 0);
	e->client = BL_RECT(0, 0, 0, 0);
}

/* adds child element. */
PRIVATE void UiAddChild(IN uiElement* parent, IN uiElement* newChild) {

	newChild->nextChild = NULL;
	if (!parent->firstChild) {
		parent->firstChild = newChild;
		newChild->parent = parent;
		return;
	}
	else {
		uiElement* currentChild = parent->firstChild;
		while (currentChild->nextChild)
			currentChild = currentChild->nextChild;
		currentChild->nextChild = newChild;
		newChild->parent = parent;
	}
}

/* removes child element. */
PRIVATE uiElement* UiRemoveChild(IN uiElement* parent, IN uiElement* child) {

	uiElement* current;
	uiElement* previous;

	previous = NULL;
	for (current = parent->firstChild; current; previous = current, current = current->nextChild) {
		if (current == child) {
			if (previous)
				previous->nextChild = current->nextChild;
			else
				parent->firstChild = current->nextChild;				
			return current;
		}
	}
	return NULL;
}

/* move child to top of z-order list. */
PRIVATE uiElement* UiOrderTop(IN uiElement* parent, IN uiElement* child) {

	if (!UiRemoveChild(parent, child))
		return NULL;
	UiAddChild(parent, child);
	return child;
}

/* given coordinate system described by "e", compute screen coordinate of position. */
PRIVATE nblRect UiToScreenCoordinates(IN uiElement* e, IN nblRect position) {

	nblRect result = position;
	uiElement* p = e->parent;

	if (!p)
		return result;

	/* sum all parent position offsets. */
	while (p) {
		result.left   += p->position.left;
		result.top    += p->position.top;
		result.right  += p->position.left;
		result.bottom += p->position.top;
		p = p->parent;
	}

	/* add the original element position offset. */
	result.left   += e->position.left;
	result.top    += e->position.top;
	result.right  += e->position.left;
	result.bottom += e->position.top;

	return result;
}

/* test if region is within bounding rectangle (partially or whole.) */
PRIVATE BOOL UiTestRegionBoundingRect(IN nblRect region, IN nblRect boundary) {

	if (boundary.right  < region.left)   return FALSE;
	if (boundary.left   > region.right)  return FALSE;
	if (boundary.top    > region.bottom) return FALSE;
	if (boundary.bottom < region.top)    return FALSE;
	return TRUE;
}

/* given a region, clip it to boundary. If region is outside of boundary,
return FALSE. if it was clipped and region is valid, return TRUE. */
PRIVATE BOOL UiClip(IN nblRect* region, IN nblRect boundary) {

	/* if bounding rectangle is outside of region, nothing to do. */
	if (!UiTestRegionBoundingRect(*region, boundary))
		return FALSE;

	/* clip bounding rectangle so its inside of region. */
	if (boundary.left < region->left)     boundary.left = region->left;
	if (boundary.right > region->right)   boundary.right = region->right;
	if (boundary.top < region->top)       boundary.top = region->top;
	if (boundary.bottom > region->bottom) boundary.bottom = region->bottom;

	*region = boundary;
	return TRUE;
}

/* invalidates the region of an element so it gets repainted. */
PRIVATE void UiInvalidate(IN uiElement* e) {

	uiElement* parent = e->parent;
	uiElement* child = e;
	/*
		The redraw region is always relative to the current element itself.
		Because of this, its location is always (0,0) with its own width and height.
	*/
	e->dirty = TRUE;
	e->redraw = BL_RECT(0, HEIGHT(e->position), 0, WIDTH(e->position));

	while (child) {
		/*
			Set the parents redraw region. This must be set to the location of
			where the original element is to be drawn with respect to the parent.
		*/
		if (parent) {
			parent->redraw = child->redraw;
			parent->redraw.top    += child->position.top;
			parent->redraw.left   += child->position.left;
			parent->redraw.bottom += child->position.top;
			parent->redraw.right  += child->position.left;
		}

		child = parent;
		child->dirty = TRUE;

		if (parent)
			parent = parent->parent;
	}
}

/* given a child element, force a repaint of its redraw region on all its parents. */
PRIVATE void UiRepaintAll(IN uiElement* child) {

	uiElement* current;
	uiElement* parentChain[4];
	int c;

	/* Invalidate the element. */
	UiInvalidate(child);

	/* If this child has no parent, paint the child and return. */
	if (! child->parent) {
		child->controller(child, WM_PAINT, 0, 0);
		return;
	}

	/* Go through all parents of this child and store them. */
	for (c = 0, current = child; current->parent && c < 4; current = current->parent, c++)
		parentChain[c] = current;
	parentChain[c] = current;

	/* Paint them from the top most parent to child (front to back.) */
	for (; c >= 0; c--)
		parentChain[c]->controller(parentChain[c], WM_PAINT, 0, 0);
}

/* given a parent, force a repaint on all child elements. */
PRIVATE void UiPaintAll(IN uiElement* parent) {

	uiElement* child;
	if (!parent)
		return;

	/* Invalidate the parent and force a repaint. */
	UiInvalidate(parent);
	parent->controller(parent, WM_PAINT, 0, 0);

	/* Draw all of its children. Note the order is dependent
	on the childs' z-index. */
	for (child = parent->firstChild; child; child = child->nextChild)
		UiPaintAll(child);
}

/* change the size of an element. */
PRIVATE void UiSize(IN uiElement* element, IN uint32_t width, IN uint32_t height) {

	element->position.right = element->position.left + width;
	element->position.bottom = element->position.top + height;
}

/* move an element around. */
PRIVATE void UiPosition(IN uiElement* element, IN uint32_t x, IN uint32_t y) {

	uint32_t width = element->position.right - element->position.left;
	uint32_t height = element->position.bottom - element->position.top;

	element->position.left = x;
	element->position.top = y;
	UiSize(element, width, height);
}

/* gets position. Note the actual position is relative to the child itself
and is always (0,0). Used with UiClip. */
#define UiGetPosition(e) \
	BL_RECT(0, HEIGHT(e->position), 0, WIDTH(e->position))

/* gets child element by index. */
PRIVATE uiElement* UiGetChildByIndex(IN uiElement* parent, IN index_t index) {

	uiElement* p;
	index_t current = 0;

	if (index == 0)
		return parent->firstChild;
	for (p = parent->firstChild; p; p = p->nextChild) {
		if (current == index)
			return p;
		current++;
	}
	return NULL;
}

/* blends two colors. */
PRIVATE nblColor BlAlphaBlend(IN nblColor foreground, IN nblColor background) {

	nblColor result;

	result.red = (foreground.red * foreground.alpha) + (background.red * (1 - foreground.alpha));
	result.green = (foreground.green * foreground.alpha) + (background.green * (1 - foreground.alpha));
	result.blue = (foreground.blue * foreground.alpha) + (background.blue * (1 - foreground.alpha));
	result.alpha = (foreground.alpha * foreground.alpha) + (background.alpha * (1 - foreground.alpha));

	return result;
}

/* given a child, blend it with all of its parents. */
PRIVATE nblColor UiGetBlendColor(IN uiElement* child) {

	nblColor c;
	uiElement* parent;

	for (parent = child->parent; parent; parent = parent->parent)
		c = BlAlphaBlend(child->background, parent->background);

	return c;
}

/* draw rectangle. */
PRIVATE void UiDrawRect(IN uiElement* e, IN nblRect rc, IN nblColor color) {

	BlDrawRect(&_primary, UiToScreenCoordinates(e, rc), color);
}

/* draw rectangle. */
PRIVATE void UiDrawFillRect(IN uiElement* e, IN nblRect rc, IN nblColor color) {

	BlDrawFillRect(&_primary, UiToScreenCoordinates(e, rc), color);
}

/* draw line. */
PRIVATE void UiDrawLine(IN uiElement* e, IN int x1, IN int y1, IN int x2, IN int y2, IN nblColor color) {

	nblRect rc = UiToScreenCoordinates(e, BL_RECT(y1, y2, x1, x2));
	BlDrawLine(&_primary, rc.left, rc.top, rc.right, rc.bottom, color);
}

/* draw string. */
PRIVATE void UiDrawWideString(IN uiElement* e, IN wchar_t* s, IN int x, IN int y, IN nblColor color) {

	unsigned int k;
	int real_x;
	int real_y;
	nblRect rc;

	/* make sure there is a string to display. */
	if (!s)
		return;

	/* must be always relative to the element position. */
	rc = UiToScreenCoordinates(e, BL_RECT(y, y + HEIGHT(e->position), x, x + WIDTH(e->position)));

	/* get the actual location. */
	real_x = rc.left;
	real_y = rc.top;

	for (k = 0; k < BlrWideStrLength(s); k++) {

		/* wrap around parent element. */
		if (real_x + _font.BBoxWidth > rc.right) {
			real_x = rc.left;
			real_y += _font.BBoxHeight;
		}

		/* and draw it. */
		BlDrawWideCharacter(s[k], &_font, color, RGBA(0, 0, 0, 0), real_x, real_y, &_primary);
		real_x += _font.BBoxWidth;
	}
}

/*** Grapical User Interface Elements. ***/

typedef struct _uiElementMenuItem {
	uiElement e;
	wchar_t*  label;
	BOOL      active;
}uiElementMenuItem;

typedef struct _uiElementMenu {
	uiElement e;
	index_t selection;
	index_t winBegin;
	index_t winEnd;
	uint32_t  count;
	uiElementMenuItem* current;
}uiElementMenu;

typedef struct _uiWindow {
	uiElement e;
	wchar_t*  title;
}uiWindow;

typedef struct _uiStatus {
	uiElement e;
	wchar_t* status;
}uiStatus;

typedef struct _uiScreen {
	uiElement e;
	nblSurface* wallpaper;
}uiScreen;

PRIVATE void ControllerScreen(IN struct _uiElement* self, IN int msg, IN uint32_t arg1, IN uint32_t arg2) {

	nblRect rc = UiGetPosition(TO_UIE(self));

	if (msg == WM_PAINT) {
		if (UiClip(&rc, self->redraw))
			UiDrawFillRect(self, rc, self->background);
		self->dirty = FALSE;
	}
}

PRIVATE void ControllerStatus(IN struct _uiElement* self, IN int msg, IN uint32_t arg1, IN uint32_t arg2) {

	uiStatus* item = (uiStatus*)self;
	nblRect rc = UiGetPosition(TO_UIE(self));

	if (msg == WM_PAINT) {
		if (self->show) {
			if (UiClip(&rc, self->redraw)) {
				/* backgrond. */
//				UiDrawFillRect(self, rc, BlAlphaBlend(self->background, BACKGROUND_COLOR));
				/* border. */
				UiDrawRect(self, rc, RGBA(255, 255, 255, 255));
				/* status. */
				UiDrawWideString(self, item->status,
					BORDER_SIZE + 1, 1 + item->e.client.bottom + (_font.BBoxHeight / 2)
					, RGBA(0, 0, 0, 255));
				UiDrawWideString(self, item->status,
					BORDER_SIZE, item->e.client.bottom + (_font.BBoxHeight / 2)
					, RGBA(255, 255, 255, 255));
			}
		}
		self->dirty = FALSE;
	}
}

void ControllerWindow(IN struct _uiElement* self, IN int msg, IN uint32_t arg1, IN uint32_t arg2) {

	uiWindow* item = (uiWindow*)self;
	nblRect rc = UiGetPosition(TO_UIE(self));

	if (msg == WM_PAINT) {
		if (UiClip(&rc, self->redraw)) {
			/* backgrond. */
			UiDrawFillRect(self, rc, BlAlphaBlend(self->background, BACKGROUND_COLOR));
			/* title. */
			UiDrawWideString(self, item->title,
				BORDER_SIZE + 1, 1 + (BORDER_SIZE / 2) + ((item->e.client.top / 2) - (_font.BBoxHeight / 2)), RGBA(0, 0, 0, 255));
			UiDrawWideString(self, item->title,
				BORDER_SIZE, (BORDER_SIZE / 2) + ((item->e.client.top / 2) - (_font.BBoxHeight / 2)), RGBA(255, 255, 255, 255));
		}
		self->dirty = FALSE;
	}
}

void ControllerMenuItem(IN struct _uiElement* self, IN int msg, IN uint32_t arg1, IN uint32_t arg2) {

	uiElementMenuItem* item = (uiElementMenuItem*)self;
	nblRect rc = UiGetPosition(TO_UIE(self));

	if (msg == WM_PAINT) {
		if (UiClip(&rc, self->redraw)) {

			/* backdround. */
			if (item->active)
				UiDrawFillRect(self, rc, RGBA(255, 255, 255, 125));
			else
				UiDrawFillRect(self, rc, self->background);

			if (item->label) {
				UiDrawWideString(self, item->label,
					BORDER_SIZE + 1, 1 + item->e.client.top + (_font.BBoxHeight / 2)
					, RGBA(0, 0, 0, 255));
				UiDrawWideString(self, item->label,
					BORDER_SIZE, item->e.client.top + (_font.BBoxHeight / 2)
					, RGBA(125, 125, 125, 255));
			}
		}
		self->dirty = FALSE;
	}
}

void UiAddMenuItem(IN uiElementMenu* menu, wchar_t* caption) {

	uiElementMenuItem* item = BlrAlloc(sizeof (uiElementMenuItem));
	UiInitElement(TO_UIE(item), RGBA(125, 255, 125, 125), ControllerMenuItem);

	if (menu->e.firstChild == NULL) {
		menu->count = 0;
		menu->selection = 0;
		menu->current = item;
		item->active = TRUE;
	}
	else {
		item->active = FALSE;
	}

	UiAddChild(TO_UIE(menu), TO_UIE(item));

	/* label and active state. */
	item->label = caption;
	item->e.name = caption;

	/* the width and height of each item is dependent on its parent menu. */
	item->e.position.right = WIDTH(menu->e.position) - (BORDER_SIZE * 2);
	item->e.position.bottom = _font.BBoxHeight + _font.BBoxHeight;

	menu->count++;
}

void ControllerMenu(IN struct _uiElement* self, IN int msg, IN uint32_t arg1, IN uint32_t arg2) {

	uint32_t key = (uint32_t)arg1;
	uiElementMenu* menu = (uiElementMenu*)self;
	uiElement* child;

	switch (msg) {
	case WM_PAINT: {

		int c = 0;

		for (child = self->firstChild; child; child = child->nextChild) {
			UiPosition(child, 10, 30 * c);
			c++;
		}

		self->dirty = FALSE;
		break;
	}
	case WM_KEYDOWN:

		/* if there are no children, there is nothing to select. */
		if (self->firstChild == NULL)
			break;

		/* if current is not yet set, default to first child element. */
		if (menu->current == NULL) {
			menu->current = FROM_UIE(uiElementMenuItem,self->firstChild);
			menu->selection = 0;
		}

		/* process input. */
		switch (key) {
		case NBL_VK_UP:
			if (menu->selection > 0){

				menu->current->active = FALSE;
				UiRepaintAll(TO_UIE(menu->current));
				menu->selection--;
				menu->current = FROM_UIE(uiElementMenuItem, UiGetChildByIndex(TO_UIE(menu), menu->selection));
				menu->current->active = TRUE;
				UiRepaintAll(TO_UIE(menu->current));
			}
			break;
		case NBL_VK_DOWN:
			if (menu->selection < menu->count - 1) {

				menu->current->active = FALSE;
				UiRepaintAll(TO_UIE(menu->current));
				menu->selection++;
				menu->current = FROM_UIE(uiElementMenuItem, UiGetChildByIndex(TO_UIE(menu), menu->selection));
				menu->current->active = TRUE;
				UiRepaintAll(TO_UIE(menu->current));
			}
			break;
		case NBL_VK_RETURN:
			; // BlBoot(selection);
			break;
		default:
			break; /* ignore. */
		};
		break;
	};
}

PUBLIC void BlGraphicsMain(IN nblDriverObject* mapper) {

	nblKeyTable* map;
	uint32_t previousTime;
	BOOL countdown_flag;
	uint32_t timeout;
	STATIC wchar_t status_bar[40];

	uiWindow* window = BlrAlloc(sizeof(uiWindow));
	uiElementMenu* menu = BlrAlloc(sizeof(uiElementMenu));
	uiStatus* status = (uiStatus*) BlrAlloc(sizeof(uiStatus));
	uiScreen* screen = (uiScreen*) BlrAlloc(sizeof(uiScreen));

	window->e.name = L"Window";
	menu->e.name = L"Menu";
	status->e.name = L"Status";
	screen->e.name = L"Screen";

	UiInitElement(TO_UIE(screen), BACKGROUND_COLOR, ControllerScreen);
	UiSize(TO_UIE(screen), _primary.width, _primary.height);

	UiInitElement(TO_UIE(window), RGBA(255, 255, 255, 200), ControllerWindow);

	UiInitElement(TO_UIE(menu), RGBA(255, 255, 255, 250), ControllerMenu);

	UiInitElement(TO_UIE(status), RGBA(255, 255, 255, 200), ControllerStatus);

	/* the window is centered on screen. */
	UiPosition(TO_UIE(window), _primary.width / 4, _primary.height / 4);
	UiSize(TO_UIE(window), _primary.width / 2, _primary.height / 2);

	/* the menu is the client area of the window. */
	UiPosition(TO_UIE(menu), 0, 0);
	UiSize(TO_UIE(menu), 300, 300);

	window->title = L"Please select an operating system:";

	UiPosition(TO_UIE(status), window->e.position.left, window->e.position.bottom + BORDER_SIZE);
	UiSize(TO_UIE(status), WIDTH(window->e.position), _font.BBoxHeight + BORDER_SIZE);

	UiAddMenuItem(menu, L"Item 1");
	UiAddMenuItem(menu, L"Item 2");
	UiAddMenuItem(menu, L"Item 3");

	UiAddChild(TO_UIE(window), TO_UIE(menu));
	UiAddChild(TO_UIE(screen), TO_UIE(window));
	UiAddChild(TO_UIE(screen), TO_UIE(status));

	map = BlrGetKeyboardMap(mapper);

	timeout = 30;
	countdown_flag = TRUE;
	previousTime = RtcGetTicks();

	UiPaintAll(TO_UIE(screen));

	while (TRUE) {

		uint32_t scan = 0;

		/* wait for keypress and map it to VK code. */
		scan = BlrTerminalInputReadKey(BlrOpenDevice("conin"));

		if (countdown_flag) {

			if (scan) {
				status->e.show = FALSE;
//				UiPaint(screen, status);
				countdown_flag = FALSE;
			}

			if (timeout <= 0) {
				countdown_flag = FALSE;
			}

			if (RtcGetTicks() - previousTime > 36) {

				timeout--;
				previousTime = RtcGetTicks();

				BlrWideSPrintf(status_bar, L"Seconds until option is selected: %i", timeout);
				status->status = status_bar;

				status->e.show = TRUE;
//				UiPaint(screen, status);
			}
		}

		if (scan < 0x80)
			menu->e.controller(TO_UIE(menu), WM_KEYDOWN, map->vscToVkMap[scan], 0);
	}

	__asm{
		mov eax, 0xaaaaaaaa
		cli
		hlt
	}
}


STATIC status_t BlInitializeGraphicsTerminal(IN nblDeviceObject* dev, IN nblSurface* draw, IN blFont* font) {

	nblMessage m;
	if (!dev)
		return NBL_STAT_INVALID_ARGUMENT;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONOUT_INIT;
	m.NBL_CONOUT_SURFACE = (uint32_t)draw;
	m.NBL_CONOUT_FONT = (uint32_t)font;

	return dev->driverObject->request(&m);
}

void BlConfigureGraphicsTerminal(IN char* font) {

	nblDeviceObject* gfxout = BlrOpenDevice("gfxout");
	BlFontLoadBDF(font, &_font);
	BlInitializeGraphicsTerminal(gfxout, &_primary, &_font);
	BlrFileOpen("gfxout");
	_conout = BlrOpenDevice("gfxout");
}

/* set video mode and create primary surface. */
STATIC BOOL BlInitGraphics(char* device, int width, int height, int depth) {

	nblDeviceObject* d;
	nblRect rc;

	/* open graphics device. */
	d = BlrOpenDevice(device);
	if (!d)
		return FALSE;

	/* create primary surface. BlInitGraphics */
	_primary.caps = NBL_SCAPS_PRIMARYSURFACE;
	_primary.flags = NBL_SFLAGS_CAPS;
	_primary.device = d;

	/* set video mode. */
	if (BlrSetVideoMode(&_primary, width, height, depth) != NBL_STAT_SUCCESS)
		return FALSE;

	/* create primary surface. */
	if (BlrCreateSurface(&_primary) != NBL_STAT_SUCCESS)
		return FALSE;

	/* background rect. */
	rc.top = 0;
	rc.left = 0;
	rc.right = _primary.width;
	rc.bottom = _primary.height;

	/* draw background. */
	BlDrawFillRect(&_primary, rc, BACKGROUND_COLOR);

	/* all good. */
	return TRUE;
}

PUBLIC BOOL BlGraphicsMainLoop(IN nblDriverObject* mapper, IN uint32_t timeout) {

	STATIC int selection = 0;

	PROPERTY* mode;
	uint32_t width;
	uint32_t height;
	uint32_t bpp;

	static uint32_t previousTime = 0;
	static BOOL countdown_flag = TRUE;

	/* attempt to find mode information to set. */
	mode = PListQueryPath("mode", NULL);
	if (!mode)
		return FALSE;
	if (mode->key.type != REG_STRING)
		return FALSE;

	/* attempt to parse graphics mode string. */
	if (!BlStrToGraphicsMode(mode->key.u.s, &width, &height, &bpp))
		return FALSE;

	/* setup video information. */
	if (!BlInitGraphics("video", width, height, bpp))
		return FALSE;

	/* setup the terminal. */
	BlConfigureGraphicsTerminal("disk(1):radon.bdf");

	/* run main. */
	BlGraphicsMain(mapper);

	return TRUE;
}
