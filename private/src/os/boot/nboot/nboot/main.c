/************************************************************************
*
*	NBoot Entry Point.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/*
	The following component implements the core NBOOT program.
*/

/*
timeout = 30
mode = "800x600x32"
font = disk(0):radon.bdf
os {
	Neptune {
		kernel = "disk(0):nexec.exe -debug"
		module = disk(0):file.txt
		module = disk(0):lol.txt
		module = disk(0):common.inc
		module = disk(0):Folder1/Folder2/Test.txt
		module = disk(0):Fat12.inc
	}
	Windows {
		kernel = disk(0):nexec.exe
	}
}
*/

#include <nbl.h>
#include <key.h>
#include <gfx.h>
#include "font.h"
#include "nboot.h"

/* console output device. */
nblDeviceObject* _conout;

/* configure console device. */
PRIVATE STATIC void BlConfigureConsole (char* dev) {

	/* set default console device. */
	_conout = BlrOpenDevice(dev);
	return;
}

/* attempt to mount fs driver on device object. */
int BlFileSystemDriverHook (nblDriverObject* drv, void* ex) {

	nblDeviceObject* dev = (nblDeviceObject*)ex;
	if (drv->type == NBL_DRV_CLASS_FS) {
		BlrFsAttach(dev, drv);
	}
	return FALSE;
}

/* mount device by iterating through fs drivers. */
int BlDeviceMountHook (nblDeviceObject* dev, void* ex) {

	if (dev->type == NBL_DEVICE_DISK_PERIPHERAL || dev->type == NBL_DEVICE_DISK_CONTROLLER) {
		BlDriverIterate(BlFileSystemDriverHook, dev);
	}
	return FALSE;
}

/* iterates through all devices to try to mount them. */
STATIC status_t BlMountDevices (void) {

	BlrDeviceIterate(0xffffffff, BlDeviceMountHook,0);
	return NBL_STAT_SUCCESS;
}

int BlDeviceHook (nblDeviceObject* dev, void* ex) {

	BlrPrintf(" %s", dev->name);
	return FALSE;
}

void BlDisplayDevices (void) {

	BlrPrintf("\n\rDevices :");
	BlrDeviceIterate(0xffffffff, BlDeviceHook,0);
}

int BlMountedDeviceHook (nblDeviceObject* dev, void* ex) {

	if (dev->attached != NULL) {
		BlrPrintf("\n\r %s->%s", dev->name, dev->attached->name);
	}
	return FALSE;
}

void BlDisplayMountedDevices (void) {

	BlrPrintf("\n\rMounted :");
	BlrDeviceIterate(0xffffffff, BlMountedDeviceHook,0);
}

int BlDriverHook (nblDriverObject* drv, void* ex) {

	BlrPrintf(" %s", drv->name);
	return FALSE;
}

/* displays driver list. */
PRIVATE STATIC void BlDisplayDrivers (void) {

	BlrPrintf("\n\rDrivers :");
	BlDriverIterate(BlDriverHook,0);
}

/* Load operating system additional boot modules. */
void BlLoadBootModules (PROPERTY* os) {

	PROPERTY* node;
	unsigned int moduleCount;
	unsigned int c;

	if (!os)
		return;

	c = 0;
	moduleCount = 0;
	for (node = PListFirstChild(os); node; node = PListNextChild(node)) {

		if (BlrStrCompare(node->name, "module") == 0) {
			if (node->key.type != REG_STRING)
				continue;

			moduleCount++;
		}
	}

	BlMbInitModuleList (moduleCount);

	for(node = PListFirstChild(os); node; node = PListNextChild(node)) {

		if (BlrStrCompare(node->name, "module") == 0) {
			if (node->key.type != REG_STRING)
				continue;

			BlrPrintf("\n\rLoading %s...", node->key.u.s);
			if (!BlMbLoadModule(node->key.u.s))
				BlrPrintf("\n\r*** Unable to load file.");
		}
	}
}

/* removes spaces from string. */
PRIVATE STATIC void _BlRemoveSpaces(char* source) {

	char* i = source;
	char* j = source;
	while (*j != 0)
	{
		*i = *j++;
		if (*i != ' ')
			i++;
	}
	*i = 0;
}

/* Extracts the width, height, bpp from graphics mode string. */
PUBLIC BOOL BlStrToGraphicsMode(IN char* str, OUT uint32_t* x, OUT uint32_t* y, OUT uint32_t* z) {

	char* memory;

	_BlRemoveSpaces(str);

	memory = str;
	*x = BlrStrToLong(memory, &memory, 10);
	memory++;
	*y = BlrStrToLong(memory, &memory, 10);
	memory++;
	*z = BlrStrToLong(memory, &memory, 10);
	if (*memory != NULL)
		BlrPrintf("\n\r*** Trailing space");
	return TRUE;
}

/* boots operating system. */
PUBLIC status_t BlBoot (IN int index) {

	PROPERTY* node;
	PROPERTY* kernel;
	PROPERTY* mode;
	int count;

	node = PListQueryPath ("oslist", NULL);
	count = 0;
	for(node = PListFirstChild(node); node; node = PListNextChild(node)) {
		if (count == index)
			break;
		count++;
	}
	if (!node)
		goto error;

	/* read kernel properties. Note that the "module"
	property is read later. */
	kernel = PListFindChild(node, "kernel");
	mode   = PListFindChild(node, "mode");

	if (!kernel)
		goto error;

	if (kernel->key.type != REG_STRING)
		goto error;

	/* if the file name is just a device name then chain load it. */
	if (!BlrStrLocateFirst(kernel->key.u.s, ':')) {

		/* clear the screen and reposition cursor. */
		BlrTerminalOutputClearScreen(_conout,WHITE,BLACK);
		BlrTerminalOutputSetPosition(_conout,0,8);

		/* chainload entry. */
		BlrChainload(kernel->key.u.s);
		return NBL_STAT_SUCCESS; /* should not get here. */
	}
	else{

		char* command_line;

		/* get command line. */
		command_line = BlrStrLocateFirst(kernel->key.u.s, ' ');

		/* load kernel first. */
		if (BlMbLoad(kernel->key.u.s, command_line) == FALSE)
			goto error;

		/* load boot modules next. */
		BlLoadBootModules (node);

		/* set video mode if requested. */
		if (mode && mode->key.type == REG_STRING) {

			uint32_t width, height, bpp;
			BlStrToGraphicsMode(mode->key.u.s, &width, &height, &bpp);
			BlMbSetVideoMode(width, height, bpp);
		}

		/* boot it. */
		BlMbBoot();
		return NBL_STAT_SUCCESS;
	}

	/* fall through. */
error:
	if (node) {
		BlrTerminalOutputSetPosition(_conout,0,22);
		BlrTerminalOutputSetAttribute(_conout,RED,BLACK);
		BlrPrintf("\n\r*** Unable to boot '%s'", node->name);
	}
	return 1;
}

/* Called on fatal error. */
PUBLIC void BlFatalError(IN char* format, ...) {

	char buffer[80];
	va_list args;

	BlrZeroMemory(buffer,80);

	NBL_VA_START (args, format);
	BlrWriteVaListToString (buffer,format,args);
	NBL_VA_END (args);

	BlrPrintf("\n\rA fatal error has been detected:\n\n\r");
	BlrPrints (buffer);
	while (1){ /* prevent waking from nmi or smi. */
		__asm{
			cli
			hlt
		}
	}
}

/* startup main program. */
PUBLIC status_t BlRun(void) {

	nblDriverObject* mapper;
	PROPERTY* timeout;
	PROPERTY* mode;
	uint32_t timeout_value;

	/* open default mapper - first we try UEFI then BIOS. */
	mapper = BlrOpenDriver("root/kbdefi");
	if (!mapper) {
		/* try BIOS driver */
		mapper = BlrOpenDriver("root/kbdus");
	}
	if (!mapper) {
		/* no firmware mapper module. */
		BlrPrintf("\n\r*** No keyboard mapper found");
		BlrExit(NBL_FW_EXIT);
	}

	/* configure console device. */
	BlConfigureConsole("conout");

	/* clear screen. */
	BlrTerminalOutputClearScreen(_conout, WHITE, BLACK);

	/* disable cursor. */
	BlrTerminalOutputEnableCursor(_conout, FALSE);

	/* get configuration and set timeout. */
	timeout = PListQueryPath("timeout", NULL);

	/* get mode request, if any. */
	mode = PListQueryPath("mode", NULL);

	/* set default timeout if it was not specified. */
	if (!timeout)
		timeout_value = 30;
	else if (timeout->key.type != REG_NUMBER)
		timeout_value = 30;
	else
		timeout_value = timeout->key.u.n;

	/* run menu. */
	BlGraphicsMainLoop(mapper, timeout_value);

	/* if graphics menu setup fails, try text mode. */
	BlTextMainLoop(mapper, timeout_value);
}

/* Entry point.. */
int NBL_CALL BlMain (void) {

	PROPERTY* debug;

	/* initialize debug services. */
	DbgInitialize();

	/* mount device objects. */
	BlMountDevices ();

	/* clear screen. */
	BlrTerminalOutputClearScreen (_conout,BLUE,WHITE);

	/* debug info. */
	BlDisplayDrivers ();
	BlDisplayDevices ();
	BlDisplayMountedDevices();

	/* loads configuration. */
	BlParseConfig();

	/* if debug mode is specified, issue breakpoint. */
	debug = PListQueryPath("debug", NULL);
	if (debug)
		BlrDbgBreak();

	/* startup. */
	return BlRun ();
}

/*

Possible idea --

The drivers can export an entry point function like so:

__declspec(dllexport) void driver_entry(...);

During initialization, NBLC can scan its own export table to
update its IAT and call the driver entry points.

The driver entry points can register the driver objects.

-----

EFI Build

/INCLUDE:__ndbgDriverObject
/INCLUDE:__ndmDriverObject
/INCLUDE:__nfatDriverObject
/INCLUDE:__nmbrDriverObject
/INCLUDE:__nEfiDriverObject
/INCLUDE:__nEfiMmDriverObject
/INCLUDE:__nEfiRtcDriverObject
/INCLUDE:__nEfiDiskDriverObject
/INCLUDE:__nEfiGopDriverObject
/INCLUDE:__nEfiConInDriverObject
/INCLUDE:__nEfiConOutDriverObject
/INCLUDE:__nEfiSerialDriverObject
/INCLUDE:__nEfiDebugDriverObject
/INCLUDE:__nKBDUS

BIOS Build

/INCLUDE:__ndmDriverObject
/INCLUDE:__nfatDriverObject
/INCLUDE:__nBiosDriverObject
/INCLUDE:__nBiosOutDriverObject
/INCLUDE:__nBiosInDriverObject
/INCLUDE:__nBiosMmDriverObject
/INCLUDE:__nBiosDiskDriverObject
/INCLUDE:__nBiosVbeDriverObject
/INCLUDE:__nBiosChainloaderDriverObject
/INCLUDE:__nBiosDebugDriverObject
/INCLUDE:__nNLoadDriverObject
/INCLUDE:__nGfxConDriverObject
/INCLUDE:__nKBDUS
/ALIGN:512
*/
