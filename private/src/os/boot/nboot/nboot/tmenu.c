/************************************************************************
*
*	Text mode menu.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/*
The following component implements the text mode menu
*/

#include <nbl.h>
#include "nboot.h"

extern nblDeviceObject* _conout;

/* calculate the vertical center position for user menu. */
STATIC uint32_t BlUserMenuCenter(IN uint32_t totalY, IN uint32_t bootEntryCount) {

	uint32_t y;

	/* total y = bootEntryCount + 2 blank lines + "Please select Operating System" and "Seconds until selected" lines. */
	y = bootEntryCount + 4;

	/* shift it up by half. */
	y += (bootEntryCount + 4) / 2;

	/* return vertical center. */
	return (uint32_t)totalY / y;
}

/* prepare to draw menu. */
STATIC void BlUserMenuPrepare(IN int bootEntryCount) {

	uint32_t verticalPosition;

	verticalPosition = BlUserMenuCenter(80, bootEntryCount);
	BlrTerminalOutputSetPosition(_conout, 0, verticalPosition);
}

/* present user menu. */
STATIC uint32_t BlTextUserMenu(IN int selection) {

	PROPERTY* node;
	PROPERTY* child;
	index_t count;

	node = PListQueryPath("oslist", NULL);

	BlrTerminalOutputSetPosition(_conout, 0, 0);

	/* count number of boot menu options and prepare to center the menu. */
	count = 0;
	for (child = PListFirstChild(node); child; child = PListNextChild(child))
		count++;
	BlUserMenuPrepare(count);

	BlrTerminalOutputSetAttribute(_conout, WHITE, BLACK);

	BlrPrintf("Please select an operating system:\n\r");

	count = 0;
	for (child = PListFirstChild(node); child; child = PListNextChild(child)) {

		if (count == selection)
			BlrTerminalOutputSetAttribute(_conout, WHITE, BLACK);
		else
			BlrTerminalOutputSetAttribute(_conout, GRAY, BLACK);

		if (child->name)
			BlrPrintf("\n\r%i: %s", count + 1, child->name);
		count++;
	}

	return count;
}

extern void DbgRaiseException(void);
extern void DbgSignal(void);

/* input processing loop. */
PUBLIC BOOL BlTextMainLoop(IN nblDriverObject* mapper, IN uint32_t timeout) {

	STATIC unsigned int selection = 0;
	index_t count;
	uint32_t scan;
	nblKeyTable* map;

	static uint32_t previousTime = 0;
	static BOOL countdown_flag = TRUE;

	while (TRUE) {

		scan = 0;
		count = BlTextUserMenu(selection);
		BlrPrintf("\n\n\r");

		BlrTerminalOutputSetAttribute(_conout, WHITE, BLACK);

		previousTime = RtcGetTicks();

		/* wait for keypress and map it to VK code. */
		while (!scan) {

			scan = BlrTerminalInputReadKey(BlrOpenDevice("conin"));
			if (scan) {
				countdown_flag = FALSE;
			}

			if (countdown_flag) {

				if (timeout <= 0) {
					if (!BlBoot(0))
						countdown_flag = FALSE;
				}

				if (RtcGetTicks() - previousTime > 36) {

					timeout--;
					previousTime = RtcGetTicks();
					BlrPrintf("\rSeconds until option is selected: %i    ", timeout);
				}
			}
			else{
				/* cleanup above. */
				int i = 0;
				BlrPrintf("\r");
				while (i++ < 70)
					BlrPrintf(" ");
			}
		}

		map = BlrGetKeyboardMap(mapper);

		/* process input. */
		if (scan < 0x80) {
			switch (map->vscToVkMap[scan]) {
			case NBL_VK_ESCAPE:
				ASSERT(FALSE); /* call debugger. */
				break;
			case NBL_VK_UP:
				if (selection > 0)
					selection--;
				break;
			case NBL_VK_DOWN:
				if (selection < count - 1)
					selection++;
				break;
			case NBL_VK_RETURN:
				BlBoot(selection);
				break;
			default:
				break; /* ignore. */
			};
		}
	}
}
