/************************************************************************
*
*	nboot.h - Internal Services.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef NBOOT_H
#define NBOOT_H

#include <nbl.h>

/* config.c */
extern PROPERTY* PListQueryPath(IN char* path, IN PROPERTY* root);
extern PROPERTY* PListFirstChild(IN PROPERTY* parent);
extern PROPERTY* PListNextChild(IN PROPERTY* first);

/* main.c */
extern BOOL BlStrToGraphicsMode(IN char* str, OUT uint32_t* x, OUT uint32_t* y, OUT uint32_t* z);
extern void BlFatalError(IN char* s, IN ...);
extern BOOL BlBoot(IN index_t entry);

#endif
