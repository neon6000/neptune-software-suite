/************************************************************************
*
*	Neptune Boot Library Configuration Services
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "nboot.h"

#if 0

Property List Grammar

<attribute> ::= <attribute_name> = <attribute_data>

<attribute_block> ::= <attribute_block_name> { <attribute_list> }

<attribute_list> ::= <attribute> <attribute_list>
                   | <attribute_block> <attribute_list>
                   ;

#endif

/*
===============================================

	Property List Structures

===============================================
*/

PRIVATE
char
tolower (IN char c) {
	return c > 0x40 && c < 0x5b ? c | 0x60 : c;
}

PRIVATE
void
StripCase(IN char* s,
		  OUT char* o) {

	size_t length = BlrStrLength(s);
	size_t i;
	for (i = 0; i < length; i++)
		o[i] = tolower(s[i]);
	o[length] = NULL;
}

static PROPERTY _root;

PRIVATE
PROPERTY*
PListFindChild(IN PROPERTY* root,
				  IN char* name) {

	LIST_ENTRY* entry;
	PROPERTY*  child;

	if (!root)
		root = &_root;

	for (entry = root->children.next;
		entry != &root->children;
		entry = entry->next) {

			child = (PROPERTY*) NRL_CONTAINING_RECORD(entry,PROPERTY,entry);
			if (BlrStrCompareEx(child->name, name, BlrStrLength(child->name)) == 0)
				return child;
	}
	return NULL;
}

PRIVATE
PROPERTY*
PListTreeInitialize(void) {

	_root.name = "nboot";
	BlrInitializeListHead(&_root.children);
	BlrInitializeListHead(&_root.entry);
	return &_root;
}

PRIVATE
PROPERTY*
PListAddChild(IN PROPERTY* parent) {

	PROPERTY* child;

	if (!parent)
		parent = &_root;

	child = BlrAlloc (sizeof (PROPERTY));
	if (!child)
		return NULL;

	child->name = NULL;
	child->parent = parent;

	BlrInitializeListHead(&child->children);
	BlrInitializeListHead(&child->entry);
	BlrInsertTailList(&parent->children,&child->entry);

	return child;
}

PRIVATE
PROPERTY*
_PListQueryPath (IN char* path,
				 IN PROPERTY* root) {

	PROPERTY*  child;

	if (!root)
		return NULL;
	if (BlrStrLength(path) == 0)
		return root;

	child = PListFindChild(root, path);
	if (child)
		return _PListQueryPath (path + BlrStrLength(path) + 1, child);
	return NULL;
}

PRIVATE
PROPERTY*
PListQueryPath (IN char* path,
				IN PROPERTY* root) {

	int length;
	char* s;
	char* tok;
	PROPERTY* result;

	if (!root)
		root = &_root;

	/* allocate temporary string. */
	length = BlrStrLength(path) + 3;
	s      = (char*) BlrAlloc(length);

	/* copy it, stripping its case. */
	StripCase(path, s);

	/* convert into multi string. */
	s[length-2] = NULL;
	s[length-1] = NULL;
	tok = BlrStrToken (s, "/");
	while (tok)
		tok = BlrStrToken(NULL, "/");

	/* read key. */
	result = _PListQueryPath(s, root);

	/* cleanup. */
	BlrFree(s);
	s = NULL;
	return result;
}

/* given a parent, get its first child. */
PRIVATE
PROPERTY*
PListFirstChild(IN PROPERTY* parent) {

	PROPERTY*   next;
	LIST_ENTRY* child;

	if (!parent)
		parent = &_root;

	child = parent->children.next;
	if (!child)
		return NULL;

	next = (PROPERTY*) NRL_CONTAINING_RECORD(child,PROPERTY,entry);
	return next;
}

/* given a siblig, get next sibling. */
PRIVATE
PROPERTY*
PListNextChild(IN PROPERTY* first) {

	PROPERTY*   next;
	LIST_ENTRY* sibling;

	if (!first)
		return NULL;

	/* if this is the last child of the parent, we are done. */
	sibling = first->entry.next;
	if (!sibling)
		return NULL;
	if (sibling == (LIST_ENTRY*) (&first->parent->children.next))
		return NULL;

	next = (PROPERTY*) NRL_CONTAINING_RECORD(sibling,PROPERTY,entry);
	return next;
}

PRIVATE
void
_PListPrintTree(PROPERTY* root, int ident) {

	LIST_ENTRY* entry;
	PROPERTY*   child;
	int i;

	if (!root)
		return;

	BlrPrintf(" \n\r");
	for(i=0; i<ident; i++)
		BlrPrintf(" ");
	if (root->name)
		BlrPrintf("%s", root->name);
	else
		BlrPrintf("<null>");

	BlrPrintf("\n\r");
	for(i=0; i<ident; i++)
		BlrPrintf(" ");

	for (entry = root->children.next;
		entry != &root->children;
		entry = entry->next) {
		child = (PROPERTY*) NRL_CONTAINING_RECORD(entry,PROPERTY,entry);

		_PListPrintTree (child, ident+1);
	}
}


void
PListPrintTree(PROPERTY* root) {

	if (!root)
		root = &_root;

	_PListPrintTree(root, 0);
}

/*
===============================================

	Scanner

===============================================
*/

typedef enum _TOKEN_TYPE {
	TOK_LABEL,
	TOK_SYMBOL,
	TOK_NUMBER,
	TOK_EOF
}TOKEN_TYPE;

typedef struct _PLIST_TOKEN {
	int type;
	ATTRIBUTE u;
}PLIST_TOKEN;

char* _cache;
int _size;
PLIST_TOKEN _tokens[100];

PRIVATE
BOOL
IsWhitespace(IN char c) {
	return (c && (c <= 0x20 || c == 0x7f));
}

PRIVATE
BOOL
IsSymbol(IN char c) {
	switch(c) {
		case ';':
		case '{':
		case '}':
		case '=':
			return TRUE;
		default:
			return FALSE;
	};
}

PRIVATE
int
ParseNumber(IN char* s,
			OUT int** n) {

	int number = 0;
	int length = 0;
	char num[32];
	char* t = s;

	for (t = s; !IsWhitespace(*t); t++) {
		if (IsSymbol(*t))
			break;
		length++;
	}
	/* too large a number. */
	if (length > 32 - 1)
		return 0;
	BlrCopyMemory(num,s,length);
	num[length] = NULL;
	number = BlrStrToLong(num, NULL, 10);
	*n = (int*) number;
	return length;
}

PRIVATE
int
ParseString(IN char* s,
			OUT char** c) {

	char* t = s;
	int length = 0;

	if (*t == '\'' || *t == '\"') {

		char delimeter = *t;
		for (t = s+1; *t != delimeter; t++) {

			if (*t == '\n' || *t == '\r' || *t == NULL /* end of cache. */ )
				BlFatalError("nboot.lst : Trailing string near '%c'", *(s+1));

			length++;
		}

		*c = BlrAlloc (length+1);
		BlrCopyMemory(*c,s+1,length);
		(*c) [length] = NULL;

		return length+2; /* consume ' or " and the delimeter. */
	}
	else{

		for (t = s; !IsWhitespace(*t); t++) {
			if (IsSymbol(*t))
				break;
			length++;
		}

		*c = BlrAlloc (length+1);
		BlrCopyMemory(*c,s,length);
		(*c) [length] = NULL;

		return length;
	}
}

PUBLIC
void
NbPListTokenize(IN char* file) {

	char* s = file;
	int index = 0;
	int tokCount = 0;
	PLIST_TOKEN tok;

	for (index = 0; index < _size - 1; index++) {

		/* ignore whitespace. */
		if (IsWhitespace( s[index] ))
			continue;

		/* comments. */
		if (s[index] == ';') {
			/* ignore rest of line. */
			while(s[index] && s[index] != '\n')
				index++;
			continue;
		}

		switch( s[index] ) {
			case '{':
			case '}':
			case '=':
			case ';': /* should not be hit. */
				tok.type = TOK_SYMBOL;
				tok.u.s = (char*) (s[index]);
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				tok.type = TOK_NUMBER;
				index += ParseNumber( &s[index] , (int**) (&tok.u.n)) - 1;
				break;
			default:
				tok.type = TOK_LABEL;
				tok.u.s = NULL;
				index += ParseString( &s[index] , &tok.u.s) - 1;
				break;
		};

		switch(tok.type) {
			case TOK_NUMBER:
				BlrCopyMemory(&_tokens[tokCount++], &tok, sizeof(PLIST_TOKEN));
				break;
			case TOK_LABEL:
				BlrCopyMemory(&_tokens[tokCount++], &tok, sizeof(PLIST_TOKEN));
				break;
			case TOK_SYMBOL:
				BlrCopyMemory(&_tokens[tokCount++], &tok, sizeof(PLIST_TOKEN));
				break;
			default:
				BlFatalError("nboot.lst : Invalid token '%x' at offset %u", *s, s - _cache);
				break;
		};
	}

	tok.type = TOK_EOF;
	tok.u.s = NULL;
	BlrCopyMemory(&_tokens[tokCount++], &tok, sizeof(PLIST_TOKEN));
}

PRIVATE
void
NbPListPrint(IN PLIST_TOKEN* tok) {
	switch(tok->type) {
		case TOK_NUMBER:
			BlrPrintf("number: %i", tok->u.n);
			break;
		case TOK_LABEL:
			BlrPrintf("label: %s", tok->u.s);
			break;
		case TOK_SYMBOL:
			BlrPrintf("symbol: %c", tok->u.c);
			break;
	};
}

/*
===============================================

	Parser

===============================================
*/

int _current = 0;

PRIVATE
PLIST_TOKEN*
NbPListLookAhead() {
	PLIST_TOKEN* tok =&_tokens[_current + 1];
	if (tok->type == TOK_EOF)
		return NULL;
	return tok;
}

PRIVATE
PLIST_TOKEN*
NbPListGetToken() {
	PLIST_TOKEN* tok =&_tokens[_current];
	if (tok->type == TOK_EOF)
		return NULL;
	return tok;
}

PRIVATE
PLIST_TOKEN*
NbPListNextToken() {
	if (NbPListGetToken()) {
		_current++;
		return NbPListGetToken();
	}
	return NULL;
}

PRIVATE
PROPERTY*
ParseAttribute(PROPERTY* node) {

	PLIST_TOKEN* current = NbPListGetToken();
	PLIST_TOKEN* next = NbPListLookAhead();
	PROPERTY*    prop = NULL;
	if (!next)
		return NULL;
	if (current->type != TOK_LABEL)
		return NULL;
	if (next->type != TOK_SYMBOL)
		return NULL;
	if (next->u.c != '=')
		return NULL;

	prop = PListAddChild(node);
	if (!prop)
		return NULL;

	prop->name = current->u.s;

	NbPListNextToken();

	next = NbPListNextToken();

	prop->key.type = next->type;
	switch(next->type) {
		case TOK_SYMBOL:
			prop->key.type = REG_CHARATER;
			prop->key.u.c = next->u.c;
			break;
		case TOK_NUMBER:
			prop->key.type = REG_NUMBER;
			prop->key.u.n = next->u.n;
			break;
		case TOK_LABEL:
			prop->key.type = REG_STRING;
			prop->key.u.s = next->u.s;
			break;
	};

	NbPListNextToken();
	return node;
}

PRIVATE
PROPERTY*
ParseAttributeList(PROPERTY* node);

PRIVATE
PROPERTY*
ParseAttributeBlock(PROPERTY* node) {

	PLIST_TOKEN* current = NbPListGetToken();
	PLIST_TOKEN* next = NbPListLookAhead();
	PROPERTY*    newParent = NULL;
	if (!next)
		return NULL;
	if (current->type != TOK_LABEL)
		return NULL;
	if (next->type != TOK_SYMBOL)
		return NULL;
	if (next->u.c != '{')
		return NULL;

	newParent = PListAddChild(node);

	newParent->name = current->u.s;

	NbPListNextToken();
	NbPListNextToken();

	ParseAttributeList(newParent);

	current = NbPListGetToken();

	if (current->type != TOK_SYMBOL)
		return NULL;
	if (current->u.c != '}')
		return NULL;
	NbPListNextToken();
	return node;
}

PRIVATE
PROPERTY*
ParseAttributeList(PROPERTY* node) {
	if (ParseAttribute(node))
		return ParseAttributeList(node);
	else if (ParseAttributeBlock(node))
		return ParseAttributeList(node);
	return NULL;
}

/*
===============================================

	Property Tree

===============================================
*/

PUBLIC
BOOL
NbPListLoad (IN char* path) {

	int size = 0;
	nblFile* _file;

	_file = BlrFileOpen(path);
	if (!_file) {
		BlFatalError ("nboot.lst: missing or corrupt.");
		return FALSE;
	}

	/* although file->size is 64 bits, we are limiting it to 32 bits. */
	size = (int)(_file->size & 0xffffffff);

	_cache = BlrAlloc(size + 1);
	if (!_cache) {
		BlrFsClose(_file);
		return FALSE;
	}

	_cache [size] = NULL;
	BlrZeroMemory(_cache,size);

	BlrFsRead(_file,size,_cache);
	BlrFsClose(_file);

	_size = size;
	return TRUE;
}

PRIVATE
BOOL
NbPListBuild (void) {

	NbPListTokenize(_cache);
	PListTreeInitialize();
	ParseAttributeList(&_root);

	if (NbPListGetToken()) {
		PLIST_TOKEN* tok;
		tok = NbPListGetToken();

		switch(tok->type) {
			case TOK_LABEL:
				BlFatalError ("nboot.lst: error near '%s'", tok->u.s);
				break;
			case TOK_SYMBOL:
				BlFatalError ("nboot.lst: error near '%c'", tok->u.c);
				break;
			case TOK_NUMBER:
				BlFatalError ("nboot.lst: error near '%i'", tok->u.n);
				break;
			case TOK_EOF:
				BlFatalError ("nboot.lst: <eof>");
				break;
		};
		return FALSE;
	}
	return TRUE;
}

BOOL BlParseConfig () {

	PROPERTY* node = NULL;

	if (!NbPListLoad("disk(0):nboot.lst"))
		return FALSE;

	if (!NbPListBuild())
		return FALSE;
	return TRUE;
}
