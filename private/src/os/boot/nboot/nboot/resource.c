/************************************************************************
*
*	Portable Executable (PE) Resource Support
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>

// https://en.wikibooks.org/wiki/X86_Disassembly/Windows_Executable_Files
// http://www.csn.ul.ie/~caolan/pub/winresdump/winresdump/doc/pefile.html
// http://www.csn.ul.ie/~caolan/pub/winresdump/winresdump/doc/resfmt.txt
// https://books.google.com/books?id=2yYT_SOlHzAC&pg=PA351&lpg=PA351&dq=string+table+resource+data+entry&source=bl&ots=F9st8-8oGl&sig=jBu1Ax7aczV-dBTNrpW13BUf8_g&hl=en&sa=X&ved=0ahUKEwj476-gn77QAhWo1IMKHXY9CtUQ6AEITjAH#v=onepage&q=string%20table%20resource%20data%20entry&f=false
// http://www.codeproject.com/Tips/431045/The-Inner-Working-of-FindResour

typedef struct _blImageDosHeader {
	uint16_t e_magic;
	uint16_t e_cblp;
	uint16_t e_cp;
	uint16_t e_crlc;
	uint16_t e_cparhdr;
	uint16_t e_minalloc;
	uint16_t e_maxalloc;
	uint16_t e_ss;
	uint16_t e_sp;
	uint16_t e_csum;
	uint16_t e_ip;
	uint16_t e_cs;
	uint16_t e_lfarlc;
	uint16_t e_ovno;
	uint16_t e_res[4];
	uint16_t e_oemid;
	uint16_t e_oeminfo;
	uint16_t e_res2[10];
	uint32_t e_lfanew;
}blImageDosHeader;

typedef struct _blImageFileHeader {
	uint16_t  Machine;
	uint16_t  NumberOfSections;
	uint32_t TimeDateStamp;
	uint32_t PointerToSymbolTable;
	uint32_t NumberOfSymbols;
	uint16_t  SizeOfOptionalHeader;
	uint16_t  Characteristics;
}blImageFileHeader;

typedef struct _blImageDataDirectory {
	uint32_t VirtualAddress;
	uint32_t Size;
}blImageDataDirectory;

#define IMAGE_NUMBEROF_DIRECTORY_ENTRIES 16

#define IMAGE_DIRECTORY_EXPORT       0
#define IMAGE_DIRECTORY_IMPORT       1
#define IMAGE_DIRECTORY_RESOURCE     2
#define IMAGE_DIRECTORY_EXCEPTION    3
#define IMAGE_DIRECTORY_SECURITY     4
#define IMAGE_DIRECTORY_RELOC        5
#define IMAGE_DIRECORY_DEBUG         6
#define IMAGE_DIRECTORY_COPYRIGHT    7
// skip 8 --
#define IMAGE_DIRECTORY_TLS          9
#define IMAGE_DIRECTORY_CONFIG       10
#define IMAGE_DIRECTORY_BOUND_IMPORT 11
#define IMAGE_DIRECTORY_IAD          12
#define IMAGE_DIRECTORY_DELAY_IMPORT 13
#define IMAGE_DIRECTORY_COM         14

typedef struct _blImageOptionalHeader {
	uint16_t                 Magic;
	uint8_t                 MajorLinkerVersion;
	uint8_t                 MinorLinkerVersion;
	uint32_t                SizeOfCode;
	uint32_t                SizeOfInitializedData;
	uint32_t                SizeOfUninitializedData;
	uint32_t                AddressOfEntryPoint;
	uint32_t                BaseOfCode;
	uint32_t                BaseOfData;
	uint32_t                ImageBase;
	uint32_t                SectionAlignment;
	uint32_t                FileAlignment;
	uint16_t                 MajorOperatingSystemVersion;
	uint16_t                 MinorOperatingSystemVersion;
	uint16_t                 MajorImageVersion;
	uint16_t                 MinorImageVersion;
	uint16_t                 MajorSubsystemVersion;
	uint16_t                 MinorSubsystemVersion;
	uint32_t                Win32VersionValue;
	uint32_t                SizeOfImage;
	uint32_t                SizeOfHeaders;
	uint32_t                CheckSum;
	uint16_t                 Subsystem;
	uint16_t                 DllCharacteristics;
	uint32_t                SizeOfStackReserve;
	uint32_t                SizeOfStackCommit;
	uint32_t                SizeOfHeapReserve;
	uint32_t                SizeOfHeapCommit;
	uint32_t                LoaderFlags;
	uint32_t                NumberOfRvaAndSizes;
	blImageDataDirectory DataDirectory[IMAGE_NUMBEROF_DIRECTORY_ENTRIES];
} blImageOptionalHeader;

typedef struct _blImageNtHeaders {
	uint32_t                 Signature;
	blImageFileHeader     FileHeader;
	blImageOptionalHeader OptionalHeader;
}blImageNtHeaders;

typedef struct _blImageResourceDirectoryEntry {
	uint32_t Name;
	uint32_t OffsetToData;
}blImageResourceDirectoryEntry;

typedef struct _blImageResourceDirectory {
	uint32_t Characteristics;
	uint32_t TimeDateStamp;
	uint16_t MajorVersion;
	uint16_t MinorVersion;
	uint16_t NumberOfNamedEntries;
	uint16_t NumberOfIdEntries;
	blImageResourceDirectoryEntry DirectoryEntries[];
}blImageResourceDirectory;

typedef struct _blImageResourceDataEntry {
	uint32_t OffsetToData;
	uint32_t Size;
	uint32_t CodePage;
	uint32_t Reserved;
}blImageResourceDataEntry;

typedef struct _blImageResourceDirectoryStringUnicode {
	uint16_t  Length;
	char16_t   NameString[1];
} _blImageResourceDirectoryStringUnicode;

#define RT_CURSOR           1
#define RT_BITMAP           2
#define RT_ICON             3
#define RT_MENU             4
#define RT_DIALOG           5
#define RT_STRING           6
#define RT_FONTDIR          7
#define RT_FONT             8
#define RT_ACCELERATOR      9
#define RT_RCDATA           10
#define RT_MESSAGETABLE     11

typedef struct _blImageResourceUnicodeString {
	uint16_t length;
	char16_t string[1];
}blImageResourceUnicodeString;

#define _nboot_base_address 0x20000

PRIVATE blImageResourceDirectoryEntry* BlGetResourceDirectoryEntry(IN blImageResourceDirectory* root, IN OPTIONAL int id, IN OPTIONAL char* name) {

	blImageResourceDirectoryEntry* entry;
	int c;

	BlrPrintf("\n\rBlGetResourceDirectoryEntry(%x,%u)", root, id);

	/* Named entries come first. Then ID entries. This order is important. */
//	for (c = 0; c < root->NumberOfNamedEntries; c++) {
//		entry = &root->DirectoryEntries[c];
//		resource_name = ((entry->Name & 0x7fffffff) + _nboot_base_address);
//		BlrPrintf("\n\r    Resource Directory Name: %x", resource_name);
//		continue;
//	}
	for (c = 0; c < root->NumberOfIdEntries; c++) {
		entry = &root->DirectoryEntries[c];
		BlrPrintf("\n\r    Resource Directory Id: %u", entry->Name);
		if (entry->Name == id)
			return entry;
	}
	return NULL;
}

PRIVATE blImageResourceDirectory* BlGetResourceTreeRoot(void) {

	blImageDosHeader* dosHeader;
	blImageNtHeaders* ntHeaders;
	blImageResourceDirectory* root;

	dosHeader = (blImageDosHeader*)_nboot_base_address;
	ntHeaders = (blImageNtHeaders*)(dosHeader->e_lfanew + _nboot_base_address);

	root = (blImageResourceDirectory*)(ntHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_RESOURCE].VirtualAddress + _nboot_base_address);

	return root;
}

PUBLIC void* BlLoadResource(IN int resourceType, IN int resourceId, IN int languageId) {

	blImageResourceDirectory*      root;
	blImageResourceDirectory*      current;
	blImageResourceDirectoryEntry* entry;
	blImageResourceDataEntry*      data;

	/* get root of resource tree */
	root = BlGetResourceTreeRoot();
	current = root;
	if (!current)
		return NULL;

	/* resource type */
	entry = BlGetResourceDirectoryEntry(current, resourceType, NULL);
	if (!entry)
		return NULL;
	current = (blImageResourceDirectory*)((entry->OffsetToData & 0x7fffffff) + (uint8_t*)root);

	/* resource identifier */
	entry = BlGetResourceDirectoryEntry(current, resourceId, NULL);
	if (!entry)
		return NULL;
	current = (blImageResourceDirectory*) ((entry->OffsetToData & 0x7fffffff) + (uint8_t*)root);

	/* resource language ID */
	entry = BlGetResourceDirectoryEntry(current, languageId, NULL);
	if (!entry)
		return NULL;
	current = (blImageResourceDirectory*) ((entry->OffsetToData & 0x7fffffff) + (uint8_t*)root);

	/* return pointer to resource data object */
	data = (blImageResourceDataEntry*) current;
	return (void*) (data->OffsetToData + _nboot_base_address);
}

PUBLIC size_t BlLoadString(IN int id, OUT char16_t* buffer, IN size_t bufferSize) {

	blImageResourceUnicodeString* currentString;
	index_t stringBlockIndex;
	index_t stringIndex;
	index_t currentIndex;

	if (! buffer)
		return 0;

	stringBlockIndex = (id >> 4) + 1;
	stringIndex = id & 0xf;

	currentString = (blImageResourceUnicodeString*)BlLoadResource(RT_STRING, stringBlockIndex, 1033);
	if (!currentString)
		return NULL;

	for (currentIndex = 0; currentIndex < stringIndex; currentIndex++)
		(uint16_t*)(currentString) += currentString->length + 1;

	if (currentIndex != stringIndex)
		return NULL;
	if (currentString->length == 0)
		return NULL;

	/* copy characters to buffer. Truncate if necessary. */
	for (currentIndex = 0; currentIndex < bufferSize - 2 && currentIndex < currentString->length; currentIndex++)
		buffer[currentIndex] = currentString->string[currentIndex];
	buffer[currentIndex] = NULL;

	/* return number of characters copied. */
	return currentIndex;
}
