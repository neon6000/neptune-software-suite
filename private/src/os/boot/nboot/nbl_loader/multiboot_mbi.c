/************************************************************************
*
*	MultiBoot Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* Unsupported:

	> Multiboot ELF Debug Symbols.
	> Drive structure.
	> APM Config Table.

*/

#include <nbl.h>
#include "elf.h"
#include "mbi.h"

typedef struct _mbModule {
	struct _mbModule* next;
	addr_t start;
	size_t size;
	char*  cmdline;
	size_t cmdlineSize;
}mbModule;

mbModule* modules;
mbModule* modulesLast;
size_t    cmdlineSize;
size_t    totalModuleCmd;
size_t    moduleCount;
char*     cmdline = NULL;
uint32_t  bootdev;
BOOL      bootdevSet;
BOOL      acceptsVideo;

PRIVATE
void
_BlMbSetAcceptsVideo(IN BOOL b) {
	acceptsVideo = b;
}

PRIVATE
void
_BlMbGetSize(void) {
	/*
  return sizeof (struct multiboot_info) + ALIGN_UP (cmdline_size, 4)
    + modcnt * sizeof (struct multiboot_mod_list) + total_modcmd
    + ALIGN_UP (sizeof(PACKAGE_STRING), 4) + get_multiboot_mmap_len ()
    + 256 * sizeof (struct multiboot_color);
	*/
	return 0;
}

PRIVATE
void
_BlMbFreeMbi (void) {

	mbModule* cur;
	mbModule* next;

	cmdlineSize = 0;
	totalModuleCmd = 0;
	moduleCount = 0;
	BlrFree (cmdline);
	cmdline = NULL;
	bootdevSet = FALSE;

	for (cur = modules; cur; cur = next) {
		next = cur->next;
		BlrFree (cur->cmdline);
		BlrFree (cur);
	}
	modules = NULL;
	modulesLast = NULL;
}

/* helper function to count number of bits in value.
Needed to count size of graphics surface bit masks. */
PRIVATE
uint32_t
_BlMbBitCount (IN uint32_t value) {
	uint32_t count = 0;
	while (value > 0) {
		if ((value & 1) == 1)
			count++;
		value >>= 1;
	}
	return count;
}

typedef enum {
	MMAP_DESCR_AVAILABLE,
	MMAP_DESCR_RESERVED,
	MMAP_DESCR_ACPI_RECLAIM,
	MMAP_DESCR_ACPI_NVS
}MMAP_DESCRIPTOR_TYPE;

/**
*	Memory descriptor.
*/
typedef struct _MMAP_DESCRIPTOR {
	uint64_t          base;
	uint64_t          length;
	uint32_t          type;
	OPTIONAL uint32_t acpiAttributes;
}MMAP_DESCRIPTOR, *PMMAP_DESCRIPTOR;


/* SEE nbl_bios/mm.c */

PRIVATE
void
_BlMbGetMemoryMap(IN MULTIBOOT_INFO* mbi) {

	size_t length;
	MMAP_DESCRIPTOR*  biosMemoryMap;
	MULTIBOOT_MMAP* mmap;
	size_t entryCount;
	index_t currentEntry;

	length = 0;
	biosMemoryMap = NULL;
	mmap = NULL;
	entryCount = 0;
	currentEntry = 0;

	/* get required size of memory map. */
	length = BlrGetMemoryMap(NULL);

	/* allocate it and get the memory map. */
	biosMemoryMap = (MMAP_DESCRIPTOR*) BlrAlloc (length);
	if (!biosMemoryMap)
		return;

	/* get number of actual entries. */
	entryCount = length / sizeof (MMAP_DESCRIPTOR);

	/* this must be allocated in ZONE_LOW since it is passed to the kernel. */
	BlrSetActiveHeap(ZONE_LOW);

	/* allocate memory map for multiboot information header. */
	mmap = (MULTIBOOT_MMAP*) BlrAlloc (entryCount * sizeof (MULTIBOOT_MMAP));
	if (!mmap) {
		BlrFree(biosMemoryMap);
		return;
	}

	/* restore normal heap. */
	BlrSetActiveHeap(ZONE_NORMAL);

	/* get system memory map. */
	BlrGetMemoryMap((void*) biosMemoryMap);

	/* build memory map. */
	for (currentEntry = 0; currentEntry < entryCount; currentEntry++) {

		mmap [currentEntry].size   = sizeof(MULTIBOOT_MMAP);
		mmap [currentEntry].addr   = biosMemoryMap [currentEntry].base;
		mmap [currentEntry].length = biosMemoryMap [currentEntry].length;
		mmap [currentEntry].acpiAttributes = 0;

		BlrPrintf("\n\r  start = %llx size = %llx",
			mmap [currentEntry].addr, mmap [currentEntry].length);

		mmap [currentEntry].type   = biosMemoryMap [currentEntry].type;
	}

	/* don't need this anymore. */
	BlrFree(biosMemoryMap);

	/* store it. */
	mbi->mmapAddr   = mmap;
	mbi->mmapLength = entryCount * sizeof (MULTIBOOT_MMAP);
}

PRIVATE void BlVbeQueryControlInfo(OUT void* controlBlock) {

	nblMessage m;
	nblDeviceObject* vbe;

	vbe = BlrOpenDevice("vbe");
	assert(vbe != NULL);

	m.source = 0;
	m.type = VBE_QUERY_CONTROL_INFO;
	m.VBE_QUERY_CONTROL_INFO_OUT = controlBlock;
	vbe->driverObject->request(&m);
}

PRIVATE uint16_t BlVbeQueryMode (void) {

	nblMessage m;
	nblDeviceObject* vbe;

	vbe = BlrOpenDevice("vbe");

	m.source = 0;
	m.type = VBE_QUERY_MODE;

	vbe->driverObject->request(&m);
	return (uint16_t) m.retval;
}

PRIVATE void BlVbeQueryModeInfo(OUT void* modeInfoBlock) {

	nblMessage m;
	nblDeviceObject* vbe;

	vbe = BlrOpenDevice("vbe");
	assert(vbe != NULL);

	m.source = 0;
	m.type = VBE_QUERY_MODE_INFO;
	m.VBE_QUERY_MODE_INFO_OUT = modeInfoBlock;
	vbe->driverObject->request(&m);
}

PRIVATE void BlVbeQueryProtectedModeInterface(OUT uint16_t* seg, OUT uint16_t* offset, OUT uint16_t* length) {

	nblMessage m;
	nblDeviceObject* vbe;

	vbe = BlrOpenDevice("vbe");
	assert(vbe != NULL);

	m.source = 0;
	m.type = VBE_QUERY_PMODE_INTERFACE;
	m.VBE_QUERY_PMODE_INTERFACE_SEG = seg;
	m.VBE_QUERY_PMODE_INTERFACE_OFF = offset;
	m.VBE_QUERY_PMODE_INTERFACE_LENGTH = length;
	vbe->driverObject->request(&m);
}

PUBLIC BOOL BlMbVideoParameters (IN MULTIBOOT_INFO* mbi, IN uint32_t width, IN uint32_t height, IN uint32_t bpp) {

	nblSurface desc;
	nblDeviceObject* device;
	void*    vbeModeInfoBlock;
	void*    vbeControlInfoBlock;
	uint16_t vbeMode;
	uint16_t vbeInterfaceSeg;
	uint16_t vbeInterfaceOffset;
	uint16_t vbeInterfaceLength;

	device = BlrOpenDevice("vbe");
	if (!device)
		return FALSE;

	desc.caps   = NBL_SCAPS_PRIMARYSURFACE;
	desc.flags  = NBL_SFLAGS_CAPS;
	desc.device = device;

	if (BlrSetVideoMode(&desc, width, height, bpp) != NBL_STAT_SUCCESS)
		return FALSE;

	/* Get VBE mode number. */
	vbeMode = BlVbeQueryMode();

	/* The VBE info block and control block are passed
	to the kernel, so this must be in ZONE_LOW. */
	BlrSetActiveHeap(ZONE_LOW);

	/* The VBE info block is always 256 bytes. Likewise,
	the control info block is 512 bytes. */
	vbeModeInfoBlock = BlrAlloc(256);
	vbeControlInfoBlock = BlrAlloc(512);

	/* reset to ZONE_NORMAL. */
	BlrSetActiveHeap(ZONE_NORMAL);

	if (vbeModeInfoBlock)
		BlVbeQueryModeInfo(vbeModeInfoBlock);
	if (vbeControlInfoBlock)
		BlVbeQueryControlInfo(vbeControlInfoBlock);

	BlVbeQueryProtectedModeInterface(&vbeInterfaceSeg, &vbeInterfaceOffset, &vbeInterfaceLength);

	mbi->vbeMode           = vbeMode;
	mbi->vbeModeInfo       = vbeModeInfoBlock;
	mbi->vbeControlInfo    = vbeControlInfoBlock;
	mbi->vbeInterfaceSeg   = vbeInterfaceSeg;
	mbi->vbeInterfaceOff   = vbeInterfaceOffset;
	mbi->vbeInterfaceLen   = vbeInterfaceLength;
	mbi->framebufferAddr   = (addr_t) desc.pixels;
	mbi->framebufferPitch  = desc.pitch;
	mbi->framebufferWidth  = desc.width;
	mbi->framebufferHeight = desc.height;
	mbi->framebufferBpp    = desc.format.bpp;

	if (desc.format.palette) {

		/* We need to convert our palette. */
		mbi->framebufferType = MULTIBOOT_FRAMEBUFFER_TYPE_INDEXED;
		mbi->framebufferPaletteAddr = desc.format.palette->colors;
		mbi->framebufferPaletteNumColors = desc.format.palette->count;
	}
	else {
		mbi->framebufferType = MULTIBOOT_FRAMEBUFFER_TYPE_RGB;
		mbi->framebufferRedFieldPosition   = desc.format.rshift;
		mbi->framebufferGreenFieldPosition = desc.format.gshift;
		mbi->framebufferBlueFieldPosition  = desc.format.bshift;
		mbi->framebufferRedMaskSize        = _BlMbBitCount(desc.format.rmask);
		mbi->framebufferGreenMaskSize      = _BlMbBitCount(desc.format.gmask);
		mbi->framebufferBlueMaskSize       = _BlMbBitCount(desc.format.bmask);
	}

	mbi->flags |= MULTIBOOT_INFO_FRAMEBUFFER_INFO;
	mbi->flags |= MULTIBOOT_INFO_VBE_INFO;
	return TRUE;
}

PRIVATE
void
_BlMbBootDevice(nblDeviceObject* dev) {

	uint32_t biosdev;
	uint32_t slice;
	uint32_t partition;

	/* get BIOS device number. */
	biosdev = BlrDiskBiosName (dev);
	if (biosdev == 0xffffffff)
		return; /* invalid name or wrong firmware. */

	/* need to get partition. Can get it from dev->name. */
	slice     = 0;
	partition = 0;

	bootdev = ((biosdev & 0xff) << 24) | ((slice & 0xff) << 16) | ((partition & 0xff) << 8) | 0xff;
	bootdevSet = TRUE;
}


PRIVATE
void
_BlMbAddModule(addr_t start, size_t size, int argc, char* argv[]) {

	mbModule* mod;
	size_t    length;
	char*     p;
	int       i;

	mod = BlrAlloc (sizeof (mbModule));
	if (!mod)
		return;

	mod->start = start;
	mod->size  = size;

	for (i = 0; i < argc; i++)
		length += BlrStrLength(argv[i]) + 1;

	if (length == 0)
		length = 1;

	mod->cmdline = BlrAlloc (length);
	if (!mod->cmdline) {
		BlrFree (mod->cmdline);
		return;
	}
	p = mod->cmdline;

	mod->cmdlineSize = length;
	totalModuleCmd += length;	/* might need to be aligned to dword. */

	for (i = 0; i < argc; i++) {
		p = BlrStrCopy(p, argv[i]);
		*(p++) = ' ';
	}

	/* Remove the space after the last word.  */
	if (p != mod->cmdline)
	p--;
	*p = '\0';

	if (modulesLast)
		modulesLast->next = mod;
	else{
		modules = mod;
		modulesLast->next = NULL;
	}
	modulesLast = mod;
	moduleCount++;
}

PRIVATE
uint32_t
_BlMbMemoryMapGetLower(IN MULTIBOOT_INFO* mbi) {

	MULTIBOOT_MMAP* mmap;
	index_t maxIndex;
	index_t currentIndex;
	uint32_t result;

	if (!mbi->mmapLength)
		return 0;

	mmap = mbi->mmapAddr;
	result = 0;
	maxIndex = mbi->mmapLength / sizeof (MULTIBOOT_MMAP);

	for (currentIndex = 0; currentIndex < maxIndex; currentIndex++) {
		if ( mmap [currentIndex].type != MMAP_DESCR_AVAILABLE)
			continue;
		if ( mmap [currentIndex].addr > 0x100000 )
			continue;
		/* convert bytes into KB. */
		result += (mmap [currentIndex].length / 0x1000);
	}

	mbi->memLower = result;
	return result;
}

PRIVATE
uint32_t
_BlMbMemoryMapGetUpper(IN MULTIBOOT_INFO* mbi) {

	MULTIBOOT_MMAP* mmap;
	index_t maxIndex;
	index_t currentIndex;
	uint32_t result;

	if (!mbi->mmapLength)
		return 0;

	mmap = mbi->mmapAddr;
	result = 0;
	maxIndex = mbi->mmapLength / sizeof (MULTIBOOT_MMAP);

	for (currentIndex = 0; currentIndex < maxIndex; currentIndex++) {
		if ( mmap [currentIndex].type != MMAP_DESCR_AVAILABLE)
			continue; /* we stop at first memory hole. */
		if ( mmap [currentIndex].addr < 0x100000 )
			continue;
		/* convert bytes into KB. */
		result += (mmap [currentIndex].length / 0x1000);
	}

	mbi->memUpper = result;
	return result;
}

PUBLIC
BOOL
BlMbBuildInformation(IN MULTIBOOT_INFO* mbi, IN char* commandLine) {

	if (!mbi)
		return FALSE;

	BlrZeroMemory(mbi, sizeof (MULTIBOOT_INFO));

	/* all data used by mbi must be allocated in ZONE_LOW
	since we need to pass it to kernel. */
	BlrSetActiveHeap(ZONE_LOW);

	mbi->bootloaderName = "nboot";
	mbi->flags |= MULTIBOOT_INFO_BOOT_LOADER_NAME;

	if (commandLine) {

		/* This must be allocated in ZONE_LOW since the region of memory
		it is currently at may be in ZONE_NORMAL which gets overwritten
		when the kernel gets copied there. */

		mbi->cmdLine = (char*)BlrAlloc(BlrStrLength(commandLine) + 1);
		BlrStrCopy(mbi->cmdLine, commandLine);

		mbi->flags |= MULTIBOOT_INFO_CMDLINE;
	}

	mbi->modsCount = 0;
	mbi->modsAddr  = 0;
	mbi->flags |= MULTIBOOT_INFO_MODS;

	_BlMbGetMemoryMap(mbi);
	mbi->flags |= MULTIBOOT_INFO_MEM_MAP;

	_BlMbMemoryMapGetLower(mbi);
	_BlMbMemoryMapGetUpper(mbi);
	mbi->flags |= MULTIBOOT_INFO_MEMORY;

	mbi->bootDevice = 0;
	mbi->flags |= MULTIBOOT_INFO_BOOTDEV;

	/* we can now revert back to ZONE_NORMAL. */
	BlrSetActiveHeap(ZONE_NORMAL);

	return TRUE;
}
