/************************************************************************
*
*	MultiBoot Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "elf.h"
#include "mbi.h"

/* building with chkstk.obj to make MSVC happy. */

extern 
PUBLIC
BOOL
BlMbBuildInformation(IN MULTIBOOT_INFO* mbi);

PRIVATE
STATIC
void
BlMbPrintHeader (IN MULTIBOOT_HEADER* header) {
	BlrPrintf("\n\r| MB header");
	BlrPrintf("\n\r+--------------------");
	BlrPrintf("\n\r| magic: %x", header->magic);
	BlrPrintf("\n\r| flags: %x", header->flags);
	BlrPrintf("\n\r| checksum: %x", header->checksum);
	BlrPrintf("\n\r| header address: %x", header->headerAddr);
	BlrPrintf("\n\r| load address: %x", header->loadAddr);
	BlrPrintf("\n\r| load end address: %x", header->loadEndAddr);
	BlrPrintf("\n\r| bss end address: %x", header->bssEndAddr);
	BlrPrintf("\n\r| entry point: %x", header->entryPoint);
	BlrPrintf("\n\r| mode type: %x", header->modeType);
	BlrPrintf("\n\r| width: %i", header->width);
	BlrPrintf("\n\r| height: %i", header->height);
	BlrPrintf("\n\r| depth: %i", header->depth);
	BlrPrintf("\n\r+--------------------");
}

PRIVATE
STATIC
int
BlMbFindHeader (IN nblFile* image, OUT MULTIBOOT_HEADER* out) {

	unsigned char  buffer[MULTIBOOT_SEARCH];
	MULTIBOOT_HEADER* mbHeader;
	int i;
	int length;

	/* read first MULTIBOOT_SEARCH bytes. */
	BlrFileSeek (image, 0, NBL_SEEK_SET);
	length = BlrFsRead(image, MULTIBOOT_SEARCH, buffer);

	/* search for multiboot header. */
	for (i = 0; i < length; i++) {
		if (*(unsigned int*) (buffer + i) == MULTIBOOT_HEADER_MAGIC) {

			mbHeader = (MULTIBOOT_HEADER*) (buffer + i);
			BlrCopyMemory(out,mbHeader,sizeof (MULTIBOOT_HEADER));
			return i;
		}
	}
	return 0;
}

extern
BOOL
BlMbLoadElf (IN nblFile* file, OUT void* buffer);

static size_t _codeSize;

char*    _multiboot_payload_orig;
addr_t   _multiboot_payload_dest;
size_t   _multiboot_pure_size;
addr_t   _multiboot_payload_eip;
addr_t   _multiboot_module_dest;

MULTIBOOT_INFO _mbi;

#if 0
X	uint32_t cmdLine;
X	uint32_t modsCount;
X	uint32_t modsAddr;
X	uint32_t mmapLength;
X	uint32_t mmapAddr;
	uint32_t drivesLength;
	uint32_t drivesAddr;
	uint32_t configTable;
	uint32_t apmTable;
X	uint32_t vbeControlInfo;
X	uint32_t vbeModeInfo;
X	uint16_t vbeMode;
X	uint16_t vbeInterfaceSeg;
X	uint16_t vbeInterfaceOff;
X	uint16_t vbeInterfaceLen;
#endif

MULTIBOOT_MODULE* _moduleList;
index_t           _moduleListSize;

static addr_t BlrPageAlign(addr_t memory)
{
	addr_t offset = memory % 0x1000;
	if (offset == 0)
		return memory;
	return memory += (0x1000 - offset);
}

STATIC uint32_t _video_width;
STATIC uint32_t _video_height;
STATIC uint32_t _video_bpp;
STATIC BOOL _enable_video;

BOOL BlMbSetVideoMode(IN uint32_t width, IN uint32_t height, IN uint32_t bpp) {
	_video_width = width;
	_video_height = height;
	_video_bpp = bpp;
	_enable_video = TRUE;
	return TRUE;
}

BOOL BlMbBoot (void) {

	addr_t  eip;
	addr_t  moduleMemory;
	index_t module;
	addr_t  moduleSize;

	if (_enable_video)
		BlMbVideoParameters(&_mbi, _video_width, _video_height, _video_bpp);

	eip = _multiboot_payload_eip;
	moduleMemory = _multiboot_module_dest;

	BlrPrintf("\n\r_moduleListSize = %i", _moduleListSize);
	BlrPrintf("\n\rmoduleMemory = %x", moduleMemory);

	/* copy modules. */
	if (_moduleListSize > 0) {

		for (module = 0; module < _moduleListSize; module++) {

			moduleSize = _moduleList[module].modEnd - _moduleList[module].modStart;

			moduleMemory = BlrPageAlign(moduleMemory);

			BlrPrintf("\n\rCopy: %x %x %i", moduleMemory, _moduleList[module].modStart, moduleSize);
			BlrCopyMemory(moduleMemory, _moduleList[module].modStart, moduleSize);

			_moduleList[module].modStart = moduleMemory;
			_moduleList[module].modEnd = moduleMemory + moduleSize;

			*(uint32_t*)(moduleMemory + moduleSize + 1) = NULL; /* null terminate. */

			moduleMemory += moduleSize + 10;
		}
	}

	_mbi.modsCount = _moduleListSize;
	_mbi.modsAddr  = (uint32_t)_moduleList;

	/* copy kernel. */
	BlrCopyMemory(_multiboot_payload_dest, _multiboot_payload_orig, _multiboot_pure_size);

	/* execute kernel. */
	_asm {
		mov eax, MULTIBOOT_BOOTLOADER_MAGIC
		lea ebx, dword ptr [_mbi]
		call [eip]
		cli
a:		hlt
		jmp a
	}
}

BOOL BlMbLoad (char* kernel, char* commandLine) {

	char* buffer;
	nblFile*          file;
	MULTIBOOT_HEADER* header;
	size_t            length;

	_enable_video = FALSE;

	buffer = BlrAlloc (MULTIBOOT_SEARCH);

	file = BlrFileOpen(kernel);
	if (!file)
		return FALSE;

	/* BUG!BUG! BlrFsRead returns length=0 when size is around 8k.
	Not sure why, it returns error but success with reading. */
	length = BlrFsRead(file, MULTIBOOT_SEARCH, buffer);

	/* because of above bug, we'll just look through entire buffer. */
	for (header = (MULTIBOOT_HEADER*) buffer;
		((char*) header <= buffer + MULTIBOOT_SEARCH - 12) || (header = 0);
		header = (MULTIBOOT_HEADER*) ((char*) header + 4)) {

		if (header->magic == MULTIBOOT_HEADER_MAGIC
			&& !(header->magic + header->flags + header->checksum))
			break;
	}

	if (!header)
		return FALSE;

	BlMbPrintHeader(header);

	if (header->flags & UNSUPPORTED_FLAGS)
		return FALSE;

	_multiboot_payload_orig = NULL;

	BlrFree(_multiboot_payload_orig);
	_multiboot_payload_orig = NULL;
	_multiboot_pure_size = 0;

	if (header->flags & MULTIBOOT_AOUT_KLUDGE) {

		int offset;
		int loadSize;

		offset = ((char*) header - buffer - (header->headerAddr - header->loadAddr));

		if (header->loadEndAddr == 0) {
			loadSize = file->size - offset;
		}
		else {
			loadSize = header->loadEndAddr - header->loadAddr;
		}

		if (header->bssEndAddr)
			_codeSize = (header->bssEndAddr - header->loadAddr);
		else
			_codeSize = loadSize;

		_multiboot_payload_dest = header->loadAddr;

		_multiboot_pure_size += _codeSize;

		_multiboot_payload_orig = BlrAlloc (_multiboot_pure_size + 65536);
		if (! _multiboot_payload_orig)
			return FALSE;

		if (BlrFileSeek (file, offset, NBL_SEEK_SET) == FALSE)
			return FALSE;

		/* note BUG! above. */
		BlrFsRead(file, loadSize, (void*) _multiboot_payload_orig);

		if (header->bssEndAddr)
			BlrZeroMemory (_multiboot_payload_orig + loadSize, header->bssEndAddr - header->loadAddr - loadSize);

		_multiboot_payload_eip = header->entryPoint;
	}
	else if (!BlMbLoadElf(file, buffer))
		return FALSE;

	if (!BlMbBuildInformation(&_mbi, commandLine))
		return FALSE;

	if (header->flags & MULTIBOOT_VIDEO_MODE)
		BlMbSetVideoMode(header->width, header->height, header->depth);

	/* this is set when loading modules. */
	_multiboot_module_dest = 0;
	_moduleListSize = 0;

	return TRUE;
}

/* This gets called by main boot loader program. */

BOOL BlMbInitModuleList (IN unsigned int moduleCount) {

	BlrSetActiveHeap(ZONE_LOW);

	_moduleList = BlrAlloc(sizeof (MULTIBOOT_MODULE) * moduleCount);
	if (!_moduleList)
		return FALSE;

	BlrSetActiveHeap(ZONE_NORMAL);

	_moduleListSize = moduleCount;
	return TRUE;
}

STATIC index_t _currentModule = 0;

BOOL BlMbAddModule (IN addr_t moduleStart, IN addr_t moduleEnd, IN char* commandLine) {

	if (_currentModule >= _moduleListSize)
		return FALSE;

	_moduleList[_currentModule].modStart = moduleStart;
	_moduleList[_currentModule].modEnd = moduleEnd;
	_moduleList[_currentModule].cmdLine = commandLine;
	_moduleList[_currentModule].reserved = 0;

	_currentModule++;
	return TRUE;
}

PUBLIC BOOL BlMbLoadModule(IN char* path) {

	nblFile* file;
	addr_t module;
	size_t size;

	if (!path)
		return FALSE;

	if (_currentModule >= _moduleListSize)
		return FALSE;

	/* if there is an error, we will be skipping this module. So
	_moduleListSize will be minus one. */
	file = BlrFileOpen(path);
	if (!file) {
		_moduleListSize--;
		return FALSE;
	}
	size = file->size;
	module = BlrAlloc(size);
	if (!module){
		BlrFsClose(file);
		_moduleListSize--;
		return FALSE;
	}

	BlMbAddModule(module, module + size, NULL);

	if (!BlrFsRead(file, size, module)){
		_moduleListSize--;
		BlrFree(module);
		BlrFsClose(file);
		return FALSE;
	}
	BlrFsClose(file);

	/* _multiboot_module_dest must always be higher then all modules. */
	if (module > _multiboot_module_dest)
		_multiboot_module_dest = module + size + 1;

	return TRUE;
}

PRIVATE STATIC status_t MbInit (IN nblLPB* lpb) {

	/* load everything here. */
	return NBL_STAT_SUCCESS;
}

PRIVATE STATIC status_t MbBoot (IN nblDeviceObject* dev) {

	/* exit boot services and call kernel. */
	return NBL_STAT_SUCCESS;
}

PRIVATE STATIC status_t MbEntryPoint (IN nblDriverObject* self, IN int dependencyCount, IN nblDriverObject* dependencies[]) {

	return NBL_STAT_SUCCESS;
}

PRIVATE STATIC status_t MbRequest (IN nblMessage* rpb) {
	switch (rpb->type) {
 		case NBL_LOADER_BOOT:
			return MbBoot ((nblDeviceObject*) rpb->NBL_LOADER_BOOT_DEVICE);
		case NBL_LOADER_INIT:
			return MbInit ((nblLPB*) rpb->NBL_LOADER_INIT_LPB);
		case NBL_CHAINLOADER_BOOT:
		default:
			return NBL_STAT_UNSUPPORTED;
	};
}

#define NBL_MB_VERSION 1

/* Multiboot driver object. */
NBL_DRIVER_OBJECT_DEFINE _nMbLoaderDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_MB_VERSION,
	NBL_DRV_LOAD_CORE,
	NBL_DRV_CLASS_OTHER,
	"mboot",
	MbEntryPoint,
	MbRequest,
	0
};
