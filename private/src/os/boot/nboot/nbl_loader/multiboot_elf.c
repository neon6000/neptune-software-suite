/************************************************************************
*
*	MultiBoot Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "elf.h"

#define MULTIBOOT_SEARCH 8192

PRIVATE
STATIC
BOOL
BlMbIsElf64 (void* buffer) {
	/* not supporting at this time. */
	return FALSE;
}

PRIVATE
STATIC
BOOL
BlMbLoadElf64(IN nblFile* file, OUT void* buffer) {
	/* not supporting at this time. */
	return FALSE;
}

PRIVATE
STATIC
BOOL
BlMbIsElf32 (void* buffer) {
	Elf32Ehdr* ehdr = (Elf32Ehdr*) buffer;
	return ehdr->ident [EI_CLASS] == ELFCLASS32;
}

static size_t _codeSize;
static size_t  _alloc_mbi;

char*    _multiboot_payload_orig;
addr_t   _multiboot_payload_dest;
size_t   _multiboot_pure_size;
uint32_t _multiboot_payload_eip;

PRIVATE
STATIC
BOOL
BlMbLoadElf32(IN nblFile* file, OUT void* buffer) {

	Elf32Ehdr* ehdr;
	char* phdrBase;
	int   lowestSegment;
	int   highestSegment;
	int   i;

	if (! BlMbIsElf32(buffer))
		return FALSE;

	ehdr = (Elf32Ehdr*) buffer;
	if (ehdr->ident [EI_CLASS] != ELFCLASS32)
		return FALSE;

	lowestSegment  = -1;
	highestSegment = -1;

	if (ehdr->ident[EI_MAG0] != ELFMAG0
		|| ehdr->ident[EI_MAG1] != ELFMAG1
		|| ehdr->ident[EI_MAG2] != ELFMAG2
		|| ehdr->ident[EI_MAG3] != ELFMAG3
		|| ehdr->ident[EI_DATA] != ELFDATA2LSB
		|| ehdr->version != EV_CURRENT
		|| ehdr->machine != EM_386) {

			BlrPrintf("\n\r*** invalid ELF header");
			return FALSE;
	}

	if (ehdr->type != ET_EXEC /* && ehdr->type != ET_DYN */ ) {
		BlrPrintf("\n\r*** invalid ELF type.");
		return FALSE;
	}

	if (ehdr->phoff + ehdr->phnum * ehdr->phentsize > MULTIBOOT_SEARCH) {
		BlrPrintf("\n\r*** Program Header at too high of offset.");
		return FALSE;
	}

	phdrBase = (char*) buffer + ehdr->phoff;
#define phdr(i)	((Elf32Phdr*) (phdrBase + (i) * ehdr->phentsize))

	for (i = 0; i < ehdr->phnum; i++) {
		if (phdr(i)->type == PT_LOAD && phdr(i)->filesz != 0) {

			/* Beware that segment 0 isn't necessarily loadable */
			if (lowestSegment == -1 || phdr(i)->paddr < phdr(lowestSegment)->paddr)
				lowestSegment = i;
			if (highestSegment == -1 || phdr(i)->paddr > phdr(highestSegment)->paddr)
				highestSegment = i;
		}
	}

	if (lowestSegment == -1) {
		BlrPrintf("\n\r*** No loadable segments.");
		return FALSE;
	}

	_codeSize = phdr(highestSegment)->paddr + phdr(highestSegment)->memsz - phdr(lowestSegment)->paddr;
	_multiboot_payload_dest = phdr(lowestSegment)->paddr;
	_multiboot_pure_size    += _codeSize;

	if (!_multiboot_payload_orig)
		return FALSE;

	/* load all loadable segments. */
	for (i = 0; i < ehdr->phnum; i++) {
		if (phdr(i)->type == PT_LOAD && phdr(i)->filesz != 0) {

			addr_t loadBase = _multiboot_payload_orig + (long) (phdr(i)->paddr - phdr(lowestSegment)->paddr);

			if (! BlrFileSeek(file, phdr(i)->offset, NBL_SEEK_SET)) {
				BlrPrintf("\n\r*** invalid offset in program header.");
				return FALSE;
			}

			if (! BlrFsRead(file,loadBase,phdr(i)->filesz)) {
				BlrPrintf("\n\r*** failed to read segment from file.");
				return FALSE;
			}

			if (phdr(i)->filesz < phdr(i)->memsz)
				BlrZeroMemory (loadBase + phdr(i)->filesz, phdr(i)->memsz - phdr(i)->filesz);
		}
	}

	for (i = 0; i < ehdr->phnum; i++) {
		if (phdr(i)->vaddr <= ehdr->entry && phdr(i)->vaddr + phdr(i)->memsz > ehdr->entry) {

			_multiboot_payload_eip = _multiboot_payload_dest
				+ (ehdr->entry - phdr(i)->vaddr) + (phdr(i)->paddr  - phdr(lowestSegment)->paddr);
		}
	}

	if (i == ehdr->phnum) {
		BlrPrintf("\n\r*** entry point is not in a segment.");
		return FALSE;
	}

	return TRUE;
}

BOOL
BlMbLoadElf (IN nblFile* file, OUT void* buffer) {
	if (BlMbIsElf32(buffer))
		return BlMbLoadElf32(file,buffer);
	else if (BlMbIsElf64(buffer))
		return BlMbLoadElf64(file,buffer);
	return FALSE;
}
