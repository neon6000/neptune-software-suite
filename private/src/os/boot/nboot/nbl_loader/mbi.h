/************************************************************************
*
*	MultiBoot Definitions.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef MBI_H
#define MBI_H

/* The bits in the required part of flags field we don't support.  */
#define UNSUPPORTED_FLAGS			0x0000fff8

/* Header must be in first 8k of the image and aligned on 4k boundary. */
#define MULTIBOOT_SEARCH			          8192
#define MULTIBOOT_HEADER_ALIGN			         4

/* Magic fields  */
#define MULTIBOOT_HEADER_MAGIC			0x1BADB002
#define MULTIBOOT_BOOTLOADER_MAGIC		0x2BADB002

/* Module alignment. */
#define MULTIBOOT_MOD_ALIGN			    0x00001000
#define MULTIBOOT_INFO_ALIGN			0x00000004

/* Multiboot header flags.  */
#define MULTIBOOT_PAGE_ALIGN			0x00000001
#define MULTIBOOT_MEMORY_INFO			0x00000002
#define MULTIBOOT_VIDEO_MODE			0x00000004
#define MULTIBOOT_AOUT_KLUDGE			0x00010000

/* Multiboot info flags. */
#define MULTIBOOT_INFO_MEMORY			0x00000001
#define MULTIBOOT_INFO_BOOTDEV			0x00000002
#define MULTIBOOT_INFO_CMDLINE			0x00000004
#define MULTIBOOT_INFO_MODS			    0x00000008
#define MULTIBOOT_INFO_AOUT_SYMS		0x00000010 /* << Mutually exclusive */
#define MULTIBOOT_INFO_ELF_SHDR			0X00000020 /* << */
#define MULTIBOOT_INFO_MEM_MAP			0x00000040
#define MULTIBOOT_INFO_DRIVE_INFO		0x00000080
#define MULTIBOOT_INFO_CONFIG_TABLE		0x00000100
#define MULTIBOOT_INFO_BOOT_LOADER_NAME 0x00000200
#define MULTIBOOT_INFO_APM_TABLE		0x00000400
#define MULTIBOOT_INFO_VBE_INFO		    0x00000800
#define MULTIBOOT_INFO_FRAMEBUFFER_INFO	0x00001000

/* framebuffer types. */
#define MULTIBOOT_FRAMEBUFFER_TYPE_INDEXED       0
#define MULTIBOOT_FRAMEBUFFER_TYPE_RGB           1
#define MULTIBOOT_FRAMEBUFFER_TYPE_EGA_TEXT	     2

/* memory map types. */
#define MULTIBOOT_MEMORY_AVAILABLE		         1
#define MULTIBOOT_MEMORY_RESERVED		         2
#define MULTIBOOT_MEMORY_ACPI_RECLAIMABLE        3
#define MULTIBOOT_MEMORY_NVS                     4
#define MULTIBOOT_MEMORY_BADRAM                  5

/* based on version 0.6.96 */
typedef struct _MULTIBOOT_HEADER {
   uint32_t magic;
   uint32_t flags;
   uint32_t checksum;
   uint32_t headerAddr;
   uint32_t loadAddr;
   uint32_t loadEndAddr;
   uint32_t bssEndAddr;
   uint32_t entryPoint;
   uint32_t modeType;
   uint32_t width;
   uint32_t height;
   uint32_t depth;
}MULTIBOOT_HEADER, *PMULTIBOOT_HEADER;

typedef struct _MULTIBOOT_AOUT_SYMBOL_TABLE {
	uint32_t tabsize;
	uint32_t strsize;
	uint32_t ddr;
	uint32_t reserved;
}MULTIBOOT_AOUT_SYMBOL_TABLE, *PMULTIBOOT_AOUT_SYMBOL_TABLE;

typedef struct _MULTIBOOT_ELF_SECTION_HEADER_TABLE {
	uint32_t num;
	uint32_t size;
	uint32_t addr;
	uint32_t shndx;
}MULTIBOOT_ELF_SECTION_HEADER_TABLE, *PMULTIBOOT_ELF_SECTION_HEADER_TABLE;

/* multiboot info structure */
typedef struct _MULTIBOOT_INFO {
	uint32_t flags;
	uint32_t memLower;
	uint32_t memUpper;
	uint32_t bootDevice;
	uint32_t cmdLine;
	uint32_t modsCount;
	uint32_t modsAddr;
	union {
		MULTIBOOT_AOUT_SYMBOL_TABLE sym;
		MULTIBOOT_ELF_SECTION_HEADER_TABLE sectionHeader;
	}u;
	uint32_t mmapLength;
	uint32_t mmapAddr;
	uint32_t drivesLength;
	uint32_t drivesAddr;
	uint32_t configTable;
	uint32_t bootloaderName;
	uint32_t apmTable;
	uint32_t vbeControlInfo;
	uint32_t vbeModeInfo;
	uint16_t vbeMode;
	uint16_t vbeInterfaceSeg;
	uint16_t vbeInterfaceOff;
	uint16_t vbeInterfaceLen;

	uint64_t framebufferAddr;
	uint32_t framebufferPitch;
	uint32_t framebufferWidth;
	uint32_t framebufferHeight;
	uint32_t framebufferBpp;
	uint8_t framebufferType;

	union {
		struct {
			uint32_t framebufferPaletteAddr;
			uint16_t framebufferPaletteNumColors;
		};
		struct {
			uint8_t framebufferRedFieldPosition;
			uint8_t framebufferRedMaskSize;
			uint8_t framebufferGreenFieldPosition;
			uint8_t framebufferGreenMaskSize;
			uint8_t framebufferBlueFieldPosition;
			uint8_t framebufferBlueMaskSize;
		};
	};
}MULTIBOOT_INFO, *PMULTIBOOT_INFO;

typedef struct MULTIBOOT_BOOT_DEVICE {
	uint8_t biosDrive;
	uint8_t part1;
/* although we can support subpartitions, this isn't being
tested at this time so these fields should always be 0xff. */
	uint8_t part2;
	uint8_t part3;
}MULTIBOOT_BOOT_DEVICE, *PMULTIBOOT_BOOT_DEVICE;

typedef struct MULTIBOOT_MODULE {
	uint32_t modStart;
	uint32_t modEnd;
	uint32_t cmdLine;
	uint32_t reserved;
}MULTIBOOT_MODULE, *PMULTIBOOT_MODULE;

/*
The following is probably easier to think of as:

typedef struct MULTIBOOT_MMAP {
	uint32_t sizeOfDescr;
	MMAP_DESCRIPTOR descr;
}MULTIBOOT_MMAP, *PMULTIBOOT_MMAP;
*/

typedef struct MULTIBOOT_MMAP {
	/* since acpiAttributes is optional, this stores
	size of following fields in bytes. */
	uint32_t size;
	/* the following fields are same as BIOS memory map. */
	uint64_t addr;
	uint64_t length;
	uint32_t type;
	/* the following field is part of BIOS memory map, but
	not defined by multiboot. */
	OPTIONAL uint32_t acpiAttributes;
}MULTIBOOT_MMAP, *PMULTIBOOT_MMAP;

/* access mode used by NBOOT */
#define MULTIBOOT_DRIVE_MODE_CHS 0
#define MULTIBOOT_DRIVE_MODE_LBA 1

typedef struct MULTIBOOT_DRIVE {
	uint32_t size;
	uint8_t number;
	uint8_t mode;
	uint16_t cylinders;
	uint8_t heads;
	uint8_t sectors;
	/* These ports are those used by the BIOS code
	which is firmware specific. Not possible for us
	to use this field. */
	uint8_t ports[];
}MULTIBOOT_DRIVE, *PMULTIBOOT_DRIVE;

typedef struct MULTIBOOT_APM_TABLE {
	uint16_t version;
	uint16_t cseg;
	uint32_t offset;
	uint16_t cseg16;
	uint16_t dseg;
	uint16_t flags;
	uint16_t csegLen;
	uint16_t cseg16Len;
	uint16_t dsegLen;
}MULTIBOOT_APM_TABLE, *PMULTIBOOT_APM_TABLE;

#endif
