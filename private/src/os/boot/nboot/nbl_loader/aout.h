/************************************************************************
*
*	a.out ("Assembler Output") File Format
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef AOUT_H
#define AOUT_H

struct AOut32Header {
  uint32_t midmag;  /* htonl(flags<<26 | mid<<16 | magic) */
  uint32_t text;    /* text segment size */
  uint32_t data;    /* initialized data size */
  uint32_t bss;     /* uninitialized data size */
  uint32_t syms;    /* symbol table size */
  uint32_t entry;   /* entry point */
  uint32_t trsize;  /* text relocation size */
  uint32_t drsize;  /* data relocation size */
};

struct AOut64Header {
  uint32_t midmag;	/* htonl(flags<<26 | mid<<16 | magic) */
  uint64_t text;    /* text segment size */
  uint64_t data;    /* initialized data size */
  uint64_t bss;     /* uninitialized data size */
  uint64_t syms;    /* symbol table size */
  uint64_t entry;   /* entry point */
  uint64_t trsize;  /* text relocation size */
  uint64_t drsize;  /* data relocation size */
};

union AOutHeader {
  struct AOut32Header aout32;
  struct AOut64Header aout64;
};

#define AOUT_TYPE_NONE		0
#define AOUT_TYPE_AOUT32	1
#define AOUT_TYPE_AOUT64	6

#define	AOUT32_OMAGIC		0x107	/* 0407 old impure format */
#define	AOUT32_NMAGIC		0x108	/* 0410 read-only text */
#define	AOUT32_ZMAGIC		0x10b	/* 0413 demand load format */
#define AOUT32_QMAGIC		0xcc	/* 0314 "compact" demand load format */

#define AOUT64_OMAGIC		0x1001
#define AOUT64_ZMAGIC		0x1002
#define AOUT64_NMAGIC		0x1003

#define	AOUT_MID_ZERO		0	    /* unknown - implementation dependent */
#define	AOUT_MID_SUN010		1	    /* sun 68010/68020 binary */
#define	AOUT_MID_SUN020		2	    /* sun 68020-only binary */
#define AOUT_MID_I386		134	    /* i386 BSD binary */
#define AOUT_MID_SPARC		138	    /* sparc */
#define	AOUT_MID_HP200		200	    /* hp200 (68010) BSD binary */
#define	AOUT_MID_SUN        0x103
#define	AOUT_MID_HP300		300	    /* hp300 (68020+68881) BSD binary */
#define	AOUT_MID_HPUX		0x20C	/* hp200/300 HP-UX binary */
#define	AOUT_MID_HPUX800	0x20B	/* hp800 HP-UX binary */

#define AOUT_FLAG_PIC		0x10	/* contains position independent code */
#define AOUT_FLAG_DYNAMIC	0x20	/* contains run-time link-edit info */
#define AOUT_FLAG_DPMASK	0x30	/* mask for the above */

#define AOUT_GETMAGIC(header) ((header).midmag & 0xffff)
#define AOUT_GETMID(header)   ((header).midmag >> 16) & 0x03ff)
#define AOUT_GETFLAG(header)  ((header).midmag >> 26) & 0x3f)

#endif
