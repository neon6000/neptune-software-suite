/************************************************************************
*
*	a.out ("Assembler Output") File Format
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include "aout.h"

/* loads a.out image into memory. */
PRIVATE
STATIC
BOOL
BlLoadAOUT (IN nblFile* file,
			IN uint32_t offset,
			IN addr_t   loadAddress,
			IN size_t   loadSize,
			IN addr_t   bssEndAddress) {

	if (! BlrFileSeek(file,offset, NBL_SEEK_SET))
		return FALSE;

	if (!loadSize)
		loadSize = file->size - offset;

	BlrAllocatePages (loadAddress, bssEndAddress - loadAddress - loadSize);

	BlrFsRead (file, loadSize, loadAddress);

	if (bssEndAddress)
		BlrZeroMemory ( loadAddress + loadSize, bssEndAddress - loadAddress - loadSize);

	return TRUE;
}
