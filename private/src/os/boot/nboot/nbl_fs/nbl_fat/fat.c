/************************************************************************
*
*	fat.c - NBL FAT driver.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>

#pragma pack(push, 1)

/* directory entry. */
typedef struct DirectoryEntry {
	uint8_t shortName [8];
	uint8_t shortExt [3];
	uint8_t attributes;
	uint8_t security;
	uint8_t creationTimeMs;
	uint16_t creationTime;
	uint16_t creationDate;
	uint16_t lastModified;
	uint16_t startClusterHi;
	uint16_t lastModifiedTime;
	uint16_t lastModifiedDate;
	uint16_t startClusterLo;
	uint32_t size;
}DirectoryEntry;

/* bpb version 3.31 */
typedef struct BiosParameterBlock331 {
	uint16_t bytesPerSector;
	uint8_t  sectorsPerCluster;
	uint16_t reservedSectorCount;
	uint8_t  fatCount;
	uint16_t rootDirectoryEntries;
	uint16_t totalSectors16;
	uint8_t  media;	/* must be in FAT[0]. */
	uint16_t fatSize16;
	uint16_t sectorsPerTrack;
	uint16_t headCount;
	uint32_t hiddenSectors;
	uint32_t totalSectors32;
}BiosParameterBlock331;

/* extended bpb */

/* FAT12, FAT16, HPFS */
typedef struct BiosParameterBlock4 {
	uint8_t  driveNumber;
	uint8_t  flags;
	uint8_t  extendedBootSiganture;
	uint32_t volumeSerialNumber;
	uint8_t  volumeLabel [11];
	uint8_t  fileSystemType [8];
}BiosParameterBlock4;

/* FAT32 */
typedef struct BiosParameterBlock71 {
	uint32_t  sectorsPerFat;
	uint16_t mirroringFlags;
	uint16_t version;
	uint32_t rootDirectoryCluster;
	uint16_t fsInfoSector;
	uint16_t backupSectors;
	uint8_t bootFileName [12];
	uint8_t driveNumber;
	uint8_t flags;
	uint8_t extendedBootSignature;
	uint32_t volumeSerialNumber;
	uint8_t volumeLabel [11];
	uint8_t fileSystemType [8];
}BiosParameterBlock71;

/* bpb. */
typedef struct BiosParameterBlock {
	uint8_t jmp[3];
	uint8_t oem[8];
	BiosParameterBlock331 fat12;
	union {
		BiosParameterBlock4  fat16;
		BiosParameterBlock71 fat32;
	}v; /* extended bpb. */
}BiosParameterBlock;

/* Fat32 FSInfo sector. */
typedef struct _fsInfoSector {
	uint8_t  leadSiganture[4];
	uint8_t  reserved[480];
	uint32_t signature;
	uint32_t freeCount;
	uint32_t nextFree;		/* next free cluster */
	uint8_t  reserved2[12];
	uint32_t trailSignature;
}fsInfoSector;

/* long file name directory entry. */
typedef struct _fsLongFileName {
	uint8_t order;
	wchar_t name1[5];
	uint8_t attribute;
	uint8_t longEntryType;
	uint8_t checksum;
	wchar_t name2[6];
	uint16_t reserved;
	wchar_t name3[2];
}fsLongFileName;

#pragma pack(pop)

typedef enum _nblFatType {
	FAT12,FAT16,FAT32, EXFAT
}nblFatType;

typedef struct _nblFatEx {
	/* Volume. */
	uint64_t   totalSectors;
	uint64_t   totalClusters;
	uint8_t    sectorsPerCluster;
	uint16_t   bytesPerSector;
	uint32_t   uuid;
	/* FAT table. */
	nblFatType fatType;
	uint64_t   fatSize;
	uint64_t   fatSectorStart;
	uint32_t   clusterEofMark;
	/* Root directory. */
	uint32_t   rootDirectorySize;
	uint64_t   rootDirectoryCluster;
	/* Data region. */
	uint64_t   dataSectorStart;
	uint64_t   dataSectorSize;
	/* File information. */
	uint32_t   fileStartCluster;
	uint32_t   fileCurrentCluster;
	uint32_t   fileCurrentClusterNum;
	uint32_t   fileSize;
	uint8_t    fileAttributes;
}nblFatEx;

/* directory read callback. */
typedef BOOL (*DIRECTORY_CALLBACK) (IN char* fname,
									IN OPTIONAL wchar_t* lfn,
									IN char* ext);

/* Fat32 FSInfo signatures. */
#define FAT_FSINFO_LEADSIG   0x41615252
#define FAT_FSINFO_TRAILSIG  0xAA550000
#define FAT_FSINFO_SIGNATURE 0x61417272

/* media descriptor fields. This field is not standard
with many different formats. Following are most common. */
#define BPB_MEDIA_35INCH_DOUBLE_80TRACKS                 0xf0
#define BPB_MEDIA_SINGLE                                 0xf8
#define BPB_MEDIA_HARD_DISK                              0xff

/* FAT entry codes. */

/* free clusters. */
#define FAT12_CLUSTER_FREE     0
#define FAT16_CLUSTER_FREE     0
#define FAT32_CLUSTER_FREE     0

/* reserved, treat as end of file. */
#define FAT12_CLUSTER_INTERNAL 1
#define FAT16_CLUSTER_INTERNAL 1
#define FAT32_CLUSTER_INTERNAL 1

/* reserved, treat as end of file. */
#define FAT12_CLUSTER_RESERVED 0xff6
#define FAT16_CLUSTER_RESERVED 0xfff6
#define FAT32_CLUSTER_RESERVED 0x0fffffff6

/* marked as bad cluster during formatting. */
#define FAT12_CLUSTER_BADSECTOR 0xff7
#define FAT16_CLUSTER_BADSECTOR 0xfff7
#define FAT32_CLUSTER_BADSECTOR 0x0ffffff7

/* end of allocation chain. */
#define FAT12_CLUSTER_EOC       0xff8
#define FAT16_CLUSTER_EOC       0xfff8
#define FAT32_CLUSTER_EOC       0x0ffffff8

/* all values EOC > x > 0xffffffff are end of chain values. */
#define FAT12_END_OF_CHAIN(x) (x>FAT12_CLUSTER_EOC)
#define FAT16_END_OF_CHAIN(x) (x>FAT16_CLUSTER_EOC)
#define FAT32_END_OF_CHAIN(x) (x>FAT32_CLUSTER_EOC)

/* directory/file attributes. */
#define FAT_ATTR_READONLY     1
#define FAT_ATTR_HIDDEN       2
#define FAT_ATTR_SYSTEM       4
#define FAT_ATTR_VOLUMEID     8
#define FAT_ATTR_DIRECTORY 0x10
#define FAT_ATTR_ARCHIVE   0x20

/* long name entries. */
#define FAT_ATTR_LONGNAME (FAT_ATTR_READONLY|FAT_ATTR_HIDDEN|FAT_ATTR_SYSTEM|FAT_ATTR_VOLUMEID)

/* valid bits. */
#define FAT_ATTR_VALID (FAT_ATTR_READONLY|FAT_ATTR_HIDDEN|FAT_ATTR_SYSTEM|FAT_ATTR_VOLUMEID|FAT_ATTR_DIRECTORY|FAT_ATTR_ARCHIVE)

/* self reference. Using separate pointer causes BIOS build
to stop working. Need to look into it later but its not
big concern since its not needed anyways. */

//nblDriverObject* _self = NULL;
NBL_DRIVER_OBJECT_DEFINE _nfatDriverObject;

/* test if character is allowed in dos shortname. */
BOOL
BlFatIsValidDosChar(char c) {
	switch(c) {
		case '\"':
		case '/':
		case '\\':
		case '[':
		case ']':
		case ':':
		case ';':
		case '=':
		case ',':
		case ' ':
		case 0:
			return FALSE;
		default:
			return TRUE;
	};
}

/* converts longname to dos shortname. Shortnames are 11 bytes. */
void
BlFatToDosName (IN char* longname,
				OUT char* shortname) {

	char result [8+1];
	char extension [3+1];
	char* name;
	char* ext;
	int length;
	int c;
	int i;

	length = BlrStrLength (longname);

	name = BlrAlloc (length+1);
	BlrZeroMemory(name,length+1);

	/* strip invalid characters. */
	for (c = 0, i=0; c<length; c++) {
		if (BlFatIsValidDosChar(longname[c]))
			name[i++] = longname[c];
	}

	BlrFillMemory(extension, ' ', 3);
	BlrFillMemory(result, ' ', 8);

	/* find last period in name. */
	ext = BlrStrGetLast (name, '.');
	if (ext) {

		/* if valid charaters follow, this is the extension.
		Else, we use the next to last period. */

		if (!BlFatIsValidDosChar(*(ext+1))) {
			*ext = NULL; /* overwrite '.' with null. */
			ext = BlrStrGetLast (name, '.'); /* try again. */
		}

		/* skip over '.' and examine the extension. */
		ext++;
		for (c = 0; c < 3; c++) {
			char k = *(ext+c);
			if (!BlFatIsValidDosChar(k))
				break;
			extension[c] = k;
		}
	}
	ext--; /* make ext point to '.' */

	/* strip invalid characters for name. */
	for (c = 0, i = 0; c < length && i < 8; c++) {
		if (&name[c] == ext)
			break; /* we reached extension. */
		if (!BlFatIsValidDosChar(name[c]))
			continue;
		result[i++] = name[c];
	}

	/* translate to uppercase. */
	for (c = 0; c < 3; c++)
		if (extension[c] >= 'a' && extension[c] <= 'z')
			extension[c] -= 32;
	for (c = 0; c < 11; c++)
		if (result[c] >= 'a' && result[c] <= 'z')
			result[c] -= 32;

	/* and return it. */
	i = 0;
	for (c = 0; c < 8; c++)
		shortname[i++] = result[c];
	for (c = 0; c < 3; c++)
		shortname[i++] = extension[c];
	shortname[i] = NULL;

	BlrFree (name);
}


/* convert multibyte char string to wide char string. */
STATIC void mbtows (char* mb, wchar_t* wc, size_t c) {
	unsigned int s = BlrStrLength(mb);
	unsigned int i;
	for (i=0;i<s;i++) {
		BlrMbtowc (wc,mb,c);
		wc++;
		mb++;
	}
	*wc=0;
}

/* attempt to attach to device. */
STATIC status_t BlFatAttach (IN nblDeviceObject* dev) {

	assert(dev != 0);

	/* FIXME: assume we can read this superblock...for now. */

//	dev->attached = _self;
	dev->attached = &_nfatDriverObject;

	return  NBL_STAT_SUCCESS;
}

/* mount volume. */
STATIC BOOL BlFatMount (IN nblDeviceObject* volume, IN nblFatEx* ex) {

	BiosParameterBlock* bpb;
	uint8_t bootsector [512];

	/* BlrDiskRead bootsector is all null. */

	/* read bpb. */
	BlrDiskRead (volume, 0, 0, 512, bootsector);
	bpb = (BiosParameterBlock*) bootsector;

	/* prepare file ex block. */
	BlrZeroMemory (ex, sizeof(nblFatEx));

	/* compute total sectors. */
	if (bpb->fat12.totalSectors16 == 0)
		ex->totalSectors = bpb->fat12.totalSectors32;
	else
		ex->totalSectors = bpb->fat12.totalSectors16;

	/* compute fat size. */
	if (bpb->fat12.fatSize16 == 0)
		ex->fatSize = bpb->v.fat32.sectorsPerFat;
	else
		ex->fatSize = bpb->fat12.fatSize16;

	/* watch for divide by 0. Means image is corrupt or invalid. */
	if (bpb->fat12.bytesPerSector == 0)
		return FALSE;

	/* compute root directory size. */
	ex->rootDirectorySize = ((bpb->fat12.rootDirectoryEntries * sizeof(DirectoryEntry))
		+ (bpb->fat12.bytesPerSector - 1)) / bpb->fat12.bytesPerSector;

	/* compute data sector region start. */
	ex->dataSectorStart = bpb->fat12.reservedSectorCount
		+ (bpb->fat12.fatCount * ex->fatSize) + ex->rootDirectorySize;

	/* compute fat sector start...for first fat. */
	ex->fatSectorStart = bpb->fat12.reservedSectorCount;

	/* compute number of sectors in data region. */
	ex->dataSectorSize = ex->totalSectors - (bpb->fat12.reservedSectorCount
		+ (bpb->fat12.fatCount * ex->fatSize) + ex->rootDirectorySize);

	/* watch for divide by 0. Means image is corrupt or invalid. */
	if (bpb->fat12.sectorsPerCluster == 0)
		return FALSE;

	/* compute total clusters. */
	ex->totalClusters = ex->dataSectorSize / bpb->fat12.sectorsPerCluster;

	/* determine fat type. */
	if (ex->totalClusters < 4085) {
		ex->fatType = FAT12;
		ex->clusterEofMark = FAT12_CLUSTER_EOC;
	}
	else if (ex->totalClusters < 65525) {
		ex->fatType = FAT16;
		ex->clusterEofMark = FAT16_CLUSTER_EOC;
	}
	else if (ex->totalClusters < 268435445) {
		ex->fatType = FAT32;
		ex->clusterEofMark = FAT32_CLUSTER_EOC;
	}
	else {
		ex->fatType = EXFAT;
		ex->clusterEofMark = FAT32_CLUSTER_EOC;	// FIXME: is this right?
	}

	/* compute root direcory starting cluster. */
	if (ex->fatType == FAT32)
		ex->rootDirectoryCluster = bpb->v.fat32.rootDirectoryCluster;
	else
		ex->rootDirectoryCluster = ex->dataSectorStart - ex->rootDirectorySize;

	/* spc and bps. */
	ex->sectorsPerCluster = bpb->fat12.sectorsPerCluster;
	ex->bytesPerSector = bpb->fat12.bytesPerSector;

	/* open root directory. */
	ex->fileAttributes = FAT_ATTR_DIRECTORY;
	ex->fileStartCluster = (uint32_t) ex->rootDirectoryCluster;
	ex->fileCurrentClusterNum = 0;
	ex->fileSize = 0; /* no directory uses this field. */
	ex->fileCurrentCluster = ex->fileStartCluster;

	/* return success. */
	return TRUE;
}

/* compute sector from cluster. */
INLINE uint64_t BlFatComputeSector (IN uint64_t cluster, IN nblFatEx* ex) {

	return ((cluster - 2) * ex->sectorsPerCluster) + ex->dataSectorStart;
}

/* given current cluster in file, get next cluster in its cluster-chain. */
STATIC uint32_t BlFatGetNextCluster (IN nblDeviceObject* volume,IN nblFatEx* ex) {

	uint64_t fatOffset;
	uint32_t fatSector;
	uint32_t fatEntry;
	uint32_t cluster;

	switch(ex->fatType) {
		case FAT12: fatOffset = ex->fileCurrentCluster + (ex->fileCurrentCluster / 2); break;
		case FAT16: fatOffset = ex->fileCurrentCluster * 2; break;
		case FAT32: fatOffset = ex->fileCurrentCluster * 4; break;
		case EXFAT: fatOffset = ex->fileCurrentCluster * 4; break;
		default:    return 0; /* invalid filesystem. */
	};

	// fatSectorStart = 1

	fatSector = (uint32_t) (ex->fatSectorStart + (fatOffset / ex->bytesPerSector));
	fatEntry = fatOffset % ex->bytesPerSector;

	if (ex->fatType == FAT12) { // && fatEntry == ex->bytesPerSector - 1) {

		/* FAT12 cluster spans a sector boundary. This requires
		reading two sectors. */

		uint32_t clusterNew;
//		uint8_t clusterLo;
//		uint32_t clusterHi;
		uint16_t value;

//		BlrDiskRead (volume, fatSector,fatEntry, 1, &clusterLo);
//		BlrDiskRead (volume, fatSector+1, 0, sizeof(uint32_t), &clusterHi);
//		clusterHi &= 0xff;
//		clusterNew = (clusterHi << 8) | clusterLo;

		BlrDiskRead(volume, fatSector, fatEntry, 2, &value);

//		cluster = clusterNew;

		cluster = value;

	}else{

		/* All other cases, just read the cluster in. */
		BlrDiskRead (volume, fatSector,fatEntry, sizeof(uint32_t), &cluster);
	}

	switch(ex->fatType) {
		case FAT12:
			cluster &= 0xffff;
			if (ex->fileCurrentCluster & 0x0001)
				cluster >>= 4;
			else
				cluster &= 0x0fff;
			return cluster;
		case FAT16:
			return cluster & 0xffff;
		case FAT32:
		case EXFAT:
			return cluster & 0x0fffffff;
	};
	return 0; /* should never get hit. */
}

/* read file data into memory. "offset" is byte offset into
file to start reading from. "length" is number of bytes to read. */
STATIC uint32_t BlFatReadData (IN nblDeviceObject* volume, IN nblFatEx* ex,
							  unsigned offset, unsigned length, void* buf) {

	uint32_t cluster;
	uint32_t result;

	if (volume->attached != &_nfatDriverObject)
		return 0;

	/* calculate cluster and adjust offset relative to it. */
	cluster = offset / (ex->bytesPerSector * ex->sectorsPerCluster);
	offset %= (ex->bytesPerSector * ex->sectorsPerCluster);
	result = 0;

	/* if requested cluster is less then current cluster, we
	have to follow the cluster chain again from the beginning. */
	if (cluster < ex->fileCurrentClusterNum) {

		ex->fileCurrentClusterNum = 0;
		ex->fileCurrentCluster = ex->fileStartCluster;
	}

	while (length > 0) {

		uint32_t sector;
		uint32_t size;

		/* follow cluster chain until we get to
		the cluster we want. */
		while (cluster > ex->fileCurrentClusterNum) {

			uint32_t nextCluster = 0;
			
			nextCluster = BlFatGetNextCluster (volume, ex);

//			BlrPrintf("\n\r** cluster: %u next: %u", ex->fileCurrentClusterNum, nextCluster);

			if (nextCluster >= ex->clusterEofMark) {
//				BlrPrintf("\n\r*** End Of Cluster, result = %x", result);
				return 0;
			}

			ex->fileCurrentCluster = nextCluster;
			ex->fileCurrentClusterNum++;
		}

		sector = (uint32_t) BlFatComputeSector (ex->fileCurrentCluster, ex);

//		if (sector >= 713)
//			BlrPrintf("\n\r sector: %u", sector);

		size = (ex->bytesPerSector * ex->sectorsPerCluster) - offset;
		if (size > length)
			size = length;

		BlrDiskRead (volume, sector, offset, size, buf);

		offset = 0;
		length -= size;
		(uint8_t*)buf += size;
		cluster++;
		result += size;
	}

	return result;
}

/* directory entry attributes. */
#define FAT_DIRECTORY_FREE  0xe5
#define FAT_DIRECTORY_LAST  0
#define FAT_DIRECTORY_KANJI 5

/* 6th bit indicates this is the last lfn entry. */
#define FAT_DIRECTORY_LASTLONG 0x40

/* iterate through a directory file. ex points to directory file
to operate on, call callback for each entry. "ext" is any input data
to pass to callback. */
STATIC BOOL BlFatDirectoryIter (IN nblDeviceObject* volume, IN nblFatEx* ex,
								IN OPTIONAL void* ext, IN OPTIONAL DIRECTORY_CALLBACK callback,
								OUT DirectoryEntry* out) {

/* only storing 5 directory enteries used by long filenames. */
#define BL_FAT_LONGNAME_MAX 5

	DirectoryEntry e;
	uint32_t off;
	uint64_t sector;

	/* long filename. */
	wchar_t longname[ 13 * BL_FAT_LONGNAME_MAX ];

	/* short filename. */
	char shortname[12];

	/* null terminate names. */
	BlrZeroMemory(shortname,12);
	BlrZeroMemory(longname, 13 * BL_FAT_LONGNAME_MAX);

	/* only read directories. */
	if (!(ex->fileAttributes & FAT_ATTR_DIRECTORY))
		return 0;

	/* starting at beginning of directory. */
	off = 0;

	/* fat12/fat16 root directory is just a sequence
	of sectors and starts at an absolute location. */
	if (ex->fatType == FAT12 || ex->fatType == FAT16) {
		if (ex->fileStartCluster == ex->rootDirectoryCluster) {
			sector = ex->fileStartCluster;
		}
	}

	/* start reading directory. */
	while (1) {

		/* special case for root directory. */
		if (ex->fileStartCluster == ex->rootDirectoryCluster) {

			if (ex->fatType == FAT12 || ex->fatType == FAT16) {

				/* fat12 and fat16 root directory is an array of directory entries. */
				BlrDiskRead (volume, (size_t) sector, off, sizeof(DirectoryEntry), &e);
			}
			else {

				/* fat32 root directory we need to follow cluster chain. */
				if (! BlFatReadData (volume, ex, off, sizeof(DirectoryEntry), &e))
					break;
			}
		}
		else
		{
			/* all other cases, just read it as a file. */
			if (! BlFatReadData (volume, ex, off, sizeof(DirectoryEntry), &e))
				break;
		}

		/* translate first character. */
		switch(e.shortName[0]) {
			case FAT_DIRECTORY_FREE:
				/* skip over. */
				off += sizeof(DirectoryEntry);
				continue;
			case FAT_DIRECTORY_LAST:
				goto done;	/* no more entries. */
			case FAT_DIRECTORY_KANJI:
				shortname[0] = FAT_DIRECTORY_FREE; /* actual character code. */
			default:
				shortname[0] = e.shortName[0];
		}

		BlrCopyMemory (&shortname[1], &e.shortName[1], 7);
		BlrCopyMemory (&shortname[8], e.shortExt, 3);

		/* long file name entry. */
		if (e.attributes == FAT_ATTR_LONGNAME) {

			wchar_t offset;
			fsLongFileName* lfn;
			int ordinal;

			lfn = (fsLongFileName*) &e;
			ordinal = lfn->order;

			/* if last entry, clear the bit and read it in. */
			if (ordinal & FAT_DIRECTORY_LASTLONG)
				  ordinal &= 0x3f;

			ordinal--;

			offset = 13 * ordinal;	/* 13 characters in lfn. */

			BlrCopyMemory (&longname [offset], lfn->name1,      5 * 2);
			BlrCopyMemory (&longname [offset + 5], lfn->name2,  6 * 2);
			BlrCopyMemory (&longname [offset + 11], lfn->name3, 2 * 2);

			/* when done, the very next entry is the shortname entry
			that corrosponds to the lfn. */
			if (ordinal==0) {

				/* go to next entry. */
				off += sizeof(DirectoryEntry);
				continue;
			}
		}

		/* go to next entry. */
		off += sizeof(DirectoryEntry);

		/* make the call. */
		if (callback) {
			if ( callback (shortname, longname, ext)) {
				BlrCopyMemory(out, &e, sizeof(DirectoryEntry));
				return TRUE;
			}
		}

		/* prepare to loop again. */
		shortname[0]=0;
		longname[0]=0;
	}
done:
	return FALSE;
}

/* callback returns FALSE to continue search, TRUE to end search. */
BOOL BlFatDirectoryCallback(IN char* shortname, IN wchar_t* longname, IN char* path) {

	wchar_t* s;
	size_t   length;
	size_t   length2;
	char     name[11 + 1];

	if( *longname == 0)
		goto short_name;

	length = BlrStrLength(path);
	length2 = BlrWideStrLength(longname);
	if (length != length2)
		return FALSE; /* should we goto short_name ? */

	s = BlrAlloc(length+1);
	mbtows (path,s,1);

	if (BlrWideStrCompare (longname, s) == 0) {
		BlrFree(s);
		return TRUE;
	}
	BlrFree(s);

	/* if long name did not match, fall through. */
short_name:

	/* convert path to shortname. */
	BlFatToDosName (path, name);

	/* and compare. */
	if (BlrStrCompare( shortname, /* path */ name ) == 0)
		return TRUE;

	return FALSE;
}

/* locate directory entry for path. */
STATIC BOOL _BlFatFind (IN nblDeviceObject* volume, IN nblFatEx* ex, IN char* path) {

	DirectoryEntry e;

	/* look in directory. */
	if (BlFatDirectoryIter (volume, ex, path, BlFatDirectoryCallback, &e)) {

		/* found file. */
		ex->fileSize = e.size;
		ex->fileStartCluster = e.startClusterLo | (e.startClusterHi << 16);
		ex->fileAttributes = e.attributes;
		ex->fileCurrentClusterNum = 0;
		ex->fileCurrentCluster = ex->fileStartCluster;
		return TRUE;
	}
	return FALSE;
}

/* locate directory entry for path. */
STATIC BOOL BlFatFind (IN nblDeviceObject* volume, IN nblFatEx* ex, IN char* path) {

	const char* delim = "/";
	char* p;
	char* s;

	s = BlrZeroAlloc (BlrStrLength (path)+1);
	BlrCopyMemory(s,path,BlrStrLength(path));

	p = BlrStrToken(s, delim);
	while (p) {
		if (! _BlFatFind (volume, ex, p)) {
			BlrFree(s);
			return FALSE;
		}
		p = BlrStrToken(NULL, delim);
	}
	BlrFree(s);
	return TRUE;
}

/* open file on device. Device->attached must point to _self, else
we don't own the device. */
STATIC nblFile* BlFatOpen (IN nblDeviceObject* volume, IN char* fname) {

	nblFile* file;
	nblFatEx* ex;

	/* allocate file object. */
	file = BlrAlloc (sizeof(nblFile));
	file->ext = BlrAlloc (sizeof(nblFatEx));
	ex = (nblFatEx*) file->ext;

	/* prepare to read. */
	if (!BlFatMount (volume, file->ext)) {
		BlrPrintf("\n\rBlFatMount fail");
		return NULL;
	}

	/* locate file. */
	if (! BlFatFind (volume, ex, fname)) {
		BlrPrintf("\n\rBlFatFind fail");
		BlrFree (file->ext);
		BlrFree (file);
		return NULL;
	}

	/* create file object. */
	file->device        = volume;
	file->driver        = &_nfatDriverObject;
	file->ext           = (void*) ex;
	file->locked        = FALSE;
	file->name          = fname;
	file->locked        = FALSE;
	file->size          = ex->fileSize;
	file->currentOffset = 0;	
	file->flags         = 0;
	file->type          = 0;
	file->writeAccess   = 0;
	file->readAccess    = 0;
	file->deleteAccess  = 0;

	/* return file pointer. */
	return file;
}

/* close file. file->ext->points to bpb on device file was opened on. */
STATIC status_t BlFatClose (IN nblFile* file) {

	nblFatEx* ex;

	if (file->driver != &_nfatDriverObject)
		return NBL_STAT_UNSUPPORTED; /* file not ours. */

	ex = (nblFatEx*) file->ext;

	BlrFree (ex);
	BlrFree (file);

	return NBL_STAT_SUCCESS;
}

/* read from file. file->ext->points to bpb on device file was opened on.
file->dev points to device of file to read from. */
STATIC size_t BlFatRead (IN nblFile* file, IN size_t size, IN void* buffer) {

	nblFatEx* ex;
	uint32_t result;

//	BlrPrintf("\n\rBlFatRead size = %i", size);

	if (!size || !buffer)
		return 0;
	if (!file)
		return 0;

	if (file->driver != &_nfatDriverObject)
		return NBL_STAT_UNSUPPORTED; /* file not ours. */

	ex = (nblFatEx*) file->ext;
	result = (uint32_t) BlFatReadData (file->device, ex, (uint32_t) file->currentOffset, size, buffer);
	file->currentOffset += result;

//	BlrPrintf("\n\rReturns: %i", result);
	return result;
}

status_t FatEntryPoint (IN nblDriverObject* self, IN int dependencyCount,
						IN nblDriverObject* dependencies[]) {

	/* save self reference and return success. */
//	_self = self;
	return NBL_STAT_SUCCESS;
}

/* request callback. */
status_t FatRequest (IN nblMessage* rpb) {
	rettype_t r;
	rpb->retval = 0;
	switch (rpb->type) {
		case NBL_FS_ATTACH:r = (rettype_t) BlFatAttach(rpb->NBL_FS_ATTACH_DEVICE); break;
		case NBL_FS_OPEN:  r = (rettype_t) BlFatOpen  (rpb->NBL_FS_OPEN_DEVICE, (char*) rpb->NBL_FS_OPEN_PATH); break;
		case NBL_FS_CLOSE: r = (rettype_t) BlFatClose ((nblFile*) rpb->NBL_FS_CLOSE_FILE); break;
		case NBL_FS_READ:  r = (rettype_t) BlFatRead  ((nblFile*) rpb->NBL_FS_READ_FILE,rpb->NBL_FS_READ_SIZE, (void*) rpb->NBL_FS_READ_BUFFER); break;
		case NBL_FS_WRITE: 	/* unsupported at this time. */		
		default: return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_FAT_VERSION 1

NBL_DRIVER_OBJECT_DEFINE _nfatDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_FAT_VERSION,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_FS,
	"fat",
	FatEntryPoint,
	FatRequest,
	0
};
