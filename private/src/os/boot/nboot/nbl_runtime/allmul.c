

/**
 * long long __allmul(long long Multiplier, long long Multiplicand);
 *
 * Parameters:
 *   [ESP+04h] - long long Multiplier
 *   [ESP+0Ch] - long long Multiplicand
 * Registers:
 *   Unknown
 * Returns:
 *   EDX:EAX - 64 bit product
 * Notes:
 *   Routine removes the arguments from the stack.
 */
__declspec(naked) void __cdecl _allmul(void) {
	__asm {
		push ebp
		mov ebp, esp
		push edi
		push esi
		push ebx
		sub esp, 12
		mov ebx, [ebp+16]
		mov eax, [ebp+8]
		mul ebx
		mov ecx, [ebp+20]
		mov [ebp-24], eax
		mov eax, [ebp+8]
		mov esi, edx
		imul eax, ecx
		add esi, eax
		mov eax, [ebp+12]
		imul ebx, eax
		lea eax, [ebx+esi]
		mov [ebp-20], eax
		mov eax, [ebp-24]
		mov edx, [ebp-20]
		add esp, 12
		pop ebx
		pop esi
		pop edi
		pop ebp
		ret 0x10
	}
}
