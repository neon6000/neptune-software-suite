/************************************************************************
*
*	Neptune Boot Library Graphics Services
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef GFX_H
#define GFX_H

INLINE nblRect BL_RECT(int t, int b, int l, int r) {
	nblRect rc = {t,b,l,r};
	return rc;
}

INLINE nblColor RGB (uint8_t r, uint8_t g, uint8_t b) {
	nblColor c = {r,g,b,255};
	return c;
}

INLINE nblColor RGBA (uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
	nblColor c = {r,g,b,a};
	return c;
}

extern status_t BlCreateSurface (OUT nblSurface* out, IN int width, IN int height, IN int bpp,
									IN uint32_t rmask, IN uint32_t gmask, IN uint32_t bmask,
									IN uint32_t amask);

extern BOOL BlGetPixel        (IN nblSurface* s, IN int x, IN int y, OUT nblColor* c);
extern BOOL BlSetPixel        (IN nblSurface* s, IN int x, IN int y, IN nblColor c);
extern void BlDrawLine        (IN nblSurface* s, IN int x1,IN int y1,IN int x2,IN int y2,IN  nblColor c);
extern void BlDrawCircle      (IN nblSurface* s, IN int cx, IN int cy,IN  float radius, IN nblColor c);
extern void BlDrawFillCircle  (IN nblSurface* s, IN int cx, IN int cy,IN  float radius, IN nblColor c);
extern void BlDrawFillRect    (IN nblSurface* s, IN nblRect rc, IN nblColor c);
extern void BlDrawRect        (IN nblSurface* s, IN nblRect rc, IN nblColor c);
extern void BlDrawGradientHorzRect (IN nblSurface* s, IN nblRect rc, IN nblColor c1, IN nblColor c2);
extern void BlDrawGradientVertRect (IN nblSurface* s, IN nblRect rc, IN nblColor c1, IN nblColor c2);
extern void BlDrawFillEllipse (IN nblSurface* s, IN int cx, IN int cy, IN int width, IN int height,  IN nblColor c);
extern void BlFillTriangle (IN nblSurface* s, int x0, int y0, int x1, int y1, int x2, int y2,
												 IN nblColor c1, IN nblColor c2, IN nblColor c3);

extern void BlFillColor (IN nblSurface* s, IN int x, IN int y, IN int width, IN int height, nblColor c);
extern void BlFillColor16 (IN nblSurface* s, IN int x, IN int y, IN int width, IN int height, nblColor c);
extern void BlFillColor24 (IN nblSurface* s, IN int x, IN int y, IN int width, IN int height, nblColor c);
extern void BlFillColor32 (IN nblSurface* s, IN int x, IN int y, IN int width, IN int height, nblColor c);


extern void BlBitBlit (IN nblSurface* in, OUT nblSurface* out,
				int xdest,int ydest, int width, int height, int xsrc, int ysrc);

extern void BlBlendBlit (IN nblSurface* in, OUT nblSurface* out,
				  int xdest,int ydest, int width,int heiht,int xsrc, int ysrc);

#endif
