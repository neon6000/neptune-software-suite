/************************************************************************
*
*	debug.c - Neptune Boot Library Runtime Debug Services
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>

#pragma warning (disable:4996)

/**
*	Assert handler. This method should not be called directly, use ASSERT instead.
*	\param cond Condition string
*	\param file Pointer to file of assertion
*	\param line Line number
*	\param function Pointer to function name
*	\param msg Optional message
*/
void  NBL_CALL BlrDbgAssert (IN char* cond, IN char* file, IN unsigned long line, IN char* function, IN OPTIONAL char* msg) {

	/* display assertion information. */
	BlrPrintf("\n\rAssertion failed!");
	BlrPrintf("\n\r  %s at %s line %i", cond, function, line);
	if (msg)
		BlrPrintf("\n\r   Message: %s", msg);

	/* call the debugger. */
	BlrDbgBreak ();
}

/**
*	Trace handler. This method should not be called directly, use TRACE instead.
*	\param cond Condition string
*	\param file Pointer to file of assertion
*	\param line Line number
*	\param function Pointer to function name
*	\param msg Optional message
*/
void  NBL_CALL BlrDbgTrace (IN char* cond, IN char* file, IN unsigned long line, IN char* function) {

}

/**
*	Debug Print handler. This method should not be called directly, use DPRINT instead.
*	\param file Pointer to file of assertion
*	\param line Line number
*	\param function Pointer to function name
*	\param format Message format
*	\param ... Optional arguments
*/
void NBL_CALL BlrDbgPrint (IN char* file, IN unsigned long line, IN char* function, IN char* format, ...) {

	char buffer[256];
	va_list args;

	BlrWriteFormattedString (buffer, "DPRINT: '%s:%s(%i)':", file,function,line);
	BlrFsWrite (nbl_stderr,BlrStrLength(buffer),buffer);

	NBL_VA_START (args, format);
	BlrWriteVaListToString (buffer,format,args);
	NBL_VA_END (args);

	BlrFsWrite (nbl_stderr,BlrStrLength(buffer),buffer);
}

void NBL_CALL BlrPrintf (IN char* format, ...) {

	char buffer[80];
	va_list args;

	BlrZeroMemory(buffer,80);

	NBL_VA_START (args, format);
	BlrWriteVaListToString (buffer,format,args);
	NBL_VA_END (args);

	BlrFsWrite (nbl_stdout, BlrStrLength(buffer) ,buffer);
}

void NBL_CALL BlrWidePrintf(IN wchar_t* format, ...) {

	wchar_t buffer[80];
	va_list args;

	NBL_VA_START(args, format);
	BlrWriteVaListToWideString(buffer, format, args);
	NBL_VA_END(args);

	BlrFsWrite(nbl_wstdout, BlrWideStrLength(buffer), buffer);
}

/* reads formatted data from string. */
int NBL_CALL BlrSPrintf (IN const char* s, IN const char* format, OUT ...) {

	va_list args;
	int result;

	NBL_VA_START (args, format);
	result = BlrWriteVaListToString (s, format, args);
	NBL_VA_END (args);
	return result;
}

/* reads formatted data from string. */
int NBL_CALL BlrWideSPrintf(IN const wchar_t* s, IN const wchar_t* format, OUT ...) {

	va_list args;
	int result;

	NBL_VA_START(args, format);
	result = BlrWriteVaListToWideString(s, format, args);
	NBL_VA_END(args);
	return result;
}

/* reads formatted string to str. Similar to scanf. */
void NBL_CALL BlrScanf (IN const char* format, OUT ...) {

	char buffer[40];
	va_list args;

	BlrFsRead (nbl_stdin,40,buffer);

	NBL_VA_START (args, format);
	BlrWriteStringToVaList (buffer, format, args);
	NBL_VA_END (args);
}

/**
* Call debugger, if one is present
*/
void NBL_CALL BlrDbgBreak (void) {

	/* raise breakpoint exception. */
	__asm int 3
}

void BlrDebugRead(IN size_t size, OUT char* buffer) {

	nblDriverObject* driver = BlrOpenDriver("root/debug");
	nblMessage rpb;
	rpb.type = NBL_DEBUG_READ;
	rpb.NBL_DEBUG_READ_SIZE = size;
	rpb.NBL_DEBUG_READ_BUFFER = buffer;

	if (driver)
		driver->request(&rpb);
}

void BlrDebugWrite(IN size_t size, IN char* buffer) {

	nblDriverObject* driver = BlrOpenDriver("root/debug");
	nblMessage rpb;
	rpb.type = NBL_DEBUG_WRITE;
	rpb.NBL_DEBUG_WRITE_SIZE = size;
	rpb.NBL_DEBUG_WRITE_BUFFER = buffer;

	if (driver)
		driver->request(&rpb);
}


