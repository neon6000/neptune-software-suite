/************************************************************************
*
*	alloc.c - NBLR Heap allocator
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include <string.h>

/* magic number for validating allocs */
#define MM_ALLOC_MAGIC 0x000a110c

/**
* Allocation header
*/
typedef struct _allocHeader {
	int magic;
	size_t size;
	struct _allocHeader* next;
}allocHeader;

/**
* Heap information block
*/
typedef struct _heap {
	unsigned char* start;
	size_t size;
	size_t maxSize;
	allocHeader* freeListHead;
}heap;

/**
*  Sometimes we need to allocate data at different regions
*  of the address space. For example, if the kernel is at 1MB
*  but main heap is also at 1MB, we need to use a secondary
*  heap at a lower address. To get around this, we use the idea
*  of "zones." A "zone" is a specialized heap for a range of
*  addresses.
*/

/* Conventional heap. (Starts at 1MB typically.) */
static heap _blHeap;

/* Low Memory Heap (Starts at 0x1000 typically.) */
static heap _blLowHeap;

/* points to active heap to allocate/free from. */
static heap* _blActiveHeap;

PUBLIC void BlrSetActiveHeap (ZONE zone) {
	if (zone == 1)
		_blActiveHeap = &_blLowHeap;
	else
		_blActiveHeap = &_blHeap;
}

STATIC status_t NBL_CALL BlrInitRegion (addr_t base, unsigned long size, ZONE zone) {

	if (zone == ZONE_NORMAL) {

		_blHeap.start   = (unsigned char*) base;
		_blHeap.size    = 0;
		_blHeap.maxSize = size;
		_blHeap.freeListHead = NULL;

		_blActiveHeap = &_blHeap;
		return NBL_STAT_SUCCESS;
	}
	else if (zone == ZONE_LOW) {

		_blLowHeap.start   = (unsigned char*) base;
		_blLowHeap.size    = 0;
		_blLowHeap.maxSize = size;
		_blLowHeap.freeListHead = NULL;

		_blActiveHeap = &_blLowHeap;
		return NBL_STAT_SUCCESS;
	}
	else
		return NBL_STAT_UNSUPPORTED;
}

status_t NBL_CALL BlrInitializeHeap (void) {

	nblDeviceObject* mm;
	nblMessage m;

	/* open memory device. */
	mm = BlrOpenDevice("mm");
	if (! mm) {
		return NBL_STAT_DEVICE;
	}

	if (!mm->driverObject->request) {
		return NBL_STAT_DEVICE;
	}

	/* allocate pages. */
	m.type = NBL_MM_INIT;
	m.NBL_MM_INIT_FUNCTION = (unsigned int) BlrInitRegion;
	if (mm->driverObject->request(&m) != NBL_STAT_SUCCESS)
		return NBL_STAT_DRIVER_INIT_FAIL;

	return NBL_STAT_SUCCESS;
}

/**
* Allocates memory from heap
* \param size Bytes to allocate
* \ret Pointer to memory or 0
*/
void* NBL_CALL BlrAlloc (IN size_t size) {

	allocHeader* cur;
	allocHeader* prev;

	/* sanity check. */
	if (size < 0) {
		return 0;
	}

	/* initialize. */
	cur = _blActiveHeap->freeListHead;
	prev = _blActiveHeap->freeListHead;

	/* find a free block */
	while (cur) {
		if (cur->size > size) {
              prev->next = cur->next;
              if(cur == _blActiveHeap->freeListHead)
                  _blActiveHeap->freeListHead = cur->next;
              cur->next = 0;
              return (void*) ++cur;
		}
		prev=cur;
		cur=cur->next;
	}
	++size;

	/* watch for memory exceeded. */
	if(_blActiveHeap->size + size + (size_t) sizeof(allocHeader) > _blActiveHeap->maxSize) {
		return 0;
	}

	/* allocate new block. */
	cur = (allocHeader*)_blActiveHeap->start;
	cur->magic = MM_ALLOC_MAGIC;
	cur->size = size;
	cur->next = 0;

	/* next block starts here. */
	_blActiveHeap->start += size + sizeof(allocHeader);

	/* return success. */
    return (void*) ++cur;


#if 0
	allocHeader* cur;
	allocHeader* prev;

	/* sanity check. */
	if (size < 0) {
		return 0;
	}

	/* initialize. */
	cur = _blFreeList;
	prev = _blFreeList;

	/* find a free block */
	while (cur) {
		if (cur->size > size) {
              prev->next = cur->next;
              if(cur == _blFreeList)
                  _blFreeList = cur->next;
              cur->next = 0;
              return (void*) ++cur;
		}
		prev=cur;
		cur=cur->next;
	}
	++size;

	/* watch for memory exceeded. */
	if(_blHeap.size + size + (size_t) sizeof(allocHeader) > _blHeap.maxSize) {
		return 0;
	}

	/* allocate new block. */
	cur = (allocHeader*)_blHeap.start;
	cur->magic = MM_ALLOC_MAGIC;
	cur->size = size;
	cur->next = 0;

	/* next block starts here. */
	_blHeap.start += size + sizeof(allocHeader);

	/* return success. */
    return (void*) ++cur;

#endif
}

/**
* Free block of memory
* \param addr Allocated block to free
*/
void NBL_CALL BlrFree (IN void* addr) {

	allocHeader* header;

	/* sanity check. */
	if (!addr) {
		return;
	}

	/* Get allocation header. */
	header = (allocHeader*)addr;
	header--;

	/* Test for valid header. */
	if (header->magic != MM_ALLOC_MAGIC) {

		/* we did not allocate this block or it was corrupted. */
		return;
	}

	/* release block. */
	if (_blActiveHeap->freeListHead == 0) {
		_blActiveHeap->freeListHead = header;
	}
	else{
		header->next = _blActiveHeap->freeListHead->next;
		_blActiveHeap->freeListHead->next = header;
	}

#if 0

	allocHeader* header;

	/* sanity check. */
	if (!addr) {
		return;
	}

	/* Get allocation header. */
	header = (allocHeader*)addr;
	header--;

	/* Test for valid header. */
	if (header->magic != MM_ALLOC_MAGIC) {

		/* we did not allocate this block or it was corrupted. */
		return;
	}

	/* release block. */
	if (_blFreeList == 0) {
		_blFreeList = header;
	}
	else{
		header->next = _blFreeList->next;
		_blFreeList->next = header;
	}

#endif

}

/**
* Allocates memory from heap
* \param size Bytes to allocate
* \ret Pointer to memory or 0
*/
void* NBL_CALL BlrZeroAlloc (IN size_t size) {

	void* ret;

	/* sanity check. */
	if (size < 0) {
		return 0;
	}

	/* allocate block. */
	ret = BlrAlloc(size);

	/* clear memory block and return success. */
	if (ret)
		BlrZeroMemory(ret,size);

	return ret;
}

/**
*	Zero memory block.
*	\param destination Destination
*	\param length Size of block
*/
void NBL_CALL BlrZeroMemory (IN OUT void* destination, IN size_t length) {
	BlrFillMemory (destination, 0, length);
}

/**
*	Fill memory block.
*	\param destination Destination
*	\param value Fill value
*	\param length Size of block
*/
void NBL_CALL BlrFillMemory (IN OUT void* destination, IN unsigned char value, IN size_t length) {
	unsigned long i;
	for(i=0; i<length; i++) {
		((char*)destination)[i] = value;
	}
}

/**
*	Copy memory block.
*	\param destination Destination
*	\param value Source
*	\param length Size of block
*/
unsigned long NBL_CALL BlrCopyMemory (IN OUT void* s1, IN void* s2, IN size_t length) {
	unsigned long i;
	for(i=0; i<length; i++) {
		((char*)s1)[i] = ((char*)s2)[i];
	}
	return length;
}

/**
*	Compare memory blocks.
*	\param s Memory block to compare to
*	\param d Memory block to compare with
*	\param length Size of block
*/
unsigned long NBL_CALL BlrCompareMemory (IN void* s, IN void* d, size_t length) {
	unsigned long i;
	for(i=0; i<length; i++) {
		if( ((char*)s)[i] != ((char*)d)[i] )
			break; 
	}
	return i;
}
