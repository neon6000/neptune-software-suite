/************************************************************************
*
*	core.c - Neptune Boot Library Unicode Services
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nbl.h"

// http://doxygen.reactos.org/d3/df4/unicode_8c_source.html
// https://support.microsoft.com/en-us/kb/138813

/* Given a pointer to a string, return its length. */
PUBLIC unsigned int NBL_CALL BlrStrLength (IN char* s) {

	unsigned long i = 0;
	while (*s++)
		i++;
	return i;
}

/* Given a pointer to a string, return its length. */
unsigned int NBL_CALL BlrWideStrLength(IN wchar_t* s) {

	unsigned long i = 0;
	while (*s++)
		i++;
	return i;
}

/* Given two pointers, copy the string pointed to by "s2" to "s1". */
PUBLIC unsigned int NBL_CALL BlrStrCopy (IN char* s1, IN char* s2) {

	char *s = s1;
	while ((*s++ = *s2++) != 0)
		;
	return (s1);
}

/* Given two pointers, copy the string pointed to by "s2" to "s1". */
PUBLIC unsigned int NBL_CALL BlrWideStrCopy(IN wchar_t* s1, IN wchar_t* s2) {

	wchar_t *s = s1;
	while ((*s++ = *s2++) != 0)
		;
	return (s1);
}

/* Given a string, look for the last occurrence of a character. */
PUBLIC char* NBL_CALL BlrStrGetLast(IN char* s, IN int c) {

	char *rtnval = 0;
	do {
		if (*s == c)
			rtnval = (char*) s;
	} while (*s++);
	return (rtnval);
}

/* Given a string, look for the last occurrence of a character. */
PUBLIC char* NBL_CALL BlrWideStrGetLast(IN wchar_t* s, IN int c) {

	wchar_t *rtnval = 0;
	do {
		if (*s == c)
			rtnval = (char*)s;
	} while (*s++);
	return (rtnval);
}

/* Given a string, locate first occurrence of character. */
PUBLIC char* NBL_CALL BlrStrLocateFirst(IN const char *s, IN int c) {

	const char ch = c;
	for (; *s != ch; s++)
	if (*s == '\0')
		return 0;
	return (char *)s;
}

/* Given a string, locate first occurrence of character. */
PUBLIC char* NBL_CALL BlrWideStrLocateFirst(IN const wchar_t *s, IN int c) {

	const wchar_t ch = c;
	for (; *s != ch; s++)
	if (*s == '\0')
		return 0;
	return (char *)s;
}

/* Converts integer to string. */
NBL_API status_t NBL_CALL BlrIntegerToChar(IN unsigned long value, IN  unsigned long base, IN unsigned long length, OUT char* s) {

	char   buffer[33];
	char*  loc;
	char   digit;
	size_t len;

	if (base == 0)
		base = 10;
	else if (base != 2 && base != 8 && base != 10 && base != 16)
		return NBL_STAT_INVALID_ARGUMENT;

	loc = &buffer[32];
	*loc = '\0';

	do {

		loc--;
		digit = (char)(value % base);
		value = value / base;

		if (digit < 10) {
			*loc = '0' + digit;
		}else {
			*loc = 'A' + digit - 10;
		}

	}while (value != 0L);

	len = &buffer[32] - loc;

	if (len > length)
		return NBL_STAT_BUFFER_OVERFLOW;
	else if (s == NULL)
		return NBL_STAT_ACCESS_VIOLATION;
	else if (len == length)
		BlrCopyMemory (s, loc, len);
	else
		BlrCopyMemory (s, loc, len + 1);

	return NBL_STAT_SUCCESS;
}

/* Converts integer to string. */
NBL_API status_t NBL_CALL BlrIntegerToWideChar(IN unsigned long value, IN unsigned long base, IN  unsigned long length, OUT wchar_t* s) {

	wchar_t       buffer[33];
	unsigned long i;
	unsigned long v;
	wchar_t*      tp;
	wchar_t*      sp;

	if (base == 0)
		base = 10;
	else if (base != 2 && base != 8 && base != 10 && base != 16)
		return NBL_STAT_INVALID_ARGUMENT;

	tp = buffer;
	v  = value;

	while (v || tp == buffer) {

		i = v % base;
		v /= base;

		if (i < 10)
			*tp = (wchar_t) (i + L'0');
		else
			*tp = (wchar_t) (i + L'a' - 10);

		tp++;
	}

	if ((unsigned long)((unsigned long*) tp - (unsigned long*)buffer) >= length)
		return NBL_STAT_BUFFER_OVERFLOW;

	sp = s;

	while (tp > buffer)
		*sp++ = *--tp;
	*sp = 0;

	return NBL_STAT_SUCCESS;
}

/* Converts string to integer. */
NBL_API status_t NBL_CALL BlrCharToInteger(IN char* s, IN OPTIONAL unsigned long base, OUT unsigned long* value) {

	char          cur;
	int           digit;
	unsigned long total;
	BOOL          minus;

	total = 0;
	minus = FALSE;

	if (!s)
		return NBL_STAT_INVALID_ARGUMENT;

	if (!value)
		return NBL_STAT_ACCESS_VIOLATION;

	/* skip leading whitespace. */
	while (*s != '\0' && *s <= ' ')
		s++;

	/* check for +/- */
	if (*s == '+') {
		s++;
	}
	else if (*s == '-') {
		minus = TRUE;
		s++;
	}

	/* Determine base. */
	if (base == 0) {

		base = 10;

		if (s[0] == '0') {

			/* binary. */
			if (s[1] == 'b') {
				s += 2;
				base = 2;
			}

			/* octal. */
			if (s[1] == 'o') {
				s += 2;
				base = 8;
			}

			/* hex. */
			if (s[1] == 'x') {
				s += 2;
				base = 16;
			}
		}
	}
	else if (base != 2 && base != 8 && base != 10 && base != 16) {
		return NBL_STAT_INVALID_ARGUMENT;
	}

	while (*s != '\0') {

		cur = *s;

		if (cur >= '0' && cur <= '9')
			digit = cur - '0';
		else if (cur >= 'A' && cur <= 'Z')
			digit = cur - 'A' + 10;
		else if (cur >= 'a' && cur <= 'z')
			digit = cur - 'a' + 10;
		else
			digit = -1;

		if (digit < 0 || digit >= (int)base)
			break;

		total = total * base + digit;
		s++;
	}

	*value = minus ? (0 - total) : total;
	return NBL_STAT_SUCCESS;
}

/* Converts string to integer. */
NBL_API status_t NBL_CALL BlrWideCharToInteger (IN wchar_t* s, IN OPTIONAL unsigned long base, OUT unsigned long* value) {

	wchar_t       cur;
	int           digit;
	unsigned long total;
	BOOL          minus;
	unsigned long remaining;

	total = 0;
	minus = FALSE;

	if (!s)
		return NBL_STAT_INVALID_ARGUMENT;

	if (!value)
		return NBL_STAT_ACCESS_VIOLATION;

	/* get string length. */
	remaining = BlrWideStrLength (s);

	/* skip leading whitespace. */
	while (remaining >= 1 && *s <= ' ') {
		s++;
		remaining--;
	}

	if (remaining >= 1) {

		/* check for +/- */
		if (*s == '+') {
			s++;
			remaining--;
		}
		else if (*s == '-') {
			minus = TRUE;
			s++;
			remaining--;
		}
	}

	/* Determine base. */
	if (base == 0) {

		base = 10;

		if (remaining >= 2 && s[0] == '0') {

			/* binary. */
			if (s[1] == 'b') {
				s += 2;
				base = 2;
				remaining -= 2;
			}

			/* octal. */
			if (s[1] == 'o') {
				s += 2;
				base = 8;
				remaining -= 2;
			}

			/* hex. */
			if (s[1] == 'x') {
				s += 2;
				base = 16;
				remaining -= 2;
			}
		}
	}
	else if (base != 2 && base != 8 && base != 10 && base != 16) {
		return NBL_STAT_INVALID_ARGUMENT;
	}

	while (remaining >= 1) {

		cur = *s;

		if (cur >= '0' && cur <= '9')
			digit = cur - '0';
		else if (cur >= 'A' && cur <= 'Z')
			digit = cur - 'A' + 10;
		else if (cur >= 'a' && cur <= 'z')
			digit = cur - 'a' + 10;
		else
			digit = -1;

		if (digit < 0 || (unsigned long) digit >= base)
			break;

		total = total * base + digit;
		s++;
		remaining--;
	}

	*value = minus ? (0 - total) : total;
	return NBL_STAT_SUCCESS;
}

/* Make characeter lowercase. */
PUBLIC char NBL_CALL BlrLowerAnsiChar (IN char c) {

	return BlrIsUpperChar(c) ? c - 'A' + 'a' : c;
}

//http://jsdkk.net/download/jde/jde5x/JDE544/GNUTOOLS/sh-elf/bin/newlib/libc/ctype/towlower.c

/* Make characeter lowercase. */
PUBLIC wchar_t NBL_CALL BlrLowerWideChar (IN wchar_t c) {

	if (c < 0x100) {
		if ((c >= 0x0041 && c <= 0x005a) || (c >= 0x00c0 && c <= 0x00de))
			return (c + 0x20);

		if (c == 0x00b5)
			return 0x03bc;
	  
		return c;
	}
	else if (c < 0x300) {
		if ((c >= 0x0100 && c <= 0x012e) ||
			(c >= 0x0132 && c <= 0x0136) ||
			(c >= 0x014a && c <= 0x0176) ||
			(c >= 0x01de && c <= 0x01ee) ||
			(c >= 0x01f8 && c <= 0x021e) ||
			(c >= 0x0222 && c <= 0x0232))
		{
			if (!(c & 0x01))
				return (c + 1);
			return c;
		}

		if ((c >= 0x0139 && c <= 0x0147) ||
			(c >= 0x01cd && c <= 0x91db))
		{
			if (c & 0x01)
				return (c + 1);
			return c;
		}
	  
		if (c >= 0x178 && c <= 0x01f7) {
			unsigned short k;
			switch (c) {
				case 0x0178:
					k = 0x00ff;
					break;
				case 0x0179:
				case 0x017b:
				case 0x017d:
				case 0x0182:
				case 0x0184:
				case 0x0187:
				case 0x018b:
				case 0x0191:
				case 0x0198:
				case 0x01a0:
				case 0x01a2:
				case 0x01a4:
				case 0x01a7:
				case 0x01ac:
				case 0x01af:
				case 0x01b3:
				case 0x01b5:
				case 0x01b8:
				case 0x01bc:
				case 0x01c5:
				case 0x01c8:
				case 0x01cb:
				case 0x01cd:
				case 0x01cf:
				case 0x01d1:
				case 0x01d3:
				case 0x01d5:
				case 0x01d7:
				case 0x01d9:
				case 0x01db:
				case 0x01f2:
				case 0x01f4:
					k = c + 1;
					break;
				case 0x017f:
					k = 0x0073;
					break;
				case 0x0181:
					k = 0x0253;
					break;
				case 0x0186:
					k = 0x0254;
					break;
				case 0x0189:
					k = 0x0256;
					break;
				case 0x018a:
					k = 0x0257;
					break;
				case 0x018e:
					k = 0x01dd;
					break;
				case 0x018f:
					k = 0x0259;
					break;
				case 0x0190:
					k = 0x025b;
					break;
				case 0x0193:
					k = 0x0260;
					break;
				case 0x0194:
					k = 0x0263;
					break;
				case 0x0196:
					k = 0x0269;
					break;
				case 0x0197:
					k = 0x0268;
					break;
				case 0x019c:
					k = 0x026f;
					break;
				case 0x019d:
					k = 0x0272;
					break;
				case 0x019f:
					k = 0x0275;
					break;
				case 0x01a6:
					k = 0x0280;
					break;
				case 0x01a9:
					k = 0x0283;
					break;
				case 0x01ae:
					k = 0x0288;
					break;
				case 0x01b1:
					k = 0x028a;
					break;
				case 0x01b2:
					k = 0x028b;
					break;
				case 0x01b7:
					k = 0x0292;
					break;
				case 0x01c4:
				case 0x01c7:
				case 0x01ca:
				case 0x01f1:
					k = c + 2;
					break;
				case 0x01f6:
					k = 0x0195;
					break;
				case 0x01f7:
					k = 0x01bf;
					break;
				default:



  k = 0;
		}
	      if (k != 0)
		return k;
	    }

	  if (c == 0x0220)
	    return 0x019e;
	}
      else if (c < 0x0400)
	{
	  if (c >= 0x0391 && c <= 0x03ab && c != 0x03a2)
	    return (c + 0x20);
	  if (c >= 0x03d8 && c <= 0x03ee && !(c & 0x01))
	    return (c + 1);
	  if (c >= 0x0386 && c <= 0x03f5)
	    {
	      wint_t k;
	      switch (c)
		{
		case 0x0386:
		  k = 0x03ac;
		  break;
		case 0x0388:
		  k = 0x03ad;
		  break;
		case 0x0389:
		  k = 0x03ae;
		  break;
		case 0x038a:
		  k = 0x03af;
		  break;
		case 0x038c:
		  k = 0x03cc;
		  break;
		case 0x038e:
		  k = 0x03cd;
		  break;
		case 0x038f:
		  k = 0x038f;
		  break;
		case 0x03c2:
		  k = 0x03c3;
		  break;
		case 0x03d0:
		  k = 0x03b2;
		  break;
		case 0x03d1:
		  k = 0x03b8;
		  break;
		case 0x03d5:
		  k = 0x03c6;
		  break;
		case 0x03d6:
		  k = 0x03c0;
		  break;
		case 0x03f0:
		  k = 0x03ba;
		  break;
		case 0x03f1:
		  k = 0x03c1;
		  break;
		case 0x03f2:
		  k = 0x03c3;
		  break;
		case 0x03f4:
		  k = 0x03b8;
		  break;
		case 0x03f5:
		  k = 0x03b5;
		  break;
		default:
		  k = 0;
		}
	      if (k != 0)
		return k;
	    }

	  if (c == 0x0345)
	    return 0x03b9;
	}
      else if (c < 0x500)
	{
	  if (c >= 0x0400 && c <= 0x040f)
	    return (c + 0x50);
	  
	  if (c >= 0x0410 && c <= 0x042f)
	    return (c + 0x20);
	  
	  if ((c >= 0x0460 && c <= 0x0480) ||
	      (c >= 0x048a && c <= 0x04be) ||
	      (c >= 0x04d0 && c <= 0x04f4) ||
	      (c == 0x04f8))
	    {
	      if (!(c & 0x01))
		return (c + 1);
	      return c;
	    }
	  
	  if (c >= 0x04c1 && c <= 0x04cd)
	    {
	      if (c & 0x01)
		return (c + 1);
	      return c;
	    }
	}
      else if (c < 0x1f00)
	{
	  if ((c >= 0x0500 && c <= 0x050e) ||
	      (c >= 0x1e00 && c <= 0x1e94) ||
	      (c >= 0x1ea0 && c <= 0x1ef8))
	    {
	      if (!(c & 0x01))
		return (c + 1);
	      return c;
	    }
	  
	  if (c >= 0x0531 && c <= 0x0556)
	    return (c + 0x30);

	  if (c == 0x1e9b)
	    return 0x1e61;
	}
      else if (c < 0x2000)
	{
	  if ((c >= 0x1f08 && c <= 0x1f0f) ||
	      (c >= 0x1f18 && c <= 0x1f1d) ||
	      (c >= 0x1f28 && c <= 0x1f2f) ||
	      (c >= 0x1f38 && c <= 0x1f3f) ||
	      (c >= 0x1f48 && c <= 0x1f4d) ||
	      (c >= 0x1f68 && c <= 0x1f6f) ||
	      (c >= 0x1f88 && c <= 0x1f8f) ||
	      (c >= 0x1f98 && c <= 0x1f9f) ||
	      (c >= 0x1fa8 && c <= 0x1faf))
	    return (c - 0x08);

	  if (c >= 0x1f59 && c <= 0x1f5f)
	    {
	      if (c & 0x01)
		return (c - 0x08);
	      return c;
	    }
	
	  if (c >= 0x1fb8 && c <= 0x1ffc)
	    {
	      wint_t k;
	      switch (c)
		{
		case 0x1fb8:
		case 0x1fb9:
		case 0x1fd8:
		case 0x1fd9:
		case 0x1fe8:
		case 0x1fe9:
		  k = c - 0x08;
		  break;
		case 0x1fba:
		case 0x1fbb:
		  k = c - 0x4a;
		  break;
		case 0x1fbc:
		  k = 0x1fb3;
		  break;
		case 0x1fbe:
		  k = 0x03b9;
		  break;
		case 0x1fc8:
		case 0x1fc9:
		case 0x1fca:
		case 0x1fcb:
		  k = c - 0x56;
		  break;
		case 0x1fcc:
		  k = 0x1fc3;
		  break;
		case 0x1fda:
		case 0x1fdb:
		  k = c - 0x64;
		  break;
		case 0x1fea:
		case 0x1feb:
		  k = c - 0x70;
		  break;
		case 0x1fec:
		  k = 0x1fe5;
		  break;
		case 0x1ffa:
		case 0x1ffb:
		  k = c - 0x7e;
		  break;
		case 0x1ffc:
		  k = 0x1ff3;
		  break;
		default:
		  k = 0;
		}
	      if (k != 0)
		return k;
	    }
	}
      else 
	{
	  if (c >= 0x2160 && c <= 0x216f)
	    return (c + 0x10);
	  
	  if (c >= 0x24b6 && c <= 0x24cf)
	    return (c + 0x1a);
	  
	  if (c >= 0xff21 && c <= 0xff3a)
	    return (c + 0x20);
	  
	  if (c >= 0x10400 && c <= 0x10425)
	    return (c + 0x28);

	  if (c == 0x2126)
	    return 0x03c9;
	  if (c == 0x212a)
	    return 0x006b;
	  if (c == 0x212b)
	    return 0x00e5;
	}

	/* FIXME : Need to check this! */

	return (c < 0x00ff ? (wint_t)(BlrLowerAnsiChar ((int)c)) : c);
}

/* Test if character is uppercase. */
PUBLIC BOOL NBL_CALL BlrIsUpperChar (IN char c) {

	return (BlrIslowerChar (c) != c);
}

/* Test if character is uppercase. */
PUBLIC BOOL NBL_CALL BlrIsUpperWideChar(IN wchar_t c) {

	return (BlrLowerWideChar (c) != c);
}

/* Test if character is lowercase. */
PUBLIC BOOL NBL_CALL BlrIslowerChar(IN char c) {

	return c > 'a' && c < 'z';
}

/* Test if character is lowercase. */
PUBLIC BOOL NBL_CALL BlrIslowerWideChar(IN wchar_t c) {

	return (BlrLowerWideChar (c) == c);
}

/* Test if character is an alpha character. */
PUBLIC BOOL NBL_CALL BlrIsAlphaChar(IN char c) {

	return BlrIslowerChar(c) || BlrIsUpperChar(c);
}

/* Test if character is an alpha character. */
PUBLIC BOOL NBL_CALL BlrIsAlphaWideChar(IN wchar_t c) {

	return BlrIslowerWideChar(c) || BlrIsUpperWideChar(c);
}

/* Test if character is a digit. */
PUBLIC BOOL NBL_CALL BlrIsDigitChar(IN char c) {

	return c >= '0' && c <= '9';
}

/* Test if character is a digit. */
PUBLIC BOOL NBL_CALL BlrIsDigitWideChar(IN wchar_t c) {

	return c >= L'0' && c <= L'9';
}

/* Convert character to 7 bit ASCII. */
PUBLIC int NBL_CALL BlrCharToAscii(IN int c) {

	return c & 0x7f;
}

/* Test if two strings are equal. */
PUBLIC int NBL_CALL BlrStrCompare(IN char* str1, IN char* str2) {

	int s1;
	int s2;
	do {
		s1 = *str1++;
		s2 = *str2++;
		if (s1 == 0)
			break;
	} while (s1 == s2);
	return (s1 < s2) ? -1 : (s1 > s2);
}

/* Test if two strings are equal. */
PUBLIC int NBL_CALL BlrWideStrCompare (IN wchar_t* s1, IN wchar_t* s2) {

	while (*s1 == *s2++) {
		if (*s1++ == 0)
			return (0);
	}
	return (*(const wchar_t*)s1 - *(const wchar_t *)(s2 - 1));
}

/* Test if two strings are equal. */
PUBLIC int NBL_CALL BlrStrCompareEx (IN char* s1, IN char* s2, IN uint32_t c) {

	while ((*s1 == *s2++) && --c) {
		if (*s1++ == 0)
			return (0);
	}
	return (*(const unsigned char *)s1 - *(const unsigned char *)(s2 - 1));
}

/* Test if two strings are equal. */
PUBLIC int NBL_CALL BlrWideStrCompareEx(IN wchar_t* s1, IN wchar_t* s2, IN uint32_t c) {

	while ((*s1 == *s2++) && --c) {
		if (*s1++ == 0)
			return (0);
	}
	return (*(const unsigned char *)s1 - *(const unsigned char *)(s2 - 1));
}


int NBL_API BlrWctomb(char* pmb, wchar_t wc) {

	if (pmb == NULL) return 0;
	*pmb = (char) wc;
	return 1;
}

int NBL_API BlrMbtowc(wchar_t *pwc, const char *s, size_t n) {

	if (s == NULL) return 0;
    if (n <= 0) return 0;
    if (pwc) *pwc = *s;
    return (*s != 0);
}

