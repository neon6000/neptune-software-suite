/************************************************************************
*
*	Neptune Boot Library Graphics Services
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nbl.h"
#include "gfx.h"

/*
	Have to look into the math for implementing unfilled ellipses.
*/

void BlDrawFillCircleTest(IN nblSurface* s, IN int cx, IN int cy, IN  float radius, IN nblColor c) {

	int x, y;
	for (y = (int)-radius; y <= radius; y++)
	for (x = (int)-radius; x <= radius; x++)
	if (x*x + y*y <= radius*radius)
		BlSetPixel(s, cx + x, cy + y, c);
}

int BlrAbs (int i) {
	return (i < 0 ? -i : i);
}

BOOL BlSetPixel (IN nblSurface* s, IN int x, IN int y, IN nblColor c) {

	if (x<0 || y<0 || x >= s->width || y >= s->height)
		return FALSE;

	switch(s->format.bpp) {
		case 1:
			{
				int bitIndex;
				int bitPosition;
				uint8_t* p;

				bitIndex = s->width + x;
				p = ((uint8_t*) s->pixels) + bitIndex / 8;
				bitPosition = bitIndex % 8;

				/* for pixel color, we only use alpha component. */
				*p = (*p & ~(1 << bitPosition)) | ((c.alpha & 1) << bitPosition);

				break;
			}
		case 8:
			((uint8_t*)s->pixels) [(y * s->width) + x] =
						c.red     << s->format.rshift
						| c.green << s->format.gshift
						| c.blue  << s->format.bshift
						| c.alpha << s->format.ashift;
			break;
		case 16: {
			uint16_t col = 	c.red     << s->format.rshift
						| c.green << s->format.gshift
						| c.blue  << s->format.bshift;
			((uint16_t*)s->pixels) [(y * s->width) + x] = col;
			break;
		}
		case 24: {
			uint8_t* p = (uint8_t*)s->pixels;
			p += (y * s->width + x) * 3;
			{
				*p++ = c.blue;
				*p++ = c.green;
				*p = c.red;
			}
			break;
		}
		case 32: {

			uint32_t col = c.red     << s->format.rshift
						| c.green << s->format.gshift
						| c.blue  << s->format.bshift
						| c.alpha << s->format.ashift;

			((uint32_t*)s->pixels) [(y * s->width) + x] = col;
			break;
		}
	};

	return TRUE;
}

BOOL BlGetPixel (IN nblSurface* s, IN int x, IN int y, OUT nblColor* c) {

	if (x<0 || y<0 || x >= s->width || y >= s->height)
		return FALSE;

	switch(s->format.bpp) {
		case 1:
			{
				int bitIndex;
				int bitPosition;
				int bit;
				uint8_t* p;

				bitIndex = y * s->width + x;
				p = ((uint8_t*) s->pixels) + bitIndex / 8;
				bitPosition = 7 - bitIndex % 8;

				bit = (*p >> bitPosition) & 1;

				if (bit == 0) {
					c->red = s->format.back.red;
					c->green = s->format.back.green;
					c->blue =  s->format.back.blue;
					c->alpha = s->format.back.alpha;
				}else{
					c->red = s->format.front.red;
					c->green = s->format.front.green;
					c->blue =  s->format.front.blue;
					c->alpha = s->format.front.alpha;
				}

				break;
			}

		case 8: {

			/* TODO : Need 8 bit support. */
			break;
		}
		case 16: {

			uint16_t value = 	((uint16_t*)s->pixels) [(y * s->width) + x];

			c->red   = (value & s->format.rmask) >> s->format.rshift;
			c->green = (value & s->format.gmask) >> s->format.gshift;
			c->blue  = (value & s->format.bmask) >> s->format.bshift;
			c->alpha = (value & s->format.amask) >> s->format.ashift;

			break;
		}
		case 24: {

			uint8_t* p = (uint8_t*)s->pixels;
			p += (y * s->width + x) * 3;
			{
				uint32_t value = *(uint32_t*) p;

				c->red   =  (value & s->format.rmask) >> s->format.rshift;
				c->green =  (value & s->format.gmask) >> s->format.gshift;
				c->blue  =  (value & s->format.bmask) >> s->format.bshift;
				c->alpha =  (value & s->format.amask) >> s->format.ashift;
			}
			break;
		}
		case 32: {

			uint32_t value = 	((uint32_t*)s->pixels) [(y * s->width) + x];

			c->red   = (value & s->format.rmask) >> s->format.rshift;
			c->green = (value & s->format.gmask) >> s->format.gshift;
			c->blue  = (value & s->format.bmask) >> s->format.bshift;
			c->alpha = (value & s->format.amask) >> s->format.ashift;

			break;
		}
	};

	return TRUE;
}

void BlDrawLine (IN nblSurface* s, IN int x1,IN int y1,IN int x2,IN int y2,IN  nblColor c) {

	int dx,dy,d,incry,incre,incrne,slopegt1=0;
	dx=BlrAbs(x1-x2);dy=BlrAbs(y1-y2);
	if(dy>dx)
	{
		{
			/* swap x1, y1 */
			int t=x1;
			x1=y1;
			y1=t;

			/* swap x2, y2 */
			t=x2;
			x2=y2;
			y2=t;

			/* swap dx, dy */
			t=dx;
			dx=dy;
			dy=t;
		}

		slopegt1=1;
	}
	if(x1>x2)
	{
		/* swap x1, x2 */
		int t=x1;
		x1=x2;
		x2=t;

		/* swap y1,y2 */
		t=y1;
		y1=y2;
		y2=t;
	}
	if(y1>y2)
		incry=-1;
	else
		incry=1;
	d=2*dy-dx;
	incre=2*dy;
	incrne=2*(dy-dx);
	while(x1<x2)
	{
		if(d<=0)
			d+=incre;
		else
		{
			d+=incrne;
			y1+=incry;
		}
		x1++;
		if(slopegt1)
			BlSetPixel(s,y1,x1,c);
		else
			BlSetPixel(s,x1,y1,c);
	}
}

void BlDrawFillCircle (IN nblSurface* s, IN int cx, IN int cy,IN  float radius, IN nblColor c) {

	int x,y;
	for(y = (int) -radius; y <= radius; y++)
		for(x = (int) -radius; x <= radius; x++)
			if(x*x+y*y <= radius*radius)
				BlSetPixel(s, cx+x, cy+y, c);
}

void BlDrawFillEllipse (IN nblSurface* s, IN int cx, IN int cy, IN int width, IN int height,  IN nblColor c) {

	int x,y;
	for(y = -height; y <= height; y++) {
		for(x = -width; x <= width; x++) {
			if(x*x*height*height+y*y*width*width <= height*height*width*width)
				BlSetPixel(s, cx+x, cy+y, c);
		}
	}
}

void BlDrawCircle (IN nblSurface* s, IN int cx, IN int cy,IN  float radius, IN nblColor c) {

  int   x, y;
  float d;

  x = 0;
  y = (int) radius;

  // Draw four corner points.
  BlSetPixel(s, cx               , cy + (int)radius ,c);
  BlSetPixel(s, cx - (int)radius , cy               ,c);
  BlSetPixel(s, cx               , cy - (int)radius ,c);
  BlSetPixel(s, cx + (int)radius , cy               ,c);

  // Draw arc points.
  for (x = 0; x <= y; x++) {

	// For each arc point, draw the other corrosponding arc points in other quadrants.
    BlSetPixel (s,  x + cx,   y + cy   ,c);
    BlSetPixel (s,  y + cx,   x + cy   ,c);
    BlSetPixel (s,  x + cx,   -y + cy  ,c);
    BlSetPixel (s,  y + cx,   -x + cy  ,c);

    BlSetPixel (s,  -x + cx,  y + cy   ,c);
    BlSetPixel (s,  -y + cx,  x + cy   ,c);
    BlSetPixel (s,  -x + cx,  -y + cy  ,c);
    BlSetPixel (s,  -y + cx,  -x + cy  ,c);

	// Compute distance.
	d = x*x + y*y - radius*radius;

	// Test next move.
	if (d <= 0) {
        // x++ case already handled. Selects Middle-East as next.
	 }
	 else{
	    y--;
		// x++ case already handled. Selects Middle North East as next.
	 }
  }
}

// note that BlDrawGradientVertRect and BlDrawGradientHorzRect were changed from float to uint8_t
// to remove warnings.

void BlDrawGradientVertRect (IN nblSurface* s, IN nblRect rc, IN nblColor c1, IN nblColor c2) {
	int y = rc.top;
	int x = rc.left;
	while (x++ < rc.right) {
		while (y++ < rc.bottom) {

			nblColor c;
			uint8_t p = (uint8_t)y / rc.bottom;

			c.red   =  c1.red   * p + c2.red   * (1 - p);
			c.blue  =  c1.blue  * p + c2.blue  * (1 - p);
			c.green =  c1.green * p + c2.green * (1 - p);
			c.alpha =  c1.alpha * p + c2.alpha * (1 - p);

			BlSetPixel (s, x, y, c);
		}
		y = rc.top;
	}
}

void BlDrawGradientHorzRect (IN nblSurface* s, IN nblRect rc, IN nblColor c1, IN nblColor c2) {
	int y = rc.top;
	int x = rc.left;
	while (y++ < rc.bottom) {
		while (x++ < rc.right) {

			nblColor c;
			uint8_t p = (uint8_t)x / rc.right;

			c.red   =  c1.red   * p + c2.red   * (1 - p);
			c.blue  =  c1.blue  * p + c2.blue  * (1 - p);
			c.green =  c1.green * p + c2.green * (1 - p);
			c.alpha =  c1.alpha * p + c2.alpha * (1 - p);

			BlSetPixel (s, x, y, c);
		}
		x = rc.left;
	}
}

void BlDrawRect (IN nblSurface* s, IN nblRect rc, IN nblColor c) {

	BlDrawLine (s, rc.left, rc.top, rc.right, rc.top, c);
	BlDrawLine (s, rc.left, rc.top, rc.left, rc.bottom, c);
	BlDrawLine (s, rc.right, rc.top, rc.right, rc.bottom, c);
	BlDrawLine (s, rc.left, rc.bottom, rc.right, rc.bottom, c);
}

void BlDrawTriangle (IN nblSurface* s, int x0, int y0, int x1, int y1, int x2, int y2, IN nblColor c) {

	BlDrawLine(s,x0,y0,x1,y1,c);
	BlDrawLine(s,x1,y1,x2,y2,c);
	BlDrawLine(s,x2,y2,x0,y0,c);
}

void BlFillTriangle (IN nblSurface* s, int x0, int y0, int x1, int y1, int x2, int y2,
					 IN nblColor c1, IN nblColor c2, IN nblColor c3) {

	/* not supported yet. this needs to be retested. */

#if 0
	int x, y;
	nblRect rc;

	rc.left=x0;
	if(x1<rc.left) rc.left=x1;
	if(x2<rc.left) rc.left=x2;

	rc.top=y0;
	if(y1>rc.top) rc.top=y1;
	if(y2>rc.top) rc.top=y2;

	rc.right=x0;
	if(x1>rc.right) rc.right=x1;
	if(x2>rc.right) rc.right=x2;

	rc.bottom=y0;
	if(y1<rc.bottom) rc.bottom=y1;
	if(y2<rc.bottom) rc.bottom=y2;

	for (y=rc.top; y>rc.bottom; y--) {
		for (x=rc.left; x<rc.right; x++) {

		  /* caulcuate distance, using implicate form of line. */
		  int d1 = (y0 - y1) * x + (x1 - x0) * y + (x0 * y1 - x1 * y0);
		  int d2 = (y1 - y2) * x + (x2 - x1) * y + (x1 * y2 - x2 * y1);
		  int d3 = (y2 - y0) * x + (x0 - x2) * y + (x2 * y0 - x0 * y2);

		  /* inverse if needed. */
		  if (d1 < 0 && d2 < 0 && d3 < 0) {
			d1 = -d1;
			d2 = -d2;
			d3 = -d3;
		  }

		  /* if pixel is in triangle. */
		  if (d1 >= 0 && d2 >= 0 && d3 >= 0) {

			/* prepare. */
			nblPoint p1,p2,p3,p;
			p.x=x; p.y=y;
			p1.x=x2; p1.y=y2;
			p2.x=x1; p2.y=y1;
			p3.x=x0; p3.y=y0;

			/* gouraud shading. A lot is happening here, but we just compute
			the area of a sub triangle and divide by the area of the entire triangle.
			We do this using the cross product and dividing it by 2. */
			{
				float alpha = (float) (((p2.y - p3.y)*(p.x - p3.x) - (p2.x - p3.x)*(p.y - p3.y))/2)
						  / (((p2.y - p3.y)*(p1.x - p3.x) + (p3.x - p2.x)*(p1.y - p3.y))/2);

				float beta = (float) (((p3.y - p1.y)*(p.x - p3.x) - (p3.x - p1.x)*(p.y - p3.y))/2)
						  / (((p2.y - p3.y)*(p1.x - p3.x) + (p3.x - p2.x)*(p1.y - p3.y))/2);

				float gamma = 1.0f - alpha - beta;

				/* now we compute output color and write the pixel. */
				{
					nblColor a=c1,b=c2,c=c3;
					nblColor d;

					// FIXME - Might not be correct? c1 not blending.
					// FIXME - Wrong colors in 16 bit mode. Related to above?

					d.red   = (a.red   * alpha) - (b.red   * alpha) - (c.red   * alpha);
					d.green = (a.green * beta)  - (b.green * beta)  - (c.green * beta);
					d.blue  = (a.blue  * gamma) - (b.blue  * gamma) - (c.blue  * gamma);
					d.alpha = 1;

					BlSetPixel(s,x,y,d);

				}
			}
			}
		}
	}
#endif
}

/* Following are video fill methods. */

/* Create RGB Surface. */

// 00ff0000

// 11111111 00000000 00000000

STATIC int BlLsb (uint32_t v) {

	unsigned pos = 0;

	if (!v)
		return 0;

	while (!(v & 1))
	{
		v >>= 1;
		++pos;
	}
	return pos;
/*
	unsigned i = 0;
	while (v >>= 1)
		i++;
	return i;
*/
}


status_t BlCreateSurface (OUT nblSurface* out, IN int width, IN int height, IN int bpp,
		IN uint32_t rmask, IN uint32_t gmask, IN uint32_t bmask, IN uint32_t amask)
{

	out->height     = height;
	out->width      = width;
	out->format.bpp = bpp;

	out->pitch = width * (bpp >> 3);
	out->format.bytesPerPixel = bpp >> 3;

	out->format.rmask = rmask;
	out->format.gmask = gmask;
	out->format.bmask = bmask;
	out->format.amask = amask;

	out->format.rshift = BlLsb (rmask);
	out->format.gshift = BlLsb (gmask);
	out->format.bshift = BlLsb (bmask);
	out->format.ashift = BlLsb (amask);

	return 0;
}


/* Optimized versions. */

void BlFillColor32 (IN nblSurface* s, IN int x, IN int y, IN int width, IN int height, nblColor c) {

	uint32_t rowskip;
	uint32_t* p;
	int i=0;
	int j=0;

	rowskip = s->pitch - s->format.bytesPerPixel * width;	
	p = (uint32_t*)s->pixels;
	p += ( (y * s->width) + x);

	for (i=0; i < height; i++) {
		for (j=0; j < width; j++) {

			uint32_t col = c.red << s->format.rshift
				| c.green << s->format.gshift
				| c.blue << s->format.bshift
				| c.alpha << s->format.ashift;

			*p++ = col;
		}
		p = (uint32_t*) (((char*)p) + rowskip);
	}
}

void BlFillColor24 (IN nblSurface* s, IN int x, IN int y, IN int width, IN int height, nblColor c) {

	uint32_t rowskip;
	uint8_t* p;
	int i=0;
	int j=0;

	rowskip = s->pitch - (s->format.bytesPerPixel * width);
	p = (uint8_t*)s->pixels;
	p += ( (y * s->width) + x) * 3;

	for (i=0; i < height; i++) {
		for (j=0; j < width; j++) {

			uint32_t col = c.red << s->format.rshift
				| c.green << s->format.gshift
				| c.blue << s->format.bshift;

			*p++ = c.blue;
			*p++ = c.green;
			*p++ = c.red;
		}
		p += rowskip;
	}
}

void BlFillColor16 (IN nblSurface* s, IN int x, IN int y, IN int width, IN int height, nblColor c) {

	uint32_t rowskip;
	uint16_t* p;
	int i=0;
	int j=0;

	rowskip = s->pitch - s->format.bytesPerPixel * width;
	p = (uint16_t*)s->pixels;

	p += ( (y * s->width) + x);

	for (i=0; i < height; i++) {
		for (j=0; j < width; j++) {

			uint32_t col = c.red << s->format.rshift
				| c.green << s->format.gshift
				| c.blue << s->format.bshift;

			*p++ = col;
		}
		p = (uint16_t*) (((char*)p) + rowskip);
	}
}

void BlDrawFillRect(IN nblSurface* s, IN nblRect rc, IN nblColor c) {

	int y = rc.top;
	int x = rc.left;
	int width = rc.right - rc.left;
	int height = rc.bottom - rc.top;

	/* if we can optimize this, do it. */
//	switch (s->format.bpp) {
//	case 16: BlFillColor16(s, x, y, width, height, c); return;
//	case 24: BlFillColor24(s, x, y, width, height, c); return;
//	case 32: BlFillColor32(s, x, y, width, height, c); return;
//	};

	/* fall back to slow method. */
	while (y <= rc.bottom) {
		while (x <= rc.right) {
			BlSetPixel(s, x, y, c);
			x++;
		}
		x = rc.left;
		y++;
	}
}

/* Implement Bit Block Transfer. */

void BlBitBlit (IN nblSurface* in, OUT nblSurface* out,
				int xdest,int ydest, int width, int height, int xsrc, int ysrc) {

	int x,y;
	for (y = 0; y < height; y++) {
		for (x = 0; x < width; x++) {
			nblColor c;
			/* Only set pixels that exist in the original image. */
			if (BlGetPixel (in, x + xsrc, y + ysrc, &c)) {
				BlSetPixel (out, x + xdest, y + ydest, c);
			}
		}
	}
}

/* Implement Bit block blend transfer. */

void BlBlendBlit (IN nblSurface* in, OUT nblSurface* out,
				  int xdest,int ydest, int width,int height,int xsrc, int ysrc)
{
	int x,y;

	for (y = 0; y < height; y++) {
		for (x = 0; x < width; x++) {

			nblColor src;
			nblColor dest;

			if (!BlGetPixel (in, x + xsrc, y + ysrc, &src))
				continue; /* no pixel here. */

			else if (src.alpha == 0)
				continue;

			else if (src.alpha == 255) {
				BlSetPixel (out, x + xdest, y + ydest, src);
				continue;
			}

			else if (!BlGetPixel(out, x + xsrc, y + ysrc, &dest))
				continue; /* no pixel here. */

			else{
				dest.red = (((src.red * src.alpha) + (dest.red   * (255 - src.alpha))) / 255);
				dest.green = (((src.green * src.alpha) + (dest.green * (255 - src.alpha))) / 255);
				dest.blue = (((src.blue * src.alpha) + (dest.blue  * (255 - src.alpha))) / 255);
				dest.alpha = src.alpha;

				BlSetPixel(out, x + xdest, y + ydest, dest);
			}
		}
	}
}
