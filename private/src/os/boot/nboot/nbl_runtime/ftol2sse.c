
long __declspec (naked) _ftol2_sse() {
	int a;
	_asm {
		fistp [a]
		mov	ebx, a
	}
}
