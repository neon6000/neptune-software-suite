/************************************************************************
*
*	core.c - Neptune Boot Library Core Services
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nbl.h"



/**
*	Returns root driver object.
*	\param in Pointer to driver object
*	\ret Pointer to root driver object
*/
NBL_API nblDriverObject* BlrDriverRoot (IN nblDriverObject* in) {

	nblDriverObject* current = in;

	ASSERT (in != NULL);

	while (current->node.parent)
		current = (nblDriverObject *) current->node.parent;

	return current;
}

/**
*	Root driver object.
*/
STATIC nblDriverObject* _rootDriverObject = NULL;

/**
*	Initialize NBLR.
*	This initializes NBLR for use with NBLC.
*	\param in Pointer to an NBLC allocated driver object
*/
NBL_API status_t BlrInitialize (IN nblDriverObject* in) {

	if (_rootDriverObject)
		return NBL_STAT_SUCCESS;

	if (!in)
		return NBL_STAT_INVALID_ARGUMENT;

	_rootDriverObject = BlrDriverRoot (in);

	return NBL_STAT_SUCCESS;
}

/**
*	Returns root driver object.
*	\ret Pointer to root driver object
*/
NBL_API nblDriverObject* BlrGetRootDriverObject (void) {

	return _rootDriverObject;
}

/**
*	Prints driver tree.
*/
PUBLIC void NBL_CALL BlrPrintDriverTree (IN nblDriverObject* cur, IN uint32_t ident) {

	char    name [NBL_DRIVER_NAME_MAX];
	index_t c;

	if (!cur)
		cur = _rootDriverObject;

	BlrPrintf("\n\r");
	for (c = 0; c < ident; c++)
		BlrPrintf("  ");
	BlrPrintf(cur->name);
	BlrPrintf(" magic: %x (%x) name: %x", cur->magic, NBL_DRIVER_MAGIC, cur->name);

	for (c = 0; c < cur->node.childCount; c++) {
		BlrPrintDriverTree (cur->node.children[c], ident+1);
	}
}

/**
*	Opens driver object.
*	\param path Path to driver object. Example "root/debug"
*	\param cur Pointer to driver object list
*	\ret Handle to driver object. A null handle indicates error.
*/
STATIC nblDriverObject* NBL_CALL _BlrOpenDriver (IN const char* path, IN nblDriverObject** cur) {

	char             name [NBL_DRIVER_NAME_MAX];
	unsigned int     c;
	char*            s;

	c = 0;
	s = (char*) path;

	/* copy over the name of the driver being requested. */
	BlrZeroMemory (name, NBL_DRIVER_NAME_MAX);
	while (*s != '/' && *s != 0) {
		name [c++] = *s++;
	}

	/* skip '/' */
	if (*s == '/')
		s++;

	/* not found. */
	if (!(*cur))
		return 0;

	/* if found, continue search in subtree. */
	if (BlrStrCompare (name, (*cur)->name) == 0) {

		/* no more subtrees to searh, this is the driver. */

		if (*s == 0) {
			return *cur;
		}

		/* search subtrees. */
		for (c = 0; c < (*cur)->node.childCount; c++) {
			nblDriverObject* result = _BlrOpenDriver (s, &((*cur)->node.children[c]));
			if ( result )
				return result;
		}
	}

	/* not found. */
	return 0;
}

/**
*	Opens driver object.
*	\param path Path to driver object
*	\ret Handle to driver object or device object. A null handle indicates error.
*/
NBL_API nblDriverObject* NBL_CALL BlrOpenDriver (IN const char* path) {

	nblDriverObject* root;
	char*            s;

	if (!path)
		return 0;

	root = _rootDriverObject;
	s    = (char*) path;
	return _BlrOpenDriver (s, &root);
}

/**
*	Set firmware environment variable.
*	\param name Variable name
*	\parm val Value
*	\ret Status
*/
NBL_API status_t NBL_CALL BlrSetEnvironmentVariable (IN char* name, IN char* val) {
	return NBL_STAT_SUCCESS;
}

/**
*	Get firmware environment variable.
*	\param name Variable name
*	\parm val Value
*	\ret Status
*/
NBL_API status_t NBL_CALL BlrGetEnvironmentVariable (IN char* name, OUT char* val) {
	return NBL_STAT_SUCCESS;
}

/**
*	Set time.
*	\param in Time to set
*	\ret Status
*/
NBL_API status_t NBL_CALL BlrSetTime (IN nblTime* in) {

	nblDeviceObject* rtc;
	nblMessage m;
	
	rtc = BlrOpenDevice ("rtc");
	if (!rtc)
		return NBL_STAT_DEVICE;

	m.type = NBL_RTC_SETTIME;
	m.NBL_RTC_SETTIME_DESCR = (uint32_t) in;

	if (rtc->driverObject->request (&m) != NBL_STAT_SUCCESS)
		return NBL_STAT_DRIVER;

	return NBL_STAT_SUCCESS;
}

/**
*	Get time.
*	\param out Pointer to time object to receive current time.
*	\ret Status
*/
NBL_API status_t NBL_CALL BlrGetTime (OUT nblTime* out) {

	nblDeviceObject* rtc;
	nblMessage m;
	
	rtc = BlrOpenDevice ("rtc");
	if (!rtc) {
		BlrPrintf("\n\rNo RTC device.");
		return NBL_STAT_DEVICE;
	}

	m.type = NBL_RTC_GETTIME;
	m.NBL_RTC_GETTIME_DESCR = (uint32_t) out;

	if (rtc->driverObject->request (&m) != NBL_STAT_SUCCESS)
		return NBL_STAT_DRIVER;

	return NBL_STAT_SUCCESS;
}

/**
*	Reset system.
*	\param reason Reason code.
*	\ret Status
*/
NBL_API status_t NBL_CALL BlrSystemReset (IN int reason) {
	return NBL_STAT_SUCCESS;
}

/**
*	Allocate pool at requested address.
*	\param base Base address of pool
*	\param length Length of pool
*	\ret Status
*/
NBL_API status_t NBL_CALL BlrAllocatePool (IN addr_t base, IN size_t length) {
	return NBL_STAT_SUCCESS;
}

/**
*	Release pool at requested address.
*	\param base Base address of pool
*	\param length Length of pool
*	\ret Status
*/
NBL_API status_t NBL_CALL BlrReleasePool (IN addr_t base, IN size_t length) {
	return NBL_STAT_SUCCESS;
}





/*

Page 74

<path> ::= <adapter>...<controller><peripheral>
[<partition>][<filepath>]
<adapter> ::= <adapter mnemonic>(<key>)
<controller> ::= <controller mnemonic>(<key>)
<peripheral> ::= <peripheral mnemonic>(<key>)
<partition> ::= partition([<number>])
<protocol> ::= tftp() | ripl() | console(0) | console(1)
<filepath> ::= <file system specific name>

adapter :

eisa(0)
tc(0)
scsi(0)
dti(0)
multi(0)

controller:

disk(0)
tape(0)
cdrom(0)
worm(0)
serial(0)
net(0)
video(0)
par(0)
point(0)
key(0)
audio(0)
other(0)

peripheral:

rdisk(0)
fdisk(0)
tape(0)
modem(0)
monitor(0)
print(0)
pointer(0)
keyboard(0)
term(0)
line(0)  -- communication ?
network(0)
other(0)

<protocol> ::= tftp() | ripl() | console(0) | console(1)

https://www.netbsd.org/docs/Hardware/Machines/ARC/riscspec.pdf

multi(0)disk(0)rdisk(0)partition(1)        = "Primary Channel, C:"
multi(0)disk(0)rdisk(0)partition(0)        = "Primary Channel, System Partition"
multi(0)disk(0)cdrom(%u)                   = "CD-ROM, Variable"
ramdisk(0)                                 = "RamDisk"
net(0)                                     = "Network"
multi(0)disk(0)fdisk(0)                    = "Diskette A:"
multi(0)disk(0)rdisk(1)partition(0)        = "Secondary Channel, Partition 0"
multi(0)disk(0)rdisk(1)partition(%d)       = "Secondary Channel, Variable Partition"
multi(0)disk(0)rdisk(1)partition(1)        = "Secondary Channel, Partition 0"
multi(0)disk(0)rdisk(0)partition(%d)       = "Primary Channel, Variable Partition"
multi(0)disk(0)fdisk(1)partition(0)        = "Diskette B:, Partition 0"
multi(0)disk(0)fdisk(0)partition(0)        = "Diskette A:, Partition 0"
multi(0)video(0)monitor(0)                 = "ConsoleOut, Monitor"
multi(0)key(0)keyboard(0)                  = "ConsoleIn, Keyboard"
multi(0)disk(0)fdisk(1)                    = "Diskette B:"
eisa(0)disk(0)fdisk(0)                     = "EISA Diskette A:"
eisa(0)disk(0)fdisk(1)partition(0)         = "EISA Diskette B:, Partition 0"
eisa(0)disk(0)fdisk(0)partition(0)         = "EISA Diskette A:, Partition 0"

Starts with MULTI or SCSI.

https://support.microsoft.com/en-us/kb/102873
http://www.dewassoc.com/kbase/multiboot/boot_ini.htm

*/

