/************************************************************************
*
*	key.h - NBL Virtual Key codes.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef NBL_KEY
#define NBL_KEY

typedef enum nblKey {

	NBL_VK_LBUTTON,
	NBL_VK_RBUTTON,
	NBL_VK_CANCEL,
	NBL_VK_MBUTTON,
	NBL_VK_XBUTTON1,
	NBL_VK_XBUTTON2,
	NBL_VK_BACK,
	NBL_VK_TAB,
	NBL_VK_CLEAR,
	NBL_VK_RETURN,
	NBL_VK_SHIFT,
	NBL_VK_CONTROL,
	NBL_VK_MENU,
	NBL_VK_PAUSE,
	NBL_VK_CAPITAL,
	NBL_VK_KANA,
	NBL_VK_HANGUEL,
	NBL_VK_HANGUL,
	NBL_VK_JUNJA,
	NBL_VK_FINAL,
	NBL_VK_HANJA,
	NBL_VK_KANJI,
	NBL_VK_ESCAPE,
	NBL_VK_CONVERT,
	NBL_VK_NONCONVERT,
	NBL_VK_ACCEPT,
	NBL_VK_MODELCHANGE,
	NBL_VK_SPACE,
	NBL_VK_PRIOR,
	NBL_VK_NEXT,
	NBL_VK_END,
	NBL_VK_HOME,
	NBL_VK_LEFT,
	NBL_VK_UP,
	NBL_VK_RIGHT,
	NBL_VK_DOWN,
	NBL_VK_SELECT,
	NBL_VK_PRINT,
	NBL_VK_EXECUTE,
	NBL_VK_SNAPSHOT,
	NBL_VK_INSERT,
	NBL_VK_DELETE,
	NBL_VK_HELP = 0x2F,
	/* 0x30 - 0x39 are keys 0 - 9. */
	NBL_VK_RESERVED = 0x40,
	/* 0x41 - 0x5a are keys A - Z. */
	NBL_VK_LWIN,
	NBL_VK_RWIN,
	NBL_VK_APPS,
	NBL_VK_SLEEP,
	NBL_VK_NUMPAD0,
	NBL_VK_NUMPAD1,
	NBL_VK_NUMPAD2,
	NBL_VK_NUMPAD3,
	NBL_VK_NUMPAD4,
	NBL_VK_NUMPAD5,
	NBL_VK_NUMPAD6,
	NBL_VK_NUMPAD7,
	NBL_VK_NUMPAD8,
	NBL_VK_NUMPAD9,
	NBL_VK_MULTIPLY,
	NBL_VK_ADD,
	NBL_VK_SEPARATOR,
	NBL_VK_SUBTRACT,
	NBL_VK_DECIMAL,
	NBL_VK_DIVIDE,
	NBL_VK_F1,
	NBL_VK_F2,
	NBL_VK_F3,
	NBL_VK_F4,
	NBL_VK_F5,
	NBL_VK_F6,
	NBL_VK_F7,
	NBL_VK_F8,
	NBL_VK_F9,
	NBL_VK_F10,
	NBL_VK_F11,
	NBL_VK_F12,
	NBL_VK_F13,
	NBL_VK_F14,
	NBL_VK_F15,
	NBL_VK_F16,
	NBL_VK_F17,
	NBL_VK_F18,
	NBL_VK_F19,
	NBL_VK_F20,
	NBL_VK_F21,
	NBL_VK_F22,
	NBL_VK_F23,
	NBL_VK_F24,
	NBL_VK_NUMLOCK,
	NBL_VK_SCROLL,
	NBL_VK_LSHIFT,
	NBL_VK_RSHIFT,
	NBL_VK_LCONTROL,
	NBL_VK_RCONTROL,
	NBL_VK_LMENU,
	NBL_VK_RMENU,
	NBL_VK_BROWSER_BACK,
	NBL_VK_BROWSER_FORWARD,
	NBL_VK_BROWSER_REFRESH,
	NBL_VK_BROWSER_STOP,
	NBL_VK_BROWSER_SEARCH,
	NBL_VK_BROWSER_FAVORITES,
	NBL_VK_BROWSER_HOME,
	NBL_VK_VOLUME_MUTE,
	NBL_VK_VOLUME_DOWN,
	NBL_VK_VOLUME_UP,
	NBL_VK_MEDIA_NEXT_TRACK,
	NBL_VK_MEDIA_PREV_TRACK,
	NBL_VK_MEDIA_STOP,
	NBL_VK_MEDIA_PLAY_PAUSE,
	NBL_VK_LAUNCH_MAIL,
	NBL_VK_LAUNCH_MEDIA_SELECT,
	NBL_VK_LAUNCH_APP1,
	NBL_VK_LAUNCH_APP2,
	NBL_VK_OEM1,				// ;: key
	NBL_VK_OEM_PLUS,
	NBL_VK_OEM_COMMA,
	NBL_VK_OEM_MINUS,
	NBL_VK_OEM_PERIOD,
	NBL_VK_OEM2,				// /? key
	NBL_VK_OEM3,				// ~` key
	NBL_VK_OEM4,				// [{ key
	NBL_VK_OEM5,				// \| key
	NBL_VK_OEM6,				// ]} key
	NBL_VK_OEM7,				// '" key
	NBL_VK_OEM8,				// others.
	NBL_VK_102,					// RT 102 keyboard angle bracket or backslash
	NBL_VK_PROCESSKEY,
	NBL_VK_ATTN,
	NBL_VK_CRSEL,
	NBL_VK_EXSEL,
	NBL_VK_EREOF,
	NBL_VK_PLAY,
	NBL_VK_ZOOM,
	NBL_VK_PA1,
	NBL_VK_OEM_CLEAR,
	NBL_VK_EMPTY = 0xff
}nblKey;

#endif

/*
	LoadKeyboardLayout
*/

// https://msdn.microsoft.com/en-us/library/windows/desktop/ms646305(v=vs.85).aspx


#if 0

	http://doxygen.reactos.org/dd/d83/kbdru1_8c_source.html#l00386

00001 /*
00002  * ReactOS Russian (Typewriter) Keyboard layout
00003  * Copyright (C) 2008 ReactOS
00004  * Author: Dmitry Chapyshev
00005  * License: LGPL, see: LGPL.txt
00006  *
00007  * Thanks to: http://www.barcodeman.com/altek/mule/scandoc.php
00008  * and http://win.tue.nl/~aeb/linux/kbd/scancodes-1.html
00009  */
00010 
00011 #define WIN32_NO_STATUS
00012 #include <stdarg.h>
00013 #include <windef.h>
00014 #include <winuser.h>
00015 #include <ndk/kbd.h>
00016 
00017 #ifdef _M_IA64
00018 #define ROSDATA static __declspec(allocate(".data"))
00019 #else
00020 #ifdef _MSC_VER
00021 #pragma data_seg(".data")
00022 #define ROSDATA static
00023 #else
00024 #define ROSDATA static __attribute__((section(".data")))
00025 #endif
00026 #endif
00027 
00028 #define VK_EMPTY  0xff   /* The non-existent VK */
00029 
00030 #define KNUMS     KBDNUMPAD|KBDSPECIAL /* Special + number pad */
00031 #define KMEXT     KBDEXT|KBDMULTIVK    /* Multi + ext */
00032 
00033 ROSDATA USHORT scancode_to_vk[] = {
00034   /* Numbers Row */
00035   /* - 00 - */
00036   /* 1 ...         2 ...         3 ...         4 ... */
00037   VK_EMPTY,     VK_ESCAPE,    '1',          '2',
00038   '3',          '4',          '5',          '6',
00039   '7',          '8',          '9',          '0',
00040   VK_OEM_MINUS, VK_OEM_PLUS,  VK_BACK,
00041   /* - 0f - */
00042   /* First Letters Row */
00043   VK_TAB,       'Q',          'W',          'E',
00044   'R',          'T',          'Y',          'U',
00045   'I',          'O',          'P',
00046   VK_OEM_4,     VK_OEM_6,     VK_RETURN,
00047   /* - 1d - */
00048   /* Second Letters Row */
00049   VK_LCONTROL,
00050   'A',          'S',          'D',          'F',
00051   'G',          'H',          'J',          'K',
00052   'L',          VK_OEM_1,     VK_OEM_7,     VK_OEM_3,
00053   VK_LSHIFT,    VK_OEM_5,
00054   /* - 2c - */
00055   /* Third letters row */
00056   'Z',          'X',          'C',          'V',
00057   'B',          'N',          'M',          VK_OEM_COMMA,
00058   VK_OEM_PERIOD,VK_OEM_2,     VK_RSHIFT | KBDEXT,
00059   /* - 37 - */
00060   /* Bottom Row */
00061   0x26a,  VK_LMENU,     VK_SPACE,     VK_CAPITAL,
00062 
00063   /* - 3b - */
00064   /* F-Keys */
00065   VK_F1, VK_F2, VK_F3, VK_F4, VK_F5, VK_F6,
00066   VK_F7, VK_F8, VK_F9, VK_F10,
00067   /* - 45 - */
00068   /* Locks */
00069   VK_NUMLOCK | KMEXT,
00070   VK_SCROLL | KBDMULTIVK,
00071   /* - 47 - */
00072   /* Number-Pad */
00073   VK_HOME | KNUMS,      VK_UP | KNUMS,         VK_PRIOR | KNUMS, VK_SUBTRACT,
00074   VK_LEFT | KNUMS,      VK_CLEAR | KNUMS,      VK_RIGHT | KNUMS, VK_ADD,
00075   VK_END | KNUMS,       VK_DOWN | KNUMS,       VK_NEXT | KNUMS,
00076   VK_INSERT | KNUMS,    VK_DELETE | KNUMS,
00077   /* - 54 - */
00078   /* Presumably PrtSc */
00079   VK_SNAPSHOT,
00080   /* - 55 - */
00081   /* Oddities, and the remaining standard F-Keys */
00082   VK_EMPTY,     VK_OEM_102,     VK_F11,       VK_F12,
00083   /* - 59 - */
00084   VK_CLEAR,     VK_OEM_WSCTRL,VK_OEM_FINISH,VK_OEM_JUMP,  VK_EREOF, /* EREOF */
00085   VK_OEM_BACKTAB,    VK_OEM_AUTO,  VK_EMPTY,    VK_ZOOM,            /* ZOOM */
00086   VK_HELP,
00087   /* - 64 - */
00088   /* Even more F-Keys (for example, NCR keyboards from the early 90's) */
00089   VK_F13, VK_F14, VK_F15, VK_F16, VK_F17, VK_F18, VK_F19, VK_F20,
00090   VK_F21, VK_F22, VK_F23,
00091   /* - 6f - */
00092   /* Not sure who uses these codes */
00093   VK_OEM_PA3, VK_EMPTY, VK_OEM_RESET,
00094   /* - 72 - */
00095   VK_EMPTY, 0xc1, VK_EMPTY, VK_EMPTY,
00096   /* - 76 - */
00097   /* One more f-key */
00098   VK_F24,
00099   /* - 77 - */
00100   VK_EMPTY, VK_EMPTY, VK_EMPTY, VK_EMPTY,
00101   VK_OEM_PA1, VK_TAB, 0xc2, 0, /* PA1 */
00102   0,
00103   /* - 80 - */
00104   0
00105 };
00106 
00107 ROSDATA VSC_VK extcode0_to_vk[] = {
00108   { 0x10, VK_MEDIA_PREV_TRACK | KBDEXT },
00109   { 0x19, VK_MEDIA_NEXT_TRACK | KBDEXT },
00110   { 0x1D, VK_RCONTROL | KBDEXT },
00111   { 0x20, VK_VOLUME_MUTE | KBDEXT },
00112   { 0x21, VK_LAUNCH_APP2 | KBDEXT },
00113   { 0x22, VK_MEDIA_PLAY_PAUSE | KBDEXT },
00114   { 0x24, VK_MEDIA_STOP | KBDEXT },
00115   { 0x2E, VK_VOLUME_DOWN | KBDEXT },
00116   { 0x30, VK_VOLUME_UP | KBDEXT },
00117   { 0x32, VK_BROWSER_HOME | KBDEXT },
00118   { 0x35, VK_DIVIDE | KBDEXT },
00119   { 0x37, VK_SNAPSHOT | KBDEXT },
00120   { 0x38, VK_RMENU | KBDEXT },
00121   { 0x47, VK_HOME | KBDEXT },
00122   { 0x48, VK_UP | KBDEXT },
00123   { 0x49, VK_PRIOR | KBDEXT },
00124   { 0x4B, VK_LEFT | KBDEXT },
00125   { 0x4D, VK_RIGHT | KBDEXT },
00126   { 0x4F, VK_END | KBDEXT },
00127   { 0x50, VK_DOWN | KBDEXT },
00128   { 0x51, VK_NEXT | KBDEXT },
00129   { 0x52, VK_INSERT | KBDEXT },
00130   { 0x53, VK_DELETE | KBDEXT },
00131   { 0x5B, VK_LWIN | KBDEXT },
00132   { 0x5C, VK_RWIN | KBDEXT },
00133   { 0x5D, VK_APPS | KBDEXT },
00134   { 0x5F, VK_SLEEP | KBDEXT },
00135   { 0x65, VK_BROWSER_SEARCH | KBDEXT },
00136   { 0x66, VK_BROWSER_FAVORITES | KBDEXT },
00137   { 0x67, VK_BROWSER_REFRESH | KBDEXT },
00138   { 0x68, VK_BROWSER_STOP | KBDEXT },
00139   { 0x69, VK_BROWSER_FORWARD | KBDEXT },
00140   { 0x6A, VK_BROWSER_BACK | KBDEXT },
00141   { 0x6B, VK_LAUNCH_APP1 | KBDEXT },
00142   { 0x6C, VK_LAUNCH_MAIL | KBDEXT },
00143   { 0x6D, VK_LAUNCH_MEDIA_SELECT | KBDEXT },
00144   { 0x1C, VK_RETURN | KBDEXT },
00145   { 0x46, VK_CANCEL | KBDEXT },
00146   { 0, 0 },
00147 };
00148 
00149 ROSDATA VSC_VK extcode1_to_vk[] = {
00150   { 0x1d, VK_PAUSE},
00151   { 0, 0 },
00152 };
00153 
00154 ROSDATA VK_TO_BIT modifier_keys[] = {
00155   { VK_SHIFT,   KBDSHIFT },
00156   { VK_CONTROL, KBDCTRL },
00157   { VK_MENU,    KBDALT },
00158   { 0,          0 }
00159 };
00160 
00161 ROSDATA MODIFIERS modifier_bits = {
00162   modifier_keys,
00163   3,
00164   { 0, 1, 2, 3 }
00165 };
00166 
00167 ROSDATA VK_TO_WCHARS2 key_to_chars_2mod[] = {
00168   {VK_OEM_3,       0, {'|',    '+' } },
00169   { '1',           0, {0x2116, '1' } },
00170   { '2',           0, {'-',    '2' } },
00171   { '3',           0, {'/',    '3' } },
00172   { '4',           0, {'\"',   '4' } },
00173   { '5',           0, {':',    '5' } },
00174   { '7',           0, {'.',    '7' } },
00175   { '8',           0, {'_',    '8' } },
00176   { '9',           0, {'?',    '9' } },
00177   { '0',           0, {'%',    '0' } },
00178   { VK_OEM_PLUS,   0, {';',    '\\'} },
00179 
00180   /* First letter row */
00181   { 'Q',           CAPLOK,   {0x439, 0x419} },
00182   { 'W',           CAPLOK,   {0x446, 0x426} },
00183   { 'E',           CAPLOK,   {0x443, 0x423} },
00184   { 'R',           CAPLOK,   {0x43a, 0x41a} },
00185   { 'T',           CAPLOK,   {0x435, 0x415} },
00186   { 'Y',           CAPLOK,   {0x43d, 0x41d} },
00187   { 'U',           CAPLOK,   {0x433, 0x413} },
00188   { 'I',           CAPLOK,   {0x448, 0x428} },
00189   { 'O',           CAPLOK,   {0x449, 0x429} },
00190   { 'P',           CAPLOK,   {0x437, 0x417} },
00191   { VK_OEM_4,      CAPLOK,   {0x445, 0x425} },
00192 
00193   /* Second letter row */
00194   { 'A',           CAPLOK,    {0x444, 0x424} },
00195   { 'S',           CAPLOK,    {0x44b, 0x42b} },
00196   { 'D',           CAPLOK,    {0x432, 0x412} },
00197   { 'F',           CAPLOK,    {0x430, 0x410} },
00198   { 'G',           CAPLOK,    {0x43f, 0x41f} },
00199   { 'H',           CAPLOK,    {0x440, 0x420} },
00200   { 'J',           CAPLOK,    {0x43e, 0x41e} },
00201   { 'K',           CAPLOK,    {0x43b, 0x41b} },
00202   { 'L',           CAPLOK,    {0x434, 0x414} },
00203   { VK_OEM_7,      CAPLOK,    {0x44d, 0x42d} },
00204 
00205   /* Third letter row */
00206   { 'Z',           CAPLOK,    {0x44f, 0x42f} },
00207   { 'X',           CAPLOK,    {0x447, 0x427} },
00208   { 'C',           CAPLOK,    {0x441, 0x421} },
00209   { 'V',           CAPLOK,    {0x43c, 0x41c} },
00210   { 'B',           CAPLOK,    {0x438, 0x418} },
00211   { 'N',           CAPLOK,    {0x442, 0x422} },
00212   { 'M',           CAPLOK,    {0x44c, 0x42c} },
00213   { VK_OEM_COMMA,  CAPLOK,    {0x431, 0x411} },
00214   { VK_OEM_PERIOD, CAPLOK,    {0x44e, 0x42e} },
00215   { VK_OEM_2,      CAPLOK,    {0x451, 0x401} },
00216 
00217   /* Specials */
00218   { 0x6e,          0, {',', ','} },
00219   { VK_TAB,        0, {9,   9  } },
00220   { VK_ADD,        0, {'+', '+'} },
00221   { VK_DIVIDE,     0, {'/', '/'} },
00222   { VK_MULTIPLY,   0, {'*', '*'} },
00223   { VK_SUBTRACT,   0, {'-', '-'} },
00224   { 0, 0 }
00225 };
00226 
00227 ROSDATA VK_TO_WCHARS3 key_to_chars_3mod[] = {
00228   /* Normal, Shifted, Ctrl */
00229   /* Legacy (telnet-style) ascii escapes */
00230   { VK_OEM_MINUS, 0, {'!',   '=',   0x1f} },
00231   { VK_OEM_6,     CAPLOK,   {0x44a, 0x42a, 0x1b} },
00232   { VK_OEM_5,     0, {')',   '(',   0x1c} },
00233   { VK_OEM_1,     CAPLOK,   {0x436, 0x416, 0x1d} },
00234   { VK_OEM_102,   0, {')',   '(',   0x1c} },
00235   { VK_BACK,      0, {0x8,   0x8,   0x7f} },
00236   { VK_ESCAPE,    0, {0x1b,  0x1b,  0x1b} },
00237   { VK_RETURN,    0, {'\r',  '\r',  '\n'} },
00238   { VK_SPACE,     0, {' ',   ' ',   ' ' } },
00239   { VK_CANCEL,    0, {0x03,  0x03,  0x03} },
00240   { 0, 0 }
00241 };
00242 
00243 ROSDATA VK_TO_WCHARS4 key_to_chars_4mod[] = {
00244   /* Normal, Shifted, Ctrl, Ctrl-Alt */
00245   /* Legacy Ascii generators */
00246   { '6', 0, {',', '6', WCH_NONE, 0x001e} },
00247   { 0, 0 }
00248 };
00249 
00250 ROSDATA VK_TO_WCHARS1 keypad_numbers[] = {
00251   { VK_NUMPAD0, 0, {'0'} },
00252   { VK_NUMPAD1, 0, {'1'} },
00253   { VK_NUMPAD2, 0, {'2'} },
00254   { VK_NUMPAD3, 0, {'3'} },
00255   { VK_NUMPAD4, 0, {'4'} },
00256   { VK_NUMPAD5, 0, {'5'} },
00257   { VK_NUMPAD6, 0, {'6'} },
00258   { VK_NUMPAD7, 0, {'7'} },
00259   { VK_NUMPAD8, 0, {'8'} },
00260   { VK_NUMPAD9, 0, {'9'} },
00261   { 0, 0 }
00262 };
00263 
00264 #define vk_master(n,x) { (PVK_TO_WCHARS1)x, n, sizeof(x[0]) }
00265 
00266 ROSDATA VK_TO_WCHAR_TABLE vk_to_wchar_master_table[] = {
00267   vk_master(3,key_to_chars_3mod),
00268   vk_master(4,key_to_chars_4mod),
00269   vk_master(2,key_to_chars_2mod),
00270   vk_master(1,keypad_numbers),
00271   { 0,0,0 }
00272 };
00273 
00274 ROSDATA VSC_LPWSTR key_names[] = {
00275   { 0x01, L"Esc" },
00276   { 0x0e, L"Backspace" },
00277   { 0x0f, L"Tab" },
00278   { 0x1c, L"Enter" },
00279   { 0x1d, L"Ctrl" },
00280   { 0x2a, L"Shift" },
00281   { 0x36, L"Right Shift" },
00282   { 0x37, L"Num *" },
00283   { 0x38, L"Alt" },
00284   { 0x39, L"Space" },
00285   { 0x3a, L"CAPLOK Lock" },
00286   { 0x3b, L"F1" },
00287   { 0x3c, L"F2" },
00288   { 0x3d, L"F3" },
00289   { 0x3e, L"F4" },
00290   { 0x3f, L"F5" },
00291   { 0x40, L"F6" },
00292   { 0x41, L"F7" },
00293   { 0x42, L"F8" },
00294   { 0x43, L"F9" },
00295   { 0x44, L"F10" },
00296   { 0x45, L"Pause" },
00297   { 0x46, L"Scroll Lock" },
00298   { 0x47, L"Num 7" },
00299   { 0x48, L"Num 8" },
00300   { 0x49, L"Num 9" },
00301   { 0x4a, L"Num -" },
00302   { 0x4b, L"Num 4" },
00303   { 0x4c, L"Num 5" },
00304   { 0x4d, L"Num 6" },
00305   { 0x4e, L"Num +" },
00306   { 0x4f, L"Num 1" },
00307   { 0x50, L"Num 2" },
00308   { 0x51, L"Num 3" },
00309   { 0x52, L"Num 0" },
00310   { 0x53, L"Num Del" },
00311   { 0x54, L"Sys Req" },
00312   { 0x57, L"F11" },
00313   { 0x58, L"F12" },
00314   { 0x7c, L"F13" },
00315   { 0x7d, L"F14" },
00316   { 0x7e, L"F15" },
00317   { 0x7f, L"F16" },
00318   { 0x80, L"F17" },
00319   { 0x81, L"F18" },
00320   { 0x82, L"F19" },
00321   { 0x83, L"F20" },
00322   { 0x84, L"F21" },
00323   { 0x85, L"F22" },
00324   { 0x86, L"F23" },
00325   { 0x87, L"F24" },
00326   { 0, NULL }
00327 };
00328 
00329 ROSDATA VSC_LPWSTR extended_key_names[] = {
00330   { 0x1c, L"Num Enter" },
00331   { 0x1d, L"Right Control" },
00332   { 0x35, L"Num /" },
00333   { 0x37, L"Prnt Scrn" },
00334   { 0x38, L"Right Alt" },
00335   { 0x45, L"Num Lock" },
00336   { 0x46, L"Break" },
00337   { 0x47, L"Home" },
00338   { 0x48, L"Up" },
00339   { 0x49, L"Page Up" },
00340   { 0x4b, L"Left" },
00341 //{ 0x4c, L"Center" },
00342   { 0x4d, L"Right" },
00343   { 0x4f, L"End" },
00344   { 0x50, L"Down" },
00345   { 0x51, L"Page Down" },
00346   { 0x52, L"Insert" },
00347   { 0x53, L"Delete" },
00348   { 0x54, L"<ReactOS>" },
00349   { 0x56, L"Help" },
00350   { 0x5b, L"Left <ReactOS>" },
00351   { 0x5c, L"Right <ReactOS>" },
00352   { 0x5d, L"Application" },
00353   { 0, NULL }
00354 };
00355 
00356 /* Finally, the master table */
00357 ROSDATA KBDTABLES keyboard_layout_table = {
00358   /* modifier assignments */
00359   &modifier_bits,
00360 
00361   /* character from vk tables */
00362   vk_to_wchar_master_table,
00363 
00364   /* diacritical marks */
00365   NULL,
00366 
00367   /* Key names */
00368   (VSC_LPWSTR *)key_names,
00369   (VSC_LPWSTR *)extended_key_names,
00370   NULL, /* Dead key names */
00371 
00372   /* scan code to virtual key maps */
00373   scancode_to_vk,
00374   sizeof(scancode_to_vk) / sizeof(scancode_to_vk[0]),
00375   extcode0_to_vk,
00376   extcode1_to_vk,
00377 
00378   MAKELONG(0, 1), /* Version 1.0 */
00379 
00380   /* Ligatures -- Russian doesn't have any */
00381   0,
00382   0,
00383   NULL
00384 };
00385 
00386 PKBDTABLES WINAPI KbdLayerDescriptor(VOID) {
00387   return &keyboard_layout_table;
00388 }
00389 
00390 INT WINAPI
00391 DllMain(
00392   PVOID hinstDll,
00393   ULONG dwReason,
00394   PVOID reserved)
00395 {
00396   return 1;
00397 }
00398 

#endif
