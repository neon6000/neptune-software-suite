
#include <nbl.h>

/*================================================
	File system driver requests.
================================================*/

status_t BlrFsAttach (IN nblDeviceObject* dev, IN nblDriverObject* fs) {
	nblMessage m;

	m.source  = 0;
	m.type    = NBL_FS_ATTACH;
	m.NBL_FS_ATTACH_DEVICE = dev;
	fs->request (&m);

	return 0;
}

nblFile* BlrFsOpen (IN nblDeviceObject* volume, IN char* fname) {
	nblMessage m;

	if (!volume->attached)
		return 0;

	m.source = 0;
	m.type = NBL_FS_OPEN;
	m.NBL_FS_OPEN_PATH = (unsigned) fname;
	m.NBL_FS_OPEN_DEVICE = volume;
	volume->attached->request(&m);
	return (nblFile*) m.retval;
}

size_t BlrFsRead(IN nblFile* file, IN size_t size, OUT void* buffer) {
	nblMessage m;
	if (!file)
		return 0;
	if (!buffer || !size)
		return 0; /* nothing to read. */
	if (!file->driver)
		return 0; /* file not associated with driver. */
	m.source = 0;
	m.type = NBL_FS_READ;
	m.NBL_FS_READ_FILE = (unsigned) file;
	m.NBL_FS_READ_SIZE = size;
	m.NBL_FS_READ_BUFFER = (unsigned int) buffer;
	file->driver->request(&m);
	return (size_t) m.retval;
}

size_t BlrFsWrite (IN nblFile* file, IN size_t size, OUT void* buffer) {
	nblMessage m;
	if (!file)
		return 0;
	if (!buffer || !size)
		return 0; /* nothing to read. */
	if (!file->driver)
		return 0; /* file not associated with driver. */
	m.source = 0;
	m.type = NBL_FS_WRITE;
	m.NBL_FS_WRITE_FILE = (unsigned) file;
	m.NBL_FS_WRITE_SIZE = size;
	m.NBL_FS_WRITE_BUFFER = (unsigned int) buffer;
	file->driver->request(&m);
	return (size_t) m.retval;
}

status_t BlrFsClose (IN nblFile* file) {
	return 0;
}

/* file methods. */

// http://www.opensource.apple.com/source/Libc/Libc-167/string.subproj/strtok.c

char* BlrStrToken (IN char* s, IN const char* delim) {
	register char *spanp;
	register int c, sc;
	char *tok;
	static char *last;

	if (s == NULL && (s = last) == NULL)
		return (NULL);

	/*
	 * Skip (span) leading delimiters (s += strspn(s, delim), sort of).
	 */
cont:
	c = *s++;
	for (spanp = (char *)delim; (sc = *spanp++) != 0;) {
		if (c == sc)
			goto cont;
	}

	if (c == 0) {		/* no non-delimiter characters */
		last = NULL;
		return (NULL);
	}
	tok = s - 1;

	/*
	 * Scan token (scan for delimiters: s += strcspn(s, delim), sort of).
	 * Note that delim must have one NUL; we stop if we see that, too.
	 */
	for (;;) {
		c = *s++;
		spanp = (char *)delim;
		do {
			if ((sc = *spanp++) == c) {
				if (c == 0)
					s = NULL;
				else
					s[-1] = 0;
				last = s;
				return (tok);
			}
		} while (sc != 0);
	}
	/* NOTREACHED */
}

BOOL BlrFileEnd (IN nblFile* in) {
	if (!in)
		return TRUE;
	else if (in->currentOffset >= in->size) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

char* BlrFileGetString (IN char* str, IN int num, IN nblFile* stream) {

	if (BlrFileEnd (stream))
		return NULL;
	while (num--) {
		if (!BlrFsRead (stream, 1, str))
			return NULL;
		if (*str == '\n')
			break;
		if (stream->currentOffset >= stream->size)
			break;
		str++;
	}
	*str = 0;
	return str;
}

uint64_t BlrFileTell (IN nblFile* stream) {
	return stream->currentOffset;
}

int BlrFileSeek (IN nblFile* stream, IN uint64_t offset, IN int origin) {
	uint64_t position = 0;
	switch(origin) {
		case NBL_SEEK_SET:
			position = offset;
			break;
		case NBL_SEEK_CUR:
			position = stream->currentOffset + offset;
			break;
		default:
			return 1;
	};
	if (position >= stream->size)
		return 0;
	stream->currentOffset = position;
	return 1;
}

char* BlrStrDup (IN char* s) {
	unsigned int length;
	char* r;
	if (!s)
		return NULL;
	length = BlrStrLength (s)+1;
	r = BlrAlloc (length);
	if (!r)
		return NULL;
	BlrCopyMemory (r, s, length);
	return r;
}

nblFile* BlrFileOpen (IN char* path) {

	nblFile* ret;
	nblDeviceObject* volume;
	char* tmp;
	char* dev;

	tmp = BlrStrDup (path);
	if (tmp == NULL)
		return NULL;

	dev = BlrStrToken (tmp, ":");
	ret = NULL;

	volume = BlrOpenDevice (dev);
	if (volume) {
		char* file = BlrStrToken (NULL, ":");
		ret = BlrFsOpen(volume, file);
	}

	BlrFree (tmp);
	return ret;
}

status_t BlrFileReopen (IN char* fname, IN nblFile* stream) {

	nblFile* file = BlrFileOpen(fname);
	if (!file) {
		return NBL_STAT_INVALID_ARGUMENT;
	}

	if (file == stream) {
		return NBL_STAT_SUCCESS;
	}

	if (stream) {
		if (stream->name) BlrFree (stream->name);
		if (stream->ext)  BlrFree (stream->ext);

		BlrCopyMemory(stream, file, sizeof(nblFile));
		BlrFree (file);
		return NBL_STAT_SUCCESS;
	}

	return NBL_STAT_DRIVER;
}

int BlrStrToInt ( const char *c ) {
    int value = 0;
    int sign = 1;
    if( *c == '+' || *c == '-' ) {
       if( *c == '-' ) sign = -1;
       c++;
    }
    while (*c >= '0' && *c <= '9' ) {
        value *= 10;
        value += (int) (*c-'0');
        c++;
    }
    return value * sign;
}

/*================================================
	Disk driver requests.
================================================*/

uint32_t BlrDiskBiosName (IN nblDeviceObject* dev) {

	/* call disk->BiosName (dev) */
	nblMessage m;
	m.source = 0;
	m.type = NBL_DISK_BIOSNAME;
	m.NBL_DISK_BIOSNAME_DEV = dev;
	dev->driverObject->request (&m);
	return m.retval;
}

size_t BlrDiskRead (IN nblDeviceObject* dev, IN size_t sector, IN size_t off, IN size_t size, OUT void* buffer) {

	/* call disk->read (dev, sector, size, buffer) */
	nblMessage m;

	m.source = 0;
	m.type = NBL_DISK_READ;
	m.NBL_DISK_READ_SIZE = size;
	m.NBL_DISK_READ_SECTOR = sector;
	m.NBL_DISK_READ_BUFFER = (unsigned) buffer;
	m.NBL_DISK_READ_DEV = dev;
	m.NBL_DISK_READ_OFFSET = off;
	dev->driverObject->request (&m);
	return 0;
}

size_t BlrDiskWrite (IN nblDeviceObject* dev, IN size_t sector, IN size_t off, IN size_t size, OUT void* buffer) {

	/* call disk->write (dev, sector, size, buffer) */
	nblMessage m;
	m.source = 0;
	m.type = NBL_DISK_WRITE;
	m.NBL_DISK_WRITE_SIZE = size;
	m.NBL_DISK_WRITE_SECTOR = sector;
	m.NBL_DISK_WRITE_BUFFER = (unsigned) buffer;
	m.NBL_DISK_WRITE_DEV = dev;
	m.NBL_DISK_WRITE_OFFSET = off;
	dev->driverObject->request (&m);
	return 0;
}

size_t BlrGetMemoryMap(IN void* memory) {

	nblMessage m;
	nblDriverObject* fw;

	fw = BlrOpenDriver("root/dm/mm");
	if (!fw)
		return 0;
	m.source = 0;
	m.type = NBL_MM_GETMAP;
	m.NBL_MM_GETMAP_ADDRESS = memory;
	fw->request (&m);
	return (size_t) m.retval;
	return 0;
}

size_t BlrExit (IN status_t status) {

	nblMessage m;
	nblDriverObject* fw;

	m.type = NBL_FW_EXIT;
	m.NBL_FW_EXIT_STATUS = status;

	fw = BlrOpenDriver("root/fw");
	if (fw)
		fw->request (&m);
	return (size_t) m.retval;
}

size_t BlrReset (IN status_t status) {

	nblMessage m;
	nblDriverObject* fw;

	m.type = NBL_FW_RESET;
	m.NBL_FW_EXIT_STATUS = status;
	m.retval = 0;

	fw = BlrOpenDriver("root/fw");
	if (fw)
		fw->request (&m);
	return (size_t) m.retval;
}

size_t BlrPrints (IN char* in) {

	nblMessage m;

	if (!nbl_stdout)
		return 0;

	m.type = NBL_CONOUT_PRINT;
	m.NBL_CONOUT_PRINT_STR = (uint32_t) in;
	m.retval = 0;

	if (nbl_stdout->driver) {
		nbl_stdout->driver->request (&m);
	}
	return (size_t) m.retval;
}

status_t BlrTerminalInputReset (IN nblDeviceObject* dev) {

	nblMessage m;
	if (!dev)
		return NBL_STAT_INVALID_ARGUMENT;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONIN_RESET;
	dev->driverObject->request (&m);
	return NBL_STAT_SUCCESS;
}

uint32_t BlrTerminalInputReadKey (IN nblDeviceObject* dev) {

	nblMessage m;
	if (!dev)
		return NBL_STAT_DEVICE;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONIN_READKEY;
	dev->driverObject->request (&m);
	return (uint32_t) m.retval;
}

status_t BlrTerminalOutputReset (IN nblDeviceObject* dev) {

	nblMessage m;
	if (!dev)
		return NBL_STAT_INVALID_ARGUMENT;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONOUT_RESET;
	dev->driverObject->request (&m);
	return NBL_STAT_SUCCESS;
}

status_t BlrTerminalOutputEnableCursor (IN nblDeviceObject* dev, IN BOOL enable) {

	nblMessage m;
	if (!dev)
		return NBL_STAT_INVALID_ARGUMENT;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONOUT_CURSOR;
	m.NBL_CONOUT_CURSOR_ENABLE = (uint32_t) enable;
	return dev->driverObject->request (&m);
}

BOOL BlrTerminalOutputGetCursor (IN nblDeviceObject* dev) {

	nblMessage m;
	if (!dev)
		return NBL_STAT_INVALID_ARGUMENT;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONOUT_GETCURSOR;
	return dev->driverObject->request (&m);
}

uint32_t BlrTerminalOutputGetPosition (IN nblDeviceObject* dev) {

	nblMessage m;
	if (!dev)
		return NBL_STAT_INVALID_ARGUMENT;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONOUT_GETXY;
	return dev->driverObject->request (&m);
}

status_t BlrTerminalOutputSetPosition (IN nblDeviceObject* dev, IN uint32_t x, IN uint32_t y) {

	nblMessage m;

	if (!dev)
		return NBL_STAT_INVALID_ARGUMENT;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONOUT_SETXY;
	m.NBL_CONOUT_SET_X = x;
	m.NBL_CONOUT_SET_Y = y;
	return dev->driverObject->request (&m);
}

status_t BlrTerminalOutputClearScreen (IN nblDeviceObject* dev, IN uint32_t fcol, IN uint32_t bcol) {

	nblMessage m;
	if (!dev)
		return NBL_STAT_INVALID_ARGUMENT;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONOUT_CLRSCR;
	m.NBL_CONOUT_CLRSCR_FCOLOR = fcol;
	m.NBL_CONOUT_CLRSCR_BCOLOR = bcol;
	return dev->driverObject->request (&m);
}

uint32_t BlrTerminalOutputSetAttribute (IN nblDeviceObject* dev, IN uint32_t fcol, IN uint32_t bcol) {

	nblMessage m;
	if (!dev)
		return NBL_STAT_INVALID_ARGUMENT;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONOUT_ATTRIB;
	m.NBL_CONOUT_ATTRIB_FCOLOR = fcol;
	m.NBL_CONOUT_ATTRIB_BCOLOR = bcol;
	return dev->driverObject->request (&m);
}

uint32_t BlrTerminalOutputGetAttribute (IN nblDeviceObject* dev) {

	nblMessage m;
	if (!dev)
		return NBL_STAT_INVALID_ARGUMENT;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONOUT_GETATTRIB;
	return dev->driverObject->request (&m);
}

status_t BlrTerminalOutputSetMode (IN nblDeviceObject* dev, IN uint32_t mode) {

	nblMessage m;
	if (!dev)
		return NBL_STAT_INVALID_ARGUMENT;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONOUT_SETMODE;
	m.NBL_CONOUT_SETMODE_MODE = mode;
	return dev->driverObject->request (&m);
}

uint32_t BlrTerminalOutputGetMode (IN nblDeviceObject* dev) {

	nblMessage m;
	if (!dev)
		return NBL_STAT_INVALID_ARGUMENT;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONOUT_GETMODE;
	return dev->driverObject->request (&m);
}

uint32_t BlrTerminalPrint (IN nblDeviceObject* dev, IN char* s) {

	nblMessage m;
	if (!dev)
		return NBL_STAT_INVALID_ARGUMENT;

	/* must be terminal device. */
	if (dev->type != NBL_DEVICE_TERMINAL_PERIPHERAL)
		return NBL_STAT_INVALID_ARGUMENT;

	m.source = 0;
	m.type = NBL_CONOUT_PRINT;
	m.NBL_CONOUT_PRINT_STR = (uint32_t) s;
	return dev->driverObject->request (&m);
}

status_t BlrCreateSurface (IN OUT nblSurface* desc) {

	nblMessage m;

	m.type = NBL_VIDEO_CREATESURFACE;
	m.NBL_VIDEO_CREATESURFACE_IN = (uint32_t) desc;
	return desc->device->driverObject->request(&m);
}

status_t BlrGetPalette (IN nblSurface* desc, IN nblPalette* palette) {

	nblMessage m;

	m.type = NBL_VIDEO_GETPALETTE;
	m.NBL_VIDEO_GETPALETTE_OUT  = (uint32_t) palette;
	return desc->device->driverObject->request(&m);
}

status_t BlrSetPalette (IN nblSurface* desc, IN nblPalette* palette) {

	nblMessage m;
	
	m.type = NBL_VIDEO_SETPALETTE;
	m.NBL_VIDEO_SETPALETTE_IN  = (uint32_t) palette;
	return desc->device->driverObject->request(&m);
}

status_t BlrQueryVideoModes (IN nblSurface* desc, NBL_QUERY_MODE_CALLBACK* callback, void* ex) {

	nblMessage m;

	m.type = NBL_VIDEO_QUERY;
	m.NBL_VIDEO_QUERY_CALL  = (uint32_t) callback;
	m.NBL_VIDEO_QUERY_EX = (uint32_t) ex;
	return desc->device->driverObject->request(&m);
}

status_t BlrGetVideoMode (IN nblSurface* desc, OUT nblVideoMode* out) {

	nblMessage m;
	
	m.type = NBL_VIDEO_GETMODE;
	m.NBL_VIDEO_GETMODE_OUT  = (uint32_t) out;
	return desc->device->driverObject->request(&m);
}

status_t BlrSetVideoMode (IN nblSurface* desc, uint32_t width, uint32_t height, uint32_t depth) {

	nblMessage m;
	
	m.type = NBL_VIDEO_SETMODE;
	m.NBL_VIDEO_SETMODE_WIDTH  = width;
	m.NBL_VIDEO_SETMODE_HEIGHT = height;
	m.NBL_VIDEO_SETMODE_DEPTH  = depth;
	return desc->device->driverObject->request(&m);
}

uint32_t BlrSerialRead (IN uint32_t size, OUT void* buffer) {

	nblMessage m;
	nblDeviceObject* dev;

	dev = BlrOpenDevice("serial");
	if (!dev)
		return 0;

	m.type = NBL_SERIAL_READ;
	m.NBL_SERIAL_READ_SIZE = size;
	m.NBL_SERIAL_READ_BUFFER  = (uint32_t) buffer;
	return dev->driverObject->request(&m);
}

uint32_t BlrSerialWrite (IN uint32_t size, IN void* buffer) {

	nblMessage m;
	nblDeviceObject* dev;

	dev = BlrOpenDevice("serial");
	if (!dev) {
		BlrPrintf("\n\rNo Serial device.");
		return 0;
	}

	m.type = NBL_SERIAL_WRITE;
	m.NBL_SERIAL_WRITE_SIZE = size;
	m.NBL_SERIAL_WRITE_BUFFER  = (uint32_t) buffer;
	return dev->driverObject->request(&m);
}

status_t BlrChainload (char* device_path) {

	nblMessage m;
	nblDriverObject* drv;

	drv = BlrOpenDriver("root/chainloader");
	if (!drv)
		return NBL_STAT_DRIVER;

	if (!drv->request)
		return NBL_STAT_DRIVER;

	m.type = NBL_CHAINLOADER_BOOT;
	m.NBL_CHAINLOADER_BOOT_DEVICE = (uint32_t) device_path;
	return drv->request (&m);
}

status_t BlrBoot (IN nblDriverObject* loader, IN char* device_path) {

	nblMessage m;

	if (!loader)
		return NBL_STAT_INVALID_ARGUMENT;

	if (!loader->request)
		return NBL_STAT_DRIVER;

	m.type = NBL_LOADER_BOOT;
	m.NBL_LOADER_BOOT_DEVICE = (uint32_t) device_path;
	return loader->request (&m);
}


nblKeyTable* BlrGetKeyboardMap (IN nblDriverObject* mapper) {

	nblMessage m;
	m.type = NBL_KBDMAP_GET;
	if (mapper->request (&m) == NBL_STAT_SUCCESS)
		return (nblKeyTable*) m.retval;
	return NULL;
}

/* allocates firmware memory. To allocate from heap, use BlrAlloc instead. */
void* BlrAllocatePages (IN addr_t phys, IN uint32_t pages) {

	nblMessage m;
	nblDriverObject* mm;

	mm = BlrOpenDriver("root/mm");
	if (!mm)
		return NULL;

	m.type = NBL_MM_ALLOC;
	m.NBL_MM_ALLOC_ADDRESS = phys;
	m.NBL_MM_ALLOC_SIZE = pages;
	return mm->request (&m);
}

void BlrRtcInstall(void) {

	nblMessage m;
	nblDriverObject* mm;

	mm = BlrOpenDriver("root/dm/rtc");
	BlrPrintf("\n\rBlrRtcInstall: driver = %x", mm);
	if (!mm)
		return NULL;

	m.type = NBL_RTC_INSTALL;
	return mm->request (&m);
}

/* frees firmware memory. To free from heap, use BlrFree instead. */
void BlrFreePages (IN addr_t phys, IN uint32_t pages) {

	nblMessage m;
	nblDriverObject* mm;

	mm = BlrOpenDriver("root/mm");
	if (!mm)
		return NULL;

	m.type = NBL_MM_FREE;
	m.NBL_MM_FREE_ADDRESS = phys;
	m.NBL_MM_FREE_SIZE = pages;
	return mm->request (&m);
}
