/************************************************************************
*
*	vargs.c - NBLR Variable Argument List Support
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>

#define ZEROPAD 1               // Pad with zero
#define SIGN    2               // Unsigned/signed long
#define PLUS    4               // Show plus
#define SPACE   8               // Space if plus
#define LEFT    16              // Left justified
#define SPECIAL 32              // 0x
#define LARGE   64              // Use 'ABCDEF' instead of 'abcdef'

STATIC char* _digits = "0123456789abcdefghijklmnopqrstuvwxyz";
STATIC char* _upper_digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

STATIC wchar_t* _digits_wide = L"0123456789abcdefghijklmnopqrstuvwxyz";
STATIC wchar_t* _upper_digits_wide = L"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

STATIC wchar_t* BlParseNumberWide(IN wchar_t* str, IN long long num, IN int base, IN int type) {

	wchar_t temp[66];
	wchar_t* digits;
	wchar_t sign;
	int i;

	if (base < 2 || base > 36)
		return 0;

	if (type & LARGE)
		digits = _upper_digits_wide;
	else
		digits = _digits_wide;

	sign = 0;
	if (type & SIGN) {
		if (num < 0) {
			num = -num;
			sign = L'-';
		}
		else if (type & PLUS) {
			sign = L'+';
		}
		else if (type & SPACE) {
			sign = L' ';
		}
	}

	i = 0;
	if (num == 0) {
		temp[i++] = L'0';
	}
	else {

		while (num != 0) {

			temp[i++] = _digits_wide[((unsigned long long) num) % (unsigned)base];
			num = ((unsigned long long) num) / (unsigned)base;
		}
	}

	if (sign)
		*str++ = sign;

	if (base == 8) {
		*str++ = L'0';
	}
	else if (base == 16) {
		*str++ = L'0';
		*str++ = digits[33];
	}

	while (i-- > 0)
		*str++ = temp[i];

	return str;
}

/* parse number to string. */
STATIC char* BlParseNumber(IN char* str, IN long long num, IN int base, IN int type) {

	char temp[66];
	char* digits;
	char sign;
	int i;

	if (base < 2 || base > 36)
		return 0;

	if(type & LARGE)
		digits=_upper_digits;
	else
		digits=_digits;

	sign = 0;
	if (type & SIGN) {
		if (num < 0) {
			num = -num;
			sign = '-';
		}
		else if (type & PLUS) {
			sign = '+';
		}
		else if (type & SPACE) {
			sign = ' ';
		}
	}

	i = 0;
	if (num == 0) {
		temp[i++] = '0';
	} else {

		while (num != 0) {

			temp[i++] = digits[((unsigned long long) num) % (unsigned) base];
			num = ((unsigned long long) num) / (unsigned) base;
		}
	}

	if  (sign)
		*str++ = sign;

	if (base == 8) {
		*str++ = '0';
	}
	else if (base == 16) {
		*str++ = '0';
		*str++ = digits [33];
	}

	while (i-- > 0)
		*str++ = temp[i];

	return str;
}

/* length modifiers. */
#define QUALIFIER_SHORT       1
#define QUALIFIER_LONG        2
#define QUALIFIER_LONG_DOUBLE 3
#define QUALIFIER_LONG_LONG   4

/* writes formatted string to buf. Similar to vsprintf. */
int NBL_CALL BlrWriteVaListToString (IN char* buf,IN const char* format,IN va_list arg) {

	int len;
	unsigned long long num;
	int i, base;
	char *str;
	char *s;

	int flags;            // Flags to number()
	int qualifier;        // 'h', 'l', or 'L' for integer fields

	num = 0;
	for (str = buf; *format; format++) {

		if (*format != '%') {
			*str++ = *format;
			continue;
		}

		/* set flags. */
		flags = 0;
next:
		format++;
		switch(*format) {
			case '-': flags |= LEFT; goto next;
			case '+': flags |= PLUS; goto next;
			case ' ': flags |= SPACE; goto next;
			case '#': flags |= SPECIAL; goto next;
			case '0': flags |= ZEROPAD; goto next;
		}

		/* conversion qualifier. */
		qualifier = 0;
		switch (*format) {
			case 'h':
				qualifier = QUALIFIER_SHORT;
				format++;
				break;
			case 'l':
				qualifier = QUALIFIER_LONG;
				format++;
				break;
			case 'L':
				qualifier = QUALIFIER_LONG_DOUBLE;
				format++;
				break;
		};
		switch (*format) {
			case 'l':
				qualifier = QUALIFIER_LONG_LONG;
				format++;
				break;
		};

		/* default base is decimal. */
		base = 10;

		switch (*format) {

			case 'c':
				*str++ = (unsigned char) NBL_VA_ARG(arg,int);
				continue;

			case 's':
				s = NBL_VA_ARG(arg,char*);
				if (!s)
					s = "<null>";
				len = BlrStrLength(s);
				for (i = 0; i < len; ++i)
					*str++ = *s++;
				continue;

			case 'p':
				str = BlParseNumber(str, (long) NBL_VA_ARG(arg, void*), 16, flags);
				continue;

			case 'n':
				if (qualifier == QUALIFIER_LONG) {
					long* ip = NBL_VA_ARG(arg, long*);
					*ip = (str - buf);
				}else{
					int* ip = NBL_VA_ARG(arg, int*);
					*ip = (str - buf);
				}
				continue;

			case 'A':	// hexidecimal upper/lower case,floating point
				flags |= LARGE;
			case 'a':
				str++;
				continue;

			case 'o':
				base = 8;
				break;

			case 'X':
				flags |= LARGE;
			case 'x':
				base = 16;
				break;

			case 'd':
			case 'i':
				flags |= SIGN;
			case 'u':
				break;

			default:
				if (*format != '%')
					*str++ = '%';
				if (*format)
					*str++ = *format;
				else
					--format;
				continue;
		}

		if (qualifier == QUALIFIER_LONG) {
			if (flags & SIGN)
				num = NBL_VA_ARG(arg,long);
			else
				num = NBL_VA_ARG(arg,unsigned long);
		}
		else if (qualifier == QUALIFIER_SHORT) {
			if (flags & SIGN)
				num = NBL_VA_ARG(arg,short);
			else
				num = NBL_VA_ARG(arg,unsigned short);
		}
		else if (qualifier == QUALIFIER_LONG_LONG) {
			if (flags & SIGN)
				num = NBL_VA_ARG(arg,long long);
			else
				num = NBL_VA_ARG(arg,unsigned long long);
		}
		else {
			if (flags & SIGN)
				num = NBL_VA_ARG(arg,int);
			else
				num =  NBL_VA_ARG(arg,unsigned int);
		}

		str = BlParseNumber(str,num,base,flags);
	}

	*str = NULL;
	return str - buf;
}

int NBL_CALL BlrWriteVaListToWideString(IN wchar_t* buf, IN const wchar_t* format, IN va_list arg) {

	int len;
	unsigned long long num;
	int i, base;
	wchar_t *str;
	wchar_t *s;

	int flags;            // Flags to number()
	int qualifier;        // 'h', 'l', or 'L' for integer fields

	num = 0;
	for (str = buf; *format; format++) {

		if (*format != '%') {
			*str++ = *format;
			continue;
		}
		flags = 0;
	next:
		format++;
		switch (*format) {
		case '-': flags |= LEFT; goto next;
		case '+': flags |= PLUS; goto next;
		case ' ': flags |= SPACE; goto next;
		case '#': flags |= SPECIAL; goto next;
		case '0': flags |= ZEROPAD; goto next;
		}

		/* default base. */
		base = 10;

		/* conversion qualifier. */
		qualifier = 0;
		switch (*format) {
		case 'h':
			qualifier = QUALIFIER_SHORT;
			format++;
			break;
		case 'l':
			qualifier = QUALIFIER_LONG;
			format++;
			break;
		case 'L':
			qualifier = QUALIFIER_LONG_DOUBLE;
			format++;
			break;
		};
		switch (*format) {
		case 'l':
			qualifier = QUALIFIER_LONG_LONG;
			format++;
			break;
		};

		switch (*format) {

		case 'c':
			*str++ = (unsigned char)NBL_VA_ARG(arg, int);
			continue;
		case 's':
			s = NBL_VA_ARG(arg, wchar_t*);
			if (!s)
				s = "<null>";
			len = BlrWideStrLength(s);
			for (i = 0; i < len; ++i)
				*str++ = *s++;
			continue;
		case 'n':
			if (qualifier == QUALIFIER_LONG) {
				long* ip = NBL_VA_ARG(arg, long*);
				*ip = (str - buf);
			}
			else{
				int* ip = NBL_VA_ARG(arg, int*);
				*ip = (str - buf);
			}

		case 'A':	// hexidecimal upper/lower case,floating point
			flags |= LARGE;
		case 'a':
			str++;
			continue;

		case 'o':
			base = 8;
			break;

		case 'X':
			flags |= LARGE;
		case 'x':
			base = 16;
			break;

		case 'd':
		case 'i':
			flags |= SIGN;
		case 'u':
			break;

		default:
			if (*format != '%')
				*str++ = '%';
			if (*format)
				*str++ = *format;
			else
				--format;
			continue;
		}

		if (qualifier == QUALIFIER_LONG) {
			if (flags & SIGN)
				num = NBL_VA_ARG(arg, long);
			else
				num = NBL_VA_ARG(arg, unsigned long);
		}
		else if (qualifier == QUALIFIER_SHORT) {
			if (flags & SIGN)
				num = NBL_VA_ARG(arg, short);
			else
				num = NBL_VA_ARG(arg, unsigned short);
		}
		else if (qualifier == QUALIFIER_LONG_LONG) {
			if (flags & SIGN)
				num = NBL_VA_ARG(arg, long long);
			else
				num = NBL_VA_ARG(arg, unsigned long long);
		}
		else {
			if (flags & SIGN)
				num = NBL_VA_ARG(arg, int);
			else
				num = NBL_VA_ARG(arg, unsigned int);
		}

		str = BlParseNumberWide(str, num, base, flags);
	}

	*str = NULL;
	return 0;
}

/* writes formatted string to str. Similar to sprintf. */
int NBL_CALL BlrWriteFormattedString (IN char* str, IN const char* format, IN ...) {

	va_list args;
	int length;

	NBL_VA_START (args, format);
	length = BlrWriteVaListToString (str,format,args);
	NBL_VA_END (args);
	return length;
}

/* writes formatted string to str. Similar to sprintf. */
int NBL_CALL BlrWriteFormattedWideString(IN wchar_t* str, IN const wchar_t* format, IN ...) {

	va_list args;
	int length;

	NBL_VA_START(args, format);
	length = BlrWriteVaListToWideString(str, format, args);
	NBL_VA_END(args);
	return length;
}

/*** scanf ***/

INLINE int BlrIsUpper(char c) {
    return (c >= 'A' && c <= 'Z');
}

INLINE int BlrIsAlpha(char c) {
    return ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'));
}

INLINE int BlrIsSpace(char c) {
    return (c == ' ' || c == '\t' || c == '\n' || c == '\12');
}

INLINE int BlrIsDigit(char c) {
    return (c >= '0' && c <= '9');
}

#define LONG_MIN ((long) 0x80000000L)
#define LONG_MAX 0xFFFFFFFFUL

// http://www.opensource.apple.com/source/xnu/xnu-1456.1.26/bsd/libkern/strtol.c

long NBL_CALL BlrStrToLong (IN const char *nptr, IN char **endptr, IN int base) {

	register const char *s = nptr;
	register unsigned long acc;
	register int c;
	register unsigned long cutoff;
	register int neg = 0, any, cutlim;

	/*
	 * Skip white space and pick up leading +/- sign if any.
	 * If base is 0, allow 0x for hex and 0 for octal, else
	 * assume decimal; if base is already 16, allow 0x.
	 */
	do {
		c = *s++;
	} while (BlrIsSpace(c));
	if (c == '-') {
		neg = 1;
		c = *s++;
	} else if (c == '+')
		c = *s++;
	if ((base == 0 || base == 16) &&
	    c == '0' && (*s == 'x' || *s == 'X')) {
		c = s[1];
		s += 2;
		base = 16;
	} else if ((base == 0 || base == 2) &&
	    c == '0' && (*s == 'b' || *s == 'B')) {
		c = s[1];
		s += 2;
		base = 2;
	}
	if (base == 0)
		base = c == '0' ? 8 : 10;

	/*
	 * Compute the cutoff value between legal numbers and illegal
	 * numbers.  That is the largest legal value, divided by the
	 * base.  An input number that is greater than this value, if
	 * followed by a legal input character, is too big.  One that
	 * is equal to this value may be valid or not; the limit
	 * between valid and invalid numbers is then based on the last
	 * digit.  For instance, if the range for longs is
	 * [-2147483648..2147483647] and the input base is 10,
	 * cutoff will be set to 214748364 and cutlim to either
	 * 7 (neg==0) or 8 (neg==1), meaning that if we have accumulated
	 * a value > 214748364, or equal but the next digit is > 7 (or 8),
	 * the number is too big, and we will return a range error.
	 *
	 * Set any if any `digits' consumed; make it negative to indicate
	 * overflow.
	 */
	cutoff = neg ? -(unsigned long)LONG_MIN : LONG_MAX;
	cutlim = cutoff % (unsigned long)base;
	cutoff /= (unsigned long)base;
	for (acc = 0, any = 0;; c = *s++) {
		if (BlrIsDigit(c))
			c -= '0';
		else if (BlrIsAlpha(c))
			c -= BlrIsUpper(c) ? 'A' - 10 : 'a' - 10;
		else
			break;
		if (c >= base)
			break;
		if (any < 0 || acc > cutoff || acc == cutoff && c > cutlim)
			any = -1;
		else {
			any = 1;
			acc *= base;
			acc += c;
		}
	}

	if (any < 0) {
		acc = neg ? LONG_MIN : LONG_MAX;
	} else if (neg)
		acc = -acc;

	if (endptr != 0)
		*endptr = (char *)(any ? s - 1 : nptr);

	return (acc);
}

/* reads formatted data from string into variableargument list. Similar to sprintf. */
int NBL_CALL BlrWriteStringToVaList (IN const char* buf, IN const char* format,IN va_list arg) {

	int      base;
	uint32_t length;
	char*    str;
	int      flags;            // Flags to number()
	int      qualifier;        // 'h', 'l', or 'L' for integer fields
	BOOL     hasLength;

	flags = 0;
	qualifier = 0;

	for (str = buf; *format; format++) {

		/*skip over whitespace. */
		if (BlrIsSpace(*format)) {
			continue;
		}

		/* only supporting format specifiers. */
		if (*format != '%') {
			break;
		}

		/* set flags. */
		flags = 0;
		hasLength = FALSE;
		length = 0;
next:
		format++;
		switch(*format) {
			case '-': flags |= LEFT; goto next;
			case '+': flags |= PLUS; goto next;
			case ' ': flags |= SPACE; goto next;
			case '#': flags |= SPECIAL; goto next;
			case '0': flags |= ZEROPAD; goto next;
		}

		/* conversion qualifier. */
		qualifier = 0;
		switch (*format) {
			case 'h':
				qualifier = QUALIFIER_SHORT;
				format++;
				break;
			case 'l':
				qualifier = QUALIFIER_LONG;
				format++;
				break;
			case 'L':
				qualifier = QUALIFIER_LONG_DOUBLE;
				format++;
				break;
		};
		switch (*format) {
			case 'l':
				qualifier = QUALIFIER_LONG_LONG;
				format++;
				break;
		};

		/* start of next token. */
		while (BlrIsSpace(*str)) {
			str++;
		}

		/* decimal.*/
		base = 10;

		/* watch length field. */
		if (*format >= '0' && *format <= '9') {
			int k=0;
			char buf[20];
			while (*format) {
				if (*format < '0' || *format > '9')
					break;
				buf[k++] = *format++;
			}
			buf[k] = 0;
			length = BlrStrToLong(buf,0,10);
			hasLength = TRUE;
		}

		switch (*format) {

			case 'c':
				*(char*) NBL_VA_ARG(arg,char*) = *str++;
				continue;

			case 's': {
				char* s = (char*) NBL_VA_ARG(arg,char*);
				while(*str) {

					/* make sure not to exceed length. */
					if (hasLength) {
						if (length-- == 0) {
							break;
						}
					}

					if (BlrIsSpace(*str))
						break;
					*s++ = *str++;
				}
				*s = NULL;
				}
				continue;

			case 'p': {
				int k=0;
				char buf[20];
				while (*str) {
					if (BlrIsSpace(*str))
						break;
					buf[k++] = *str++;
				}
				buf[k] = 0;
				*(unsigned long*) NBL_VA_ARG(arg,unsigned long) = BlrStrToLong(buf,0,16);
				}
				continue;

			case 'n':
				if (qualifier == QUALIFIER_LONG) {
					long* ip = NBL_VA_ARG(arg, long*);
					*ip = (str - buf);
				}else{
					int* ip = NBL_VA_ARG(arg, int*);
					*ip = (str - buf);
				}
				continue;

			case 'A':
			case 'a':
				continue;

			case 'o':
				base = 8;
				break;

			case 'X':
				flags |= LARGE;
			case 'x':
				base = 16;
				break;

			case 'd':
			case 'i':
				flags |= SIGN;
			case 'u':
				break;

			default:
				continue;
		}

		if (qualifier == QUALIFIER_LONG) {
			int k=0;
			char buf[20];
			while (*str) {
				if (BlrIsSpace(*str))
					break;
				buf[k++] = *str++;
			}
			buf[k] = 0;
			*(unsigned long*) NBL_VA_ARG(arg,unsigned long) = BlrStrToLong(buf,0,base);
		}
		else if (qualifier == QUALIFIER_SHORT) {
			int k=0;
			char buf[20];
			while (*str) {
				if (BlrIsSpace(*str))
					break;
				buf[k++] = *str++;
			}
			buf[k] = 0;

			if (flags & SIGN)
				*(short*) NBL_VA_ARG(arg,short) = (short) BlrStrToLong(buf,0,base);
			else
				*(unsigned short*) NBL_VA_ARG(arg,unsigned short) = (unsigned short) BlrStrToLong(buf,0,base);
		}
		else {
			int k=0;
			char buf[20];
			while (*str) {
				if (BlrIsSpace(*str))
					break;
				buf[k++] = *str++;
			}
			buf[k] = 0;
			*(int*) NBL_VA_ARG(arg,int) = BlrStrToLong(buf,0,base);
		}
	}

	return str - buf;
}
