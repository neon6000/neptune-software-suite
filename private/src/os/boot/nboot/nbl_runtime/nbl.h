/************************************************************************
*
*	Neptune Boot Library Core Services
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef NBL_H
#define NBL_H

#include "key.h"

typedef unsigned short wchar_t;
typedef unsigned short wint_t;
typedef unsigned short wctype_t;

/**
*	Basic types.
*/
typedef unsigned int       handle_t;
typedef unsigned int       size_t;
typedef unsigned int       index_t;
typedef unsigned int       addr_t;
typedef unsigned int       status_t;
typedef unsigned long long uint64_t;
typedef long long          int64_t;
typedef signed long long   sint64_t;
typedef unsigned int       BOOL;
typedef BOOL               bool_t;
typedef unsigned int       uint32_t;
typedef int                int32_t;
typedef signed int         sint32_t;
typedef unsigned short     uint16_t;
typedef signed short       sint16_t;
typedef short              int16_t;
typedef unsigned char      uint8_t;
typedef signed char        sint8_t;
typedef char               int8_t;
typedef char               char8_t;
typedef wchar_t            char16_t;
typedef signed char        sint8_t;
typedef signed short       sint16_t;
typedef signed int         sint32_t;
typedef signed long long   sint64_t;

/* access types. */
#define PRIVATE
#define PUBLIC

/* variable argument list type. */
typedef char* va_list;

/* driver and device objects. */
struct nblDriverObject;
struct nblDeviceObject;

/* driver and device objects. */
typedef struct _nblDriverObject nblDriverObject;
typedef struct _nblDeviceObject nblDeviceObject;

/* exit callback. */
typedef void (*NBL_EXIT_FUNC)(void);

/* mm init callback. */
typedef status_t (*NBL_MM_INIT_FUNC) (addr_t base, unsigned long size, int zone);

/* Loader Paramater Block. */
typedef struct _nblLPB {
	char*         root;
	char*         cmd;
	char*         initrd;
	size_t        modcount;
	char**        modules;
}nblLPB;

/**
*	I/O Types.
*/
#define NBL_IO_TYPE_ADAPTER                 1
#define NBL_IO_TYPE_CONTROLLER              2
#define NBL_IO_TYPE_DEVICE                  3
#define NBL_IO_TYPE_DRIVER                  4
#define NBL_IO_TYPE_FILE                    5
#define NBL_IO_TYPE_IRP                     6
#define NBL_IO_TYPE_MASTER_ADAPTER          7
#define NBL_IO_TYPE_OPEN_PACKET             8
#define NBL_IO_TYPE_TIMER                   9
#define NBL_IO_TYPE_VPB                     10
#define NBL_IO_TYPE_ERROR_LOG               11
#define NBL_IO_TYPE_ERROR_MESSAGE           12
#define NBL_IO_TYPE_DEVICE_OBJECT_EXTENSION 13

/* driver load points. */

#define NBL_DRV_LOAD_CORE		0			/* statically linked core driver. */
#define NBL_DRV_LOAD_CORE2		1			/* dynamically linked core driver. */
#define NBL_DRV_LOAD_MODULE		2			/* external driver. */

/* Driver load point. */
typedef unsigned int nblDriverLoadType;

/**
*	Driver types.
*/
#define NBL_DRV_CLASS_ROOT   0		/* Root */
#define NBL_DRV_CLASS_DLL    1		/* DLL */
#define NBL_DRV_CLASS_FW     2		/* Firmware */
#define NBL_DRV_CLASS_MM     3		/* Memory Manager */
#define NBL_DRV_CLASS_DM     4		/* Device Manager */
#define NBL_DRV_CLASS_VM     5		/* Volume Manager */
#define NBL_DRV_CLASS_CON    6		/* Console Manager */
#define NBL_DRV_CLASS_DBG    7		/* Debugger */
#define NBL_DRV_CLASS_NET    8		/* Network */
#define NBL_DRV_CLASS_FS     9		/* File system */
#define NBL_DRV_CLASS_BLOCK  10		/* Block device */
#define NBL_DRV_CLASS_CHAR   11		/* Character device */
#define NBL_DRV_CLASS_VIDEO  12		/* Video device */
#define NBL_DRV_CLASS_LOADER 13     /* Loader. */
#define NBL_DRV_CLASS_OTHER  14		/* Other */

/* Driver type. */
typedef unsigned int nblDriverType;

typedef enum gfxColor {
	BLACK = 0,
	BLUE,
	GREEN,
	CYAN,
	RED,
	MAGENTA,
	BROWN,
	LGRAY,
	GRAY,
	LBLUE,
	LGREEN,
	LCYAN,
	LRED,
	LMAGENTA,
	YELLOW,
	WHITE
}gfxColor;

typedef struct nblLoaderParameterBlock {
	char*  kernelPath;
	char*  commandLine;
	char*  modulePath[];
}nblLoaderParameterBlock;

typedef enum nblVkFlag {
	NBL_KEY_EX,
	NBL_KEY_MULTI,
	NBL_KEY_SPECIAL,
	NBL_KEY_NUMPAD
}nblVkFlag;

typedef enum nblVkModifier {
	NBL_KEY_SHIFT    = 1,
	NBL_KEY_CONTROL  = 2,
	NBL_KEY_ALT
}nblVkModifier;

typedef struct nblVkCharMap {
	nblKey  vk;
	uint8_t attributes;
	/*
		wch[0] => normal
		wch[1] => shift
		wch[2] => control
		wch[3] => special
	*/
	wchar_t wch[4];
}nblVkCharMap;

typedef struct nblVscStr {
	uint8_t vsc;
	wchar_t* str;
}nblVscStr;

/* maps virtual scan codes to virtual key codes. */
typedef struct nblKeyTable {

	/* vk to char map. */
	nblVkCharMap*    keyMap;

	/* key names. */
	nblVscStr*       keyNames;
	nblVscStr*       keyNamesEx;

	/* scan code to vk maps. */
	nblKey*          vscToVkMap;

}nblKeyTable;

/* 2d point. */
typedef struct nblPoint {
	int x, y;
}nblPoint;

/* color. */
typedef struct nblColor {
	uint8_t red, green, blue, alpha;
}nblColor;

/* rectangle. */
typedef struct nblRect {
	int top,bottom,left,right;
}nblRect;

/* color palette. */
typedef struct nblPalette {
	nblColor* colors;
	int count;
}nblPalette;

/* pixel format. */
typedef struct nblPixelFormat {
	nblPalette* palette;
	uint8_t  bpp;
	uint8_t  bytesPerPixel;
	uint32_t rmask;
	uint32_t gmask;
	uint32_t bmask;
	uint32_t amask;
	uint8_t  rshift;
	uint8_t  gshift;
	uint8_t  bshift;
	uint8_t  ashift;
	nblColor front;
	nblColor back;
}nblPixelFormat;

/* surface capabilities. */
typedef enum nblSurfaceCaps {
	NBL_SCAPS_PRIMARYSURFACE = (1<<0),
	NBL_SCAPS_BACKBUFFER     = (1<<1),
	NBL_SCAPS_PALETTE        = (1<<2),
	NBL_SCAPS_FRONTBUFFER    = (1<<3),
	NBL_SCAPS_ALPHA          = (1<<4),
	NBL_SCAPS_FLIP           = (1<<5),
	NBL_SCAPS_VIDEOMEMORY    = (1<<6),
	NBL_SCAPS_SYSTEMMEMORY   = (1<<7),
	NBL_SCAPS_READONLY       = (1<<8),
	NBL_SCAPS_WRITEONLY      = (1<<9)
}nblSurfaceCaps;

/* surface flags. */
typedef enum nblSurfaceFlags {
	NBL_SFLAGS_CAPS          = (1<<0),
	NBL_SFLAGS_WIDTH         = (1<<1),
	NBL_SFLAGS_HEIGHT        = (1<<2),
	NBL_SFLAGS_PITCH         = (1<<3),
	NBL_SFLAGS_DEPTH         = (1<<4),
	NBL_SFLAGS_PIXELS        = (1<<5),
	NBL_SFLAGS_CLIP          = (1<<6)
}nblSurfaceFlags;

/* image surface. */
typedef struct nblSurface {
	nblPixelFormat format;
	nblSurfaceFlags flags;
	nblSurfaceCaps caps;
	int      width;
	int      height;
	int      pitch;
	void*    pixels;
	nblRect  clip;
	struct nblSurface* attached;
	nblDeviceObject* device;
	void*    ex;
}nblSurface;

/* video mode. */
typedef struct nblVideoMode {
	nblPixelFormat format;
	uint32_t mode;
	uint32_t colors;
	uint32_t width;
	uint32_t height;
	uint32_t pitch;
}nblVideoMode;

/* video mode iteration. */
typedef BOOL (*NBL_QUERY_MODE_CALLBACK) (nblVideoMode* out, void* ex);

/**
* Device classes.
*/
typedef enum _nblComponentClass {
	NBL_DEVICE_SYSTEM_CLASS = 0,
	NBL_DEVICE_PROCESSOR_CLASS,
	NBL_DEVICE_CACHE_CLASS,
	NBL_DEVICE_ADAPTER_CLASS,
	NBL_DEVICE_CONTROLLER_CLASS,
	NBL_DEVICE_PERIPHERAL_CLASS,
	NBL_DEVICE_MEMORY_CLASS
}nblComponentClass;

/**
* Device types.
*/
typedef enum _nblComponentType {
	NBL_DEVICE_CPU = 1,
	NBL_DEVICE_FPU,
	NBL_DEVICE_PRIMARY_ICACHE,
	NBL_DEVICE_PRIMARY_DCACHE,
	NBL_DEVICE_SECONDARY_ICACHE,
	NBL_DEVICE_SECONDARY_DCACHE,
	NBL_DEVICE_SECONDARY_CACHE,
	NBL_DEVICE_EISA_ADAPTER,
	NBL_DEVICE_TCA_ADAPTER,
	NBL_DEVICE_SCSI_ADAPTER,
	NBL_DEVICE_DTI_ADAPTER,
	NBL_DEVICE_MULTI_FUNCTION_ADAPTER,
	NBL_DEVICE_DISK_CONTROLLER,
	NBL_DEVICE_TNBL_DEVICE_APE_CONTROLLER,
	NBL_DEVICE_WORM_CONTROLLER,
	NBL_DEVICE_SERIAL_CONTROLLER,
	NBL_DEVICE_NETWORK_CONTROLLER,
	NBL_DEVICE_DISPLAY_CONTROLLER,
	NBL_DEVICE_PARALLEL_CONTROLLER,
	NBL_DEVICE_POINTER_CONTROLLER,
	NBL_DEVICE_KEYBOARD_CONTROLLER,
	NBL_DEVICE_AUDIO_CONTROLLER,
	NBL_DEVICE_OTHER_CONTROLLER,
	NBL_DEVICE_DISK_PERIPHERAL,
	NBL_DEVICE_FLOPPY_DISK_PERIPHERAL,
	NBL_DEVICE_TAPE_PERIPHERAL,
	NBL_DEVICE_MODEM_PERIPHERAL,
	NBL_DEVICE_MONITOR_PERIPHERAL,
	NBL_DEVICE_PRINTER_PERIPHERAL,
	NBL_DEVICE_POINTER_PERIPHERAL,
	NBL_DEVICE_KEYBOARD_PERIPHERAL,
	NBL_DEVICE_TERMINAL_PERIPHERAL,
	NBL_DEVICE_OTHER_PERIPHERAL,
	NBL_DEVICE_LINE_PERIPHERAL,
	NBL_DEVICE_NETWORK_PERIPHERAL,
	NBL_DEVICE_MEMORY_UNIT
}nblComponentType;

/**
* Device characteristics.
*/
typedef enum _nblComponentFlag {
	NBL_DEVICE_FAILED      = 1,
	NBL_DEVICE_READ_ONLY   = 2,
	NBL_DEVICE_REMOVABLE   = 4,
	NBL_DEVICE_CONSOLE_IN  = 8,
	NBL_DEVICE_CONSOLE_OUT = 16,
	NBL_DEVICE_INPUT       = 32,
	NBL_DEVICE_OUTPUT      = 64,
	NBL_DEVICE_MOUNTED     = 128
}nblComponentFlag;

/* Device type. */
typedef unsigned int nblDeviceType;

/* memory types. */
typedef enum _nblMemoryType {
	NBL_MEMORY_HOLE,
	NBL_MEMORY_CODE,
	NBL_MEMORY_AVAILABLE,
	NBL_MEMORY_RESERVED,
	NBL_MEMORY_ACPI_RECLAIM,
	NBL_MEMORY_ACPI_NVS
}nblMemoryType;

/* memory descriptor. */
typedef struct _nblMemoryDescriptor {
	uint64_t      phys;
	uint64_t      virt;
	uint64_t      size;
	nblMemoryType type;
	handle_t      handle;
	struct _nblMemoryDescriptor* next;
}nblMemoryDescriptor;


/*===========================================================
	Message format.
===========================================================*/

/* Message formats. */
typedef struct {unsigned m1i1, m1i2, m1i3, m1i4, m1i5, m1i6;  nblDeviceObject* m1d1; } msg1;

/* return value type. */
typedef uint64_t rettype_t;

/* NBL Message type. */
typedef struct _nblMessage {
    nblDriverObject* source;
	int              type;
	rettype_t        retval;
	union {
       msg1 m1;
	}u;
}nblMessage;

/* accessors. */

#define m1_i1 u.m1.m1i1
#define m1_i2 u.m1.m1i2
#define m1_i3 u.m1.m1i3
#define m1_i4 u.m1.m1i4
#define m1_i5 u.m1.m1i5
#define m1_i6 u.m1.m1i6
#define m1_d1 u.m1.m1d1

typedef int (*NBL_DRV_ITERATE_HOOK_FUNC) (nblDriverObject*, void*);

/*===========================================================
	Device Manager.
===========================================================*/

typedef int (*NBL_DEV_ITERATE_HOOK_FUNC) (nblDeviceObject*, void*);

#define NBL_DEV_REGISTER         0
# define NBL_DEV_REGISTER_OBJ    m1_d1
# define NBL_DEV_REGISTER_STAT   m1_i2

#define NBL_DEV_UNREGISTER       1
# define NBL_DEV_UNREGISTER_OBJ  m1_d1

#define NBL_DEV_ITERATE          2
# define NBL_DEV_ITERATE_TYPE    m1_i1
# define NBL_DEV_ITERATE_HOOK    m1_i2
# define NBL_DEV_ITERATE_EX      m1_i3

#define NBL_DEV_OPEN             3
# define NBL_DEV_OPEN_NAME       m1_i1
# define NBL_DEV_OPEN_OUT        m1_i2

#define NBL_DEV_CLOSE            4
# define NBL_DEV_CLOSE_OBJ       m1_d1

/*===========================================================
	Debugger.
===========================================================*/

#define NBL_DBG_BREAK           0

/*===========================================================
	File System.
===========================================================*/

/* The driver is responsible for providing methods to parse
file systems from a volume device object. Since the file object
is a generic device object, it can be used by multiple drivers.
Therefore, message ID's are large. */

/* nblFile* fs_open (IN char* fileName, IN nblVPB* volume) */
#define NBL_FS_OPEN            0x100
# define NBL_FS_OPEN_PATH      m1_i1
# define NBL_FS_OPEN_DEVICE    m1_d1

/* bytes_read fs_read (IN blFile* file, IN size_t size, OUT void* buffer) */
#define NBL_FS_READ            0x200
# define NBL_FS_READ_FILE      m1_i1
# define NBL_FS_READ_SIZE      m1_i2
# define NBL_FS_READ_BUFFER    m1_i3

/* bytes_written fs_write (IN blFile* file, IN size_t size, OUT void* buffer) */
#define NBL_FS_WRITE           0x300
# define NBL_FS_WRITE_FILE     m1_i1
# define NBL_FS_WRITE_SIZE     m1_i2
# define NBL_FS_WRITE_BUFFER   m1_i3

/* status_t fs_close (IN blFile*) */
#define NBL_FS_CLOSE           0x400
# define NBL_FS_CLOSE_FILE     m1_i1

/* status_t fs_mount (IN nblDeviceObject* dev) */
#define NBL_FS_ATTACH          0x500
# define NBL_FS_ATTACH_DEVICE   m1_d1

/*===========================================================
	Disk.
===========================================================*/

/* The driver is responsible for installing the disk device
objects it manages and responding to requests from those disk
device objects. */

/* bytes_read disk_read (IN blDeviceObject* disk, IN size_t sector, IN size_t off, IN size_t size, OUT void* buffer) */
#define NBL_DISK_READ  0
# define NBL_DISK_READ_SECTOR m1_i1
# define NBL_DISK_READ_SIZE   m1_i2
# define NBL_DISK_READ_BUFFER m1_i3
# define NBL_DISK_READ_OFFSET m1_i4
# define NBL_DISK_READ_DEV    m1_d1

/* bytes_read disk_write (IN blDeviceObject* disk, IN size_t sector, IN size_t off, IN size_t size, OUT void* buffer) */
#define NBL_DISK_WRITE 1
# define NBL_DISK_WRITE_SECTOR m1_i1
# define NBL_DISK_WRITE_SIZE   m1_i2
# define NBL_DISK_WRITE_BUFFER m1_i3
# define NBL_DISK_WRITE_OFFSET m1_i4
# define NBL_DISK_WRITE_DEV    m1_d1

/* BOOL BlVolumeScan (IN nblDeviceObject* disk) */
#define NBL_DISK_SCAN 2
# define NBL_DISK_SCAN_DEV    m1_d1

/* uint32_t bios_name (IN nblDeviceObject* disk) */
#define NBL_DISK_BIOSNAME 3
# define NBL_DISK_BIOSNAME_DEV m1_d1

/*===========================================================
	Partition Proxy.
===========================================================*/

/* The Partition Proxy driver is responsible for managing volume
information per device object. It is responsible for scanning
compatible device objects and creating volume device objects. */

/* bytes_read patition_read (IN nblDeviceObject* volumeDeviceObject, IN size_t sector, IN size_t size, OUT void* buffer) */
#define NBL_PARTITION_READ  0
# define NBL_PARTITION_READ_SECTOR m1_i1
# define NBL_PARTITION_READ_SIZE   m1_i2
# define NBL_PARTITION_READ_BUFFER m1_i3
# define NBL_PARTITION_READ_DEV    m1_i4

/* bytes_read partition_write (IN nblDeviceObject* volumeDeviceObject, IN size_t sector, IN size_t size, OUT void* buffer) */
#define NBL_PARTITION_WRITE 1
# define NBL_PARTITION_WRITE_SECTOR m1_i1
# define NBL_PARTITION_WRITE_SIZE   m1_i2
# define NBL_PARTITION_WRITE_BUFFER m1_i3
# define NBL_PARTITION_WRITE_DEV    m1_i4

/*===========================================================
	Firmware Services.
===========================================================*/

/* void exit (status) */
#define NBL_FW_EXIT      0
# define NBL_FW_EXIT_STATUS m1_i1

/* void reset (status) */
#define NBL_FW_RESET   1
# define NBL_FW_RESET_STATUS m1_i1

/* efi_system_table* services (void) */
#define NBL_FW_EFI_SERVICES 2

/* handle_t getImageHandle (void) */
#define NBL_FW_EFI_IMAGEHANDLE 3

/* void shutdown (void) */
#define NBL_FW_SHUTDOWN 4

/*===========================================================
	Memory.
===========================================================*/

/* void* mm_alloc (addr_t, page_count) */
#define NBL_MM_ALLOC  0
# define NBL_MM_ALLOC_ADDRESS m1_i1
# define NBL_MM_ALLOC_SIZE    m1_i2

/* void mm_free (addr_t, page_count) */
#define NBL_MM_FREE 1
# define NBL_MM_FREE_ADDRESS    m1_i1
# define NBL_MM_FREE_SIZE       m1_i2

/* size_t mm_get_map (addr_t memory) */
#define NBL_MM_GETMAP 2
# define NBL_MM_GETMAP_ADDRESS  m1_i1

/* status_t mm_init (NBL_MM_INIT_FUNCTION) */
#define NBL_MM_INIT 3
# define NBL_MM_INIT_FUNCTION   m1_i1

/*===========================================================
	Real Time Clock.
===========================================================*/

/* void rtc_gettime (OUT nblTime*); */
#define NBL_RTC_GETTIME 0
# define NBL_RTC_GETTIME_DESCR     m1_i1

/* void rtc_settime (IN nblTime*); */
#define NBL_RTC_SETTIME 1
# define NBL_RTC_SETTIME_DESCR     m1_i1

/* void rtc_stall (IN ms); */
#define NBL_RTC_STALL 2
# define NBL_RTC_STALL_MS          m1_i1

#define NBL_RTC_INSTALL 3

/*===========================================================
	Video.
===========================================================*/

/* status_t video_setmode (width,height,depth) */
#define NBL_VIDEO_SETMODE 0
# define NBL_VIDEO_SETMODE_WIDTH  m1_i1
# define NBL_VIDEO_SETMODE_HEIGHT m1_i2
# define NBL_VIDEO_SETMODE_DEPTH  m1_i3

/* status_t video_getmode (OUT nblVideoMode*) */
#define NBL_VIDEO_GETMODE 1
# define NBL_VIDEO_GETMODE_OUT m1_i1

/* status_t video_querymodes (NBL_QUERY_MODE_CALLBACK*, void* ext) */
#define NBL_VIDEO_QUERY 2
# define NBL_VIDEO_QUERY_CALL m1_i1
# define NBL_VIDEO_QUERY_EX   m1_i2

/* status_t video_setpalette (nblPalette*) */
#define NBL_VIDEO_SETPALETTE 3
# define NBL_VIDEO_SETPALETTE_IN m1_i1

/* status_t video_getpalette (nblPalette*) */
#define NBL_VIDEO_GETPALETTE 4
# define NBL_VIDEO_GETPALETTE_OUT m1_i1

/* status_t video_createsurface (IN OUT nblSurface*) */
#define NBL_VIDEO_CREATESURFACE 5
# define NBL_VIDEO_CREATESURFACE_IN m1_i1

/*  void QueryProtectedModeInterface(OUT uint16_t* seg, OUT uint16_t* off, OUT uint16_t* length) */
#define VBE_QUERY_PMODE_INTERFACE 6
# define VBE_QUERY_PMODE_INTERFACE_SEG m1_i1
# define VBE_QUERY_PMODE_INTERFACE_OFF m1_i2
# define VBE_QUERY_PMODE_INTERFACE_LENGTH m1_i3

/* uint16_t QueryCurrentModeNumber(void) */
#define VBE_QUERY_MODE 7

/*  void QueryControlInfo(OUT PVBE_INFO_BLOCK buffer)  */
#define VBE_QUERY_CONTROL_INFO 8
# define VBE_QUERY_CONTROL_INFO_OUT m1_i1

/* void QueryModeInfo(OUT PVBE_MODE_INFO pModeInfo) */
#define VBE_QUERY_MODE_INFO 9
# define VBE_QUERY_MODE_INFO_OUT m1_i1

/*===========================================================
	Console In Device.
===========================================================*/

/* status_t conin_reset (void) */
#define NBL_CONIN_RESET 0

/* scancode conin_read_key (void) */
#define NBL_CONIN_READKEY 1

/*===========================================================
	Console Out Device.
===========================================================*/

/* status_t conout_reset (void) */
#define NBL_CONOUT_RESET 0

/* status_t conout_cursor (BOOL enable) */
#define NBL_CONOUT_CURSOR 1
# define NBL_CONOUT_CURSOR_ENABLE m1_i1

/* BOOL conout_get_cursor_state (void) */
#define NBL_CONOUT_GETCURSOR 2

/* uint32_t conout_get_xy (void) */
#define NBL_CONOUT_GETXY 3

/* status_t conout_set_xy (uint16_t col, uint16_t row) */
#define NBL_CONOUT_SETXY 4
# define NBL_CONOUT_SET_X m1_i1
# define NBL_CONOUT_SET_Y m1_i2

/* status_t conout_clrscr (uint16_t fcol, uint16_t bcolo) */
#define NBL_CONOUT_CLRSCR 5
# define NBL_CONOUT_CLRSCR_FCOLOR m1_i1
# define NBL_CONOUT_CLRSCR_BCOLOR m1_i2

/* status_t conout_attrib (uint16_t fcol, uint16_t bcolo) */
#define NBL_CONOUT_ATTRIB 6
# define NBL_CONOUT_ATTRIB_FCOLOR m1_i1
# define NBL_CONOUT_ATTRIB_BCOLOR m1_i2

/* uint32_t conout_get_attrib (void) */
#define NBL_CONOUT_GETATTRIB 7

/* status_t conout_set_mode (uint16_t mode) */
#define NBL_CONOUT_SETMODE 8
# define NBL_CONOUT_SETMODE_MODE m1_i1

/* uint16_t conout_get_mode (void) */
#define NBL_CONOUT_GETMODE 9

/* status_t conout_print_string (char* s) */
#define NBL_CONOUT_PRINT 10
# define NBL_CONOUT_PRINT_STR m1_i1

/* graphics terminals only. */
#define NBL_CONOUT_INIT 11
# define NBL_CONOUT_SURFACE m1_i1
# define NBL_CONOUT_FONT m1_i2

/*===========================================================
	Serial.
===========================================================*/

/* uint32_t serial_read(size, buffer) */
#define NBL_SERIAL_READ 0
#define NBL_SERIAL_READ_SIZE m1_i1
#define NBL_SERIAL_READ_BUFFER m1_i2

/* uint32_t serial_write(size, buffer) */
#define NBL_SERIAL_WRITE 1
#define NBL_SERIAL_WRITE_SIZE m1_i1
#define NBL_SERIAL_WRITE_BUFFER m1_i2

/* bytes_read debug_read (IN size_t size, OUT void* buffer) */
#define NBL_DEBUG_READ            0x200
# define NBL_DEBUG_READ_SIZE      m1_i2
# define NBL_DEBUG_READ_BUFFER    m1_i3

/* bytes_written debug_write (IN size_t size, IN void* buffer) */
#define NBL_DEBUG_WRITE           0x300
# define NBL_DEBUG_WRITE_SIZE     m1_i2
# define NBL_DEBUG_WRITE_BUFFER   m1_i3

/*===========================================================
	Loader.
===========================================================*/

/* status_t boot (char* device_path) */
#define NBL_LOADER_BOOT 0
#define NBL_LOADER_BOOT_DEVICE m1_i1

/* status_t load_init (nblLPB* params) */
#define NBL_LOADER_INIT 1
#define NBL_LOADER_INIT_LPB m1_i1

/* status_t chainload (char* device_path) */
#define NBL_CHAINLOADER_BOOT 2
#define NBL_CHAINLOADER_BOOT_DEVICE m1_i1

/*===========================================================
	Keyboard Mapper.
===========================================================*/

/* nblKeyTable* kbdmap_get(void) */
#define NBL_KBDMAP_GET 0

/*
=====================================================

	NBL Basic Constants

=====================================================
*/

/* Status codes. */

#define NBL_STAT_SUCCESS            0
#define NBL_STAT_INVALID_ARGUMENT   -1
#define NBL_STAT_UNSUPPORTED        -2
#define NBL_STAT_DEPENDENCY         -3
#define NBL_STAT_DRIVER             -4
#define NBL_STAT_MEMORY             -5
#define NBL_STAT_CORRUPT            -6
#define NBL_STAT_BUFFER_OVERFLOW    -7
#define NBL_STAT_FW_INIT_FAIL       -8
#define NBL_STAT_DRIVER_INIT_FAIL   -9
#define NBL_STAT_ACCESS_VIOLATION   -10
#define NBL_STAT_BUFFER_UNDERFLOW   -11
#define NBL_STAT_DEVICE             -12
#define NBL_STAT_HARDWARE_ERROR     -13

/* ASSERT macro. */
#ifdef assert
#undef assert
#endif
#define assert(cond)                                               \
	do {                                                           \
		if (!(cond))                                               \
			BlrDbgAssert ((void*) #cond, __FILE__,__LINE__,__FUNCTION__, 0);  \
	}while(0);

/* ASSERT. */
#define ASSERT assert

/* ASSERTMSG macro. */
#ifdef assertmsg
#undef assertmsg
#endif
#define assertmsg(m,cond)                                          \
	do {                                                           \
		if (!(cond))                                               \
			BlrDbgAssert ((void*) #cond, __FILE__,__LINE__,__FUNCTION__, m);  \
	}while(0);

/* ASSERTMSG. */
#define ASSERTMSG assertmsg

/* TRACE macro. */
#define TRACE(fmt)      BlrDbgTrace (__FILE__,__LINE__,__FUNCTION__,fmt)

/* DPRINT macro. */
#define DPRINT(fmt,...) BlrDbgPrint(__FILE__,__LINE__,__FUNCTION__,fmt,__VA_ARGS__)

/* UNIMPLEMENTED macro. */
#define UNIMPLEMENTED TRACE("%s is unimplemented.")

/* width of stack == width of int */
#define	STACKITEM	int

/* round up width of objects pushed on stack. The expression before the
& ensures that we get 0 for objects of size 0. */
#define	NBL_VA_SIZE(TYPE)					\
	((sizeof(TYPE) + sizeof(STACKITEM) - 1)	\
		& ~(sizeof(STACKITEM) - 1))

/* &(LASTARG) points to the LEFTMOST argument of the function call (before the ...) */
#define	NBL_VA_START(AP, LASTARG)	\
	(AP=((NblVaNblListEntry)&(LASTARG) + NBL_VA_SIZE(LASTARG)))

/* VA_ARG macro. */
#define NBL_VA_ARG(AP, TYPE)	\
	(AP += NBL_VA_SIZE(TYPE), *((TYPE *)(AP - NBL_VA_SIZE(TYPE))))

/* nothing for va_end */
#define NBL_VA_END(AP)

/* Helper macros. */
#define MAKEWORD(a, b)      ((WORD)(((BYTE)((DWORD_PTR)(a) & 0xff)) | ((WORD)((BYTE)((DWORD_PTR)(b) & 0xff))) << 8))
#define MAKELONG(a, b)      ((LONG)(((WORD)((DWORD_PTR)(a) & 0xffff)) | ((DWORD)((WORD)((DWORD_PTR)(b) & 0xffff))) << 16))
#define LOWORD(l)           ((WORD)((DWORD)(l) & 0xffff))
#define HIWORD(l)           ((WORD)((DWORD)(l) >> 16))
#define LOBYTE(w)           ((BYTE)((DWORD)(w) & 0xff))
#define HIBYTE(w)           ((BYTE)((DWORD)(w) >> 8))
#define CHECKBIT(var,pos)	((var) & (1<<(pos)))

/* NULL */
#ifndef NULL
#define NULL 0
#endif

/* Call types. */
#undef FAR
#undef NEAR
#define FAR far
#define NEAR near

/* API type. */
#ifdef _MSC_VER
#define NBL_API
#else
#define NBL_API __declspec(dllexport)
#define NBL_API
#endif

/* NBL_DRIVER_API. */
#define NBL_DRIVER_API NBL_API

/* BOOL. */
#define FALSE 0
#define TRUE 1

/* Operand types. */
#define IN
#define OUT
#define OPTIONAL

/* Specifies non null terminating strings. */
#define NOT_NULL

/* INLINE. */
#ifdef _MSC_VER
#define INLINE __inline
#else
#define INLINE
#endif

/* STATIC. */
#define STATIC static

/* MIN and MAX macros. */
#define min(X, Y) (((X) < (Y)) ? (X) : (Y))
#define max(X, Y) (((X) > (Y)) ? (X) : (Y))

/**
*	Hashing algorithms.
*/
#define NBL_HASH_ALGORITHM_DEFAULT 0
#define NBL_HASH_ALGORITHM_X65599  1

/**
*	Calling convention.
*/
#ifdef _MSC_VER
#define NBL_CALL __cdecl
#else
#define NBL_CALL
#endif

/*
=====================================================

	NBLC Services.

=====================================================
*/

/**
*	Create driver section.
*/
#pragma section(".drv$a")
#pragma section(".drv$u")
#pragma section(".drv$z")

/*===========================================================
	NBLC Constants.
===========================================================*/

/* magic : 'NBL1' */
#define NBL_DRIVER_MAGIC 0x4e424c31

/* max name length. */
#define NBL_DRIVER_NAME_MAX 32

/* max number of children a driver object can have. */
#define NBL_DRIVER_OBJECT_CHILD_MAX 8

/* max number of dependencies a driver object can have. */
#define NBL_DRIVER_DEPEND_MAX 2

/* max length of volume labels. */
#define NBL_VOLUME_LABEL_MAX 32

/* va NblListEntry parameter NblListEntry */
typedef unsigned char *NblVaNblListEntry;

/**
*	Driver entry point
*/
#ifdef _MSC_VER
#define NBL_DRIVER_OBJECT_DEFINE \
	__declspec(allocate(".drv$u")) \
	volatile nblDriverObject
#else
#define NBL_DRIVER_OBJECT_DEFINE
#endif

#ifdef __GNUC__
#define nblDriverObject_DECL 	__attribute__ ((section(".drv"))
#else
#define nblDriverObject_DECL
#endif

#if 0
/**
*	Defines basic string type.
*/
typedef struct _nblAnsiString {
	size_t   size;
	size_t   allocated;
	char8_t* buffer;
}nblAnsiString;

/**
*	Defines basic unicode string type.
*/
typedef struct _nblUnicodeString {
	size_t    size;
	size_t    allocated;
	char16_t* buffer;
}nblUnicodeString;
#endif

/**
*	Driver entry point. The FW driver is required to locate and call
*	this method for driver initialization.
*/
typedef status_t (*NBL_DRIVER_ENTRY)    (IN nblDriverObject* self,
									IN int dependencyCount,
									IN nblDriverObject* dependencies[]);

/**
*	Trap callback. For FW drivers that have the support, this provides a
*	service to call the boot application on fatal errors.
*/
typedef status_t (*NBL_DRIVER_TRAP)    (IN struct nblDriverObject* source,
								   IN status_t trap,
								   IN OPTIONAL int arg1, IN OPTIONAL int arg2);

/**
*	Driver request method
*/
typedef status_t (*NBL_DRIVER_REQUEST)  (IN nblMessage* rpb);

/**
*	GUID type.
*/
#pragma pack(push, 1)
typedef struct _nblGUID {
	uint32_t data1;
	uint16_t data2;
	uint16_t data3;
	uint8_t  data4[8];
}nblGUID;
#pragma pack(pop)

/**
*	Volume Paramater Block.
*/

//typedef struct _nblVPB {
//	struct _nblDriverObject* fs;             /* File system that created the volume. */
//	struct _nblDeviceObject* io;             /* Device associated with the volume. */
//}nblVPB;

/**
*	Device Object Descriptor.
*/
typedef struct _nblDeviceObject {
	nblDeviceType             type;
	int                       devclass;
	unsigned int              characteristics;
	volatile nblDriverObject* driverObject;    /* Driver that created device. */
	volatile nblDriverObject* attached;
	nblDeviceObject*          next;            /* Next device. */
	char*                     name;
	void*                     ext;             /* Driver specific data. */
}nblDeviceObject;

/**
*	Driver dependency descriptor
*/
typedef struct _nblDriverObjectDependency {
	char name[NBL_DRIVER_NAME_MAX];
}nblDriverObjectDependency;

/**
*	Driver object node descriptor
*/
typedef struct _nblDriverObjectNode {
	nblDriverObject*  parent;
	unsigned int       childCount;
	nblDriverObject*  children[NBL_DRIVER_OBJECT_CHILD_MAX];
}nblDriverObjectNode;

/**
*	Driver object
*
*	Drivers of type NBL_DRV_LOAD_CORE are statically allocated global driver objects.
*	They should be defined directly within the boot application.
*
*	To create a core driver object, your code must look like the following example.
*
*	NBL_DRIVER_OBJECT_DEFINE _blUniqueDriverObjectName = {
*			NBL_DRIVER_MAGIC,
*			"MyDriverName",
*			DriverType
*			DependencyCount
*			MyDriverEntryPoint
*			MyDriverRequestHandler,
*			"My Dependency 1", "My Dependency 2"};
*
*	You can have as many dependencies as NBL_DRIVER_DEPEND_MAX. The dependency NblListEntry
*	is a NblListEntry of names of the drivers that the respective driver depends on. NBL will
*	attempt to initialize dependent drivers first.
*
*	Drivers of type NBL_DRV_LOAD_CORE2 are dynamically linked drivers. The driver must
*	exist as a shared driver in a seperate section of the boot application. In addition,
*	these types of drivers must have a driver object installed somewhere in the driver
*	section so NBL can locate the driver. These drivers will be loaded, moved, initialized,
*	and dynamically linked with the shared NBLC.
*
*	Drivers of type NBL_DRV_LOAD_MODULE are external shared libraries. NBL loads these
*	drivers on demand and links them in the same way it does with NBL_DRV_LOAD_CORE2.
*	NBL will call the entry point of the driver. It is responsible for calling NblRegisterDriverObject
*	to install the driver object in the driver object tree. NBL will dynamically link the
*	driver with the shared NBLC.
*
*	Modifying this structure will break existing drivers.
*/
typedef struct _nblDriverObject {

	/* The following must be set by the driver. */

	int                        magic;
	int                        version;
	nblDriverLoadType          loadType;
	nblDriverType              type;
	char                       name[NBL_DRIVER_NAME_MAX];
	NBL_DRIVER_ENTRY           entryPoint;
	NBL_DRIVER_REQUEST         request;
	size_t                     dependencyCount;
	nblDriverObjectDependency  dependencies[NBL_DRIVER_DEPEND_MAX];
	nblDriverObjectNode        node;

}nblDriverObject;

/* partition flags */

#define NBL_DRV_PART_SYSTEM         0
#define NBL_DRV_PART_LEGACY_BIOS    1
#define NBL_DRV_PART_READONLY       2
#define NBL_DRV_PART_HIDDEN         3
#define NBL_DRV_PART_NOAUTOMOUNT    4

/* partition manager subclasses */

#define NBL_DRV_CLASS_PART_LEGACY    0
#define NBL_DRV_CLASS_PART_GPT       1

/**
*	Partition types.
*/
typedef enum _nblPartitionType {
	NBL_PARTITION_TYPE_MBR,
	NBL_PARTITION_TYPE_GPT,
	NBL_PARTITION_TYPE_RAW
}nblPartitionType;

/**
*	Master boot record.
*/
typedef struct _nblPartitionMbr {
	unsigned char type;
	BOOL bootIndicater;
	size_t hiddenSectors;
}nblPartitionMbr;

/* GUID Partition types. */
#define NBL_PARTITION_BASIC_DATA_GUID     {0xebd0a0a2, 0xb9e5, 0x4433, {0x87, 0xc0, 0x68, 0xb6, 0xb7, 0x26, 0x99, 0xc7} }
#define NBL_PARTITION_ENTRY_UNUSED_GUID   {0, 0, 0, {0, 0, 0, 0, 0, 0, 0, 0} }
#define NBL_PARTITION_SYSTEM_GUID         {0xc12a7328, 0xf81f, 0x11d2, {0xba, 0xb4, 0, 0xa0, 0xc9, 0x3e, 0xc9, 0x3b} }
#define NBL_PARTITION_MSFT_RESERVED_GUID  {0, 0, 0, {0, 0, 0, 0, 0, 0, 0, 0} }
#define NBL_PARTITION_LDM_METADATA_GUID   {0, 0, 0, {0, 0, 0, 0, 0, 0, 0, 0} }
#define NBL_PARTITION_LDM_DATA_GUID       {0, 0, 0, {0, 0, 0, 0, 0, 0, 0, 0} }
#define NBL_PARTITION_MSFT_RECOVERY_GUID  {0, 0, 0, {0, 0, 0, 0, 0, 0, 0, 0} }

/**
*	GUID Partition Table.
*/
typedef struct _nblPartitionGpt {
	nblGUID  type;
	nblGUID  id;
	uint64_t attributes;
	wchar_t  name[36];
}nblPartitionGpt;

/**
*	Partition Descriptor.
*/
typedef struct _nblPartition {
	nblPartitionType type;
	size_t           start;
	size_t           length;
	union t {
		nblPartitionMbr mbr;
		nblPartitionGpt gpt;
	};
}nblPartition;

/**
*	File object.
*/
#pragma pack(push,1)
typedef struct _nblFile {
	unsigned short   type;
	uint64_t         size;
	nblDeviceObject* device; /* device object where file resides. */
	volatile nblDriverObject* driver; /* driver object that created file. */
	BOOL             locked;
	BOOL             readAccess;
	BOOL             writeAccess;
	BOOL             deleteAccess;
	unsigned long    flags;
	uint64_t         currentOffset;
	char*            name;
	void*            ext;   /* File system specific information. */
}nblFile;
#pragma pack(pop)

/* standard file objects. */
extern nblFile* nbl_stderr;
extern nblFile* nbl_stdin;
extern nblFile* nbl_stdout;
extern nblFile* nbl_wstdout;

/**
*	Double linked list.
*/
typedef struct _NblListEntry {
	struct _NblListEntry* next;
	struct _NblListEntry* prev;
}NblListEntry;

/**
*	Calculates byte offset of member in structure
*/
#ifndef offsetof
#ifdef WIN64
#define offsetof(s,m) ((unsigned long long)(&((s *)0)->m))
#else
#define offsetof(s,m) ((unsigned long)(&((s *)0)->m))
#endif
#endif

/**
*	Returns struct within entry.
*	\param p address of NblListEntry in structure
*	\param s structure type
*	\param m name of NblListEntry in structure
*/
#define NblListEntryEntry(p,s,m) \
	(s*)(((unsigned char*)p)-offsetof(s,m))

/**
* Queue element
*/
typedef struct _blQueueElement {
	void* data;
	struct _blQueueElement* next;
}blQueueElement;

/**
* Queue
*/
typedef struct _blQueue {
	int size;
	blQueueElement* head;
	blQueueElement* tail;
}blQueue;

#define NBL_SEEK_SET 0
#define NBL_SEEK_CUR 1

/**
*	Initializes NblListEntry.
*	\param p Pointer to NblListEntry
*/
STATIC INLINE void NblListEntryInit (IN NblListEntry* p) {
	p->next = p;
	p->prev = p;
}

/**
*	Insert node into NblListEntry.
*	\param after Previous node
*	\param node Node to insert
*/
STATIC INLINE void NblListEntryInsertAfter (IN NblListEntry* after, IN NblListEntry* node) {
	node->next = after->next;
	node->prev = after;
	after->next->prev = node;
	after->next = node;
}

/**
*	Insert node into NblListEntry.
*	\param before Node to insert before
*	\param node Node to insert
*/
STATIC INLINE void NblListEntryInsertBefore (IN NblListEntry* before, IN NblListEntry* node) {
	node->next = before;
	node->prev = before->prev;
	before->prev->next = node;
	before->prev = node;
}

/**
*	Insert node into head of NblListEntry.
*	\param node Node to insert
*/
STATIC INLINE void NblListEntryInsertHead (IN NblListEntry* head, IN NblListEntry* node) {
	NblListEntryInsertAfter (head, node);
}

/**
*	Insert node into tail of NblListEntry.
*	\param node Node to insert
*/
STATIC INLINE void NblListEntryInsertTail (IN NblListEntry* tail, IN NblListEntry* node) {
	NblListEntryInsertBefore (tail, node);
}

/**
*	Remove node from NblListEntry.
*	\param node Node to remove
*/
STATIC INLINE void NblListEntryRemove (IN NblListEntry* node) {
	node->prev->next = node->next;
	node->next->prev = node->prev;
}

/**
*	Test if NblListEntry is empty.
*	\param node Head of NblListEntry
*	\ret 1: NblListEntry is not empty, 0: NblListEntry is empty
*/
STATIC INLINE int NblListEntryEmpty (IN NblListEntry* node) {
	return (node->next == node);
}

/**
*	Returns size of NblListEntry.
*	\param node Head of NblListEntry
*	\ret Number of elements in NblListEntry
*/
STATIC INLINE unsigned int NblListEntrySize (IN NblListEntry* node) {
	unsigned int ret = 0;
	NblListEntry* cur = 0;
	for (cur = node->next; cur != node; cur = cur->next)
		ret++;
	return ret;
}

/**
*	Returns next element in NblListEntry.
*	\param head Head of NblListEntry
*	\param node Node to incrememnt
*	\return Next element in NblListEntry or 0 if end
*/
STATIC INLINE NblListEntry* NblListEntryNext (IN NblListEntry* head, IN NblListEntry* node) {
	if (node == head)
		return 0;
	return node->next;
}

/**
*	Returns previous element in NblListEntry.
*	\param head Head of NblListEntry
*	\param node Node to incrememnt
*	\return Previous element in NblListEntry or 0 if start of NblListEntry
*/
STATIC INLINE NblListEntry* NblListEntryPrev (IN NblListEntry* head, IN NblListEntry* node) {
	if (node == head)
		return 0;
	return node->prev;
}

/**
* Initialize queue
* \param q Queue object (not pointer)
*/
#define BlInitQueue(q) do { \
	q.size = 0; \
	q.head = NULL; \
	q.tail = NULL; \
}while(0);

/**
* Return size of queue
* \param q Queue (not pointer)
*/
#define BlQueueSize(q) \
	q.size

/**
* Time object.
*/
typedef struct _nblTime {
	uint16_t year;
	uint8_t  month;
	uint8_t  day;
	uint8_t  hour;
	uint8_t  minute;
	uint8_t  second;
	uint32_t nanosecond;
	int16_t  timezone;
	uint8_t  daylight;
} nblTime;

#define UNICODE_NULL "\x00\x00"
#define ASCII_NULL   0x00


/* returns offset of record in structure. */
#define NRL_OFFSETOF(t,f) ((unsigned long)(addr_t)&(((t*) 0)->f))

/* returns containing record in parent structure. */
#define NRL_CONTAINING_RECORD(address,type,field)  ((type *)(((addr_t)address) - (addr_t)(&(((type *)0)->field))))

/* bit map. */
typedef struct _RTLBITMAP {
	addr_t       base;
	size_t       size;
}RTLBITMAP;

/* double linked list. */
typedef struct _LIST_ENTRY {
	struct _LIST_ENTRY* next;
	struct _LIST_ENTRY* prev;
}LIST_ENTRY;

INLINE void BlrInitializeBitmap(IN OUT RTLBITMAP* in) {

	in->base = in->size = 0;
}

INLINE void BlrSetBit(IN RTLBITMAP* in, IN int index) {

	uint32_t* base = (uint32_t*) in->base;
	base [index / 32] |= (1 << (index % 32));
}

INLINE void BlrClearBit(IN RTLBITMAP* in, IN int index) {

	uint32_t* base = (uint32_t*) in->base;
	base [index / 32] &= ~(1 << (index % 32));
}

INLINE BOOL BlrBitTest(IN RTLBITMAP* in, IN int index) {

	uint32_t* base = (uint32_t*) in->base;
	if (base [index / 32] & (1 << (index % 32)))
		return TRUE;
   return FALSE;
}

INLINE void BlrInitializeListHead(IN LIST_ENTRY* head) {

	head->next = head->prev = head;
}

INLINE void BlrInsertHeadList(IN LIST_ENTRY* head, IN LIST_ENTRY* e) {

	LIST_ENTRY* oldNext = head->next;
	e->next = oldNext;
	e->prev = head;
	oldNext->prev = e;
	head->next = e;
}

INLINE void BlrInsertTailList(IN LIST_ENTRY* head, IN LIST_ENTRY* e) {

	LIST_ENTRY* oldPrev = head->prev;
	e->next = head;
	e->prev = oldPrev;
	head->prev = e;
	oldPrev->next = e;
}

INLINE bool_t BlrIsListEmpty (IN LIST_ENTRY* head) {

	return (head->next == head);
}

INLINE bool_t BlrRemoveEntryList(IN LIST_ENTRY* e) {

	LIST_ENTRY* oldPrev;
	LIST_ENTRY* oldNext;

	oldNext = e->next;
	oldPrev = e->prev;
	oldNext->prev = oldPrev;
	oldPrev->next = oldNext;
	return (oldNext == oldPrev);
}

INLINE LIST_ENTRY* BlrRemoveHeadList(IN LIST_ENTRY* head) {

	LIST_ENTRY* next;
	LIST_ENTRY* e;

	e = head->next;
	next = e->next;
	head->next = next;
	next->prev = head;
	return e;
}

INLINE LIST_ENTRY* BlrRemoveTailList(IN LIST_ENTRY* head) {

	LIST_ENTRY* prev;
	LIST_ENTRY* e;

	e = head->prev;
	prev = e->prev;
	head->prev = prev;
	prev->next = head;
	return e;
}

/* config.c structures */

typedef enum _VALUE_TYPE {
	REG_CHARATER,
	REG_NUMBER,
	REG_STRING,
	REG_INVALID
}VALUE_TYPE;

typedef union _ATTRIBUTE {
	int   n;
	char* s;
	char  c;
}ATTRIBUTE;

typedef struct _KEY {
	VALUE_TYPE type;
	ATTRIBUTE  u;
}KEY;

typedef struct _PROPERTY {
	char*     name;
	KEY       key;
	struct _PROPERTY* parent;
	LIST_ENTRY children;
	LIST_ENTRY entry;
}PROPERTY;

/*
===============================================================

	NBL Core Services.

===============================================================
*/

extern BOOL NBL_CALL BlInitializeDrivers (void);
extern void NBL_CALL BlCallDriverEntryPoints (nblDriverLoadType type);
extern nblDriverObject* NBL_CALL BlRootDriverObject (void);
extern BOOL BlDriverIterate (NBL_DRV_ITERATE_HOOK_FUNC hook, void* ex);
extern NBL_API status_t NBL_CALL BlAtExit (IN NBL_EXIT_FUNC p);
extern NBL_API status_t NBL_CALL BlStartBootApplication (void);

/*
=============================================================

	NBL Runtime Services.

=============================================================
*/

extern NBL_API char8_t  NBL_CALL BlrWideCharToSingleiByte(IN char16_t character);
extern NBL_API char16_t NBL_CALL BlrSingleByteToWideChar(IN char character);

extern NBL_API void NBL_CALL BlrDbgDisplayDriverTree (IN nblDriverObject* root);

static INLINE void* NBL_CALL BlrQueueFront (IN blQueue* in) {
	if (!in || !in->head)
		return 0;
	return (in->size) ? in->head->data : 0;
}

extern NBL_API BOOL NBL_CALL BlrQueuePush  (IN blQueue* in, IN void* elem);
extern NBL_API BOOL NBL_CALL BlrQueuePop   (IN blQueue* in, IN BOOL release);
extern NBL_API BOOL NBL_CALL BlrQueueClear (IN blQueue* in, IN BOOL release);

extern void             BlrPrintf(char*,...);
extern void             BlrWidePrintf(wchar_t*, ...);
extern NBL_API status_t BlrCharToInteger (IN char* s, IN OPTIONAL unsigned long base, OUT unsigned long* value);
extern NBL_API status_t BlrWideCharToInteger (IN wchar_t* s, IN OPTIONAL unsigned long base, OUT unsigned long* value);
extern NBL_API status_t BlrIntegerToChar (IN unsigned long value, IN unsigned long base, IN unsigned long length, OUT char* s);
extern NBL_API status_t BlrIntegerToWideChar (IN unsigned long value, IN unsigned long base, IN unsigned long length, OUT wchar_t* s);

extern NBL_API void NBL_CALL BlrZeroMemory (IN void* destination, size_t length);
extern NBL_API void NBL_CALL BlrFillMemory (IN void* destination, IN  unsigned char value,  IN size_t length);
extern NBL_API unsigned long NBL_CALL BlrCopyMemory (IN void* s1, IN void* s2, IN size_t length);
extern unsigned int NBL_CALL BlrStrCopy (IN char* s1, IN char* s2);
extern NBL_API unsigned long NBL_CALL BlrCompareMemory (IN void* s, IN void* d, size_t length);
extern unsigned int NBL_CALL BlrStrLength (IN char* s);
extern unsigned int NBL_CALL BlrWideStrLength (IN wchar_t* s);
extern int NBL_CALL BlrWideStrCompare (IN wchar_t* s1, IN wchar_t* s2);
extern int NBL_CALL BlrStrCompare (IN char* s1, IN char* s2);
extern int NBL_CALL BlrStrCompareEx (IN char* s1, IN char* s2, IN uint32_t c);
extern int NBL_CALL BlrWideStrCompareEx (IN wchar_t* s1, IN wchar_t* s2, IN uint32_t c);

extern char NBL_API BlrUpperAnsiChar (char c);
extern wchar_t NBL_API BlrUpperWideChar (wchar_t c);
extern char NBL_API BlrLowerAnsiChar (char c);
extern wchar_t NBL_API BlrLowerWideChar (wchar_t c);
extern char* BlrStrGetLast (IN char* s, IN int c);
extern char NBL_API BlrLowerAnsiChar (char c);

extern BOOL NBL_API BlrIsUpperChar (IN char c);
extern BOOL NBL_API BlrIslowerWideChar (IN wchar_t c);
extern BOOL NBL_API BlrIsUpperWideChar (IN wchar_t c);
extern int  NBL_API  BlrWctomb(char* pmb, wchar_t wc);
extern int  NBL_API  BlrMbtowc(wchar_t *pwc, const char *s, size_t n);
extern BOOL NBL_API BlrIslowerChar (IN char c);
extern NBL_API void BlrDbgAssert (IN char* cond, IN char* file, IN unsigned long line, IN char* function, IN OPTIONAL char* msg);
extern NBL_API void BlrDbgTrace (IN char* cond, IN char* file, IN unsigned long line, IN char* function);
extern NBL_API void BlrDbgPrint (IN char* file, IN unsigned long line, IN char* function, IN char* format, ...);
extern NBL_API nblDriverObject* BlrDriverRoot (IN nblDriverObject* in);
extern NBL_API nblDriverObject* BlrGetRootDriverObject (void);
extern NBL_API status_t BlrInitialize (IN nblDriverObject* in);
extern NBL_API status_t NBL_CALL BlrInitializeHeap (void);
extern NBL_API  void* NBL_CALL BlrAlloc (IN size_t size);

typedef enum _ZONE {
	ZONE_NORMAL = 0,
	ZONE_LOW = 1
}ZONE;

extern PUBLIC void BlrSetActiveHeap (ZONE zone);
extern NBL_API void NBL_CALL BlrFree (IN void* addr);
extern NBL_API void* NBL_CALL BlrZeroAlloc (IN size_t size);
extern NBL_API uint32_t NBL_CALL BlrCrc32 (IN uint32_t crc, IN const void *buf, IN size_t size);
extern NBL_API void NBL_CALL BlrDbgBreak (void);
extern NBL_API nblDriverObject* NBL_CALL BlrOpenDriver (IN const char* path);
extern NBL_API status_t NBL_CALL BlrRegisterDevice (IN nblDeviceObject* dev);
extern NBL_API void NBL_CALL BlrUnregisterDevice (IN nblDeviceObject* dev);
extern NBL_API void NBL_CALL BlrDeviceIterate (IN int type, IN NBL_DEV_ITERATE_HOOK_FUNC hook, void* ex);
extern NBL_API nblDeviceObject* NBL_CALL BlrOpenDevice (IN char* name);
extern NBL_API void NBL_CALL BlrCloseDevice (IN nblDeviceObject* dev);
extern NBL_API status_t NBL_CALL BlrSetEnvironmentVariable (IN char* name, IN char* val);
extern NBL_API status_t NBL_CALL BlrGetEnvironmentVariable (IN char* name, OUT char* val);
extern NBL_API status_t NBL_CALL BlrSetTime (IN nblTime* in);

extern int NBL_CALL BlrSPrintf (IN const char* s, IN const char* format, OUT ...);
extern int NBL_CALL BlrWideSPrintf(IN const wchar_t* s, IN const wchar_t* format, OUT ...);
extern char* BlrStrLocateFirst (IN const char *s, IN int c);
extern int BlrWriteFormattedString (IN OUT char* str, IN const char* format, IN ...);
extern int NBL_CALL BlrWriteVaListToString (IN char* buf,IN const char* format, IN va_list arg);
extern void NBL_CALL BlrPrintf (IN char* format, ...);
extern size_t BlrPrints(char* in);

#define BlrError BlrPrintf

extern int NBL_CALL  BlrWriteStringToVaList (IN const char* buf,IN const char* format,IN va_list arg);
extern int NBL_CALL  BlrScanFormattedString (IN const char* format, IN va_list arg);
extern int NBL_CALL  BlrSScanf (IN const char* s, IN const char* format, OUT ...);
extern void NBL_CALL BlrScanf (IN const char* format, OUT ...);
extern long NBL_CALL BlrStrToLong (IN const char *nptr, IN char **endptr, IN int base);
extern char*         BlrStrToken (IN char* s, IN const char* delim);
extern char*         BlrStrDup (IN char* s);
extern int           BlrStrToInt ( const char *c );

extern status_t      BlrGetTime (OUT nblTime* out);
extern status_t      BlrSystemReset (IN int reason);
extern status_t      BlrAllocatePool (IN addr_t base, IN size_t length);
extern status_t      BlrReleasePool (IN addr_t base, IN size_t length);
extern status_t      BlrFsAttach (IN nblDeviceObject* dev, IN nblDriverObject* fs);
extern nblFile*      BlrFsOpen (IN nblDeviceObject* volume, IN char* fname);
extern size_t        BlrFsRead (IN nblFile* file, IN size_t size, OUT void* buffer);
extern size_t        BlrFsWrite (IN nblFile* file, IN size_t size, OUT void* buffer);
extern status_t      BlrFsClose (IN nblFile* file);
extern BOOL          BlrFileEnd (IN nblFile* in);
extern char*         BlrFileGetString (IN char* str, IN int num, IN nblFile* stream);
extern uint64_t      BlrFileTell (IN nblFile* stream);
extern int           BlrFileSeek (IN nblFile* stream, IN uint64_t offset, IN int origin);
extern nblFile*      BlrFileOpen (IN char* path);
extern status_t      BlrFileReopen (IN char* fname, IN nblFile* stream);
extern uint32_t      BlrDiskBiosName (IN nblDeviceObject* dev);
extern size_t        BlrDiskRead (IN nblDeviceObject* dev, IN size_t sector, IN size_t off, IN size_t size, OUT void* buffer);
extern size_t        BlrDiskWrite (IN nblDeviceObject* dev, IN size_t sector, IN size_t off, IN size_t size, OUT void* buffer);
extern size_t        BlrGetMemoryMap(IN void* memory);
extern size_t        BlrExit (IN status_t status);
extern size_t        BlrReset (IN status_t status);
extern size_t        BlrPrints (IN char* in);
extern status_t      BlrTerminalInputReset (IN nblDeviceObject* dev);
extern uint32_t      BlrTerminalInputReadKey (IN nblDeviceObject* dev);
extern status_t      BlrTerminalOutputReset (IN nblDeviceObject* dev);
extern status_t      BlrTerminalOutputEnableCursor (IN nblDeviceObject* dev, IN BOOL enable);
extern BOOL          BlrTerminalOutputGetCursor (IN nblDeviceObject* dev);
extern uint32_t      BlrTerminalOutputGetPosition (IN nblDeviceObject* dev);
extern status_t      BlrTerminalOutputSetPosition (IN nblDeviceObject* dev, IN uint32_t x, IN uint32_t y);
extern status_t      BlrTerminalOutputClearScreen (IN nblDeviceObject* dev, IN uint32_t fcol, IN uint32_t bcol);
extern uint32_t      BlrTerminalOutputSetAttribute (IN nblDeviceObject* dev, IN uint32_t fcol, IN uint32_t bcol);
extern uint32_t      BlrTerminalOutputGetAttribute (IN nblDeviceObject* dev);
extern status_t      BlrTerminalOutputSetMode (IN nblDeviceObject* dev, IN uint32_t mode);
extern uint32_t      BlrTerminalOutputGetMode (IN nblDeviceObject* dev);
extern uint32_t      BlrTerminalPrint (IN nblDeviceObject* dev, IN char* s);
extern status_t      BlrCreateSurface (IN OUT nblSurface* desc);
extern status_t      BlrGetPalette (IN nblSurface* desc, IN nblPalette* palette);
extern status_t      BlrSetPalette (IN nblSurface* desc, IN nblPalette* palette);
extern status_t      BlrQueryVideoModes (IN nblSurface* desc, NBL_QUERY_MODE_CALLBACK* callback, void* ex);
extern status_t      BlrGetVideoMode (IN nblSurface* desc, OUT nblVideoMode* out);
extern status_t      BlrSetVideoMode (IN nblSurface* desc, uint32_t width, uint32_t height, uint32_t depth);
extern uint32_t      BlrSerialRead (IN uint32_t size, OUT void* buffer);
extern uint32_t      BlrSerialWrite (IN uint32_t size, IN void* buffer);
extern status_t      BlrChainload (char* device_path);
extern status_t      BlrBoot (IN nblDriverObject* loader, IN char* device_path);
extern nblKeyTable*  BlrGetKeyboardMap (IN nblDriverObject* mapper);
extern void*         BlrAllocatePages (IN addr_t phys, IN uint32_t pages);
extern void          BlrRtcInstall(void);
extern void          BlrFreePages (IN addr_t phys, IN uint32_t pages);
extern void          BlrDebugRead(IN size_t size, OUT char* buffer);
extern void          BlrDebugWrite(IN size_t size, IN char* buffer);

#endif
