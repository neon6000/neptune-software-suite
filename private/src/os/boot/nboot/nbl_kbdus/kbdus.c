/************************************************************************
*
*	NBL US Keyboard Mapper Driver
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* Maps US keyboard scancodes to NBL VK codes. */

#include <nbl.h>

/*
	maps virtual scan code to virtual key code.	
*/
STATIC
nblKey _vscToVkMap[] = {
	NBL_VK_EMPTY, NBL_VK_ESCAPE, '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
	NBL_VK_OEM_MINUS, NBL_VK_OEM_PLUS, NBL_VK_BACK,
	/* 0f */
	NBL_VK_TAB, 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', NBL_VK_OEM4, NBL_VK_OEM6, NBL_VK_RETURN,
	/* 1d */
	NBL_VK_LCONTROL, 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', NBL_VK_OEM1, NBL_VK_OEM7, NBL_VK_OEM3,
	NBL_VK_SHIFT, NBL_VK_OEM5,
	/* 2c */
	'Z', 'X', 'C', 'V', 'B', 'N', 'M', NBL_VK_OEM_COMMA, NBL_VK_OEM_PERIOD, NBL_VK_OEM2, NBL_VK_RSHIFT,
	/* 37 */
	NBL_VK_MULTIPLY, NBL_VK_LMENU, NBL_VK_SPACE, NBL_VK_CAPITAL,
	/* 3b */
	NBL_VK_F1, NBL_VK_F2, NBL_VK_F3, NBL_VK_F4, NBL_VK_F5, NBL_VK_F6, NBL_VK_F7,
	NBL_VK_F8, NBL_VK_F9, NBL_VK_F10,
	/* 45 */
	NBL_VK_NUMLOCK, NBL_VK_SCROLL,
	/* 47 */
	NBL_VK_HOME, NBL_VK_UP, NBL_VK_PRIOR, NBL_VK_SUBTRACT,
	NBL_VK_LEFT, NBL_VK_CLEAR, NBL_VK_RIGHT, NBL_VK_ADD,
	NBL_VK_END, NBL_VK_DOWN, NBL_VK_NEXT, NBL_VK_INSERT,
	NBL_VK_DELETE,
	/* 54 */
	NBL_VK_SNAPSHOT,
	/* 55 */
	NBL_VK_EMPTY, NBL_VK_102, NBL_VK_F11, NBL_VK_F12,
	/* 59 */
	NBL_VK_CLEAR, NBL_VK_EMPTY, NBL_VK_EMPTY, NBL_VK_EMPTY, NBL_VK_EMPTY,
	NBL_VK_EMPTY, NBL_VK_EMPTY, NBL_VK_EMPTY, NBL_VK_EMPTY, NBL_VK_EMPTY,
	NBL_VK_HELP,
	/* 6f */
	NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,
	/* 72 */
	NBL_VK_EMPTY, NBL_VK_EMPTY,NBL_VK_EMPTY,NBL_VK_EMPTY,
	/* 76 */
	NBL_VK_EMPTY, /* f-24 key */
	/* 77 */
	NBL_VK_EMPTY, NBL_VK_EMPTY, NBL_VK_EMPTY, NBL_VK_EMPTY,
	NBL_VK_EMPTY, NBL_VK_EMPTY, NBL_VK_EMPTY, NBL_VK_EMPTY,
	NBL_VK_EMPTY,
	/* 80 */
	0
};

/*
	maps virtual key code to wide-character name.
*/
STATIC
nblVkCharMap _vkCharMap[] = {

	/* number pad. */
	{NBL_VK_NUMPAD0, 0, {'0'} },
	{NBL_VK_NUMPAD1, 0, {'0'} },
	{NBL_VK_NUMPAD2, 0, {'0'} },
	{NBL_VK_NUMPAD3, 0, {'0'} },
	{NBL_VK_NUMPAD4, 0, {'0'} },
	{NBL_VK_NUMPAD5, 0, {'0'} },
	{NBL_VK_NUMPAD6, 0, {'0'} },
	{NBL_VK_NUMPAD7, 0, {'0'} },
	{NBL_VK_NUMPAD8, 0, {'0'} },
	{NBL_VK_NUMPAD9, 0, {'0'} },

	/* alphabet */
	{'A', 0, {'a', 'A'} },
	{'B', 0, {'b', 'B'} },
	{'C', 0, {'c', 'C'} },
	{'D', 0, {'d', 'D'} },
	{'E', 0, {'e', 'E'} },
	{'F', 0, {'f', 'F'} },
	{'G', 0, {'g', 'G'} },
	{'H', 0, {'h', 'H'} },
	{'I', 0, {'i', 'I'} },
	{'J', 0, {'j', 'J'} },
	{'K', 0, {'k', 'K'} },
	{'L', 0, {'l', 'L'} },
	{'M', 0, {'m', 'M'} },
	{'N', 0, {'n', 'N'} },
	{'O', 0, {'o', 'O'} },
	{'P', 0, {'p', 'P'} },
	{'Q', 0, {'q', 'Q'} },
	{'R', 0, {'r', 'R'} },
	{'S', 0, {'s', 'S'} },
	{'T', 0, {'t', 'T'} },
	{'U', 0, {'u', 'U'} },
	{'V', 0, {'v', 'V'} },
	{'W', 0, {'w', 'W'} },
	{'X', 0, {'x', 'X'} },
	{'Y', 0, {'y', 'Y'} },
	{'Z', 0, {'z', 'Z'} },

	/* numbers. */
	{'1', 0, {'1', '!'} },
	{'2', 0, {'2', '@'} }, /* ctrl+2 generates NUL */
	{'3', 0, {'3', '#'} },
	{'4', 0, {'4', '$'} },
	{'5', 0, {'5', '%'} },
	{'6', 0, {'6', '^', 0, 0x1e /* RS */ } },
	{'7', 0, {'7', '&'} },
	{'8', 0, {'8', '*'} },
	{'9', 0, {'9', '('} },
	{'0', 0, {'0', ')'} },

	/* specials. */
	{NBL_VK_OEM_PLUS,   0, {'7', '&'} },
	{NBL_VK_OEM_MINUS,  0, {'-','_', 0, 0x1f /* US */ } },
	{NBL_VK_OEM1,       0, {';', ':'} },
	{NBL_VK_OEM7,       0, {'\'', '\"'} },
	{NBL_VK_OEM3,       0, {'`', '~'} },
	{NBL_VK_OEM_COMMA,  0, {',', '<'} },
	{NBL_VK_OEM_PERIOD, 0, {'.', '>'} },
	{NBL_VK_OEM2,       0, {'/', '?'} },

	/* does not have shift states */
	{NBL_VK_TAB,      0,             {'\t', '\t', '\t'} },
	{NBL_VK_ADD,      0,             {'+', '+','+'} },
	{NBL_VK_SUBTRACT, 0,             {'-', '-','-'} },
	{NBL_VK_DECIMAL,  0,             {'.', '.','.'} },
	{NBL_VK_MULTIPLY, 0,             {'*', '*','*'} },
	{NBL_VK_DIVIDE,   0,             {'/', '/', '/'} },

	/* escapes */
	{NBL_VK_OEM4,     0,             {'['  , '{' , 0x1b /* ESC */ } },
	{NBL_VK_OEM5,     0,             {'\\' , '|' , 0x1c /* FS */  } },
	{NBL_VK_OEM6,     0,             {']'  , '}' , 0x1d /* GS */  } },
	{NBL_VK_102,      0,             {'\\' , '|' , 0x1c /* FS */  } },
	{NBL_VK_BACK,     0,             {0x8  , 0xb , 0x7F           } },
	{NBL_VK_ESCAPE,   0,             {0x1b , 0x1b, 0x1b           } },
	{NBL_VK_RETURN,   0,             {'\r' , '\r', '\n'           } },
	{NBL_VK_SPACE,    0,             {' '  , ' ' , ' '            } },
	{NBL_VK_CANCEL,   0,             {0x3  , 0x3 , 0x3            } },
	{0,0}
};

STATIC
nblVscStr _keynames[] = {
	{ 0x01, L"Esc" },
	{ 0x0e, L"Backspace" },
	{ 0x0f, L"Tab" },
	{ 0x1c, L"Enter" },
	{ 0x1d, L"Ctrl" },
	{ 0x2a, L"Shift" },
	{ 0x36, L"Right Shift" },
	{ 0x37, L"Num *" },
	{ 0x38, L"Alt" },
	{ 0x39, L"Space" },
	{ 0x3a, L"CAPLOK Lock" },
	{ 0x3b, L"F1" },
	{ 0x3c, L"F2" },
	{ 0x3d, L"F3" },
	{ 0x3e, L"F4" },
	{ 0x3f, L"F5" },
	{ 0x40, L"F6" },
	{ 0x41, L"F7" },
	{ 0x42, L"F8" },
	{ 0x43, L"F9" },
	{ 0x44, L"F10" },
	{ 0x45, L"Pause" },
	{ 0x46, L"Scroll Lock" },
	{ 0x47, L"Num 7" },
	{ 0x48, L"Num 8" },
	{ 0x49, L"Num 9" },
	{ 0x4a, L"Num -" },
	{ 0x4b, L"Num 4" },
	{ 0x4c, L"Num 5" },
	{ 0x4d, L"Num 6" },
	{ 0x4e, L"Num +" },
	{ 0x4f, L"Num 1" },
	{ 0x50, L"Num 2" },
	{ 0x51, L"Num 3" },
	{ 0x52, L"Num 0" },
	{ 0x53, L"Num Del" },
	{ 0x54, L"Sys Req" },
	{ 0x56, L"\\" },
	{ 0x57, L"F11" },
	{ 0x58, L"F12" },
	{ 0x7c, L"F13" },
	{ 0x7d, L"F14" },
	{ 0x7e, L"F15" },
	{ 0x7f, L"F16" },
	{ 0x80, L"F17" },
	{ 0x81, L"F18" },
	{ 0x82, L"F19" },
	{ 0x83, L"F20" },
	{ 0x84, L"F21" },
	{ 0x85, L"F22" },
	{ 0x86, L"F23" },
	{ 0x87, L"F24" },
	{ 0, NULL }
};

STATIC
nblVscStr _keynamesEx[] = {
	{ 0x1c, L"Num Enter" },
	{ 0x1d, L"Right Ctrl" },
	{ 0x35, L"Num /" },
	{ 0x37, L"Prnt Scrn" },
	{ 0x38, L"Right Alt" },
	{ 0x45, L"Num Lock" },
	{ 0x46, L"Break" },
	{ 0x47, L"Home" },
	{ 0x48, L"Up" },
	{ 0x49, L"Page Up" },
	{ 0x4a, L"-" },
	{ 0x4b, L"Left" },
	{ 0x4d, L"Right" },
	{ 0x4f, L"End" },
	{ 0x50, L"Down" },
	{ 0x51, L"Page Down" },
	{ 0x52, L"Insert" },
	{ 0x53, L"Delete" },
	{ 0x54, L"<00>" },
	{ 0x56, L"Help" },
	{ 0x5b, L"Left Windows" },
	{ 0x5c, L"Right Windows" },
	{ 0x5d, L"Application" },
	{ 0, NULL },
};

STATIC
nblKeyTable _keymap = {
	_vkCharMap,
	_keynames,
	_keynamesEx,
	_vscToVkMap
};

STATIC
nblKeyTable*
KBDUSGetTable (void) {
	/* returns keyboard map.*/
	return &_keymap;
}

STATIC
status_t
KBDUSEntryPoint (IN nblDriverObject* self,
				 IN int dependencyCount,
				 IN nblDriverObject* dependencies[]) {

	/* nothing to do. */
	return NBL_STAT_SUCCESS;
}

/* request callback. */
STATIC
status_t
KBDUSRequest (IN nblMessage* rpb) {

	rettype_t r;
	rpb->retval = 0;
	switch (rpb->type) {
		case NBL_KBDMAP_GET:
			r = (rettype_t) KBDUSGetTable();
			break;
		default: return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = r;
	return NBL_STAT_SUCCESS;
}

#define NBL_KBDUS_VERSION 1

/* US keyboard map driver object. */
NBL_DRIVER_OBJECT_DEFINE _nKBDUS = {
	NBL_DRIVER_MAGIC,
	NBL_KBDUS_VERSION,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_OTHER,
	"kbdus",
	KBDUSEntryPoint,
	KBDUSRequest,
	0
};
