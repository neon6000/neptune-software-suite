;***********************************************************************
;
;  i386.asm - i386 back end code for nbl_ndbg.
;
;   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
;
;***********************************************************************

; Note this is BIOS build specific. For EFI builds, we
; muse use the debug protocol. TODO: move into nbl_bios!

; Here for now since nbl_dbg is still being architectured.

bits 32
section .text

extern _DbgExceptionHandler

;=== Exceptions ====================================

;
; Processor Generated Exceptions
;
global _i386DivideByZero
global _i386DebugException
global _i386NMIException
global _i386Breakpoint
global _i386Overflow
global _i386BoundException
global _i386InvalidOpcode
global _i386FPUNotAvailable
global _i386DoubleFault
global _i386CoprocessorSegment
global _i386InvalidTss
global _i386SegmentNotPresent
global _i386StackException
global _i386GeneralProtectionFault
global _i386PageFault
global _i386CoprocessorError
global _i386AlignmentCheck
global _i386MachineCheck

;
; Exception entry point
;
_i386ExceptionEntry:
	;
	; Call exception handler.
	;
	pushad
	mov eax, ss
	push eax	; we copy to eax in order to clear the high bytes of the stack element.
	mov eax, ds
	push eax
	mov eax, es
	push eax
	mov eax, fs
	push eax
	mov eax, gs
	push eax
	push dword esp		; pointer to stack frame.
	push dword 0		; nested exception flag.
	call _DbgExceptionHandler
	add esp, 8
	pop gs
	pop fs
	pop es
	pop ds
	pop ss
	popad
	;
	; ExceptionWithErrorCode and ExceptionWithoutErrorCode
	; push an error code and vector number on stack, so
	; we pop them now.
	;
	add esp, 8
	iret

;
; Calls exception handler. It is
; assumed CPU already pushed error code.
;
%macro ExceptionWithErrorCode 1
	push dword %1
	jmp _i386ExceptionEntry
%endmacro

;
; Calls exception handler with
; a NULL error code.
;
%macro ExceptionWithoutErrorCode 1
	push dword 0
	push dword %1
	jmp _i386ExceptionEntry
%endmacro

;
; Exceptions.
;
_i386DivideByZero:           ExceptionWithoutErrorCode(0)
_i386DebugException:         ExceptionWithoutErrorCode(1)
_i386NMIException:           ExceptionWithoutErrorCode(2)
_i386Breakpoint:             ExceptionWithoutErrorCode(3)
_i386Overflow:               ExceptionWithoutErrorCode(4)
_i386BoundException:         ExceptionWithoutErrorCode(5)
_i386InvalidOpcode:          ExceptionWithoutErrorCode(6)
_i386FPUNotAvailable:        ExceptionWithoutErrorCode(7)
_i386DoubleFault:            ExceptionWithErrorCode(8)
_i386CoprocessorSegment:     ExceptionWithoutErrorCode(9)
_i386InvalidTss:             ExceptionWithErrorCode(10)
_i386SegmentNotPresent:      ExceptionWithErrorCode(11)
_i386StackException:         ExceptionWithErrorCode(12)
_i386GeneralProtectionFault: ExceptionWithErrorCode(13)
_i386PageFault:	             ExceptionWithErrorCode(14)
_i386CoprocessorError:       ExceptionWithoutErrorCode(15)
_i386AlignmentCheck:         ExceptionWithoutErrorCode(16)
_i386MachineCheck:           ExceptionWithoutErrorCode(17)

;=== Interrupt Vector Table ================================

global _i386LoadInterruptTable

_i386LoadInterruptTable:

%define IDTR_POINTER [esp+4]

	mov ebx, IDTR_POINTER
	lidt [ebx]
	ret
