/************************************************************************
*
*	Neptune Debugger Client
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>

/* This component implements the NDBG NBOOT support. */

//https://www.kernel.org/pub/linux/kernel/people/marcelo/linux-2.4/arch/mips/kernel/gdb-stub.c

/*
	For compatibility with GDB, NDBG communicates using the same command set.
*/

/*
 * command          function                               Return value
 *
 *    g             return the value of the CPU registers  hex data or ENN
 *    G             set the value of the CPU registers     OK or ENN
 *
 *    mAA..AA,LLLL  Read LLLL bytes at address AA..AA      hex data or ENN
 *    MAA..AA,LLLL: Write LLLL bytes at address AA.AA      OK or ENN
 *
 *    c             Resume at current address              SNN   ( signal NN)
 *    cAA..AA       Continue at address AA..AA             SNN
 *
 *    s             Step one instruction                   SNN
 *    sAA..AA       Step one instruction from AA..AA       SNN
 *
 *    k             kill
 *
 *    ?             What was the last sigval ?             SNN   (signal NN)
 *
 *    bBB..BB	    Set baud rate to BB..BB		   OK or BNN, then sets
 *							   baud rate
 * All commands and responses are sent with a packet which includes a
 * checksum.  A packet consists of
 *
 * $<packet info>#<checksum>.
 *
 * where
 * <packet info> :: <characters representing the command or response>
 * <checksum>    :: < two hex digits computed as modulo 256 sum of <packetinfo>>
 *
 * When a packet is received, it is first acknowledged with either '+' or '-'.
 * '+' indicates a successful transfer.  '-' indicates a failed transfer.
 *
 * Example:
 *
 * Host:                  Reply:
 * $m0,10#2a               +$00010203040506070809101112131415#42
*/

/*
 * BUFMAX defines the maximum number of characters in inbound/outbound buffers
 * at least NUMREGBYTES*2 are needed for register packets
 */
#define BUFFER_MAX 400

STATIC const char hexchars[]="0123456789abcdef";

static int hex(unsigned char ch)
{
	if (ch >= 'a' && ch <= 'f')
		return ch - 'a' + 10;
	if (ch >= '0' && ch <= '9')
		return ch - '0';
	if (ch >= 'A' && ch <= 'F')
		return ch - 'A' + 10;
	return -1;
}

/*
* Convert the memory pointed to by mem into hex, placing result in buf.
* Return a pointer to the last char put in buf (null), in case of mem fault,
* return 0.
* may_fault is non-zero if we are reading from arbitrary memory, but is currently
* not used.
*/
static unsigned char *mem2hex(char *mem, char *buf, int count, int may_fault)
{
	unsigned char ch;
	int i;

	for (i = 0; i < count; i++) {
		ch = *mem++;
		*buf++ = hexchars[ch >> 4];
		*buf++ = hexchars[ch % 16];
	}

	*buf = 0;
	return buf;
}

/*
* convert the hex array pointed to by buf into binary to be placed in mem
* return a pointer to the character AFTER the last byte written
* may_fault is non-zero if we are reading from arbitrary memory, but is currently
* not used.
*/
static char *hex2mem(char *buf, char *mem, int count, int may_fault)
{
	int i;
	unsigned char ch;

	for (i = 0; i<count; i++)
	{
		ch = hex(*buf++) << 4;
		ch |= hex(*buf++);
		*mem++ = ch;
	}

	return mem;
}

static int hex2int(char **ptr, int *intValue)
{
	int numChars = 0;
	int hexValue;

	*intValue = 0;

	while (**ptr) {
		hexValue = hex(**ptr);
		if (hexValue < 0)
			break;

		*intValue = (*intValue << 4) | hexValue;
		numChars++;

		(*ptr)++;
	}

	return (numChars);
}

/*==========================================================

	NDBG Stub <> Serial Driver Interface

===========================================================*/

/**
*	Write character to serial device.
*	\param c Character
*	\ret Status code
*/
STATIC status_t DbgSendChar (char c) {

	BlrDebugWrite(1, &c);
	return NBL_STAT_SUCCESS;
}

/**
*	Read character from serial device.
*	\ret Character
*/
STATIC char DbgGetChar (void) {

	char c;
	BlrDebugRead(1, &c);
	return c;
}

/* $<data>#<checksum> */
STATIC void DbgGetPacket(char* buffer, size_t maxBytes) {

	unsigned char checksum;
	unsigned char xmitcsum;
	unsigned char ch;
	index_t count;
	int i;

	/* $<packet information>#<checksum> */
	do {

		/* waits for start character. */
		while ((ch = (DbgGetChar() & 0x7f)) != '$')
			;

		count = 0;
		checksum = 0;
		xmitcsum = -1;

		/* read unti end character (#) or end of buffer. */
		while (count < maxBytes) {
			ch = DbgGetChar() & 0x7f;
			if (ch == '#')
				break;
			checksum += ch;
			buffer[count] = ch;
			count++;
		}

		/* if greater then output buffer, ignore this packet. */
		if (count >= maxBytes) {
			continue;
		}

		buffer[count] = 0;

		if (ch == '#') {

			xmitcsum = hex(DbgGetChar() & 0x7f) << 4;
			xmitcsum |= hex(DbgGetChar() & 0x7f);

			if (checksum != xmitcsum)
				DbgSendChar('-');
			else
				DbgSendChar('+');

			/* if a sequence character is present, reply with
			the sequence id. */

			if (buffer[2] == ':') {

				DbgSendChar(buffer[0]);
				DbgSendChar(buffer[1]);

				/* remove sequence characters from buffer. */
				count = BlrStrLength(buffer);
				for (i = 3; i <= count; i++)
					buffer[i - 3] = buffer[i];
			}
		}

		BlrPrintf("\n\r%x == %x", checksum, xmitcsum);
	} while (checksum != xmitcsum);
}

/* $<data>#<checksum> */
STATIC void DbgSendPacket(char* buffer) {

	unsigned char c;
	unsigned char checksum;
	int count;

	/* $<packet information>#<checksum> */
	do {
		checksum = 0;
		count = 0;

		DbgSendChar('$');
		while ((c = buffer[count]) != 0) {
			DbgSendChar(c);
			checksum += c;
			count += 1;
		}

		DbgSendChar('#');
		DbgSendChar(hexchars[checksum >> 4]);
		DbgSendChar(hexchars[checksum & 0xf]);

	} while (DbgGetChar() != '+');
}

/*==========================================================

	NDBG Stub Core

===========================================================*/

/* gets called when nboot raises a breakpoint exception. This
can happen from a failed assertion or a debugger requested breakpoint
instruction. */
void DbgBreakpoint(void) {


}

/* issue debug print request. */
PUBLIC void DbgPrint(char* s) {

	DbgSendPacket(s);
}

typedef struct NdbgRegs32 {
	uint32_t esp, ebp, esi, edi;
	uint32_t eax, ebx, ecx, edx;
	uint32_t eip;
	uint32_t cs, ds, es, ss, fs, gs;
	uint32_t eflags;
}NdbgRegs32;

/* initialize debugger. Note that this is called directly by NBOOT
and must not be a driver service. */
PUBLIC void DbgInitialize(void) {

	/* install traps. */
//	HalInitializeIdt();

	/* here we need to check if the debugger
	was started before us. If it was,
	read in any packets now and send an ack to
	the debugger. */
}

/* The order of this structure is defined by i386ExceptionEntry. Careful when modifying. */
typedef struct _HalExceptionContext {
	/* segments. */
	uint32_t gs, fs, es, ds, ss;
	/* pushad */
	uint32_t eax, ecx, edx, ebx, esp, ebp, esi, edi;
	/* cpu context */
	uint32_t vector;
	uint32_t errorCode;
	uint32_t eip;
	uint32_t cs;
	uint32_t eflags;
}HalExceptionContext;

/* given an exception context, display its information. */
PUBLIC void DbgDumpExceptionContext(HalExceptionContext* context) {

	BlrPrintf("\n\r ds: %x fs: %x gs: %x es: %x", context->ds, context->fs, context->gs, context->es);
	BlrPrintf("\n\r cs:eip %x:%x", context->cs, context->eip);
	BlrPrintf("\n\r ss:esp %x:%x", context->ss, context->esp);
	BlrPrintf("\n\r flags: %x vector: %x errorCode: %x", context->eflags, context->vector, context->errorCode);
	BlrPrintf("\n\r eax: %x ebx: %x ecx: %x edx: %x", context->eax, context->ebx, context->ecx, context->edx);
	BlrPrintf("\n\r esi: %x edi: %x ebp: %x", context->esi, context->edi, context->ebp);
}

char input[BUFFER_MAX];
char output[BUFFER_MAX];

/* called when nboot raises an exception. */
PUBLIC void DbgExceptionHandler(uint32_t nested, HalExceptionContext* context) {

	char* ptr;
	int addr;
	int length;

	/* initialize communications port. */
//	DbgSerialInitialize();

	/* display register state. */
	DbgDumpExceptionContext(context);

	/* trap code. */
	ptr = output;
	*ptr++ = 'T';
	*ptr++ = hexchars[(context->vector & 0xf0) >> 4];
	*ptr++ = hexchars[context->vector & 0xf];

	/* faulting instruction. */
	*ptr++ = ':';
	ptr = mem2hex((char*)&context->eip, ptr, 4, 0);
	*ptr++ = ';';

	/* stack pointer. */
	ptr = mem2hex((char*)&context->esp, ptr, 4, 0);
	*ptr++ = ';';
	*ptr++ = 0;

	/* send trap information. */
	DbgSendPacket(output);

	while (TRUE) {

		/* get response. */
		DbgGetPacket(input, BUFFER_MAX);

		switch (input[0]) {

			/* get exception vector. */
			case '?':
				output[0] = '$';
				output[1] = hexchars[(context->vector & 0xf0) >> 4];
				output[2] = hexchars[context->vector & 0xf];
				output[3] = 0;
				break;

			/* toggle debug flag. */
			case 'd':
				break; /* ignored. */

			/* get cpu registers. */
			case 'g':
				mem2hex((char*)context, output, sizeof(HalExceptionContext), 0);
				break;

			/* set cpu registers. Note we assume ESP has not changed. */
			case 'G':
				ptr = &input[1];
				hex2mem(ptr, (char*)context, sizeof(HalExceptionContext), 0);
				DbgDumpExceptionContext(context);
				BlrStrCopy(output, "OK");
				break;

			/* mAA..AA,LLLL read LLLL bytes at address AA..AA. */
			case 'm':
				ptr = &input[1];
				if (hex2int(&ptr, &addr) && *ptr++ == ',' && hex2int(&ptr, &length)) {
					if (mem2hex((char*)addr, output, length, 1))
						break;
					BlrStrCopy(output, "E03");
				}
				else
					BlrStrCopy(output, "E01");
				break;

			/* MAA..AA,LLLL: write LLLL bytes at address AA..AA. */
			case 'M':
				ptr = &input[1];
				if (hex2int(&ptr, &addr) && *ptr++ == ',' && hex2int(&ptr, &length) && *ptr++ == ':') {
					if (hex2mem(ptr, (char*)addr, length, 1))
						BlrStrCopy(output, "OK");
					else
						BlrStrCopy(output, "E03");
				}else
					BlrStrCopy(output, "E02");
				break;

			/* c resume at current address */
			case 'c':
				return;

			/* kill program. */
			case 'k':
				break; /* ignored. */

			/* reset machine. */
			case 'r':
				break;
		};

		/* reply to the request. */
		DbgSendPacket(output);
	}
}

/*==========================================================

	NDBG Stub <> NBL Interface

===========================================================*/

/**
*	Debugger entry point.
*	\param self Pointer to own device object
*	\param dependencyCount Number of dependencies
*	\param dependencies Dependency list
*/
status_t DbgEntryPoint (IN nblDriverObject* self,IN int dependencyCount,IN nblDriverObject* dependencies[]) {
	return NBL_STAT_SUCCESS;
}

/**
*	Request callback.
*	\param rpb Request Paramater Block
*	\ret Status code
*/
status_t DbgRequest (IN nblMessage* rpb) {

	switch (rpb->type) {
		case NBL_DBG_BREAK:
			{
				DbgBreakpoint();
				return NBL_STAT_SUCCESS;
			}
		default:
			return NBL_STAT_UNSUPPORTED;
	};
}

#define NBL_DBG_VERSION 1

/**
*	Driver object.
*/
NBL_DRIVER_OBJECT_DEFINE _ndbgDriverObject = {
	NBL_DRIVER_MAGIC,
	NBL_DBG_VERSION,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_DBG,
	"ndbg",
	DbgEntryPoint,
	DbgRequest,
	0
	/* This driver does rely on the serial driver. */
};

// https://opensource.apple.com/source/gdb/gdb-228/src/gdb/i386-stub.c

