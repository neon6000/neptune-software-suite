/************************************************************************
*
*	bdf.c Glyph Bitmap Distribution Format support
* 
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "font.h"
#include "gfx.h"

static uint8_t _unknownGlyph [] =
{
  /*       76543210 */
  0x7C, /*  ooooo   */
  0x82, /* o     o  */
  0xBA, /* o ooo o  */
  0xAA, /* o o o o  */
  0xAA, /* o o o o  */
  0x8A, /* o   o o  */
  0x9A, /* o  oo o  */
  0x92, /* o  o  o  */
  0x92, /* o  o  o  */
  0x92, /* o  o  o  */
  0x92, /* o  o  o  */
  0x82, /* o     o  */
  0x92, /* o  o  o  */
  0x82, /* o     o  */
  0x7C, /*  ooooo   */
  0x00  /*          */
};

STATIC int BdfHexToInt (char s[]) {
    int hexdigit, i, inhex, n;    
    i=0;
    if(s[i] == '0') {
        ++i;
        if(s[i] == 'x' || s[i] == 'X'){            
            ++i;
        }
    }
     
    n = 0;
    inhex = TRUE; 
    for(; inhex == TRUE; ++i) {
        if(s[i] >= '0' && s[i] <= '9') {            
            hexdigit = s[i] - '0';
        } else if(s[i] >= 'a' && s[i] <= 'f') {            
            hexdigit = s[i] - 'a' + 10;
        } else if(s[i] >= 'A' && s[i] <= 'F') {
            hexdigit = s[i] - 'A' + 10;
        } else {
            inhex = FALSE;
        }
         
        if(inhex == TRUE) {
            n = 16 * n + hexdigit;
        }
    }
     
    return n;
}

/**
*	Read next token.
*/
STATIC char* gettok (char* p) {
	return BlrStrToken (p, " \t\n\r");
//	return strtok(p, " \t\n\r");
}

/**
*	Load global font info
*/
STATIC void BlLoadFontInfo (blFont* out) {

	/* buffer for line. */
	char line [1024];

	/* read until either EOF or CHARs section.
	which marks end of global font info. */
	while (! BlrFileEnd (out->file)) {

		char* s = 0;

		/* get next line. */
		BlrFileGetString (line, 1024, out->file);

		/* get property. */
		s = gettok(line);

		/* we now extract what we need. We skip
		over properties that are not supported. */
		if (BlrStrCompare (s, "FONT_ASCENT") == 0) {
			s = gettok(0);
			out->ascent = BlrStrToInt(s);
			continue;
		}
		if (BlrStrCompare (s, "FONT_DESCENT") == 0) {
			s = gettok(0);
			out->descent = BlrStrToInt (s);
			continue;
		}
		if (BlrStrCompare (s, "FAMILY_NAME") == 0) {
			s = gettok(0);
			out->family = BlrStrDup (s);
			continue;
		}
		if (BlrStrCompare (s, "WEIGHT_NAME") == 0) {
			s = gettok(0);
			if(BlrStrCompare (s, "\"Medium\"") == 0)
				out->weight = FONT_WEIGHT_NORMAL;
			else if (BlrStrCompare(s, "\"Bold\"") == 0)
				out->weight = FONT_WEIGHT_BOLD;
			continue;
		}
		if (BlrStrCompare (s, "SIZE") == 0) {
			s = gettok(0);
			out->pointSize = BlrStrToInt (s);
			s = gettok(0);
			out->xres  = BlrStrToInt (s);
			s = gettok(0);
			out->yres  = BlrStrToInt (s);
			continue;
		}
		if (BlrStrCompare (s, "FONT") == 0) {
			s = gettok(0);
			out->name = BlrStrDup (s);
			continue;
		}
		if (BlrStrCompare (s, "FONTBOUNDINGBOX") == 0) {
			s = gettok(0);
			out->BBoxWidth = BlrStrToInt (s);
			s = gettok(0);
			out->BBoxHeight = BlrStrToInt (s);
			continue;
		}
		if (BlrStrCompare (s, "FAMILYNAME") == 0) {
			s = gettok(0);
			out->family = BlrStrDup (s);
			continue;
		}
		if (BlrStrCompare (s, "CHARS") == 0) {
			s = gettok(0);
			out->glyphCount = BlrStrToInt (s);

			/* Global font information ends here. */
			break;
		}
	}
}

/**
*	Build index list for fast lookup.
*/
STATIC void BlBuildIndices (blFont* font) {

	/* current index. */
	size_t index;

	/* buffer for line. */
	char line [1024+1];

	/* allocate index list. */
	font->indices = BlrAlloc (font->glyphCount * sizeof (blGlyphIndex));
	if (!font->indices) {
		BlrPrintf("\n\rBlBuildIndices ALLOC FAIL");
		return;
	}

	index = 0;

	/* read until either EOF or CHARs section.
	which marks end of global font info. */
	while (! BlrFileEnd (font->file) ) {

		char* s = 0;

		/* get current offset. */
		uint32_t offset = (uint32_t) BlrFileTell (font->file);

		/* get next line. */
		BlrFileGetString (line, 1024, font->file);

		/* get property. */
		s = gettok(line);

		if (BlrStrCompare (s, "ENDFONT") == 0) {
			/* no more characters. */
			break;
		}
		if (BlrStrCompare (s, "STARTCHAR") == 0) {

			/* get code point. */
			uint32_t code = 0;

			while (! BlrFileEnd (font->file) ) {

				BlrFileGetString (line, 1024, font->file);
				s = gettok(line);
				if (!s)
					break;

				if (BlrStrCompare (s, "ENCODING") == 0) {
					s = gettok(0);
					code = BlrStrToInt (s);
					if (code == -1) {
						/* non standard encoding. next value
						is code point in other encoding scheme. */
						char* p = gettok(0);
						if (!p) {
							/* some fonts do not have any number despite spec,
							so just keep -1. */
							code = -1;
						}else{
							code =  BlrStrToInt(p);
						}
					}
				}
				if (BlrStrCompare (s, "ENDCHAR") == 0) {
					/* end of character info. */
					break;
				}
			}

			/* add character to index list. */
			font->indices[index].code   = code;
			font->indices[index].offset = offset;
			font->indices[index].cache  = 0;
			index++;

			/* go to next character. */
			continue;
		}
	}
}

/**
*	Code point -> Glyph
*/
blGlyphIndex* BlFontGetIndex (blFont* font, uint32_t code) {

	int first, last, middle;

	first  = 0;
	last   = font->glyphCount - 1;
	middle = (first+last)/2;

	/* binary search. */
	while (first <= last) {
		if (font->indices [middle].code < code)
			first = middle + 1;
		else if (font->indices [middle].code == code)
			return &font->indices [middle];
		else
			last = middle - 1;
		middle = (first + last) / 2;
	}
	return 0;
}

/**
*	Get glyph from file.
*/
blGlyph* BlFontLoadGlyph (blFont* font, int code) {

	blGlyphIndex* index;
	blGlyph*      glyph;
	char          line [1024+1];

	/* get index into map. */
	index = BlFontGetIndex (font, code);
	if (!index)
		return 0;

	/* if glyph was already loaded, return it. */
	if (index->cache != 0)
		return index->cache;

	/* seek to offset of glyph. */
	BlrFileSeek (font->file, index->offset, NBL_SEEK_SET);

	/* allocate glyph. */
	glyph = BlrAlloc (sizeof (blGlyph));
	BlrZeroMemory (glyph, sizeof(blGlyph));

	/* load glyph data. */
	while (!BlrFileEnd (font->file)) {

		char* s = 0;
		BlrZeroMemory (line, 1024);
		BlrFileGetString (line, 1024, font->file);
		s = gettok(line);
		if (!s)
			break;

		if (BlrStrCompare (s, "STARTCHAR") == 0) {
			/* STARTCHAR glyph-name -- don't really care to store. */
			continue;
		}
		if (BlrStrCompare (s, "ENCODING") == 0) {
			s = gettok(0);
			glyph->encoding = BlrStrToInt(s);
			if (code == -1) {
				/* non standard encoding. next value
				is code point in other encoding scheme. */
				s = gettok(0);
				glyph->encoding = BlrStrToInt(s);
			}
			continue;
		}
		if (BlrStrCompare (s, "SWIDTH") == 0) {
			s = gettok(0);
			glyph->swx0 = BlrStrToInt (s);
			s = gettok(0);
			glyph->swy0 = BlrStrToInt (s);
			continue;
		}
		if (BlrStrCompare (s, "SWIDTH1") == 0) {
			s = gettok(0);
			glyph->swx1 = BlrStrToInt (s);
			s = gettok(0);
			glyph->swy1 = BlrStrToInt (s);
			continue;
		}
		if (BlrStrCompare (s, "DWIDTH") == 0) {
			s = gettok(0);
			glyph->dwx0 = BlrStrToInt (s);
			s = gettok(0);
			glyph->dwy0 = BlrStrToInt (s);
			continue;
		}
		if (BlrStrCompare (s, "DWIDTH1") == 0) {
			s = gettok(0);
			glyph->dwx1 = BlrStrToInt (s);
			s = gettok(0);
			glyph->dwy1 = BlrStrToInt (s);
			continue;
		}
		if (BlrStrCompare (s, "BBX") == 0) {
			s = gettok(0);
			glyph->bbWidth = BlrStrToInt (s);
			s = gettok(0);
			glyph->bbHeight = BlrStrToInt (s);
			s = gettok(0);
			glyph->bbOffsetX = BlrStrToInt (s);
			s = gettok(0);
			glyph->bbOffsetY = BlrStrToInt (s);
			continue;
		}
		if (BlrStrCompare (s, "BITMAP") == 0) {

			unsigned char* buffer  = 0;

			/* counts number of bytes to allocate for bitmap. */
			uint32_t size = 0;

			/* section must appear after bbx. */
			int h  = glyph->bbHeight;

			/* need to guess size and over compensate. */
			buffer = BlrAlloc ( glyph->bbHeight * glyph->bbWidth );

			/* read until height of bitmap. */
			while (h--) {
				unsigned int i = 0;
				BlrFileGetString (line, 1024, font->file);
				/* read until EOL */
				while (i < BlrStrLength (line) ) {
					long     value;
					char     buf[2];
					/* read every two bytes and convert to binary. */
					buf[0] = line [ i ];
					buf[1] = line [ i+1 ];

		//			value  = strtol(buf, 0, 16);
					value  = BdfHexToInt (buf);

					/* copy to buffer. */
					buffer [size++] = (unsigned char) value;
					/* we read two bytes. */
					i += 2;
				}
			}

			/* now copy over bitmap and free buffer. */
			glyph->bitmap = BlrAlloc (size);
			BlrCopyMemory (glyph->bitmap, buffer, size);
			BlrFree (buffer);

			/* some fonts dont override font height. */
			if (glyph->bbHeight != 0) {
				glyph->bytesPerScanLine = size / glyph->bbHeight;
			}
			else {
				glyph->bytesPerScanLine = size / font->BBoxHeight;
			}

			/* cache it. */
			index->cache = glyph;
			continue;
		}
		if (BlrStrCompare (s, "ENDCHAR") == 0) {
			/* done. */
			break;
		}
	}
	return glyph;
}

/**
*	Load font
*/
int BlFontLoadBDF (char* name, blFont* out) {

	/* attempt to open file. */
	nblFile* file = BlrFileOpen (name);
	if (!file)
		return FALSE;

	/* prepare font. */
	BlrZeroMemory (out, sizeof (blFont));
	out->file = file;

	/* load font info. */
	BlLoadFontInfo (out);

	/* build indices for quick lookup. */
	BlBuildIndices(out);

	/* we are done. */
	return TRUE;
}

/**
*	Draw graph to surface.
*/
void BlDrawGlyph (blGlyph* glyph, nblColor c1, nblColor c2, int x, int baseline, nblSurface* out) {

	nblSurface image;
	int realx;
	int realy;
	int realw;
	int realh;

//status_t BlCreateSurface (OUT nblSurface* out, IN int width, IN int height, IN int bpp,
//		IN uint32_t rmask, IN uint32_t gmask, IN uint32_t bmask, IN uint32_t amask)

	/* create 1 bit surface for entire glyph. */
	BlCreateSurface(&image, glyph->bytesPerScanLine * 8, glyph->bbHeight,1,0,0,0,0);

//	BlCreateSurface(&image, glyph->bbWidth, glyph->bbHeight,1,0,0,0,0);

	/* need to adjust pitch to actual width. */
	image.pitch = image.width;

	image.format.back.red   = c2.red;
	image.format.back.green = c2.green;
	image.format.back.blue  = c2.blue;
	image.format.back.alpha = c2.alpha;

	image.format.front.red   = c1.red;
	image.format.front.green = c1.green;
	image.format.front.blue  = c1.blue;
	image.format.front.alpha = c1.alpha;

	image.pixels = glyph->bitmap;

	realx = x;
	realy = baseline;
	realw = glyph->bbWidth;
	realh = glyph->bbHeight;

	realx += glyph->bbOffsetX;
	realy += glyph->bbOffsetY;

	/* draw glyph. */
	BlBlendBlit (&image, out, realx, realy, realw, realh, 0, 0);
}

void BlDrawString (char* s, blFont* font,nblColor c1,nblColor c2,
				   int x, int baseline,nblSurface* out) {

	unsigned int c;
	int realx = 0;
	for (c = 0; c < BlrStrLength (s); c++) {

		blGlyph* glyph;
		uint32_t code;

		code = s [c];
		glyph = BlFontLoadGlyph(font,code);
		if (glyph) {
			BlDrawGlyph(glyph,c1,c2,x + realx, baseline, out);
			realx += glyph->dwx0;
		}
	}
}

void BlDrawWideString (wchar_t* s, blFont* font,nblColor c1,nblColor c2,
				   int x, int baseline,nblSurface* out) {

	unsigned int c;
	int realx = 0;
	for (c = 0; c < BlrWideStrLength (s); c++) {

		blGlyph* glyph;
		uint32_t code;

		code = s [c];
		glyph = BlFontLoadGlyph(font,code);
		if (glyph) {
			BlDrawGlyph(glyph,c1,c2,x + realx, baseline, out);
			realx += glyph->dwx0;
		}
	}
}

void BlDrawCharacter (char s, blFont* font,nblColor c1,nblColor c2,
				   int x, int baseline,nblSurface* out) {

	int realx = 0;
	blGlyph* glyph;
	uint32_t code;

	code = s;
	glyph = BlFontLoadGlyph(font,code);
	if (glyph) {
		BlDrawGlyph(glyph,c1,c2,x + realx, baseline, out);
		realx += glyph->dwx0;
	}
}
