/************************************************************************
*
*	NBOOT Graphics Terminal.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nbl.h>
#include <key.h>
#include "font.h"
#include "gfx.h"

/* map colors to NBL color type. */
struct {
	gfxColor name; nblColor col;
} _colors [] = {
	{ BLACK,     {0,  0,  0,  0} },
	{ BLUE,      {0,  0,  128,0} },
	{ GREEN,     {0,  128,0,  0} },
	{ CYAN,      {0,  128,128,0} },
	{ RED,       {128,0,  0,  0} },
	{ MAGENTA,   {128,0,  128,0} },
	{ BROWN,     {128,128, 0, 0} },
	{ LGRAY,     {192,192,192,0} },
	{ GRAY,      {128,128,128,0} },
	{ LBLUE,     {0,  0,  128,0} },
	{ LGREEN,    {0,  255,0,  0} },
	{ LCYAN,     {0,  255,255,0} },
	{ LRED,      {255,0,  0,  0} },
	{ LMAGENTA,  {255,0,  255,0} },
	{ YELLOW,    {255,255,0,  0} },
	{ WHITE,     {255,255,255,255} },
};

/* console information. */
typedef struct gfxConInfo {
	nblPoint cursor;
	nblPoint size;
	BOOL     cursorEnable;
	nblColor back;
	nblColor front;
	uint32_t attrib;
}gfxConInfo;

/* device extended attributes. */
typedef struct gfxConEx {
	nblSurface* draw;
	nblSurface* wallpaper;
	blFont*     font;
	gfxConInfo  info;
}gfxConEx;

/* forward declaration for driver object. */
NBL_DRIVER_OBJECT_DEFINE _nGfxConDriverObject;

/* this driver supports file devices. */
nblFile _gfxFile = {
	0,0,0,&_nGfxConDriverObject,0,0,0,0,0,0,0,0
};

/* this driver only needs one device object. */
STATIC nblDeviceObject _gfxDeviceObject;

/* forward decls. */
status_t BlGfxConOutEntryPoint (IN nblDriverObject* self, IN int dependencyCount,
						 IN nblDriverObject* dependencies[]);
status_t BlGfxRequest (IN nblMessage* rpb);

/* enable or disable cursor. */
STATIC status_t BlGfxCursorEnable (IN BOOL enable) {

	gfxConEx* ex = (gfxConEx*)_gfxDeviceObject.ext;
	if (ex) {
		ex->info.cursorEnable = enable;
		return NBL_STAT_SUCCESS;
	}
	return NBL_STAT_DEVICE;
}

/* returns cursor location. */
STATIC uint32_t BlGfxGetXY (void) {

	gfxConEx* ex = (gfxConEx*)_gfxDeviceObject.ext;
	if (ex)
		return (ex->info.cursor.x << 8) | ex->info.cursor.y;
	return 0;
}

/* sets cursor location. */
STATIC status_t BlGfxSetXY (uint32_t x, uint32_t y) {
	gfxConEx* ex = (gfxConEx*)_gfxDeviceObject.ext;
	if (ex) {
		ex->info.cursor.x = x;
		ex->info.cursor.y = y;
		return NBL_STAT_SUCCESS;
	}
	return NBL_STAT_DEVICE;
}

/* returns cursor status. */
STATIC BOOL BlGfxGetCursor (void) {
	gfxConEx* ex = (gfxConEx*)_gfxDeviceObject.ext;
	if (ex)
		return ex->info.cursorEnable;
	return FALSE;
}

/* set attribute. */
STATIC status_t BlGfxSetAttribute (uint32_t fcol, uint32_t bcol) {

	gfxConEx* ex = (gfxConEx*)_gfxDeviceObject.ext;

	if (fcol > WHITE || bcol > WHITE)
		return NBL_STAT_INVALID_ARGUMENT;

	if (ex) {
		ex->info.front  = _colors [fcol].col;
		ex->info.back   = _colors [bcol].col;
		ex->info.attrib = (bcol << 4) | fcol;
		return NBL_STAT_SUCCESS;
	}
	return NBL_STAT_DEVICE;
}

/* get attribute. */
STATIC uint32_t BlGfxGetAttribute (void) {
	gfxConEx* ex = (gfxConEx*)_gfxDeviceObject.ext;
	if (ex)
		return ex->info.attrib;
	return 0;
}

/* scroll console. */
STATIC void BlGfxScroll (void) {

}

/* standard print string method. */
STATIC status_t BlGfxPrintString (IN char* str) {

	gfxConEx* ex = (gfxConEx*)_gfxDeviceObject.ext;
	char s[2];

	if (!ex)
		return NBL_STAT_DEVICE;

	s[1] = 0;
	while (*str) {

		s[0] = *str++;
		switch(s[0]) {
			case '\n':
				if (ex->info.cursor.y <= ex->info.size.y)
					ex->info.cursor.y++;
				else
					; /* scroll. */
				break;
			case '\r':
				ex->info.cursor.x = 0;
				break;
			case '\b': {
				/* never hit. */
				BlDrawString (" ",ex->font,
					ex->info.front,
					ex->info.back,
					ex->info.cursor.x * ex->font->BBoxWidth,
					(ex->info.cursor.y * ex->font->BBoxHeight) - ex->font->BBoxHeight,
					ex->draw);
				if (ex->info.cursor.x > 0)
					ex->info.cursor.x--;
				}
			   break;
		}

		BlDrawString (s,
			ex->font,
			ex->info.front,
			ex->info.back,
			ex->info.cursor.x * ex->font->BBoxWidth,
			(ex->info.cursor.y * ex->font->BBoxHeight) - ex->font->BBoxHeight,
			ex->draw);

		ex->info.cursor.x++;
		if (ex->info.cursor.x > ex->info.size.x) {
			/* go to next line. */
			ex->info.cursor.x = 0;
			if (ex->info.cursor.y <= ex->info.size.y)
				ex->info.cursor.y++;
			else
				; /* scroll. */
		}
	}

	return NBL_STAT_SUCCESS;
}

/* clear screen. */
STATIC status_t BlGfxClearScreen (uint32_t fcol, uint32_t bcol) {

	return NBL_STAT_SUCCESS;
}

/* the termainal file device can be used to write to the console. */
STATIC uint32_t BlGfxWrite (IN nblFile* file, IN size_t size, OUT char* buffer) {
	BlGfxPrintString (buffer);
	return BlrStrLength (buffer);
}

/* console output device object. */
STATIC nblDeviceObject _gfxDeviceObject = {
	NBL_DEVICE_TERMINAL_PERIPHERAL,
	NBL_DEVICE_PERIPHERAL_CLASS,
	0, &_nGfxConDriverObject,
	&_nGfxConDriverObject, 0, "gfxout", 0
};

/* attempt to open device as file object. */
STATIC nblFile* BlGfxOpen (IN char* path, IN nblDeviceObject* dev) {

	/* only operate on our own device. */
	if (dev != &_gfxDeviceObject)
		return NULL;

	/* Set standard streams and return file device object. */
	nbl_stdout = &_gfxFile;
	nbl_stderr = &_gfxFile;
	return nbl_stdout;
}

/* initialize device. */
STATIC status_t BlGfxInit (nblSurface* draw, blFont* font) {

	gfxConEx* ex = (gfxConEx*)_gfxDeviceObject.ext;

	if (!ex)
		return NBL_STAT_DRIVER;

	ex->draw = draw;
	ex->font = font;

	ex->info.front = _colors [WHITE].col;
	ex->info.back = _colors [BLACK].col;
	ex->info.size.x = 80;
	ex->info.size.y = 25;

	ex->info.cursor.x = 1;
	ex->info.cursor.y = 1;

	return NBL_STAT_SUCCESS;
}

/* driver requests. */
status_t BlGfxRequest (IN nblMessage* rpb) {
	rettype_t r;
	r = 0;
	switch (rpb->type) {
		case NBL_CONOUT_INIT:
			r = (rettype_t) BlGfxInit ((nblSurface*) rpb->NBL_CONOUT_SURFACE, (blFont*) rpb->NBL_CONOUT_FONT);
			break;
		case NBL_CONOUT_PRINT:
			r = (rettype_t) BlGfxPrintString ((char*) rpb->NBL_CONOUT_PRINT_STR);
			break;
		case NBL_FS_WRITE:
			r = (rettype_t) BlGfxWrite ((nblFile*) rpb->NBL_FS_WRITE_FILE,
				rpb->NBL_FS_WRITE_SIZE, (char*) rpb->NBL_FS_WRITE_BUFFER);
			break;
		case NBL_FS_OPEN:
			r = (rettype_t) BlGfxOpen ((char*) rpb->NBL_FS_OPEN_PATH,
				(nblDeviceObject*) rpb->NBL_FS_OPEN_DEVICE);
			break;
		case NBL_CONOUT_CURSOR:
			r = (rettype_t) BlGfxCursorEnable ((BOOL)rpb->NBL_CONOUT_CURSOR_ENABLE);
			break;
		case NBL_CONOUT_GETXY:
			r = (rettype_t) BlGfxGetXY ();
			break;
		case NBL_CONOUT_SETXY:
			r = (rettype_t) BlGfxSetXY (rpb->NBL_CONOUT_SET_X, rpb->NBL_CONOUT_SET_Y);
			break;
		case NBL_CONOUT_GETCURSOR:
			r = (rettype_t) BlGfxGetCursor ();
			break;
		case NBL_CONOUT_CLRSCR:
			r = (rettype_t) BlGfxClearScreen (rpb->NBL_CONOUT_CLRSCR_FCOLOR, rpb->NBL_CONOUT_CLRSCR_BCOLOR);
			break;
		case NBL_CONOUT_ATTRIB:
			r = (rettype_t) BlGfxSetAttribute (rpb->NBL_CONOUT_ATTRIB_FCOLOR, rpb->NBL_CONOUT_ATTRIB_BCOLOR);
			break;
		case NBL_CONOUT_GETATTRIB:
			r = (rettype_t) BlGfxGetAttribute ();
			break;
		/* this terminal does not have any modes to reset to. */
		case NBL_CONOUT_SETMODE:
		case NBL_CONOUT_GETMODE:
		case NBL_CONOUT_RESET:
		default:
			return NBL_STAT_UNSUPPORTED;
	};
	rpb->retval = 0;
	return NBL_STAT_SUCCESS;
}

/* driver entry point. */
status_t BlGfxConOutEntryPoint (IN nblDriverObject* self, IN int dependencyCount,
						 IN nblDriverObject* dependencies[]) {

	nblDriverObject* dm;
	nblMessage rpb;

	return 0;

	/* dm should aways be present since its part of NBLC. */
	dm = dependencies[0];
	if (!dm)
		NBL_STAT_DEPENDENCY;

	/* allocate device data. It needs to be initialized later so
	we clear it for now. */
	_gfxDeviceObject.ext = BlrAlloc (sizeof (gfxConEx));
	if (!_gfxDeviceObject.ext)
		return NBL_STAT_DRIVER;
	BlrZeroMemory (_gfxDeviceObject.ext, sizeof (gfxConEx));

	/* create console device. */
	rpb.source = self;
	rpb.type   = NBL_DEV_REGISTER;
	rpb.NBL_DEV_REGISTER_OBJ = &_gfxDeviceObject;
	if (dm->request (&rpb) != NBL_STAT_SUCCESS)
		return NBL_STAT_DEPENDENCY;

	return NBL_STAT_SUCCESS;
}

/* Driver object. */
NBL_DRIVER_OBJECT_DEFINE _nGfxConDriverObject = {
	NBL_DRIVER_MAGIC,
	0,
	NBL_DRV_LOAD_CORE2,
	NBL_DRV_CLASS_CON,
	"gfxout",
	BlGfxConOutEntryPoint,
	BlGfxRequest,
	1,
	"dm"
};
