/************************************************************************
*
*	font.h Font support
* 
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

//http://partners.adobe.com/public/developer/en/font/5005.BDF_Spec.pdf

#ifndef FONT_H_INCLUDED
#define FONT_H_INCLUDED

#include "nbl.h"

/*
To convert the scalable width to the width in device pixels, multiply SWIDTH
times p/1000 times r/72, where r is the device resolution in pixels per inch.
The result is a real number giving the ideal width in device pixels.
*/

/**
* Glyph.
*/
typedef struct _blGlyph {
	/* code point. */
	int   encoding;
	/* device pixels. */
	int   dwx0;
	int   dwy0;
	int   dwx1;
	int   dwy1;
	/* scalable width and height. */
	int   swx0;
	int   swy0;
	int   swx1;
	int   swy1;
	/* bounding box. */
	int   bbWidth;
	int   bbHeight;
	int   bbOffsetY;
	int   bbOffsetX;
	/* width of scan line. */
	int   bytesPerScanLine;
	unsigned char* bitmap;
}blGlyph;

/**
* Maps code point to glyph offset.
*/
typedef struct _blGlyphIndex {
	uint32_t code;
	uint32_t offset;
	blGlyph* cache;
}blGlyphIndex;

#define FONT_WEIGHT_NORMAL 0
#define FONT_WEIGHT_BOLD   1

/**
* Font.
*/
typedef struct _blFont {
	char* name;
	nblFile* file;
	short pointSize;
	int   xres;
	int   yres;
	char* family;
	int   weight;
	int   BBoxWidth;
	int   BBoxHeight;
	short ascent;
	short descent;
	int   glyphCount;
	blGlyphIndex* indices;
}blFont;

#endif
