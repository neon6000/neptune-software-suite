;*******************************************************
;	rmode_enable.asm
;		switch the processor into real mode
;
;	Copyright 2008 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

section .text
bits 32

;=============================================;
;                                             ;
;	void rmode_enable (void)                  ;
;      Enables real mode                      ;
;                                             ;
;=============================================;

rmode_enable:
	cli
    mov eax, cr0
    and al, 0FEh
    mov cr0, eax
    ret
