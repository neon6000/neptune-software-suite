;*******************************************************
;	pmode_enable.asm
;		switch the processor into protected mode
;
;	Copyright 2008 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

section .text
bits 16

;=============================================;
;                                             ;
;	void pmode_enable (void)                  ;
;      Enables protected mode                 ;
;                                             ;
;=============================================;

pmode_enable:
    cli	
    mov    eax, cr0
    or     eax, 1
    mov    cr0, eax
	ret
