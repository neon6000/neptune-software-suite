;*******************************************************
;	gdt.asm
;		Global Descriptor Table
;
;	Copyright 2008-2016 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

;
; This is real mode code.
;
section .text
bits	16

;=============================================;
;                                             ;
;	void gdt_install (void)                   ;
;		-installs gdt into gdtr               ;
;                                             ;
;=============================================;

gdt_install:
    cli	
    pusha
    lgdt    [toc]	;; 0f01163d80 -> lgdt ds:0x803d
    sti
    popa
    ret

;=============================================;
;                                             ;
;	Global Descriptor Table                   ;
;                                             ;
;=============================================;

gdt_data: 
	dd 0 				; null descriptor
	dd 0 

; gdt code:				; code descriptor
	dw 0FFFFh 			; limit low
	dw 0 				; base low
	db 0 				; base middle
	db 10011010b 		; access
	db 11001111b 		; granularity
	db 0 				; base high

; gdt data:				; data descriptor
	dw 0FFFFh 			; limit low (Same as code)
	dw 0 				; base low
	db 0 				; base middle
	db 10010010b 		; access
	db 11001111b 		; granularity
	db 0				; base high

; gdt code:				; code descriptor (16bit)
	dw 0FFFFh 			; limit low
	dw 0x0 				; base low
	db 0 				; base middle
	db 10011010b 		; access
	db 00001111b 		; granularity
	db 0 				; base high

; gdt data:				; data descriptor (16bit)
	dw 0FFFFh 			; limit low (Same as code)
	dw 0x0 				; base low
	db 0 				; base middle
	db 10010010b 		; access
	db 00001111b 		; granularity
	db 0				; base high

	;GDT code
	
	dw		0FFFFFh						;Limit low
	dw		0							;Base low
	db		0							;Base middle
	db		11111010b					;Access
	db		11001111b					;Granularity
	db		0							;Base high
	

end_of_gdt:
toc: 
      dw end_of_gdt - gdt_data - 1   ; limit (Size of GDT) ;; 0x2f
      dd gdt_data                    ; base of GDT  ;; 0xd

; gdt selector offsets

%define NULL_SEL 0
%define CODE_SEL 8
%define DATA_SEL 0x10
%define CODE_SEL_16BIT 0x18
%define DATA_SEL_16BIT 0x20
