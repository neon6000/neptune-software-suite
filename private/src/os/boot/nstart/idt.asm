;*******************************************************
;	idt.asm
;		interrupt descriptor table
;
;	Copyright 2008-2016 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

;
; This is protected mode code
;
section .text
bits 32

;=============================================;
;                                             ;
;	IDT Descriptor                            ;
;                                             ;
;=============================================;

struc idt_entry
   .m_baseLow	resw   1
   .m_selector	resw   1
   .m_reserved	resb   1
   .m_flags		resb   1
   .m_baseHi	resw   1
endstruc

;=============================================;
;                                             ;
;	IDTR                                      ;
;                                             ;
;=============================================;
struc idt_ptr
   .m_size         resw   1
   .m_base         resd   1
endstruc

;=============================================;
;                                             ;
;	Interrupt Descriptor Table                ;
;                                             ;
;=============================================;

_IDT:
	times 8*256 db 0

;%rep 256
;   istruc idt_entry
;      at idt_entry.m_baseLow,	dw 0
;      at idt_entry.m_selector,	dw 0x8   
;      at idt_entry.m_reserved,	db 0
;      at idt_entry.m_flags,		db 010001111b
;      at idt_entry.m_baseHi,	dw 0
;   iend
;%endrep

_IDT_End:
_IDT_Size   dw   _IDT_End - _IDT   
_Desc_Size   db   8

_IDT_Ptr:
	dw 0
	dd 0

;   istruc idt_ptr
;      at idt_ptr.m_size, dw 0
;      at idt_ptr.m_base, dd 0
;   iend


;=============================================;
;                                             ;
;	void idt_load(void)                       ;
;     - Loads IDT                             ;
;                                             ;
;=============================================;

idt_load:
   cli
   lidt   [_IDT_Ptr]
   ret
 
;=============================================;
;                                             ;
;	void idt_install(void)                    ;
;     - Installs IDT                          ;
;                                             ;
;=============================================;

idt_install:
   pusha
   cli
   mov		word [_IDT_Ptr+idt_ptr.m_size], _IDT_Size - 1
   mov		dword [_IDT_Ptr+idt_ptr.m_base], _IDT
   call		idt_load
   popa
   ret

;=============================================;
;                                             ;
;	void idt_set_gate (void)                  ;
;		-installs an interrupt gate           ;
;                                             ;
;	EAX: interrupt number                     ;
;	EBX: interrupt routine                    ;
;                                             ;
;=============================================;

idt_set_gate:
   pusha
   mov   edx, 8
   mul   dx
   add   eax, _IDT
   mov   word [eax+idt_entry.m_baseLow], bx
   shr   ebx, 16
   mov   word [eax+idt_entry.m_baseHi], bx   
   popa
   ret
