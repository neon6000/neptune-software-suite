
;*******************************************************
;	entry.asm
;		NSTART entry point
;
;	Copyright 2008-2016 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

;
; We are loaded at 0x8000 by the boot record.
;
section .text
org 0x8000

;
; This is real mode code.
;
bits	16

;
; Jump to entry point.
;
jmp	main

%include "gdt.asm"
%include "a20.asm"
%include "common.inc"
%include "stdout16.asm"
%include "pmode_enable.asm"

;=============================================================;
;                                                             ;
; void main (void)                                            ;
;	-inits basic system state and jumps into protected mode   ;
;                                                             ;
;=============================================================;

main:
	;
	; Setup registers.
	;
    cli
    xor   ax, ax
    mov   ds, ax
    mov   es, ax
    mov   gs, ax
    mov   fs, ax
    mov   ss, ax
    mov   sp, 0x7C00
	mov   bp, sp
	sti
	;
	; Save boot drive and file system ID.
	;
	mov		dword [bootfsys], 0
	mov		dword [bootdrive], 0
    mov     [bootfsys], dh
    mov     [bootdrive], dl
	;
	; Enable A20 line.
	;
    call    enable_a20
	;
	; Install initial GDT.
	;
    call    gdt_install
    ;
    ; Enable protected mode.
    ;
	call	pmode_enable
    jmp     CODE_SEL:pmode_entry

;
; The following files are only for 32 bit
; protected mode.
;
bits 32

%include "stdio_rmode.asm"
%include "entry_pmode.asm"

;
; This defines the last byte of NSTART. NBOOT.EXE
; should be located at this address.
;
STARTUP_SYS_END:
