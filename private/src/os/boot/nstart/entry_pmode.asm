;*******************************************************
;	entry_pmode.asm
;		startup protected mode entry point
;
;	Copyright 2008-2016 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

;
; This is protected mode code.
;
section .text
bits 32

%include "idt.asm"
%include "exception.asm"
%include "boot_error.asm"
;%include "paging.asm"
%include "rmode_enable.asm"
%include "su2.asm"

;
; NBOOT DOS Header
;
struc IMAGE_DOS_HEADER
   .magic    resw   1
   .cblp     resw   1
   .cp       resw   1
   .crlc     resw   1
   .cparhdr  resw   1
   .minalloc resw   1
   .maxalloc resw   1
   .ss	     resw   1
   .sp       resw   1
   .csum     resw   1
   .ip       resw   1
   .cs       resw   1
   .lfarlc   resw   1
   .ovno     resw   1
   .reserv   resw   4
   .oemid    resw   1
   .oeminfo  resw   1
   .reserv2  resw   10
   .lfanew   resd   1
endstruc

;
; NBOOT File Header
;
struc IMAGE_FILE_HEADER
	.magic                resd 1
	.machine              resw 1
	.numberOfSections     resw 1
	.timeDateStamp        resd 1
	.pointerToSymbolTable resd 1
	.numberOfSymbols      resd 1
	.sizeOfOptionalHeader resw 1
	.characteristics      resw 1
endstruc

;
; NBOOT Not so optional Header
;
struc IMAGE_OPTIONAL_HEADER
	.magic                       resw 1
	.majorLinkerVersion          resb 1
	.minorLinkerVersion          resb 1
	.sizeOfCode                  resd 1
	.sizeOfInitializedData       resd 1
	.sizeOfUninitializedData     resd 1
	.addressOfEntryPoint         resd 1
	.baseOfCode                  resd 1
	.baseOfData                  resd 1
	.imageBase                   resd 1
	.sectionAlignment            resd 1
	.fileAlignment               resd 1
	.majorOperatingSystemVersion resw 1
	.minorOperatingSystemVersion resw 1
	.majorImageVersion           resw 1
	.minorImageVersion           resw 1
	.majorSubsystemVersion       resw 1
	.minorSubsystemVersion       resw 1
	.win32VersionValue           resd 1
	.sizeOfImage                 resd 1
	.sizeOfHeaders               resd 1
	.checkSum                    resd 1
	.subsystem                   resw 1
	.dllCharacteristics          resw 1
	.sizeOfStackReserve          resd 1
	.sizeOfStackCommit           resd 1
	.sizeOfHeapReserve           resd 1
	.sizeOfHeapCommit            resd 1
	.loaderFlags                 resd 1
	.numberOfRvaAndSizes         resd 1
endstruc

;==============================================;
;                                              ;
;	void pmode_entry (void)                    ;
;		- Initializes environment and          ;
;         executes NBOOT                       ;
;                                              ;
;==============================================;

pmode_entry:
	;
	; Set up selectors and stack
	;
    mov  ax, DATA_SEL
    mov  ds, ax
    mov  ss, ax
    mov  es, ax
    mov  fs, ax
    mov  gs, ax
	mov  esp, 0x7e00
	cli
	;
	; Initialize protected mode structures
	;
	call	pmode_init
	;
	; Execute NBOOT
	;
	call	execute_loader
.loop_forever:
    cli
    hlt
    jmp .loop_forever ; avoid being awoken by NMI or SMI

;==============================================;
;                                              ;
;	void pmode_init (void)                     ;
;		-initializes protected envirement      ;
;                                              ;
;==============================================;

pmode_init:
	;
	; Install initial IDT and exeption handlers.
	;
	call	idt_install
	call	install_exception_handlers
	ret

;==============================================;
;                                              ;
;	void install_exception_handlers (void)     ;
;		-installs system exception handlers    ;
;                                              ;
;==============================================;

install_exception_handlers:
	;
	; Install exception handlers.
	;
	mov		eax, 0
	mov		ebx, divide_by_0
	call	idt_set_gate
	mov		eax, 1
	mov		ebx, single_step
	call	idt_set_gate
	mov		eax, 2
	mov		ebx, nmi_fault
	call	idt_set_gate
	mov		eax, 3
	mov		ebx, breakpoint_trap
	call	idt_set_gate
	mov		eax, 4
	mov		ebx, overflow_fault
	call	idt_set_gate
	mov		eax, 5
	mov		ebx, bounds_check
	call	idt_set_gate	
	mov		eax, 6
	mov		ebx, invalid_opcode
	call	idt_set_gate
	mov		eax, 7
	mov		ebx, no_coprocessor
	call	idt_set_gate
	mov		eax, 8
	mov		ebx, double_fault
	call	idt_set_gate
	mov		eax, 9
	mov		ebx, coprocessor_fault
	call	idt_set_gate
	mov		eax, 10
	mov		ebx, invalid_tss
	call	idt_set_gate
	mov		eax, 11
	mov		ebx, invalid_segment
	call	idt_set_gate
	mov		eax, 12
	mov		ebx, segment_overrun
	call	idt_set_gate	
	mov		eax, 13
	mov		ebx, gpf_handler
	call	idt_set_gate
	mov		eax, 14
	mov		ebx, page_fault
	call	idt_set_gate
	mov		eax, 16
	mov		ebx, coprocessor_general
	call	idt_set_gate
	ret

;==============================================;
;                                              ;
;	void execute_loader (void)                 ;
;		-executes NBOOT                        ;
;                                              ;
;==============================================;

execute_loader:
	;
	; find NBOOT.EXE at end of NSTART.
	;
	mov		ebp, STARTUP_SYS_END
	;
	; This should be IMAGE_DOS_HEADER
	;
	cmp    word [ebp + IMAGE_DOS_HEADER.magic], 0x5a4d
	jne    .error
	;
	; Now we go to IMAGE_FILE_HEADER
	;
	mov    ebp, [ebp + IMAGE_DOS_HEADER.lfanew]
	add    ebp, STARTUP_SYS_END
	cmp    dword [ebp + IMAGE_FILE_HEADER.magic], 0x00004550
	jne    .error
	;
	; Now we go to IMAGE_OPTIONAL_HEADER. This is right
	; after IMAGE_FILE_HEADER, which is 24 bytes. Make
	; sure its type is PE32.
	;
	add    ebp, 24
	cmp    word [ebp + IMAGE_OPTIONAL_HEADER.magic], 0x010B
	jne    .error
	;
	; Now we save the size of the image into ECX.
	;
	; ecx -> 0x19e00
	mov    ecx, [ebp + IMAGE_OPTIONAL_HEADER.sizeOfImage]
	;
	; Now we save the base of the image into EDI.
	;
	;; 0x9039
	mov    edi, [ebp + IMAGE_OPTIONAL_HEADER.imageBase]
	push   edi
	;
	; Now we save the source in ESI. This is where we will
	; be copying from.
	;
	mov    esi, STARTUP_SYS_END
	push   esi
	;
	; Adjust ECX. We should make this into a REP MOVSD.
	;
	add   ecx, 1
	push  ecx
	;
	; Copy NBOOT. We start at source+size and destination+size
	; and copy backward so we prevent overwriting nboot during copy.
	;
	add    edi, ecx
	add    esi, ecx
	std
	rep    movsb
	cld
	;; rdi: 00000000_00020000
	;
	; Restore from stack.
	;
	pop   ecx
	pop   esi
	pop   edi
	;
	; Call NBOOT
	;
	mov    ebx, [ebp + IMAGE_OPTIONAL_HEADER.addressOfEntryPoint]
	add    ebx, edi
	call   ebx
	;
	; Crash system.
	;
.error:
	mov		esi, .corruptNboot
	call	boot_error
	cli
	hlt
	
.corruptNboot db "NBOOT is corrupt ", 0
