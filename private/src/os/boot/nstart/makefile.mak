#------------------------------------------------------------------
#	makefile.mak
#		Startup module makefile
#
# Copyright (c) BrokenThorn Entertainment, Co. All Rights Reserved
#------------------------------------------------------------------
TARGET = TARGET_BIN

!include $(NEPTUNE_ENV)\makefile.def

startup.bin : startup.asm
