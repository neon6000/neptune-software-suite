;*******************************************************
;	common.inc
;		Common definitions
;
;	Copyright 2008-2016 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

section .text

;
; boot drive
;
bootdrive   db 0

;
; boot file system ID
;
bootfsys    dd 0
