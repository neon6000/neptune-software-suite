;*******************************************************
;	exception.asm
;		NSTART exception handlers
;
;	Copyright 2008-2016 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

;
; This is protected mode code.
;
section .text
bits 32

divide_by_0:
	call	clear_display16
	mov		esi, .divideStr
	call	boot_error
	cli
	hlt
	.divideStr db "Divide by 0 fault",0

single_step:
	cli
	hlt

nmi_fault:
	call	clear_display16
	call	set_pos16
	mov		esi, .nmiStr
	call	boot_error
	cli
	hlt
	.nmiStr db "NMI pin activated",0

breakpoint_trap:
	cli
	hlt

overflow_fault:
	call	clear_display16
	call	set_pos16
	mov		esi, .overflowStr
	call	boot_error
	cli
	hlt
	.overflowStr db "Overflow detected",0

bounds_check:
	call	clear_display16
	call	set_pos16
	mov		esi, .boundsStr
	call	boot_error
	cli
	hlt
	.boundsStr db "Bounds Check",0

invalid_opcode:
	call	clear_display16
	call	set_pos16
	mov		esi, .opcodeStr
	call	boot_error
	cli
	hlt
	.opcodeStr db "Invalid instruction",0

no_coprocessor:
	call	clear_display16
	call	set_pos16
	mov		esi, .copStr
	call	boot_error
	cli
	hlt
	.copStr db "No coprocessor detected",0

double_fault:
	call	clear_display16
	call	set_pos16
	mov		esi, .copStr
	call	boot_error
	cli
	hlt
	.copStr db "Double Fault",0

coprocessor_fault:
	call	clear_display16
	call	set_pos16
	mov		esi, .copStr
	call	boot_error
	cli
	hlt
	.copStr db "Coprocessor segment overrun",0

invalid_tss:
	call	clear_display16
	call	set_pos16
	mov		esi, .copStr
	call	boot_error
	cli
	hlt
	.copStr db "Invalid TSS detected",0

invalid_segment:
	call	clear_display16
	call	set_pos16
	mov		esi, .copStr
	call	boot_error
	cli
	hlt
	.copStr db "Invalid segment or selector detected",0

segment_overrun:
	call	clear_display16
	call	set_pos16
	mov		esi, .copStr
	call	boot_error
	cli
	hlt
	.copStr db "Stack segment overrun detected",0

gpf_handler:
	call	clear_display16
	call	set_pos16
	mov		esi, .copStr
	call	boot_error
	cli
	hlt
	.copStr db "General Protection Fault",0

page_fault:
	call	clear_display16
	call	set_pos16
	mov		esi, .copStr
	call	boot_error
	cli
	hlt
	.copStr db "Page Fault",0

coprocessor_general:
	call	clear_display16
	call	set_pos16
	mov		esi, .copStr
	call	boot_error
	cli
	hlt
	.copStr db "Generic coprocessor error detected",0
