;*******************************************************
;	boot_error.asm
;		boot errors
;
;	Copyright 2008-2016 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

section .text

;
; This is protected mode code.
;
bits 32

;=====================================================;
;                                                     ;
; void BOOT_FAILURE (void)                            ;
;		displays an error and halts the boot process  ;
;                                                     ;
;	ESI: Pointer to string to display                 ;
;                                                     ;
;=====================================================;

boot_error:
	call  clear_display16
	call  set_pos16
	push  esi
	mov   esi, .bootError
	call  puts16
	pop   esi
	call  puts16
.loop_forever:
	cli
	hlt
	jmp   .loop_forever ; prevent NMI or SMI from waking us.

.bootError:  db "The system has detected a problem and NSTART has halted the machine.",0x0a,0x0d
            db "Please provide us the crash log report with the error.",0x0a,0x0a,0x0d
            db "Error information:",0x0a,0x0a,0x0d,0
