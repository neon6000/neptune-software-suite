;*******************************************************
;	stdio_rmode.asm
;		32 bit stdout routines
;
;	Copyright 2008-2016 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

;
; This is protected mode code
;
section .text
bits	32

;=============================================;
;                                             ;
;	void puts16 (void)                        ;
;      Display a string                       ;
;   DS:SI = Null Terminated String            ;
;                                             ;
;=============================================;

puts16:
	pusha
.loop:
	lodsb
	or     al, al

	; 0x80ec
	;; jz .+35
	jz     .done
	;
	; Call BIOS int 0x10 function 0xe
	;
	mov    ah, 0eh
	mov	   word [REGS_LOCAL+REGS.m_eax], ax
	lea	   eax, [REGS_LOCAL]
	push   dword eax
	push   dword eax
	push   dword 10h
	call   io_services
	add	   esp, 12
	jmp   .loop
.done:
; 0x8115

	nop
	nop
	nop
	popa
	nop
	nop
	nop
; 0x8116
	ret

;=============================================;
;                                             ;
;	void clear_display16 (void)               ;
;      Clears display                         ;
;                                             ;
;=============================================;

clear_display16:
    ;
    ; BIOS int 0x10 funcion 2
    ;
	mov   ah, 06h
	mov   al, 0
	mov   dl, 79
	mov   dh, 24
	mov   ebx, 0x1f00
	mov   dword [REGS_LOCAL+REGS.m_eax], eax
	mov   dword [REGS_LOCAL+REGS.m_ebx], ebx
	mov   dword [REGS_LOCAL+REGS.m_ecx], 0
	mov   dword [REGS_LOCAL+REGS.m_edx], edx
	lea   eax, [REGS_LOCAL]
	push  dword eax
	push  dword eax
	push  dword 10h
	call  io_services
	add   esp, 12
	ret

;=============================================;
;                                             ;
;	void clear_pos16 (void)                   ;
;      Resets cursor position back to origin  ;
;                                             ;
;=============================================;

set_pos16:
	;
	; BIOS int 0x10 function 2
	;
	mov  ah, 02h
	mov  dword [REGS_LOCAL+REGS.m_eax], eax
	mov  dword [REGS_LOCAL+REGS.m_ebx], 0
	mov  dword [REGS_LOCAL+REGS.m_edx], 0
	lea  eax, [REGS_LOCAL]
	push dword eax
	push dword eax
	push dword 10h
	call io_services
	add  esp, 12
	ret
