;*******************************************************
;	su.asm
;		protected mode <> real mode interface module
;
;	Copyright 2008 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

section .text
bits 32

;----------------------------------------------
;    regs structure
;----------------------------------------------

struc REGS
   .m_eax        resw    1
   .m_ebx        resw    1
   .m_ecx        resw    1
   .m_edx        resw    1
   .m_esi        resw    1
   .m_edi        resw    1
   .m_es        resw    1
   .m_cs        resw    1
   .m_ss        resw    1
   .m_ds        resw    1
   .m_flags     resw    1
   .m_reserved  resw    1    ;aligns struct to 6 dwords in size
endstruc

;----------------------------------------------
;    data section
;----------------------------------------------

; protected mode stack location
stack dd 0

; real mode ivt pointer
ivt_ptr:
	dw 0
	dd 0
;   istruc idt_ptr
;      at idt_ptr.m_size, dw 0
;      at idt_ptr.m_base, dd 0
;   iend

; protected mode idt pointer
pmode_idt:
	dw 0
	dd 0
;   istruc idt_ptr
;      at idt_ptr.m_size, dw 0
;      at idt_ptr.m_base, dd 0
;   iend

; protected mode gdt
pmode_gdt:
      dw 0
      dd 0

; local copy of registers
REGS_LOCAL:
	dw 0
	dw 0
	dw 0
	dw 0
	dw 0
	dw 0
	dw 0
	dw 0
	dw 0
	dw 0
	dw 0
	dw 0

;istruc REGS
;   at REGS.m_eax, dw 0
;   at REGS.m_ebx, dw 0
;   at REGS.m_ecx, dw 0
;   at REGS.m_edx, dw 0
;   at REGS.m_esi, dw 0
;   at REGS.m_edi, dw 0
;   at REGS.m_es, dw 0
;   at REGS.m_cs, dw 0
;   at REGS.m_ss, dw 0
;   at REGS.m_ds, dw 0
;   at REGS.m_flags,dw 0
;   at REGS.m_reserved, dw 0
;iend

;==========================================================;
;                                                          ;
;    void io_services (int function, REGS* in, REGS* out)  ;
;        -16bit input/output services                      ;
;                                                          ;
;    int function: 16bit interrupt number to call          ;
;    REGS* in: input registers for interrupt call          ;
;    REGS* out: output registers from interrupt call       ;
;                                                          ;
;==========================================================;

io_services:

    pushad

    ; get local copy of REGS in parameter
    cld
    mov        esi, [esp+32+8]
    mov        edi, REGS_LOCAL
    mov        ecx, 6
    rep        movsd

    ; save protected mode stack and idtr
    mov        [stack], esp
    cli
    sgdt       [pmode_gdt]
    sidt       [pmode_idt]

    ; normally should disable paging here. Commented out for your release
    call        disable_paging

    ; install real mode ivt
    mov        word [ivt_ptr+idt_ptr.m_size], 0xffff
    mov        dword [ivt_ptr+idt_ptr.m_base], 0
    lidt       [ivt_ptr]

    ; jump to 16 bit protected mode
    jmp        CODE_SEL_16BIT:.pmode16

bits 16

.pmode16:

    ; jump into real mode
    call        rmode_enable
    jmp         0:.rmode

.rmode:

    ; clear all segments
    xor        ax, ax
    mov        fs, ax
    mov        gs, ax
    mov        ds, ax
    mov        es, ax
    mov        ss, ax

    ; set up the registers
    mov        ax, [REGS_LOCAL+REGS.m_eax]
    mov        bx, [REGS_LOCAL+REGS.m_ebx]
    mov        cx, [REGS_LOCAL+REGS.m_ecx]
    mov        dl, [esp+32+4]        ; get interrupt to call and set it
    mov        byte [.call_service+1], dl
    mov        dx, [REGS_LOCAL+REGS.m_edx]
    mov        si, [REGS_LOCAL+REGS.m_esi]
    mov        di, [REGS_LOCAL+REGS.m_edi]
    mov        es, [REGS_LOCAL+REGS.m_es]

    ; invoke BIOS interrupt. Warning: self modifying code being used
.call_service:
    int        0

    ; save return registers
    pushfd
    pop        dword [REGS_LOCAL+REGS.m_flags]
    mov        word [REGS_LOCAL+REGS.m_reserved], 0
    mov        [REGS_LOCAL+REGS.m_eax], ax
    mov        [REGS_LOCAL+REGS.m_ebx], bx
    mov        [REGS_LOCAL+REGS.m_ecx], cx
    mov        [REGS_LOCAL+REGS.m_edx], dx
    mov        [REGS_LOCAL+REGS.m_esi], si
    mov        [REGS_LOCAL+REGS.m_edi], di
    mov        [REGS_LOCAL+REGS.m_es], es

    ; jump into protected mode
    call       pmode_enable
    jmp        CODE_SEL:.return

bits 32

.return:

    ; restore selectors and stack
    mov        ax, DATA_SEL
    mov        fs, ax
    mov        gs, ax
    mov        ds, ax
    mov        es, ax
    mov        ss, ax
    mov        esp, [stack]

    ; copy output paramater
    cld
    mov        esi, REGS_LOCAL
    mov        edi, [esp+32+12]
    mov        ecx, 6
    rep        movsd

    ; normally you would want to re-enable paging here. Commented out for your use
    call    enable_paging

    ; restore registers and idtr
    popad
    lgdt    [pmode_gdt]
    lidt    [pmode_idt]
    ret
