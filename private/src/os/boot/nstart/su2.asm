;*******************************************************
;	su.asm
;		protected mode <> real mode interface module
;
;	Copyright 2008 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

section .text
bits 32

;----------------------------------------------
;    regs structure
;----------------------------------------------

struc REGS
   .m_eax		resd    1
   .m_ebx		resd    1
   .m_ecx		resd    1
   .m_edx		resd    1
   .m_esi		resd    1
   .m_edi		resd    1
   .m_ebp		resd	1
   .m_esp		resd	1
   .m_eip		resd	1
   .m_cs        resw    1
   .m_ds        resw    1
   .m_es        resw    1
   .m_ss        resw    1
   .m_fs		resw	1
   .m_gs		resw	1
   .m_flags     resw    1   ; 50 bytes
endstruc

REGS_LOCAL:
   dd 0
   dd 0
   dd 0
   dd 0
   dd 0
   dd 0
   dd 0
   dd 0
   dd 0
   dw 0
   dw 0
   dw 0
   dw 0
   dw 0
   dw 0
   dw 0

;istruc REGS
;   at REGS.m_eax, dd 0
;   at REGS.m_ebx, dd 0
;   at REGS.m_ecx, dd 0
;   at REGS.m_edx, dd 0
;   at REGS.m_esi, dd 0
;   at REGS.m_edi, dd 0
;   at REGS.m_ebp, dd 0
;   at REGS.m_esp, dd 0
;   at REGS.m_eip, dd 0
;   at REGS.m_cs, dw 0
;   at REGS.m_ds, dw 0
;   at REGS.m_es, dw 0
;   at REGS.m_ss, dw 0
;   at REGS.m_fs, dw 0
 ;  at REGS.m_gs, dw 0
;   at REGS.m_flags,dw 0
;iend

; protected mode stack location
stack2 dd 0

; real mode ivt pointer
ivt_ptr:
	dw 0
	dd 0

;   istruc idt_ptr
;      at idt_ptr.m_size, dw 0
;      at idt_ptr.m_base, dd 0
;   iend

; protected mode idt pointer
pmode_idt2:
	dw 0
	dd 0

;   istruc idt_ptr
;      at idt_ptr.m_size, dw 0
;      at idt_ptr.m_base, dd 0
;   iend

; protected mode gdt
pmode_gdt2:
      dw 0
      dd 0

;==========================================================;
;                                                          ;
;    void io_services (int function, REGS* in, REGS* out)  ;
;        -16bit input/output services                      ;
;                                                          ;
;    int function: 16bit interrupt number to call          ;
;    REGS* in: input registers for interrupt call          ;
;    REGS* out: output registers from interrupt call       ;
;                                                          ;
;==========================================================;

; 0x8DA5
io_services:

    pushad

    ; get local copy of REGS in parameter
    cld
    mov        esi, [esp+32+8]
    mov        edi, REGS_LOCAL
    mov        ecx, 13
    rep        movsd

    ; save protected mode stack and idtr
    mov        [stack2], esp
    cli
    sgdt       [pmode_gdt2]
    sidt       [pmode_idt2]

 ;   call        disable_paging

    ; install real mode ivt
    mov        word [ivt_ptr+idt_ptr.m_size], 0xffff
    mov        dword [ivt_ptr+idt_ptr.m_base], 0
    lidt       [ivt_ptr]
 
	; jump to 16 bit protected mode
    jmp        CODE_SEL_16BIT:.pmode16

bits 16

.pmode16:

    ; jump into real mode
    call        rmode_enable
    jmp         0:.rmode

.rmode:

    ; clear all segments
    xor        ax, ax
    mov        fs, ax
    mov        gs, ax
    mov        ds, ax
    mov        es, ax
    mov        ss, ax

    ; set up the registers
    mov        eax, [REGS_LOCAL+REGS.m_eax]
    mov        ebx, [REGS_LOCAL+REGS.m_ebx]
    mov        ecx, [REGS_LOCAL+REGS.m_ecx]
    mov        dl, [esp+32+4]        ; set int number
    mov        byte [.call_service+1], dl
    mov        edx, [REGS_LOCAL+REGS.m_edx]
    mov        esi, [REGS_LOCAL+REGS.m_esi]
    mov        edi, [REGS_LOCAL+REGS.m_edi]
    mov        ebp, [REGS_LOCAL+REGS.m_ebp]
    mov        es, [REGS_LOCAL+REGS.m_es]
    mov        fs, [REGS_LOCAL+REGS.m_fs]
    mov        gs, [REGS_LOCAL+REGS.m_gs]

    ; invoke BIOS interrupt. Warning: self modifying code being used
.call_service:
    int        0

    ; save return registers
    pushf
    pop        word [REGS_LOCAL+REGS.m_flags]
    mov        [REGS_LOCAL+REGS.m_eax], eax
    mov        [REGS_LOCAL+REGS.m_ebx], ebx
    mov        [REGS_LOCAL+REGS.m_ecx], ecx
    mov        [REGS_LOCAL+REGS.m_edx], edx
    mov        [REGS_LOCAL+REGS.m_esi], esi
    mov        [REGS_LOCAL+REGS.m_edi], edi
    mov        [REGS_LOCAL+REGS.m_es], es
    mov        [REGS_LOCAL+REGS.m_fs], fs
    mov        [REGS_LOCAL+REGS.m_gs], gs
   
    ; jump into protected mode
    call       pmode_enable
    jmp        CODE_SEL:.return

bits 32

.return:

    ; restore selectors and stack
    mov        ax, DATA_SEL
    mov        fs, ax
    mov        gs, ax
    mov        ds, ax
    mov        es, ax
    mov        ss, ax
    mov        esp, [stack2]

    ; copy output paramater
    cld
    mov        esi, REGS_LOCAL
    mov        edi, [esp+32+12]
    mov        ecx, 13
    rep        movsd

    ; re-enable paging
 ;   call    enable_paging

    ; restore registers and idtr
    popad
    lgdt    [pmode_gdt2]
    lidt    [pmode_idt2]
    ret
