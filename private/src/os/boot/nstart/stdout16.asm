
;*******************************************************
;	stdout.asm
;		16bit stdio routines
;
;	Copyright 2008-2016 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

section .text
bits 16

;=============================================;
;                                             ;
;	void puts_16bit (void)                    ;
;      Display a string in real mode          ;
;   DS:SI = Null Terminated String            ;
;                                             ;
;=============================================;

puts_16bit:

   lodsb
   or    al, al
   jz    .done
   mov   ah, 0eh
   xor   ebx, ebx			
   int   10h
   jmp   puts_16bit
.done:
   ret
