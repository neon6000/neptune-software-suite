
;*******************************************************
;	paging.asm
;		pqging interface routines
;
;	Copyright 2008-2016 BrokenThorn Entertainment, Co. All Rights Reserved.
;*******************************************************

; This is protected mode code.
;
section .text
bits	32

;
; page directory table
;
%define		PAGE_DIR			0x9C000

;
; 0th page table. Address must be 4KB aligned
;
%define		PAGE_TABLE_0		0x9D000

;
; 768th page table. Address must be 4KB aligned
;
%define		PAGE_TABLE_768		0x9E000

;
; each page table has 1024 entries
;
%define		PAGE_TABLE_ENTRIES	1024

;
; attributes (page is present;page is writable; supervisor mode)
;
%define		PRIV				3

;
; Each page table entry is 4 bytes.
;
%define     PAGE_ENTRY_SIZE 4

;
; A page is 4096 bytes.
;
%define     PAGE_SIZE 4096

;=============================================;
;                                             ;
;	void enable_paging (void)                 ;
;		- Installs paging structures          ;
;                                             ;
;=============================================;

enable_paging:
	;
	;	idenitity map 1st page table (4MB)
	;
	pusha
	mov   eax, PAGE_TABLE_0
	mov   ebx, 0x0 + PRIV
	mov   ecx, PAGE_TABLE_ENTRIES
.loop:
	mov   dword [eax], ebx
	add   eax, PAGE_ENTRY_SIZE
	add	  ebx, PAGE_SIZE
	loop  .loop
	;
	;	set up the entries in the directory table
	;
	mov		eax, PAGE_TABLE_0 + PRIV
	mov		dword [PAGE_DIR], eax
	mov		eax, PAGE_TABLE_768 + PRIV
	mov		dword [PAGE_DIR+(768*4)], eax
	;
	;	install directory table
	;
	mov		eax, PAGE_DIR
	mov		cr3, eax
	;
	;	enable paging
	;
	mov		eax, cr0
	or		eax, 0x80000000
	mov		cr0, eax
	popa
	ret

;=============================================;
;                                             ;
;	void disable_paging (void)                ;
;		- Disables paging structures          ;
;                                             ;
;=============================================;

disable_paging:
	push	eax
	mov		eax, cr0
	and		eax, 0x7fffffff
	mov		cr0, eax
	pop		eax
	ret
