/************************************************************************
*
*	nburg.h - NCC Code generator generator
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef NBURG
#define NBURG

#define IN
#define OUT
#define PRIVATE static
#define PUBLIC

typedef enum { TERM = 1, NONTERM } Kind;
typedef struct RULE *PRULE;
typedef struct TERM *PTERM;
struct TERM {		/* terminals: */
	char *name;		/* terminal name */
	Kind kind;		/* TERM */
	int esn;		/* external symbol number */
	int arity;		/* operator arity */
	PTERM link;		/* next terminal in esn order */
	PRULE rules;		/* rules whose pattern starts with term */
};

typedef struct NONTERM *PNONTERM;
struct NONTERM {	/* nonterminals: */
	char *name;		/* nonterminal name */
	Kind kind;		/* NONTERM */
	int number;		/* identifying number */
	int lhscount;		/* # times nt appears in a rule lhs */
	int reached;		/* 1 iff reached from start nonterminal */
	PRULE rules;		/* rules w/nonterminal on lhs */
	PRULE chain;		/* chain rules w/nonterminal on rhs */
	PNONTERM link;		/* next terminal in number order */
};

typedef struct TREE *PTREE;
struct TREE {		/* tree patterns: */
	void *op;		/* a terminal or nonterminal */
	PTREE left, right;	/* operands */
	int nterms;		/* number of terminal nodes in this tree */
};

struct RULE {		/* rules: */
	PNONTERM lhs;		/* lefthand side nonterminal */
	PTREE    pattern;	/* rule pattern */
	int      ern;		/* external rule number */
	int      packed;	/* packed external rule number */
	int      cost;		/* cost, if a constant */
	char*    code;		/* cost, if an expression */
	char*    template;	/* assembler template */
	PRULE    link;		/* next rule in ern order */
	PRULE    next;		/* next rule with same pattern root */
	PRULE    chain;		/* next chain rule with same rhs */
	PRULE    decode;	/* next rule with same lhs */
	PRULE    kids;		/* next rule with same _kids pattern */
};

extern void*    alloc       (IN int nbytes);
extern PNONTERM NewNonterm  (IN char *id);
extern PTERM    NewTerm     (IN char *id, IN int extrnSymNum);
extern PTREE    NewTree     (IN char *op, IN PTREE left, IN PTREE right);
extern PRULE    NewRule     (IN char *id, IN PTREE pattern,
                             IN char *template, IN char *code);

#endif
