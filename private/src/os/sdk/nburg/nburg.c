/************************************************************************
*
*	nburg.c - NCC Code generator generator
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/*
	This component contains software based on IBURG.
*/

#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "nburg.h"

static char     *prefix = "";
static int      Tflag;
static int      ntnumber;
static PNONTERM start;
static PTERM    terms;
static PNONTERM nts;
static PRULE    rules;
static int      nrules;
static int      errcnt;
static FILE*    infp;
static FILE*    outfp;
static char     buf[BUFSIZ];
static char*    bp = buf;
static int      ppercent;
static int      code;

static struct BLOCK {
	struct BLOCK *link;
} *memlist;			/* list of allocated blocks */

/* alloc - allocate nbytes or issue fatal error */
PRIVATE void* alloc(IN int nbytes) {

	struct BLOCK *p = calloc(1, sizeof *p + nbytes);

	if (p == NULL) {
		fprintf(stderr, "out of memory\n");
		exit(1);
	}
	p->link = memlist;
	memlist = p;
	return p + 1;
}

/* stringf - format and save a string */
PRIVATE char* stringf(IN char *fmt, IN ...) {

	va_list ap;
	char    buf[512];

	va_start(ap, fmt);
	vsprintf(buf, fmt, ap);
	va_end(ap);
	return strcpy(alloc(strlen(buf) + 1), buf);
}

/* print - formatted output */
PRIVATE void print(IN char *fmt, IN ...) {

	va_list ap;
	va_start(ap, fmt);
	for (; *fmt; fmt++)
	if (*fmt == '%')
		switch (*++fmt) {
		case 'd': fprintf(outfp, "%d", va_arg(ap, int)); break;
		case 's': fputs(va_arg(ap, char *), outfp); break;
		case 'P': fprintf(outfp, "%s_", prefix); break;
		case 'T': {
					  PTREE t = va_arg(ap, PTREE);
					  print("%S", t->op);
					  if (t->left && t->right)
						  print("(%T,%T)", t->left, t->right);
					  else if (t->left)
						  print("(%T)", t->left);
					  break;
		}
		case 'R': {
					  PRULE r = va_arg(ap, PRULE);
					  print("%S: %T", r->lhs, r->pattern);
					  break;
		}
		case 'S': {
					  PTERM t = va_arg(ap, PTERM);
					  fputs(t->name, outfp);
					  break;
		}
		case '1': case '2': case '3': case '4': case '5': {
					  int n = *fmt - '0';
					  while (n-- > 0)
						  putc('\t', outfp);
					  break;
		}
		default: putc(*fmt, outfp); break;
	}
	else
		putc(*fmt, outfp);
	va_end(ap);
}

struct ENTRY {
	union {
		char *name;
		struct TERM t;
		struct NONTERM nt;
	} sym;
	struct ENTRY *link;
} *table[211];

#define HASHSIZE (sizeof table/sizeof table[0])

/* hash - return hash number for str */
PRIVATE unsigned hash(char *str) {

	unsigned h = 0;
	while (*str)
		h = (h << 1) + *str++;
	return h;
}

/* lookup - lookup symbol name */
PRIVATE void* GetSymbol(char *name) {

	struct ENTRY *p = table[hash(name) % HASHSIZE];
	for (; p; p = p->link)
	if (strcmp(name, p->sym.name) == 0)
		return &p->sym;
	return 0;
}

/* install - install symbol name */
PRIVATE void* InstallSymbol(char *name) {
	
	struct ENTRY *p = alloc(sizeof *p);
	int i = hash(name) % HASHSIZE;
	p->sym.name = name;
	p->link = table[i];
	table[i] = p;
	return &p->sym;
}

/* nonterm - create a new terminal id, if necessary */
PUBLIC PNONTERM NewNonterm(char *id) {

	PNONTERM p = GetSymbol(id), *q = &nts;
	if (p && p->kind == NONTERM)
		return p;
	if (p && p->kind == TERM)
		fprintf(stderr, "`%s' is a terminal\n", id);
	p = InstallSymbol(id);
	p->kind = NONTERM;
	p->number = ++ntnumber;
	if (p->number == 1)
		start = p;
	while (*q && (*q)->number < p->number)
		q = &(*q)->link;
	assert(*q == 0 || (*q)->number != p->number);
	p->link = *q;
	*q = p;
	return p;
}

/* term - create a new terminal id with external symbol number esn */
PUBLIC PTERM NewTerm(IN char *id, IN int extrnlSymNum) {

	PTERM p = GetSymbol(id), *q = &terms;
	if (p)
		fprintf(stderr, "redefinition of terminal `%s'\n", id);
	else
		p = InstallSymbol(id);
	p->kind = TERM;
	p->esn = extrnlSymNum;
	p->arity = -1;
	while (*q && (*q)->esn < p->esn)
		q = &(*q)->link;
	if (*q && (*q)->esn == p->esn)
		fprintf(stderr, "duplicate external symbol number `%s=%d'\n",
	p->name, p->esn);
	p->link = *q;
	*q = p;
	return p;
}

/* tree - create & initialize a tree node with the given fields */
PUBLIC PTREE NewTree(char *id, PTREE left, PTREE right) {

	PTREE t = alloc(sizeof *t);
	PTERM p = GetSymbol(id);
	int arity = 0;

	if (left && right)
		arity = 2;
	else if (left)
		arity = 1;
	if (p == NULL && arity > 0) {
		fprintf(stderr, "undefined terminal `%s'\n", id);
		p = NewTerm(id, -1);
	}
	else if (p == NULL && arity == 0)
		p = (PTERM)NewNonterm(id);
	else if (p && p->kind == NONTERM && arity > 0) {
		fprintf(stderr, "`%s' is a nonterminal\n", id);
		p = NewTerm(id, -1);
	}
	if (p->kind == TERM && p->arity == -1)
		p->arity = arity;
	if (p->kind == TERM && arity != p->arity)
		fprintf(stderr, "inconsistent arity for terminal `%s'\n", id);
	t->op = p;
	t->nterms = p->kind == TERM;
	if ((t->left = left) != NULL)
		t->nterms += left->nterms;
	if ((t->right = right) != NULL)
		t->nterms += right->nterms;
	return t;
}

/* rule - create & initialize a rule with the given fields */
PUBLIC PRULE NewRule(IN char *id, IN PTREE pattern, IN char *template, IN char *code) {

	PRULE r = alloc(sizeof *r), *q;
	PTERM p = pattern->op;
	char *end;

	r->lhs = NewNonterm(id);
	r->packed = ++r->lhs->lhscount;
	for (q = &r->lhs->rules; *q; q = &(*q)->decode)
		;
	*q = r;
	r->pattern = pattern;
	r->ern = ++nrules;
	r->template = template;
	r->code = code;
	r->cost = strtol(code, &end, 10);
	if (*end) {
		r->cost = -1;
		r->code = stringf("(%s)", code);
	}
	if (p->kind == TERM) {
		for (q = &p->rules; *q; q = &(*q)->next)
			;
		*q = r;
	}
	else if (pattern->left == NULL && pattern->right == NULL) {
		PNONTERM p = pattern->op;
		r->chain = p->chain;
		p->chain = r;
		if (r->cost == -1)
			fprintf(stderr, "illegal nonconstant cost `%s'\n", code);
	}
	for (q = &rules; *q; q = &(*q)->link)
		;
	r->link = *q;
	*q = r;
	return r;
}

PRIVATE void EmitRecord  (IN char *pre, IN PRULE r, IN char *c, IN int cost);
PRIVATE void EmitTest    (IN PTREE t, IN char *v, IN char *suffix);
PRIVATE void EmitRecalc  (IN char *pre, IN PTERM root, IN PTERM kid);
PRIVATE void EmitCost    (IN PTREE t, IN char *v);
PRIVATE void CheckReach  (IN PNONTERM p);

/* reach - mark all nonterminals in tree t as reachable */
PRIVATE void Reach(IN PTREE t) {

	PNONTERM p = t->op;

	if (p->kind == NONTERM)
	if (!p->reached)
		CheckReach(p);
	if (t->left)
		Reach(t->left);
	if (t->right)
		Reach(t->right);
}

/* ckreach - mark all nonterminals reachable from p */
PRIVATE void CheckReach(IN PNONTERM p) {

	PRULE r;

	p->reached = 1;
	for (r = p->rules; r; r = r->decode)
		Reach(r->pattern);
}

/* emitcase - emit one case in function state */
PRIVATE void EmitCase (IN PTERM p, IN int ntnumber) {

	PRULE r;

	print("%1case %d: /* %S */\n", p->esn, p);
	switch (p->arity) {
	case 0: case -1:
		break;
	case 1:
		print("%2%Plabel(LEFT_CHILD(a));\n");
		break;
	case 2:
		print("%2%Plabel(LEFT_CHILD(a));\n");
		print("%2%Plabel(RIGHT_CHILD(a));\n");
		break;
	default: assert(0);
	}
	for (r = p->rules; r; r = r->next) {
		char *indent = "\t\t\0";
		switch (p->arity) {
		case 0: case -1:
			print("%2/* %R */\n", r);
			if (r->cost == -1) {
				print("%2c = %s;\n", r->code);
				EmitRecord ("\t\t", r, "c", 0);
			}
			else
				EmitRecord ("\t\t", r, r->code, 0);
			break;
		case 1:
			if (r->pattern->nterms > 1) {
				print("%2if (%1/* %R */\n", r);
				EmitTest (r->pattern->left, "LEFT_CHILD(a)", " ");
				print("%2) {\n");
				indent = "\t\t\t";
			}
			else
				print("%2/* %R */\n", r);
			if (r->pattern->nterms == 2 && r->pattern->left
				&&  r->pattern->right == NULL)
				EmitRecalc (indent, r->pattern->op, r->pattern->left->op);
			print("%sc = ", indent);
			EmitCost (r->pattern->left, "LEFT_CHILD(a)");
			print("%s;\n", r->code);
			EmitRecord (indent, r, "c", 0);
			if (indent[2])
				print("%2}\n");
			break;
		case 2:
			if (r->pattern->nterms > 1) {
				print("%2if (%1/* %R */\n", r);
				EmitTest (r->pattern->left, "LEFT_CHILD(a)",
					r->pattern->right->nterms ? " && " : " ");
				EmitTest (r->pattern->right, "RIGHT_CHILD(a)", " ");
				print("%2) {\n");
				indent = "\t\t\t";
			}
			else
				print("%2/* %R */\n", r);
			print("%sc = ", indent);
			EmitCost (r->pattern->left, "LEFT_CHILD(a)");
			EmitCost (r->pattern->right, "RIGHT_CHILD(a)");
			print("%s;\n", r->code);
			EmitRecord (indent, r, "c", 0);
			if (indent[2])
				print("%2}\n");
			break;
		default: assert(0);
		}
	}
	print("%2break;\n");
}

/* emitclosure - emit the closure functions */
PRIVATE void EmitClosure (IN PNONTERM nts) {

	PNONTERM p;

	for (p = nts; p; p = p->link)
	if (p->chain)
		print("static void %Pclosure_%S(NODEPTR_TYPE, int);\n", p);
	print("\n");
	for (p = nts; p; p = p->link)
	if (p->chain) {
		PRULE r;
		print("static void %Pclosure_%S(NODEPTR_TYPE a, int c) {\n"
			"%1struct %Pstate *p = STATE_LABEL(a);\n", p);
		for (r = p->chain; r; r = r->chain)
			EmitRecord ("\t", r, "c", r->cost);
		print("}\n\n");
	}
}

/* emitcost - emit cost computation for tree t */
PRIVATE void EmitCost (IN PTREE t, IN char *v) {

	PNONTERM p = t->op;

	if (p->kind == TERM) {
		if (t->left)
			EmitCost (t->left, stringf("LEFT_CHILD(%s)", v));
		if (t->right)
			EmitCost (t->right, stringf("RIGHT_CHILD(%s)", v));
	}
	else
		print("((struct %Pstate *)(%s->x.state))->cost[%P%S_NT] + ", v, p);
}

/* emitdefs - emit nonterminal defines and data structures */
PRIVATE void EmitDefs (IN PNONTERM nts, IN int ntnumber) {

	PNONTERM p;

	for (p = nts; p; p = p->link)
		print("#define %P%S_NT %d\n", p, p->number);
	print("\n");
	print("static char *%Pntname[] = {\n%10,\n");
	for (p = nts; p; p = p->link)
		print("%1\"%S\",\n", p);
	print("%10\n};\n\n");
}

PRIVATE void EmitTop(void) {

	char buf[100];
	time_t rawtime;
	struct tm *info;
	char*  top = "\
/************************************************************************\n\
*\n\
*	Instruction selector (generated %s)\n\
*\n\
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.\n\
*\n\
************************************************************************\n\n";

	time(&rawtime);
	info = localtime(&rawtime);
	strftime(buf, 80, "%x - %I:%M%p", info);
	print(top, buf);
	print("#ifndef IN\n");
	print("# define IN\n");
	print("# define OUT\n");
	print("#endif\n");
	print("#ifndef PRIVATE\n");
	print("# define PRIVATE static\n");
	print("# define PUBLIC\n");
	print("#endif\n");
}

/* emitheader - emit initial definitions */
PRIVATE void EmitHeader (void) {

	print("PRIVATE void %Pkids\t(IN NODEPTR_TYPE, IN int, IN NODEPTR_TYPE[]);\n");
	print("PRIVATE void %Plabel\t(IN NODEPTR_TYPE);\n");
	print("PRIVATE int %Prule\t(IN void*, IN int);\n\n");
}

/* computekids - compute paths to kids in tree t */
PRIVATE char *ComputeKids (IN PTREE t, IN char *v, IN char *bp, IN int *ip) {

	PTERM p = t->op;

	if (p->kind == NONTERM) {
		sprintf(bp, "\t\tkids[%d] = %s;\n", (*ip)++, v);
		bp += strlen(bp);
	}
	else if (p->arity > 0) {
		bp = ComputeKids(t->left, stringf("LEFT_CHILD(%s)", v), bp, ip);
		if (p->arity == 2)
			bp = ComputeKids(t->right, stringf("RIGHT_CHILD(%s)", v), bp, ip);
	}
	return bp;
}

/* emitkids - emit _kids */
PRIVATE void EmitKids (IN PRULE rules, IN int nrules) {

	int i;
	PRULE r, *rc = alloc((nrules + 1 + 1)*sizeof *rc);
	char **str = alloc((nrules + 1 + 1)*sizeof *str);

	for (i = 0, r = rules; r; r = r->link) {
		int j = 0;
		char buf[1024], *bp = buf;
		*ComputeKids (r->pattern, "p", bp, &j) = 0;
		for (j = 0; str[j] && strcmp(str[j], buf); j++)
			;
		if (str[j] == NULL)
			str[j] = strcpy(alloc(strlen(buf) + 1), buf);
		r->kids = rc[j];
		rc[j] = r;
	}
	print("static void %Pkids(NODEPTR_TYPE p, int eruleno, NODEPTR_TYPE kids[]) {\n"
		"%1if (!p)\n%2fatal(\"%Pkids\", \"Null tree\\n\", 0);\n"
		"%1if (!kids)\n%2fatal(\"%Pkids\", \"Null kids\\n\", 0);\n"
		"%1switch (eruleno) {\n");
	for (i = 0; (r = rc[i]) != NULL; i++) {
		for (; r; r = r->kids)
			print("%1case %d: /* %R */\n", r->ern, r);
		print("%s%2break;\n", str[i]);
	}
	print("%1default:\n%2fatal(\"%Pkids\", \"Bad rule number %%d\\n\", eruleno);\n%1}\n}\n\n");
}

/* emitlabel - emit label function */
PRIVATE void EmitLabel (IN PTERM terms, IN PNONTERM start, IN int ntnumber) {

	int i;
	PTERM p;

	print("static void %Plabel(NODEPTR_TYPE a) {\n%1int c;\n"
		"%1struct %Pstate *p;\n\n"
		"%1if (!a)\n%2fatal(\"%Plabel\", \"Null tree\\n\", 0);\n");
	print("%1STATE_LABEL(a) = p = allocate(sizeof *p, FUNC);\n"
		"%1p->rule._stmt = 0;\n");
	for (i = 1; i <= ntnumber; i++)
		print("%1p->cost[%d] =\n", i);
	print("%20x7fff;\n%1switch (OP_LABEL(a)) {\n");
	for (p = terms; p; p = p->link)
		EmitCase (p, ntnumber);
	print("%1default:\n"
		"%2fatal(\"%Plabel\", \"Bad terminal %%d\\n\", OP_LABEL(a));\n%1}\n}\n\n");
}

/* computents - fill in bp with _nts vector for tree t */
PRIVATE char* Computents (IN PTREE t, IN char *bp) {

	if (t) {
		PNONTERM p = t->op;
		if (p->kind == NONTERM) {
			sprintf(bp, "%s_%s_NT, ", prefix, p->name);
			bp += strlen(bp);
		}
		else
			bp = Computents(t->right, Computents(t->left, bp));
	}
	return bp;
}

/* emitnts - emit _nts ragged array */
PRIVATE void EmitNTS (IN PRULE rules, IN int nrules) {

	PRULE r;
	int i, j, *nts = alloc((nrules + 1)*sizeof *nts);
	char **str = alloc((nrules + 1)*sizeof *str);

	for (i = 0, r = rules; r; r = r->link) {
		char buf[1024];
		*Computents(r->pattern, buf) = 0;
		for (j = 0; str[j] && strcmp(str[j], buf); j++)
			;
		if (str[j] == NULL) {
			print("static short %Pnts_%d[] = { %s0 };\n", j, buf);
			str[j] = strcpy(alloc(strlen(buf) + 1), buf);
		}
		nts[i++] = j;
	}
	print("\nstatic short *%Pnts[] = {\n");
	for (i = j = 0, r = rules; r; r = r->link) {
		for (; j < r->ern; j++)
			print("%10,%1/* %d */\n", j);
		print("%1%Pnts_%d,%1/* %d */\n", nts[i++], j++);
	}	
	print("};\n\n");
}

/* emitrecalc - emit code that tests for recalculation of INDIR?(VREGP) */
PRIVATE void EmitRecalc (IN char *pre, IN PTERM root, IN PTERM kid) {

	if (root->kind == TERM && strncmp(root->name, "INDIR", 5) == 0
		&& kid->kind == TERM &&  strcmp(kid->name, "VREGP") == 0) {
		PNONTERM p;
		print("%sif (mayrecalc(a)) {\n", pre);
		print("%s%1struct %Pstate *q = a->syms[RX]->u.t.cse->x.state;\n", pre);
		for (p = nts; p; p = p->link) {
			print("%s%1if (q->cost[%P%S_NT] == 0) {\n", pre, p);
			print("%s%2p->cost[%P%S_NT] = 0;\n", pre, p);
			print("%s%2p->rule.%P%S = q->rule.%P%S;\n", pre, p, p);
			print("%s%1}\n", pre);
		}
		print("%s}\n", pre);
	}
}

/* emitrecord - emit code that tests for a winning match of rule r */
PRIVATE void EmitRecord (IN char *pre, IN PRULE r, IN char *c, IN int cost) {

	if (Tflag)
		print("%s%Ptrace(a, %d, %s + %d, p->cost[%P%S_NT]);\n",
		pre, r->ern, c, cost, r->lhs);
	print("%sif (", pre);
	print("%s + %d < p->cost[%P%S_NT]) {\n"
		"%s%1p->cost[%P%S_NT] = %s + %d;\n%s%1p->rule.%P%S = %d;\n",
		c, cost, r->lhs, pre, r->lhs, c, cost, pre, r->lhs,
		r->packed);
	if (r->lhs->chain)
		print("%s%1%Pclosure_%S(a, %s + %d);\n", pre, r->lhs, c, cost);
	print("%s}\n", pre);
}

/* emitrule - emit decoding vectors and _rule */
PRIVATE void EmitRule (IN PNONTERM nts) {

	PNONTERM p;

	for (p = nts; p; p = p->link) {
		PRULE r;
		print("static short %Pdecode_%S[] = {\n%10,\n", p);
		for (r = p->rules; r; r = r->decode)
			print("%1%d,\n", r->ern);
		print("};\n\n");
	}
	print("static int %Prule(void *state, int goalnt) {\n"
		"%1if (goalnt < 1 || goalnt > %d)\n%2fatal(\"%Prule\", \"Bad goal nonterminal %%d\\n\", goalnt);\n"
		"%1if (!state)\n%2return 0;\n%1switch (goalnt) {\n", ntnumber);
	for (p = nts; p; p = p->link)
		print("%1case %P%S_NT:"
		"%1return %Pdecode_%S[((struct %Pstate *)state)->rule.%P%S];\n", p, p, p);
	print("%1default:\n%2fatal(\"%Prule\", \"Bad goal nonterminal %%d\\n\", goalnt);\n%2return 0;\n%1}\n}\n\n");
}

/* emitstring - emit arrays of templates, instruction flags, and rules */
PRIVATE void EmitString(IN PRULE rules) {

	PRULE r;

	print("static char *%Ptemplates[] = {\n");
	print("/* 0 */%10,\n");
	for (r = rules; r; r = r->link)
		print("/* %d */%1\"%s\",%1/* %R */\n", r->ern, r->template, r);
	print("};\n");
	print("\nstatic char %Pisinstruction[] = {\n");
	print("/* 0 */%10,\n");
	for (r = rules; r; r = r->link) {
		int len = strlen(r->template);
		print("/* %d */%1%d,%1/* %s */\n", r->ern,
			len >= 2 && r->template[len - 2] == '\\' && r->template[len - 1] == 'n',
			r->template);
	}
	print("};\n");
	print("\nstatic char *%Pstring[] = {\n");
	print("/* 0 */%10,\n");
	for (r = rules; r; r = r->link)
		print("/* %d */%1\"%R\",\n", r->ern, r);
	print("};\n\n");
}

/* emitstruct - emit the definition of the state structure */
PRIVATE void EmitStruct(IN PNONTERM nts, IN int ntnumber) {

	print("struct %Pstate {\n%1short cost[%d];\n%1struct {\n", ntnumber + 1);
	for (; nts; nts = nts->link) {
		int n = 1, m = nts->lhscount;
		while ((m >>= 1) != 0)
			n++;
		print("%2unsigned int %P%S:%d;\n", nts, n);
	}
	print("%1} rule;\n};\n\n");
}

/* emittest - emit clause for testing a match */
PRIVATE void EmitTest (IN PTREE t, IN char *v, IN char *suffix) {

	PTERM p = t->op;

	if (p->kind == TERM) {
		print("%3%s->op == %d%s/* %S */\n", v, p->esn,
			t->nterms > 1 ? " && " : suffix, p);
		if (t->left)
			EmitTest (t->left, stringf("LEFT_CHILD(%s)", v),
			t->right && t->right->nterms ? " && " : suffix);
		if (t->right)
			EmitTest (t->right, stringf("RIGHT_CHILD(%s)", v), suffix);
	}
}

/*
spec := decls PPERCENT rules
     | decls
     ;

decls:= lambda
     | decls decl
     ;

decl := TERMINAL  blist '\n'
     | START nonterm '\n'
     | '\n'
     | error '\n'
     ;

blist := lambda
      | blist ID '=' INT
      ;

rules := lambda
    // rules nonterm ':' tree '=' INT cost ';'
    | rules nonterm ':' tree '=' TEMPLATE cost ';'
    | rules '\n'
    | rules error '\n'
    ;

nonterm := ID
        ;

tree := ID
     | ID '(' tree ')'
     | ID '(' tree ',' tree ')'
     ;

cost := CODE
*/

#define TOKEN_TERM       1
#define TOKEN_START      2
#define TOKEN_IDENT      3
#define TOKEN_TEMPLATE   4
#define TOKEN_CODE       5
#define TOKEN_NUMBER     6
#define TOKEN_ERR        7
#define TOKEN_PPERCENT   8
#define TOKEN_COLON      9
#define TOKEN_NEWLINE   10
#define TOKEN_EQ        11
#define TOKEN_LPARAN    12
#define TOKEN_RPARAN    13
#define TOKEN_COMMA     14
#define TOKEN_EOF       15

typedef struct TOKEN {
	int    type;
	char*  str;
	int    num;
}TOKEN;

void printtok(IN TOKEN tok) {
	switch (tok.type) {
	case TOKEN_TERM: fprintf(stdout, "%%term\n"); break;
	case TOKEN_START: fprintf(stdout, "%%start\n"); break;
	case TOKEN_IDENT: fprintf(stdout, "ident\t%s\n", tok.str); break;
	case TOKEN_TEMPLATE: fprintf(stdout, "tmpl\t'%s'\n", tok.str); break;
	case TOKEN_CODE: fprintf(stdout, "code\t'%s'\n", tok.str); break;
	case TOKEN_NUMBER: fprintf(stdout, "num\t'%i'\n", tok.num); break;
	case TOKEN_ERR: fprintf(stdout, "error\n"); break;
	case TOKEN_PPERCENT: fprintf(stdout, "op\t%%%%\n"); break;
	case TOKEN_COLON: fprintf(stdout, "op\t:\n"); break;
	case TOKEN_NEWLINE: fprintf(stdout, "newline\n"); break;
	case TOKEN_EQ: fprintf(stdout, "op\t=\n"); break;
	case TOKEN_LPARAN: fprintf(stdout, "op\t(\n"); break;
	case TOKEN_RPARAN: fprintf(stdout, "op\t)\n"); break;
	case TOKEN_COMMA: fprintf(stdout, "op\t,\n"); break;
	case TOKEN_EOF: fprintf(stdout, "op\tEOF\n"); break;
	default: fprintf(stdout, "<invalid>\n"); __debugbreak();
	}
}

TOKEN NewToken(int type) {
	TOKEN tok;
	tok.type = type;
	tok.str = NULL;
	tok.num = 0;
	return tok;
}

TOKEN NewTokenStr(int type, char* str) {
	TOKEN tok;
	tok = NewToken(type);
	tok.str = str;
	return tok;
}

TOKEN NewTokenInt(int num) {
	TOKEN tok;
	tok = NewTokenStr(TOKEN_NUMBER, NULL);
	tok.num = num;
	return tok;
}

int fpeekc(IN FILE* stream) {
	int c;
	c = fgetc(stream);
	ungetc(c, stream);
	return c;
}

#define GET_TEXT 1
#define GET_NUM  2

char* getstr(IN int type, IN int delim) {
	int c;
	int k;
	bp = buf;
	k = 0;
	if (!type)
		type = GET_TEXT;
	while (1) {
		c = fgetc(infp);
		if (k++ == BUFSIZ) {
			fscanf(stderr, "Identifier too large");
			exit(-1);
		}
		if (c == EOF) {
			fscanf(stderr, "Premature end of file");
			return NULL;
		}
		if (delim && c == delim)
			break; //consume it
		if (delim) {
			if (c == '\n' || c == '\r') {
				fscanf(stderr, "Premature end of line");
				break;
			}
			*bp++ = c;
		}
		else if (type == GET_NUM && isdigit(c))
			*bp++ = c;
		else if (type == GET_TEXT && (isdigit(c) || isalpha(c) || c == '_' || c == '$'))
			*bp++ = c;
		else {
			ungetc(c, infp);
			break;
		}
	}
	*bp = '\0';
	bp = buf;
	if (!buf)
		__debugbreak();
	return buf;
}

TOKEN keytok(void) {
	char* s;
	s = getstr(GET_TEXT, 0);
	if (!s) return NewToken(TOKEN_ERR);
	if (!strcmp(s, "start")) return NewToken(TOKEN_START);
	else if (!strcmp(s, "term")) return NewToken(TOKEN_TERM);
	return NewToken(TOKEN_ERR);
}

TOKEN identtok(void) {
	char* s;
	s = getstr(GET_TEXT, 0);
	if (!s) return NewToken(TOKEN_ERR);
	return NewTokenStr(TOKEN_IDENT, _strdup(s));
}

TOKEN codetok(void) {
	char* s;
	s = getstr(GET_TEXT, '\n');
	if (!s) return NewToken(TOKEN_ERR);
	if (*s == 0)
		return NewTokenStr(TOKEN_CODE, "0");
	return NewTokenStr(TOKEN_CODE, _strdup(s));
}

TOKEN templatetok(void) {
	char* s;
	s = getstr(GET_TEXT, '"');
	if (!s) return NewToken(TOKEN_ERR);
	return NewTokenStr(TOKEN_TEMPLATE, _strdup(s));
}

TOKEN numtok(void) {
	char* s;
	s = getstr(GET_NUM, 0);
	if (!s)
		return NewToken(TOKEN_ERR);
	return NewTokenInt(strtol(s, NULL, 10));
}

void skipws(void) {
	int   c;
	while (1) {
		c = fgetc(infp);
		if (c == EOF) return;
		if (c == '\n' || !isspace(c)) {
			ungetc(c, infp);
			return;
		};
	}
}

TOKEN Lex(void) {
	int   c;
	if (code) {
		code--;
		skipws();
		return codetok();
	}
	while (1) {
		if (feof(infp))
			return NewToken(TOKEN_EOF);
		c = fgetc(infp);
		if (c == EOF) return NewToken(TOKEN_EOF);
		if (c == '\r' || c == '\n') {
			if (c == '\r' && fpeekc(infp) == '\n')
				fgetc(infp);
			return NewToken(TOKEN_NEWLINE);
		}
		if (iswspace(c)) continue;
		if (!isprint(c)) {
			fprintf(stderr, "Unrecognizable character");
			__debugbreak();
			exit(-1);
		}
		// %{ ... }% are code blocks:
		if (c == '%' && fpeekc(infp) == '{') {
			fgetc(infp); // consume '{'
			ungetc('\n', infp);
			while (1) {
				if (fgets(buf, sizeof buf, infp) == NULL) {
					fprintf(stderr, "Unterminated %{ ... %}");
					return NewToken(TOKEN_EOF);
				}
				if (!strcmp(buf, "%}\n"))
					break;
				fputs(buf, outfp);
			}
			buf[0] = '\0';
			continue;
		}
		// multi-line comments
		else if (c == '/' && fpeekc(infp) == '*') {
			while (1) {
				c = fgetc(infp);
				if (c == EOF) {
					fprintf(stderr, "End of file reached processing comment");
					return NewToken(TOKEN_EOF);
				}
				if (c == '*' && fpeekc(infp) == '/') {
					fgetc(infp); // consume '/'
					break;
				}
			}
		}
		// single line comments
		else if (c == '/' && fpeekc(infp) == '/') {
			while (1) {
				c = fgetc(infp);
				if (c == EOF) return NewToken(TOKEN_EOF);
				if (c == '\r' || c == '\n') {
					if (c == '\r' && fpeekc(infp) == '\n')
						fgetc(infp);
					return NewToken(TOKEN_NEWLINE);
				}
			}
		}
		else if (c == ',') return NewToken(TOKEN_COMMA);
		else if (c == ':') return NewToken(TOKEN_COLON);
		else if (c == '(') return NewToken(TOKEN_LPARAN);
		else if (c == ')') return NewToken(TOKEN_RPARAN);
		else if (c == '=') return NewToken(TOKEN_EQ);
		else if (c == '%' && fpeekc(infp) == '%') {
			fgetc(infp); // consume '%'
			return ppercent++ ? NewToken(TOKEN_EOF) : NewToken(TOKEN_PPERCENT);
		}
		else if (c == '\"') return templatetok();
		else if (c == '%') return keytok();
		else if (isdigit(c)) {
			ungetc(c, infp);
			return numtok();
		}
		else if (c == '_' || c == '$' || isalpha(c)) {
			ungetc(c, infp);
			return identtok();
		}
		else return NewToken(TOKEN_ERR);
	}
}

TOKEN currenttok;
TOKEN pushback[3];
int   pushbackTop = -1;

PRIVATE TOKEN Next(void) {
	if (pushbackTop >= 0)
		currenttok = pushback[pushbackTop--];
	else
		currenttok = Lex();
	return currenttok;
}

PRIVATE void Unget(IN TOKEN tok) {
	if (pushbackTop > 2) {
		fprintf(stderr, "internal parse error");
		__debugbreak();
		exit(-1);
	}
	pushback[++pushbackTop] = tok;
	currenttok = tok;
}

PRIVATE TOKEN Current() {
	return currenttok;
}

PRIVATE TOKEN Peek() {

	TOKEN tok = Next();
	Unget(tok);
	return tok;
}

PRIVATE int Accept(IN int token) {

	TOKEN tok = Current();
	return tok.type == token;
}

PRIVATE int Expect (IN int token) {

	if (!Accept(token)) {
		fprintf(stderr, "Unexpected token");
		__debugbreak();
		//		next();
		//		unget(newtok(TOKEN_ERR));
		return 0;
	}
	return 1;
}

PRIVATE void ParseBList(void) {

	char* ident;
	while (Accept(TOKEN_IDENT)) {
		ident = currenttok.str;
		Next();
		if (!Expect(TOKEN_EQ))
			return;
		Next();
		if (!Expect(TOKEN_NUMBER))
			return;
		NewTerm(ident, currenttok.num);
		Next();
	}
}

PRIVATE PNONTERM ParseNonTerm(void) {

	PNONTERM nt = NULL;
	if (Accept(TOKEN_IDENT)) {
		nt = NewNonterm(currenttok.str);
		Next();
	}
	return nt;
}

PRIVATE void ParseDecls (void) {

	while (1) {
		if (Accept(TOKEN_START)) {
			Next();
			if (!ParseNonTerm())
				Expect(TOKEN_IDENT);
			Expect(TOKEN_NEWLINE);
			Next();
		}
		else if (Accept(TOKEN_TERM)) {
			Next();
			ParseBList();
			Expect(TOKEN_NEWLINE);
			Next();
		}
		else if (Accept(TOKEN_NEWLINE))
			Next();
		else if (Accept(TOKEN_ERR)) {
			// consume tokens until EOL or EOF
			__debugbreak();
			Next();
		}
		else break;
	}
}

PRIVATE PTREE ParseTree(void) {

	char* ident;
	PTREE left;
	PTREE right;

	if (!Expect(TOKEN_IDENT))
		return NULL;
	ident = currenttok.str;
	left = NULL;
	right = NULL;
	Next();
	if (Accept(TOKEN_LPARAN)) {
		Next();
		left = ParseTree();
		if (Accept(TOKEN_COMMA)) {
			Next();
			right = ParseTree();
			if (!Expect(TOKEN_RPARAN))
				return NULL;
		}
		else if (!Expect(TOKEN_RPARAN))
			return NULL;
		Next(); // consume ')'
	}
	return NewTree(ident, left, right);
}

PRIVATE void ParseRules (void) {

	PNONTERM nt;
	PTREE    tr;
	char*    tmpl;
	char*    codestr;

	while (1) {
		if (Accept(TOKEN_NEWLINE))
			Next();
		else if (Accept(TOKEN_ERR)) {
			// consume tokens until EOL or EOF
			__debugbreak();
			Next();
		}
		else {
			nt = ParseNonTerm();
			if (nt) {
				if (!Expect(TOKEN_COLON))
					return;
				Next();
				tr = ParseTree();
				if (!tr)
					__debugbreak();
				if (!Expect(TOKEN_TEMPLATE))
					__debugbreak();
				tmpl = currenttok.str;
				code++; // prepare to scan TOKEN_CODE
				Next(); // consumes to (and including) newline
				Accept(TOKEN_CODE);
				codestr = currenttok.str;
				Next();
				NewRule(nt->name, tr, tmpl, codestr);
			}
			else break;
		}
	}
}

PRIVATE void ParseSpec(void) {

	ParseDecls();
	if (Accept(TOKEN_PPERCENT)) {
		Next();
		ParseRules();
	}
}

PRIVATE void Parse(void) {

	Next();
	ParseSpec();
}

int main (IN int argc, IN char *argv[]) {

	int c, i;
	PNONTERM p;

	for (i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-T") == 0)
			Tflag = 1;
		else if (strncmp(argv[i], "-p", 2) == 0 && argv[i][2])
			prefix = &argv[i][2];
		else if (strncmp(argv[i], "-p", 2) == 0 && i + 1 < argc)
			prefix = argv[++i];
		else if (*argv[i] == '-' && argv[i][1]) {
			fprintf(stderr, "usage: %s [-T | -p prefix]... [ [ input ] output ] \n", argv[0]);
			exit(1);
		}
		else if (infp == NULL) {
			if (strcmp(argv[i], "-") == 0)
				infp = stdin;
			else if ((infp = fopen(argv[i], "r")) == NULL) {
				fprintf(stderr, "%s: can't read `%s'\n", argv[0], argv[i]);
				exit(1);
			}
		}
		else if (outfp == NULL) {
			if (strcmp(argv[i], "-") == 0)
				outfp = stdout;
			if ((outfp = fopen(argv[i], "w")) == NULL) {
				fprintf(stderr, "%s: can't write `%s'\n", argv[0], argv[i]);
				exit(1);
			}
		}
	}
	if (infp == NULL)
		infp = stdin;
	if (outfp == NULL)
		outfp = stdout;
	EmitTop();
	Parse();
	if (start)
		CheckReach(start);
	for (p = nts; p; p = p->link) {
		if (p->rules == NULL)
			fprintf(stderr, "undefined nonterminal `%s'\n", p->name);
		if (!p->reached)
			fprintf(stderr, "can't reach nonterminal `%s'\n", p->name);
	}
	EmitHeader();
	EmitDefs(nts, ntnumber);
	EmitStruct(nts, ntnumber);
	EmitNTS(rules, nrules);
	EmitString(rules);
	EmitRule(nts);
	EmitClosure(nts);
	if (start)
		EmitLabel(terms, start, ntnumber);
	EmitKids(rules, nrules);
	if (!feof(infp))
	while ((c = getc(infp)) != EOF)
		putc(c, outfp);
	while (memlist) {	/* for purify */
		struct BLOCK *q = memlist->link;
		free(memlist);
		memlist = q;
	}
	return errcnt > 0;
}
