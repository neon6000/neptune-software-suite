/************************************************************************
*                                                                       *
*	option.h															*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

#ifndef OPTION_H_
#define OPTION_H_

/**
*	option id's must match _options list for easy lookup
*/
typedef enum _optionId {

	OPTION_H = 0,
	OPTION_B,
	OPTION_U,
	OPTION_E,
	OPTION_X,
	OPTION_S,
	OPTIONS_MAX

}optionId;

/**
*	option paramater types
*/
typedef enum _optionType {

	OPTION_NONE     = 0,
	OPTION_VALUE    = 1,
	OPTION_STRING   = 2

}optionType;

/**
*	nasm option
*/
typedef struct _option {

	char*      name;				//! name
	char*      desc;				//! description
	optionType type;				//! paramater type
	union {
		int value;					//! numerical value or string name
		char* string;
	}u;

}option;

/**
*	global options
*/
typedef struct _disasmOptions {

	unsigned int operationMode;
	unsigned int orgin;
	unsigned int startOffset;

}disasmOptions;
extern disasmOptions _globalOptions;

//! options
extern option _options [OPTIONS_MAX];

extern void      displayHelp ();
extern void      optionsDisplay    ();
extern optionId  getOptionIdByName (char* opt);
extern void      parseOptions      (int argc, char** argv);

#endif
