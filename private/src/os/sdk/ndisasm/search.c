/************************************************************************
*                                                                       *
*	search.c															*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

#include <string.h>
#include <search.h>

/**
*	binary search an array of strings. returns index in array
*/
int binsearch(char **str, int max, char *value) {

	int position = 0;
	int begin = 0; 
	int end = max - 1;
	int cond = 0;

	while(begin <= end) {
		position = (begin + end) / 2;
	if((cond = strncmp(str[position], value, strlen(str[position]) )) == 0)
		return position;
	else if(cond < 0)
		begin = position + 1;
	else
		end = position - 1;
	}

	return -1;
}

/**
*	linear search an array of strings. Returns index
*	span is number of bytes separating strings. 0 if none
*/
int search (char *str, int max, char* p, unsigned int stride) {

	int count = 0;
	char* currentStr = str;

	for (count = 0; count < max; count++) {
		if (strcmp (currentStr, p) == 0)
			return count;

		if (!stride)
			currentStr -= strlen (str) + 1;
		else
			currentStr -= stride + 1;
	}

	return -1;
}
