/************************************************************************
*                                                                       *
*	option.c															*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

/*
	Manages disassembler command line options
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "option.h"
#include "main.h"

/**
*	global options
*/
disasmOptions _globalOptions = {
	16,
	0,
	0
};

/**
*	nasm options table
*/
option _options [OPTIONS_MAX] = {

	{ "-h",            "-h\t\tLists options and command syntax",            OPTION_NONE, 0},
	{ "-b",	           "-b=value\tSet operation mode. Default 16 bit",      OPTION_VALUE, 0},
	{ "-u",            "-u\t\tSet 32 bit operation mode",                   OPTION_NONE, 0},
	{ "-e",            "-e=value\tSkip n bytes from start",                 OPTION_VALUE, 0},
	{ "-x",            "-x=name\tTranslator module. Default \'x86\'",       OPTION_STRING, 0},
	{ "-s",            "-s=name\tParser module. Default \'parser\'",        OPTION_STRING, 0}
};

/**
*	displays supported options
*/
void optionsDisplay () {

	int i=0;

	printf ("Option\tDescription\n\r");
	printf ("------\t-----------\n\r");
	for (i=0; i < OPTIONS_MAX; i++)
		printf ("%s\t %s\n\r", _options[i].name, _options[i].desc);
}

/**
*	get option id from name
*/
optionId getOptionIdByName (char* opt) {

	if (opt) {

		int id = 0;

		if (opt [0] != '-')
			return OPTIONS_MAX; //ERROR invalid option

		//! search for option
		for (id = 0; id < OPTIONS_MAX; id++)
			if (strncmp (opt, _options[id].name, strlen(_options[id].name) ) == 0)
				return id;
	}

	return OPTIONS_MAX;
}

/**
*	display help info
*/
void displayHelp () {

	printf ("Neptune Disassembler\n\r");
	printf ("Copyright BrokenThorn Entertainment, Co\n\r");
	printf ("Syntax: ndisasm [option list] inputFiles\n\r\n\r");
	optionsDisplay ();
}

/**
*	parse command line
*/
void parseOptions (int argc, char** argv) {

	const char separator = '=';
	int c;

	for (c=1; c<argc; c++) {
		if (argv[c][0] == '-') {

			optionId option = getOptionIdByName (argv[c]);
			switch (option) {
				case OPTION_B: {
					if (argv[c][2] == separator) {
						unsigned int value = strtol(&argv[c][3],0,10);
						if (value == 16 || value == 32 ||value == 64) {
							_globalOptions.operationMode = value;
						}
						else
							printf ("\n\rInvalid operation mode, ignoring");
					}
					break;
				}
				case OPTION_U: {
					_globalOptions.operationMode = 32;
					break;
				}
				case OPTION_E: {
					if (argv[c][2] == separator) {
						unsigned int value = strtol(&argv[c][3],0,16);
						_globalOptions.startOffset = value;
					}
					break;
				}
				case OPTION_H:
				default: {
					displayHelp ();
					break;
				}
			}
		}
	}
}
