/************************************************************************
*                                                                       *
*	main.c																*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

/*
	Manages program image and Neptune Disassembler entry point
*/

#ifdef _MSC_VER
#ifdef _DEBUG
#include <crtdbg.h>
#endif
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "option.h"
#include "list.h"
#include "x86/cpu.h"

/**
*	program info
*/
program _programInfo;

/**
*	add image file
*/
bool addImageFile (char* name) {

	imageFile* image=0;
	if (!name)
		return false;

	image = allocateImageFile (name);
	if (!image)
		return 0;

	listAddElement (image, &_programInfo.images);
	return true;
}

/**
*	free image files
*/
void freeAllImageFiles () {

	listNode* node = _programInfo.images.first;
	while (node) {
		imageFile* image = (imageFile*)node->data;
		freeImageFile (image);
		node = node->next;
	}

	listFreeAll (&_programInfo.images);
	_programInfo.current=0;
}

/**
*	initialize
*/
bool initialize (int argc, char** argv) {

	int c=0;
	parseOptions (argc, argv);

	//! all other operands are input files
	for (c=1; c<argc; c++) {
		if (argv[c][0] != '-') {
			if (!addImageFile (argv[c]))
				printf ("\n\rUnable to open \'%s\', skipping", argv[c]);
		}
	}

	return true;
}

/**
*	release resources
*/
void shutdown () {

	freeAllImageFiles ();

#ifdef _MSC_VER
#ifdef _DEBUG
	_CrtDumpMemoryLeaks ();
#endif
#endif
}

/**
*	disassemble program
*/
void disassembleProgram () {

	listNode* node = 0;
	node = _programInfo.images.first;
	while (node) {

		imageFile* image = (imageFile*) node->data;

		printf ("\n\n\rDisassembling '%s'...\n\r", image->name);
		disassembleImage(image);
		node = node->next;
	}
}

/**
*	entry point
*/
int main (int argc, char** argv) {  

	if (argc==1) {
		displayHelp();
		return 0;
	}
	if (!initialize (argc, argv)) {
		shutdown ();
		return 0;
	}
	disassembleProgram ();
	shutdown ();
	return 0;
}
