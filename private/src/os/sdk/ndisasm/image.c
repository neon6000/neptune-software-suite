/************************************************************************
*                                                                       *
*	image.c																*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

/*
	Manages program image files
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "image.h"

/**
*	locate image by name
*/
FILE* openImage (char* name) {

	FILE* pFile = 0;
	if (!name)
		return 0;
#ifdef _MSC_VER
	fopen_s (&pFile, name, "rb");
#else
	pFile = fopen (name, "rb");
#endif
	return pFile;
}

/**
*	returns file size in bytes
*/
unsigned int getFileSize (FILE* pFile) {

	unsigned int size=0;

	fseek (pFile, 0, SEEK_END);
	size = ftell (pFile);

	fseek (pFile, 0, SEEK_SET);
	return size;
}

/**
*	release memory
*/
void freeImageFile (imageFile* image) {

	if (image)
		free (image->buffer);
}

/**
*	allocate image file
*/
imageFile* allocateImageFile (char* name) {

	imageFile* image = 0;

	FILE* pFile = openImage (name);
	if (pFile) {

		//! get file size
		unsigned int size = getFileSize (pFile);

		//! allocate image
		image = (imageFile*) malloc (sizeof (imageFile));
		if (!image) {
			fclose (pFile);
			return 0;
		}

		//! read image
		image->buffer = (unsigned char*)malloc (size+1);
		fread (image->buffer, 1, size, pFile);
		fclose (pFile);

		//! initialize
		image->currentPos = image->buffer;
		image->size = size;
#ifdef _MSC_VER
		strncpy_s(image->name,32,name,32);
#else
		strncpy (image->name, name, 32);
#endif
	}

	return image;
}
