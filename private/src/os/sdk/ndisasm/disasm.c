/************************************************************************
*                                                                       *
*	disasm.c															*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

/*
	Manages disassembling of image files
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "option.h"
#include "main.h"

/**
*	disassemble image
*/
void disassembleImage (imageFile* image) {

	unsigned int offset = _globalOptions.startOffset;

	//! FIXME address displacements should be +origin

	while (offset < image->size) {

		unsigned int size = 0;
		unsigned int c = 0;
		unsigned char machineCode[16] = {0};
		instr instruction = newInstr();

		//US 0x00000738      0x83790400              add [ecx+0x4], 0x0
		//NASM 00000738  83790400          cmp dword [ecx+0x4],byte +0x0

		//0x0000072A      0x0F
		if (offset==0x00000728)
			__asm __emit 0xcc

		size = disassemble(&image->buffer[offset], instruction);
		if (size == -1)
			size = 1;	//invalid code, so skip this byte

		//! print offset
		printf ("0x%08X\t", offset);

		//! print machine code
		memcpy (machineCode, &image->buffer[offset], size);
		printf ("0x");
		for (c=0; c<size; c++)
			printf ("%02X", (unsigned char) machineCode[c]);
		c+=2;

		while (c++ < 16)
			printf (" ");
		printf ("\t");

		displayInstruction (instruction);

		free (instruction);

		offset += size;
		printf ("\n\r");
	}
}
