/************************************************************************
*                                                                       *
*	main.h																*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include "image.h"

#ifndef bool
#define bool int
#define true 1
#define false 0
#endif

/**
*	program
*/
typedef struct _program {

	imageFileList images;
	imageFile* current;

}program;
extern program _programInfo;

typedef void* instr;

#if 0
	typedef unsigned int (*disassembleInstruction) (void* instruction,instr* out);
	typedef void         (*displayInstruction) (instr in);
	typedef instr        (*newInstr) ();
	typedef int          (*setTranslationMode) (int mode);

	extern disassembleInstruction lpfnDisassemble;
	extern newInstr               lpfnNewInstr;
	extern displayInstruction     lpfnDisplay;
	extern setTranslationMode     lpfnTranslationMode;
#endif

#endif
