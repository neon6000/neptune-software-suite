/************************************************************************
*                                                                       *
*	search.h															*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

#ifndef SEARCH_H_INCLUDED
#define SEARCH_H_INCLUDED

extern
int binsearch (char **str, int max, char *value);

extern
int search (char *str, int max, char* p, unsigned int stride);

#endif
