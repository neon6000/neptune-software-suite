/************************************************************************
*                                                                       *
*	list.h																*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

/*
	generic linked list implementation. This handles allocations
	and freeing of listNodes in a lists structure. The calling code should
	malloc() or free() the data ptr as needed
*/

/**
*	list node
*/
typedef struct _listNode {

	struct _listNode* next;
	struct _listNode* prev;
	void* data;

}listNode;

/**
*	linked list
*/
typedef struct _list {

	unsigned int count;
	listNode* first;
	listNode* last;

}list;

extern
list* listInitialize (list* root);

extern
listNode* listAddElement (void* data,
					  list* root);

extern
unsigned int listSize (list* root);

extern
listNode* listRemoveElement (unsigned int num,
								list* root);

extern
void listFreeAll (list* root);

#endif
