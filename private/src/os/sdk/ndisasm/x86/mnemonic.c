/************************************************************************
*                                                                       *
*	x86.c																*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

#include "mnemonic.h"
#include <search.h>

/**
*	mnumeric list. Must match x86_mnumericId
*/
char* _mnemonicStr [MNUMERIC_MAX] = {
	"aaa",
	"aad",
	"aam",
	"aas",
	"adc",
	"add",
	"amx",
	"and",
	"arpl",
	"bound",
	"callf",
	"call",
	"cbw",
	"cdq",
	"clc",
	"cld",
	"cli",
	"cmc",
	"cmpsb",
	"cmpsd",
	"cmpsw",
	"cmp",
	"cwde",
	"cwd",
	"daa",
	"das",
	"dec",
	"div",
	"enter",
	"hlt",
	"idiv",
	"imul",
	"inc",
	"insb",
	"insw",
	"into",
	"int",
	"int",
	"in",
	"iret",
	"jae",
	"ja",
	"jbe",
	"jb",
	"je",
	"jge",
	"jg",
	"jle",
	"jl",
	"jmp",
	"jne",
	"jnl",
	"jno",
	"jnp",
	"jns",
	"jo",
	"jpe",
	"js",
	"lahf",
	"lds",
	"leave",
	"lea",
	"les",
	"lodsb",
	"lodsw",
	"loopnz",
	"loop",
	"movsb",
	"movsd",
	"movsw",
	"mov",
	"mul",
	"neg",
	"nop",
	"not",
	"or",
	"outsb",
	"outsd",
	"outsw",
	"outs",
	"out",
	"pop",
	"popa",
	"popf",
	"pushad",
	"pusha",
	"pushf",
	"push",
	"rcl",
	"rcr",
	"ret",
	"retf",
	"rol",
	"ror",
	"sahf",
	"sal",
	"sar",
	"sbb",
	"scasb",
	"shl",
	"shr",
	"stc",
	"std",
	"sti",
	"stosb",
	"stosw",
	"sub",
	"test",
	"xchg",
	"xor"
};

/**
*	find mnumeric by name
*/
int findMnemonicId (char* name) {

	return binsearch(_mnemonicStr, MNUMERIC_MAX, name);
}

/**
*	find mnumeric by id
*/
char* findMnemonicName (unsigned id) {

	if (id >= MNUMERIC_MAX)
		return 0;

	return _mnemonicStr[id];
}
