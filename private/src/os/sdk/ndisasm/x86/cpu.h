/************************************************************************
*                                                                       *
*	x86.h																*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

/*
	x86 Translator API
*/

#ifndef X86_H_INCLUDED
#define X86_H_INCLUDED

#ifdef _MSC_VER
#ifdef DYNAMIC_BUILD
# define X86DECL __declspec(dllexport)
#else
# define X86DECL
#endif
#endif

#undef MAX_OPERAND
#undef PREFIX_COUNT
#undef MAX_PREFIX

#define MAX_OPERAND 4
#define PREFIX_COUNT 8
#define MAX_PREFIX 4

/**
*	operand types (can bitwise OR)
*/
typedef enum _x86_operandType {

	NONE		=	0,
	MEM8		=	0x00000001,
	MEM16		=	0x00000002,
	MEM32		=	0x00000004,
	REG8		=	0x00000008,
	REG16		=	0x00000010,
	REG32		=	0x00000020,
	DISP8		=	0x00000040,
	DISP16		=	0x00000080,
	DISP32		=	0x00000100,
	IMM8		=	0x00000200,
	IMM16		=	0x00000400,
	IMM32		=	0x00000800,

	EAX			=	0x00001000,
	AX			=	0x00002000,
	AL			=	0x00004000,
	AH          =   0x00008000,

	ECX			=	0x00010000,
	CX			=	0x00020000,

	CS			=	0x00040000,
	DS			=	0x00080000,
	ES			=	0x00100000,
	FS			=	0x00200000,
	GS			=	0x00400000,
	SS			=	0x00800000,

	EDX			=	0x01000000,
	DX			=	0x02000000,

	DI			=	0x04000000,
	EDI			=	0x08000000,

	SI			=	0x10000000,
	ESI			=	0x20000000,

	MEM			=	MEM8 | MEM16 | MEM32,
	REG			=	REG8 | REG16 | REG32,
	DISP		=	DISP8 | DISP16 | DISP32,
	IMM			=	IMM8 | IMM16 | IMM32,
	ACC			=	AL | AH | AX | EAX,

}x86_operandType;

/**
*	instruction operation mode
*/
typedef enum _x86_operationMode {

	BIT16			=   0x00000001,
	BIT32			=   0x00000002,
	BIT64			=   0x00000004,		//bits 0-3 of flags
	ANYMODE =   BIT16 | BIT32 | BIT64

}x86_operationMode;

/**
*	register flags (can be ORed together)
*/
typedef enum _x86_registerClass {

	CONTROL				=	0x00010000,
	DEBUG				=	0x00020000,
	TEST_REG			=	0x00040000,
	SEG					=	0x00080000,
	MMX_REG				=	0x00100000,
	XMMX				=	0x00200000,
	FLOAT				=	0x00400000,
	BASEINDEX			=	0x00800000,
	IOPORT				=	0x01000000

}x86_registerClass;

/**
*	instruction documentation
*/
typedef enum _x86_documentation {

	DOCUMENTED		=	0x00000000,
	UNDOCUMENTED	=	0x00000008		//! bit 4 of flags

}x86_documentation;

/**
*	instruction sets
*/
typedef enum _x86_instructionSet {

	MMX				=	0x00000000,
	SSE1			=	0x00000000,
	SSE2			=	0x00000000,
	SSE3			=	0x00000000,
	SSSE3			=	0x00000000,
	SSE41			=	0x00000000,
	SSE42			=	0x00000000,
	VMX				=	0x00000000,
	SMX				=	0x00000000

}instructionSet;

/**
*	x86 CPU support
*/
typedef enum _x86_cpu {

	I8086		=	0x00000000,
	I386		=   0x00000001

}x86_cpu;

/**
*	register
*/
typedef struct _x86_register {

	char*           name;
	unsigned int    id;
	x86_operandType flags;

}x86_register;

/**
*	address mode
*/
typedef struct _x86_addressMode {
	int           flags;
	x86_register* base;		//[ebx]
	x86_register* index;	//[edi]
	int           scale;	//[ * 2]
	int           disp;		// + 4
}x86_addressMode;

/**
*	instruction operand
*/
typedef struct _x86_operand {
	x86_operandType flags;	//flags (MEM, REG, IMM, etc)
	x86_register*   reg;	//single reg if REG
	int             imm;	//immediate if IMM
	x86_addressMode mem;	//memory if MEM
}x86_operand;

/**
*	ModRM byte
*/
typedef struct _x86_ModRM {
	int      mode;
	int      reg;
	int      regMem;
}x86_ModRM;

/**
*	SIB byte
*/
typedef struct _x86_SIB {
	int     scale;
	int     base;
	int     index;
}x86_SIB;

/**
*	instruction opcode modifier
*/
typedef enum _x86_opcodeModifier {

	OP_W			=	0x00000010,			//! operand size prefix
	OP_S			=	0x00000020,			//! sign extend bit in opcode
	OP_D			=	0x00000040,			//! direction bit in opcode
	OP_R			=	0x00000080,			//! register opcode field
	OP_TTTN			=	0x00000100,			//! used only with conditional instructions
	OP_SR			=	0x00000200,			//! 2 bit segment register
	OP_SRE			=	0x00000400,			//! 3 bit segment register
	OP_MF			=	0x00000800,			//! FPU only, memory format

	//not part of opcode

	OP_MODRM		=	0x00001000,			//! modRM byte follows
	OP_SIB			=	0x00002000,			//! SIB byte follows
	OP_JMP			=	0x00004000,
	OP_EXT          =   0x00008000,

}x86_opcodeModifier;

/**
*	instruction
*/
typedef struct _x86_instr {
	unsigned int       mnumeric;
	x86_opcodeModifier flags;
	unsigned int       opcode;
	unsigned int       ext;    //opcode extension
	x86_SIB            sib;
	x86_ModRM          modRM;
	unsigned int       prefixCount;
	int                prefix [MAX_PREFIX];
	unsigned int       operandCount;
	x86_operand        operands[MAX_OPERAND];
}x86_instr;

extern X86DECL unsigned int disassemble          (void* instruction, x86_instr* out);
extern X86DECL int          findMnemonicId       (char* name);
extern X86DECL char*        findMnemonicName     (unsigned id);
extern X86DECL x86_instr*   newInstr             ();
extern X86DECL int          setTranslationMode   (int bits);

#endif
