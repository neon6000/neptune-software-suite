/************************************************************************
*                                                                       *
*	error.h																*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

#ifndef X86_ERROR_H_INCLUDED
#define X86_ERROR_H_INCLUDED

/**
*	x86 errors
*/
typedef enum _x86_error {

	X86_INVALID_COMBINATION = 0,
	X86_INVALID_SCALE,
	X86_EXCEED_OPERAND_COUNT,
	X86_INVALID_INSTRUCTION,
	X86_ERR_MAX

}x86_error;

extern
void x86Error (int n, ...);

#endif
