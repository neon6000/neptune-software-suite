/************************************************************************
*                                                                       *
*	mnumeric.h															*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

#ifndef X86_MNUMERIC_H_INCLUDED
#define X86_MNUMERIC_H_INCLUDED

#include "cpu.h"

/*
	This module lists a standard set of constants
	used throughout the code generator
*/

/**
*	mnumeric ID
*/
typedef enum _x86__mnemonicId {
	AAA,
	AAD,
	AAM,
	AAS,
	ADC,
	ADD,
	AMX,
	AND,
	ARPL,
	BOUND,
	CALL,
	CALLF,
	CBW,
	CDQ,
	CLC,
	CLD,
	CLI,
	CMC,
	CMP,
	CMPSB,
	CMPSD,
	CMPSW,
	CWD,
	CWDE,
	DAA,
	DAS,
	DEC,
	DIV,
	ENTER,
	HLT,
	IDIV,
	IMUL,
	IN,
	INC,
	INSB,
	INSW,
	INT,
	INT3,
	INTO,
	IRET,
	JA,
	JAE,
	JB,
	JBE,
	JE,
	JG,
	JGE,
	JL,
	JLE,
	JMP,
	JNE,
	JNL,
	JNO,
	JNP,
	JNS,
	JO,
	JPE,
	JS,
	LAHF,
	LDS,
	LEA,
	LEAVE,
	LES,
	LODSB,
	LODSW,
	LOOP,
	LOOPNZ,
	MOV,
	MOVSB,
	MOVSD,
	MOVSW,
	MUL,
	NEG,
	NOP,
	NOT,
	OR,
	OUT,
	OUTS,
	OUTSB,
	OUTSD,
	OUTSW,
	POP,
	POPA,
	POPF,
	PUSH,
	PUSHA,
	PUSHAD,
	PUSHF,
	RCL,
	RCR,
	RET,
	RETF,
	ROL,
	ROR,
	SAHF,
	SAL,
	SAR,
	SBB,
	SCASB,
	SHL,
	SHR,
	STC,
	STD,
	STI,
	STOSB,
	STOSW,
	SUB,
	TEST,
//	VMCALL,
//	VMLAUNCH,
//	VMRESUME,
//	VMXOFF,
	XCHG,
	XOR,
	MNUMERIC_MAX
}x86_mnumericId;

/**
*	x86 mnumeric
*/
typedef struct _x86_mnemonic {

	x86_mnumericId id;
	unsigned int opcode;
	unsigned int ext;
	unsigned int numParms;
	unsigned int parmFlags [4];
	unsigned int flags;
	unsigned int cpu;

}x86_mnemonic;

extern
X86DECL int x86_findMnemonicId(char* name);

extern
X86DECL char* x86_findMnemonicName(unsigned id);

#endif
