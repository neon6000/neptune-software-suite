/************************************************************************
*                                                                       *
*	x86.c																*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

/*
	This module provides the interface for generating x86
	instructions and instruction level optomizations
*/

/*
If we have: 0x66 0x0f ...whatever in byte stream:

1. We know 0x66 is a prefix byte code. This only makes sense if the byte
following it is either a different prefix byte code or opcode.
2. 0x0f is NEITHER
3. Because 0x0f is the starting opcode for a multibyte instruction,
0x66 MUST be a part of the opcode: 0x66 0x0f being the 2 byte opcode
*/

//http://sandpile.org/ia32/

#include <string.h>
#include <malloc.h>
#include <stdio.h>

#include "cpu.h"
#include "mnemonic.h"
#include "opcode.h"	

/**
http://www.c-jump.com/CIS77/CPU/x86/lecture.html

	x86 machine instruction format:

+--------------------------------------------------------------------------------------------+
| prefix | REX prefix |     OPCode     | Mod R/M |   SIB  | Displacement   | Immediate       |
| 1 byte |   1 byte   | 1,2,or 3 bytes |  1 byte | 1 byte | 1,2, or 4 byte | 1,2, or 4 bytes |
+--------------------------------------------------------------------------------------------+
 optional  64bit only      required         if        if         if              if
                                         required  required   required        required
                                                  32bit only

Mod R/M
  7                           0
+---+---+---+---+---+---+---+---+
|  mod  |    reg    |     rm    |
+---+---+---+---+---+---+---+---+
  2 bits   3 bits       3 bits

Mod R/M is only required when /r or /0 through /7 is specified at
the opcode syntax.

When /0 through /7 is specified at the opcode syntax, use that value as the value
for the MODRM.reg field. MODRM.reg can also be 3 bit opcode extension.

reg=source
rm=destination
some instructions (like OR) contain direction bit that changes this
If an instruction requires only one operand, the unused reg field holds extra opcode bits rather than a register code
This is especially true for floating-point instructions, which use ST(0) as their implied destination.

register ID used in ModRM and SIB bytes

000	AL, AX, EAX
001	CL, CX, ECX
010	DL, DX, EDX
011	BL, BX, EBX
100	AH, SP, ESP
101	CH, BP, EBP
110	DH, SI, ESI
111	BH, DI, EDI

Now, you already know that you use the operand-size prefix to select
either the 16-bit or 32-bit registers. But how do you select the 8-bit
registers? Well, there are special opcodes for that purpose which work
on only 8-bit registers.

Realise, ofcourse, than my description of the Register/Operand byte is
very basic. I have opted to leave out some of the specifics because they
would fill pages and are already freely available at developer.intel.com.

RM byte values - 16 bit mode when MODE byte is 00,01,or 10
-------------------------------
000	BX + SI
001	BX + DI
010	BP + SI
011	BP + DI
100	SI
101	DI
110	BP
111	BX

Mode byte values
---------------------------------
0 [rm] The operand's memory address is in rm
01	[rm + byte]	The operand's memory address is rm + a byte-sized displacement.
10	[rm + word]	The operand's memory address is rm + a word-sized displacement.
11	rm	The operand is rm itself.

-All registers that use 32bit registers have the address override
	size prefix. ModRM byte for 32bit instructions are --different--
	then their 16bit counterparts.

ModRM 32bit http://sandpile.org/ia32/opc_rm32.htm
ModRM 16bit http://sandpile.org/ia32/opc_rm16.htm

SIB
  7                           0
+---+---+---+---+---+---+---+---+
| scale |   index   |    base   |
+---+---+---+---+---+---+---+---+
  2 bits   3 bits       3 bits

The scale is a 2 bit value which represent a scale of either 1 (00),2 (01),4 (10) or 8 (11).
The index is a register, which is denoted by the table above. The base is also a
register.

Example of usage of following structs:

Scale | Index | Base
  1       3      1

Look up the values of base and index in the enums to find
what register they are:

  1 | INDEX_CODE_EBX | BASE_CODE_ECX

This translates to [ecx + ebx] * 1. Remember its always base_reg + index_reg!

OPCode
  7                           0
+---+---+---+---+---+---+---+---+
|  OPCode   | 4 | 3 | 2 | 1 | 0 |
+---+---+---+---+---+---+---+---+
  2 bits   3 bits       3 bits

High bits:
	base opcode
Bits 0-4:
	rest of opcode or,
	some instructions encode information in these bits:

Name  |  Bit number | Combined with | Description
------+-------------+---------------+-------------
r+    |  Low 4 bits |     none      | register code < CHECK: might be 3 bits!
w     |  Bit 0      |     d,s       | operand size (0=16bit, 1=32bit/64bit)
s	  |  Bit 1      |      w        | sign extend bit
d	  |  Bit 1      |      w        | direction bit
tttn  |  Low 4 bits |     none      | used only with conditional instructions
sr    |  Bits 3,4   |     none      | segment register specifier
sre   |  see below  |     none      | segment register specifier
mf    |  Bits 1,2   |     none      | memory format. x86 FPU instructions only
------+-------------+---------------+------------------------------------------
sre: 3 bits, eaither at bit index 0 or 3
*/

extern
x86_register* x86_findRegisterById (unsigned int id, x86_operandType flags);

extern
x86_register* x86_findRegister (char* name);

/**
*	current operation mode
*/
x86_operationMode _operationMode = BIT16;

/**
*	opcode modifier bitmasks
*/
typedef enum _opcodeModifier {

	OPMOD_R = 7,
	OPMOD_W = 1,
	OPMOD_S = 2,
	OPMOD_D = 2

}opcodeModifier;

/**
*	class 1 instruction prefix
*/
typedef enum _prefix1 {

	PREFIX1_LOCK  = 0xf0,
	PREFIX1_REPNE = 0xf2,
	PREFIX1_REPNZ = PREFIX1_REPNE,
	PREFIX1_REP   = 0xf3,
	PREFIX1_REPE  = PREFIX1_REP,
	PREFIX1_REPZ  = PREFIX1_REP

}prefix1;

/**
*	class 2 instruction prefix
*/
typedef enum _prefix2 {

	PREFIX2_CSOVER = 0x2e,  //segment overrides
	PREFIX2_SSOVER = 0x36,
	PREFIX2_DSOVER = 0x3e,
	PREFIX2_ESOVER = 0x26,
	PREFIX2_FSOVER = 0x64,
	PREFIX2_GSOVER = 0x65,
	PREFIX2_NOBRANCH = 0x2e, //branch not taken
	PREFIX2_BRANCH = 0x3e    //branch taken

}prefix2;

/**
*	class 3 instruction prefix
*/
typedef enum _prefix3 {

	PREFIX3_OPSIZE = 0x66   //operand size override

}prefix3;

/**
*	class 4 instruction prefix
*/
typedef enum _prefix4 {

	PREFIX4_ADDSIZE = 0x67  //address size override

}prefix4;

/**
*	REX prefix (64 bit only)
*/
typedef struct _prefixRex {

	unsigned int reserve : 4; //must be 0,1,0,0 binary
	unsigned int w : 1; //0: default op size used, 1: 64 bit op size
	unsigned int r : 1; //extension of MODRM.reg byte
	unsigned int x : 1; //extension of SIB.index byte
	unsigned int b : 1; //extension of MODRM.rm byte

}prefixRex;

/**
*	prefix template
*/
typedef struct _prefixTemplate {

	unsigned int code;
	char* name;

}prefixTemplate;

/**
*	prefix list
*/
prefixTemplate _prefixList [] = {

	{PREFIX1_LOCK, "lock"},
	{PREFIX1_REPNE, "repne"},
	{PREFIX1_REP, "rep"},
	{PREFIX2_CSOVER, "cs"},
	{PREFIX2_SSOVER, "ss"},
	{PREFIX2_DSOVER, "ds"},
	{PREFIX2_ESOVER, "es"},
	{PREFIX2_FSOVER, "fs"},
	{PREFIX2_GSOVER, "gs"}
};

/**
*	ModRM modes idenitifies how rm field works (16 bit mode)
*/
typedef enum _x86_modRMmode16 {

	MODRM_MEM,				//[rm] operand memory address is in rm
	MODRM_MEM_DISP8,		//[rm + byte] operand's memory address is rm + a byte-sized disp
	MODRM_MEM_DISP16,		//[rm + word] operand's memory address is rm + a word-sized disp
							//32 bit mod: this is MODRM_MEM_DISP32
	MODRM_REG				//operand is rm itself

}x86_modRMmode16;

/**
*	16 bit mode ModRM rm flags
*/
typedef enum _x86_modRM16 {

	MODRM16_BX_SI = 0,			// [bx+si]
	MODRM16_BX_DI,
	MODRM16_BP_SI,
	MODRM16_BP_DI,
	MODRM16_SI,
	MODRM16_DI,
	MODRM16_BP,					// mode 0: use IMM, mode 1,2: use BP
	MODRM16_IMM = MODRM16_BP,
	MODRM16_BX

}x86_modRM16;

/**
*	32 bit mode ModRM rm flags
*/
typedef enum _x86_modRM32 {

	MODRM32_EAX = 0,
	MODRM32_ECX,
	MODRM32_EDX,
	MODRM32_EBX,
	MODRM32_SIB,
	MODRM32_EBP,
	MODRM32_IMM = MODRM32_EBP,
	MODRM32_ESI,
	MODRM32_EDI

}x86_modRM32;

/*
==============================================

	Core functions

==============================================
*/

//http://www.swansontec.com/sintel.html
//http://en.wikibooks.org/wiki/X86_Assembly/Machine_Language_Conversion

/**
*	test if value is prefix
*/
unsigned int x86_isPrefix (unsigned int value) {

	switch (value) {
		case PREFIX1_LOCK:
		case PREFIX1_REPNE:
		case PREFIX1_REP:
		case PREFIX2_CSOVER:
		case PREFIX2_SSOVER:
		case PREFIX2_DSOVER:
		case PREFIX2_ESOVER:
		case PREFIX2_FSOVER:
		case PREFIX2_GSOVER:
		case PREFIX3_OPSIZE:
		case PREFIX4_ADDSIZE: {
			return value;
		}
	};
	return 0;
}

/**
*	adds modifier to opcode
*/
unsigned int x86_addOpcodeModifier (unsigned int modifier,
									unsigned int value,
									unsigned int opcode) {

	opcode |= (value & modifier);
	return opcode;
}

/**
*	removes modifiers from opcode
*/
unsigned int x86_removeOpcodeModifiers (unsigned int opcode) {

	return (opcode & 0xf8); // remove low 4 bits
}

/**
*	returns number of bytes mem type uses
*/
unsigned int x86_memSize (unsigned int mem) {

	/*
		Do we even need this??
		only called from readDisp
	*/
	if (mem & MEM) {

		if (mem & MEM8)
			return 1;
		else if (mem & MEM16)
			return 2;
		else if (mem & MEM32)
			return 4;
	}
	return 0;
}

/**
*	returns number of bytes disp type uses
*/
unsigned int x86_dispSize (unsigned int disp) {

	if (disp & DISP) {
		if (disp & DISP8)
			return 1;
		else if (disp & DISP16)
			return 2;
		else if (disp & DISP32)
			return 4;
	}
	return 0;
}

/**
*	returns number of bytes imm type uses
*/
unsigned int x86_immSize (unsigned int imm) {

	if (imm & IMM) {
		if (imm & IMM8)
			return 1;
		else if (imm & IMM16)
			return 2;
		else if (imm & IMM32)
			return 4;
	}
	return 0;
}

/**
*	get number of bytes in use
*/
unsigned int x86_getUseByteCount (unsigned int opcode) {

	unsigned int c=0;

	//! if value is > 2 bytes, c = 2+1+1=4 bytes
	if (opcode > 0xffff)
		c=2;

	//! if value is > 1 byte, c = 1+1=2 bytes
	if (opcode > 0xff)
		c++;

	//! size is always 1 byte
	return c+1;
}

/**
*	returns mnumeric that matches opcode bytes
*/
x86_mnemonic* x86_findMnumericByOpcode (unsigned int opcode) {

	//! linear search, yuck
	unsigned int c = 0;
	for (c=0; c < _x86_opcodeTableSize; c++)
		if (_x86_opcodes[c].opcode == opcode)
			return &_x86_opcodes[c];

	return 0;
}

/**
*	returns mnumeric that closest matches operands and name
*/
x86_mnemonic* x86_findMnumeric(unsigned int mnumericId, unsigned int numOperands,
								unsigned int* operandFlags) {

	x86_mnemonic* mnumeric = 0;
	unsigned int curOpcode = 0;

	//! find base mnumeric ny id
	for (curOpcode = 0; curOpcode < _x86_opcodeTableSize; curOpcode++) {
		mnumeric = &_x86_opcodes [curOpcode];
		if (mnumeric->id == mnumericId)
			break;
	}

	//! search rest of mnumerics with same Id
	for ( ; curOpcode < _x86_opcodeTableSize; curOpcode++) {

		unsigned int c=0;

		//! no more mnumerics with same Id in table, bail out
		mnumeric = &_x86_opcodes [curOpcode];
		if (mnumeric->id != mnumericId)
			break;

		//! FIXME treating AX and AL as same flag

		//! make sure all operand types match
		for (c=0; c<numOperands; c++)
			if (! (mnumeric->parmFlags[c] & (operandFlags[c])))
				break;

		//! this will only be true if all operands matched above
		if (c == numOperands)
			return mnumeric;
	}

	//! no match found
	return 0;
}

/*
=======================================

	x86 specific instruction interface

=======================================
*/

/**
*	add ModRM byte to instruction
*/
void x86_addModRM (x86_instr* instruction, int mod, int reg, int rm) {

	//! init ModRM byte
	instruction->modRM.mode = mod;
	instruction->modRM.reg = reg;
	instruction->modRM.regMem = rm;

	//! set flags
	instruction->flags |= OP_MODRM;
}

/**
*	adds prefix byte to instruction
*/
void x86_addPrefix (x86_instr* instruction, unsigned int prefix) {

	//! only add prefix if limit has not been exceeded
	unsigned int c = instruction->prefixCount;
	if (c < MAX_PREFIX)
		instruction->prefix [c++] = prefix;

	//! add one more prefix
	instruction->prefixCount++;
}

/**
*	adds SIB byte to instruction
*/
void x86_addSIB (x86_instr* instruction,int scale,int index,int base) {

	//! init SIB byte
	instruction->sib.scale = scale;
	instruction->sib.index = index;
	instruction->sib.base = base;

	//! set flags
	instruction->flags |= OP_SIB;
}

/**
*	add imm operand
*/
void x86_addImmOp (x86_operand* op, unsigned int value) {

	//! optomize: get smallest byte count used by imm value
	unsigned int c = x86_getUseByteCount (value);

	//! set flags and imm value
	switch (c) {
		case 1:	{
			op->flags = IMM8;
			break;
		}
		case 2:	{
			op->flags = IMM16;
			break;
		}
		case 4:	{
			op->flags = IMM32;
			break;
		}
	}
	op->imm = value;
}

/**
*	add register operand
*/
void x86_addRegisterOp (x86_operand* op, char* name) {

	x86_register* reg = x86_findRegister (name);
	if (reg) {

		//! set the register in the operand
		op->reg = reg;
		op->flags = reg->flags; //not compatable with other flag types

		//! if this register is an ACC, set its flag
		if (reg->flags & ACC)
			op->flags |= ACC;
	}
}

/**
*	attach opcode modifiers to instruction
*/
void x86_attachOpcodeModifiers (x86_instr* instruction,
	x86_mnemonic* mnumeric) {

	//! operand size (0=16bit, 1=32bit/64bit)
	if (mnumeric->flags & OP_W)
		x86_addOpcodeModifier (OP_W, 1, instruction->opcode);

	//! register code
	if (mnumeric->flags & OP_R) {

		//! get non memory reg operand
//		x86_operand* reg = &instruction->operands[0].u.x86;
		x86_operand* reg = 0;
//		if (reg->flags & MEM)
//			reg = &instruction->operands[1].u.x86;

		if (!reg)
			return;

		instruction->opcode =
			x86_addOpcodeModifier (0xf, reg->reg->id, instruction->opcode);
	}
}

//http://en.wikibooks.org/wiki/X86_Assembly/Machine_Language_Conversion
//http://www.swansontec.com/sintel.html
//http://sandpile.org/ia32/opc_rm16.htm

/**
*	RM register flag to ModRM mode
*/
x86_modRMmode16 x86_ModRMGetMode (unsigned int rmFlags) {

	//! compare with operand address mode
	if (rmFlags & DISP8)
		return MODRM_MEM_DISP8;
	else
	if (rmFlags & DISP16)
		return MODRM_MEM_DISP16;
	else
	if (rmFlags & MEM)
		return MODRM_MEM;

	return MODRM_REG;
}

/**
*	build ModRM byte 16 bit mode
*/
void x86_buildModRM16 (x86_instr* instruction, x86_operand* reg, x86_operand* rm) {

	//! get ModRM mode
	x86_modRMmode16 mode = x86_ModRMGetMode (rm->mem.flags);
	switch (mode) {

		case MODRM_REG: {
			x86_addModRM (instruction, MODRM_REG,(reg) ? (reg->reg->id) : 0,rm->reg->id);
			break;
		}

		//! displacements are added later so we can combine these
		case MODRM_MEM_DISP8:
		case MODRM_MEM_DISP16:
		case MODRM_MEM: {

			//! if there is a base, base or base+index mode
			unsigned int regRM=0;
			if (rm->mem.base) {

				if (!rm->mem.base->flags & REG16) {
					printf ("\n\rModRM base not 16bit");
					return;
				}

				//! if index reg, base+index mode
				if (rm->mem.index) {

					if (!rm->mem.index->flags & REG16) {
						printf ("\n\rModRM index not 16bit");
						return;
					}

					//! bx+si
					if (rm->mem.base->id == 3 && rm->mem.index->id == 6)
						regRM=0;
					//! bx+di
					else if (rm->mem.base->id == 3 && rm->mem.index->id == 7)
						regRM=1;
					//! bp+si
					else if (rm->mem.base->id == 5 && rm->mem.index->id == 6)
						regRM=2;
					//! bp+di
					else if (rm->mem.base->id == 5 && rm->mem.index->id == 7)
						regRM=3;
					else
						printf ("\n\rModRM invalid combination");
				}
				else {

					//! maps register id to ModRM mode
					unsigned int lookupRM [] =
						{0,0,0,MODRM16_BX,0,MODRM16_BP,MODRM16_SI,MODRM16_DI};

					//! get RM value
					regRM = lookupRM [rm->mem.base->id];
					if (!regRM) {
						printf ("\n\rModRM invalid base reg");
						return;
					}

					//! watch for BP register with ModRM mode 0
					if (regRM == MODRM16_BP && mode == MODRM_MEM) {

						//! there is no form of [bp] in ModRM byte mode 0
						//! so we turn it into a [bp+0]

						//! set displacement to 0
						rm->mem.disp = 0;
						rm->mem.flags |= DISP8;

						//! adjust
						mode = MODRM_MEM_DISP8;
						regRM = MODRM16_BP;
					}
				}
			}
			else {

				//! immediate addressing
				regRM = MODRM16_IMM;
			}

			//! add ModRM to instruction
			x86_addModRM (instruction, mode, (reg) ? (reg->reg->id) : 0, regRM);
			break;
		}
	}
}

/**
*	build modRM 32 bit mode (untested)
*/
void x86_buildModRM32 (x86_instr* instruction, x86_operand* reg, x86_operand* rm) {

	//! get ModRM mode
	x86_modRMmode16 mode = x86_ModRMGetMode (rm->mem.flags);
	switch (mode) {

		case MODRM_REG: {
			x86_addModRM (instruction, MODRM_REG,(reg) ? (reg->reg->id) : 0,rm->reg->id);
			break;
		}

		//! combined for now, they share a lot of ground
		case MODRM_MEM_DISP8:
		case MODRM_MEM_DISP16: //DISP16 is DISP32 in 32 bit mode
		case MODRM_MEM: {

			unsigned int regRM=0;

			//! maps register id to rm value
			unsigned int lookupRM[] = {
				MODRM32_EAX,MODRM32_ECX,MODRM32_EDX,MODRM32_EBX,
				0,MODRM32_EBP,MODRM32_ESI,MODRM32_EDI
			};

			//! watch if this instruction uses an SIB byte
			if (instruction->flags & OP_SIB) {
				regRM = MODRM32_SIB;
			}
			else {
				//! lookup regRM value for register
				regRM = lookupRM [rm->mem.base->id];
				if (!regRM) {
					printf ("\n\rModRM32 invalid reg combo");
					return;
				}
			}

			//! add ModRM. Displacement is added later if any
			x86_addModRM (instruction, mode, (reg) ? (reg->reg->id) : 0, regRM);
			break;
		}
	}
}

/**
*	calculate machine instruction size
*/
#if 0
unsigned int x86_instructionSize (instr* instruction) {

	unsigned int size=0;
	unsigned int flags = instruction->flags;
	unsigned int c=0;

	size += instruction->prefixCount;
	size += x86_getUseByteCount (instruction->opcode);

	if (flags & OP_MODRM)
		size++;

//	if ((flags & X86_INSTR_SIB) == X86_INSTR_SIB)
//		size++;
/*
	for (c=0; c<instruction->operandCount;c++)
		if ( (instruction->operands[c].u.x86.flags & IMM) ) {
			size += x86_immSize (instruction->operands[c].u.x86.flags);
		}

	for (c=0; c<instruction->operandCount;c++)
		if ( (instruction->operands[c].u.x86.flags & MEM) )
			size += x86_dispSize (instruction->operands[c].u.x86.mem.flags);
*/
	return size;
}
#endif

/**
*	adds operand to instruction
*/
#if 0
void x86_addOperand (instr* instruction, operand* op) {

	//! add new operand
	unsigned int c = instruction->operandCount;
	if (c<OPERAND_MAX)
		memcpy (&instruction->operands [c++], op, sizeof (operand));

	instruction->operandCount = c;
}
#endif

/**
*	extract SIB byte information
*/
void x86_extractSIB (x86_instr* instr, x86_operand* sourceOp, x86_operand* destOp) {

	//http://www.sandpile.org/x86/opc_sib.htm

	x86_operand* op = 0;
	unsigned int scaleFactors[] = {1,2,4,8};

	if (! (instr->flags & OP_SIB))
		return;

	//If source op is a memory operand
	if ( (sourceOp) && (sourceOp->flags & MEM))
		op=sourceOp;
	else {
		op=destOp;
	}
	if (! (op->flags & MEM))
		return;

	op->mem.base = x86_findRegisterById (instr->sib.base,REG32);
	op->mem.index = x86_findRegisterById (instr->sib.index,REG32);
	op->mem.scale = scaleFactors [instr->sib.scale];
}

/**
*	extract 32 bit ModRM byte into operands
*/
void x86_extractModRM32 (x86_instr* instr, x86_operand* op, x86_operand* rm) {

	op->reg = x86_findRegisterById (instr->modRM.reg,op->flags);
	switch (instr->modRM.mode) {

		case MODRM_REG: {
			x86_register* reg = x86_findRegisterById (instr->modRM.regMem,rm->flags);
			if (reg) {
				//! clear MEM and DISP bits and set register operand
				rm->flags &= ~(MEM|DISP);
				rm->reg = reg;
			}
			break;
		}
		case MODRM_MEM:
		case MODRM_MEM_DISP8:
		case MODRM_MEM_DISP16: {

			switch (instr->modRM.regMem) {

				case MODRM32_EAX: {
					rm->mem.base = x86_findRegister ("eax");
					rm->mem.index = 0;
					break;
				}
  				case MODRM32_ECX: {
					rm->mem.base = x86_findRegister ("ecx");
					rm->mem.index = 0;
					break;
				}
  				case MODRM32_EDX: {
					rm->mem.base = x86_findRegister ("edx");
					rm->mem.index = 0;
					break;
				}
    			case MODRM32_EBX: {
					rm->mem.base = x86_findRegister ("ebx");
					rm->mem.index = 0;
					break;
				}
      			case MODRM32_SIB: {
					//SIB byte is read in later
					instr->flags |= OP_SIB;
					break;
				}
        		case MODRM32_EBP: {
					//[displacement32] mode
					if (instr->modRM.mode==MODRM_MEM) {
						rm->flags |= DISP32;
					}
					else {
						rm->mem.base = x86_findRegister ("ebp");
						rm->mem.index = 0;
					}
					break;
				}
          		case MODRM32_ESI: {
					rm->mem.base = x86_findRegister ("esi");
					rm->mem.index = 0;
					break;
				}
            	case MODRM32_EDI: {
					rm->mem.base = x86_findRegister ("edi");
					rm->mem.index = 0;
					break;
				}
			}
			//displacement is added later
		}
	}
}

/**
*	extract 16 bit ModRM byte into operands
*/
void x86_extractModRM16 (x86_instr* instr, x86_operand* op, x86_operand* rm) {

	op->reg = x86_findRegisterById (instr->modRM.reg,op->flags);
	switch (instr->modRM.mode) {

		case MODRM_REG: {
			x86_register* reg = x86_findRegisterById (instr->modRM.regMem,rm->flags);
			if (reg) {

				//! clear MEM and DISP bits and set register operand
				rm->flags &= ~(MEM|DISP);
				rm->reg = reg;
			}
			break;
		}
		case MODRM_MEM:
		case MODRM_MEM_DISP8:
		case MODRM_MEM_DISP16: {

			//! maps r/m field to register address mode
			unsigned int addressMode[] = {
				MODRM16_BX_SI,MODRM16_BX_DI,MODRM16_BP_SI,MODRM16_BP_DI,
				MODRM16_SI,MODRM16_DI,MODRM16_BP,MODRM16_BX};

			unsigned int mode = addressMode [instr->modRM.regMem];

			if (mode == MODRM16_BX_SI) {
				rm->mem.base = x86_findRegister ("bx");
				rm->mem.index = x86_findRegister ("si");
			}
			else if (mode == MODRM16_BX_DI) {
				rm->mem.base = x86_findRegister ("bx");
				rm->mem.index = x86_findRegister ("di");
			}
			else if (mode == MODRM16_BP_SI) {
				rm->mem.base = x86_findRegister ("bp");
				rm->mem.index = x86_findRegister ("si");
			}
			else if (mode == MODRM16_BP_DI) {
				rm->mem.base = x86_findRegister ("bp");
				rm->mem.index = x86_findRegister ("di");
			}
			else if (mode == MODRM16_SI) {
				rm->mem.base = x86_findRegister ("si");
				rm->mem.index = 0;
			}
			else if (mode == MODRM16_DI) {
				rm->mem.base = x86_findRegister ("di");
				rm->mem.index = 0;
			}
			else if (mode == MODRM16_BP) {
				if (instr->modRM.mode==MODRM_MEM) {
					//! [displacement] mode
					rm->flags |= DISP16;
				}
				else {
					rm->mem.base = x86_findRegister ("bp");
					rm->mem.index = 0;
				}
			}
			else if (mode == MODRM16_BX) {
				rm->mem.base = x86_findRegister ("bx");
				rm->mem.index = 0;
			}

			break;
		}
	}
}

/**
*	extract ModRM byte into operands
*/
void x86_extractModRM (x86_instr* instruction, x86_operand* sourceOp,
					   x86_operand* destinationOp, int operationMode) {

	x86_operand* reg = 0;
	x86_operand* rm = 0;

	if (! instruction->flags & OP_MODRM)
		return;

	//! if source op is a memory operand, reverse rm and reg
	if ( (sourceOp) && (sourceOp->flags & MEM) ) {
		rm = sourceOp;
		reg = destinationOp;
	}
	else{
		//! all other cases, rm=destination and reg=source
		rm = destinationOp;
		reg = sourceOp;
	}

	//! extract ModRM byte
	switch (operationMode) {
		case BIT16: {
			x86_extractModRM16 (instruction, reg, rm);
			return;
		}
		case BIT32: {
			x86_extractModRM32 (instruction, reg, rm);
			return;
		}
	}
}

/**
*	build ModRM byte
*/
#if 0
void x86_buildModRM (instr* instruction,
					 int operationMode) {

	x86_operand* reg = 0;
	x86_operand* rm = 0;
	x86_instr* instr = &instruction->u.x86;

	x86_operand* sourceOp = &instruction->operands[1].u.x86;
	x86_operand* destinationOp = &instruction->operands[0].u.x86;

	//! ModRM requires at least one operand
	if (instruction->operandCount==0)
		return;

	//! if source op is a memory operand, reverse rm and reg
	if ( (sourceOp) && (sourceOp->flags & MEM) ) {
		rm = sourceOp;
		reg = destinationOp;
	}
	else{
		//! all other cases, rm=destination and reg=source
		rm = destinationOp;
		reg = sourceOp;
	}

	//! build ModRM byte
	switch (operationMode) {

		case BIT16: {
			x86_buildModRM16 (&instruction->u.x86, reg, rm);
			break;
		}
		case BIT32: {
			x86_buildModRM32 (&instruction->u.x86, reg, rm);
			break;
		}
		default:
			break;
	}
}
#endif

/**
*	process prefix byte
*/
int x86_processPrefix (x86_operand* out, int prefix) {

	switch (prefix) {

		case PREFIX3_OPSIZE: {
			if (! (out->flags & MEM)) {
				if (_operationMode==BIT32)
					out->flags &= ~(REG32|IMM32|EAX);
				else
					out->flags &= ~(REG16|IMM16|AX);
			}
			return prefix;
		}
		case PREFIX4_ADDSIZE: {
			if (out->flags & MEM) {
				if (_operationMode==BIT32)
					out->flags &= ~(MEM32|DISP32);
				else
					out->flags &= ~(MEM16|DISP16);
			}
			return prefix;
		}
	}
	return 0;
}

/**
*	upload to instruction
*/
void x86_instructionUpload(x86_instr* instruction, x86_mnemonic* mnumeric,
							x86_operand* operands) {

	//! upload operand flags
	unsigned int c = 0;
	for (c=0; c<mnumeric->numParms; c++) {

		int prefix=0;
		operands[c].flags = mnumeric->parmFlags[c];

		{
			unsigned int prefixCount = 0;
			for (prefixCount = 0; prefixCount < instruction->prefixCount; prefixCount++) {

				int rc = x86_processPrefix (&operands[c], instruction->prefix[prefixCount]);
				if(!prefix)
					prefix=rc;
			}
		}

		if (!prefix) {

			//! if 32 bit mode, clear non-default settings
			if (_operationMode==BIT32) {

				int operandSize = instruction->opcode & OPMOD_W;

				//! watch operand size override bit
				if (( instruction->flags & OP_W) && (operandSize == OPMOD_W) )
					operands[c].flags &= ~(REG32|DISP32|IMM32|EAX);
				else
					operands[c].flags &= ~(REG16|DISP16|IMM16|AX);
			}

			//! if 16 bit mode, clear non-default settings
			if (_operationMode==BIT16) {

				int operandSize = instruction->opcode & OPMOD_W;

				//! watch operand size override bit
				if (( instruction->flags & OP_W) && (operandSize == OPMOD_W) )
					operands[c].flags &= ~(REG16|DISP16|IMM16|AX);
				else
					operands[c].flags &= ~(REG32|DISP32|IMM32|EAX);
			}
		}

		//! if instruction uses REG opcode modifier
		if (instruction->flags & OP_R) {

			//! this only applies to the non-memory operand
			if (operands[c].flags & REG) {

				int registerId = instruction->opcode & OPMOD_R;
				operands[c].reg = x86_findRegisterById(registerId, operands[c].flags);
			}
		}

		if (mnumeric->parmFlags[c] & (ESI|SI)) {

			if (mnumeric->parmFlags[c] & SI) {
				if (mnumeric->parmFlags[c] & MEM)
					operands[c].mem.base = x86_findRegister("si");
				else
					operands[c].reg = x86_findRegister("si");
				operands[c].flags |= REG16;
			}
			else
			if (mnumeric->parmFlags[c] & ESI) {
				if (mnumeric->parmFlags[c] & MEM)
					operands[c].mem.base = x86_findRegister("esi");
				else
					operands[c].reg = x86_findRegister("esi");
				operands[c].flags |= REG32;
			}
		}

		if (mnumeric->parmFlags[c] & (EDI|DI)) {

			if (mnumeric->parmFlags[c] & DI) {
				if (mnumeric->parmFlags[c] & MEM)
					operands[c].mem.base = x86_findRegister("di");
				else
					operands[c].reg = x86_findRegister("di");
				operands[c].flags |= REG16;
			}
			else
			if (mnumeric->parmFlags[c] & EDI) {
				if (mnumeric->parmFlags[c] & MEM)
					operands[c].mem.base = x86_findRegister("edi");
				else
					operands[c].reg = x86_findRegister("edi");
				operands[c].flags |= REG32;
			}
		}

		if (mnumeric->parmFlags[c] & SEG) {

			if (mnumeric->parmFlags[c] & CS)
				operands[c].reg = x86_findRegister ("cs");
			if (mnumeric->parmFlags[c] & DS)
				operands[c].reg = x86_findRegister ("ds");
			if (mnumeric->parmFlags[c] & ES)
				operands[c].reg = x86_findRegister ("es");
			if (mnumeric->parmFlags[c] & GS)
				operands[c].reg = x86_findRegister ("gs");
			if (mnumeric->parmFlags[c] & FS)
				operands[c].reg = x86_findRegister ("fs");

			operands[c].flags |= REG16;
		}

		if (mnumeric->parmFlags[c] & (EDX|DX)) {

			if (mnumeric->parmFlags[c] & DX) {
				operands[c].reg = x86_findRegister("dx");
				operands[c].flags |= REG16;
			}
			else
			if (mnumeric->parmFlags[c] & EDX) {
				operands[c].reg = x86_findRegister("edx");
				operands[c].flags |= REG32;
			}
		}

		if (mnumeric->parmFlags[c] & (EAX|AX|AL)) {

			if (mnumeric->parmFlags[c] & AX) {
				operands[c].reg = x86_findRegister("ax");
				operands[c].flags |= REG16;
			}
			else
			if (mnumeric->parmFlags[c] & AL) {
				operands[c].reg = x86_findRegister("al");
				operands[c].flags |= REG8;
			}
			else
			if (mnumeric->parmFlags[c] & EAX) {
				operands[c].reg = x86_findRegister("eax");
				operands[c].flags |= REG32;
			}
		}
	}
}

#if 0
/**
*	build instruction, ModRM, SIB bytes, etc
*/
void x86_buildInstruction (instr* instruction) {

	x86_mnumeric*  opcode;
	unsigned int   operandFlags [4] = {0,0,0,0};
	unsigned int   c=0;

	//! copy over operand flags
	for (c=0; c<instruction->operandCount; c++)
		operandFlags[c] = instruction->operands[c].u.x86.flags;

	//! find mnumeric
	opcode = x86_findMnumeric (instruction->mnumeric, c, operandFlags);
	if (!opcode) {
//		x86Error (X86_INVALID_COMBINATION);
		return;
	}

	//! set instruction opcode and flags
	instruction->u.x86.opcode = opcode->opcode;
	instruction->u.x86.flags = opcode->flags;

	//! attach opcode modifiers
	x86_attachOpcodeModifiers (&instruction->u.x86, opcode);

	/**
	*	set instruction flags
	*/

//	if (opcode->flags & OP_MODRM)
//		x86_buildModRM (instruction, BIT16);
}
#endif

#if 0
/**
*	create instruction
*/
instr* x86_createInstruction (instr* instruction,
							  char* mnumeric,
							operand* op, unsigned int opCount) {

	unsigned int mnumericId = 0;
	unsigned int c=0;

	memset (instruction, 0, sizeof (instr));

	//! attempt to get mnumeric id
	mnumericId = x86_findMnumericId (mnumeric);
	if (mnumericId == -1) {
//		x86Error (X86_INVALID_INSTRUCTION, mnumeric);
		return 0;
	}

	//! add operands
	for (c=0; c<opCount;c++)
		x86_addOperand (instruction, &op[c]);

	instruction->mnumeric=mnumericId;
	return instruction;
}
#endif

/*
======================================

	Code Generator

======================================
*/

/**
*	read immediate bytes into structure
*/
unsigned int x86_readImm (void* instruction, x86_instr* out,
						  unsigned int operandCount, x86_operand* operands) {

	unsigned int c = 0;
	x86_operand* rm=0;

	for (c=0; c<operandCount; c++) {
		if (operands[c].flags & IMM) {
			rm = &operands[c];
			break;
		}
	}
	if (!rm)
		return 0;

	c = x86_immSize (rm->flags);
	memcpy (&rm->imm, instruction, c);
	return c;
}

/**
*	write immediate bytes
*/
unsigned int x86_writeImm (x86_instr* instruction, void* out ) {

	return 0;
}

#if 0
unsigned int x86_writeImm (instr* instruction, void* out ) {

	x86_operand* op = &instruction->operands[0].u.x86;
	unsigned int copied=0;

	if (!op)
		return 0;

	if (! (op->flags & IMM) )
		op = &instruction->operands[1].u.x86;

	if (! (op->flags & IMM) )
		return 0;

	copied = x86_immSize (op->flags);
	memcpy (out, &op->imm, copied);
	return copied;
}
#endif

/**
*	read displacement bytes into structure
*/
unsigned int x86_readDisp (void* instruction, x86_instr* out,
						   x86_operand* sourceOp, x86_operand* destinationOp) {

	unsigned int c=0;
	x86_operand* rm=0;

	rm = destinationOp;
	if ( ! (rm->flags & (DISP | MEM)) )
		rm = sourceOp;

	//! get displacement size from operand
	if (rm->flags & DISP) {
		c = x86_dispSize (rm->flags);
		if (c==1)
			rm->mem.disp = *(unsigned char*)instruction;
		else
		if (c==2) {
			rm->mem.disp = *(unsigned short*)instruction;
		}
		else
		if (c==4) {
			rm->mem.disp = *(unsigned int*)instruction;
		}
	}
	else {
		//! get displacement size from ModRM
		x86_ModRM* modRM = &out->modRM;
		unsigned int flags=0;
		switch (modRM->mode) {

			case MODRM_MEM_DISP8: {			//[base+index+byte]
				c = 1;
				flags |= DISP8;
				break;
			}
			case MODRM_MEM_DISP16: {		//[base+index+word]
				c = 2;
				flags |= DISP16;
				break;
			}
		}

		//! clear displacement bits and set them
		rm->flags &= ~(DISP);
		rm->flags |= flags;
		memcpy (&rm->mem.disp, instruction, c);
	}
	return c;
}

/**
*	write displacement bytes
*/
#if 0
unsigned int x86_writeDisp (instr* instruction, void* out ) {
	unsigned int count=0;
	x86_operand* rm=0;

	x86_operand* sourceOp = &instruction->operands[1].u.x86;
	x86_operand* destinationOp = &instruction->operands[0].u.x86;

	if (instruction->operandCount == 0)
		return 0;

	rm = destinationOp;
	if (! (rm->flags & MEM) )
		rm = sourceOp;

	if (!rm)
		return 0;

	//! running when should not
	if (rm->mem.flags & DISP) {
		count = x86_dispSize (rm->mem.flags);
		memcpy (out, &rm->mem.disp, count);
	}
	return count;
}
#endif

unsigned int x86_writeDisp (x86_instr* instruction, void* out ) {

	return 0;
}

/**
*	write SIB byte
*/
unsigned int x86_writeSIB (x86_instr* instruction, void* out) {

	unsigned int scale = instruction->sib.scale;
	unsigned int index = instruction->sib.index;
	unsigned int base = instruction->sib.base;
	unsigned char* SIB = (unsigned char*)out;

	return 1;
}

/**
*	read SIB byte
*/
unsigned int x86_readSIB (void* instruction, x86_instr* out) {

	unsigned char* byte = (unsigned char*)instruction;

	if (out->flags & OP_SIB) {
		out->sib.scale =  ((*byte) & 0xc0) >> 6;
		out->sib.index =   ((*byte) & 0x38) >> 3;
		out->sib.base = (*byte) & 0x7;
		return 1;
	}
	return 0;
}

/**
*	read ModRM byte into structure
*/
unsigned int x86_readModRM (void* instruction, x86_instr* out) {

	unsigned char* byte = (unsigned char*)instruction;
	if (out->flags & OP_MODRM) {

		out->modRM.mode =  ((*byte) & 0xc0) >> 6;
		out->modRM.reg =   ((*byte) & 0x38) >> 3;
		out->modRM.regMem = (*byte) & 0x7;
		return 1;
	}
	return 0;
}

/**
*	write ModRM byte from structure
*/
unsigned int x86_writeModRM (x86_instr* instruction, void* out) {

	unsigned int mod = instruction->modRM.mode;
	unsigned int reg = instruction->modRM.reg;
	unsigned int rm = instruction->modRM.regMem;
	unsigned char* modRM = (unsigned char*)out;

	if (instruction->flags & OP_MODRM) {
		*modRM = ( (mod & 0x3) << 6) | ( (reg & 0x7) << 3) | (rm & 0x7);
		return 1;
	}

	return 0;
}

/**
*	read opcode bytes into structure
*/
unsigned int x86_readOpcode (void* instruction, x86_instr* out) {

	unsigned char* opcode = (unsigned char*)instruction;
	out->opcode = (int) *opcode;

	//If opcode begins with 0x0f, this is a multibyte opcode
	if (_operationMode==BIT32) {
		if ( (*opcode) == 0x0f) {
			out->opcode <<= 8;
			out->opcode |= (int) opcode[1];
			return 2;
		}
	}
	return 1;
}

/**
*	write opcode bytes from structure
*/
unsigned int x86_writeOpcode (x86_instr* instruction, void* out) {

	unsigned int opcode = instruction->opcode;
	unsigned int c = x86_getUseByteCount (opcode);
	memcpy (out, &opcode, c);
	return c;
}

/**
*	read prefix bytes into structure
*/
unsigned int x86_readPrefix (void* instruction, x86_instr* out) {

	unsigned char* currentByte = (unsigned char*)instruction;
	unsigned int c = 0;

	//! start reading prefix bytes
	out->prefixCount=0;
	for(c = 0; c < 4; c++) {

		//! we are done when we find first non-prefix byte
		unsigned int value = (unsigned int) *currentByte;
		if (!x86_isPrefix (value))
			return c;

		//! if next byte is 0x0f prefix, break now. This prefix byte
		//! is a part of the instruction opcode
		if (currentByte[c+1]==0x0f)
			return c;

		//! store prefix
		out->prefix [out->prefixCount] = value;	
		out->prefixCount++;
		currentByte++;
	}
	return c;
}

/**
*	write prefix bytes from structure
*/
unsigned int x86_writePrefix (x86_instr* instruction, void* out) {

	unsigned int c = 0;
	unsigned char* pData = (unsigned char*)out;

	for (c = 0; c < instruction->prefixCount; c++)
		pData [c] = instruction->prefix [c];

	return c;
}

#if 0
US
0x0000070E      0xFFD5                  inc ch
0x00000710      0x83C408                and ah, 0x8

0x00000713      0x8906C746              mov [esi+0x46c7], eax

0x00000717      0x0400                  add al, 0x0
0x00000719      0x0000                  add [eax], al
0x0000071B      0x008B0D30              add [ebx+0x300d], cl
0x0000071F      0x44                    inc esp
0x00000720      0x40                    inc eax
0x00000721      0x00893534              add [ecx+0x3435], cl
0x00000725      0x44                    inc esp
0x00000726      0x40                    inc eax
0x00000727      0x0085C90F              add [ebp+0xfc9], al
0x0000072B      0x84800100              test [eax+0x1], al
0x0000072F      0x0085F60F              add [ebp+0xff6], al
0x00000733      0x847801                test [eax+0x1], bh
0x00000736      0x0000                  add [eax], al
0x00000738      0x83790400              and [ecx+0x4], 0x0
0x0000073C      0x8B3D                  mov edi, []
0x0000073E      0x0430                  add al, 0x30
0x00000740      0x40                    inc eax
0x00000741      0x00751D                add [ebp+0x1d], dh
0x00000744      0x8B4108                mov eax, [ecx+0x8]
0x00000747      0x85C0                  test eax, eax

NASM

0000070E  FFD5              call ebp

00000710  83C408            add esp,byte +0x8
00000713  8906              mov [esi],eax
00000715  C7460400000000    mov dword [esi+0x4],0x0
0000071C  8B0D30444000      mov ecx,[dword 0x404430]
00000722  893534444000      mov [dword 0x404434],esi
00000728  85C9              test ecx,ecx

0000072A  0F8480010000      jz dword 0x8b0


00000730  85F6              test esi,esi
00000732  0F8478010000      jz dword 0x8b0
00000738  83790400          cmp dword [ecx+0x4],byte +0x0
0000073C  8B3D04304000      mov edi,[dword 0x403004]
00000742  751D              jnz 0x761
00000744  8B4108            mov eax,[ecx+0x8]
00000747  85C0              test eax,eax
00000749  7416              jz 0x761
0000074B  6898314000        push dword 0x403198
00000750  50                push eax
00000751  FFD7              call edi
00000753  8B3534444000      mov esi,[dword 0x404434]
00000759  8B0D30444000      mov ecx,[dword 0x404430]
#endif

/**
*	disassemble instruction
*/
unsigned int disassemble (void* instruction, x86_instr* out) {

	unsigned char* machineCode = (unsigned char*)instruction;
	x86_mnemonic* mnemonic = 0;
	unsigned int c = 0;

	//! read prefix and opcode bytes
	c += x86_readPrefix (&machineCode[c], out);
	c += x86_readOpcode (&machineCode[c], out);

	//! search for instruction
	mnemonic = x86_findMnumericByOpcode(out->opcode);
	if (!mnemonic) {

		//! remove possible opcode modifiers and try search again
		unsigned int opcode = x86_removeOpcodeModifiers (machineCode[c-1]);
		mnemonic = x86_findMnumericByOpcode(opcode);
		if (!mnemonic) {
			out->mnumeric=MNUMERIC_MAX;
			return -1;
		}
	}

	out->mnumeric = mnemonic->id;
	out->flags = mnemonic->flags;
	out->operandCount = mnemonic->numParms;

	//! upload mnumeric data
	x86_instructionUpload(out, mnemonic, out->operands);

	//! read rest of instruction
	if (out->flags & OP_MODRM) {
		c += x86_readModRM (&machineCode[c], out);
		x86_extractModRM  (out, &out->operands[0], &out->operands[1], _operationMode);
	}

	if (_operationMode == BIT32) {
		if (out->flags & OP_SIB) {
			c += x86_readSIB (&machineCode[c], out);
			x86_extractSIB (out,&out->operands[0], &out->operands[1]);
		}
	}

	c += x86_readDisp (&machineCode[c], out, &out->operands[0], &out->operands[1]);
	c += x86_readImm(&machineCode[c], out, mnemonic->numParms, &out->operands[0]);
	return c;
}

/**
*	assemble instruction
*/
unsigned int assemble (x86_instr* instruction, void* out) {

	unsigned char* machineCode = 0;
	unsigned int c = 0;

	//! write machine instruction
	machineCode = (unsigned char*) out;
	c += x86_writePrefix (instruction, &machineCode[c]);
	c += x86_writeOpcode (instruction, &machineCode[c]);
	c += x86_writeModRM (instruction, &machineCode[c]);
//	machineCode += x86_writeSIB (instruction, machineCode);
	c += x86_writeDisp (instruction, &machineCode[c]);
	c += x86_writeImm (instruction, &machineCode[c]);

	return c;
}

x86_instr* newInstr () {

	x86_instr* ins = (x86_instr*)malloc (sizeof (x86_instr));
	memset (ins, 0, sizeof (x86_instr));
	return ins;
}

/**
*	set translation mode
*/
int setTranslationMode (int bits) {

	switch (bits) {
		case 16: {
			_operationMode = BIT16;
			break;
		}
		case 32: {
			_operationMode = BIT32;
			break;
		}
		default:
			return 0;
	}
	return _operationMode;
}

void displayOperand(x86_operand* dest) {

	//! immediate operand
	if (dest->flags & IMM)
		printf("0x%x", dest->imm);

	//! memory operand
	else if (dest->flags & (DISP | MEM)) {

		printf("[");
		if (dest->flags & MEM) {
			if (dest->mem.base)
				printf("%s", dest->mem.base->name);
			if (dest->mem.index)
				printf("+%s", dest->mem.index->name);
			if (dest->mem.scale != 0)
				printf("*%i", dest->mem.scale);
			if (dest->flags & DISP)
			if (dest->mem.base)
				printf("+");
		}
		if (dest->flags & DISP) {
			printf("0x%x", dest->mem.disp);
		}
		printf("]");
	}

	//! register operand
	else if (dest->flags & (REG | SEG | AL | AX | EAX)) {
		x86_register* reg = dest->reg;
		if (reg)
			printf("%s", reg->name);
	}
}

/**
*	display instruction
*/
void displayInstruction(x86_instr* in) {

	unsigned int c = 0;
	unsigned int i = 0;
	char* insName = 0;

	if (!in)
		return;

	insName = findMnemonicName(in->mnumeric);
	if (!insName)
		return;

	//! instruction name
	printf("%s ", insName);

	if (in->operandCount>0)
		displayOperand(&in->operands[0]);

	for (c = 1; c<in->operandCount; c++) {
		printf(", ");
		displayOperand(&in->operands[c]);
	}
}
