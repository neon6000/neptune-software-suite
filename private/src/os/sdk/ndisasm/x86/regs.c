/************************************************************************
*                                                                       *
*	regs.c																*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

#include <string.h>
#include "cpu.h"

/**
*	register table
*/
x86_register _registers[] = {

	{"al",  0, REG8 | AL},
	{"ax",  0, REG16 | AX },
	{"eax", 0, REG32 | EAX},
	{"cl",  1, REG8},
	{"cx",  1, REG16 | CX},
	{"ecx", 1, REG32 | ECX},
	{"dl",  2, REG8},
	{"dx",  2, REG16},
	{"edx", 2, REG32},
	{"bl",  3, REG8 | BASEINDEX },
	{"bx",  3, REG16 | BASEINDEX },
	{"ebx", 3, REG32 | BASEINDEX},
	{"ah",  4, REG8 | AH},
	{"sp",  4, REG16 | BASEINDEX},
	{"esp", 4, REG32 | BASEINDEX},
	{"ch",  5, REG8},
	{"bp",  5, REG16 | BASEINDEX},
	{"ebp", 5, REG32 | BASEINDEX},
	{"dh",  6, REG8},
	{"si",  6, REG16 | BASEINDEX},
	{"esi", 6, REG32 | BASEINDEX},
	{"bh",  7, REG8},
	{"di",  7, REG16 | BASEINDEX},
	{"edi", 7, REG32 | BASEINDEX},

	{"es",  0, REG16 | SEG | ES},
	{"cs",  1, REG16 | SEG | CS},
	{"ss",  2, REG16 | SEG | SS},
	{"ds",  3, REG16 | SEG | DS},
	{"fs",  4, REG16 | SEG | FS},
	{"gs",  5, REG16 | SEG | GS}
};
unsigned int _registerCount = sizeof (_registers) / sizeof (x86_register);

/**
*	look up register by id
*/
x86_register* x86_findRegisterById (unsigned int id, x86_operandType flags) {

	unsigned int c=0;
	for (c=0; c<_registerCount; c++)
		if (_registers[c].id == id && (_registers[c].flags & flags))
			return &_registers [c];

	return 0;
}

/**
*	look up register by name
*/
x86_register* x86_findRegister (char* name) {

	unsigned int c=0;
	for (c=0; c<_registerCount; c++)
		if (strcmp (_registers[c].name, name) == 0)
			return &_registers [c];

	return 0;
}
