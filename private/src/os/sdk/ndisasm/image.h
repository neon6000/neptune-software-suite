/************************************************************************
*                                                                       *
*	image.h																*
*                                                                       *
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.	*
*                                                                       *
************************************************************************/

#ifndef IMAGE_H_INCLUDED
#define IMAGE_H_INCLUDED

#include "list.h"

/**
*	image file
*/
typedef struct _imageFile {

	char name[32];
	unsigned int size;
	unsigned char* buffer;
	unsigned char* currentPos;

}imageFile;
typedef list imageFileList;

extern void        freeImageFile     (imageFile* image);
extern imageFile*  allocateImageFile (char* name);

#endif
