
#include <stdio.h>
#include <Windows.h>
/* note we have to be careful about including defs.h here due to handle_t conflict. */

HANDLE DbgOpenPipe() {

	return CreateFile("\\\\.\\pipe\\ndbg",
		GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_WRITE_THROUGH | FILE_FLAG_OVERLAPPED, NULL);
}

HANDLE DbgCreatePipe(char* name) {

#if 0

	return DbgOpenPipe();

#else

	HANDLE pipe = CreateNamedPipe("\\\\.\\pipe\\ndbg",
		PIPE_ACCESS_DUPLEX | FILE_FLAG_WRITE_THROUGH | FILE_FLAG_OVERLAPPED,
		PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT | PIPE_ACCEPT_REMOTE_CLIENTS,
		PIPE_UNLIMITED_INSTANCES,
		255, 255, 0, NULL);

	ConnectNamedPipe(pipe, NULL);

	return pipe;

#endif
}

char DbgReadPipe(HANDLE pipe) {

	DWORD bytesAvailable;
	DWORD numWritten;
	while (PeekNamedPipe(pipe, NULL, 0, NULL, &bytesAvailable, NULL)) {
		char c;
		ReadFile(pipe, &c, 1, &numWritten, NULL);
		return c;
	}
	return 0;
}

size_t DbgWritePipe(HANDLE pipe, char in) {

	DWORD numWritten;
	WriteFile(pipe, &in, 1, &numWritten, NULL);
	return numWritten;
}
