/********************************************

	session.c
		- Debug session management

********************************************/

/*
	This component provides debug session facilities.
*/

#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include "list.h"

/* current session */
static dbgSession*	_currentSession = 0;

INLINE char* DbgSessionGetProcessName (dbgSession* session) {
	return session->process.name;
}

INLINE dbgPtid* DbgSessionGetPtid (dbgSession* session) {
	return &session->process.id;
}

void DbgRegisterEventProc (dbgSession* session, DbgSessionEventProc event, DbgSessionSendEventProc send) {
	if (session) {
//		DbgMutexLock (&session->mutex);
		session->event = event;
		session->send = send;
//		DbgMutexUnlock (&session->mutex);
	}
	else
		printf ("EVENTPROC SESSION INVALID.\n\r");
}

dbgSession* DbgGetCurrentSession (void) {
	return _currentSession;
}

void DbgSetCurrentSession (dbgSession* session) {
	_currentSession = session;
}

dbgSession* DbgSessionNew (char* command, pid_t pid, tid_t tid, handle_t process, handle_t thread) {
	dbgSession* session = (dbgSession*) malloc (sizeof (dbgSession));   
	if (!session)
		return 0;
	session->process.name = command;
	session->process.id.pid = pid;
	session->process.id.tid = tid;
	session->process.process = process;
	session->process.thread = thread;
	session->state = DBG_STATE_CONTINUE;
	session->event = NULL;
	session->send = NULL;
	listInit (&session->process.libraryList);
	listInit (&session->process.threadList);
	listInit (&session->process.sourceFileList);
	listInit (&session->process.breakPointList);
	listInit (&session->process.watchPointList);
	return session;
}

void DbgSessionDeleteProc (dbgSession* session) {
	listNode* current;
	size_t    c;

	listFreeAll(&session->process.libraryList);
	listFreeAll(&session->process.threadList);
	current = session->process.sourceFileList.first;
	for (c=0; c<session->process.sourceFileList.count; c++) {
		dbgSourceFile* sourceFile;

		sourceFile = (dbgSourceFile*) current->data;
		free (sourceFile->name);
		sourceFile->name = 0;

		listFreeAll (&sourceFile->sourceLineList);
		current = current->next;
	}
	listFreeAll(&session->process.sourceFileList);
}

void DbgSessionDelete (dbgSession* session) {
	if (!session)
		return;
	if (session->process.id.pid)
		DebugActiveProcessStop (session->process.id.pid);
	DbgSessionDeleteProc (session);
}

/**
*	Session entry point
*	\param command Command line
*	\ret Error code
*/
int __stdcall DbgSessionThreadEntry (char* command) {
	dbgSession*         session;
	DEBUG_EVENT         dbgEvent;
	PROCESS_INFORMATION process;
	STARTUPINFO         startup;

	/* start process */
	memset (&process, 0, sizeof(PROCESS_INFORMATION));
	memset (&startup, 0, sizeof(STARTUPINFO));

	if (!CreateProcess (0,command,0,0,FALSE,
		CREATE_SUSPENDED|CREATE_NEW_CONSOLE|DEBUG_PROCESS,
		0,0,&startup,&process)) {
		fprintf(stderr, "Error: Unable to create process.\n\r");
	}

	/* create debug session */
	session = DbgSessionNew (command,process.dwProcessId,process.dwThreadId,(handle_t)process.hProcess,(handle_t)process.hThread);
	if (!session) {
		fprintf(stderr, "Error: Unable to create session.\n\r");
		CloseHandle (process.hThread);
		CloseHandle (process.hProcess);
		return EXIT_FAILURE;
	}

	/* attempt to enumerate symbol information */
	if (!DbgSymbolEnumerate (session))
		DbgDisplayError ("*** Unable to load symbols");
	else
		DbgDisplayMessage ("Symbols loaded");

	/* this is the current session */
	DbgSetCurrentSession (session);

	/* session thread event loop */
	while (TRUE) {

		if (session->state == DBG_STATE_QUIT)
			break;

		if (session->state == DBG_STATE_CONTINUE) {

			/* wait for debug event from process */
	//		if (WaitForDebugEvent (&dbgEvent, INFINITE)) {
			if (WaitForDebugEvent (&dbgEvent, 1000)) {

				/* process event */
				session->state = DbgSessionProcessEvent (session, &dbgEvent);

				/* continue execution */
				ContinueDebugEvent (dbgEvent.dwProcessId,dbgEvent.dwThreadId, DBG_CONTINUE);
			}
		}
	}

	/* free session */
	DbgSessionDelete (session);

	/* free symbols */
	DbgSymbolFree (session);

	/* clear current session, release resources and return */
	if (DbgGetCurrentSession() == session)
		DbgSetCurrentSession (0);

	free (session);
	return EXIT_SUCCESS;
}

/**
*	Create session
*	\param path Command line
*/
void DbgCreateSession (char* path) {   
	if (DbgGetCurrentSession()) {
		printf ("\nAttempt to create more then one debug session");
		return;
	}
	/* create new thread for debug session */
	CreateThread (0, 0, (LPTHREAD_START_ROUTINE)DbgSessionThreadEntry, path, 0,0);
}

/* flush instruction cache. Should this be a SESSION message? */
void DbgFlushInstructionCache (dbgSession* in, vaddr_t addr, uint32_t size) {

	FlushInstructionCache(in->process.process,addr,size);
}
