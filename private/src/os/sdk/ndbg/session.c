/********************************************

session.c
- Debug session management

********************************************/

/*
This component provides debug session facilities.
*/

#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include "list.h"

/* current session */
static dbgSession*	_currentSession = 0;

INLINE char* DbgSessionGetProcessName(dbgSession* session) {
	return session->process.name;
}

INLINE dbgPtid* DbgSessionGetPtid(dbgSession* session) {
	return &session->process.id;
}

void DbgRegisterEventProc(IN dbgSession* session, IN DbgSessionEventProc event) {
	if (session)
		session->event = event;
}

dbgSession* DbgGetCurrentSession(void) {
	return _currentSession;
}

void DbgSetCurrentSession(dbgSession* session) {
	_currentSession = session;
}

dbgSession* DbgSessionNew(char* command, pid_t pid, tid_t tid, handle_t process, handle_t thread) {
	dbgSession* session = (dbgSession*)malloc(sizeof (dbgSession));
	if (!session)
		return 0;
	session->process.name = command;
	session->process.id.pid = pid;
	session->process.id.tid = tid;
	session->process.process = process;
	session->process.thread = thread;
	session->state = DBG_STATE_CONTINUE;
	session->event = NULL;
	session->request = NULL;
	session->attach = NULL;
	listInit(&session->process.libraryList);
	listInit(&session->process.threadList);
	listInit(&session->process.sourceFileList);
	listInit(&session->process.breakPointList);
	listInit(&session->process.watchPointList);
	return session;
}

void DbgSessionDeleteProc(dbgSession* session) {
	listNode* current;
	size_t    c;

	listFreeAll(&session->process.libraryList);
	listFreeAll(&session->process.threadList);
	current = session->process.sourceFileList.first;
	for (c = 0; c<session->process.sourceFileList.count; c++) {
		dbgSourceFile* sourceFile;

		sourceFile = (dbgSourceFile*)current->data;
		free(sourceFile->name);
		sourceFile->name = 0;

		listFreeAll(&sourceFile->sourceLineList);
		current = current->next;
	}
	listFreeAll(&session->process.sourceFileList);
}

void DbgSessionDelete(dbgSession* session) {
	if (!session)
		return;
//	if (session->process.id.pid)
//		DebugActiveProcessStop(session->process.id.pid);
	DbgSessionDeleteProc(session);
}


size_t DbgProcessRequest(IN dbgProcessReq request, IN dbgSession* session,
	IN OPT void* addr, IN OUT OPT void* data, IN OPT size_t size) {

	return session->request(request, session, addr, data, size);
}

/* flush instruction cache. Should this be a SESSION message? */
//void DbgFlushInstructionCache(dbgSession* in, vaddr_t addr, uint32_t size) {
//	FlushInstructionCache(in->process.process, addr, size);
//}
