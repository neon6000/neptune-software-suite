/********************************************

	symbol.c
		- Symbol tables

********************************************/

/*
	This component implements the symbol table API.
*/

#include "defs.h"

/*
	NDBG Symbol services
*/

BOOL DbgSymbolFromName (IN dbgSession* in, IN const char* name, OUT dbgSymbol* symbol) {

	return DbgSymbolFromNamePDB (&in->process, name, symbol);
}

BOOL DbgSymbolFromAddress (IN dbgSession* in, IN vaddr_t address, OUT dbgSymbol* symbol) {

	return DbgSymbolFromAddressPDB (address, symbol);
}

BOOL DbgSymbolLoad (IN dbgSession* in) {

	vaddr_t modbase;
	BOOL result;

	PdbInitialize (&in->process);
	DbgDisplayMessage("Loading symbols for : %s", in->process.name);

	modbase = (vaddr_t) PdbLoadSymbolTable (&in->process);
//	in->process.base = (vaddr_t) modbase;

	if (modbase) {
		result = DbgLoadSymbolsPDB (&in->process);
		if (result)
			PdbDumpSymbolInfo(&in->process);
		return result;
	}
	return FALSE;
}

BOOL DbgSymbolFree (IN dbgSession* in) {
	if (!in)
		return FALSE;
	PdbCleanup (&in->process);
	return TRUE;
}
