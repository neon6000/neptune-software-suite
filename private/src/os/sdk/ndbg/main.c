/********************************************

	main.c
		- Entry point

********************************************/

/*
	This component provides program startup
	and shutdown facilities.
*/

#define _CRTDBG_MAP_ALLOC

#ifdef _MSC_VER
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <crtdbg.h>

/* win32 specific? */
#include <fcntl.h>

#include "defs.h"

CRITICAL_SECTION _dbgDisplayMutex;

void DbgDisplayDebugOut (const char* msg, ...) {
	va_list args;

	EnterCriticalSection (&_dbgDisplayMutex);

#ifdef _WIN32
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN|FOREGROUND_INTENSITY);
#endif
	printf ("\n("NDBG_NAME") ");
#ifdef _WIN32
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_INTENSITY);
#endif
	if(msg) {
		va_start (args, msg);
		vprintf (msg, args);
		va_end (args);
	}
#ifdef _WIN32
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);
#endif

	LeaveCriticalSection (&_dbgDisplayMutex);
}

void DbgDisplayError (const char* msg, ...) {
	va_list args;

	EnterCriticalSection (&_dbgDisplayMutex);

#ifdef _WIN32
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN|FOREGROUND_INTENSITY);
#endif
	printf ("\n\r("NDBG_NAME") ");
#ifdef _WIN32
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED|FOREGROUND_INTENSITY);
#endif
	if(msg) {
		va_start (args, msg);
		vprintf (msg, args);
		va_end (args);
	}
#ifdef _WIN32
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);
#endif

	LeaveCriticalSection (&_dbgDisplayMutex);
}

void DbgDisplayMessage (const char* msg, ...) {
	va_list args;

	EnterCriticalSection (&_dbgDisplayMutex);

#ifdef _WIN32
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN|FOREGROUND_INTENSITY);
#endif
	printf ("\n("NDBG_NAME") ");
#ifdef _WIN32
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_GREEN);
#endif
	if(msg) {
		va_start (args, msg);
		vprintf (msg, args);
		va_end (args);
	}
#ifdef _WIN32
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);
#endif

	LeaveCriticalSection (&_dbgDisplayMutex);
}

void DbgInfo (void) {
	printf (NDBG_PRODUCT" "NDBG_VERSION);
	printf ("\n"NDBG_COPY" "NDBG_VENDER);
}


unsigned long KeProcessRequest(IN dbgProcessReq request, IN dbgSession* session,
	IN OPT void* addr, IN OUT OPT void* data, IN OPT size_t size);
dbgSessionState KeSessionProcessEvent(dbgSession* session);

extern void KeSessionAttachProcess(char* path);

void DbgCreateSession(char* name) {

	/* here, we can create either a win32 session or ndbg session. */

	dbgSession* session = DbgSessionNew(name, 0, 0, NULL, NULL);

	// we need to get the size and base of the file for symbol loading:

	{
		FILE* file = fopen("A:/nboot.efi", "rb");
		IMAGE_DOS_HEADER dos;
		IMAGE_NT_HEADERS nt;

		fread(&dos, sizeof(IMAGE_DOS_HEADER), 1, file);
		fseek(file, dos.e_lfanew, SEEK_SET);
		fread(&nt, sizeof(IMAGE_NT_HEADERS), 1, file);

		session->process.base = nt.OptionalHeader.ImageBase;
		session->process.size = nt.OptionalHeader.SizeOfImage;

		fclose(file);
	}

	session->request = KeProcessRequest;
	session->target = NULL;
	session->attach = KeSessionAttachProcess;
	session->process.name = "C:\\Users\\Michael\\Desktop\\NEPTUNE\\neptune-software-suite\\private\\src\\os\\boot\\nboot\\Debug\\nboot.pdb";
	session->process.process = 1;

	session->attach("Called from DbgCreateSession");

	DbgSetCurrentSession(session);
}

void DbgParseCommandLine (int argc, char** argv) {
	if (argv[1]) {
		/* create new session with argv[1] program file */
		dbgSession* session = 0;
		DbgCreateSession (argv[1]);
		do {
			session = DbgGetCurrentSession ();
		}while (session == 0);
		DbgInitialize (session);
	}
}

int main (int argc, char** argv) {

	InitializeCriticalSection(&_dbgDisplayMutex);

	// session->target = pipe.

	DbgInfo ();
	printf ("\n");

	/* Create default session; application is in argv[1]. */
	DbgParseCommandLine (argc, argv);
	DbgConsoleEntry ();

	DeleteCriticalSection(&_dbgDisplayMutex);

/*

https://msdn.microsoft.com/en-us/library/windows/hardware/ff538891(v=vs.85).aspx

According to in memory window of debugger, region 0x00039530 gets freed just fine but
_CrtDumpMemoryLeaks still detects it as a memory even though we watched it get deallocated
directly by free. 0x00039530 only gets freed that one time.

Note -- memory leaks refer to NDBG specific structures dbgSourceFile, dbgSourceLine, dbgSymbol
when they are allocated in listAddElement.

*/

//	_CrtDumpMemoryLeaks();
	return EXIT_SUCCESS;
}
