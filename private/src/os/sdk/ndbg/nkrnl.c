/************************************************************************
*
*	nkrnl.c - Neptune Kernel Session
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "defs.h"

char DbgRead(IN dbgSession* session) {
	char c =  DbgReadPipe(session->target);
//	printf("[get:%c]", c);
	return c;
}

void DbgWrite(IN dbgSession* session, IN char c) {

	/* write the byte. */
	DbgWritePipe(session->target, c);
}

void DbgWriteAck(IN dbgSession* session) {

	DbgWritePipe(session->target, '+');
}

void DbgWriteNack(IN dbgSession* session) {

	DbgWritePipe(session->target, '-');
}

static int hex(unsigned char ch)
{
	if (ch >= 'a' && ch <= 'f')
		return ch - 'a' + 10;
	if (ch >= '0' && ch <= '9')
		return ch - '0';
	if (ch >= 'A' && ch <= 'F')
		return ch - 'A' + 10;
	return -1;
}
static const char hexchars[] = "0123456789abcdef";

void DbgGetPacket(IN dbgSession* session, IN char* buffer, IN size_t maxBytes) {

	unsigned char checksum;
	unsigned char xmitcsum;
	unsigned char ch;
	index_t count;
	int i;

	/* $<packet information>#<checksum> */
	do {

		/* waits for start character. */
		while ((ch = (DbgRead(session) & 0x7f)) != '$')
			;

		count = 0;
		checksum = 0;
		xmitcsum = -1;

		/* read unti end character (#) or end of buffer. */
		while (count < maxBytes) {
			ch = DbgRead(session) & 0x7f;
			if (ch == '#')
				break;
			checksum += ch;
			buffer[count] = ch;
			count++;
		}

		/* if greater then output buffer, ignore this packet. */
		if (count >= maxBytes) {
			printf("\n\r** ignore, %i > %i", count, maxBytes);
			continue;
		}

		buffer[count] = 0;

		if (ch == '#') {

			xmitcsum = hex(DbgRead(session) & 0x7f) << 4;
			xmitcsum |= hex(DbgRead(session) & 0x7f);

			if (checksum != xmitcsum)
				DbgWriteNack(session);
			else
				DbgWriteAck(session);

			/* if a sequence character is present, reply with
			the sequence id. */

			if (buffer[2] == ':') {

				DbgWrite(session, buffer[0]);
				DbgWrite(session, buffer[1]);

				/* remove sequence characters from buffer. */
				count = strlen(buffer);
				for (i = 3; i <= count; i++)
					buffer[i - 3] = buffer[i];
			}
		}

	} while (checksum != xmitcsum);
}

void DbgSendPacket(IN dbgSession* session, IN char* buffer) {

	unsigned char c;
	unsigned char checksum;
	int count;

	/* $<packet information>#<checksum> */
	do {
		checksum = 0;
		count = 0;

		printf("\n\rDbgSendPacket: ");
		DbgWrite(session, '$');
		while ((c = buffer[count]) != 0) {

			/*
				nboot's DbgReadSerial needs time to take characters out of the
				FIFO buffer. If we send it to fast, nboot won't get it in time
				(and will run out of characters to process - getting stuck in a
				loop. So we sleep to give nboot some time.

				We chose 10 since FIFO buffers for UART devices are typically 14-16 bytes.
			*/
			if ((count % 10) == 0)
				Sleep(50);

			DbgWrite(session, c);
			checksum += c;
			count += 1;
		}
		printf("\n\r");
		DbgWrite(session, '#');
		DbgWrite(session, hexchars[checksum >> 4]);
		DbgWrite(session, hexchars[checksum & 0xf]);

	} while (DbgRead(session) != '+');
}

void KeSessionAttachProcess(char* path);

#define BUFMAX 400

/* this is called from main thread NOT nkrnl thread. */
unsigned long KeProcessRequest(IN dbgProcessReq request, IN dbgSession* session,
	IN OPT void* addr, IN OUT OPT void* data, IN OPT size_t size) {

	char output[BUFMAX];

	switch(request) {
	case DBG_REQ_CONTINUE:

		output[0] = 'c';
		output[1] = NULL;

		printf("\n\rDBG_REQ_CONTINUE");
		DbgSendPacket(session, output);

		DbgConsoleSetState(DBG_CON_BREAK);
		break;

	case DBG_REQ_READ:
	case DBG_REQ_WRITE:
	case DBG_REQ_GETCONTEXT:
	case DBG_REQ_SETCONTEXT:
	case DBG_REQ_BREAK:
	case DBG_REQ_STOP:
	case DBG_REQ_ATTACH:
	case DBG_REQ_DETATCH:
	/*
	read/write/translate physical/virtual addresses
	may not be supported by all session types
	*/
	case DBG_REQ_READPHYS:
	case DBG_REQ_WRITEPHYS:
	case DBG_REQ_TRANSLATE:	/* translates vaddr_t to paddr_t */
		break;
	};

	printf("\n\rKeProcessRequest");
	return TRUE;
}

typedef struct _HalExceptionContext {
	/* segments. */
	uint32_t gs, fs, es, ds, ss;
	/* pushad */
	uint32_t eax, ecx, edx, ebx, esp, ebp, esi, edi;
	/* cpu context */
	uint32_t vector;
	uint32_t errorCode;
	uint32_t eip;
	uint32_t cs;
	uint32_t eflags;
}HalExceptionContext;

// http://lxr.free-electrons.com/source/arch/mn10300/kernel/gdb-stub.c#L183

static int hexToInt(char **ptr, int *intValue)
{
	int numChars = 0;
	int hexValue;

	*intValue = 0;

	while (**ptr) {
		hexValue = hex(**ptr);
		if (hexValue < 0)
			break;

		*intValue = (*intValue << 4) | hexValue;
		numChars++;

		(*ptr)++;
	}

	return numChars;
}

static char *hex2mem(char *buf, char *mem, int count, int may_fault)
{
	int i;
	unsigned char ch;

	for (i = 0; i<count; i++)
	{
		ch = hex(*buf++) << 4;
		ch |= hex(*buf++);
		*mem++ = ch;
	}

	return buf;
}

static unsigned char *mem2hex(char *mem, char *buf, int count, int may_fault)
{
	unsigned char ch;
	int i;

	for (i = 0; i < count; i++) {
		ch = *mem++;
		*buf++ = hexchars[ch >> 4];
		*buf++ = hexchars[ch % 16];
	}

	*buf = 0;
	return buf;
}


// flags and cs not sent...

typedef struct NdbgRegs32 {
	uint32_t esp, ebp, esi, edi;
	uint32_t eax, ebx, ecx, edx;
	uint32_t eip;
	uint32_t cs, ds, es, ss, fs, gs;
	uint32_t eflags;
}NdbgRegs32;

void DbgDumpExceptionContext(HalExceptionContext* context) {

	printf("\n\r ds: %x fs: %x gs: %x es: %x", context->ds, context->fs, context->gs, context->es);
	printf("\n\r cs:eip %x:%x", context->cs, context->eip);
	printf("\n\r ss:esp %x:%x", context->ss, context->esp);
	printf("\n\r flags: %x vector: %x errorCode: %x", context->eflags, context->vector, context->errorCode);
	printf("\n\r eax: %x ebx: %x ecx: %x edx: %x", context->eax, context->ebx, context->ecx, context->edx);
	printf("\n\r esi: %x edi: %x ebp: %x", context->esi, context->edi, context->ebp);
}

char input[BUFMAX];
char output[BUFMAX];

/* reads memory contents. */
BOOL KeGetMemory(dbgSession* session, paddr_t address, void* data, size_t size) {

	char hexAddress[10];
	char hexSize[10];
	char* p = output;

	/* convert the numbers as hex strings. */
	sprintf(hexAddress, "%x", address);
	sprintf(hexSize, "%x", size);

	/* build up the packet. The syntax is mAA..AA,LLLL */
	*p++ = 'm';
	strcpy(p, hexAddress);
	p += strlen(hexAddress);
	*p++ = ',';
	strcpy(p, hexSize);

	/* send packet and wait for a response. */
	DbgSendPacket(session, output);
	DbgGetPacket(session, input, BUFMAX);

	/* this can only fail if mem2hex fails in client. */
	if (strcmp(input, "E03") == 0)
		return FALSE;

	/* malformed syntax - this is a bug. */
	if (strcmp(input, "E01") == 0)
		return FALSE;

	/* copy the memory contents and return success. */
	memcpy(data, input, size);
	return TRUE;
}

/* mAA..AA,LLLL: write LLLL bytes at address AA..AA. */
BOOL KeSetMemory(dbgSession* session, paddr_t address, void* data, size_t size) {

	char hexAddress[10];
	char hexSize[10];
	char* p = output;

	/* convert the numbers as hex strings. */
	sprintf(hexAddress, "%x", address);
	sprintf(hexSize, "%x", size);

	/* build up the packet. The syntax is MAA..AA,LLLL:data */
	*p++ = 'M';
	strcpy(p, hexAddress);
	p += strlen(hexAddress);
	*p++ = ',';
	strcpy(p, hexSize);
	p += strlen(hexSize);
	*p++ = ':';
	p = mem2hex(data, p, size, 0);

	printf("\n\rWrite packet: '%s'", output);

	/* this can only fail if mem2hex fails in client. */
	DbgSendPacket(session, output);
	DbgGetPacket(session, input, BUFMAX);

	printf("\n\rResult packet: '%s'", input);

	/* if the memory contents was set successfully, return success. */
	if (strcmp(input, "OK") == 0)
		return TRUE;

	/* this can only fail if mem2hex fails in client. */
	else if (strcmp(input, "E03") == 0)
		return FALSE;

	/* malformed syntax - this is a bug. */
	else if (strcmp(input, "E02") == 0)
		return FALSE;

	/* unkown error. */
	return FALSE;
}

void KeGetContext(dbgSession* session, HalExceptionContext* context) {

	DbgSendPacket(session, "g");
	DbgGetPacket(session, input, BUFMAX);
	hex2mem(input, context, sizeof(NdbgRegs32), 0);
}

void KeSetContext(dbgSession* session, HalExceptionContext* context) {

	char* p = input;

	output[0] = 'G';
	p = mem2hex(context, &output[1], sizeof(HalExceptionContext), 0);

	DbgSendPacket(session, output);

	DbgGetPacket(session, input, 32);
	if (strcmp(input, "OK") == 0)
		printf("\n\r--> Good");
	else
		printf("\n\rInvalid: '%s'", input);
}

void KeMemoryPrint(unsigned char* memory, size_t size) {

	int col = 0;
	printf("\n\r");
	while (size--) {
		printf("\t%x", *memory++);
		if (col++ > 7) {
			printf("\n\r");
			col = 0;
		}
	}
}

void KeExceptionHandler(dbgSession* session, char* message) {

	uint32_t vector;
	uint32_t eip;
	uint32_t esp;
	uint32_t value;
	char* ptr;
	NdbgRegs32 registers;

	/*
		Format from client is: TNN:EIP;ESP;
		where T is command type, NN is exception code,
		and EIP and ESP are the instruction pointer and
		stack pointer.	
	*/
	ptr = &message[1];
	hexToInt(&ptr, &vector);
	ptr = hex2mem(ptr+1, &eip, 4, 0);
	ptr = hex2mem(ptr+1, &esp, 4, 0);

	printf("\n\r*** Exception (%i) at 0x%x", vector, eip);

	KeGetContext(session, &registers);

	DbgDumpExceptionContext(&registers);


	{
		unsigned char data[30];
		memset(data, 0, 30);

		KeGetMemory(session, 0x1000, data, 30);

		printf("\n\rGot memory");

		KeMemoryPrint(data, 30);

		KeSetMemory(session, 0x500000, data, 30);
	}

}

dbgSessionState KeSessionProcessEvent(dbgSession* session, char* message) {

	// calls session->event to notify actual debugger event handler in dbg.c.

	uint32_t eventCode = EXCEPTION_DEBUG_EVENT;

	if (message[0] == 'T')
		eventCode = EXCEPTION_DEBUG_EVENT;

	switch (eventCode) {

	case EXCEPTION_DEBUG_EVENT:
		KeExceptionHandler(session,message);
		break;

	/* these are not yet supported. */
	case CREATE_THREAD_DEBUG_EVENT:
	case CREATE_PROCESS_DEBUG_EVENT:
	case EXIT_THREAD_DEBUG_EVENT:
	case EXIT_PROCESS_DEBUG_EVENT:
	case LOAD_DLL_DEBUG_EVENT:
	case UNLOAD_DLL_DEBUG_EVENT:
	case OUTPUT_DEBUG_STRING_EVENT:
	case RIP_EVENT:
		break;
	};

	DbgConsoleSetState(DBG_CON_CONTINUE);
	return DBG_STATE_CONTINUE;
}

int __stdcall KeSessionAttachThreadEntry(IN char* command) {

	dbgSession*         session;
	char input[BUFMAX];

	session = DbgGetCurrentSession();
	if (!session)
		return 0;

	if (!session->target) {
		session->target = (handle_t)DbgCreatePipe("ndbg");
	}

	/* attempt to enumerate symbol information */
	if (!DbgSymbolLoad(session))
		DbgDisplayError("Unable to load symbols");
	else
		DbgDisplayMessage("Symbols loaded");

	/* this is the current session */
	DbgSetCurrentSession(session);

	/* session thread event loop */
	while (TRUE) {

		/* wait for remote client. */
		DbgGetPacket(session, input, BUFMAX);

		/* process client event. */
		KeSessionProcessEvent(session, input);
		session->state = DBG_STATE_SUSPEND;
	}

	/* free symbols */
	DbgSymbolFree(session);

	/* clear current session, release resources and return */
	if (DbgGetCurrentSession() == session)
		DbgSetCurrentSession(0);

	free(session);
	return 0;
}

/**
*	Create session
*	\param path Command line
*/
void KeSessionAttachProcess(char* path) {

	/* create new thread for debug session */
	CreateThread(0, 0, (LPTHREAD_START_ROUTINE)KeSessionAttachThreadEntry, path, 0, 0);
}
