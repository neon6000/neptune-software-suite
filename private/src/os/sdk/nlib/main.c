/************************************************************************
*
*	main.c
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nlib.h"
#include <time.h>
#include <stdarg.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

// http://www.mit.edu/afs.new/sipb/project/wine/src/wine-0.9.37/tools/winedump/lib.c

PRIVATE int         _globalFlags;
PRIVATE char*       _outFileName;
PRIVATE StringList* _extractList;
PRIVATE StringList* _removeList;
PRIVATE StringList* _fileList;

PRIVATE void Error(IN char* s, ...) {

	char msg[128];
	va_list args;

	va_start(args, s);
	vsnprintf(msg, 127, s, args);
	va_end(args);
	fprintf(stdout, "\n\rError: %s", msg);
	__debugbreak();
	exit(1);
}

PRIVATE void DisplayLogo(void) {

	puts("\n\rNeptune Library Manager");
	puts("Copyright(c) BrokenThorn Entertainment, Co");
}

PRIVATE char* OpenFirstArchive(IN StringList* files) {

	String* fname;
	for (fname = files; fname; fname = fname->next) {
		if (IsArchive(fname->text))
			return fname->text;
	}
	return NULL;
}

PRIVATE void ListArchive(IN Archive* archive, IN BOOL listSymbols) {

	Module* module;
	String* symbol;

	printf("\nArchive '%s'", archive->file->name);
	if (listSymbols) {
		for (module = archive->firstModule; module; module = module->next) {
			printf("\n\n  %s", module->name);
			for (symbol = module->symbols; listSymbols && symbol; symbol = symbol->next)
				printf("\n    Symbol '%s'", symbol->text);
		}
	}
	else{
		for (module = archive->firstModule; module; module = module->next)
			printf("\n  %s", module->name);
	}
}

PRIVATE Module* AddObject(IN Archive* archive, IN char* fname) {

	Module* module;
	File*   src;

	src = FileOpen(fname);
	if (!src) {
		Error("Not able to open file '%s'", fname);
		return NULL;
	}
	module = AddModule(archive, fname);
	module->srcfile = src;
	module->date = FileModifyTime(src);
	module->size = FileSize(src);
	module->symbols = ExtractSymbols(src, 0);
	return module;
}

#define OPTION_EXTRACT   0x1
#define OPTION_LIST      0x2
#define OPTION_NOLOGO    0x4
#define OPTION_OUT       0x8
#define OPTION_REMOVE   0x10
#define OPTION_VERBOSE  0x20
#define OPTION_LISTALL  0x40
#define OPTION_HELP     0x80

typedef struct Option {
	int   type;
	char* optionSwitch;
	char* descr;
}Option;

PRIVATE Option _options[] = {

	{ OPTION_EXTRACT, "extract", "Extract object file from archive" },
	{ OPTION_LIST,    "list",    "List all archive modules" },
	{ OPTION_NOLOGO,  "nologo",  "Supress startup banner" },
	{ OPTION_OUT,     "out",     "Specify output archive file name" },
	{ OPTION_REMOVE,  "remove",  "Remove object file from archive" },
	{ OPTION_VERBOSE, "verbose", "Enable verbose output" },
	{ OPTION_LISTALL, "listall", "List all archive modules and extracted symbols" },
	{ OPTION_HELP,    "help",    "Provides information on supported options" },
	{ OPTION_HELP,    "?",       "Provides information on supported options" }
};

PRIVATE Option* FindOption(IN char* s) {

	size_t  max;
	size_t  i;
	char*   opt;
	char*   end;
	Option* res;

	// options may be of the form -option and -option:parameter
	// so match only the option:

	res = NULL;
	opt = _strdup(s);
	end = strrchr(opt, ':');
	if (end)
		*end = '\0';

	// support both -option and /option. this is case insensitive:

	if (opt[0] == '-' || opt[0] == '/') {
		max = sizeof(_options) / sizeof(Option);
		for (i = 0; i < max; i++) {
			if (!_stricmp(_options[i].optionSwitch, opt + 1)) {
				res = &_options[i];
				break;
			}
		}
	}
	free(opt);
	return res;
}

PRIVATE void DisplayOptions(void) {

	size_t  max;
	size_t  i;

	max = sizeof(_options) / sizeof(Option);
	for (i = 0; i < max; i++)
		printf("\n   -%-8s%s", _options[i].optionSwitch, _options[i].descr);
}

PRIVATE BOOL ParseCommandLine(IN int argc, IN char** argv) {

	Option* option;
	int     i;
	char*   param;

	if (argc == 1)
		return TRUE;

	for (i = 1; i < argc; i++) {

		option = FindOption(argv[i]);
		if (!option) {
			if (isalpha(argv[i][0])) {
				AddString(_fileList, argv[i], FALSE);
				continue;
			}
			else{
				Error("Invalid or unsupported option '%s'", argv[i]);
				return FALSE;
			}
		}
		switch (option->type) {
		case OPTION_LISTALL: SET(_globalFlags, OPTION_LISTALL); break;
		case OPTION_LIST:    SET(_globalFlags, OPTION_LIST);    break;
		case OPTION_NOLOGO:  SET(_globalFlags, OPTION_NOLOGO);  break;
		case OPTION_VERBOSE: SET(_globalFlags, OPTION_VERBOSE); break;
		case OPTION_HELP:    SET(_globalFlags, OPTION_HELP);    break;
		case OPTION_OUT:
			param = strrchr(argv[i], ':');
			if (!param)
				Error("'%s': missing parameter", argv[i]);
			_outFileName = _strdup(param + 1);
			break;
		case OPTION_REMOVE:
			param = strrchr(argv[i], ':');
			if (!param)
				Error("'%s': missing parameter", argv[i]);
			AddString(_removeList, _strdup(param + 1), FALSE);
			SET(_globalFlags, OPTION_REMOVE);
			break;
		case OPTION_EXTRACT:
			param = strrchr(argv[i], ':');
			if (!param)
				Error("'%s': missing parameter", argv[i]);
			AddString(_extractList, _strdup(param + 1), FALSE);
			SET(_globalFlags, OPTION_EXTRACT);
			break;
		};
	}
	return TRUE;
}

PUBLIC void DisplayHelp(void) {

	printf("\nSyntax: NLIB [options] [modules...]");
	printf("\n\nOptions:\n");
	DisplayOptions();
	printf("\n");
}

PUBLIC int main(IN int argc, IN char** argv) {

	Archive* archive;
	String*  member;

	_removeList = NewStringList();
	_extractList = NewStringList();
	_fileList = NewStringList();

	if (!ParseCommandLine(argc, argv)) {
		DisplayLogo();
		DisplayHelp();
		return 1;
	}

	if (OFF(_globalFlags, OPTION_NOLOGO))
		DisplayLogo();

	if (ON(_globalFlags, OPTION_HELP))
		DisplayHelp();

	if (ON(_globalFlags, OPTION_LIST)) {
		archive = LoadArchive(OpenFirstArchive(_fileList));
		if (archive)
			ListArchive(archive, FALSE);
		CloseArchive(archive);
		return 0;
	}

	if (ON(_globalFlags, OPTION_LISTALL)) {
		archive = LoadArchive(OpenFirstArchive(_fileList));
		if (archive)
			ListArchive(archive, TRUE);
		CloseArchive(archive);
		return 0;
	}

	// Handle /EXTRACT:memberName options. For these, extract
	// all members in _extractList from the archive file specified
	// by the user into memberName.obj:

	if (ON(_globalFlags, OPTION_EXTRACT)) {

		String* memberName;
		Module* module;
		File*   outfile;

		archive = LoadArchive(OpenFirstArchive(_fileList));
		if (!archive)
			return 0;
		for (memberName = _extractList; memberName; memberName = memberName->next) {
			module = FindModule(archive, memberName->text);
			if (!module)
				continue;
			outfile = FileCreate(module->name);
			ExtractModule(module, outfile);
			FileClose(outfile);
		}
		return 0;
	}

	// handle /REMOVE:memberName options. For these, remove
	// all members in _removeList from the archive file specified
	// by the user. If _outFileName is specified, write
	// the new archive to it. Else, overwrite the archive file.

	if (ON(_globalFlags, OPTION_REMOVE)) {

		File* outfile;
		String* memberName;
		char*   from;
		char*   to;
		BOOL    usingTemp;

		usingTemp = FALSE;
		archive = LoadArchive(OpenFirstArchive(_fileList));
		if (!archive)
			return 0;
		if (_outFileName)
			outfile = FileCreate(_outFileName);
		else {
			usingTemp = TRUE;
			outfile = FileCreateTemp(archive->file->name);
		}

		for (memberName = _removeList; memberName; memberName = memberName->next)
			FreeModule(archive, memberName->text);
		WriteArchive(archive, outfile->name);
		from = outfile->name;
		to = archive->file->name;
		FileClose(outfile);
		FileClose(archive->file);

		if (usingTemp) {
			FileRemove(to);
			FileRename(from, to);
		}
		return 0;
	}

	// if the user already specified an archive file, use it:

	archive = LoadArchive(OpenFirstArchive(_fileList));

	// Can only add members to one archive file. If multiple are specified,
	// we would not know which archive to place members to, so we do not allow this:

	if (archive && _outFileName) {
		Error("Can only add members to one archive file");
		return 1;
	}

	// if no archive file, then an output file name must be specified:

	if (!archive) {

		File* outfile;
		if (!_outFileName) {
			Error("Unspecified archive file");
			return 1;
		}
		outfile = FileCreate(_outFileName);
		archive = NewArchive(outfile);
	}

	// add members to archive:

	for (member = _fileList; member; member = member->next)
		AddObject(archive, member->text);
	WriteArchive(archive, "./outfile.rar");
	CloseArchive(archive);

	__debugbreak();
	return 0;
}
