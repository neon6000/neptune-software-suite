/************************************************************************
*
*	list.c - Linked list
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <string.h>
#include <malloc.h>
#include <assert.h>
#include "nlib.h"

PUBLIC char* NewString(IN char* s, IN unsigned long len) {

	char* mem = malloc(len + 1);
	memcpy(mem, s, len);
	mem[len] = 0;
	return mem;
}

PUBLIC StringList* FindString(IN StringList* list, IN char* str) {

	for (StringList* s = list; s; s = s->next) {
		if (!s->text)
			continue;
		if (!strcmp(s->text, str))
			return s;
	}
	return NULL;
}

PUBLIC size_t GetStringCount(IN StringList* list) {

	String* s;
	size_t count;

	count = 0;
	for (s = list; s; s = s->next)
		count++;
	return count;
}

PUBLIC StringList* NewStringList(void) {

	StringList* s = ALLOC_OBJ(StringList);
	s->text = NULL;
	s->next = NULL;
	return s;
}

PUBLIC void AddString(IN StringList* list, IN char* str, IN BOOL dup) {

	StringList* s;
	if (!dup && FindString(list, str))
		return;
	if (!list->text) {
		list->text = str;
		list->next = NULL;
		return;
	}
	for (s = list; s->next; s = s->next)
		;
	s->next = ALLOC_OBJ(StringList);
	s->next->text = str;
	s->next->next = NULL;
}
