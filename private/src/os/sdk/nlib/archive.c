/************************************************************************
*
*	archive.c
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nlib.h"
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

// reference https://courses.cs.washington.edu/courses/cse378/04wi/lectures/LinkerFiles/coff.pdf
// section 7 -- Archive (Library) File Format

// import types:
#define IMPORT_CODE  0
#define IMPORT_DATA  1
#define IMPORT_CONST 2

// import name type:
#define IMPORT_ORDINAL         0
#define IMPORT_NAME            1
#define IMPORT_NAME_NOPREFIX   2
#define IMPORT_NAME_UNDECORATE 3

#define ARCHIVE_SIGNATURE              "!<arch>\n"
#define ARCHIVE_PAD                    "\n"
#define ARCHIVE_LINKER_MEMBER          "/               "
#define ARCHIVE_LONGNAMES_MEMBER       "//              "

typedef struct ArchiveHeader {
	uint8_t name[16];                          // File member name - `/' terminated.
	uint8_t date[12];                          // File member date - decimal.
	uint8_t userID[6];                         // File member user id - decimal.
	uint8_t groupID[6];                        // File member group id - decimal.
	uint8_t mode[8];                           // File member mode - octal.
	uint8_t size[10];                          // File member size - decimal.
	uint8_t endHeader[2];                      // String to end header.
} ArchiveHeader;

#define SIZEOF_ARCHIVE_MEMBER_HDR      60
#define ALIGN(x)                       ((x + 1) & ~1)

// global archive list:
PRIVATE Archive* _archives;

PUBLIC BOOL IsArchive(IN char* fname) {

	char  sig[8];
	File* file;

	file = FileOpen(fname);
	if (!file)
		return FALSE;
	FileRead(file, 8, sig);
	FileClose(file);

	if (!memcmp(sig, ARCHIVE_SIGNATURE, 8))
		return TRUE;
	return FALSE;
}

PUBLIC Archive* NewArchive(IN File* file) {

	Archive* archive;
	Archive* current;

	archive = ALLOC_OBJ(Archive);
	archive->file = file;
	archive->firstModule = NULL;
	archive->next = NULL;

	if (!_archives)
		_archives = archive;
	else{
		for (current = _archives; current->next; current = current->next)
			;
		current->next = archive;
	}

	return archive;
}

PUBLIC BOOL ExtractModule(IN Module* module, OUT File* outfile) {

	char          temp[512];
	ArchiveHeader header;
	Archive*      archive;
	uint32_t      start;
	uint32_t      blocks;
	uint32_t      extra;
	File*         srcfile;

	archive = module->parent;
	blocks = module->size / 512;
	extra = module->size % 512;

	if (module->srcfile) {
		srcfile = module->srcfile;
		start = 0;
	}
	else{
		srcfile = archive->file;
		start = FileTell(srcfile);
		FileSeek(srcfile, module->hdrOffset);
		FileRead(srcfile, sizeof(ArchiveHeader), &header);
	}

	for (; blocks != 0; blocks--) {
		FileRead(srcfile, 512, temp);
		FileWrite(outfile, 512, temp);
	}
	if (extra > 0) {
		FileRead(srcfile, extra, temp);
		FileWrite(outfile, extra, temp);
	}

	if (start)
		FileSeek(srcfile, start);
	return TRUE;
}

PUBLIC Module* AddModule(IN Archive* archive, IN char* name) {

	Module* current;
	Module* module;

	module = ALLOC_OBJ(Module);
	memset(module, 0, sizeof(Module));
	module->name = name;
	module->parent = archive;

	if (!archive->firstModule)
		archive->firstModule = module;
	else{
		for (current = archive->firstModule; current->next; current = current->next)
			;
		current->next = module;
	}

	return module;
}

PUBLIC Module* FindModule(IN Archive* archive, IN char* name) {

	Module* current;

	for (current = archive->firstModule; current; current = current->next) {
		if (!strcmp(current->name, name))
			return current;
	}
	return NULL;
}

PUBLIC size_t GetModuleCount(IN Archive* archive) {

	Module* current;
	uint32_t count;

	count = 0;
	for (current = archive->firstModule; current; current = current->next)
		count++;
	return count;
}

PUBLIC void FreeModule(IN Archive* archive, IN char* name) {

	Module* current;
	Module* module;

	if (!archive->firstModule)
		return;
	if (!strcmp(archive->firstModule->name, name)) {
		module = archive->firstModule;
		archive->firstModule = module->next;
		free(module);
		return;
	}
	for (current = archive->firstModule; current->next; current = current->next) {
		if (!strcmp(current->next->name, name)) {
			module = current->next;
			current->next = current->next->next;
			free(module);
			return;
		}
	}
}

PUBLIC void CloseArchive(IN Archive* archive) {

	if (!archive)
		return;

	FileClose(archive->file);
}

PUBLIC Archive* LoadArchive(IN char* name) {

	char          sig[8];
	File*         file;
	Archive*      archive;
	ArchiveHeader header;
	Module*       module;
	char*         longnames;
	unsigned long fpos;
	unsigned long size;

	// try to open the archive file:

	longnames = NULL;
	file = FileOpen(name);
	if (!file)
		return NULL;

	// verify that the signature is correct:

	FileRead(file, 8, sig);
	if (memcmp(sig, ARCHIVE_SIGNATURE, 8)) {
		FileClose(file);
		return NULL;
	}

	// read first linker member:

	FileRead(file, sizeof(ArchiveHeader), &header);
	if (memcmp(header.name, ARCHIVE_LINKER_MEMBER, 16)) {
		FileClose(file);
		return NULL;
	}
	size = strtoul((const char *)header.size, NULL, 10);
	FileSeek(file, FileTell(file) + ALIGN(size));

	// read second linker member:

	FileRead(file, sizeof(ArchiveHeader), &header);
	if (memcmp(header.name, ARCHIVE_LINKER_MEMBER, 16)) {
		FileClose(file);
		return NULL;
	}
	size = strtoul((const char *)header.size, NULL, 10);
	FileSeek(file, FileTell(file) + ALIGN(size));

	// read long name linker member:

	FileRead(file, sizeof(ArchiveHeader), &header);
	if (memcmp(header.name, ARCHIVE_LONGNAMES_MEMBER, 16)) {
		FileClose(file);
		return NULL;
	}
	size = strtoul((const char *)header.size, NULL, 10);
	longnames = malloc(size);
	fpos = FileTell(file);
	FileRead(file, size, longnames);
	FileSeek(file, fpos + ALIGN(size));

	archive = NewArchive(file);

	// read module list:

	while (TRUE) {

		char* realname;
		char*  c;
		unsigned long hdroffset;

		hdroffset = FileTell(file);

		if (!FileRead(file, sizeof(ArchiveHeader), &header))
			break;
		size = strtoul((const char *)header.size, NULL, 10);

		if (header.name[0] == '/' && isdigit(header.name[1])) {
			unsigned long offset = strtoul((const char *)header.name + 1, NULL, 10);
			realname = _strdup(longnames + offset);
		}
		else{
			c = memchr(header.name, '/', 16);
			if (c) {
				*c = 0;
				realname = _strdup(header.name);
				*c = '/';
			}
		}

		if (realname) {

			// prepare module:

			module = AddModule(archive, realname);
			module->hdrOffset = hdroffset;
			module->size = size;
			module->date = strtoul((const char *)header.date, NULL, 10);
			module->mode = strtoul((const char *)header.mode, NULL, 10);
			module->parent = archive;

			// extract symbols from this module:

			module->symbols = ExtractSymbols(file, hdroffset + sizeof(ArchiveHeader));
		}

		FileSeek(file, ALIGN(hdroffset + sizeof(ArchiveHeader) + size));
	}

	return archive;
}

PRIVATE void WriteHeader(IN File* outfile, IN char* name, IN time_t date, IN int userId,
	   IN int groupId, IN int mode, IN int size,
	   IN OPTIONAL StringList* longNames) {

	size_t        offset;
	ArchiveHeader hdr;
	String*       longName;
	char*         value;

	value = malloc((sizeof(int)* 8 + 1));
	memset(&hdr, ' ', sizeof(hdr));

	if (strlen(name) > 15) {

		// look up name in the long names list and compute its offset:
		offset = 0;
		for (longName = longNames; longName; longName = longName->next) {
			if (!strcmp(longName->text, name))
				break;
			offset += strlen(longName->text) + 1;
		}

		// long name enteries have the name "/<offset>":
		_itoa(offset, value, 10);
		hdr.name[0] = '/';
		memcpy(&hdr.name[1], value, strlen(value));
	}
	else{
		memcpy(hdr.name, name, strlen(name));
		hdr.name[strlen(name)] = '/';
	}

	_itoa((int) date, value, 10);
	memcpy(hdr.date, value, strlen(value));
	_itoa(mode, value, 10);
	memcpy(hdr.mode, value, strlen(value));
	_itoa(size, value, 10);
	memcpy(hdr.size, value, strlen(value));
	memcpy(&hdr.endHeader, "`\n",     2);
	free(value);

	FileWrite(outfile, sizeof(ArchiveHeader), &hdr);
}

#define TO_BIG_ENDIAN(x) (( x >> 24 ) | (( x << 8) & 0x00ff0000 )| ((x >> 8) & 0x0000ff00) | ( x << 24))

PRIVATE void WritePadding(IN File* outfile) {

	int fpos;
	int padbytes;

	fpos = FileTell(outfile);
	padbytes = ALIGN(fpos);

	while (fpos++ < padbytes)
		FileWrite(outfile, 1, ARCHIVE_PAD);
}

PRIVATE size_t WriteFirstLinkerMember(IN File* outfile, IN Archive* archive, IN uint32_t* hdrOffsets, IN StringList* longnames) {

	Module*     module;
	uint32_t*   offsets;
	StringList* stringTable;
	uint32_t    moduleIndex;
	uint32_t    offsetIndex;
	String*     string;
	uint32_t    totalSize;
	uint32_t    symtablesize;
	size_t      symcount;
	time_t      currentTime;
	size_t      symCountBig;

	symcount = 0;
	symtablesize = 0;
	moduleIndex  = 0;
	offsetIndex  = 0;

	for (module = archive->firstModule; module; module = module->next) {
		String* symbol;
		for (symbol = module->symbols; symbol; symbol = symbol->next) {
			symtablesize += strlen(symbol->text) + 1; // +1 fo null terminator
			symcount++;
		}
	}
	symCountBig = TO_BIG_ENDIAN(symcount);

	if (!outfile)
		return sizeof(ArchiveHeader)+4 + 4 * symcount + symtablesize;

	offsets      = malloc(4 * symcount);
	stringTable  = NewStringList();

	for (module = archive->firstModule; module; module = module->next) {
		String* symbol;
		for (symbol = module->symbols; symbol; symbol = symbol->next) {
			AddString(stringTable, symbol->text, TRUE);
			offsets[offsetIndex++] = TO_BIG_ENDIAN(hdrOffsets[moduleIndex]);
		}
		moduleIndex++;
	}

	totalSize = 4 + 4 * symcount + symtablesize;

	time(&currentTime);
	WriteHeader(outfile, "", currentTime, 0, 0, 0, totalSize, longnames);
	FileWrite(outfile, 4, &symCountBig);
	FileWrite(outfile, 4 * symcount, offsets);
	for (string = stringTable; string; string = string->next)
		FileWrite(outfile, strlen(string->text) + 1, string->text); // +1 for null terminator

	WritePadding(outfile);
	return 0;
}

typedef struct SymbolIndex {
	char*   symbol;
	size_t  index;
}SymbolIndex;

PRIVATE int CompareSymbolIndex(IN const SymbolIndex* a, IN const SymbolIndex* b) {

	return strcmp (a->symbol, b->symbol);
}

PRIVATE size_t WriteSecondLinkerMember(IN File* outfile, IN Archive* archive, IN uint32_t* hdrOffsets, IN StringList* longnames) {

	size_t       symcount;
	Module*      module;
	size_t       index;
	size_t       currentHdr;
	StringList*  symname;
	SymbolIndex* symbols;
	size_t       totalSize;
	size_t       symtablesize;
	size_t       modCount;
	time_t       currentTime;

	currentHdr = 0;
	index      = 0;
	symcount   = 0;
	symtablesize = 0;

	// get total number of symbols:

	modCount = 0;
	for (module = archive->firstModule; module; module = module->next) {
		modCount++;
		for (symname = module->symbols; symname; symname = symname->next) {
			symtablesize += strlen(symname->text) + 1;  // +1 for null terminator
			symcount++;
		}
	}

	if (!outfile)
		return sizeof(ArchiveHeader)+4 + 4 * modCount + 4 + 2 * symcount + symtablesize;

	// build symbol list:

	symbols = malloc(symcount * sizeof(SymbolIndex));
	for (module = archive->firstModule; module; module = module->next) {
		for (symname = module->symbols; symname; symname = symname->next) {
			symbols[index].symbol = symname->text;
			symbols[index].index  = currentHdr + 1;       // the indices are 1-based not 0-based
			index++;
		}
		currentHdr++;
	}

	// sort in lexagraphic order:

	qsort(symbols, symcount, sizeof(SymbolIndex), CompareSymbolIndex);

	// write the second linker member to file:

	totalSize = 4 + 4 * modCount + 4 + 2 * symcount + symtablesize;

	time(&currentTime);
	WriteHeader(outfile, "", currentTime, 0, 0, 0, totalSize, longnames);
	FileWrite(outfile, 4, &modCount);
	FileWrite(outfile, 4 * modCount, hdrOffsets);
	FileWrite(outfile, 4, &symcount);
	for (index = 0; index < symcount; index++)
		FileWrite(outfile, 2, &symbols[index].index);
	for (index = 0; index < symcount; index++)
		FileWrite(outfile, strlen(symbols[index].symbol)+1, symbols[index].symbol); // +1 for null terminator

	WritePadding(outfile);
	return 0;
}

PRIVATE size_t WriteLongNamesMember(IN File* outfile, IN StringList* longnames) {

	String* name;
	size_t  totalSize;
	time_t  currentTime;

	totalSize = 0;
	for (name = longnames; name; name = name->next)
		totalSize += strlen(name->text) + 1;

	if (!outfile)
		return sizeof(ArchiveHeader) + totalSize;

	time(&currentTime);
	WriteHeader(outfile, "/", currentTime, 0, 0, 0, totalSize, longnames);
	for (name = longnames; name; name = name->next)
		FileWrite(outfile, strlen(name->text) + 1, name->text);
	WritePadding(outfile);
	return 0;
}

PRIVATE size_t WriteModuleMembers(IN File* outfile, IN Archive* archive, IN uint32_t* hdrOffsets, IN StringList* longNames) {

	Module* module;
	int     fpos;
	int     oldfpos;
	size_t  blocks;
	size_t  extra;
	File*   srcfile;
	char    temp[512];
	size_t  written;

	for (module = archive->firstModule; module; module = module->next) {

		oldfpos = 0;
		written = 0;
		blocks = module->size / 512;
		extra  = module->size % 512;

		if (!module->srcfile) {
			srcfile = archive->file;
			oldfpos = FileTell(srcfile);
			fpos = module->hdrOffset + sizeof(ArchiveHeader);
		}
		else{
			srcfile = module->srcfile;
			fpos = 0;
		}

		FileSeek(srcfile, fpos);

		WriteHeader(outfile, module->name, module->date, 0, 0, 100666, module->size, longNames);

		for (; blocks != 0; blocks--) {
			FileRead(srcfile, 512, temp);
			FileWrite(outfile, 512, temp);
			written += 512;
		}
		if (extra > 0) {
			FileRead(srcfile, extra, temp);
			FileWrite(outfile, extra, temp);
			written += extra;
		}

		if (oldfpos)
			FileSeek(srcfile, oldfpos);

		WritePadding(outfile);
	}

	return 0;
}

PUBLIC int WriteArchive(IN Archive* archive, IN char* outname) {

	File*       outfile;
	uint32_t*   hdrOffsets;
	size_t      modCount;
	Module*     module;
	uint32_t    fileoffset;
	uint32_t    index;
	size_t      firstMemberSize;
	size_t      secondMemberSize;
	size_t      thirdMemberSize;
	StringList* longNames;

	outfile = FileCreate(outname);
	if (!outfile)
		return 1;
	modCount = GetModuleCount(archive);

	// build long name string list:

	longNames = NewStringList();
	for (module = archive->firstModule; module; module = module->next) {
		if (strlen(module->name) > 15)
			AddString(longNames, module->name, TRUE);
	}

	// get size requirement for each linker member:

	firstMemberSize  = WriteFirstLinkerMember   (NULL, archive, NULL, longNames);
	secondMemberSize = WriteSecondLinkerMember  (NULL, archive, NULL, longNames);
	thirdMemberSize  = WriteLongNamesMember     (NULL, longNames);

	// build module header offsets array. ourHdrOffsets contains a list
	// of file offsets of all future module headers in the output file:

	fileoffset = strlen(ARCHIVE_SIGNATURE);
	fileoffset += ALIGN(firstMemberSize);
	fileoffset += ALIGN(secondMemberSize);
	fileoffset += ALIGN(thirdMemberSize);

	hdrOffsets = malloc(modCount * sizeof(uint32_t));
	index = 0;
	for (module = archive->firstModule; module; module = module->next) {
		hdrOffsets[index++] = fileoffset;
		fileoffset += ALIGN(sizeof(ArchiveHeader)+module->size);
	}

	// now write the archive file:

	FileWrite               (outfile, strlen(ARCHIVE_SIGNATURE), ARCHIVE_SIGNATURE);
	WriteFirstLinkerMember  (outfile, archive, hdrOffsets, longNames);
	WriteSecondLinkerMember (outfile, archive, hdrOffsets, longNames);
	WriteLongNamesMember    (outfile, longNames);
	WriteModuleMembers      (outfile, archive, hdrOffsets, longNames);
	FileClose(outfile);
	return 0;
}
