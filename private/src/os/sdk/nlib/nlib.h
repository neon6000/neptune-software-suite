/************************************************************************
*
*	nlib.h
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <stdio.h>

#define IN
#define OUT
#define OPTIONAL
#define PRIVATE static
#define PUBLIC
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))
#define LITTLE_ENDIAN(x) ((x >> 24) | ((x & 0xff0000) >> 16) | ((x & 0xff00) >> 8) | (x & 0xff))

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long uint64_t;
typedef signed char sint8_t;
typedef signed short sint16_t;
typedef signed int sint32_t;
typedef signed long long sint64_t;
typedef char int8_t;
typedef short int16_t;
typedef int int32_t;
typedef long long int64_t;
typedef int bool_t;
typedef bool_t BOOL;
#define TRUE 1
#define FALSE 0
#define INVALID_HANDLE ((unsigned int)0)
typedef unsigned int handle_t;
typedef unsigned int index_t;
typedef unsigned int offset_t;
typedef unsigned int size_t;
typedef void* addr_t;
typedef uint32_t handle_t;

#define PRIVATE static
#define PUBLIC

#define ALLOC_OBJ(t) (malloc(sizeof(t)))

#define ON(v,n)    ((v&n)==(n))
#define OFF(A,B)   (!ON(A,B)) 
#define CLEAR(v,n) (v &= ~(n))
#define SET(v,n)   (v |= (n))
#define FLIP(v,n)  (ON(v,(n)) ? CLEAR(v,(n)) : SET(v,(n)))

typedef struct StringList {
	char*    text;
	struct StringList* next;
}StringList, String;

typedef struct File {
	FILE*    stream;
	char*    name;
	uint32_t size;
}File;

typedef struct Module {
	struct Archive* parent;
	StringList*     symbols;   // EXTERNAL symbols from this module.
	uint32_t        hdrOffset; // if Module is loaded from an archive.
	File*           srcfile;   // if Module is loaded from a file.
	char*           name;
	time_t          date;
	uint32_t        size;
	uint32_t        mode;
	struct Module*  next;
}Module;

typedef struct Archive {
	File*       file;
	Module*     firstModule;
	struct Archive* next;
}Archive;

/* file.c */
extern File*  FileOpen   (IN char* name);
extern File*  FileCreate (IN char* name);
extern size_t FileRead   (IN File* file, IN size_t size, OUT void* data);
extern size_t FileWrite  (IN File* file, IN size_t size, IN void* data);
extern int    FileSeek   (IN File* file, IN long offset);
extern int    FileTell   (IN File* file);
extern void   FileClose  (IN File* file);
extern size_t FileSize   (IN File* file);
extern time_t FileModifyTime (IN File* file);
extern BOOL   FileExists (IN char* filename);
extern BOOL   FileRename (IN char* from, IN char* to);
extern BOOL   FileRemove (IN char* file);
extern File*  FileCreateTemp (IN char* base);

/* object.c */
extern StringList* ExtractSymbols(IN File* object, IN int seekBase);

/* list.c */
extern char*       NewString      (IN char* s, IN unsigned long len);
extern StringList* FindString     (IN StringList* list, IN char* str);
extern size_t      GetStringCount (IN StringList* list);
extern StringList* NewStringList  (void);
extern void        AddString      (IN StringList* list, IN char* str, IN BOOL dup);

/* main.c */
extern void Error (IN char* s, ...);

/* archive.c */
extern BOOL     IsArchive      (IN char* fname);
extern Archive* NewArchive     (IN File* file);
extern BOOL     ExtractModule  (IN Module* module, OUT File* outfile);
extern Module*  AddModule      (IN Archive* archive, IN char* name);
extern Module*  FindModule     (IN Archive* archive, IN char* name);
extern void     FreeModule     (IN Archive* archive, IN char* name);
extern size_t   GetModuleCount (IN Archive* archive);
extern Archive* LoadArchive    (IN char* name);
extern int      WriteArchive   (IN Archive* archive, IN char* outfile);
extern void     CloseArchive   (IN Archive* archive);
