/************************************************************************
*
*	file.c
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nlib.h"
#include <malloc.h>
#include <sys/stat.h>

PUBLIC File* FileCreateTemp(IN char* base) {

	char*  tempname;
	File*  file;

	tempname = _tempnam(NULL, base);
	file = (File*) ALLOC_OBJ(File);
	file->stream = fopen(tempname, "wb");
	file->name = _strdup(tempname);
	file->size = 0;
	return file;
}

PUBLIC BOOL FileRename(IN char* from, IN char* to) {

	if (!rename(from, to))
		return TRUE;
	return FALSE;
}

PUBLIC BOOL FileRemove(IN char* file) {

	if (!remove(file))
		return TRUE;
	return FALSE;
}

PUBLIC File* FileCreate(IN char* name) {

	File*    file;
	FILE*    stream;

	if (!name) return NULL;

	stream = fopen(name, "wb");
	if (!stream)
		return NULL;

	file = (File*) ALLOC_OBJ(File);
	file->stream = stream;
	file->name = name;
	file->size = 0;
	return file;
}

PUBLIC File* FileOpen(IN char* name) {

	File*    file;
	FILE*    stream;
	uint32_t size;

	if (!name) return NULL;

	stream = fopen(name, "rb");
	if (!stream)
		return NULL;

	fseek(stream, 0, SEEK_END);
	size = ftell(stream);
	rewind(stream);

	file = (File*) ALLOC_OBJ(File);
	file->stream = stream;
	file->name = name;
	file->size = size;
	return file;
}

PUBLIC size_t FileRead(IN File* file, IN size_t size, OUT void* data) {

	return fread(data, 1, size, file->stream);
}

PUBLIC size_t FileWrite(IN File* file, IN size_t size, IN void* data) {

	return fwrite(data, 1, size, file->stream);
}

PUBLIC int FileSeek(IN File* file, IN long offset) {

	return fseek(file->stream, offset, SEEK_SET);
}

PUBLIC int FileTell(IN File* file) {

	return ftell(file->stream);
}

PUBLIC void FileClose(IN File* file) {

	fclose(file->stream);
	file->stream = NULL;
	file->size = 0;
	file->name = NULL;
}

PUBLIC size_t FileSize(IN File* file) {

	size_t size;
	fseek(file->stream, 0, SEEK_END);
	size = ftell(file->stream);
	fseek(file->stream, 0, SEEK_SET);
	return size;
}

PUBLIC time_t FileModifyTime(IN File* file) {

	struct stat st;

	if (stat(file->name, &st) == -1)
		return (time_t)0L;

	return st.st_mtime;
}

PUBLIC BOOL FileExists(IN char* filename) {

	struct stat st;
	if (stat(filename, &st) == -1)
		return FALSE;
	return TRUE;
}
