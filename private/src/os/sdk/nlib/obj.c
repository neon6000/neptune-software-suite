/************************************************************************
*
*	obj.c - PE/COFF object file
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nlib.h"
#include <string.h>
#include <malloc.h>

typedef struct CoffFileHeader {
	uint16_t machine;
	uint16_t numberOfSections;
	uint32_t timeDateStamp;
	uint32_t pointerToSymbolTable;
	uint32_t numberOfSymbols;
	uint16_t sizeOfOptionalHeader;
	uint16_t characteristics;
}CoffFileHeader;

typedef struct CoffSectionHeader {
	uint8_t name[8];
	uint32_t virtualSize;
	uint32_t virtualAddress;
	uint32_t sizeOfRawData;
	uint32_t pointerToRawData;
	uint32_t pointerToRelocations;
	uint32_t pointerToLineNumbers;
	uint16_t numberOfRelocations;
	uint16_t numberOfLineNumbers;
	uint32_t characteristics;
}CoffSectionHeader;

/* symbol number values. */
#define IMAGE_SYM_UNDEFINED 0
#define IMAGE_SYM_ABSOLUTE -1
#define IMAGE_SYM_DEBUG -2

/* symbol data types. */
#define IMAGE_DT_FUNCTION 0x20

/* storage class values. */
#define IMAGE_SYM_CLASS_END_OF_FUNCTION -1
#define IMAGE_SYM_CLASS_NULL 0
#define IMAGE_SYM_CLASS_AUTOMATIC 1
#define IMAGE_SYM_CLASS_EXTERNAL 2
#define IMAGE_SYM_CLASS_STATIC 3
#define IMAGE_SYM_CLASS_REGISTER 4
#define IMAGE_SYM_CLASS_EXTERNAL_DEF 5
#define IMAGE_SYM_CLASS_LABEL 6
#define IMAGE_SYM_CLASS_UNDEFINED_LABEL 7
#define IMAGE_SYM_CLASS_MEMBER_OF_STRUCT 8
#define IMAGE_SYM_CLASS_ARGUMENT 9
#define IMAGE_SYM_CLASS_STRUCT_TAG 10
#define IMAGE_SYM_CLASS_MEMBER_OF_UNION 11
#define IMAGE_SYM_CLASS_UNION_TAG 12
#define IMAGE_SYM_CLASS_TYPE_DEFINITION 13
#define IMAGE_SYM_CLASS_UNDEFINED_STATIC 14
#define IMAGE_SYM_CLASS_ENUM_TAG 15
#define IMAGE_SYM_CLASS_MEMBER_OF_ENUM 16
#define IMAGE_SYM_CLASS_REGISTER_PARAM 17
#define IMAGE_SYM_CLASS_BIT_FIELD 18
#define IMAGE_SYM_CLASS_BLOCK 100
#define IMAGE_SYM_CLASS_FUNCTION 101
#define IMAGE_SYM_CLASS_END_OF_STRUCT 102
#define IMAGE_SYM_CLASS_FILE 103
#define IMAGE_SYM_CLASS_SECTION 104
#define IMAGE_SYM_CLASS_WEAK_EXTERNAL 105

typedef struct SymbolOffset {
	unsigned long zero;
	unsigned long offset;
}SymbolOffset;

typedef struct Symbol {
	union {
		char shortname[8];
		SymbolOffset e;
	}u;
	int32_t value;
	int16_t section;
	int16_t type;
	int8_t storageClass;
	int8_t numAuxSymbols;
}Symbol;

#define IMAGE_FILE_MACHINE_I386 0x14c
#define IMAGE_FILE_MACHINE_IA64 0x200

// given an object file, return a list of EXTERN symbol names:
PUBLIC StringList* ExtractSymbols (IN File* object, IN int seekBase) {

	CoffFileHeader fh;
	Symbol         symbol;
	StringList*    symbols;
	uint32_t       numSymbols;
	char*          stringTable;
	uint32_t       currentSymbol;
	uint32_t       stringTableSize;

	stringTableSize = 0;
	stringTable = NULL;

	// get number of symbols:

	FileRead(object, sizeof(CoffFileHeader), &fh);
	numSymbols = fh.numberOfSymbols;

	if (!numSymbols)
		return NULL;

//	if (fh.machine != IMAGE_FILE_MACHINE_I386 || fh.machine != IMAGE_FILE_MACHINE_IA64)
//		return NULL;

	// read in string table. String table is located right after
	// symbol table:

	FileSeek(object, fh.pointerToSymbolTable + numSymbols * sizeof(Symbol) + seekBase);
	FileRead(object, sizeof(uint32_t), &stringTableSize);
	if (stringTableSize != 0) {
		// "stringTableSize" is size of symbol table + size of itself in the file.
		// so we subtract its size here:
		stringTableSize -= 4;
		stringTable = malloc(stringTableSize);
		FileRead(object, stringTableSize, stringTable);
	}

	// read symbols:

	symbols = NewStringList();
	FileSeek(object, fh.pointerToSymbolTable + seekBase);
	for (currentSymbol = 0; currentSymbol < numSymbols; currentSymbol++) {

		int currentAux;

		// read symbol:

		FileRead(object, sizeof(Symbol), &symbol);

		// only add symbols that are EXTERNAL and either VALUE or a valid SECTION number:

		if (symbol.storageClass == IMAGE_SYM_CLASS_EXTERNAL && (symbol.value || symbol.section)) {
			if (symbol.u.e.zero == 0) {
				char* realname = stringTable + symbol.u.e.offset - 4;
				AddString(symbols, _strdup(realname), TRUE);
			}
			else{
				char realname[9];
				memcpy(realname, symbol.u.shortname, 8);
				realname[8] = 0;
				AddString(symbols, _strdup(realname), TRUE);
			}
		}

		// skip over any auxiliary symbol enteries:

		for (currentAux = 0; currentAux < symbol.numAuxSymbols; currentAux++) {
			Symbol aux;
			FileRead(object, sizeof(Symbol), &aux);
			currentSymbol++;
		}
	}
	return symbols;
}
