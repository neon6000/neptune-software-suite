/************************************************************************
*
*	pproc.c - Preprocessor
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <malloc.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "nmake.h"

PRIVATE FileManager* _fm;

typedef enum IfFlags {
	IfElse    = 1, // set if if/ifdef, clear for else
	Condition = 2, // set if condition is true, clear if not
	Ignore    = 4, // set if if/endif block is to be skipped
	ElseIf    = 8  // set for else if/ifdef etc, clear for else
}IfFlags;

#define IF_DEPTH 30

// stores the stack of current if directives:
PRIVATE IfFlags _ifStack[IF_DEPTH];

// stores top of stack:
PRIVATE int _ifStackTop;

PRIVATE void PushIf(IN IfFlags dir) {

	if (_ifStackTop == IF_DEPTH)
		MakeError("!if depth cannot exceed 30");
	_ifStack[_ifStackTop++] = dir;
}

PRIVATE void PopIf(void) {

	assert(_ifStackTop > 0);
	--_ifStackTop;
}

PRIVATE IfFlags GetIf(void) {

	return _ifStack[_ifStackTop - 1];
}

PRIVATE BOOL InIf(void) {

	return _ifStackTop != 0;
}

PRIVATE BOOL IsSpace(IN int c) {

	return c == ' ' || c == '\t';
}

PRIVATE void SkipWhitespace(void) {

	int c = _fm->getc();
	if (!isspace(c)) {
		_fm->ungetc(c);
		return;
	}
	while (c != EOF && c != '\n' && IsSpace(c))
		c = _fm->getc();
	_fm->ungetc(c);
}

PRIVATE void ProcessError(void) {

	char* s;
	char* expanded;

	if (InIf() && ON(GetIf(), Ignore))
		return;

	SkipWhitespace();
	s = _fm->getline(0);
	expanded = Expand(s, FALSE);

	MakeError("%s", expanded);
	free(s);
	free(expanded);
}

PRIVATE void ProcessMessage(void) {

	char* s;
	char* expanded;

	if (InIf() && ON(GetIf(), Ignore))
		return;

	SkipWhitespace();
	s = _fm->getline(0);
	expanded = Expand(s, FALSE);

	MakeInfo("%s", expanded);
	free(s);
	free(expanded);
}

PRIVATE void ProcessInclude(void) {

	char* s;
	File* file;
	char* expanded;

	if (InIf() && ON(GetIf(), Ignore))
		return;

	SkipWhitespace();
	s = _fm->getline(0);
	expanded = Expand(s, FALSE);

	file = _fm->open(expanded);
	if (!file)
		MakeError("Unable to open include file '%s'", expanded);
	_fm->push(file);

	if (ON(_globalFlags, DISPLAY_INCLUDES))
		MakeInfo("Include file %s", expanded);

	free(expanded);
	free(s);
	// do not free value.data -- this is referenced by File->fname.
}

PRIVATE void ProcessUndef(void) {

	char* s;
	Macro* macro;

	if (InIf() && ON(GetIf(), Ignore))
		return;

	SkipWhitespace();
	s = _fm->getline(0);

	macro = GetMacro(s);
	free(s);
}

typedef enum PpTokenType {

	/* tokens */
	String,          // " <n-characters> "
	Number,          // <integer>
	LeftParen,       // )
	RightParen,      // (

	/* operators */
	Defined,         // DEFINED ( <macro_name> )
	Exist,           // EXIST (<file_path>
	Not,             // !
	OneComplement,   // ~
	Mul,             // *
	Divide,          // /
	Mod,             // %
	Plus,            // +
	Minus,           // -
	ShiftLeft,       // <<
	ShiftRight,      // >>
	LessOrEqual,     // <=
	GreaterOrEqual,  // >=
	LessThan,        // <
	GreaterThan,      // >
	Equality,        // ==
	Inequality,      // !=
	BitwiseAnd,      // &
	BitwiseXor,      // ^
	BitwiseOr,       // |
	LogicalAnd,      // &&
	LogicalOr,       // ||
	InvalidOp
}PpTokenType;

typedef struct PpToken {
	PpTokenType type;
	File* file;
	int line;
	char* s;
	int num;
}PpToken;

PRIVATE char* SkipStringWhitespace(IN char* s) {

	while(*s && (*s == ' ' || *s == '\t'))
		s++;
	return s;
}

PRIVATE PpTokenType GetOp(IN char* p) {

	PpTokenType tok = InvalidOp;
	switch (*p) {
	case '\0':
		break;
	case '(':
		tok = LeftParen;
		break;
	case ')':
		tok = RightParen;
		break;
	case '!':
		if (*(p + 1) == '=')
			tok = Inequality;
		else
			tok = Not;
		break;
	case '~':
		tok = OneComplement;
		break;
	case '*':
		tok = Mul;
		break;
	case '/':
		tok = Divide;
		break;
	case '%':
		tok = Mod;
		break;
	case '+':
		tok = Plus;
		break;
	case '-':
		tok = Minus;
		break;
	case '<':
		if (*(p + 1) == '<')
			tok = ShiftLeft;
		else if (*(p + 1) == '=')
			tok = LessOrEqual;
		else
			tok = LessThan;
		break;
	case '>':
		if (*(p + 1) == '>')
			tok = ShiftRight;
		else if (*(p + 1) == '=')
			tok = GreaterOrEqual;
		else
			tok = GreaterThan;
		break;
	case '=':
		if (*(p + 1) == '=')
			tok = Equality;
		else
			tok = InvalidOp;
		break;
	case '^':
		tok = BitwiseXor;
		break;
	case '&':
		if (*(p + 1) == '&')
			tok = LogicalAnd;
		else
			tok = BitwiseAnd;
		break;
	case '|':
		if (*(p + 1) == '|')
			tok = LogicalOr;
		else
			tok = BitwiseOr;
		break;
	};
	return tok;
}

// given token "s" return the token type:
PRIVATE PpTokenType GetTokenType(IN char* s) {

	PpTokenType tok = InvalidOp;
	char temp[7]; // largest token is "defined"

	if (isdigit(*s)) {
		tok = Number;
	}
	else if (*s == '\"' || *s == '\'')
		tok = String;
	else if (isalpha(*s)) {
		if (strlen(s) >= 7) {
			memcpy(temp, s, 7);
			for (int i = 0; i < 7; i++) {
				if (isupper(temp[i]))
					tolower(temp[i]);
			}
			if (!memcmp(temp, "defined", 7))
				tok = Defined;
		}
		if (strlen(s) >= 5) {
			memcpy(temp, s, 5);
			for (int i = 0; i < 5; i++) {
				if (isupper(temp[i]))
					tolower(temp[i]);
			}
			if (!memcmp(temp, "exist", 5))
				tok = Exist;
		}

		if (tok != Defined && tok != Exist)
			tok = String;
	}
	if (tok == InvalidOp)
		tok = GetOp(s);
	return tok;
}

// given a pointer to *s, return the next Preprocessor token
// and update "*s" to the size of the token. If there is no more
// input or the token is invalid, return InvalidOp token type:
PRIVATE PpToken _GetToken(IN OUT char** s) {

	char* p;
	int size;
	PpToken tok;

	p = SkipStringWhitespace(*s);
	tok.type = GetTokenType(p);
	tok.s = NULL;
	tok.num = 0;
	tok.file = _fm->top();
	tok.line = _fm->top()->loc.y;
	size = 0;

	switch (tok.type) {
		case String:
			if (*p == '\"' || *p == '\'') {
				char delim = *p;
				for (size = 1; p[size] && p[size] != delim; size++)
					;
				if (p[size] != delim) {
					MakeError("%z: Missing %c", _fm->top(), delim);
				}
				tok.s = realloc(NULL, size);
				memcpy(tok.s, p+1, size-1);
				tok.s[size-1] = '\0';
				size++;
			}
			else {
				for (size = 0; isalpha(p[size]) || p[size] == '_'; size++)
					;
				tok.s = realloc(NULL, size + 1);
				memcpy(tok.s, p, size);
				tok.s[size] = '\0';
			}
			break;
		case Number:
			for (size = 0; isdigit(p[size]); size++)
				;
			tok.s = realloc(NULL, size+1);
			memcpy(tok.s, p, size);
			tok.s[size] = '\0';
			tok.num = strtol(tok.s, NULL, 0);
			break;
		case Defined:
			size = 7;
			break;
		case Exist:
			size = 5;
			break;
		case ShiftLeft:
		case ShiftRight:
		case LessOrEqual:
		case GreaterOrEqual:
		case Equality:
		case Inequality:
		case LogicalAnd:
		case LogicalOr:
			size = 2;
			break;
		default:
			size = 1;
	};

	// p - *s is number of whitespace characters skipped:
	size += (p - *s);
	*s += size;
	return tok;
}

PpToken _current;
PpToken _pushback[5];
unsigned int _pushbackTop;

void UngetTok(IN PpToken tok) {

	if (_pushbackTop >= 5)
		__debugbreak();
	_pushback[_pushbackTop++] = tok;
}

PpToken GetTok(IN char** s) {

	if (_pushbackTop > 0)
		return _pushback[--_pushbackTop];
	_current = _GetToken(s);
	return _current;
}

PpToken CurrentTok(void) {

	return _current;
}

PpToken Calc(IN PpToken l, IN PpToken r, IN PpTokenType op) {

	switch (op) {
	case Mul:              l.num *= r.num; break;
	case Divide:           l.num /= r.num; break;
	case Mod:              l.num %= r.num; break;
	case Plus:             l.num += r.num; break;
	case Minus:            l.num -= r.num; break;
	case ShiftLeft:        l.num <<= r.num; break;
	case ShiftRight:       l.num >>= r.num; break;
	case BitwiseAnd:       l.num &= r.num; break;
	case BitwiseXor:       l.num ^= r.num; break;
	case BitwiseOr:        l.num |= r.num; break;
	case Not:              l.num = !l.num; break;
	case OneComplement:    l.num = ~l.num; break;
	case LessOrEqual:      l.num = l.num <= r.num; break;
	case GreaterOrEqual:   l.num = l.num >= r.num; break;
	case LessThan:         l.num = l.num < r.num; break;
	case GreaterThan:      l.num = l.num > r.num; break;
	case Equality:    {
		if (l.type == String) {
			if (r.type != String) {
				MakeError("%z: Incompatible types", l.file);
			}
			if (strlen(l.s) != strlen(r.s))
				l.num = FALSE;
			else {
				if (strcmp(l.s, r.s))
					l.num = FALSE;
				else
					l.num = TRUE;
			}
		}
		else if (l.type == Number)
			l.num = (l.num == r.num);
		else {
			MakeError("%z: Unknown token in expression", l.file);
		}
		break;
	}
	case Inequality:
		if (l.type == String) {
			if (r.type != String) {
				MakeError("%z: Incompativle types", l.file);
			}
			if (strlen(l.s) != strlen(r.s))
				l.num = TRUE;
			else{
				if (strcmp(l.s, r.s))
					l.num = TRUE;
				else
					l.num = FALSE;
			}
		}
		else if (l.type == Number)
			l.num = (l.num != r.num);
		else {
			MakeError("%z: Unknown token in expression", l.file);
		}
		break;
	case LogicalAnd:       l.num = l.num && r.num; break;
	case LogicalOr:        l.num = l.num || r.num; break;
	default: __debugbreak();
	};
	return l;
}

PpToken LogicalOrOp(IN char** s);

PpToken BaseExpr(IN char** s) {

	// ( <expr> )
	if (CurrentTok().type == LeftParen) {
		PpToken tok;
		GetTok(s);
		tok = LogicalOrOp(s);
		if (CurrentTok().type != RightParen)
			MakeError("%z: Missing ')'", _fm->top());
		GetTok(s);
		return tok;
	}
	else {
		PpToken tok = CurrentTok();
		if (tok.type == InvalidOp)
			MakeError("%z: Invalid expression", tok.file);
		GetTok(s);
		return tok;
	}
}

PpToken SpecialOp(IN char** s) {

	// EXIST or DEFINED
	PpToken op = CurrentTok();

	if (op.type == Exist) {
		MakeError("EXISTS operator not yet supported. Use !ifdef instead");
		return op;
	}
	else if (op.type == Defined) {
		MakeError("DEFINED operator not yet supported. Use !ifdef instead.");
		return op;
	}
	else
		return BaseExpr(s);
}

PpToken UnaryOp(IN char** s) {

	// ! ~ -
	PpToken op = CurrentTok();
	PpToken zero = { Number, NULL, 0 };
	if (op.type == Not || op.type == OneComplement) {
		GetTok(s);
		return Calc(SpecialOp(s), zero, op.type);
	}
	else if (op.type == Minus) {
		GetTok(s);
		return Calc(zero, SpecialOp(s), op.type);
	}
	else
		return SpecialOp(s);
}

PpToken MultOrDivOp(IN char** s) {

	// * / %
	PpToken left = UnaryOp(s);
	PpToken t = CurrentTok();
	while (t.type == Mul || t.type == Divide || t.type == Mod) {
		GetTok(s);
		left = Calc(left, UnaryOp(s), t.type);
		t = CurrentTok();
	}
	return left;
}

PpToken PlusOrMinusOp(IN char** s) {

	// + -
	PpToken left = MultOrDivOp(s);
	PpToken t = CurrentTok();
	while (t.type == Plus || t.type == Minus) {
		GetTok(s);
		left = Calc(left, MultOrDivOp(s), t.type);
		t = CurrentTok();
	}
	return left;
}

PpToken BitwiseShiftOp(IN char** s) {

	// << >>
	PpToken left = PlusOrMinusOp(s);
	PpToken t = CurrentTok();
	while (t.type == ShiftLeft || t.type == ShiftRight) {
		GetTok(s);
		left = Calc(left, PlusOrMinusOp(s), t.type);
		t = CurrentTok();
	}
	return left;
}

PpToken ComparisionOp(IN char** s) {

	// <= >= < >
	PpToken left = BitwiseShiftOp(s);
	PpToken t = CurrentTok();
	while (t.type == LessOrEqual || t.type == GreaterOrEqual || t.type == LessThan || t.type == GreaterThan) {
		GetTok(s);
		left = Calc(left, BitwiseShiftOp(s), t.type);
		t = CurrentTok();
	}
	return left;
}

PpToken EqualityOp(IN char** s) {

	// == or !=
	PpToken left = ComparisionOp(s);
	PpToken t = CurrentTok();
	while (t.type == Equality || t.type == Inequality) {
		GetTok(s);
		left = Calc(left, ComparisionOp(s), t.type);
		t = CurrentTok();
	}
	return left;
}

PpToken BitwiseOp(IN char** s) {

	// & ^ |
	PpToken left = EqualityOp(s);
	PpToken t = CurrentTok();
	while (t.type == BitwiseAnd || t.type == BitwiseOr || t.type == BitwiseXor) {
		GetTok(s);
		left = Calc(left, EqualityOp(s), t.type);
		t = CurrentTok();
	}
	return left;
}

PpToken LogicalAndOp(IN char** s) {

	// &&
	PpToken left = BitwiseOp(s);
	PpToken t = CurrentTok();
	while (t.type == LogicalAnd) {
		GetTok(s);
		left = Calc(left, BitwiseOp(s), t.type);
		t = CurrentTok();
	}
	return left;
}

PpToken LogicalOrOp(IN char** s) {

	// ||
	PpToken left = LogicalAndOp(s);
	PpToken t = CurrentTok();
	while (t.type == LogicalOr) {
		GetTok(s);
		left = Calc(left, LogicalAndOp(s), t.type);
		t = CurrentTok();
	}
	return left;
}

// given a string representing an expression in "str",
// scan the string and evaluate it:
PRIVATE BOOL Eval(IN char* str) {

	char* s = str;
	PpToken result;

	GetTok(&s);
	result = LogicalOrOp(&s);

	if (result.num == 0)
		return FALSE;
	return TRUE;
}

// "if [condition]"
PRIVATE void ProcessIf(void) {

	char* s;
	IfFlags flags;
	char* expanded;

	SkipWhitespace();
	s = _fm->getline(0);
	expanded = Expand(s, FALSE);

	flags = 0;
	if (InIf() && ON(GetIf(), Ignore))
		SET(flags, Ignore);
	SET(flags, IfElse);
	if (Eval(expanded))
		SET(flags, Condition);
	else
		SET(flags, Ignore);

	PushIf(flags);
	free(s);
	free(expanded);
}

// "elseif [condition]" or "else if [condition]"
PRIVATE void ProcessElseIf(void) {

	char* s;
	IfFlags flags;
	char* expanded;

	if (!InIf())
		MakeError("%z: !elseif with no matching !if", _fm->top());

	PopIf();

	SkipWhitespace();
	s = _fm->getline(0);
	expanded = Expand(s, FALSE);

	flags = 0;
	if (InIf() && ON(GetIf(), Ignore))
		SET(flags, Ignore);
	SET(flags, ElseIf);
	if (Eval(expanded))
		SET(flags, Condition);
	else
		SET(flags, Ignore);

	PushIf(flags);
	free(s);
	free(expanded);
}

PRIVATE void ProcessIfdef(void) {

	char* s;
	Macro* macro;
	IfFlags flags;

	flags = 0;
	SkipWhitespace();
	s = _fm->getline(0);
	macro = GetMacro(s);

	if (macro) {
		SET(flags, IfElse);
		SET(flags, Condition);
		if (InIf() && ON(GetIf(), Ignore))
			SET(flags, Ignore);
		PushIf(flags);
	}
	else {
		SET(flags, IfElse);
		SET(flags, Ignore);
		PushIf(flags);
	}
	free(s);
}

PRIVATE void ProcessIfndef(void) {

	char* s;
	Macro* macro;
	IfFlags flags;

	flags = 0;
	SkipWhitespace();
	s = _fm->getline(0);

	macro = GetMacro(s);
	if (macro) {
		SET(flags, IfElse);
		SET(flags, Ignore);
		PushIf(flags);
	}
	else {
		SET(flags, IfElse);
		SET(flags, Condition);
		if (InIf() && ON(GetIf(), Ignore))
			SET(flags, Ignore);
		PushIf(flags);
	}
	free(s);
}

PRIVATE void ProcessElseIfdef(void) {

	char* s;
	Macro* macro;
	IfFlags flags;
	File* f = _fm->top();

	if (!InIf())
		MakeError("%z: !elseifdef without matching !if", f);

	SkipWhitespace();
	s = _fm->getline(0);
	macro = GetMacro(s);

	flags = GetIf();		
	PopIf();

	CLEAR(flags, IfElse);
	SET(flags, ElseIf);

	if (macro) {
		SET(flags, Condition);
		if (InIf() && ON(GetIf(), Ignore))
			SET(flags,Ignore);
		PushIf(flags);
	}
	else {
		CLEAR(flags, Condition);
		SET(flags, Ignore);
	}
	free(s);
}

PRIVATE void ProcessElse(void) {

	char* s;
	IfFlags flags;
	File* f = _fm->top();

	s = _fm->getline(0);

	if (!InIf())
		MakeError("%z: !else without matching !if", f);

	flags = GetIf();
	PopIf();
	CLEAR(flags, ElseIf|IfElse);
	FLIP(flags, Condition);
	FLIP(flags, Ignore);
	PushIf(flags);
	free(s);
}

PRIVATE void ProcessEndif(void) {

	char* s;
	File* f = _fm->top();
	s = _fm->getline(0);
	
	if(!InIf())
		MakeError("%z: !endif without matching !if", f);

	PopIf();
	free(s);
}

PRIVATE void ProcessInvalid(void) {

	char* s;
	File* f = _fm->top();
	MakeInfo("%z: Invalid directive, ignored", _fm->top());
	s = _fm->getline(0);
	free(s);
}

PRIVATE char* ScanDirective(void) {

	int c;
	int i;
	char* name;

	name = realloc(NULL, 32);
	int s = 1;
	i = 0;

	SkipWhitespace();
	c = _fm->getc();

	while (c != EOF && c != '\n' && isalpha(c)) {
		name[i++] = tolower(c);
		c = _fm->getc();
		if ((i % 31) == 0)
			name = realloc(name, 32 * ++s);
	}

	_fm->ungetc(c);
	name[i] = '\0';
	return name;
}

PRIVATE void ProcessDirective(void) {

	char* name;

	name = ScanDirective();
	if (!strcmp(name, "error"))           ProcessError();
	else if (!strcmp(name, "message"))    ProcessMessage();
	else if (!strcmp(name, "include"))    ProcessInclude();
	else if (!strcmp(name, "if"))         ProcessIf();
	else if (!strcmp(name, "ifdef"))      ProcessIfdef();
	else if (!strcmp(name, "ifndef"))     ProcessIfndef();
	else if (!strcmp(name, "elseif"))     ProcessElseIf();
	else if (!strcmp(name, "else if"))    ProcessElseIf();
	else if (!strcmp(name, "else"))       ProcessElse();
	else if (!strcmp(name, "elseifdef"))  ProcessElseIfdef();
	else if (!strcmp(name, "else ifdef")) ProcessElseIfdef();
	else if (!strcmp(name, "endif"))      ProcessEndif();
	else if (!strcmp(name, "undef"))      ProcessUndef();
	else ProcessInvalid();

	free(name);
}

PRIVATE void SkipToNextDirective(void) {

	File* f;

	f = _fm->top();
	while (TRUE) {
		if (!_fm->top() || (_fm->top() != f))
			MakeError("%z: Premature EOF (did you forget !endif?)", f);
		if (f->firstColumn && _fm->peek(1) == '!')
			break;
		_fm->getc();
	}
}

PRIVATE int Getc(void) {

	File* f;
	int i;

	while (InIf() && ON(GetIf(), Ignore)) {
		SkipToNextDirective();
		_fm->getc(); // consume '!'
		ProcessDirective();
	}

	f = _fm->top();
	i = _fm->getc();

	if (i == EOF)
		return EOF;

	if (f->firstChar && i == '!') {
		ProcessDirective();
		return '\n';
	}

	if (f->firstColumn && _fm->peek(1) == '!') {
		_fm->getc(); // consume '!'
		ProcessDirective();
		return '\n';
	}

	return i;
}

PRIVATE Ungetc(IN int c) {

	_fm->ungetc(c);
}

PRIVATE int Peek(IN int ahead) {

	return _fm->peek(ahead);
}

PRIVATE BOOL Init(IN FileManager* fm) {

	_fm = fm;
	return TRUE;
}

PRIVATE char* GetLine(IN int delim) {

	return _fm->getline(delim);
}

PRIVATE void Cleanup(void) {

}

PRIVATE Preprocessor _pproc = { Init, Getc, Ungetc, Peek, GetLine, Cleanup };

PUBLIC Preprocessor* GetPreprocessor(void) {
	return &_pproc;
}
