/************************************************************************
*
*	file.c - File management
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <malloc.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <sys/stat.h>
#include <direct.h>
#include <io.h>
#include "nmake.h"

/* PRIVATE Definitions ***********************************/

#define MAX_FILE_STACK 20

File* _stack[MAX_FILE_STACK];
int   _stacktop;

/**
*	FindFirstFile
*
*	Description : Returns the first file that matches "fname".
*
*	Input : fname - Pointer to string representing the file path and name.
*	                The string may contain optional wildcards (? and *).
*	        data -  Pointer to file data structure for FindFirstFile to
*	                store the file information.
*	        handle - FindFirstFile writes a handle to this that may be used
*	                 with FindNextFile.
*
*	Output : Pointer to expanded file name of the first matching file. This function
*	         also writes the file data block to "data" and writes a handle to "handle"
*	         that can be used with FindNextFile. Returns NULL if no file found.
*/
PRIVATE char* FindFirstFile(IN char* fname, IN OUT struct _finddata_t* data, IN OUT intptr_t* handle) {

	BOOL wildcards = FALSE;
	for (char* s = fname; *s; s++) {
		if (strrchr("?*", *s)) {
			wildcards = TRUE;
			break;
		}
	}
	*handle = _findfirst(fname, data);
	if (*handle == 0xffffffff) {
		*handle = (intptr_t) NULL;
		return NULL;
	}
	if (!wildcards) {
		_findclose(*handle);
		*handle = (intptr_t) NULL;
	}
	return data->name;
}

/**
*	FindNextFile
*
*	Description : Returns the next file that matches "fname".
*
*	Input : fname - Pointer to string representing the file path and name.
*	                The string may contain optional wildcards (? and *).
*	        data -  Pointer to file data structure for FindNextFile to
*	                store the file information.
*	        handle - Pointer to handle that was previously set by FindFirstFile.
*
*	Output : Pointer to expanded file name of the next matching file. This function
*	         also writes the file data block to "data" and writes a handle to "handle"
*	         that can be used with another call to FindNextFile. Returns NULL if no more
*	         files.
*/
PRIVATE char* FindNextFile(IN char* fname, IN OUT struct _finddata_t* data, IN OUT intptr_t* handle) {

	if (*handle == (intptr_t) NULL)
		return NULL;
	if (_findnext(*handle, data)) {
		_findclose(*handle);
		*handle = (intptr_t) NULL;
		return NULL;
	}
	return data->name;
}

/**
*	ExpandWildcards
*
*	Description : Given a file path and name, return a string list
*	of all matching files while expnding wildcard characters (? and *).
*
*	Input : fname - Pointer to string representing the file path and name.
*	                The string may contain optional wildcards (? and *).
*
*	Output : Pointer to string list of matching files.
*/
PRIVATE StringList* ExpandWildcards(IN char* fname) {

	struct _finddata_t data;
	StringList* matches;
	intptr_t handle;
	char* name;

	name = FindFirstFile(fname, &data, &handle);
	if (!name)
		return NULL;

	matches = NewStringList();
	for (; name; name = FindNextFile(fname, &data, &handle))
		AddString(matches, _strdup(name));
	return matches;
}

/**
*	CurrentDirectory
*
*	Description : Return current working directory.
*
*	Output : Pointer to current working directory string.
*/
PRIVATE char* CurrentDirectory(void) {

	char dir[256];
	char* s;

	s = _getcwd(dir, sizeof(dir));
	if (!s)
		return NULL;
	return _strdup(s);
}

/**
*	OpenFile
*
*	Description : Open specified file.
*
*	Input : fname - Pointer to file path and name.
*
*	Output : Pointer to file object or NULL if not found.
*/
PRIVATE File* OpenFile(IN char* fname) {

	FILE* stream;
	File* file;

	if (!fname)
		return NULL;
	stream = fopen(fname, "r");
	if (!stream)
		return NULL;
	file = ALLOC_OBJ(File);
	if (!file) {
		fclose(stream);
		return NULL;
	}
	memset(file->pushback, 0, FILE_PUSHBACK_MAX);
	file->pushbackTop = 0;
	file->firstColumn = TRUE;
	file->fname = fname;
	file->loc.src = file;
	file->loc.x = 1;
	file->loc.y = 1;
	file->stream = (struct FILE*) stream;
	file->eof = FALSE;
	file->firstChar = TRUE;
	return file;
}

/**
*	CloseFile
*
*	Description : Close specified file.
*
*	Input : fname - Pointer to file object.
*/
PRIVATE void CloseFile(IN File* file) {

	if (!file)
		return;
	if (file->stream)
		fclose((FILE*) file->stream);
	FREE(file);
}

/**
*	PushFile
*
*	Description : Push file object to file stack.
*
*	Input : fname - Pointer to file object.
*/
PRIVATE void PushFile(IN File* file) {

	if (_stacktop == MAX_FILE_STACK-1) {
		MakeError("File depth exceeds limit. Not able to include %s", file->fname);
		return;
	}
	_stack[_stacktop++] = file;
}

/**
*	PopFile
*
*	Description : Pop file object from stack.
*
*	Output : Pointer to file object or NULL if empty.
*/
PRIVATE File* PopFile(void) {

	if (_stacktop == 0)
		return NULL;
	return _stack[--_stacktop];
}

/**
*	TopFile
*
*	Description : Return pointer to file object on top of stack.
*
*	Output : Pointer to file object.
*/
PRIVATE File* TopFile(void) {

	return _stack[_stacktop - 1];
}

PRIVATE void FileUngetc(IN int c);

/**
*	FileGetc
*
*	Description : Get next character from file.
*
*	1. If EOF is reached, POP next file off stack and try again.
*	2. If no more files, return EOF.
*	3. If file pushback buffer is not empty, pop next character from pushback buffer.
*	4. Else:
*	       get next character from the file.
*          if this is the first character of the file, set file->firstChar to TRUE.
*	       (this is used by preprocessor to process directives on first line.)
*	4. If line continuation character, skip initial whitespace and return
*	   a single space character.
*	5. If CR, return LF.
*	6. If CRLF, ignore the CR and return LF.
*
*	Output : Next character from input sources or EOF if file stack is empty.
*/
PRIVATE int FileGetc(void) {

	File* file;
	int c;

next_character:

	file = TopFile();

	if (!file)
		return EOF;

	/* dont read past EOF */
	if (file->eof) {

		/* reached end of current file, so continue
		with next file if possible: */
		if (_stacktop > 0) {
			PopFile();
			goto next_character;
		}

		/* if no more files, return EOF. */
		return EOF;
	}

	if (file->pushbackTop > 0)
		c = file->pushback[--file->pushbackTop];
	else {
		if (ftell((FILE*)file->stream) == 0)
			file->firstChar = TRUE;
		else
			file->firstChar = FALSE;
		c = fgetc((FILE*)file->stream);
	}

	/* if eof, we are done: */
	if (c == EOF) {
		file->eof = TRUE;
		goto next_character;
	}

	/* handle line continuation character: */
	if (c == '\\') {
		int next = 0;
		if (file->pushbackTop > 0)
			next = file->pushback[--file->pushbackTop];
		else
			next = fgetc((FILE*)file->stream);
		if (next == '\n') {
			// skip initial space:
			int k=0;
			for (k = FileGetc(); isspace(k); k = FileGetc())
				;
			FileUngetc(k);
			return ' ';
		}
		FileUngetc(next);
	}

	/* translate CR -> LF and CRLF -> LF */
	if (c == '\r') {
		int next = 0;
		if (file->pushbackTop > 0)
			next = file->pushback[--file->pushbackTop];
		else
			next = fgetc((FILE*)file->stream);
		if (next != '\n')
			FileUngetc(next);
	}

	if (c == '\n') {
		file->firstColumn = TRUE;
		file->loc.y++;
	}
	else
		file->firstColumn = FALSE;
	return c;
}

/**
*	FileUngetc
*
*	Description : Put character back into file.
*
*	Input : c - Character to put back.
*/
PRIVATE void FileUngetc(IN int c) {

	File* file;

	file = TopFile();
	if (!file)
		return;

	if (file->pushbackTop == FILE_PUSHBACK_MAX)
		MakeError("%z: File pushback buffer excceded %i", file, FILE_PUSHBACK_MAX);

	file->pushback[file->pushbackTop++] = c;
	if (c == '\n') {
		file->firstColumn = TRUE;
		file->loc.y--;
	}
	else {
		file->firstColumn = FALSE;
	}
}

/**
*	FilePeek
*
*	Description : Look ahead and return the character ahead
*	of current position.
*
*	Input : ahead - Number of characters to look ahead.
*
*	Output : Character at that position.
*/
PRIVATE int FilePeek(IN int ahead) {

	int c[5];
	int k;

	assert(ahead < 5 && ahead > 0);

	for (k = 0; k < ahead; k++)
		c[k] = FileGetc();
	for (k = ahead - 1; k >= 0; k--)
		FileUngetc(c[k]);
	return c[ahead - 1];
}

/**
*	FileGetLine
*
*	Description : From the current file position, reads the
*	the rest of the line until EOL, EOF, or "delim" character,
*	whichever comes first.
*
*	Input : delim - Delimiter to use.
*
*	Output : Pointer to string containing the line.
*/
PRIVATE char* FileGetLine(IN int delim) {

	int c;
	char* s;
	int i;
	int size;

	s = realloc(NULL, 32);
	size = 1;
	i = 0;

	// skip initial space:
	for (c = FileGetc(); c != '\n' && c != delim && isspace(c); c = FileGetc())
		;

	while (c != EOF && c != '\n' && c != delim) {
		s[i++] = c;
		c = FileGetc();
		if ((i % 32) == 0)
			s = realloc(s, 32 * ++size);
	}
	FileUngetc(c);
	s[i] = '\0';
	return s;
}

/**
*	FileTouch
*
*	Description : Update the last modified date of the specified file.
*
*	Input : fname - File path and name to update.
*
*	Output : TRUE on success.
*/
PRIVATE BOOL FileTouch(IN char* fname) {

	FILE* file;
	int c;

	if (!fname)
		return FALSE;

	file = fopen(fname, "rw");
	if (!file)
		return FALSE;

	// get first character:
	c = fgetc(file);

	// watch for empty file:
	if (c == EOF) {
		fclose(file);
		return FALSE;
	}

	// now write the character back:
	fseek(file, 0, SEEK_SET);
	fputc(c, file);
	fclose(file);
	return TRUE;
}

/**
*	FileDateTime
*
*	Description : Get file date time stamp.
*
*	Input : fname - File path and name to update.
*
*	Output : File time object or 0L if not found.
*/
PRIVATE time_t FileDateTime(IN char* fname) {

	struct stat st;

	if (!fname)
		return (time_t) 0;

	if (stat(fname, &st) == -1)
		return (time_t) 0L;

	return st.st_mtime;
}

/* File manager. */
PRIVATE FileManager _fm = {
	OpenFile, CloseFile, PushFile, PopFile, TopFile, FileGetc,
	FileUngetc, FilePeek, FileGetLine, FileTouch, FileDateTime,
	FindFirstFile, FindNextFile, ExpandWildcards, CurrentDirectory
};

/* PUBLIC Definitions *****************************/

/**
*	GetFileManager
*
*	Description : Get file manager.

*	Output : Pointer to global file manager object.
*/
PUBLIC FileManager* GetFileManager(void) {

	return &_fm;
}
