
#include <Windows.h>
#include <stdio.h>

// http://pierrelib.pagesperso-orange.fr/exec_formats/MS_Symbol_Type_v1.0.pdf

typedef struct Record {
	WORD length;
	WORD index;
	char data[];
}Record;

void _readDebugInfo(void) {

	FILE* file = fopen("Debug/main.obj", "rb");
	IMAGE_FILE_HEADER header;
	IMAGE_SECTION_HEADER section[7];
	IMAGE_SECTION_HEADER* section_debug_s = &section[1];

	fread(&header, sizeof (IMAGE_FILE_HEADER), 1, file);

	for (int i = 0; i < 7 && i < header.NumberOfSections; i++)
		fread(&section[i], sizeof (IMAGE_SECTION_HEADER), 1, file);

	Record* data = malloc(section_debug_s->SizeOfRawData);

	fseek(file, section_debug_s->PointerToRawData,SEEK_SET);
	fread(data, 1, section_debug_s->SizeOfRawData, file);



//	DWORD   SizeOfRawData;
//	DWORD   PointerToRawData;
	//section_debug_s->VirtualAddress

	free(data);

	fclose(file);

	printf("\n\r--- read debug info ---");
}

