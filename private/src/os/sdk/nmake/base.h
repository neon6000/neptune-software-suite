/************************************************************************
*
*	base.h - Base types.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef BASE_H
#define BASE_H

#define IN
#define OUT
#define OPTIONAL
#define PRIVATE static
#define PUBLIC
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long uint64_t;
typedef signed char sint8_t;
typedef signed short sint16_t;
typedef signed int sint32_t;
typedef signed long long sint64_t;
typedef char int8_t;
typedef short int16_t;
typedef int int32_t;
typedef long long int64_t;
typedef int bool_t;
typedef bool_t BOOL;
#define TRUE 1
#define FALSE 0
#define INVALID_HANDLE ((unsigned int)0)
typedef unsigned int handle_t;
typedef unsigned int index_t;
typedef unsigned int offset_t;
typedef unsigned int size_t;
typedef void* addr_t;
typedef uint32_t handle_t;

typedef struct Set {
	char* s;
	struct Set* next;
}Set;

PUBLIC Set* SetInsert(IN Set* k, IN char* s);
PUBLIC BOOL SetFind(IN Set* k, IN char* s);
PUBLIC Set* SetUnion(IN Set* a, IN Set* b);
PUBLIC Set* SetIntersect(IN Set* a, IN Set* b);

typedef struct Buffer {
	size_t elementCount;
	size_t allocCount;
	uint8_t* data;
}Buffer;

PUBLIC void     BufferInit(IN Buffer* buffer);
PUBLIC Buffer*  NewBuffer(void);
PUBLIC void     BufferFree(IN Buffer* r);
PUBLIC uint8_t* GetBufferData(IN Buffer* b);
PUBLIC size_t   GetBufferLength(IN Buffer* b);
PUBLIC void     BufferWrite(IN Buffer* b, IN char c);
PUBLIC void     BufferAppend(IN Buffer* b, IN char* s, IN int len);
PUBLIC void     BufferPrintf(IN Buffer* b, IN char* fmt, ...);
PUBLIC void     BufferPush(IN Buffer* b, IN char c);
PUBLIC char     BufferPop(IN Buffer* b);

/* Vector. */

typedef struct Vector {
	uint8_t* data;
	size_t elementCount;
	size_t numAllocations;
	size_t elementSize;
}Vector;
typedef Vector TokenVector;
typedef Vector FileVector;

PUBLIC Vector* NewVector(IN OPTIONAL unsigned int numAllocations, IN size_t elementSize);
PUBLIC void ExtendVector(IN Vector* v, IN size_t numAllocations);
PUBLIC Vector* CopyVector(IN Vector* v);
PUBLIC void VectorPush(IN Vector* v, IN void* e);
PUBLIC void VectorPop(IN Vector* v, OUT void* data);
PUBLIC void VectorGet(IN Vector* v, IN index_t index, OUT void* data);
PUBLIC void VectorSet(IN Vector* v, IN index_t index, OUT void* data);
PUBLIC void VectorFirstElement(IN Vector* v, OUT void* data);
PUBLIC BOOL VectorLastElement(IN Vector* v, OUT void* data);
PUBLIC Vector* NewReverseVector(IN Vector* v);
PUBLIC void VectorAppend(IN Vector* v, IN Vector* a);
PUBLIC void* VectorData(IN Vector* v);
PUBLIC size_t VectorLength(IN Vector* v);
PUBLIC void FreeVector(IN Vector* v);

#endif
