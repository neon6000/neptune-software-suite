/************************************************************************
*
*	vector.c - Vector support
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <assert.h>
#include <string.h>
#include <malloc.h>
#include "base.h"

/* vectors are dynamic sized arrays of arbitrary elements. */

PUBLIC Vector* NewVector(IN OPTIONAL unsigned int numAllocations, IN size_t elementSize) {

	Vector* v;

	assert(elementSize > 0);

	v = (Vector*)malloc(sizeof(Vector));
	if (!v)
		return NULL;
	v->data = NULL;
	if (numAllocations > 0)
		v->data = (uint8_t*)realloc(NULL, numAllocations * elementSize);
	v->elementSize = elementSize;
	v->elementCount = 0;
	v->numAllocations = numAllocations;
	return v;
}

PUBLIC void ExtendVector(IN Vector* v, IN size_t numAllocations) {

	assert(v != NULL);

	if (v->elementCount + numAllocations <= v->numAllocations)
		return; /* don't need to extend. */

	v->numAllocations += numAllocations;
	v->data = realloc(v->data, v->numAllocations * v->elementSize);
}

PUBLIC Vector* CopyVector(IN Vector* v) {

	Vector* n;

	assert(v != NULL);

	n = NewVector(v->elementCount, v->elementSize);
	if (n) {
		memcpy(n->data, v->data, v->elementCount * v->elementSize);
		n->elementCount = v->elementCount;
	}
	return n;
}

PUBLIC void VectorPush(IN Vector* v, IN void* e) {

	assert(v != NULL);
	assert(e != NULL);
	ExtendVector(v, 1);
	memcpy(&v->data[v->elementCount * v->elementSize], e, v->elementSize);
	v->elementCount++;
}

PUBLIC void VectorPop(IN Vector* v, OUT OPTIONAL void* data) {

	assert(v != NULL);
	assert(v->elementCount > 0);
	v->elementCount--;
	if (data)
		memcpy(data, &v->data[v->elementCount * v->elementSize], v->elementSize);
}

PUBLIC void VectorGet(IN Vector* v, IN index_t index, OUT void* data) {

	assert(v != NULL);
	assert(data != NULL);
	assert(0 <= index && index < v->numAllocations);
	memcpy(data, &v->data[index * v->elementSize], v->elementSize);
}

PUBLIC void VectorSet(IN Vector* v, IN index_t index, OUT void* data) {

	assert(v != NULL);
	assert(data != NULL);
	assert(0 <= index && index < v->numAllocations);
	memcpy(&v->data[index * v->elementSize], data, v->elementSize);
}

PUBLIC void VectorFirstElement(IN Vector* v, OUT void* data) {

	assert(v != NULL);
	assert(data != NULL);
	assert(v->elementCount > 0);
	memcpy(data, &v->data[0], v->elementSize);
}

PUBLIC BOOL VectorLastElement(IN Vector* v, OUT void* data) {

	assert(v != NULL);
	assert(data != NULL);
	if (v->elementCount == 0)
		return TRUE;
	memcpy(data, &v->data[(v->elementCount - 1) * v->elementSize], v->elementSize);
	return FALSE;
}

PUBLIC Vector* NewReverseVector(IN Vector* v) {

	Vector* n;
	index_t i;

	assert(v != NULL);

	n = NewVector(v->elementCount, v->elementSize);
	if (!n)
		return NULL;
	n->elementCount = v->elementCount;
	for (i = 0; i < n->elementCount; i++) {
		memcpy(&n->data[i * n->elementSize],
			&v->data[(v->elementCount - i - 1) * v->elementSize],
			n->elementSize);
	}
	return n;
}

PUBLIC void VectorAppend(IN Vector* v, IN Vector* a) {

	assert(v != NULL);
	assert(v->elementSize == a->elementSize);

	ExtendVector(v, a->elementCount);
	memcpy(&v->data[v->elementCount * v->elementSize], a->data,
		a->elementSize * a->elementCount);
	v->elementCount += a->elementCount;
}

PUBLIC void* VectorData(IN Vector* v) {

	assert(v != NULL);
	return v->data;
}

PUBLIC size_t VectorLength(IN Vector* v) {

	assert(v != NULL);
	return v->elementCount;
}

PUBLIC void FreeVector(IN Vector* v) {

	assert(v != NULL);
	free(v->data);
	free(v);
}
