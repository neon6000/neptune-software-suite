/************************************************************************
*
*	build.c - Build Maintenance Utility
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <io.h>
#include <time.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <sys/stat.h>
#include "nmake.h"

/* PRIVATE Definitions ****************************/

PRIVATE int DoCommands(IN char* targetName, IN StringList* commands, IN char* firstDep, int targetFlags) {

	StringList* command;
	char* line;
	int retval;

	retval = 0;
	for (command = commands; command && command->text; command = command->next) {
		line = Expand(command->text, TRUE);
		MakeInfo("\t%s\n\r", line);
		if (OFF(_globalFlags, NO_EXECUTE))
			retval += system(line);
		if (OFF(_globalFlags, IGNORE_EXIT_CODES) && retval != 0)
			MakeError("A tool returned error code 0x%x", retval);
		free(line);
	}

	return retval;
}

// given a dependent name and timestamp, add it to the dependent list:
PRIVATE Dependent* AddDependent(IN DepList** list, IN char* depname, IN time_t deptime) {

	Dependent* newdep;
	Dependent* dep;

	newdep = ALLOC_OBJ(Dependent);
	newdep->name = _strdup(depname);
	newdep->depTime = deptime;
	newdep->next = NULL;
	if (!*list)
		*list = newdep;
	else{
		for (dep = *list; dep->next; dep = dep->next)
			;
		dep->next = newdep;
	}
	return newdep;
}

// free dependency list:
PRIVATE void FreeDependentList(IN DepList** list) {

	for (Dependent* dep = *list; dep;) {
		Dependent* next = dep->next;
		//free(dep->name); // note this may be pointed by targets when calling CreateTempTarget
		free(dep);
		dep = next;
	}
	*list = NULL;
}

// build dependency list:
PRIVATE DepList* BuildDependencyList(IN BuildBlock* block, IN char* targetname) {

	DepList*           list;
	intptr_t           handle;
	FileManager*       fm;
	struct _finddata_t find;

	fm = GetFileManager();
	list = NULL;

	for (StringList* dependent = block->dependents; dependent && dependent->text; dependent = dependent->next) {

		char* s = dependent->text;
		char* value;
		char* filename;
		BOOL  isFile;
		char* pathEnd;

		filename = NULL;
		isFile = FALSE;
		_dollarDollarAt = targetname;

		// get dependency value from its text:
		value = Expand(s, TRUE);
		
		// skip over path if any:
		if (*value == '{') {
			while (*value != '}')
				value++;
			value++;
		}

		// if "value" has spaces, surround it in quotes:
		if (!strchr(value, ' ') && strchr(value, '\"')) {
			MakeInfo("--add quotes to value--");
		}

		// use findFirst and findNexy since "value" can have wildcards in filenames:
		filename = fm->findFirst(value, &find, &handle);
		do {

			Buffer temp;
			time_t deptime = 0L;

			if (filename) {

				isFile = TRUE;
				deptime = find.time_write;
				pathEnd = FindLastPathSeparator(value);

				// if "value" has no directory path:
				if (!pathEnd) {
					AddDependent(&list, filename, deptime);
					continue;
				}	

				// bug is here -- need to manually push null character again?

				// if "value" has a directory path, prepend it to "filename":
				BufferInit(&temp);
				BufferPrintf(&temp, "%.*s", pathEnd - value + 1, value);
				BufferPrintf(&temp, "%s", filename);
				BufferPush(&temp, '\0');
				filename = temp.data;
				AddDependent(&list, filename, deptime);
				free(filename);
			}

			// this is a psuedotarget so add it "as is":
			else {
				filename = value;
				AddDependent(&list, filename, deptime);
			}

		} while (isFile && (filename = fm->findNext(value, &find, &handle)));
	}

	return list;
}

PRIVATE int _buildLevel = 0;

// build target:
PRIVATE int Build(IN Target* target, IN int parentFlags, OUT time_t* targetTime, IN char* firstDep, BOOL inMakefile) {

	DepList*     dependencies;
	Dependent*   dependent;
	char*        inferredDependent;
	BuildBlock*  block;
	BuildBlock*  explComBlock;
	StringList*  questionList;
	StringList*  starList;
	StringList*  impliedComList;
	time_t       targetTimeFileSystem;
	time_t       newTargetTime;
	time_t       depTime;
	time_t       tempTargetTime;
	time_t       mostRecentDepTime;
	time_t*      blockTime;
	int          status;
	Rule*        rule;
	BOOL         built;
	struct stat  st;

	if (!target) {
		*targetTime = 0L;
		return FALSE;
	}

	if (ON(target->flags, TARGET_ALREADY_BUILT)) {

		*targetTime = target->dateTime;
		if (OFF(_globalFlags, QUESTION_STATUS) && OFF(target->flags, TARGET_OUT_OF_DATE) && _buildLevel == 1 && !stat(target->name, &st))
			MakeInfo("'%s' id up-to-date", target->name);
		if (ON(target->flags, TARGET_OUT_OF_DATE))
			return 1;
		return 0;
	}

	if (ON(target->flags, TARGET_BUILD_THIS_ONE))
		MakeError("Cycle in dependency tree for target '%s'", target->name);

	_dollarStar = _dollarAt = target->name;

	explComBlock = NULL;
	blockTime = NULL;
	impliedComList = NULL;
	block = NULL;
	dependencies = NULL;
	rule = NULL;
	inferredDependent = NULL;
	targetTimeFileSystem = 0L;
	mostRecentDepTime = 0L;
	tempTargetTime = 0L;
	newTargetTime = 0L;
	depTime = 0L;
	status = 0;
	built = FALSE;
	questionList = NULL;
	starList = NULL;

	// we are building this target:
	SET(target->flags, TARGET_BUILD_THIS_ONE);

	// go through build blocks:
	for (BuildList* currentBlock = target->buildList; currentBlock; currentBlock = currentBlock->next) {

		depTime = 0L;
		block = currentBlock->buildBlock;

		// if the command block has already been executed,
		// set target time and skip this block:
		if (block->datetime != 0) {
			targetTimeFileSystem = MAX(targetTimeFileSystem, block->datetime);
			built = TRUE;
			continue;
		}

		blockTime = &block->datetime;

		// go through dependency list:
		dependencies = BuildDependencyList(block, target->name);
		for (dependent = dependencies; dependent; dependent = dependent->next) {

			// get current dependent time:
			tempTargetTime = dependent->depTime;

			// build dependent:
			status += InvokeBuild(dependent->name, 0, &tempTargetTime, NULL);

			// set max dependent time:
			depTime = MAX(depTime, tempTargetTime);

			// if target exists, then we need its timestamp to correctly construct $?
			if (!targetTimeFileSystem && OFF(block->flags, TARGET_DOUBLECOLON) && !stat(target->name, &st)) {
				targetTimeFileSystem = st.st_mtime;
				target->dateTime = st.st_mtime;
			}

			// if dependent was rebuilt, add to $?
			if (ON(target->flags, TARGET_FORCE_BUILD)
				|| targetTimeFileSystem < tempTargetTime
				|| (ON(_globalFlags, BUILD_IF_EQUAL) && (targetTimeFileSystem == tempTargetTime))) {

				if (!questionList)
					questionList = NewStringList();
				AddString(questionList, dependent->name);
			}

			// always add dependent to $**
			if (!starList)
				starList = NewStringList();
			AddString(starList, dependent->name);
		}

		// free dependent lists:
		FreeDependentList(&dependencies);

		if (ON(block->flags, TARGET_DOUBLECOLON)) {

			if (block->buildCommands) {

				_dollarQuestion = questionList;
				_dollarStar = target->name;
				_dollarAt = target->name;
				_dollarLessThan = NULL;
				_dollarDollarAt = NULL;
				_dollarStarStar = starList;

				if ((status == 0) && (targetTimeFileSystem < depTime)
					|| (ON(_globalFlags, BUILD_IF_EQUAL) && (targetTimeFileSystem == depTime))
					|| (targetTimeFileSystem == 0 && depTime == 0)
					|| (!block->dependents)) {

					struct _stat64i32 st;

					firstDep = firstDep ? firstDep : (_dollarStarStar ? _dollarStarStar->text : NULL);
					status += DoCommands(target->name, block->buildCommands, firstDep, block->flags);

					if (OFF(target->flags, NO_EXECUTE) && !_stat(target->name, &st))
						newTargetTime = st.st_mtime;
					else if (mostRecentDepTime)
						newTargetTime = mostRecentDepTime;
					else
						newTargetTime = 0L; // current time.
					
					block->datetime = newTargetTime;
					built = TRUE;
					starList = NULL;
					questionList = NULL;
				}
			}
		}
		else{

			if (block->buildCommands) {
				if (explComBlock)
					MakeError("Too many rules in %s", target->name);
				explComBlock = block;
			}
			mostRecentDepTime = MAX(mostRecentDepTime, depTime);
		}

		// if this is the last build block:
		if (ON(block->flags, TARGET_DOUBLECOLON) && !currentBlock->next) {

			CLEAR(target->flags, TARGET_BUILD_THIS_ONE);
			SET(target->flags, TARGET_ALREADY_BUILT);

			if (status > 0)
				SET(target->flags, TARGET_OUT_OF_DATE);
			else
				CLEAR(target->flags, TARGET_OUT_OF_DATE);

			targetTimeFileSystem = MAX(newTargetTime, targetTimeFileSystem);
			target->dateTime = targetTimeFileSystem;
			*targetTime = targetTimeFileSystem;
			return status;
		}
	}

	_dollarLessThan = NULL;
	_dollarDollarAt = NULL;

	if (!(targetTimeFileSystem = *targetTime)) {
		struct _stat64i32 st;
		if (target->dateTime)
			targetTimeFileSystem = target->dateTime;
		else if (!_stat(target->name, &st))
			targetTimeFileSystem = st.st_mtime;
	}

	if (ON(target->flags, TARGET_DISPLAY_FILE_DATES))
		MakeInfo("DisplayFileDates: %s [%w]", target->name, targetTimeFileSystem);

	built = FALSE;

	rule = UseRule(target, &inferredDependent, targetTimeFileSystem, &questionList, &starList, &status, &mostRecentDepTime, &firstDep);
	if (rule) {
		if (!explComBlock) {
			_dollarLessThan = inferredDependent;
			impliedComList = rule->buildCommands;
		}
	}

	_dollarStar     = target->name;
	_dollarAt       = target->name;
	_dollarQuestion = questionList;
	_dollarStarStar = starList;

	if (status == 0 && targetTimeFileSystem < mostRecentDepTime
		|| (targetTimeFileSystem == 0 && mostRecentDepTime == 0)
		|| (ON(_globalFlags, BUILD_IF_EQUAL) && (targetTimeFileSystem == mostRecentDepTime))
		|| ON(target->flags, TARGET_FORCE_BUILD)) {

		if (explComBlock) {

			firstDep = firstDep ? firstDep : (_dollarStarStar ? _dollarStarStar->text : NULL);
			status += DoCommands(target->name, explComBlock->buildCommands, firstDep, explComBlock->flags);
		}
		else if (impliedComList) {

			status += DoCommands(target->name, impliedComList, firstDep, target->flags);
		}
		else if (ON(_globalFlags, TOUCH_TARGETS)) {

			if (block)
				status += DoCommands(target->name, block->buildCommands, firstDep, block->flags);
		}
		else if (!inMakefile && targetTimeFileSystem == 0) {

			MakeError("Not able to make target %s", target->name);
		}

		// if command executed or has no dependents then current time else max of dependent time
		if (explComBlock || impliedComList || !_dollarStarStar)
			newTargetTime = 0L; // current system time + 2
		else
			newTargetTime = mostRecentDepTime;

		// set block time if block was executed:
		if (blockTime && explComBlock)
			*blockTime = newTargetTime;
	}

	else if (OFF(_globalFlags, QUESTION_STATUS) && !built && _buildLevel == 1)
		MakeInfo("'%s' is up-to-date", target->name);

	CLEAR(target->flags, TARGET_BUILD_THIS_ONE);
	SET(target->flags, TARGET_ALREADY_BUILT);
	if (!status)
		SET(target->flags, TARGET_OUT_OF_DATE);
	else
		CLEAR(target->flags, TARGET_OUT_OF_DATE);

	targetTimeFileSystem = MAX(targetTimeFileSystem, newTargetTime);
	target->dateTime = targetTimeFileSystem;
	*targetTime = targetTimeFileSystem;

	return status;
}

/* PUBLIC Definitions ****************************/

PUBLIC int InvokeBuild(IN char* targetname, IN int parentFlags, OUT time_t* targetTime, IN char* firstDep) {

	Target* target = GetTarget(targetname);
	BOOL inMakefile = TRUE;
	int status = 0;

	// temporary targets are used by UseRule for inferred dependents:
	if (!target) {
		target = AddTempTarget(targetname);
		inMakefile = FALSE;
		if (ON(_globalFlags, FORCE_BUILD))
			SET(target->flags, TARGET_FORCE_BUILD);
	}
	_buildLevel++;
	status = Build(target, parentFlags, targetTime, firstDep, inMakefile);
	_buildLevel--;
	return status;
}

// $(ProjectDir)

// C:\Users\Michael\Desktop\NEPTUNE\neptune-software-suite\private\nmake_test
