/************************************************************************
*
*	rule.c
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <malloc.h>
#include <string.h>
#include <sys/stat.h>
#include "nmake.h"

/* PRIVATE Definitions ************************/

PRIVATE RuleList*   _rules;

// Rules have the following format: {frompath}.fromext{topath}.toext
// where {frompath} and {topath} are optional or can be empty (e.g. {}.fromext{}.toext)

PRIVATE char* ConsumePathList(IN char* s) {

	if (*s == '{') {
		for (; *s && *s != '}'; s++)
			;
		s++;
	}
	return s;
}

PRIVATE char* ConsumeExt(IN char* s) {

	if (*s++ == '.') {
		for (; *s && *s != '{' && *s != '.'; s++)
			;
	}
	return s;
}

PRIVATE char* ScanFromPath(IN char* rule) {

	Buffer path;

	if (*rule++ != '{')
		return NULL;
	BufferInit(&path);
	while (*rule != '}')
		BufferPush(&path, *rule++);
	BufferPush(&path, '\0');
	return path.data;
}

PRIVATE char* ScanFromExt(IN char* rule) {

	Buffer path;
	char* s = ConsumePathList(rule);

	if (*s != '.') return NULL;
	BufferInit(&path);
	BufferPush(&path, *s);
	while (s++) {
		if (*s == '.' || *s == '{')
			break;
		BufferPush(&path, *s);
	}
	BufferPush(&path, '\0');
	return path.data;
}

PRIVATE char* ScanToPath(IN char* rule) {

	Buffer path;

	rule = ConsumePathList(rule);
	rule = ConsumeExt(rule);
	if (*rule++ != '{')
		return NULL;
	BufferInit(&path);
	while (*rule != '}')
		BufferPush(&path, *rule++);
	BufferPush(&path, '\0');
	return path.data;
}

PRIVATE char* ScanToExt(IN char* rule) {

	Buffer path;
	char* s;

	s = strrchr(rule, '.');
	if (!s) return NULL;
	BufferInit(&path);
	BufferPush(&path, *s);
	while (s++) {
		if (*s == '.' || *s == '{')
			break;
		BufferPush(&path, *s);
	}
	BufferPush(&path, '\0');
	return path.data;
}

PRIVATE RuleList* NewRule(IN char* name, IN StringList* commands) {

	RuleList* rule = ALLOC_OBJ(RuleList);
	rule->name = name;
	rule->next = NULL;
	rule->back = NULL;
	rule->buildCommands = commands;
	return rule;
}

/* PUBLIC Definitions ****************************/

// skip past first brace pair(if any)
// if next character is a period,
//		look for next brace
//		if there are no path separators between second brace pair,
//		   and there's just a suffix after them, it's a rule
//		else if there's another period later on, and no path seps
//		   after it, then it's a rule.

PUBLIC BOOL IsRule(IN char* s) {

	s = ConsumePathList(s);
	if (*s != '.')
		return FALSE;
	for (s += 1; *s && *s != '{' && *s != '.'; s++)
		;
	if (*s == '{') {
		for (; *s && *s != '}'; s++)
			;
		if (*s == (int) NULL)
			return FALSE;
		s++;
		if (*s == '.') {
			for (s += 1; *s; s++) {
				if (IsPathSeparator(*s))
					return FALSE;
				else if (*s == '.')
					MakeError("Too many rule names");
			}
			return TRUE;
		}
	}
	if (*s == '.') {
		s++;
		if (*s == (int) NULL)
			return FALSE;
		for (; *s; s++) {
			if (IsPathSeparator(*s))
				return FALSE;
			else if (*s == '.')
				MakeError("Too many rule names");
		}
		return TRUE;
	}
	return FALSE;
}

PUBLIC RuleList* GetRule(IN char* name) {

	RuleList* rule = NULL;
	for (rule = _rules; rule; rule = rule->next) {
		if (!strcmp(rule->name, name))
			break;
	}
	return rule;
}

PUBLIC RuleList* AddRule(IN char* name, IN StringList* commands) {

	RuleList* r = _rules;
	RuleList* node;

	if (GetRule(name))
		MakeError("Duplicate rule %s", name);
	if (!r) {
		r = NewRule(name, commands);
		_rules = r;
		return r;
	}
	while (r->next)
		r = r->next;
	node = NewRule(name, commands);
	r->next = node;
	node->back = r;
	return node;
}

PUBLIC void PrintRules(void) {

	MakeInfo("");
	MakeInfo("RULES:");
	MakeInfo("");
	for (RuleList* rule = _rules; rule; rule = rule->next) {
		MakeInfo("%s:", rule->name);
		if (rule->buildCommands)
			MakeInfo("\tcommands:\t%s", rule->buildCommands->text);
		for (StringList* s = rule->buildCommands->next; s; s = s->next)
			MakeInfo("\t\t\t%s", s->text);
	}
}

PUBLIC Rule* FindRule(OUT char** name, IN char* target, OUT struct stat* datetime) {

	char* targetext;
	char* targetname;
	char* toPath;
	char* fromExt;
	char* fromPath;
	Buffer dependent;
	unsigned int i;

	if (IsPathSeparator(*target))
		target++; // jump over initial separator if any
	targetext = strrchr(target, '.');
	if (!targetext)
		return NULL;
	targetname = FindLastPathSeparator(target);
	if (!targetname)
		targetname = target;
	else
		targetname++;

	for (Rule* rule = _rules; rule; rule = rule->next) {
		char* toExt = ScanToExt(rule->name);
		if (!strncmp(toExt, targetext, strlen(toExt))) {

			// test if topath matches:
			toPath = ScanToPath(rule->name);
			i = 0;
			for (char* s = target; s && s != targetname && i < strlen(toPath); s++, i++) {
				if (IsPathSeparator(*s) == IsPathSeparator(toPath[i]))
					continue;
				if (*s != toPath[i])
					break;
			}
			if (i != strlen(toPath))
				break;
			free(toPath);

			// build dependent file name:
			fromExt = ScanFromExt(rule->name);
			fromPath = ScanFromPath(rule->name);
			BufferInit(&dependent);
			if (fromPath && strlen(fromPath) > 0)
				BufferPrintf(&dependent, "%s/", fromPath);
			BufferPrintf(&dependent, "%.*s%s ", targetext - targetname, targetname, fromExt);
			BufferPush(&dependent, '\0'); // BUG: BufferPrintf not writing null char
			free(fromExt);
			free(fromPath);

			struct stat st;
			if (!_stat(dependent.data, &st)) {
				free(toExt);
				*name = dependent.data;
				*datetime = st;
				return rule;
			}
			free(dependent.data);
		}
		free(toExt);
	}

	*name = NULL;
	return NULL;
}

PUBLIC Rule* UseRule(IN Target* target, OUT char** inferredDependent, IN time_t targetTime,
	OUT StringList** questionList, OUT StringList** starList, OUT int* status,
	OUT time_t* maxDepTime, OUT char** firstDep) {

	Rule*       rule;
	time_t      depTime;
	StringList* temp;
	struct stat depStat;

	rule = FindRule(inferredDependent, target->name, &depStat);
	if (!rule)
		return NULL;
	depTime = depStat.st_mtime;

	*firstDep = *inferredDependent;
	for (temp = *starList; temp; temp = temp->next) {
		if (!strcmp(temp->text, *inferredDependent))
			break;
	}
	if (temp)
		CLEAR(target->flags, TARGET_DISPLAY_FILE_DATES);

	*status += InvokeBuild(*inferredDependent, target->flags, &depTime, NULL);
	if (ON(target->flags, TARGET_FORCE_BUILD) || targetTime < depTime) {
		if (!temp) {
			if (!*questionList)
				*questionList = NewStringList();
			AddString(*questionList, *inferredDependent);
			if (!*starList)
				*starList = *questionList;
		}
	}

	if (ON(target->flags, TARGET_DISPLAY_FILE_DATES) && OFF(target->flags, TARGET_FORCE_BUILD)) {
		MakeInfo("target: %s %w", target->name, target->dateTime);
		MakeInfo("inferredDependent: %s %w", *inferredDependent, depTime);
	}
	*maxDepTime = MAX(*maxDepTime, depTime);
	return rule;
}

PUBLIC void SortRules(void) {

	// sort list of inference rules on .SUFFIXES order.
	// The inference rules which have their '.toext' part listed earlier in .SUFFIXES are reordered to
	// be earlier in the inference rules list. The inference rules for suffixes that
	// are not in .SUFFIXES are detected here and are ignored.

}
