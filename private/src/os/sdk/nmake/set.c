/************************************************************************
*
*	set.c - Set support
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <assert.h>
#include <string.h>
#include <malloc.h>
#include "nmake.h"

/* This represents a set of unique strings. */


PUBLIC Set* SetInsert(IN Set* k, IN char* s) {

	Set* r;

	r = (Set*)malloc(sizeof(Set));
	if (!r)
		return NULL;
	r->next = k;
	r->s = s;
	return r;
}

PUBLIC BOOL SetFind(IN Set* k, IN char* s) {

	Set* v;

	if (!k) return FALSE;
	for (v = k; v; v = v->next) {
		if (!v->s)
			continue;
		if (!strcmp(v->s, s))
			return TRUE;
	}
	return FALSE;
}

PUBLIC Set* SetUnion(IN Set* a, IN Set* b) {

	Set* k = b;
	for (; a; a = a->next) {
		if (!SetFind(b, a->s))
			k = SetInsert(k, a->s);
	}
	return k;
}

PUBLIC Set* SetIntersect(IN Set* a, IN Set* b) {

	Set* k = NULL;
	for (; a; a = a->next) {
		if (SetFind(b, a->s))
			k = SetInsert(k, a->s);
	}
	return k;
}

PUBLIC void PrintSet(IN Set* set) {

	MakeInfo("  Set:");
	for (Set* k = set; k; k = k->next)
		MakeInfo("    %s", k->s);
}
