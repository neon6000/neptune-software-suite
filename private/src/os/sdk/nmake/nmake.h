/************************************************************************
*
*	nmake.h - Master include file.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef NMAKE_H
#define NMAKE_H

#include <time.h>
#include "base.h"

#define ALLOC_OBJ(type) ((type*)malloc(sizeof(type)))
#define FREE(obj) (free(obj))

/*
	The File Manager is responsible for managing the File stack,
	getting and ungetting characters, character lookahead, and
	general file management facilities needed for the build process
	such as touch and datetime.
*/

struct FILE;
typedef struct File File;

typedef struct StringList {
	char* text;
	struct StringList* next;
}StringList;

typedef struct Location {
	File* src;
	int x, y;
}Location;

#define FILE_PUSHBACK_MAX 50

typedef struct File {
	struct FILE* stream;
	char*    fname;
	Location loc;
	BOOL     firstColumn;
	BOOL     firstChar;
	BOOL     eof;
	char     pushback[FILE_PUSHBACK_MAX];
	int      pushbackTop;
}File;

typedef File*       (*file_open_t)     (IN char*);
typedef void        (*file_close_t)    (IN File*);
typedef void        (*file_push_t)     (IN File*);
typedef File*       (*file_pop_t)      (void);
typedef File*       (*file_top_t)      (void);
typedef int         (*file_getc_t)     (void);
typedef void        (*file_ungetc_t)   (IN int);
typedef int         (*file_peek_t)     (IN int ahead);
typedef char*       (*file_get_line_t) (IN int delim);
typedef BOOL        (*file_touch_t)    (IN char* fname);
typedef time_t      (*file_datetime_t) (IN char* fname);
typedef char*       (*file_findfirst_t)(IN char* fname, IN OUT struct _finddata_t* data, IN OUT intptr_t* handle);
typedef char*       (*file_findnext_t) (IN char* fname, IN OUT struct _finddata_t* data, IN OUT intptr_t* handle);
typedef StringList* (*file_expand_t)   (IN char* fname);
typedef char*       (*file_curdir_t)   (void);

typedef struct FileManager {
	file_open_t      open;
	file_close_t     close;
	file_push_t      push;
	file_pop_t       pop;
	file_top_t       top;
	file_getc_t      getc;
	file_ungetc_t    ungetc;
	file_peek_t      peek;
	file_get_line_t  getline;
	file_touch_t     touch;
	file_datetime_t  datetime;
	file_findfirst_t findFirst;
	file_findnext_t  findNext;
	file_expand_t    expand;
	file_curdir_t    currentDir;
}FileManager;

extern FileManager* GetFileManager(void);

/*
	The preprocessor handles all directives. Directives
	start with a '!' on the first column. The scanner
	calls Preprocessor->get to read the next character.
	If the current column is the beginning of a directive,
	the preprocessor returns a new line character.

	All directive processing happens here. It sets between the
	File Manager and the Scanner. For !if directives, this
	will need its own scanner/parser for expression evaluation
	since this is before tokenization.
*/

typedef int  (*pproc_get_t)   (void);
typedef void (*pproc_unget_t) (IN int);
typedef int  (*pproc_peek_t)  (IN int ahead);
typedef BOOL (*pproc_init_t)  (IN FileManager*);
typedef char* (*pproc_getline_t)(IN int delim);
typedef void(*pproc_cleanup_t)(void);

typedef struct Preprocessor {
	pproc_init_t  init;
	pproc_get_t   getc;
	pproc_unget_t ungetc;
	pproc_peek_t  peek;
	pproc_getline_t getline;
	pproc_cleanup_t cleanup;
}Preprocessor;

extern Preprocessor* GetPreprocessor(void);

/*
	The scanner is responsible for tokenizing the
	input characters from the Preprocessor. It is
	called by the Parsr to construct tokens as needed.
*/

typedef enum TokenType {
	TokenEof           = 0,
	TokenNewLine       = 1<<1,
	TokenNewLineSpace  = 1<<2,
	TokenColon         = 1<<3,
	TokenDoubleColon   = 1<<4,
	TokenSemicolon     = 1<<5,
	TokenEquals        = 1<<6,
	TokenName          = 1<<7,
	TokenString        = 1<<8,
	TokenValue         = 1<<9,
	TokenEquals2       = 1<<10 // ':=' token, internal
}TokenType;

typedef struct Token {
	TokenType type;
	Location  loc;
	char*     val;
	BOOL      expandOnce;
}Token;

typedef void  (*scan_init_t)   (IN Preprocessor*);
typedef Token (*scan_get_t)    (void);
typedef void  (*scan_unget_t)  (IN Token);
typedef Token (*scan_string_t) (void);
typedef Token (*scan_value_t)  (void);

typedef struct Scanner {
	scan_init_t   init;
	scan_get_t    get;
	scan_unget_t  unget;
	scan_string_t string;
	scan_value_t  value;
}Scanner;

extern Scanner* GetScanner(void);

/*
	The Parser reads in the input files and sets things up
	for later phases. This must be compatible with other Make
	utilities. Please reference:

	https://github.com/fstudio/nmake/blob/master/source/grammar.h

	Makefile := BlankLines Makefile
	         | newline name Body Makefile
	         | // epsilon
	         ;

	Body	:= NameList Separator BuildInfo
	        | equals value
	        ;

	NameList := name NameList
	         | // epsilon
	         ;

	Commands := MoreBuildLines
	         | semicolon string MoreBuildLines
	         | // epsilon
	         ;

	MoreBuildLines := NewLineWhitespace string MoreBuildLines

	BlankLines := newline BlankLines
	           | newLineWhitespace BlankLines
	           | // epsilon
	           ;

	BuildInfo := NameList Commands
	          | // epsilon
	          ;

	Separator := colon | doublecolon

	<<string>> := Everything from the current input character up
	to the first newline which is not preceded by a backslash, or
	end of file (whichever comes first).

	<<value>> := This is a <<string>> whith comments stripped.
*/

typedef void (*parser_init_t) (IN Scanner* scan);
typedef void (*parse_t)       (void);

typedef struct Parser {
	parser_init_t init;
	parse_t       parse;
}Parser;

extern Parser* GetParser(void);

typedef struct BuildBlock {
	int flags;
	time_t datetime;
	StringList* dependents;
	StringList* buildCommands;
}BuildBlock;

typedef struct BuildList {
	BuildBlock* buildBlock;
	struct BuildList* next;
}BuildList;

// inference rules:
typedef struct RuleList {
	char* name;
	StringList* buildCommands;
	struct RuleList* next;
	struct RuleList* back;
}RuleList, Rule;

typedef struct DepList {
	char* name;
	time_t depTime;
	struct DepList* next;
}DepList, Dependent;

typedef struct Macro {
	char* name;
	int flags;
	StringList* values;
	struct Macro* next;
}Macro;

typedef struct Target {
	char* name;
	int flags;
	time_t dateTime;
	BuildList* buildList;
	struct Target* next;
}Target;

extern Macro*      _macroTable[];
extern Target*     _targetTable[];
extern RuleList*   _rules;
extern StringList* _macros;

extern char*       _dollarDollarAt;
extern char*       _dollarLessThan;
extern char*       _dollarStar;
extern char*       _dollarAt;
extern StringList* _dollarQuestion;
extern StringList* _dollarStarStar;

typedef enum MacroType {
	INVALID_MACRO    = 0,
	SPECIAL_MACRO   = 0x01,  // $* $@ $? $< $**
	DYNAMIC_MACRO   = 0x02,  // $$@
	X_SPECIAL_MACRO = 0x03,  // $(*F) $(@D) etc.
	X_DYNAMIC_MACRO = 0x04,  // $$(@F) $$(@D)
	DOLLAR_MACRO    = 0x05,  // $$ -> $
	USER_MACRO      = 0x06,  // $<character>
	X_USER_MACRO    = 0x07   // $(characters)
}MacroType;

typedef unsigned int NMakeFlags;
extern NMakeFlags _globalFlags;

/* flags for global _nasmFlags */
#define PRINT_INFORMATION   1
#define IGNORE_PREDEFINED   2
#define USE_ENV_VARS        4
#define QUESTION_STATUS     8
#define TOUCH_TARGETS       0x10
#define CRYPTIC_OUTPUT      0x20
#define NO_LOGO             0x40
#define IGNORE_EXTERN_RULES 0x80
#define SUPRESS_OUTPUT      0x100
#define NO_EXECUTE          0x200
#define DISPLAY_FILE_DATES  0x400
#define IGNORE_EXIT_CODES   0x800
#define NO_ECHO             0x1000
#define FORCE_BUILD         0x2000
#define BUILD_IF_EQUAL      0x4000
#define DISPLAY_INCLUDES    0x8000

/* flags for target->flags and block->flags */
#define TARGET_DISPLAY_FILE_DATES 1
#define TARGET_IGNORE_EXIT_CODES  2
#define TARGET_NO_EXECUTE         4
#define TARGET_NO_ECHO            8
#define TARGET_FORCE_BUILD        0x10
#define TARGET_DOUBLECOLON        0x20
#define TARGET_ALREADY_BUILT      0x40
#define TARGET_BUILD_THIS_ONE     0x80
#define TARGET_OUT_OF_DATE        0x100
#define TARGET_ERROR_IN_CHILD     0x200

/* flag helpers */
#define ON(v,n)    ((v&n)==(n))
#define OFF(A,B)   (!ON(A,B)) 
#define CLEAR(v,n) (v &= ~(n))
#define SET(v,n)   (v |= (n))
#define FLIP(v,n)  (ON(v,(n)) ? CLEAR(v,(n)) : SET(v,(n)))

// parse.c:
extern  Target*    GetFirstTarget(void);

// lex.c:
extern char*       Tok2Str(IN Token* tok);

// action.c:
extern void        DefineMacro       (IN char* name, IN char* value);
extern void        DefineBuildRule(void);

extern StringList* NewStringList     (void);
extern void        AddString         (IN StringList* list, IN char* str);
extern BuildList*  NewBuildList      (void);
extern void        AddBuildBlock     (IN BuildList* list, IN BuildBlock* block);

/* expand.c */
extern MacroType   DetermineMacroType(IN char* value);
extern char*       ConsumeMacro      (IN char* value);
extern BOOL        IsPathSeparator   (IN char c);
extern char*       ExpandUserMacros  (IN char* value);
extern char*       Expand            (IN char* value, IN BOOL special);
extern MacroType   DetermineMacroType(IN char* value);
extern char*       FindFirstPathSeparator(IN char* s);
extern char*       FindLastPathSeparator (IN char* s);

/* rule.c */
extern RuleList*   AddRule(IN char* name, IN StringList* commands);
extern RuleList*   GetRule           (IN char* name);
extern BOOL        IsRule            (IN char* s);
extern void        PrintRules        (void);
extern Rule*       FindRule          (OUT char** name, IN char* target, OUT struct stat* dat);
extern Rule*       UseRule(IN Target* target, OUT char** depName, IN time_t targetTime,
	OUT StringList** questionList, OUT StringList** starList, OUT int* status,
	OUT time_t* maxDepTime, OUT char** firstDep);

/* macro.c */
extern void        PutMacro          (IN char* name, IN StringList* values, IN int flags);
extern void        PutSimpleMacro    (IN char* name, IN char* value, IN int flags);
extern Macro*      GetMacro          (IN char* name);
extern void        PrintMacros       (void);

/* target.c */
extern Target*     GetTarget         (IN char* name);
extern Target*     AddTarget         (IN char* name, IN BuildBlock* block);
extern Target*     AddTempTarget     (IN char* name);
extern void        PrintTargets      (void);

/* build.c */
extern int        InvokeBuild       (IN char* targetname, IN int parentFlags, OUT time_t* targetTime, IN char* firstDep);

/* main.c */
extern void      GetCurrentTime  (OUT time_t* out);
extern void      MakeInfo        (IN char* s, ...);
extern void      MakeError       (IN char* s, ...);

#endif
