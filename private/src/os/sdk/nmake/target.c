/************************************************************************
*
*	target.c
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <io.h>
#include <time.h>
#include <malloc.h>
#include <string.h>
#include <stdio.h>
#include "nmake.h"

/* PRIVATE Definitions ****************************/

#define TARGETTABLE_MAX 64

PRIVATE Target* _targetTable[TARGETTABLE_MAX];

/**
*	Hash
*
*	Description : Given a string, compute the hash.
*
*	Input : str - String to hash.
*
*	Output : Hash value.
*/
PRIVATE int Hash(IN char* str) {

	int val = 0;
	for (char* p = str; *p; p++)
		val += (int)*p;
	return val;
}

/**
*	PutTarget
*
*	Description : Given the name of a target, put it
*	in the hash table. Ignore duplicates.
*
*	Input : name - Name of target.
*
*	Output : Pointer to Target object or NULL on error.
*/
PRIVATE Target* PutTarget(IN char* name) {

	int index;
	Target* target;

	// watch for duplicates. This is not an error
	// since targets can be used with '::' operator:
	target = GetTarget(name);
	if (target)
		return target;

	index = Hash(name) % TARGETTABLE_MAX;
	target = _targetTable[index];
	if (target) {
		Target* t;
		for (t = target; t->next; t = t->next)
			;
		t = ALLOC_OBJ(Target);
		t->name = name;
		t->next = NULL;
		t->buildList = NULL;
		t->dateTime = (time_t)0;
		t->flags = 0;
		target->next = t;
		return t;
	}
	else{
		target = ALLOC_OBJ(Target);
		target->name = name;
		target->next = NULL;
		target->buildList = NULL;
		target->dateTime = (time_t)0;
		target->flags = 0;
		_targetTable[index] = target;
	}
	return target;
}

/**
*	PrintTarget
*
*	Description : Print the specified target.
*
*	Input : target - Pointer to target to print out.
*/
PRIVATE void PrintTarget(IN Target* target) {

	MakeInfo("%s:", target->name);
	for (BuildList* c = target->buildList; c; c = c->next) {

		Buffer depends;
		BufferInit(&depends);

		BuildBlock* block = c->buildBlock;
		for (StringList* deps = block->dependents; deps; deps = deps->next)
			BufferPrintf(&depends, " %s", deps->text);
		BufferPush(&depends, '\0');
		MakeInfo("\tdependents:\t%s", depends.data);
		free(depends.data);
		if (block->buildCommands)
			MakeInfo("\tcommands:\t%s", block->buildCommands->text);
		for (StringList* cmd = block->buildCommands->next; cmd; cmd = cmd->next)
			MakeInfo("\t\t\t%s", cmd->text);
	}
}

/* PUBLIC Definitions *****************************/

/**
*	PrintTargets
*
*	Description : Print all targets.
*/
PUBLIC void PrintTargets(void) {

	MakeInfo("");
	MakeInfo("TARGETS:");
	for (int i = 0; i < TARGETTABLE_MAX; i++) {
		Target* target = _targetTable[i];
		if (!target)
			continue;
		while (target) {
			MakeInfo("");
			PrintTarget(target);
			target = target->next;
		}
	}
}

/**
*	GetTarget
*
*	Description : Given the name of a target,
*	attempt to return the associated Target object.
*
*	Input : name - Name of target.
*
*	Output : Target object or NULL if not found.
*/
PUBLIC Target* GetTarget(IN char* name) {

	int index;
	Target* target;

	index = Hash(name) % TARGETTABLE_MAX;
	target = _targetTable[index];
	if (!target)
		return NULL;
	for (; target; target = target->next) {
		if (!strcmp(target->name, name))
			return target;
	}
	return NULL;
}

/**
*	AddTarget
*
*	Description : Adds a new target to the target table.
*
*	Input : name - Name of target.
*	        block - Pointer to first build block.
*
*	Output : Target object or NULL if error.
*/
PUBLIC Target* AddTarget(IN char* name, IN BuildBlock* block) {

	Target* target = GetTarget(name);
	if (!target)
		target = PutTarget(name);
	if (!target->buildList)
		target->buildList = NewBuildList();
	AddBuildBlock(target->buildList, block);
	return target;
}

/**
*	AddTempTarget
*
*	Description : Adds a new target to the target table.
*	Temporary targets are created by inferred dependents
*	when rules are used. They have a timestamp of 0 and have
*	no build list.
*
*	Input : name - Name of target.
*
*	Output : Target object or NULL if error.
*/
PUBLIC Target* AddTempTarget(IN char* name) {

	return PutTarget(name);
}
