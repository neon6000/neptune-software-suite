/************************************************************************
*
*	buffer.c - Dynamic buffer.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "base.h"
#include <assert.h>
#include <malloc.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#ifdef BUFFER_TMP_WIN32_FIX
#define vsnprintf _vsnprintf
#endif

/* Private Definitions ****************************/

#define BUFFER_SIZE 8

/**
*	BufferFree
*
*	Description : Release memory.
*
*	Input : r - Buffer object to free.
*/
PRIVATE void Resize(IN Buffer* b) {

	int newsize = b->allocCount * 2;
	uint8_t* data = realloc(b->data, newsize);
	b->data = data;
	b->allocCount = newsize;
}

/* Public Definitions ****************************/

/**
*	BufferInit
*
*	Description : Initializes a Buffer object.

*	Input : buffer - Pointer to Buffer object to initialize.
*/
PUBLIC void BufferInit(IN Buffer* buffer) {

	buffer->data = realloc(NULL, 8);
	buffer->allocCount = 8;
	buffer->elementCount = 0;
}

/**
*	NewBuffer
*
*	Description : Allocates a new Buffer object.
*
*	Output - New Buffer object or NULL on error.
*/
PUBLIC Buffer* NewBuffer(void) {

	Buffer* r = malloc(sizeof(Buffer));
	r->data = malloc(BUFFER_SIZE);
	r->allocCount = BUFFER_SIZE;
	r->elementCount = 0;
	return r;
}

/**
*	BufferFree
*
*	Description : Release memory.
*
*	Input : r - Buffer object to free.
*/
PUBLIC void BufferFree(IN Buffer* r) {

	if (!r)
		return;
	free(r->data);
	free(r);
}

/**
*	GetBufferData
*
*	Description : Returns a pointer to the buffer data.
*
*	Input : b - Pointer to Buffer Object to get data from.
*
*	Output : Pointer to internal buffer data.
*/
PUBLIC uint8_t* GetBufferData(IN Buffer* b) {

	return b->data;
}

/**
*	GetBufferLength
*
*	Description : Returns the number of elements added to the
*	buffer.
*
*	Input : b - Pointer to Buffer Object to get data from.
*
*	Output : Number of elements added to the buffer.
*/
PUBLIC size_t GetBufferLength(IN Buffer* b) {

	return b->elementCount;
}

/**
*	BufferWrite
*
*	Description : Writes data to the Buffer object.
*
*	Input : b - Pointer to Buffer Object to write to.
*	        c - Character to write to Buffer.
*/
PUBLIC void BufferWrite(IN Buffer* b, IN char c) {

	if (b->allocCount == (b->elementCount))
		Resize(b);
	b->data[b->elementCount++] = c;
}

/**
*	BufferPush
*
*	Description : Writes data to the Buffer object.
*
*	Input : b - Pointer to Buffer Object to write to.
*	        c - Character to write to Buffer.
*/
PUBLIC void BufferPush(IN Buffer* b, IN char c) {

	BufferWrite(b, c);
}

/**
*	BufferPop
*
*	Description : Removes the last element added to
*	the Buffer object.
*
*	Input : b - Pointer to Buffer Object to write to.
*
*	Output : Last element added to the Buffer object.
*/
PUBLIC char BufferPop(IN Buffer* b) {

	assert(b->elementCount != 0);
	return b->data[--b->elementCount];
}

/**
*	BufferAppend
*
*	Description : Appends a string to the end of the Buffer object.
*
*	Input : b - Pointer to Buffer Object to write to.
*	        s - Pointer to string to append.
*	        len - Number of bytes to copy from "s".
*/
PUBLIC void BufferAppend(IN Buffer* b, IN char* s, IN int len) {

	for (int i = 0; i < len; i++)
		BufferWrite(b, s[i]);
}

/**
*	BufferPop
*
*	Description : Appends formatted string to Buffer object. Note that
*	this does not append a null terminator so can be used for repeated calls
*	to build up a string.
*
*	Input : b - Pointer to Buffer Object to write to.
*	        fmt - String to append.
*	        ... - Variable argument list to substitute format specifiers with.
*/
PUBLIC void BufferPrintf(IN Buffer* b, IN char* fmt, ...) {

	va_list args = NULL;
	while (TRUE) {

		int avail = b->allocCount - b->elementCount;
		int written;

		va_start(args, fmt);
		written = vsnprintf(b->data + b->elementCount, avail, fmt, args);
		if (written == -1) {
			Resize(b);
			continue;
		}
		va_end(args);
		b->elementCount += written;
		return;
	}
}
