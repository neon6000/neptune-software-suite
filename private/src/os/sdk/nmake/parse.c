/************************************************************************
*
*	parse.c - Parser
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <assert.h>
#include <malloc.h>
#include <string.h>
#include <ctype.h>
#include "nmake.h"

PRIVATE Scanner* _scan;
PRIVATE Token    _current;
PRIVATE Target*  _firstTarget;

PRIVATE Token Get(void) {
	_current = _scan->get();
	return _current;
}

PRIVATE BOOL Accept(IN TokenType type) {
	return _current.type == type;
}

PRIVATE Token GetString(void) {
	_current = _scan->string();
	return _current;
}

PRIVATE Token GetValue(void) {
	_current = _scan->value();
	return _current;
}

PRIVATE void Unget(Token tok) {
	_scan->unget(tok);
}

PRIVATE Token Peek(IN int index) {

	Token tok[5];
	assert(index < 5);
	for (int i = 0; i < index; i++)
		tok[i] = _scan->get();
	for (int i = index-1; i >= 0; i--)
		_scan->unget(tok[i]);
	return tok[index-1];
}

/*
	NameList: = name NameList
				| // epsilon
				;
*/
PRIVATE StringList* NameList(void) {

	Token tok;
	StringList* names = NULL;
	while (TRUE) {
		char* s;
		tok = Get();
		if (tok.type != TokenName)
			break;
		if (!names)
			names = NewStringList();
		s = Expand(tok.val, FALSE);
		char* p = strtok(s, " \t");
		AddString(names, p);
		while (p = strtok(NULL, " \t"))
			AddString(names, p);
		free(tok.val);
	};
	Unget(tok);
	return names;
}

/*
	MoreBuildLines := NewLineWhitespace string MoreBuildLines
*/
PRIVATE void MoreBuildLines(IN StringList* out) {

	Token tok;
	while (TRUE) {
		Token commandString;
		tok = Get();
		if (tok.type != TokenNewLineSpace)
			break;
		commandString = GetString();
		AddString(out, commandString.val);
	}
	Unget(tok);
}

/*
	Commands: = MoreBuildLines
		| semicolon string MoreBuildLines
		| // epsilon
		;
*/
PRIVATE StringList* Commands(void) {

	StringList* list = NewStringList();
	Token tok = Peek(1);
	if (tok.type == TokenSemicolon) {
		Get();
		Token cmd = GetString();
		AddString(list, cmd.val);
		MoreBuildLines(list);
		return list;
	}
	MoreBuildLines(list);
	return list;
}

/*
	BuildInfo := NameList Commands
				| // epsilon
				;
*/
PRIVATE BuildBlock* BuildInfo(void) {

	BuildBlock* block = ALLOC_OBJ(BuildBlock);
	block->datetime = (time_t)0;
	block->flags = 0;
	block->dependents = NameList();
	block->buildCommands = Commands();
	return block;
}

/*
	Makefile := BlankLines Makefile
			| newline name Body Makefile
			| // epsilon

	Body	:= NameList Separator BuildInfo
			| equals value
			;
	This consumes the "name" token from Makefile():
*/
PRIVATE void Body(void) {

	int flags;
	Token eq;
	Token value;
	Token target;
	StringList* nameList;
	StringList* current;
	BuildBlock* block;

	// macro_name = <value>
	if (Peek(2).type == TokenEquals) {
		target = Get();
		eq = Get();
		value = GetValue();
		DefineMacro(target.val, value.val);
		return;
	}

	// <name_list> <separator> <buildInfo>:
	nameList = NameList();
	eq = Get();
	block = NULL;
	flags = 0;

	if (ON(_globalFlags, DISPLAY_FILE_DATES))
		SET(flags, TARGET_DISPLAY_FILE_DATES);

	if (ON(_globalFlags, FORCE_BUILD))
		SET(flags, TARGET_FORCE_BUILD);

	if (Accept(TokenColon)) {
		block = BuildInfo();
	}
	else if (Accept(TokenDoubleColon)) {
		block = BuildInfo();
		SET(flags, TARGET_DOUBLECOLON);
		SET(block->flags, TARGET_DOUBLECOLON);
	}
	else
		MakeError("Missing separator ':' or '::");

	if (IsRule(nameList->text)) {
		if (nameList->next)
			MakeError("Too many names in rule");
		AddRule(nameList->text, block->buildCommands);
	}
	else{
		for (current = nameList; current; current = current->next)
			AddTarget(current->text, block);
		if (!_firstTarget) {
			_firstTarget = GetTarget(nameList->text);
			_firstTarget->next = NULL;
		}
	}
}

/*
	BlankLines := newline BlankLines
		| newLineWhitespace BlankLines
		| // epsilon
		;

	Makefile := BlankLines Makefile
		| newline name Body Makefile
		| // epsilon
		;
*/
PRIVATE void Makefile(void) {

	while (TRUE) {
		Get();
		if (Accept(TokenEof))
			break;
		else if (Accept(TokenNewLine) && Peek(1).type == TokenName) {
			Body();
			continue;
		}
		else if (Accept(TokenNewLine))
			continue;
		MakeError("%z: Unexpected token '%t'", _current.loc.src, &_current);
	}
}

PRIVATE void Initialize(IN Scanner* scan) {

	_scan = scan;
}

PUBLIC Target* GetFirstTarget(void) {

	return _firstTarget;
}

PRIVATE Parser _parser = { Initialize, Makefile };

PUBLIC Parser* GetParser(void) {

	return &_parser;
}
