/************************************************************************
*
*	lex.c - Scanner
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <malloc.h>
#include <assert.h>
#include <string.h>
#include "nmake.h"

/* Private Definitions ************************/

PRIVATE Preprocessor* _fm;

/**
*	ScanOperator
*
*	Description : Scans the next operator from the input stream.
*
*	Output : Token type detected.
*/
PRIVATE TokenType ScanOperator(void) {

	int op = _fm->getc();
	if (op == ':') {
		int next = _fm->peek(1);
		if (next == ':') {
			_fm->getc(); // consume '::'
			return TokenDoubleColon;
		}
		else if (next == '=') {
			_fm->getc(); // consume ':='
			return TokenEquals2;
		}
		return TokenColon;
	}
	else if (op == ';')
		return TokenSemicolon;
	else if (op == '=')
		return TokenEquals;

	// does not matter what we return here -- this is an error:
	MakeError("Unknown operator");
	return TokenNewLine;
}

/**
*	ScanComment
*
*	Description : Scans the next comment character from the input stream.
*	This method consumes the comment and disreguards it.
*/
PRIVATE void ScanComment(void) {

	int hash = _fm->getc();
	char* line = _fm->getline(0);
	free(line);
}

/**
*	IsSpace
*
*	Description : Test if character is a space character.
*
*	Input : c - Character to test.
*
*	Output : TRUE is space or tab character, FALSE if not.
*/
PRIVATE BOOL IsSpace(IN int c) {

	return c == ' ' || c == '\t';
}

/**
*	ScanNewLine
*
*	Description : Attempt to scan a new line token. This may
*	scan a TokenNewLine or TokenNewLineSpace type.
*
*	Output : Detected token type.
*/
PRIVATE TokenType ScanNewLine(void) {

	int newline = _fm->getc();
	if (IsSpace(_fm->peek(1))) {
		int c = EOF;
		while (TRUE) {
			c = _fm->getc();
			if (!IsSpace(c))
				break;
		}
		_fm->ungetc(c);
		return TokenNewLineSpace;
	}
	return TokenNewLine;
}

/**
*	ScanName
*
*	Description : Attempt to scan a Name token. A Name token
*	is typically delimited by whitespace or a symbol character,
*	unless it is in a path specifier. For example, "a b c" is
*	three Name tokens, however name tokens with path separators
*	'{' '}' may contain spaces in between the {}. For example,
*	"{a b c\foo}hello" is one Name token.
*
*	Output : Pointer to name that was scanned.
*/
PRIVATE char* ScanName(void) {

	int c;
	Buffer s;
	BufferInit(&s);
	for (c = _fm->getc(); !isspace(c) && c != EOF && c != '\n' && c != '=' && c != ':'; c = _fm->getc()) {
		if (c == '{') {
			int k;
			BufferPush(&s, '{');
			for (k = _fm->getc(); k != '}' && k != '\n' && k != EOF; k = _fm->getc())
				BufferPush(&s, k);
			c = k;
		}
		else if (c == '$') {
			// case: $(x_user_macro)
			// case: $$(x_special_macro)
			int a = _fm->getc();
			int b = _fm->getc();
			if (a == '(' || (a == '$' && b == '(')) {
				int k;
				if (a == '(') {
					BufferPrintf(&s, "$(");
					BufferPush(&s, b);
				}
				else BufferPrintf(&s, "$$(");
				for (k = _fm->getc(); k != ')' && k != '\n' && k != EOF; k = _fm->getc())
					BufferPush(&s, k);
				c = k;
			}
			else{
				_fm->ungetc(b);
				_fm->ungetc(a);
			}
		}
		BufferPush(&s, c);
	}
	_fm->ungetc(c);
	BufferPush(&s, '\0');
	return s.data;
}

/**
*	ScanString
*
*	Description : Attempt to scan a String token. A String
*	token is used for Command lists for targets and rules.
*	It starts at the current file position and continues until
*	EOL or EOF. Note that comment character '#' is not
*	ignored here and is a part of the String.
*
*	Output : Token that was scanned.
*/
PRIVATE Token ScanString(void) {

	Token tok;
	char* s;

	s = _fm->getline(0);
	tok.loc.x = 0;
	tok.loc.y = 0;
	tok.type = TokenValue;
	tok.val = s;
	return tok;
}

/**
*	ScanValue
*
*	Description : Attempt to scan a Value token. A Value
*	token is used for macro values. It starts at the current
*	file position and continues until EOL, EOF, or comment ('#').
*
*	Output : Token that was scanned.
*/
PRIVATE Token ScanValue(void) {

	Token tok;
	char* s;
	// values are the same as strings, however
	// comments are stripped. So stop reading at '#':
	s = _fm->getline('#');
	tok.loc.x = 0;
	tok.loc.y = 0;
	tok.type = TokenValue;
	tok.val = s; // ExpandUserMacros(s);
//	free(s);
	return tok;
}

/**
*	SkipWhitespace
*
*	Description : Skip characters until the first non-whitespace
*	character or EOF is reached.
*/
PRIVATE void SkipWhitespace(void) {

	int c = _fm->getc();
	while (c == ' ' || c == '\t')
		c = _fm->getc();
	_fm->ungetc(c);
}

PRIVATE Token _pushback[10];
PRIVATE int   _pushbackTop;

/**
*	ScanGet
*
*	Description : Get the next token from the character stream.
*	Tokens might come from the following:
*
*		1. If any tokens where put back, they are returned in reverse order.
*		2. If the pushback buffer is empty, scan a new token from the top of
*		file stack.
*		3. If nothing is in the pushback buffer and no more files, return EOF.
*
*	Output : Scanned Token.
*/
PRIVATE Token ScanGet(void) {

	int c;
	File* f;
	Token tok;
	char* val;
	TokenType type;
	FileManager* fm;

	if (_pushbackTop > 0)
		return _pushback[--_pushbackTop];

try_again:

	SkipWhitespace();

	c = _fm->peek(1);
	val = NULL;
	tok.expandOnce = FALSE;

	if (c == EOF) {
		type = TokenEof;
	}
	else if (c == '#') {
		ScanComment();
		goto try_again;
	}
	else if (c == '\n') {
		type = ScanNewLine();
	}
	else if (c == ':' || c == ';' || c == '=') {
		type = ScanOperator();
		if (type == TokenEquals2) {
			type = TokenEquals;
			tok.expandOnce = TRUE;
		}
	}
	else{
		val = ScanName();
		type = TokenName;
	}

	fm = GetFileManager();
	f = fm->top();

	tok.type = type;
	tok.val = val;
	tok.loc.src = f;
	tok.loc.x = 0;
	tok.loc.y = f != NULL ? f->loc.y : 0;
	return tok;
}

/**
*	ScanUnget
*
*	Description : Unget a token. This puts the token
*	in the pushback buffer.
*/
PRIVATE void ScanUnget(IN Token token) {

	assert(_pushbackTop < 10);
	_pushback[_pushbackTop++] = token;
}

/**
*	ScanInit
*
*	Description : Initializes the scanner.
*
*	Input : pproc - Pointer to Preprocessor.
*/
PRIVATE void ScanInit(IN Preprocessor* pproc) {

	_fm = pproc;
}

PRIVATE Scanner _scan = { ScanInit, ScanGet, ScanUnget, ScanString, ScanValue };

/* Public Definitions **********************/

/**
*	PrintToken
*
*	Description : Prints out the token type.
*/
PUBLIC void PrintToken(IN TokenType type) {
	switch (type) {
	case TokenName: printf("\n\rTokenName"); break;
	case TokenNewLine: printf("\n\rTokenNewLine"); break;
	case TokenNewLineSpace: printf("\n\rTokenNewLineSpace"); break;
	case TokenColon: printf("\n\rTokenColon"); break;
	case TokenDoubleColon: printf("\n\rTokenDoubleColon"); break;
	case TokenSemicolon: printf("\n\rTokenSemicolon"); break;
	case TokenEquals: printf("\n\rTokenEquals"); break;
	case TokenString: printf("\n\rTokenString"); break;
	case TokenValue: printf("\n\rTokenValue"); break;
	case TokenEof: printf("\n\rTokenEof"); break;
	};
}

/**
*	PrintToken
*
*	Description : Prints out the token type.
*/
PUBLIC Scanner* GetScanner(void) {
	return &_scan;
}

/**
*	Tok2Str
*
*	Description : Converts a token object to a string for display.
*
*	Input : tok - Pointer to Token object.
*
*	Output : String representation of the token.
*/
PUBLIC char* Tok2Str(IN Token* tok) {

	switch (tok->type) {
	case TokenEof: return "EOF";
	case TokenNewLine: return "Newline";
	case TokenNewLineSpace: return "TokenNewLineSpace";
	case TokenColon: return ":";
	case TokenDoubleColon: return "::";
	case TokenSemicolon: return ";";
	case TokenEquals: return "=";
	case TokenEquals2: return ":=";
	case TokenName:
	case TokenString:
	case TokenValue: return tok->val;
	};
	return "<invalid>";
}
