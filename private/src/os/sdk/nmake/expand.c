/************************************************************************
*
*	expand.c
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <malloc.h>
#include <string.h>
#include <ctype.h>
#include "nmake.h"

/* Public Globals ************************/

// these are set in action.c as needed:
PUBLIC char*       _dollarStar;
PUBLIC StringList* _dollarStarStar;
PUBLIC char*       _dollarAt;
PUBLIC char*       _dollarDollarAt;		// Valid only as dependent in a dependency.
PUBLIC char*       _dollarLessThan;     // Valid only in commands in inference rules.
PUBLIC StringList* _dollarQuestion;

/* Private Definitions ********************/

typedef struct MacroToken {
	char*     name;
	char      modifier;
	MacroType type;
	char*     replaceWhat;
	char*     replaceWith;
}MacroToken;

/**
*	ReplaceString
*
*	Description : Given an origin string, replace the substring specified
*	by "rep" with "with". Return a pointer to the new string. If "orig"
*	or "rep" is NULL, return NULL.
*
*	Input : orig - Original string.
*	        rep - Substring of "orig" to replace.
*	        with - String to replace with.
*
*	Output : Pointer to new string or NULL if "rep" or "orig" is NULL.
*/
PRIVATE char* ReplaceString (IN char *orig, IN char *rep, IN char *with) {

	char* result;    // the return string
	char* ins;       // the next insert point
	char* tmp;       // varies
	int   len_rep;   // length of rep (the string to remove)
	int   len_with;  // length of with (the string to replace rep with)
	int   len_front; // distance between rep and end of last rep
	int   count;     // number of replacements

	// sanity checks and initialization
	if (!orig || !rep)
		return NULL;
	len_rep = strlen(rep);
	if (len_rep == 0)
		return NULL; // empty rep causes infinite loop during count
	if (!with)
		with = "";
	len_with = strlen(with);

	// count the number of replacements needed
	ins = orig;
	for (count = 0; tmp = strstr(ins, rep); ++count) {
		ins = tmp + len_rep;
	}

	tmp = result = malloc(strlen(orig) + (len_with - len_rep) * count + 1);

	if (!result)
		return NULL;

	// first time through the loop, all the variable are set correctly
	// from here on,
	//    tmp points to the end of the result string
	//    ins points to the next occurrence of rep in orig
	//    orig points to the remainder of orig after "end of rep"
	while (count--) {
		ins = strstr(orig, rep);
		len_front = ins - orig;
		tmp = strncpy(tmp, orig, len_front) + len_front;
		tmp = strcpy(tmp, with) + len_with;
		orig += len_front + len_rep; // move to next "end of rep"
	}
	strcpy(tmp, orig);
	return result;
}


/**
*	DetermineMacroType
*
*	Description : Determine the macro type specified by "value".
*
*	Input : value - String representing the macro invocation. This pointer
*	must point to the initial '$' character. For example, "$(macro_name)" or "$<" etc.
*
*	Output : Macro type.
*/
PRIVATE MacroType DetermineMacroType(IN char* value) {

	char spec[] = "*@<?";
	MacroType type;

	type = INVALID_MACRO;
	if (isalpha(value[1]))
		type = USER_MACRO;
	else if (value[1] == '(' && (value[2] == '_' || isalpha(value[2])))
		type = X_USER_MACRO;
	else if (strchr(spec, value[1]))
		type = SPECIAL_MACRO;
	else if (value[1] == '(' && strchr(spec, value[2]))
		type = X_SPECIAL_MACRO;
	else if (value[1] == '$' && value[2] == '@')
		type = DYNAMIC_MACRO;
	else if (value[1] == '$' && value[2] == '(' && value[3] == '@')
		type = X_DYNAMIC_MACRO;
	else if (value[1] == '$')
		type = DOLLAR_MACRO;

	if (type == X_USER_MACRO || type == X_SPECIAL_MACRO || type == X_DYNAMIC_MACRO) {
		for (char* s = value; *s; s++) {
			if (*s == ')')
				return type;
		}
		MakeError("Missing ')' in macro: %s", value);
	}
	return type;
}

/**
*	IsModifier
*
*	Description : Test if the specified character is a filename modifier character.
*
*	Input : c - Character to test.
*
*	Output : TRUE if "c" is a filename modifier character.
*/
PRIVATE BOOL IsModifier(IN char c) {

	return c == 'B' || c == 'F' || c == 'R' || c == 'D';
}

/**
*	ConsumeMacro
*
*	Description : Given a pointer to a macro invocation, this returns
*	a pointer to one character past the end of the invocation.
*
*	Input : value - String pointing to the initial '$' of the macro invocation.
*
*	Output : Pointer to "value" that is one character past the end of the macro
*	invocation.
*/
PRIVATE char* ConsumeMacro(IN char* value) {

	MacroType type = DetermineMacroType(value);
	switch (type) {
	case USER_MACRO:
		return value + 2; // $<character>
	case DYNAMIC_MACRO:
		return value + 3; // $$@
	case SPECIAL_MACRO:
		if (value[1] == '*' && value[2] == '*')
			return value + 3; // $**
		return value + 2; // $* $@ $? $< $**
	case DOLLAR_MACRO:
		return value + 2; // $$
	case X_DYNAMIC_MACRO:
	case X_SPECIAL_MACRO:
	case X_USER_MACRO:
		while (*value && *value != ')')
			value++; // skip to ')'
		return value + 1;
	}
	return value;
}

/**
*	GetMacroToken
*
*	Description : This parses the given "value" for the macro name and string substitution
*	components and outputs them to "macro".
*
*	Input : value - String pointing to the initial '$' of the macro invocation.
*	        macro - Output paramater to store the macro information obtained from "value".
*	        special - Set to TRUE to expand special macros, FALSE otherwise.
*/
PRIVATE void GetMacroToken(IN char* value, OUT MacroToken* macro, IN BOOL special) {

	char* s = value;
	MacroType type = DetermineMacroType(value);
	memset(macro, 0, sizeof(MacroToken));
	macro->replaceWhat = "";
	macro->replaceWith = "";
	if (type == USER_MACRO) {
		char name[2];
		name[0] = value[1];
		name[1] = '\0';
		macro->name = _strdup(name);
	}
	else if (type == X_USER_MACRO) {
		Buffer buf;
		BufferInit(&buf);
		for (char* s = value + 2; *s && *s != ')' && *s != ':'; s++)
			BufferPush(&buf, *s);
		BufferPush(&buf, '\0');
		macro->name = buf.data;
	}
	if (type == X_USER_MACRO || type == X_DYNAMIC_MACRO || type == X_SPECIAL_MACRO) {
		char* s;
		for (s = value; *s && *s != ')' && *s != ':'; s++)
			;
		if (*s++ == ':') {
			Buffer replaceWhat;
			Buffer replaceWith;
			BufferInit(&replaceWhat);
			BufferInit(&replaceWith);
			for (; *s && *s != ')' && *s != '='; s++)
				BufferPush(&replaceWhat, *s);
			if (*s++ == '=') {
				for (; *s && *s != ')'; s++)
					BufferPush(&replaceWith, *s);
			}
			BufferPush(&replaceWhat, '\0');
			BufferPush(&replaceWith, '\0');
			macro->replaceWhat = replaceWhat.data;
			macro->replaceWith = replaceWith.data;
		}
	}
	if (type == X_SPECIAL_MACRO)
		macro->modifier = value[3];
	else if (type == X_DYNAMIC_MACRO)
		macro->modifier = value[4];

	if (special && macro->modifier != 0 && !IsModifier(macro->modifier))
		MakeError("Invalid macro modifier specified: '%c' for '%s'", macro->modifier, value);
	macro->type = type;
}

/**
*	ExpandSpecialMacro
*
*	Description : Expands the given special macro. Special macros are expanded
*	to the values specified in the global variables _dollarStar, _dollarAt,
*	_dollarDollarAt, _dollarQuestion, _dollarStarStar, and _dollarLessThan.
*	If there is an attempt to expand a macro to a NULL string, this will
*	raise an error "undefined special macro".
*
*	Input : type - Macro type.
*	        value - Pointer to the initial '$' of the macro invocation.
*	        buf - Pointer to resulting buffer to receive the expanded string.
*/
PRIVATE void ExpandSpecialMacro(IN MacroType type, IN char* value, OUT Buffer* buf) {

	StringList* s;

	switch (type) {
	case X_SPECIAL_MACRO:
	case SPECIAL_MACRO:
		if (value[1] == '*' && value[2] == '*') {
			if (!_dollarStarStar)
				MakeError("Special macro undefined: $**");
			for (s = _dollarStarStar; s; s = s->next)
				BufferPrintf(buf, "%s ", s->text);
		}
		else if (value[1] == '*' && _dollarStar)
			BufferPrintf(buf, "%s", _dollarStar);
		else if (value[1] == '@' && _dollarAt)
			BufferPrintf(buf, "%s", _dollarAt);
		else if (value[1] == '?' && _dollarQuestion) {
			for (s = _dollarQuestion; s; s = s->next)
				BufferPrintf(buf, "%s ", s->text);
		}
		else if (value[1] == '<' && _dollarLessThan)
			BufferPrintf(buf, "%s", _dollarLessThan);
		else {
			char name[4];
			memset(name, 0, 5);
			name[0] = value[0];
			name[1] = value[1];
			if (value[1] == '*' && value[2] == '*')
				name[2] = value[2];
			MakeError("Special macro undefined: %s", name);
		}
		return;
	case X_DYNAMIC_MACRO:
	case DYNAMIC_MACRO:
		if (!_dollarDollarAt)
			MakeError("Special macro undefined: $$@");
		BufferPrintf(buf, "%s", _dollarDollarAt);
		return;
	case DOLLAR_MACRO:
		BufferPrintf(buf, "$");
		return;
	}
}

/**
* _Expand
*
* Description :
*
*	This procedure expands all macros specified by "value". If "special" is TRUE,
*	this also attempts to expand special macros. This function is RECURSIVE. It
*	will continue to expand macros until there is nothing else to expand.
*
*	Call as :
*		_Expand(NULL, value, hideset, &result, "", "", special, 0, 0);
*
*	Input : name - Name of the parent macro in recursive calls or NULL.
*	        value - Pointer to the string to expand macros for.
*	        hideset - Pointer to a hideset to watch for circular macro references.
*	        buf - Output buffer to receive the resulting expanded macro value.
*	        replaceWhat - Used with recursive calls; used for string substitution.
*	        replaceWith - Used with recursive calls; used for string substitutio.
*	        special - Set to TRUE to expand special macros, FALSE otherwise.
*	        reclevel - Used for self referential macros.
*	        recseen - Used for self referential macros.
*/
PRIVATE void _Expand(IN char* name, IN char* value, IN Set* hideset,
	OUT Buffer* buf, IN char* replaceWhat, IN char* replaceWith,
	IN BOOL special, IN int reclevel, IN int recseen) {

	for (char* s = value; *s; s++) {
		if (*s == '$') {
			MacroToken tok;
			Macro* m = NULL;
			char* expanded = NULL;
			char* afterExpand = NULL;
			GetMacroToken(s, &tok, special);
			switch (tok.type) {
			case USER_MACRO:
			case X_USER_MACRO:
				m = GetMacro(tok.name);
				if (m) {
					// if this is a self referential macro:
					if (name && !strcmp(name, m->name)) {
						StringList* s = m->values;
						for (unsigned i = reclevel; i != recseen && s; --i)
							s = s->next;
						expanded = s ? s->text : "";
					}
					// if this is some macro already in the hideset:
					else if (SetFind(hideset, m->name))
						MakeError("Cycle in macro definition '%s'", m->name);
					// this is a new macro:
					else {
						expanded = m->values->text;
					}
					Buffer tmp;
					BufferInit(&tmp);
					_Expand(m->name, expanded, SetInsert(hideset, tok.name), &tmp,
						tok.replaceWhat, tok.replaceWith, special, reclevel + 1,
						(name && strcmp(name, m->name)) ? reclevel : recseen);
					BufferPush(&tmp, '\0');
					s = ConsumeMacro(s);
					s--;
					char* p = ReplaceString(tmp.data, replaceWhat, replaceWith);
					if (!p)
						p = tmp.data;
					BufferPrintf(buf, "%s", p);
					break;
				}
				else{
					// unknown macro expands to empty string:
					s = ConsumeMacro(s);
					s--;
					break;
				}
			case SPECIAL_MACRO:
			case DYNAMIC_MACRO:
			case DOLLAR_MACRO:
			case X_SPECIAL_MACRO:
			case X_DYNAMIC_MACRO:

				// if we are not expanding special macros, skip over it:
				if (!special) {
					BufferPush(buf, '$');
					break;
				}
				ExpandSpecialMacro(tok.type, s, buf);
				s = ConsumeMacro(s);
				s--;
				break;
			};
			continue;
		}
		if (strlen(replaceWhat) > 0 && !strncmp(s, replaceWhat, strlen(replaceWhat))) {
			BufferPrintf(buf, "%s", replaceWith);
			s += strlen(replaceWhat) - 1;
		}
		else
			BufferPush(buf, *s);
	}
}

/* Public Definitions ********************/

/**
*	IsPathSeparator
*
*	Description : Test if the specified character is a path separator.
*
*	Input : c - Character to test.
*
*	Output : TRUE if this is a path separator character.
*/
PUBLIC BOOL IsPathSeparator(IN char c) {

	return (c == '/' || c == '\\');
}

/**
*	FindFirstPathSeparator
*
*	Description : Returns a pointer to the first path
*	separator found in "s" or NULL if it does not exist.
*
*	Input : s - Input string to search in.
*
*	Output : Pointer to first path separator or NULL if not found.
*/
PUBLIC char* FindFirstPathSeparator(IN char* s) {

	char* slash = strchr(s, '/');
	char* backslash = strchr(s, '\\');
	if (backslash != NULL && (backslash < slash || slash == NULL))
		slash = backslash;
	return slash;
}

/**
*	FindLastPathSeparator
*
*	Description : Returns a pointer to the last path
*	separator found in "s" or NULL if it does not exist.
*
*	Input : s - Input string to search in.
*
*	Output : Pointer to last path separator or NULL if not found.
*/
PUBLIC char* FindLastPathSeparator(IN char* s) {

	char* slash = strrchr(s, '/');
	char* backslash = strrchr(s, '\\');

	if (backslash > slash)
		slash = backslash;
	return slash;
}

/**
*	Expand
*
*	Description : This procedure expands all macros in "value". If special is TRUE,
*	this will also expand special macros. Self referential macros are
*	also expanded.
*
*	Input : value - Pointer to string to expand macros for.
*	        special - TRUE to expand special macros, FALSE otherwise.
*
*	Output : Pointer to new string containing "value" with all macros
*	         expanded.
*/
PUBLIC char* Expand(IN char* value, BOOL special) {

	Set* hideset;
	Buffer result;

	BufferInit(&result);
	hideset = NULL;

	_Expand(NULL, value, hideset, &result, "", "", special, 0, 0);
	BufferPush(&result, '\0');
	return result.data;
}
