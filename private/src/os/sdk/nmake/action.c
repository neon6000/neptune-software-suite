/************************************************************************
*
*	action.c
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <malloc.h>
#include <string.h>
#include "nmake.h"

/* Private Definitions *************************/

/**
*	NewBuildList
*
*	Description : Allocates a new BuildList object.
*
*	Output : Pointer to BuildLIst object or NULL on error.
*/
PUBLIC BuildList* NewBuildList(void) {

	BuildList* list = ALLOC_OBJ(BuildList);
	list->buildBlock = NULL;
	list->next = NULL;
	return list;
}

/**
*	AddBuildBlock
*
*	Description : Add a BuildBlock to a BuildList.
*
*	Input : list - BuildList to add to.
*	        block - Pointer to build block to add.
*/
PUBLIC void AddBuildBlock(IN BuildList* list, IN BuildBlock* block) {

	BuildList* cur;
	BuildList* node;

	if (!list->buildBlock) {
		list->buildBlock = block;
		list->next = NULL;
		return;
	}

	cur = list;
	while (cur->next)
		cur = cur->next;

	node = ALLOC_OBJ(BuildList);
	node->buildBlock = block;
	node->next = NULL;
	cur->next = node;
}

/**
*	NewStringList
*
*	Description : Allocates a new StringList object.
*
*	Output : Pointer to StringList object or NULL on error.
*/
PUBLIC StringList* NewStringList(void) {

	StringList* list = ALLOC_OBJ(StringList);
	list->text = NULL;
	list->next = NULL;
	return list;
}

/**
*	AddStringFront
*
*	Description : Add a new string to the front of a String List.
*
*	Input : list - Pointer to String List object.
*	        str - Pointer to string to add.
*
*	Output : Pointer to new front of String List object.
*/
PUBLIC StringList* AddStringFront(IN StringList* list, IN char* str) {

	StringList* s = NewStringList();
	s->next = list;
	s->text = str;
	return s;
}

/**
*	AddString
*
*	Description : Add a new string to the end of the String List.
*
*	Input : list - Pointer to String List object.
*	        str - Pointer to string to add.
*/
PUBLIC void AddString(IN StringList* list, IN char* str) {

	StringList* c;
	StringList* node;
	c = list;
	if (!list->text) {
		list->text = str;
		list->next = NULL;
		return;
	}
	while (c->next)
		c = c->next;
	node = NewStringList();
	node->next = NULL;
	node->text = str;
	c->next = node;
}

/**
*	DefineMacro
*
*	Description : Define a new macro and store it in the Macro
*	table. If the macro already exist, this will add the value
*	to the start of the list to insure the correct order for
*	self referential macros.
*
*	Input : name - Pointer to name of macro.
*	        value - Pointer to macro value.
*/
PUBLIC void DefineMacro(IN char* name, IN char* value) {

	StringList* values;
	Macro* macro;

	macro = GetMacro(name);
	if (macro) {
		// must be added in reverse order so its bottom-up.
		// required for correct order in expanding self referential macros
		macro->values = AddStringFront(macro->values, value);
		return;
	}
	values = NewStringList();
	AddString(values, value);
	PutMacro(name, values, 0);
}
