/************************************************************************
*
*	macro.c
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <malloc.h>
#include <string.h>
#include "nmake.h"

/* PRIVATE Definitions ***************************/

#define MACROTABLE_MAX 64
PRIVATE Macro* _macroTable[MACROTABLE_MAX];

/**
*	Hash
*
*	Description : Compute hash of string.
*
*	Input : Pointer to string.
*
*	Output : Computed hash value.
*/
PRIVATE int Hash(IN char* str) {

	int val = 0;
	for (char* p = str; *p; p++)
		val += (int)*p;
	return val;
}

/* PUBLIC Definitons ******************************/

/**
*	PrintMacros
*
*	Description : Display all macros.
*/
PUBLIC void PrintMacros(void) {

	MakeInfo("");
	MakeInfo("MACROS:");
	MakeInfo("");
	for (int i = 0; i < MACROTABLE_MAX; i++) {
		Macro* m = _macroTable[i];
		if (!m)
			continue;
		while (m) {
			MakeInfo("%m", m);
			m = m->next;
		}
	}
}

/**
*	GetMacro
*
*	Description : Given the name of a macro, return
*	the macro object.
*
*	Input : Pointer to name of macro.
*
*	Output : Pointer to macro object or NULL if not found.
*/
PUBLIC Macro* GetMacro(IN char* name) {

	int index;
	Macro* m;

	index = Hash(name) % MACROTABLE_MAX;
	m = _macroTable[index];
	if (!m)
		return NULL;
	for (; m; m = m->next) {
		if (!strcmp(m->name, name))
			return m;
	}
	return NULL;
}

/**
*	PutMacro
*
*	Description : Stores a macro in the macro table.
*
*	Input : name - Pointer to name of macro.
*	        values - List of values assigned to the macro.
*	        flags - Nacro flags.
*/
PUBLIC void PutMacro(IN char* name, IN StringList* values, IN int flags) {

	int index;
	Macro* m;

	index = Hash(name) % MACROTABLE_MAX;
	m = _macroTable[index];
	if (m) {
		Macro* n;
		for (n = m; n->next; n = n->next)
			;
		m = ALLOC_OBJ(Macro);
		m->flags = flags;
		m->next = NULL;
		m->values = values;
		m->name = name;
		n->next = m;
	}
	else{
		m = ALLOC_OBJ(Macro);
		m->flags = flags;
		m->next = NULL;
		m->values = values;
		m->name = name;
		_macroTable[index] = m;
	}
}

/**
*	PutSimpleMacro
*
*	Description : Stores a macro in the macro table.
*
*	Input : name - Pointer to name of macro.
*	        value - Pointer to value to assign to macro.
*	        flags - Nacro flags.
*/
PUBLIC void PutSimpleMacro(IN char* name, IN char* value, IN int flags) {

	StringList* s = NewStringList();
	AddString(s, value);
	PutMacro(name, s, flags);
}
