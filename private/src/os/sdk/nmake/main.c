/************************************************************************
*
*	main.c - Build Maintenance Utility
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/*
	Neptune Make Utility

	This utility can be used to build makefile projects and is a component of the Neptune
	Build Environment.

	Known bugs :

		1. !else does not yet function as expected. Might need to store additional information on the If stack.
		2. Double colon rules -- the datetime stamp of the Target should be MAX(time stamp of all BuildBlocks).
			Time stamp of build blocks = MAX(time stamp of all dependents tied to the Build block.)
*/

#include <direct.h> 
#include <time.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdarg.h>
#include <malloc.h>
#include <stdlib.h>
#include <io.h>
#include "nmake.h"

/* PRIVATE definitions ***********************************************/

typedef enum NasmOptionType {
	OptionA,
	OptionB,
	OptionC,
	OptionD,
	OptionE,
	OptionG,
	OptionHELP,
	OptionI,
	OptionK,
	OptionN,
	OptionNOLOGO,
	OptionP,
	OptionQ,
	OptionR,
	OptionS,
	OptionT,
	OptionU,
	OptionF
}NasmOptionType;

typedef struct Option {
	NasmOptionType type;
	char* cmd; // case insensitive. e.g. /a, -a, /A, -A are all valid.
	char* msg;
}Option;

Option _options[] = {
	{ OptionA, "A", "Build all evaluated targets" },
	{ OptionB, "B", "Build if time stamps are equal" },
	{ OptionC, "C", "Suppress output messages" },
	{ OptionD, "D", "Display build information" },
	{ OptionE, "E", "Override env-var macros" },
	{ OptionG, "G", "Display !include filenames" },
	{ OptionHELP, "HELP", "Display brief usage message" },
	{ OptionI, "I", "Ignore exit codes from commands" },
	{ OptionK, "K", "Build unrelated targets on error" },
	{ OptionN, "N", "Display commands but do not execute" },
	{ OptionNOLOGO, "NOLOGO", "Suppress copyright message" },
	{ OptionP, "P", "Display NMAKE information" },
	{ OptionQ, "Q", "Check time stamps but do not build" },
	{ OptionR, "R", "Ignore predefined rules/macros" },
	{ OptionS, "S", "Suppress executed-commands display" },
	{ OptionT, "T", "Change time stamps but do not build" },
	{ OptionU, "U", "Dump inline files" },
	//	{ "y", "Disable batch-mode" },
	{ OptionHELP, "?", "Display brief usage message" },
	{ OptionF, "F", "Provide file name" }
};

NMakeFlags _globalFlags;

PRIVATE FileManager*  fm;
PRIVATE Preprocessor* pproc;
PRIVATE Scanner*      scan;
PRIVATE Parser*       parse;

PRIVATE char   _nmakeInvokeDirectory[FILENAME_MAX];
PRIVATE char*  _nmakeInvokeCommand;
PRIVATE Set*   _targets;
PRIVATE char*  _makefile;

/**
*	ScanFormatString
*
*	Description : Given an input string and argument list, scan
*	the input string and substitute the format specifiers with
*	the variable arguments.
*
*	Input : s - Pointer to string to scan.
*	        args - Pointer to variable argument list to substitute with.
*
*	Output : Pointer to new allocated string.
*/
PRIVATE char* ScanFormatString(IN char* s, IN va_list* args) {

	Buffer msg;
	Macro* m;
	Token* t;
	File* z;
	time_t w;
	char* mem;

	m = NULL;
	BufferInit(&msg);
	for (; *s; s++) {

		if (*s == '%') {
			s++;
			switch (*s) {
			case 's':
				BufferPrintf(&msg, "%s", va_arg(*args, char*));
				break;
			case 'i':
				BufferPrintf(&msg, "%i", va_arg(*args, int));
				break;
			case 'f':
				BufferPrintf(&msg, "%f", va_arg(*args, float));
				break;
			case 'o':
				BufferPrintf(&msg, "%o", va_arg(*args, unsigned int));
				break;
			case 'u':
				BufferPrintf(&msg, "%u", va_arg(*args, unsigned int));
				break;
			case 'e':
				BufferPrintf(&msg, "%e", va_arg(*args, unsigned int));
				break;
			case 'd':
				BufferPrintf(&msg, "%d", va_arg(*args, unsigned int));
				break;
			case 'c':
				BufferPush(&msg, va_arg(*args, char));
				break;
			case 'x':
				BufferPrintf(&msg, "%x", va_arg(*args, unsigned int));
				break;
			case '%':
				BufferPush(&msg, '%');
				break;
			case 'm':
				m = va_arg(*args, Macro*);
				BufferPrintf(&msg, "MACRO %s", m->name);
				for (StringList* values = m->values; values; values = values->next)
					BufferPrintf(&msg, "\n\t%s ", values->text);
				break;
			case 't':
				t = va_arg(*args, Token*);
				BufferPrintf(&msg, "%s", Tok2Str(t));
				break;
			case 'z':
				z = va_arg(*args, File*);
				if (z)
					BufferPrintf(&msg, "%s:%i", z->fname, z->loc.y);
				else
					BufferPrintf(&msg, "<unknown>:0");
				break;
			case 'w':
				w = va_arg(*args, time_t);
				mem = ctime(&w);
				BufferPrintf(&msg, "%s", mem);
				break;
			}
		}
		else
			BufferPush(&msg, *s);
	}
	BufferPush(&msg, '\0');
	return msg.data;
}

/**
*	GetMakeInvokeDirectory
*
*	Description : Returns a pointer to the directory
*	that NMAKE was invoked in.
*
*	Output : Pointer to string containing the
*	directory path NMAKE was invoked at.
*/
PUBLIC char* GetMakeInvokeDirectory(void) {

	return _nmakeInvokeDirectory;
}

/**
*	GetMakeInvokeCommand
*
*	Description : Returns a pointer to the user
*	invoked command string for invoking NMAKE.
*
*	Output : Pointer to string containing the user
*	invoked command.
*/
PUBLIC char* GetMakeInvokeCommand(void) {

	return _nmakeInvokeCommand;
}

/**
*	GetMakeFlagString
*
*	Description : Returns a pointer to the user 
*	specified flags passed to NMAKE.
*
*	Output : Pointer to string containing the
*	user invoked string.
*/
PUBLIC char* GetMakeFlagString(void) {

	return NULL;
}

/**
*	InheritEnvironmentVariable
*
*	Description : Converts the specified environment variable
*	and adds it to the macro table.
*
*	Input : s - Pointer to string of the form "macro=value".
*/
PRIVATE void InheritEnvironmentVariable(IN char* s) {

	Buffer name;
	Buffer value;

	BufferInit(&name);
	BufferInit(&value);

	while (*s && *s != '=')
		BufferPush(&name, toupper(*s++));
	BufferPush(&name, '\0');
	s++;
	while (*s)
		BufferPush(&value, *s++);
	BufferPush(&value, '\0');

	PutSimpleMacro(name.data, value.data, 0);
}

/**
*	InheritEnvironmentVariables
*
*	Description : Puts all environment variables in the macro table.
*
*	Input : env - Pointer to macro/value pairs.
*/
PRIVATE void InheritEnvironmentVariables(IN char** env) {

	for (; *env; env++)
		InheritEnvironmentVariable(*env);
}

/**
*	PutSpecialMacros
*
*	Description : Puts built-in macros and environment macros
*	in the macro table.
*
*	Input : env - Pointer to macro/value pairs inherited from the
*	environment.
*/
PRIVATE void PutSpecialMacros(IN char** env) {

	if (ON(_globalFlags, IGNORE_PREDEFINED))
		return;

	// special recursion macros:
	PutSimpleMacro("MAKE", GetMakeInvokeCommand(), 0);
	PutSimpleMacro("MAKEDIR", GetMakeInvokeDirectory(), 0);
	PutSimpleMacro("MAKEFLAGS", GetMakeFlagString(), 0);

	// special command macros:
	PutSimpleMacro("AS", "ml", 0);
	PutSimpleMacro("BC", "bc", 0);
	PutSimpleMacro("CC", "cl", 0);
	PutSimpleMacro("CPP", "cl", 0);
	PutSimpleMacro("CXX", "cl", 0);
	PutSimpleMacro("RC", "rc", 0);

	// special options macros:
	PutSimpleMacro("AFLAGS", "", 0);
	PutSimpleMacro("BFLAGS", "", 0);
	PutSimpleMacro("CFLAGS", "", 0);
	PutSimpleMacro("CPPFLAGS", "", 0);
	PutSimpleMacro("CXXFLAGS", "", 0);
	PutSimpleMacro("RFLAGS", "", 0);

	// environment macros:
	InheritEnvironmentVariables(env);
}

/**
*	GetOption
*
*	Description : Return the specified option.
*
*	Input : s - Pointer to command line option
*
*	Output : Pointer to option or NULL if not supported.
*/
PRIVATE Option* GetOption(IN char* s) {

	int max = sizeof(_options) / sizeof(Option);
	for (int i = 0; i < max; i++) {
		if (!_stricmp(_options[i].cmd, s))
			return &_options[i];
	}
	return NULL;
}

/**
*	PrintOptions
*
*	Description : Print out command line options.
*/
PRIVATE void PrintOptions(void) {

	int max = sizeof(_options) / sizeof(Option);
	for (int i = 0; i < max; i++)
		printf("\n\r/%s %s", _options[i].cmd, _options[i].msg);
}

/**
*	ApplyOption
*
*	Description : Applies option to global flags.
*
*	Input : opt - Pointer to command line option.
*/
PRIVATE void ApplyOption(IN Option* opt) {

	switch (opt->type) {
	case OptionA:  SET(_globalFlags, FORCE_BUILD); break;
	case OptionB:  SET(_globalFlags, BUILD_IF_EQUAL); break;
	case OptionC: SET(_globalFlags, SUPRESS_OUTPUT); break;
	case OptionD: SET(_globalFlags, DISPLAY_FILE_DATES); break;
	case OptionG: SET(_globalFlags, DISPLAY_INCLUDES); break;
	case OptionHELP: PrintOptions(); break;
	case OptionI: SET(_globalFlags, IGNORE_EXIT_CODES); break;
	case OptionN: SET(_globalFlags, NO_EXECUTE); break;
	case OptionNOLOGO: SET(_globalFlags, NO_LOGO); break;
	case OptionP: SET(_globalFlags, PRINT_INFORMATION); break;
	case OptionQ: SET(_globalFlags, QUESTION_STATUS); break;
	case OptionR: SET(_globalFlags, IGNORE_PREDEFINED); break;
	case OptionS: SET(_globalFlags, NO_ECHO); break;
	case OptionT: SET(_globalFlags, TOUCH_TARGETS); break;
		// Causes environment variables to override makefile macro definitions.
	case OptionE: SET(_globalFlags, USE_ENV_VARS); break;
		//	case OptionK: _nasmFlags.buildUnrelatedTargets = TRUE; break;
		//	case OptionU: _nasmFlags.dumpInlineFiles = TRUE; break;
	}
}

/**
*	ScanCommandLineOptions
*
*	Description : Scan command line optins and applies them.
*
*	Input : argc - Number of command line options
*	        argv - Pointer to array of option values.
*/
PRIVATE void ScanCommandLineOptions(IN int argc, IN char** argv) {

	for (int i = 1; i < argc; i++) {

		char* s = argv[i];
		Option* opt = NULL;

		// support both -flag and /flag syntax:
		if (s[0] == '-' || s[0] == '/') {
			opt = GetOption(&s[1]);
			if (!opt)
				MakeError("Invalid option %s", s);
			if (opt->type == OptionF) {
				if (i == argc-1 || _makefile)
					MakeError("Too many files specified");
				_makefile = argv[++i];
			}
			else
				ApplyOption(opt);
		}
		else {
			_targets = SetInsert(_targets, s);
		}
	}
}

/**
*	DisplayLogo
*
*	Description : Display logo
*/
PRIVATE void DisplayLogo(void) {

	if (ON(_globalFlags, NO_LOGO))
		return;
	printf("\n\rNeptune Make");
	printf("\n\rCopyright(c) BrokenThorn Entertainment, Co\n\r");
}

/* PUBLIC Definitions *****************************************/

/**
*	MakeInfo
*
*	Description : Display information
*
*	Input : s - Pointer to message to display
*	        ... - Additional arguments.
*/
PUBLIC void MakeInfo(IN char* s, ...) {

	char* msg;
	va_list args;

	if (ON(_globalFlags, SUPRESS_OUTPUT))
		return;

	va_start(args, s);
	msg = ScanFormatString(s, &args);
	va_end(args);
	fprintf(stdout, "\n\r%s", msg);
	free(msg);
}

/**
*	MakeError
*
*	Description : Display error and initiate breakpoint.
*
*	Input : s - Pointer to message to display
*	        ... - Additional arguments.
*/
PUBLIC void MakeError(IN char* s, ...) {

	char* msg;
	va_list args;

	va_start(args, s);
	msg = ScanFormatString(s, &args);
	va_end(args);
	fprintf(stderr, "\n\r*** NMAKE Error: %s", msg);
	free(msg);
#ifdef _DEBUG
	__debugbreak();
#endif
	exit(0);
}

/**
*	GetCurrentTime
*
*	Description : Return current time.
*
*	Input : out - Output time block.
*/
PUBLIC void GetCurrentTime(OUT time_t* out) {

	time(out);
}

/**
*	main
*
*	Description : This performs the following:
*
*	    1. Scan command line options in the form /<option> or -option
*	    2. Install build-in macros and inherit environment variables.
*	    3. Parse the makefile to install targets/rules.
*	    4. Build all targets specified by user.
*
*	If no makefile specified in command line, default to "makefile".
*	If no targets specified, build the first target which is set by parser.
*
*	Input : argc - Number of command line operands.
*	        argv - Pointer to array of command line operands.
*	        env - Pointer to array of environment macro=value strings.
*
*	Output : 0 on success.
*/
PUBLIC int main (IN int argc, IN char** argv, IN char** env) {

	File*   file;
	Set*    target;
	time_t  targettime;

	ScanCommandLineOptions(argc, argv);

	_nmakeInvokeCommand = argv[0];
	_getcwd(_nmakeInvokeDirectory, FILENAME_MAX);

	fm    = GetFileManager();
	pproc = GetPreprocessor();
	scan  = GetScanner();
	parse = GetParser();

	DisplayLogo();

	if (!_makefile)
		_makefile = "makefile";

	file = fm->open(_makefile);
	if (!file)
		MakeError("Unable to open file %s", _makefile);

	fm->push (file);
	pproc->init (fm);
	scan->init (pproc);
	parse->init (scan);
	PutSpecialMacros (env);

	parse->parse ();
	fm->close(file);

	if (ON(_globalFlags, PRINT_INFORMATION)) {
		PrintMacros();
		PrintTargets();
		PrintRules();
	}
	
	if (!GetFirstTarget())
		MakeError("No targets found");

	if (!_targets)
		_targets = SetInsert(_targets, GetFirstTarget()->name);

	for (target = _targets; target; target = target->next) {
		targettime = 0L;
		InvokeBuild(target->s, 0, &targettime, NULL);
	}

#ifdef _DEBUG
	__debugbreak();
#endif
	return 0;
}

// initial code for ninstall -- need to be its own project!
#if 0
int main(IN int argc, IN char** argv) {

	if (argc != 3) {
		printf("syntax: ninstall bootsector image-file");
		return 0;
	}

	FILE* bootrecordFile = fopen(argv[1], "rb");
	FILE* imageFile = fopen(argv[2], "rb+");
	if (!bootrecordFile) {
		printf("unable to open %s", argv[1]);
		return 0;
	}
	if (!imageFile) {
		printf("unable to open %s", argv[2]);
		return;
	}

	// installs boot sector code into image file:
	fseek(bootrecordFile, 0, SEEK_END);
	int bootRecordSize = ftell(bootrecordFile);
	fseek(bootreqcordFile, 0, SEEK_SET);
	unsigned char* bootrecord = malloc(bootRecordSize);
	fread(bootrecord, 1, bootRecordSize, bootrecordFile);
	fclose(bootrecordFile);

	fwrite(bootrecord, bootRecordSize, 1, imageFile);
	fclose(imageFile);
	return 0;
}
#endif
