/************************************************************************
*
*	set.c - Set support
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nasm.h"
#include <assert.h>
#include <string.h>
#include <malloc.h>

/* This represents a set of unique strings. */

PUBLIC PSET SetInsert(IN PSET k, IN char* s) {

	PSET r;

	r = (PSET)malloc(sizeof(SET));
	if (!r)
		return NULL;
	memcpy(r->magic, "SET ", 4);
	r->next = k;
	r->s = s;
	return r;
}

PUBLIC BOOL SetFind(IN PSET k, IN char* s) {

	PSET v;

	if (!k) return FALSE;
	for (v = k; v; v = v->next) {
		if (!v->s)
			continue;
		if (!strcmp(v->s, s))
			return TRUE;
	}
	return FALSE;
}

PUBLIC PSET SetUnion(IN PSET a, IN PSET b) {

	PSET k = b;
	for (; a; a = a->next) {
		if (!SetFind(b, a->s))
			k = SetInsert(k, a->s);
	}
	return k;
}

PUBLIC PSET SetIntersect(IN PSET a, IN PSET b) {

	PSET k = NULL;
	for (; a; a = a->next) {
		if (SetFind(b, a->s))
			k = SetInsert(k, a->s);
	}
	return k;
}

PUBLIC void SetFree(IN PSET a) {

	PSET k;

	while (a) {
		k = a;
		a = a->next;
		free(k);
	}
}
