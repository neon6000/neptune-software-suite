/************************************************************************
*
*	preproc.c - Preprocessor
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <assert.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include "instr.h"
#define IN
#define OUT

typedef SET IncludeFileSet;

IncludeFileSet _includeFiles;
IncludeFileSet* _includeFilePaths;

//
// PARSER sets to TRUE on first token on line.
// ALL other cases, PARSER sets to FALSE.
//
PUBLIC BOOL _lineStart;

PRIVATE void UngetTokens(IN PVECTOR tokens);

typedef MAP MacroTable;

typedef enum MACROTYPE {
	MACRO_OBJECT,
	MACRO_FUNCTION,
	MACRO_MULTILINE
}MACROTYPE;

typedef struct MACRO {
	char      magic[6];
	PSTRING   name;
	MACROTYPE type;
	int       operandCount;
	PVECTOR   body;
	index_t   curtok;
	BOOL      isVariableArgument;
}MACRO, *PMACRO;

PUBLIC PMACRO MacroGet(IN PSTRING name);

/*
	Used with %push and %pop to store the
	current context and local scope.
*/
typedef struct SCOPE *PSCOPE;
typedef struct SCOPE {
	char        magic[8];
	char*       name;
	MacroTable* macros;
	PSCOPE    parent;
}SCOPE, *PSCOPE;

#define IF_DISABLE 0
#define IF_ENABLE  1

typedef VECTOR SCOPESTACK, *PSCOPESTACK;
typedef VECTOR IFSTACK, *PIFSTACK;

PRIVATE PSCOPESTACK   _scopeStack;
PRIVATE PSCOPE        _scope;
PRIVATE PIFSTACK      _ifStack;

typedef struct ARG ARG, *PARG;
typedef struct ARG {
	PVECTOR tokens;
	index_t position;
	PARG    next;
}ARG, *PARG;

typedef struct ARGLIST {
	PARG   first;
	size_t count;
}ARGLIST, *PARGLIST;

PRIVATE long GetIfState(void) {

	long state;

	if (!VectorLastElement(_ifStack, &state))
		return state;
	return IF_ENABLE;
}

/**
*	FreeArgs
*
*	Description : Free argument list
*
*	Input : args - Argument list to free
*/
PRIVATE void FreeArgs(IN PARGLIST args) {

	PARG arg;
	PARG next;

	if (!args) return;

	for (arg = args->first; arg; arg = next) {
		next = arg->next;
		FreeVector(arg->tokens);
		free(arg);
	}
	free(args);
}

/**
*	GetArg
*
*	Description : Get an argument based on its "position".
*	The position is obtained from TokenMacroOperand token types
*
*	Input : args - Argument list
*	        idx - Position
*
*	Output : Token vector associated with this argument
*/
PRIVATE PVECTOR GetArg(IN PARGLIST args, IN index_t idx) {

	PARG arg;

	for (arg = args->first; arg; arg = arg->next) {
		if (arg->position == idx)
			return arg->tokens;
	}
	return NULL;
}

/**
*	AddArg
*
*	Description : Adds an argument to an argument list
*
*	Input : args - Argument list
*	        arg - Argument to add
*/
PRIVATE void AddArg(IN PARGLIST args, PARG arg) {

	// the position is stored in "arg" so order doesn't matter
	// so keep it O(1)
	arg->next = args->first;
	args->first = arg;
	args->count++;
}

/**
*	NewArgList
*
*	Description : Creates a new argument list
*
*	Output : Empty argument list object
*/
PRIVATE PARGLIST NewArgList(void) {

	PARGLIST args;

	args = malloc(sizeof(ARGLIST));
	args->count = 0;
	args->first = NULL;
	return args;
}

/**
*	InHideset
*
*	Description : Test if "name" is in the hide set
*
*	Input : hide - Hideset
*	        name - Name to check
*
*	Output : TRUE if name in hideset, FALSE otherwise
*/
PRIVATE BOOL InHideset(IN PVECTOR hide, IN PSTRING name) {

	PSTRING str;
	index_t max;
	index_t cur;

	str = (PSTRING) hide->data;
	max = VectorLength(hide);

	for (cur = 0; cur < max; cur++, str++) {
		if (!strcmp(str->value, name->value))
			return TRUE;
	}
	return FALSE;
}

/**
*	GetArgument
*
*	Description : Reads a single argument for a maco invocation
*
*	Input : position - The position of this argument (0 based index)
*	        last - If this is the last argument, writes TRUE to this even if "delim" is never reached
*	        delim - Token representing what should be the end of the argument list
*	        acceptNewLine - if TRUE, treat new line as any other character
*
*	Output : Argument or NULL
*/
PRIVATE PARG GetArgument(IN index_t position, OUT BOOL* last, IN TOKENTYPE delim, IN BOOL acceptNewLine) {

	PARG    arg;
	PVECTOR tokens;
	TOKEN   tok;

	tokens = NewVector(0, sizeof(TOKEN));

	while (TRUE) {

		Lex(&tok);
		if (tok.type == TokenEndOfFile) {
			Error("Unexpected EOF when processing argument list.");
			if (tokens) FreeVector(tokens);
			return NULL;
		}
		if (!acceptNewLine && tok.type == TokenEndOfLine) {
			Error("Unexpected EOL when processing argument list.");
			if (tokens) FreeVector(tokens);
			return NULL;
		}
		if (tok.type == TokenComma)
			break;
		if (tok.type == delim) {
			// no more arguments to read:
			*last = TRUE;
			break;
		}
		VectorPush(tokens, &tok);
	}

	arg = malloc(sizeof(ARG));
	arg->position = position;
	arg->tokens = tokens;
	arg->next = NULL;

	return arg;
}

/**
*	GetArgumentList
*
*	Description : Reads the argument list of a macro invocation
*
*	Input : mac - Macro associated with this argument list
*	        delim - Token represents the end of the argument list
*	        acceptNewLine - if TRUE, this will treat newline is any other character
*
*	Output : Argument list or NULL if no arguments
*/
PRIVATE PARGLIST GetArgumentList(IN PMACRO mac, IN TOKENTYPE delim, IN BOOL acceptNewLine) {

	PARGLIST args;
	index_t  op;
	BOOL     last;

	if (mac->type == MACRO_OBJECT) return NULL;

	if (!mac->operandCount && LexPeek(0).type == delim)
		return NULL;

	last = FALSE;
	args = NewArgList();
	for (op = 0; last == FALSE && op < mac->operandCount; op++)
		AddArg(args, GetArgument(op, &last, delim, acceptNewLine));
	return args;
}

/**
*	_Expand
*
*	Description : Recursively expands a macro into "tokens". Performs substitution
*                 of function like and multi-line macro operands
*
*	Input : mac - Parent macro to expand
*           args - Argument list of "mac"
*	        tokens - Token vector to write to
*	        hide - Hide set
*/
PRIVATE void _Expand(IN PMACRO mac, IN PARGLIST args, IN PVECTOR tokens, IN PVECTOR hide) {

	PMACRO   toexpand;
	PVECTOR  argtokens;
	PARGLIST toexpandargs;
	PTOKEN   tok;
	index_t  max;
	index_t  cur;
	index_t  arg;

	VectorPush(hide, mac->name);

	tok = (PTOKEN)mac->body->data;
	max = VectorLength(mac->body);

	for (cur = 0; cur < max; cur++, tok++) {

		if (tok->type == TokenMacroOperand) {

			if (tok->position >= mac->operandCount) {
				Fatal("*** BUGCHECK: TokenMacroOperand position %i >= macro opcount %i",
					tok->position, mac->operandCount);
			}

			argtokens = GetArg(args, tok->position);

			if (!argtokens)
				Fatal("*** BUGCHECK: TokenMacroOperand position %i does not exist", tok->position);

			VectorAppend(tokens, argtokens);
			continue;
		}

		if (tok->type != TokenIdentifier) {
			VectorPush(tokens, tok);
			continue;
		}

		// only expand macro operands in multline macros.
		if (mac->type == MACRO_MULTILINE) {
			VectorPush(tokens, tok);
			continue;
		}

		toexpand = MacroGet(tok->value.s);
		if (!toexpand) {
			VectorPush(tokens, tok);
			continue;
		}

		if (mac->type == MACRO_OBJECT && toexpand->type == MACRO_MULTILINE) {
			VectorPush(tokens, tok);
			continue;
		}

		if (InHideset(hide, tok->value.s)) {
			VectorPush(tokens, tok);
			continue;
		}

		switch (toexpand->type) {
		case MACRO_FUNCTION:
			toexpandargs = GetArgumentList(toexpand, TokenRightParen, FALSE);
			break;
		case MACRO_MULTILINE:
			toexpandargs = GetArgumentList(toexpand, TokenEndOfLine, TRUE);
			break;
		case MACRO_OBJECT:
		default:
			toexpandargs = NULL;
		}
		_Expand(toexpand, toexpandargs, tokens, hide);
		FreeArgs(toexpandargs);
	}
	VectorPop(hide, NULL);
}

/**
*	Expand
*
*	Description : Reads a token. If the token is a macro, expands it first
*
*	Output : Next token
*/
PRIVATE TOKEN Expand(void) {

	PVECTOR  hide;
	PVECTOR  tokens;
	TOKEN    tok;
	PMACRO   macro;
	PARGLIST args;
	PVECTOR  pushback;
	index_t  i;
	PTOKEN   tmp;

	Lex(&tok);

	if (tok.type != TokenIdentifier)
		return tok;

	macro = MacroGet(tok.value.s);

	if (!macro)
		return tok;

	args = NULL;
	hide = NewVector(0, sizeof(STRING));
	tokens = NewVector(0, sizeof(TOKEN));

	switch (macro->type) {
	case MACRO_FUNCTION:
		args = GetArgumentList(macro, TokenRightParen, FALSE);
		break;
	case MACRO_MULTILINE:
		args = GetArgumentList(macro, TokenEndOfLine, TRUE);
		break;
	}

	_Expand(macro, args, tokens, hide);

	pushback = NewReverseVector(tokens);

	UngetTokens(pushback);

	FreeArgs(args);
	FreeVector(hide);
	FreeVector(tokens);
	FreeVector(pushback);

	Lex(&tok);
	return tok;
}

/**
*	FreeMacro
*
*	Description : Free a macro resources
*
*	Input : mac - Macro to free
*/
PRIVATE void FreeMacro(IN PMACRO mac) {

	PTOKEN  tok;

	if (!mac) return;
	if (mac->body) {
		tok = mac->body->data;
		if (tok)
			SetFree(tok->hideset);
	}
	FreeVector(mac->body);
	free(mac);
}

/**
*	GetContextCount
*
*	Description : This procedure returns the number of Preprocessor
*	contexts currently open.
*
*	Output : The number of Preprocessor contexts currently opened.
*/
PRIVATE unsigned long GetContextCount(void) {

	assert(_scopeStack != NULL);
	return _scopeStack->elementCount;
}

/**
*	GetFirstContext
*
*	Description : This procedure returns a pointer to the global
*	Preprocessor context.
*
*	Output : Pointer to Global Preprocessor context.
*/
PRIVATE PSCOPE GetFirstContext(void) {

	return (PSCOPE) _scopeStack->data;
}

/**
*	GetContext
*
*	Description : This procedure returns a pointer to the local
*	Preprocessor context.
*
*	Output : Pointer to Local Preprocessor context.
*/
PRIVATE PSCOPE GetContext(void) {

	return _scope;
}

/**
*	PushContext
*
*	Description : This procedure pushes a new Preprocessor context
*	and opens a new local scope.
*
*	Input : Pointer to a name for the context.
*
*	Output : Pointer to Local Preprocessor context.
*/
PRIVATE PSCOPE PushContext(IN char* name) {

	PSCOPE ctx;
	
	ctx = malloc(sizeof(SCOPE));
	strcpy(ctx->magic, "SCOPE");
	ctx->name = name;
	ctx->macros = NULL;
	ctx->parent = _scope;

	VectorPush(_scopeStack, &ctx);
	VectorLastElement(_scopeStack, &_scope);

	return _scope;
}

PRIVATE void FreeMacroElement(IN PMAPELEMENT e) {

	PMACRO m;

	m = e->value;
	FreeMacro(m);
}

/**
*	PopContext
*
*	Description : This procedure pops the last Preprocessor context
*	and closes the local scope.
*
*	Output : Pointer to local (or global) preprocessor context.
*/
PRIVATE PSCOPE PopContext(void) {

	PSCOPE ctx;

	VectorPop(_scopeStack, &ctx);

	MapFree(ctx->macros, FreeMacroElement);
	free(ctx);

	VectorLastElement(_scopeStack, &_scope);
	return _scope;
}

/**
*	MacroUndef
*
*	Description : Undefine macro
*
*	Input : mac - Macro to undefine	
*/
PRIVATE void MacroUndef(IN PMACRO mac) {

	PSCOPE ctx;

	ctx = GetContext();

	MapRemove(ctx->macros, mac->name->value, mac);

	if (MapGet(ctx->macros, mac->name->value))
		Fatal("*** BUGCHECK: MacroUndef: failed to remove %s", mac->name->value);

	FreeMacro(mac);
}

/**
*	Init
*
*	Description : Initializes the Preprocessor.
*/
PRIVATE void Init(void) {

	_ifStack = NewVector(0, sizeof(long));
	_scopeStack = NewVector(0, sizeof(PSCOPE));
	PushContext("global");
}

/**
*	NewObjectMacro
*
*	Description : This procedure allocates a new object macro.
*
*	Input : Pointer to a Vector object containing the Tokens for
*	the body of the macro.
*
*	Output : Pointer to the new macro or NULL on error.
*/
PRIVATE PMACRO NewObjectMacro (IN PVECTOR body, IN PSTRING name) {

	PMACRO m;

	m = (PMACRO)malloc(sizeof(MACRO));
	if (!m)
		return NULL;

	strcpy(m->magic, "MACRO");
	m->name = name;
	m->type = MACRO_OBJECT;
	m->operandCount = 0;
	m->curtok = 0;
	m->body = body;
	m->isVariableArgument = FALSE;

	return m;
}

/**
*	NewFunctionMacro
*
*	Description : This procedure allocates a new function like macro.
*
*	Input : body - Pointer to a Vector object containing the Tokens for
*	               the body of the macro.
*	        opcount - Number of operands.
*	        variableArg - Currently not supported.
*
*	Output : Pointer to the new macro or NULL on error.
*/
PRIVATE PMACRO NewFunctionMacro(IN PVECTOR body, IN int opcount, IN BOOL variableArg, IN PSTRING name) {

	PMACRO m;

	m = (PMACRO)malloc(sizeof(MACRO));
	if (!m)
		return NULL;

	strcpy(m->magic, "MACRO");
	m->name = name;
	m->type = MACRO_FUNCTION;
	m->operandCount = opcount;
	m->body = body;
	m->curtok = 0;
	m->isVariableArgument = variableArg;

	return m;
}

/**
*	NewMultilineMacro
*
*	Description : This procedure allocates a new multi-line macro.
*
*	Input : body - Pointer to a Vector object containing the Tokens for
*	               the body of the macro.
*	        opcount - Number of operands.
*
*	Output : Pointer to the new macro or NULL on error.
*/
PRIVATE PMACRO NewMultilineMacro(IN PVECTOR body, IN int opcount, IN PSTRING name) {

	PMACRO m;

	m = (PMACRO)malloc(sizeof(MACRO));
	if (!m)
		return NULL;

	strcpy(m->magic, "MACRO");
	m->name = name;
	m->type = MACRO_MULTILINE;
	m->operandCount = opcount;
	m->body = body;
	m->curtok = 0;
	m->isVariableArgument = FALSE;

	return m;
}

/**
*	MacroPut
*
*	Description : Stores a macro in the local Preprocessor Context.
*
*	Input : name - Pointer to the name of the macro.
*	        macro - Pointer to the Macro object.
*
*	Output : TRUE if successful, FALSE on error.
*/
PUBLIC BOOL MacroPut(IN char* name, IN PMACRO macro) {

	PSCOPE   ctx;
	PMACRO   mac;

	ctx = GetContext();

	if (!ctx) {
		Init();
		ctx = GetContext();
	}
	if (!ctx->macros) {
		ctx->macros = MapCreate(10);
		if (!ctx->macros)
			return FALSE;
	}

	// if the macro already exists, free it
	// as we are about to replace it:
	mac = MapGet(ctx->macros, name);
	if (mac)
		FreeMacro(mac);

	if (MapPut(ctx->macros, name, macro))
		return TRUE;
	return FALSE;
}

/**
*	MacroGet
*
*	Description : Gets a macro from the local Preprocessor Context.
*	This procedure will look in parent scopes if it does not exist in local scope.
*
*	Input : name - Pointer to the name of the macro.
*
*	Output : Pointer to the Macro object with the specified name or NULL
*	if the macro does not exist.
*/
PUBLIC PMACRO MacroGet (IN PSTRING name) {

	PMACRO m;
	PSCOPE ctx;

	/* walk through the context stack. */
	for (ctx = GetContext(); ctx; ctx = ctx->parent) {

		/* if the macro exists in this context, return it. */
		m = (PMACRO)MapGet(ctx->macros, name->value);
		if (m)
			return m;
	}

	return NULL;
}

/**
*	NewMacroToken
*
*	Description : Creates a new lexer TOKEN that represents
*	an argument in a function like macro body.
*
*	Input : position - Argument index.
*	        vararg - TRUE if variable-argument TOKEN, FALSE otherwise.
*
*	Output : Pointer to the TOKEN object or NULL on error.
*/
PRIVATE PTOKEN NewMacroToken (IN int position, IN BOOL vararg) {

	PTOKEN tok = (PTOKEN)malloc(sizeof(TOKEN));
	if (!tok)
		return NULL;
	memset(tok, 0, sizeof(TOKEN));
	strcpy(tok->magic, "TOK");
	tok->type = TokenMacroOperand;
	tok->value.s = InternString("<token_macro_operand>");
	tok->position = position;
	tok->isvararg = vararg;
	return tok;
}

/**
*	ProcessFunctionLikeMacroOper
*
*	Description : Processes a function like macro operand. The procedure
*	reads tokens from the scanner to parse the argument list and writes
*	the arguments to "op."
*
*	Input : op - Pointer to Dictionary object to accept entries.
*
*	Output : TRUE if successful, FALSE on error.
*/
PRIVATE BOOL ProcessFunctionLikeMacroOper(OUT PMAP op) {

	TOKEN LParen;
	TOKEN tok;
	int position;

	Lex(&LParen);
	position = 0;

	while (TRUE) {

		Lex(&tok);
		if (tok.type == TokenRightParen)
			return FALSE;
		if (position > 0) {
			if (tok.type != TokenComma)
				Error(", expected, but got %t", tok);
			Lex(&tok);
		}
		if (tok.type == TokenEndOfLine || tok.type == TokenEndOfFile) {
			Error("Missing ')' in parameter list");
		}
		if (tok.type != TokenIdentifier) {
			Error("Identifier expected but got %t", tok);
		}

		/* store this operand. */
		MapPut(op, tok.value.s->value, NewMacroToken(position++, FALSE));
	}

	return FALSE;
}

/**
*	ProcessFunctionLikeMacroBody
*
*	Description : Processes a function like macro body. This procedure
*	reads tokens from the scanner and performs macro substitution.
*
*	Input : op - Pointer to Dictionary object of macro arguments.
*
*	Output : Pointer to new Vector object containing new Tokens.
*/
PRIVATE PVECTOR ProcessFunctionLikeMacroBody (IN PMAP op) {

	PVECTOR   r;
	TOKEN     tok;

	r = NewVector(0, sizeof(TOKEN));

	while (TRUE) {

		Lex(&tok);
		if (tok.type == TokenEndOfLine || tok.type == TokenEndOfFile)
			return r;
		if (tok.type == TokenIdentifier) {

			/* All operand tokens are replaced with "TokenMacroOperand"
			type in the body so the macro expansion later will know when
			to look up operands. */
			PTOKEN substitute = (PTOKEN) MapGet(op, tok.value.s->value);
			if (substitute) {
				VectorPush(r, substitute);
				continue;
			}
		}
		VectorPush(r, &tok);
	}
}

/**
*	ProcessFunctionLikeMacro
*
*	Description : Processes a function like macro. This procedure is
*	responsible for parsing a function like macro from the Scanner
*	and adding the new Macro to the current Preprocessor Context.
*
*	Input : name - Name of function like macro to create.
*
*	Output : TRUE if successful, FALSE on error. Creates
*	new Function Like Macro and adds it to local or global scope.
*/
PRIVATE BOOL ProcessFunctionLikeMacro(IN PSTRING name) {

	PMAP op;
	BOOL varargs;
	PMACRO macro;
	PVECTOR body;

	assert(name != NULL);

	op = MapCreate(10);
	if (!op)
		return FALSE;

	varargs = ProcessFunctionLikeMacroOper(op);
	body = ProcessFunctionLikeMacroBody(op);
	macro = NewFunctionMacro(body, MapElementCount(op), varargs, name);

	MapFree(op, NULL);

	if (MacroPut(name->value, macro))
		return TRUE;
	return FALSE;
}

/**
*	ProcessObjectLikeMacro
*
*	Description : Processes an object like macro. This reads
*	tokens from the Scanner and creates a new Macro in the
*	current Preprocessor Context.
*
*	Input : name - Name of function like macro to create.
*/
PRIVATE void ProcessObjectLikeMacro(IN PSTRING name) {

	PVECTOR body;
	TOKEN tok;

	assert(name != NULL);

	body = NewVector(0, sizeof(TOKEN));
	while (TRUE) {
		Lex(&tok);
		if (tok.type == TokenEndOfLine || tok.type == TokenEndOfFile)
			break;
		VectorPush(body, &tok);
	}
	MacroPut(name->value, NewObjectMacro(body, name));
}

/**
*	ProcessDefine
*
*	Description : Processes #define directive as
*	a Function like macro or Object like macro.
*/
PRIVATE void ProcessDefine(void) {

	TOKEN hash;
	TOKEN directive;
	TOKEN name;
	TOKEN next;

	Lex(&hash);
	Lex(&directive);
	Lex(&name);

	if (name.type != TokenIdentifier) {
		Error("%%define: Expected identifier");
		LexUnget(name);
		return;
	}
	next = LexPeek(0);
	if (next.type == TokenLeftParen) {
		ProcessFunctionLikeMacro(name.value.s);
		return;
	}
	ProcessObjectLikeMacro(name.value.s);
}

/**
*	ProcesUndef
*
*	Description : Process %undef macro
*/
PRIVATE void ProcessUndef(void) {

	TOKEN   hash;
	TOKEN   directive;
	TOKEN   name;
	PMACRO  mac;

	Lex(&hash);
	Lex(&directive);
	Lex(&name);

	if (name.type != TokenIdentifier) {
		Error("%%undef: Expected identifier");
		LexUnget(name);
		return;
	}

	mac = MacroGet(name.value.s);
	if (!mac) {
		Error("%%undef: Macro '%s' does not exist", name.value.s->value);
		LexUnget(name);
		return;
	}

	MacroUndef(mac);
}

/**
*	IncludeFile
*
*	Description : Attempts to open the specified
*	source file and pushes it on the File stack.
*
*	Input : filename - Pointer to file nme to open.
*
*	Output : TRUE if successful, FALSE if error.
*/
PRIVATE BOOL IncludeFile (IN PSTRING filename) {

	PSOURCE         file;
	IncludeFileSet* s;
	PSTRING         path;
	char*           fullname;
	size_t          pathlen;
	size_t          fnamelen;
	char*           p;

	file = FileOpen(filename);
	if (!file){

		for (s = _includeFilePaths; s; s = s->next) {

			path = InternString(s->s);
			pathlen = strlen(path->value);
			fnamelen = strlen(filename->value);

			fullname = malloc(pathlen + fnamelen + 1);
			strcpy(fullname, path->value);
			strcpy(&fullname[pathlen], filename->value);

			file = FileOpen(InternString(fullname));
			free(fullname);
			if (file)
				goto found_file;
		}

		return FALSE;
	}
found_file:
	SetInsert(&_includeFiles, filename);
	return TRUE;
}

/**
*	ProcessInclude
*
*	Description : Process %include directive. This
*	adds the new file to the top of the File stack.
*/
PRIVATE void ProcessInclude(void) {

	TOKEN hash;
	TOKEN directive;
	TOKEN filename;

	Lex(&hash);
	Lex(&directive);
	Lex(&filename);

	if (filename.type != TokenString) {
		Error("ProcessInclude: Invalid include file. Ignoring TOKEN...");
		LexUnget(filename);
		return;
	}

	if (!IncludeFile(filename.value.s))
		Error("Unable to open include file '%t'", filename);
}

PRIVATE PreprocTokenType _IsDirective(void);

/**
*	ProcessMacroBody
*
*	Description : Process the body of a multi-line macro.
*
*	Input : numParameters - Number of operands.
*
*	Output : Pointer to new Vector containing the Tokens of the
*	macro body. Tokens %1, %2, ... are replaced with Tokens <token_macro_operand>
*	for macro expansion.
*/
PRIVATE TokenVector* ProcessMacroBody(IN numParameters) {

	TokenVector* v;
	TOKEN        tok;
	TOKEN        val;

	v = NewVector(0, sizeof(TOKEN));

	while (TRUE) {

		if (LexPeek(0).type == TokenEndOfFile) {
			Error("EOF reached while reading %macro (Did you forget %endmacro?");
			return v;
		}

		if (_IsDirective() == TokenEndmacro)
			return v;

		Lex(&tok);

		/* watch for "%number". Note that there really shouldn't be
		spaces in between % and number, but the current scanner skips
		all whitespace so "% number" is also detected. This should be
		corrected in later versions. */
		if (tok.type == TokenMod && LexPeek(0).type == TokenInteger) {

			int arg;

			Lex(&val);
			arg = val.value.inum;

			if (arg > numParameters)
				Error("Macro argument %i > %i", arg, numParameters);

			memset(&tok, 0, sizeof(TOKEN));
			tok.type = TokenMacroOperand;
			tok.value.s = InternString("<token_macro_operand>");
			tok.position = arg - 1;
			tok.isvararg = FALSE;
		}
		VectorPush(v, &tok);
	}
}

/**
*	ProcessMacro
*
*	Description : Process a multi-line macro.
*/
PRIVATE void ProcessMacro(void) {

	PMACRO       macro;
	TOKEN        hash;
	TOKEN        directive;
	TOKEN        name;
	TokenVector* body;
	int          numParameters;

	Lex(&hash);
	Lex(&directive);
	Lex(&name);

	numParameters = 0;
	if (LexPeek(0).type == TokenInteger) {
		Lex(&hash);
		numParameters = hash.value.inum;
	}
	body = ProcessMacroBody(numParameters);

	/* consume %endmacro */
	if (_IsDirective() == TokenEndmacro) {
		Lex(&hash);
		Lex(&hash);
	}

	macro = NewMultilineMacro(body, numParameters, name.value.s);
	MacroPut(name.value.s->value, macro);
}

/**
*	ProcessEndif
*
*	Description : Process %endif directive.
*/
PRIVATE void ProcessEndif(void) {

	TOKEN hash;
	TOKEN directive;

	Lex(&hash);
	Lex(&directive);

	if (VectorLength(_ifStack) > 0)
		VectorPop(_ifStack, NULL);
	else
		Error("%%endif without matching %%if, %%ifdef, or %%ifndef");
}

/**
*	ProcessElse
*
*	Description : Process %else directive.
*/
PRIVATE void ProcessElse(void) {

	TOKEN  hash;
	TOKEN  directive;
	long   cond;

	Lex(&hash);
	Lex(&directive);

	if (VectorLength(_ifStack) == 0) {
		Error("%%else without matching %%if, %%ifdef, or %%ifndef");
		return;
	}
	VectorPop(_ifStack, &cond);
	if (GetIfState() == IF_DISABLE)
		cond = IF_DISABLE;
	else
		cond = !cond;
	VectorPush(_ifStack, &cond);
}

/**
*	ProcessIf
*
*	Description : Process %if directive.
*/
PRIVATE void ProcessIf(void) {

	TOKEN  hash;
	TOKEN  directive;
	TOKEN  tok;
	PEXPR  expr;
	long   cond;

	Lex(&hash);
	Lex(&directive);

	expr = NULL;
	cond = IF_DISABLE;

	if (GetIfState() == IF_ENABLE) {
		StdScan(&tok);
		if (tok.type == TokenEndOfLine) {
			Error("%%if missing expression");
			return;
		}
		expr = Expr(StdScan, &tok);
		if (!IsNumber(expr)) {
			Error("%%if requires numeric expression");
			free(expr);
			return;
		}
	}

	if (expr && expr->val > 0)
		cond = IF_ENABLE;

	if (expr)
		free(expr);

	VectorPush(_ifStack, &cond);
}

/**
*	ProcessIfdef
*
*	Description : Process %ifdef directive.
*/
PRIVATE void ProcessIfdef(void) {

	TOKEN  hash;
	TOKEN  directive;
	TOKEN  name;
	long   cond;

	Lex(&hash);
	Lex(&directive);
	Lex(&name);

	cond = GetIfState();
	if (cond == IF_DISABLE) {
		VectorPush(_ifStack, &cond);
		return;
	}

	if (MacroGet(name.value.s))
		cond = IF_ENABLE;
	else
		cond = IF_DISABLE;

	VectorPush(_ifStack, &cond);
}

/**
*	ProcessIfndef
*
*	Description : Process %ifndef directive.
*/
PRIVATE void ProcessIfndef (void) {

	TOKEN  hash;
	TOKEN  directive;
	TOKEN  name;
	long   cond;

	Lex(&hash);
	Lex(&directive);
	Lex(&name);

	cond = GetIfState();
	if (cond == IF_DISABLE) {
		VectorPush(_ifStack, &cond);
		return;
	}

	if (MacroGet(name.value.s))
		cond = IF_DISABLE;
	else
		cond = IF_ENABLE;

	VectorPush(_ifStack, &cond);
}

/**
*	ProcessPush
*
*	Description : Process %push directive.
*/
PRIVATE void ProcessPush(void) {

	TOKEN hash;
	TOKEN directive;
	TOKEN name;

	Lex(&hash);
	Lex(&directive);
	if (LexPeek(0).type != TokenIdentifier) {
		Error("%%push requires a name");
		return;
	}
	Lex(&name);
	PushContext(name.value.s->value);
}

/**
*	ProcessPop
*
*	Description : Process %pop directive.
*/
PRIVATE void ProcessPop(void) {

	TOKEN hash;
	TOKEN directive;

	Lex(&hash);
	Lex(&directive);

	if (VectorLength(_scopeStack) == 0) {
		Error("%%pop with no matching %%push");
		return;
	}
	PopContext();
}

/**
*	PProcUnget
*
*	Description : Process %unget directive.
*/
PRIVATE void PProcUnget(IN TOKEN t) {

	LexUnget(t);
}

/**
*	UngetTokens
*
*	Description : Given a Vector of Tokens, this
*	puts them back in the TOKEN stream.
*/
PRIVATE void UngetTokens(IN PVECTOR tokens) {

	unsigned int i;
	TOKEN tok;
	for (i = 0; i < VectorLength(tokens); i++) {
		VectorGet(tokens, i, &tok);
		PProcUnget(tok);
	}
}

/**
*	_IsDirective
*
*	Description : Test if the current TOKEN is
*	a preprocessor directive.
*
*	Output : Preprocessor TOKEN Type.
*/
PRIVATE PreprocTokenType _IsDirective(void) {

	TOKEN a, b;
	char* lexeme;

	a = LexPeek(0);

	if (a.type == TokenEndOfFile)
		return PreprocTokenInvalid;
	b = LexPeek(1);

	if (_lineStart && a.type == TokenMod && (b.type == TokenMnemonic || b.type == TokenIdentifier)) {

		if (b.type == TokenMnemonic) {
			if (b.value.mnemonic == PUSH)
				lexeme = "push";
			else if (b.value.mnemonic == POP)
				lexeme = "pop";
			else return PreprocTokenInvalid;
		}else
			lexeme = b.value.s->value;

		if (!_stricmp(lexeme, "define")) return TokenDefine;
		else if (!_stricmp(lexeme, "undef")) return TokenUndef;
		else if (!_stricmp(lexeme, "include")) return TokenInclude;
		else if (!_stricmp(lexeme, "if")) return TokenIf;
		else if (!_stricmp(lexeme, "endif")) return TokenEndif;
		else if (!_stricmp(lexeme, "else")) return TokenElse;
		else if (!_stricmp(lexeme, "ifdef")) return TokenIfdef;
		else if (!_stricmp(lexeme, "ifndef")) return TokenIfndef;
		else if (!_stricmp(lexeme, "macro")) return TokenMacro;
		else if (!_stricmp(lexeme, "endmacro")) return TokenEndmacro;
		else if (!_stricmp(lexeme, "push")) return TokenPush;
		else if (!_stricmp(lexeme, "pop")) return TokenPop;
	}
	return PreprocTokenInvalid;
}

/**
*	ReadDirective
*
*	Description : Process the given TOKEN type.
*
*	Input : t - TOKEN Type to process.
*/
PRIVATE void ReadDirective(IN PreprocTokenType t) {

	switch (t) {
	case TokenDefine: ProcessDefine(); break;
	case TokenUndef: ProcessUndef(); break;
	case TokenInclude: ProcessInclude(); break;
	case TokenIf: ProcessIf(); break;
	case TokenEndif: ProcessEndif(); break;
	case TokenIfdef: ProcessIfdef(); break;
	case TokenIfndef: ProcessIfndef(); break;
	case TokenElse: ProcessElse(); break;
	case TokenMacro: ProcessMacro(); break;
	case TokenPush: ProcessPush(); break;
	case TokenPop: ProcessPop(); break;
	};
}

/**
*	IsIfDirective
*
*	Description : Test if token is an if conditional directive
*
*	Input : t - Token to test
*
*	Output : TRUE if it is an if conditional directive otherwise FALSE
*/
PRIVATE BOOL IsIfDirective(IN PreprocTokenType t) {

	switch (t) {
	case TokenEndif:
	case TokenIfdef:
	case TokenIfndef:
	case TokenElse:
	case TokenIf:
		return TRUE;
	};
	return FALSE;
}

/**
*	PprocSkipLine
*
*	Description : Skip a line
*/
PRIVATE void PprocSkipLine(void) {

	TOKEN tok;

	while (TRUE) {
		Lex(&tok);
		if (tok.type == TokenEndOfLine) break;
		else if (tok.type == TokenEndOfFile)
			Fatal("End of file reached while looking for matching %%endif");
	}
}

/**
*	PProcGet
*
*	Description : Called by parser to get the next TOKEN.
*/
PRIVATE TOKEN PProcGet (void) {

	TOKEN tok;
	PreprocTokenType ptt;

	if (!_scopeStack)
		Init();

	while (TRUE) {
		ptt = _IsDirective();

		if (GetIfState() == IF_DISABLE) {
			if (IsIfDirective(ptt)) {
				ReadDirective(ptt);
				continue;
			}
			PprocSkipLine();
			continue;
		}

		if (ptt) {
			ReadDirective(ptt);
			continue;
		}
		tok = Expand();
		return tok;
	}
}

/**
*	PProcPeek
*
*	Description : Called by the parser to peek at the next TOKEN.
*/
PRIVATE TOKEN PProcPeek(IN int count) {

	TOKEN tok[CONTEXT_PUSHBACK_MAX];
	int i;

	assert(count >= 0 && count < CONTEXT_PUSHBACK_MAX);

	for (i = 0; i <= count; i++)
		tok[i] = PProcGet();
	for (i = count; i >= 0; i--)
		PProcUnget(tok[i]);

	return tok[count];
}

/**
*	PProcFixPathSeparators
*
*	Description : Given a file path, append PATH_SEPARATOR
*	if needed and fix all separators to the operating system-specific
*	PATH_SEPARATOR character. Returns PSTRING
*
*	Input : s - Path to fix
*
*	Output : Interned string
*/
PUBLIC PSTRING PProcFixPathSeparators(IN char* s) {

	PSTRING str;
	char*   fixedpath;
	char*   p;
	size_t  len;

	if (!s) return NULL;

	len = strlen(s);
	if (s[len - 1] == '/' || s[len - 1] == '\\')
		len--;

	fixedpath = malloc(len + 1);
	strcpy(fixedpath, s);
	fixedpath[len] = PATH_SEPARATOR;
	fixedpath[len + 1] = '\0';

	p = fixedpath;
	for (; *p; p++) {
		if (*p == '/' || *p == '\\')
			*p = PATH_SEPARATOR;
	}

	str = InternString(fixedpath);
	free(fixedpath);
	return str;
}

/*
	PProcAddIncludePath

	Description : Given a file path, add it to the %include file path.

	Input : s - File path string to add
*/
PUBLIC void PProcAddIncludePath(IN char* s) {

	PSTRING str;

	str = PProcFixPathSeparators(s);
	if (!str) return;

	_includeFilePaths = SetInsert(_includeFilePaths, str->value);
}

/**
*	FreeMacros
*
*	Description : Frees all macros from the last pass. Also
*	frees the scope and if conditional stacks and sets up
*	for the next pass
*/
PUBLIC void FreeMacros(void) {

	PSCOPE scope;
	PSCOPE parent;

	if (!_scope) return;

	if (strcmp(_scope->name, "global"))
		Error("%%push with no matching %%pop");

	for (scope = _scope; scope; scope = parent) {
		parent = scope->parent;
		MapFree(scope->macros, FreeMacroElement);
		free(scope);
	}
	FreeVector(_scopeStack);
	FreeVector(_ifStack);
	_ifStack = NULL;
	_scopeStack = NULL;
	_scope = NULL;
}

/*
	StdScan

	Description : Global scanner function performs preprocessing. See related
	Lex function which bypasses preprocessing (used for directives.)

	Input : tok - Pointer to TOKEN to receive next token

	Output : Token type
*/
PUBLIC int StdScan(OUT PTOKEN tok) {

	*tok = PProcGet();
	return tok->type;
}
