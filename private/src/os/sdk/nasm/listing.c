/************************************************************************
*
*	listing.c - Listing file
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <assert.h>
#include <string.h>
#include "nasm.h"

FILE* _listingFile;

#define MAXLINE 150
#define COLWIDTH 18

char _sourceLine[MAXLINE];
BOOL _validSourceLine;
long _lineNumber;
char _listingLine[MAXLINE];
offset_t _listingOffset;

/* strip new line character from source line. */
PRIVATE void stripLF(void) {

	unsigned int c;
	for (c = 0; c < strlen(_sourceLine); c++) {
		if (_sourceLine[c] == '\n')
			_sourceLine[c] = '\0';
	}
}

PRIVATE void init(IN char* fname) {

	if (!fname)
		return;
	_listingFile = fopen(fname, "w");
}

PRIVATE void cleanup(void) {

	if (_listingFile)
		fclose(_listingFile);
}

PRIVATE void write(void) {

	if (!_listingFile)
		return;

	if (!_listingLine[0])
		return;

	fprintf(_listingFile, "%6u: ", _lineNumber);

	if (_listingLine[0])
		fprintf(_listingFile, "%08u %-*s", _listingOffset, COLWIDTH+1, _listingLine);
	else
		fprintf(_listingFile, "%*s ", COLWIDTH+10, "");

	if (_validSourceLine)
		fprintf(_listingFile, " %s", _sourceLine);

	fputc('\n', _listingFile);

	_listingLine[0] = '\0';
	_validSourceLine = FALSE;
}

PRIVATE void append(IN offset_t offset, IN char* s) {

	if (!_listingFile)
		return;

	if (strlen(_listingLine) + strlen(s) > COLWIDTH) {
		strcat(_listingLine, "-");
		write();
	}

	if (!_listingLine[0])
		_listingOffset = offset;
	strcat(_listingLine, s);
}

static char xdigit[] = "0123456789ABCDEF";

#define HEX(a,b) (*(a)=xdigit[((b)>>4)&15],(a)[1]=xdigit[(b)&15]);

PRIVATE void addr(IN offset_t offset, IN char* s, IN int64_t dat, IN output_t type, size_t size) {

	char q[20];
	char *r = q;

	assert(size <= 8);	

	*r++ = s[0];
	while (size--) {
		HEX(r, dat);
		dat >>= 8;
		r += 2;
	}
	*r++ = s[1];
	*r = '\0';
	append(offset, q);
}

PRIVATE void out(IN offset_t offset, IN void* dat, IN output_t type, size_t size) {

	char data[8];
	uint8_t* p = dat;

	if (!_listingFile)
		return;

	if (!dat)
		return;

	switch (type) {
	case WRITE_RAWDATA:

		for (unsigned int c = 0; c < size; c++) {

			HEX(data, *p);
			data[2] = '\0';
			append(offset, data);
			p++;
		}
		break;

	case WRITE_RESERVE:
		break;
	case WRITE_ADDRESS:
		addr(offset, "[]", *(int64_t*)dat, type, size); break;
	case WRITE_REL1ADR:
	case WRITE_REL2ADR:
	case WRITE_REL4ADR:
	case WRITE_REL8ADR:
		addr(offset, "()", *(int64_t*)dat, type, size); break;
		break;
	};
}

PRIVATE void setLine(IN char* line, IN uint32_t lnum) {

	if (!_listingFile)
		return;

	if (!line) {
		write();
		_validSourceLine = FALSE;
		return;
	}
	_lineNumber = lnum;
	write();
	strncpy(_sourceLine, line, MAXLINE);
	stripLF();
	_validSourceLine = TRUE;
}

PUBLIC LISTING _listingNASM = {
	"listgen",
	&init,
	&cleanup,
	&out,
	&setLine
};
