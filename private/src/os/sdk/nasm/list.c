/************************************************************************
*
*	list.c - Linked list
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <string.h>
#include <malloc.h>
#include <assert.h>
#include "nasm.h"

/* initialize linked list. */
PUBLIC List* ListInitialize(IN List* in, IN BOOL duplicates) {

	assert(in != NULL);
	memcpy(in->magic, "LIST", 4);
	in->duplicates = duplicates;
	in->first = in->last = 0;
	return in;
}

/* get first instance of data in list. */
PUBLIC ListNode* ListGetFirst(IN List* root, IN list_element_t data) {

	ListNode* current;

	assert(root != NULL);

	if (root->first == NULL)
		return NULL;
	for (current = root->first; current; current = current->next){
		if (current->data == data)
			return current;
	}
	return NULL;
}

/* get next instance of data in list. */
PUBLIC ListNode* ListGetNext(IN ListNode* node) {

	ListNode* current;

	assert(node != NULL);

	if (!node)
		return NULL;
	for (current = node; current; current = current->next) {
		if (current->data == node->data)
			return current;
	}
	return NULL;
}

/* add element to list. */
PUBLIC ListNode* ListAdd(IN List* root, IN list_element_t data) {

	ListNode* node;

	assert(root != NULL);

	node = malloc(sizeof (ListNode));
	if (!node)
		return 0;

	if (root->duplicates == FALSE && ListGetFirst(root, data)) {
		free(node);
		return NULL;
	}
	memcpy(node->magic, "LST0", 4);
	node->data = data;
	if (root->first == NULL) {

		root->first = node;
		root->last = node;
		node->prev = 0;
		node->next = 0;
	}
	else {

		node->prev = root->last;
		node->next = 0;
		root->last->next = node;
		root->last = node;
	}
	return node;
}

/* returns size of list. */
PUBLIC size_t ListSize(List* root) {
	
	ListNode* currentNode;
	uint32_t size;

	assert(root != NULL);

	for (size = 0, currentNode = root->first; currentNode; currentNode = currentNode->next)
		size++;
	return size;
}

/* removes element. */
PUBLIC BOOL ListRemove(IN List* root, IN ListNode* node) {

	assert(root != NULL);

	if (!node)
		return FALSE;
	if (root->first == NULL || node == NULL)
		return FALSE;
	if (root->first == node) {
		root->first = node->next;
		if (root->first)
			root->first->prev = NULL;
	}
	else if (node->next == NULL) {
		node->prev = NULL;
	}
	else{
		node->prev->next = node->next;
		node->next->prev = node->prev;
	}
	free(node);
	return TRUE;
}

/* free list. */
PUBLIC void ListFree(IN List* list, IN OPTIONAL list_free_callback_t callback) {

	assert(list != NULL);

	while (list->first) {
		if (callback)
			callback(list->first);
		ListRemove(list, list->first);
	}

// Bug / Design: ListInitialize never allocates this so this
// should not be freed here either. We leave it up to the caller:
//	free(list);
}
