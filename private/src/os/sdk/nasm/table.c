/************************************************************************
*
*	table.c - Instruction table.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This is a work in progress------------------- */

#include "nasm.h"
#include "instr.h"

// for floating point, reference - https://cs.fit.edu/~mmahoney/cse3101/float.html

//
// Operand size and control flags
//

#define o16 ModSizeWord
#define o32 ModSizeDword
#define o64 ModSizeQword
#define a16 ModAddrSizeWord
#define a32 ModAddrSizeDword

#define PS ModPsuedo
#define OC ModEmitOpcodeOnly
#define X OpcodeExtNone
#define R ModMODRM
#define rcode ModRegOpcode

#define imm OperandImm

//
// Need to support these types for MOV:
//

#define memoffs8   OperandRm8
#define memoffs16  OperandRm8
#define memoffs32  OperandRm8

//
// Operand types
//

#define mem OperandClassMem
#define rm8 OperandRm8
#define rm16 OperandRm16
#define rm32 OperandRm32
#define rm64 OperandRm64
#define r8 OperandReg8
#define r16 OperandReg16
#define r32 OperandReg32
#define r64 OperandReg64
#define imm8 OperandImm8
#define imm16 OperandImm16
#define imm32 OperandImm32
#define imm64 OperandImm64
#define mem128 OperandMem128
#define mem80 OperandMem80
#define mem64 OperandMem64
#define mem32 OperandMem32
#define mem16 OperandMem16
#define mem8 OperandMem8
#define es OperandEs
#define ss OperandSs
#define fs OperandFs
#define gs OperandGs
#define cs OperandCs
#define ds OperandDs
#define al OperandAl
#define ah OperandAh
#define ax OperandAx
#define eax OperandEax
#define rax OperandRax
#define dx OperandDx
#define edx OperandEdx
#define rdx OperandRdx
#define cl OperandCl
#define cx OperandCx
#define ecx OperandEcx
#define rcx OperandRcx
#define seg OperandSeg
#define rel8 OperandRel8
#define rel16 OperandRel16
#define rel32 OperandRel32
#define ptr16_16 OperandPtr16_16
#define ptr16_32 OperandPtr16_32
#define sb8 OperandSByte8
#define sb16 OperandSByte16
#define sb32 OperandSByte32
#define cr0 OperandCr0
#define cr2 OperandCr2
#define cr3 OperandCr3
#define cr4 OperandCr4
#define st0 OperandST0
#define sti OperandClassReg|OperandRegFpu
#define mm OperandClassReg|OperandRegMmx
#define xmm OperandClassReg|OperandRegXmm

//
// Ignore means that the operand is for syntax only;
// it is used to match instructions but has no
// effect (and hence ignored) for code generation.
// This is because the operand is implied
//
#define I OperandIgnore

//
// Instruction table
//
// The instructions listed do NOT need to be in order,
// however they MUST be grouped together (i.e. all MOV
// instructions must be next to each other.) Within each group,
// the instruction should be ordered in increasing size/complexity.
// MatchInstruction attempts to select the "BEST" match from the
// selected group.
//

InstrTableEntry _instructions[] = {

	/* instructions -------------- */

	{ AAA, 0x37, X, 0, { 0 }, 0 },

	{ AAS, 0x3f, X, 0, { 0 }, 0 },

	{ AAD, 0xd50a, X, 0, { 0 }, 0 },
	{ AAD, 0xd5, X, 1, { imm8 }, 0 },

	{ AAM, 0xd40a, X, 0, { 0 }, 0 },
	{ AAM, 0xd4, X, 1, { imm8 }, 0 },

	{ ADC, 0x10, X, 2, { rm8, r8 }, R },
	{ ADC, 0x11, X, 2, { rm16, r16 }, R|o16 },
	{ ADC, 0x11, X, 2, { rm32, r32 }, R|o32 },
	{ ADC, 0x12, X, 2, { r8, rm8 }, R },
	{ ADC, 0x13, X, 2, { r16, rm16 }, R|o16 },
	{ ADC, 0x13, X, 2, { r32, rm32 }, R|o32 },
	{ ADC, 0x83, 2, 2, { rm16, imm8 }, R|o16 },
	{ ADC, 0x83, 2, 2, { rm32, imm8 }, R|o32 },
	{ ADC, 0x81, 2, 2, { rm16, imm16 }, R|o16 },
	{ ADC, 0x81, 2, 2, { rm32, imm32 }, R | o32 },
	{ ADC, 0x14, X, 2, { al, imm8 }, 0 },
	{ ADC, 0x15, X, 2, { ax, imm16 }, o16 },
	{ ADC, 0x15, X, 2, { eax, imm32 }, o32 },

	{ ADD, 0, X, 2, { rm8, r8 }, R },
	{ ADD, 1, X, 2, { rm16, r16 }, R|o16 },
	{ ADD, 1, X, 2, { rm32, r32 }, R|o32 },
	{ ADD, 2, X, 2, { r8, rm8 }, R },
	{ ADD, 3, X, 2, { r16, rm16 }, R|o16 },
	{ ADD, 3, X, 2, { r32, rm32 }, R|o32 },
	{ ADD, 4, X, 2, { al, imm8 }, 0 },
	{ ADD, 5, X, 2, { ax, imm16 }, o16 },
	{ ADD, 5, X, 2, { eax, imm32 }, o32 },
	{ ADD, 0x80, 0, 2, { rm8, imm8 }, R },
	{ ADD, 0x81, 0, 2, { rm16, imm16}, R|o16 },
	{ ADD, 0x81, 0, 2, { rm32, imm32 }, R|o32 },
	{ ADD, 0x83, 0, 2, { rm16, imm8 }, R|o16 },
	{ ADD, 0x83, 0, 2, { rm32, imm8 }, R|o32 },

	{ AND, 0x20, X, 2, { rm8, r8 }, R },
	{ AND, 0x21, X, 2, { rm16, r16 }, R|o16 },
	{ AND, 0x21, X, 2, { rm32, r32 }, R|o32 },
	{ AND, 0x22, X, 2, { r8, rm8 }, R },
	{ AND, 0x23, X, 2, { r16, rm16 }, R|o16 },
	{ AND, 0x23, X, 2, { r32, rm32 }, R|o32 },
	{ AND, 0x83, 4, 2, { rm16, imm8 }, R|o16 },
	{ AND, 0x83, 4, 2, { rm32, imm8 }, R|o32 },
	{ AND, 0x24, X, 2, { al, imm8 }, 0 },
	{ AND, 0x25, X, 2, { ax, imm16 }, o16 },
	{ AND, 0x25, X, 2, { eax, imm32 }, o32 },
	
	{ ARPL, 0x63, X, 2, { rm16, r16 }, R },
	
	{ BOUND, 0x62, X, 2, {r16, mem }, R|o16 },
	{ BOUND, 0x62, X, 2, { r32, mem }, R | o32 },

	{ BSF, 0x0fbc, X, 2, { r16, rm16 }, R|o16 },
	{ BSF, 0x0fbc, X, 2, { r32, rm32 }, R|o32 },

	{ BSR, 0x0fbd, X, 2, { r16, rm16 }, R|o16 },
	{ BSR, 0x0fbd, X, 2, { r32, rm32 }, R|o32 },

	{ BSWAP, 0x0fc8, X, 1, { r32 }, 0 }, // check

	{ BT, 0x0fa3, X, 2, { rm16, r16 }, R|o16 },
	{ BT, 0x0fa3, X, 2, { rm32, r32 }, R|o32 },
	{ BT, 0x0fba, 4, 2, { rm16, imm8 }, R|o16 },
	{ BT, 0x0fba, 4, 2, { rm32, imm8 }, R|o32 },

	{ BTC, 0x0fbb, X, 2, { rm16, r16 }, R|o16 },
	{ BTC, 0x0fbb, X, 2, { rm32, r32 }, R|o32 },
	{ BTC, 0x0fba, 7, 2, { rm16, imm8 }, R|o16 },
	{ BTC, 0x0fba, 7, 2, { rm32, imm8 }, R|o32 },

	{ BTR, 0x0fb3, X, 2, { rm16, r16 }, R|o16 },
	{ BTR, 0x0fb3, X, 2, { rm32, r32 }, R|o32 },
	{ BTR, 0x0fba, 6, 2, { rm16, imm8 }, R|o16 },
	{ BTR, 0x0fba, 6, 2, { rm32, imm8 }, R|o32 },

	{ BTS, 0x0fab, X, 2, { rm16, r16 }, R|o16 },
	{ BTS, 0x0fab, X, 2, { rm32, r32 }, R|o32 },
	{ BTS, 0x0fba, 5, 2, { rm16, imm8 }, R|o16 },
	{ BTS, 0x0fba, 5, 2, { rm32, imm8 }, R|o32 },

	{ CALL, 0xe8, X, 1, { rel16 }, o16 },
	{ CALL, 0xe8, X, 1, { rel32 }, o32 },
	{ CALL, 0x9a, X, 1, { ptr16_16 }, o16 },
	{ CALL, 0x9a, X, 1, { ptr16_32 }, o32 },
	{ CALL, 0xff, 2, 1, { rm16 }, R|o16 },
	{ CALL, 0xff, 2, 1, { rm32 }, R|o32 },
//	{ CALL, 0xff, 3, 1, { rm16 }, R | o16 },  // far_mem16
//	{ CALL, 0xff, 3, 1, { rm32 }, R | o32 },  // far_mem32

	{ CBW, 0x98, X, 0, { 0 }, o16 },
	{ CWDE, 0x98, X, 0, { 0 }, o32 },
	{ CWD, 0x99, X, 0, { 0 }, o16 },
	{ CDQ, 0x99, X, 0, { 0 }, o32 },

	{ CLC, 0xf8, X, 0, { 0 }, 0 },
	{ CLD, 0xfc, X, 0, { 0 }, 0 },
	{ CLI, 0xfa, X, 0, { 0 }, 0 },
	{ CLTS, 0x0f06, X, 0, { 0 }, 0 },

	{ CLFLUSH, 0x0fae, 7, 1, { mem }, R },

	{ CMC, 0xf5, X, 0, { 0 }, 0 },

	{ CMP, 0x38, X, 2, { rm8, r8 }, R },
	{ CMP, 0x39, X, 2, { rm16, r16 }, R|o16 },
	{ CMP, 0x39, X, 2, { rm32, r32 }, R|o32 },
	{ CMP, 0x3a, X, 2, { r8, rm8 }, R },
	{ CMP, 0x3b, X, 2, { r16, rm16 }, R|o16 },
	{ CMP, 0x3b, X, 2, { r32, rm32 }, R|o32 },
	{ CMP, 0x80, 7, 2, { rm8, imm8 }, R },
	{ CMP, 0x81, 7, 2, { rm16, imm16 }, R|o16 },
	{ CMP, 0x81, 7, 2, { rm32, imm32 }, R|o32 },
	{ CMP, 0x83, 7, 2, { rm16, imm8 }, R|o16 },
	{ CMP, 0x83, 7, 2, { rm32, imm8 }, R|o32 },
	{ CMP, 0x3c, X, 2, { al, imm8 }, 0 },
	{ CMP, 0x3d, X, 2, { ax, imm16 }, o16 },
	{ CMP, 0x3d, X, 2, { eax, imm32 }, o32 },

	{ CMPSB, 0xa6, X, 0, { 0 }, 0},
	{ CMPSW, 0xa6, X, 0, { 0 }, 0},
	{ CMPSD, 0xa6, X, 0, { 0 }, 0},

	{ CMPXCHG8B, 0x0fc7, 1, 1, { mem }, R },

	{ CPUID, 0x0fa2, X, 0, { 0 }, 0 },

	{ DAA, 0x27, X, 0, { 0 }, 0 },
	{ DAS, 0x2f, X, 0, { 0 }, 0 },

	{ DEC, 0x48, X, 1, { r16 }, o16|rcode },
	{ DEC, 0x48, X, 1, { r32 }, o32|rcode },
	{ DEC, 0xfe, 1, 1, { rm8 }, R },
	{ DEC, 0xff, 1, 1, { rm16 }, R|o16 },
	{ DEC, 0xff, 1, 1, { rm32 }, R|o32 },

	{ DIV, 0xf6, 6, 1, { rm8 }, R },
	{ DIV, 0xf7, 6, 1, { rm16 }, R|o16 },
	{ DIV, 0xf7, 6, 1, { rm32 }, R|o32 },

	{ ENTER, 0xc8, X, 2, { imm16, imm8 }, 0 },

	{ FSETPM, 0xdbe4, X, 0, { 0 }, 0 },

	{ HLT, 0xf4, X, 0, { 0 }, 0 },

	{ IBTS, 0x0fa7, X, 2, { rm16, r16 }, R|o16 },
	{ IBTS, 0x0fa7, X, 2, { rm32, r32 }, R|o32 },

	{ IDIV, 0xf6, 7, 1, { rm8 }, R },
	{ IDIV, 0xf7, 7, 1, { rm16 }, R|o16 },
	{ IDIV, 0xf7, 7, 1, { rm32 }, R|o32 },

	{ IMUL, 0xf6, 5, 1, { rm8 }, R },
	{ IMUL, 0xf7, 5, 1, { rm16 }, R|o16 },
	{ IMUL, 0xf7, 5, 1, { rm32 }, R|o32 },
	{ IMUL, 0x0faf, X, 2, { r16, rm16 }, R|o16 },
	{ IMUL, 0x0faf, X, 2, { r32, rm32 }, R|o32 },
	{ IMUL, 0x6b, X, 2, { r16, imm8 }, R|o16 },
	{ IMUL, 0x69, X, 2, { r16, imm16 }, R|o16 },
	{ IMUL, 0x6b, X, 2, { r32, imm8 }, R|o32 },
	{ IMUL, 0x69, X, 2, { r32, imm32 }, R|o32 },
	{ IMUL, 0x6b, X, 3, { r16, rm16, imm8 }, R|o16 },
	{ IMUL, 0x69, X, 3, { r16, rm16, imm16 }, R|o16 },
	{ IMUL, 0x6b, X, 3, { r32, rm32, imm8 }, R|o32 },
	{ IMUL, 0x69, X, 3, { r32, rm32, imm32 }, R|o32 },

	{ IN, 0xe4, X, 2, { al, imm8 }, 0 },
	{ IN, 0xe5, X, 2, { ax, imm8 }, o16 },
	{ IN, 0xe5, X, 2, { eax, imm8 }, o32 },
	{ IN, 0xec, X, 2, { al, dx }, 0 },
	{ IN, 0xed, X, 2, { ax, dx }, o16 },
	{ IN, 0xed, X, 2, { eax, dx }, o32 },

	{ INC, 0x40, X, 1, { r16 }, o16|rcode },
	{ INC, 0x40, X, 1, { r32 }, o32|rcode },
	{ INC, 0xfe, 0, 1, { rm8 }, R },
	{ INC, 0xff, 0, 1, { rm16 }, R|o16 },
	{ INC, 0xff, 0, 1, { rm32 }, R|o32 },

	{ INSB, 0x6c, X, 0, { 0 }, 0 },
	{ INSW, 0x6d, X, 0, { 0 }, o16 },
	{ INSD, 0x6d, X, 0, { 0 }, o32 },

	{ INT, 0xcd, X, 1, { imm8 }, 0 },

	{ INT1, 0xf1, X, 0, { 0 }, 0 },
	{ INT01, 0xf1, X, 0, { 0 }, 0 },
	{ ICEBP, 0xf1, X, 0, { 0 }, 0 },

	{ INT3, 0xcc, X, 0, { 0 }, 0 },
	{ INT03, 0xcc, X, 0, { 0 }, 0 },

	{ INTO, 0xce, X, 0, { 0 }, 0 },

	{ INVD, 0x0f08, X, 0, { 0 }, 0 },

	{ INVLPG, 0x0f01, 7, 1, { mem }, R },

	{ IRET, 0xcf, X, 0, { 0 }, o16 },
	{ IRETW, 0xcf, X, 0, { 0 }, o16 },
	{ IRETD, 0xcf, X, 0, { 0 }, o32 },
	{ IRETQ, 0xcf, X, 0, { 0 }, o64 },

	{ JO, 0x70, X, 1, { rel8 }, 0 },
	{ JO, 0x0f80, X, 1, { rel16 }, o16 },
	{ JO, 0x0f80, X, 1, { rel32 }, o32 },

	{ JNO, 0x71, X, 1, { rel8 }, 0 },
	{ JNO, 0x0f81, X, 1, { rel16 }, o16 },
	{ JNO, 0x0f81, X, 1, { rel32 }, o32 },

	{ JB, 0x72, X, 1, { rel8 }, 0 },
	{ JB, 0x0f82, X, 1, { rel16 }, o16 },
	{ JB, 0x0f82, X, 1, { rel32 }, o32 },

	{ JNAE, 0x72, X, 1, { rel8 }, 0 },
	{ JNAE, 0x0f82, X, 1, { rel16 }, o16 },
	{ JNAE, 0x0f82, X, 1, { rel32 }, o32 },

	{ JC, 0x72, X, 1, { rel8 }, 0 },
	{ JC, 0x0f82, X, 1, { rel16 }, o16 },
	{ JC, 0x0f82, X, 1, { rel32 }, o32 },

	{ JNB, 0x73, X, 1, { rel8 }, 0 },
	{ JNB, 0x0f83, X, 1, { rel16 }, o16 },
	{ JNB, 0x0f83, X, 1, { rel32 }, o32 },

	{ JAE, 0x73, X, 1, { rel8 }, 0 },
	{ JAE, 0x0f83, X, 1, { rel16 }, o16 },
	{ JAE, 0x0f83, X, 1, { rel32 }, o32 },

	{ JNC, 0x73, X, 1, { rel8 }, 0 },
	{ JNC, 0x0f83, X, 1, { rel16 }, o16 },
	{ JNC, 0x0f83, X, 1, { rel32 }, o32 },

	{ JE, 0x74, X, 1, { rel8 }, 0 },
	{ JE, 0x0f84, X, 1, { rel16 }, o16 },
	{ JE, 0x0f84, X, 1, { rel32 }, o32 },

	{ JZ, 0x74, X, 1, { rel8 }, 0 },
	{ JZ, 0x0f84, X, 1, { rel16 }, o16 },
	{ JZ, 0x0f84, X, 1, { rel32 }, o32 },

	{ JNE, 0x75, X, 1, { rel8 }, 0 },
	{ JNE, 0x0f85, X, 1, { rel16 }, o16 },
	{ JNE, 0x0f85, X, 1, { rel32 }, o32 },

	{ JNZ, 0x75, X, 1, { rel8 }, 0 },
	{ JNZ, 0x0f85, X, 1, { rel16 }, o16 },
	{ JNZ, 0x0f85, X, 1, { rel32 }, o32 },

	{ JBE, 0x76, X, 1, { rel8 }, 0 },
	{ JBE, 0x0f86, X, 1, { rel16 }, o16 },
	{ JBE, 0x0f86, X, 1, { rel32 }, o32 },

	{ JNA, 0x76, X, 1, { rel8 }, 0 },
	{ JNA, 0x0f86, X, 1, { rel16 }, o16 },
	{ JNA, 0x0f86, X, 1, { rel32 }, o32 },

	{ JA, 0x77, X, 1, { rel8 }, 0 },
	{ JA, 0x0f87, X, 1, { rel16 }, o16 },
	{ JA, 0x0f87, X, 1, { rel32 }, o32 },

	{ JNBE, 0x77, X, 1, { rel8 }, 0 },
	{ JNBE, 0x0f87, X, 1, { rel16 }, o16 },
	{ JNBE, 0x0f87, X, 1, { rel32 }, o32 },

	{ JS, 0x78, X, 1, { rel8 }, 0 },
	{ JS, 0x0f88, X, 1, { rel16 }, o16 },
	{ JS, 0x0f88, X, 1, { rel32 }, o32 },

	{ JNS, 0x79, X, 1, { rel8 }, 0 },
	{ JNS, 0x0f89, X, 1, { rel16 }, o16 },
	{ JNS, 0x0f89, X, 1, { rel32 }, o32 },

	{ JP, 0x7a, X, 1, { rel8 }, 0 },
	{ JP, 0x0f8a, X, 1, { rel16 }, o16 },
	{ JP, 0x0f8a, X, 1, { rel32 }, o32 },

	{ JPE, 0x7a, X, 1, { rel8 }, 0 },
	{ JPE, 0x0f8a, X, 1, { rel16 }, o16 },
	{ JPE, 0x0f8a, X, 1, { rel32 }, o32 },

	{ JNP, 0x7b, X, 1, { rel8 }, 0 },
	{ JNP, 0x0f8b, X, 1, { rel16 }, o16 },
	{ JNP, 0x0f8b, X, 1, { rel32 }, o32 },

	{ JPO, 0x7b, X, 1, { rel8 }, 0 },
	{ JPO, 0x0f8b, X, 1, { rel16 }, o16 },
	{ JPO, 0x0f8b, X, 1, { rel32 }, o32 },

	{ JL, 0x7c, X, 1, { rel8 }, 0 },
	{ JL, 0x0f8c, X, 1, { rel16 }, o16 },
	{ JL, 0x0f8c, X, 1, { rel32 }, o32 },

	{ JNGE, 0x7c, X, 1, { rel8 }, 0 },
	{ JNGE, 0x0f8c, X, 1, { rel16 }, o16 },
	{ JNGE, 0x0f8c, X, 1, { rel32 }, o32 },

	{ JGE, 0x7d, X, 1, { rel8 }, 0 },
	{ JGE, 0x0f8d, X, 1, { rel16 }, o16 },
	{ JGE, 0x0f8d, X, 1, { rel32 }, o32 },

	{ JNL, 0x7d, X, 1, { rel8 }, 0 },
	{ JNL, 0x0f8d, X, 1, { rel16 }, o16 },
	{ JNL, 0x0f8d, X, 1, { rel32 }, o32 },

	{ JLE, 0x7e, X, 1, { rel8 }, 0 },
	{ JLE, 0x0f8e, X, 1, { rel16 }, o16 },
	{ JLE, 0x0f8e, X, 1, { rel32 }, o32 },

	{ JNG, 0x7e, X, 1, { rel8 }, 0 },
	{ JNG, 0x0f8e, X, 1, { rel16 }, o16 },
	{ JNG, 0x0f8e, X, 1, { rel32 }, o32 },

	{ JG, 0x7f, X, 1, { rel8 }, 0 },
	{ JG, 0x0f8f, X, 1, { rel16 }, o16 },
	{ JG, 0x0f8f, X, 1, { rel32 }, o32 },

	{ JNLE, 0x7f, X, 1, { rel8 }, 0 },
	{ JNLE, 0x0f8f, X, 1, { rel16 }, o16 },
	{ JNLE, 0x0f8f, X, 1, { rel32 }, o32 },

	{ JCXZ, 0xe3, X, 1, { rel8 }, a16 },
	{ JECXZ, 0xe3, X, 1, { rel8 }, a32 },

	{ JMP, 0xe9, X, 1, { rel16 }, o16 },
	{ JMP, 0xe9, X, 1, { rel32 }, o32 },
	{ JMP, 0xeb, X, 1, { rel8 }, 0 }, // JMP SHORT
	{ JMP, 0xea, X, 1, { ptr16_16 }, o16 },
	{ JMP, 0xea, X, 1, { ptr16_32 }, o32 },
	{ JMP, 0xff, 5, 1, { mem }, R|o16 }, 
	{ JMP, 0xff, 5, 1, { mem }, R|o32 },
	{ JMP, 0xff, 4, 1, { rm16 }, R|o16 },
	{ JMP, 0xff, 4, 1, { rm32 }, R|o32 },

	{ LAHF, 0x9f, X, 0, { 0 }, 0 },

	{ LAR, 0x0f02, X, 2, { r16, rm16 }, R|o16 },
	{ LAR, 0x0f02, X, 2, { r32, rm32 }, R|o32 },

	{ LDS, 0xc5, X, 2, { r16, mem }, R|o16 },
	{ LDS, 0xc5, X, 2, { r32, mem }, R | o32 },

	{ LES, 0xc4, X, 2, { r16, mem }, R | o16 },
	{ LES, 0xc4, X, 2, { r32, mem }, R | o32 },

	{ LFS, 0x0fb4, X, 2, { r16, mem }, R | o16 },
	{ LFS, 0x0fb4, X, 2, { r32, mem }, R | o32 },

	{ LGS, 0x0fb5, X, 2, { r16, mem }, R|o16 },
	{ LGS, 0x0fb5, X, 2, { r32, mem }, R|o32 },

	{ LSS, 0x0fb2, X, 2, { r16, mem }, R|o16 },
	{ LSS, 0x0fb2, X, 2, { r32, mem }, R|o32 },

	{ LEA, 0x8d, X, 2, { r16, mem }, R|o16 },
	{ LEA, 0x8d, X, 2, { r32, mem }, R|o32 },

	{ LEAVE, 0xc9, X, 0, { 0 }, 0 },

	{ LGDT, 0x0f01, 2, 1, { mem }, R },
	{ LIDT, 0x0f01, 3, 1, { mem }, R },
	{ LLDT, 0x0f00, 2, 1, { mem }, R },

	{ LMSW, 0x0f01, 6, 1, { rm16 }, R },

	{ LOADALL, 0x0f07, X, 0, { 0 }, 0 },
	{ LOADALL286, 0x0f05, X, 0, { 0 }, 0 },

	{ LODSB, 0xac, X, 0, { 0 }, 0 },
	{ LODSW, 0xad, X, 0, { 0 }, o16 },
	{ LODSD, 0xad, X, 0, { 0 }, o32 },

	{ LOOP, 0xe2, X, 1, { rel8 }, 0 },
	{ LOOP, 0xe2, X, 2, { rel8, cx }, o16 },
	{ LOOP, 0xe2, X, 2, { rel8, ecx }, o32 },

	{ LOOPE, 0xe1, X, 1, { rel8 }, 0 },
	{ LOOPE, 0xe1, X, 2, { rel8, cx }, o16 },
	{ LOOPE, 0xe1, X, 2, { rel8, ecx }, o32 },

	{ LOOPZ, 0xe1, X, 1, { rel8 }, 0 },
	{ LOOPZ, 0xe1, X, 2, { rel8, cx }, o16 },
	{ LOOPZ, 0xe1, X, 2, { rel8, ecx }, o32 },

	{ LOOPNE, 0xe0, X, 1, { rel8 }, 0 },
	{ LOOPNE, 0xe0, X, 2, { rel8, cx }, o16 },
	{ LOOPNE, 0xe0, X, 2, { rel8, ecx }, o32 },

	{ LOOPNZ, 0xe0, X, 1, { rel8 }, 0 },
	{ LOOPNZ, 0xe0, X, 2, { rel8, cx }, o16 },
	{ LOOPNZ, 0xe0, X, 2, { rel8, ecx }, o32 },

	{ LSL, 0x0f03, X, 2, { r16, rm16 }, R|o16 },
	{ LSL, 0x0f03, X, 2, { r32, rm32 }, R|o32 },

	{ LTR, 0x0f00, 3, 1, { rm16 }, R },

	{ MFENCE, 0x0fae, 6, 0, { 0 }, 0 },

	{ MOV, 0x88, X, 2, { rm8, r8 }, R },
	{ MOV, 0x89, X, 2, { rm16, r16 }, R|o16 },
	{ MOV, 0x89, X, 2, { rm32, r32 }, R|o32 },
	{ MOV, 0x89, X, 2, { rm64, r64 }, R|o64 },            // <----
	{ MOV, 0x8a, X, 2, { r8, rm8 }, R },
	{ MOV, 0x8b, X, 2, { r16, rm16 }, R|o16 },
	{ MOV, 0x8b, X, 2, { r32, rm32 }, R|o32 },
	{ MOV, 0xb0, X, 2, { r8, imm8 }, rcode },
	{ MOV, 0xb8, X, 2, { r16, imm16 }, rcode|o16 },
	{ MOV, 0xb8, X, 2, { r32, imm32 }, rcode|o32 },
	{ MOV, 0xb8, X, 2, { r64, imm64 }, rcode | o64 },
	{ MOV, 0xc6, 0, 2, { rm8, imm8 }, 0 },
	{ MOV, 0xc7, 0, 2, { rm16, imm16 }, R|o16 },
	{ MOV, 0xc7, 0, 2, { rm32, imm32 }, R|o32 },

//	{ MOV, 0xa0, X, 2, { al, memoffs8 }, 0 },
//	{ MOV, 0xa1, X, 2, { ax, memoffs16 }, o16 },
//	{ MOV, 0xa1, X, 2, { ax, memoffs32 }, o32 },
//	{ MOV, 0xa2, X, 2, { memoffs8, al }, 0 },
//	{ MOV, 0xa3, X, 2, { memoffs16, ax }, o16 },
//	{ MOV, 0xa3, X, 2, { memoffs32, eax }, o32 },
	{ MOV, 0x8c, X, 2, { rm16, seg }, R|o16 },
	{ MOV, 0x8c, X, 2, { rm32, seg }, R|o32 },
	{ MOV, 0x8e, X, 2, { seg, rm16 }, R|o16 },
	{ MOV, 0x8e, X, 2, { seg, rm32 }, R|o32 },
	{ MOV, 0x0f20, X, 2, { r32, cr0 }, R|o32 },
	{ MOV, 0x0f20, X, 2, { r32, cr2 }, R|o32 },
	{ MOV, 0x0f20, X, 2, { r32, cr3 }, R|o32 },
	{ MOV, 0x0f20, X, 2, { r32, cr4 }, R|o32 },
	{ MOV, 0x0f22, X, 2, { cr0, r32 }, R|o32 },
	{ MOV, 0x0f22, X, 2, { cr2, r32 }, R|o32 },
	{ MOV, 0x0f22, X, 2, { cr3, r32 }, R|o32 },
	{ MOV, 0x0f22, X, 2, { cr4, r32 }, R|o32 },

//	{ MOV, 0x0f21, X, 2, { r32, RegDr0 | RegDr1 | RegDr2 | RegDr3 | RegDr6 | RegDr7 }, R },
//	{ MOV, 0x0f24, X, 2, { r32, RegTr3 | RegTr4 | RegTr5 | RegTr6 | RegTr7 }, R },

//	{ MOV, 0x0f23, X, 2, { RegDr0 | RegDr1 | RegDr2 | RegDr3 | RegDr6 | RegDr7, r32 }, R },
//	{ MOV, 0x0f23, X, 2, { RegTr3 | RegTr4 | RegTr5 | RegTr6 | RegTr7, r32 }, R },

	{ MOVSB, 0xa4, X, 0, { 0 }, 0 },
	{ MOVSW, 0xa5, X, 0, { 0 }, o16 },
	{ MOVSD, 0xa5, X, 0, { 0 }, o32 },

	{ MOVSX, 0x0fb3, X, 2, { r16, rm8 }, R|o16 },
	{ MOVSX, 0x0fbe, X, 2, { r32, rm8 }, R|o32 },
	{ MOVSX, 0x0fbf, X, 2, { r32, rm16 }, R|o32 },

	{ MOVZX, 0x0fb6, X, 2, { r16, rm8 }, R|o16 },
	{ MOVZX, 0x0fb6, X, 2, { r32, rm8 }, R|o32 },
	{ MOVZX, 0x0fb7, X, 2, { r32, rm16 }, R|o32 },

	{ MUL, 0xf6, 4, 1, { rm8 }, R },
	{ MUL, 0xf7, 4, 1, { rm16 }, R|o16 },
	{ MUL, 0xf7, 4, 1, { rm32 }, R|o32 },

	{ NEG, 0xf6, 3, 1, { rm8 }, R },
	{ NEG, 0xf7, 3, 1, { rm16 }, R|o16 },
	{ NEG, 0xf7, 3, 1, { rm32 }, R|o32 },

	{ NOT, 0xf6, 2, 1, { rm8 }, R },
	{ NOT, 0xf7, 2, 1, { rm16 }, R|o16 },
	{ NOT, 0xf7, 2, 1, { rm32 }, R|o32 },

	{ NOP, 0x90, X, 0, { 0 }, 0 },

	{ OR, 0x08, X, 2, { rm8, r8 }, R },
	{ OR, 0x09, X, 2, { rm16, r16 }, R|o16 },
	{ OR, 0x09, X, 2, { rm32, r32 }, R|o32 },
	{ OR, 0x0a, X, 2, { r8, rm8 }, R },
	{ OR, 0x0b, X, 2, { r16, rm16 }, R|o16 },
	{ OR, 0x0b, X, 2, { r32, rm32 }, R|o32 },
	{ OR, 0x80, 1, 2, { rm8, imm8 }, R },
	{ OR, 0x81, 1, 2, { rm16, imm16 }, R|o16 },
	{ OR, 0x81, 1, 2, { rm32, imm32 }, R|o32 },
	{ OR, 0x83, 1, 2, { rm16, imm8 }, R|o16 },
	{ OR, 0x83, 1, 2, { rm32, imm8 }, R|o32 },
	{ OR, 0x0c, X, 2, { al, imm8 }, 0 },
	{ OR, 0x0d, X, 2, { ax, imm16 }, 0 },
	{ OR, 0x0d, X, 2, { eax, imm32 }, 0 },

	{ OUT, 0xe6, X, 2, { imm8, al }, 0 },
	{ OUT, 0xe7, X, 2, { imm8, ax }, o16 },
	{ OUT, 0xe7, X, 2, { imm8, eax }, o32 },
	{ OUT, 0xee, X, 2, { dx, al }, 0 },
	{ OUT, 0xef, X, 2, { dx, ax }, o16 },
	{ OUT, 0xef, X, 2, { dx, eax }, o32 },

	{ OUTSB, 0x6e, X, 0, { 0 }, 0 },
	{ OUTSW, 0x6f, X, 0, { 0 }, o16 },
	{ OUTSD, 0x6f, X, 0, { 0 }, o32 },

	{ PAUSE, 0xf390, X, 0, { 0 }, 0 },

	{ POP, 0x58, X, 1, { r16 }, rcode|o16 },
	{ POP, 0x58, X, 1, { r32 }, rcode|o32 },
	{ POP, 0x8f, 0, 1, { rm16 }, R|o16 },
	{ POP, 0x8f, 0, 1, { rm32 }, R|o32 },
	{ POP, 0x0f, X, 1, { cs }, 0 },
	{ POP, 0x1f, X, 1, { ds }, 0 },
	{ POP, 0x07, X, 1, { es }, 0 },
	{ POP, 0x17, X, 1, { ss }, 0 },
	{ POP, 0x0fa1, X, 1, { fs }, 0 },
	{ POP, 0x0fa9, X, 1, { gs }, 0 },

	{ POPA, 0x61, X, 0, { 0 }, 0 },
	{ POPAW, 0x61, X, 0, { 0 }, o16 },
	{ POPAD, 0x61, X, 0, { 0 }, o32 },

	{ POPF, 0x9d, X, 0, { 0 }, 0 },
	{ POPFW, 0x9d, X, 0, { 0 }, o16 },
	{ POPFD, 0x9d, X, 0, { 0 }, o32 },

	{ PUSH, 0x50, X, 1, { r16 }, rcode|o16 },
	{ PUSH, 0x50, X, 1, { r32 }, rcode|o32 },
	{ PUSH, 0xff, 6, 1, { rm16 }, R|o16 },
	{ PUSH, 0xff, 6, 1, { rm32 }, R|o32 },
	{ PUSH, 0x0e, X, 1, { cs }, 0 },
	{ PUSH, 0x1e, X, 1, { ds }, 0 },
	{ PUSH, 0x06, X, 1, { es }, 0 },
	{ PUSH, 0x16, X, 1, { ss }, 0 },
	{ PUSH, 0x0fa0, X, 1, { fs }, 0 },
	{ PUSH, 0x0fa8, X, 1, { gs }, 0 },
	{ PUSH, 0x6a, X, 1, { imm8 }, 0 },
	{ PUSH, 0x68, X, 1, { imm16 }, o16 },
	{ PUSH, 0x68, X, 1, { imm32 }, o32 },

	{ PUSHA, 0x60, X, 0, { 0 }, 0 },
	{ PUSHAW, 0x60, X, 0, { 0 }, o16 },
	{ PUSHAD, 0x60, X, 0, { 0 }, o32 },

	{ PUSHF, 0x9c, X, 0, { 0 }, 0 },
	{ PUSHFW, 0x9c, X, 0, { 0 }, o16 },
	{ PUSHFD, 0x9c, X, 0, { 0 }, o32 },

	{ RCL, 0xd0, 2, 2, { rm8, 1 }, R }, // need to work on. Second operand is literal 1.
	{ RCL, 0xd2, 2, 2, { rm8, cl }, R },
	{ RCL, 0xc0, 2, 2, { rm8, imm8 }, R },
	{ RCL, 0xd1, 2, 2, { rm16, 1 }, R|o16 }, // see above. o16
	{ RCL, 0xd3, 2, 2, { rm16, cl }, R|o16 },
	{ RCL, 0xc1, 2, 2, { rm16, imm8 }, R|o16 },
	{ RCL, 0xd1, 2, 2, { rm32, 1 }, R|o32 }, // see above. o32
	{ RCL, 0xd3, 2, 2, { rm32, cl }, R|o32 },
	{ RCL, 0xc1, 2, 2, { rm32, imm8 }, R|o32 },

	{ RCR, 0xd0, 3, 2, { rm8, 1 }, R }, // need to work on. Second operand is literal 1.
	{ RCR, 0xd2, 3, 2, { rm8, cl }, R },
	{ RCR, 0xc0, 3, 2, { rm8, imm8 }, R },
	{ RCR, 0xd1, 3, 2, { rm16, 1 }, R|o16 }, // see above. o16
	{ RCR, 0xd3, 3, 2, { rm16, cl }, R|o16 },
	{ RCR, 0xc1, 3, 2, { rm16, imm8 }, R|o16 },
	{ RCR, 0xd1, 3, 2, { rm32, 1 }, R|o32 }, // see above. o32
	{ RCR, 0xd3, 3, 2, { rm32, cl }, R|o32 },
	{ RCR, 0xc1, 3, 2, { rm32, imm8 }, R|o32 },

	{ RDMSR, 0x0f32, X, 0, { 0 }, 0 },

	{ RDPMC, 0x0f33, X, 0, { 0 }, 0 },

	{ RDTSC, 0x0f31, X, 0, { 0 }, 0 },

	{ RET, 0xc3, X, 0, { 0 }, 0 },
	{ RET, 0xc2, X, 1, { imm16 }, 0 },

	{ RETF, 0xcb, X, 0, { 0 }, 0 },
	{ RETF, 0xca, X, 1, { imm16 }, 0 },

	{ RETN, 0xc3, X, 0, { 0 }, 0 },
	{ RETN, 0xc2, X, 1, { imm16 }, 0 },

	{ ROL, 0xd0, 0, 2, { rm8, 1 }, R }, // need to work on. Second operand is literal 1.
	{ ROL, 0xd2, 0, 2, { rm8, cl }, R },
	{ ROL, 0xc0, 0, 2, { rm8, imm8 }, R },
	{ ROL, 0xd1, 0, 2, { rm16, 1 }, R|o16 }, // see above. o16
	{ ROL, 0xd3, 0, 2, { rm16, cl }, R|o16 },
	{ ROL, 0xc1, 0, 2, { rm16, imm8 }, R|o16 },
	{ ROL, 0xd1, 0, 2, { rm32, 1 }, R|o32 }, // see above. o32
	{ ROL, 0xd3, 0, 2, { rm32, cl }, R|o32 },
	{ ROL, 0xc1, 0, 2, { rm32, imm8 }, R|o32 },

	{ ROR, 0xd0, 1, 2, { rm8, 1 }, R }, // need to work on. Second operand is literal 1.
	{ ROR, 0xd2, 1, 2, { rm8, cl }, R },
	{ ROR, 0xc0, 1, 2, { rm8, imm8 }, R },
	{ ROR, 0xd1, 1, 2, { rm16, 1 }, R|o16 }, // see above. o16
	{ ROR, 0xd3, 1, 2, { rm16, cl }, R|o16 },
	{ ROR, 0xc1, 1, 2, { rm16, imm8 }, R|o16 },
	{ ROR, 0xd1, 1, 2, { rm32, 1 }, R|o32 }, // see above. o32
	{ ROR, 0xd3, 1, 2, { rm32, cl }, R|o32 },
	{ ROR, 0xc1, 1, 2, { rm32, imm8 }, R|o32 },

	{ RSM, 0x0faa, X, 0, { 0 }, 0 },

	{ SAHF, 0x9e, X, 0, { 0 }, 0 },

	{ SAL, 0xd0, 4, 2, { rm8, 1 }, R }, // need to work on. Second operand is literal 1.
	{ SAL, 0xd2, 4, 2, { rm8, cl }, R },
	{ SAL, 0xc0, 4, 2, { rm8, imm8 }, R },
	{ SAL, 0xd1, 4, 2, { rm16, 1 }, R|o16 }, // see above. o16
	{ SAL, 0xd3, 4, 2, { rm16, cl }, R|o16 },
	{ SAL, 0xc1, 4, 2, { rm16, imm8 }, R|o16 },
	{ SAL, 0xd1, 4, 2, { rm32, 1 }, R|o32 }, // see above. o32
	{ SAL, 0xd3, 4, 2, { rm32, cl }, R|o32 },
	{ SAL, 0xc1, 4, 2, { rm32, imm8 }, R|o32 },

	{ SAR, 0xd0, 7, 2, { rm8, 1 }, R }, // need to work on. Second operand is literal 1.
	{ SAR, 0xd2, 7, 2, { rm8, cl }, R },
	{ SAR, 0xc0, 7, 2, { rm8, imm8 }, R },
	{ SAR, 0xd1, 7, 2, { rm16, 1 }, R|o16 }, // see above. o16
	{ SAR, 0xd3, 7, 2, { rm16, cl }, R|o16 },
	{ SAR, 0xc1, 7, 2, { rm16, imm8 }, R|o16 },
	{ SAR, 0xd1, 7, 2, { rm32, 1 }, R|o32 }, // see above. o32
	{ SAR, 0xd3, 7, 2, { rm32, cl }, R|o32 },
	{ SAR, 0xc1, 7, 2, { rm32, imm8 }, R|o32 },

	{ SALC, 0xd6, X, 0, { 0 }, 0 },

	{ SBB, 0x18, X, 2, { rm8, r8 }, R },
	{ SBB, 0x19, X, 2, { rm16, r16 }, R|o16 },
	{ SBB, 0x19, X, 2, { rm32, r32 }, R|o32 },

	{ SBB, 0x1a, X, 2, { r8, rm8 }, R },
	{ SBB, 0x1b, X, 2, { r16, rm16 }, R|o16 },
	{ SBB, 0x1b, X, 2, { r32, rm32 }, R|o32 },

	{ SBB, 0x80, 3, 2, { rm8, imm8 }, R },
	{ SBB, 0x81, 3, 2, { rm16, imm16 }, R|o16 },
	{ SBB, 0x81, 3, 2, { rm32, imm32 }, R|o32 },

	{ SBB, 0x83, 3, 2, { rm16, imm8 }, R|o16 },
	{ SBB, 0x83, 3, 2, { rm32, imm8 }, R|o32 },

	{ SBB, 0x1c, X, 2, { al, imm8 }, 0 },
	{ SBB, 0x1d, X, 2, { ax, imm16 }, o16 },
	{ SBB, 0x1d, X, 2, { eax, imm32 }, o32 },

	{ SCASB, 0xae, X, 0, { 0 }, 0 },
	{ SCASW, 0xaf, X, 0, { 0 }, o16 },
	{ SCASD, 0xaf, X, 0, { 0 }, o32 },

	{ SGDT, 0x0f01, 0, 1, { mem }, R },
	{ SIDT, 0x0f01, 1, 1, { mem }, R },
	{ SLDT, 0x0f00, 0, 1, { rm16 }, R },

	{ SHL, 0xd0, 4, 2, { rm8, 1 }, R }, // need to work on. Second operand is literal 1.
	{ SHL, 0xd2, 4, 2, { rm8, cl }, R },
	{ SHL, 0xc0, 4, 2, { rm8, imm8 }, R },
	{ SHL, 0xd1, 4, 2, { rm16, 1 }, R }, // see above. o16
	{ SHL, 0xd3, 4, 2, { rm16, cl }, R|o16 },
	{ SHL, 0xc1, 4, 2, { rm16, imm8 }, R|o16 },
	{ SHL, 0xd1, 4, 2, { rm32, 1 }, 0 }, // see above. o32
	{ SHL, 0xd3, 4, 2, { rm32, cl }, R|o32 },
	{ SHL, 0xc1, 4, 2, { rm32, imm8 }, R|o32 },

	{ SHR, 0xd0, 5, 2, { rm8, 1 }, R }, // need to work on. Second operand is literal 1.
	{ SHR, 0xd2, 5, 2, { rm8, cl }, R },
	{ SHR, 0xc0, 5, 2, { rm8, imm8 }, R },
	{ SHR, 0xd1, 5, 2, { rm16, 1 }, R|o16 }, // see above. o16
	{ SHR, 0xd3, 5, 2, { rm16, cl }, R|o16 },
	{ SHR, 0xc1, 5, 2, { rm16, imm8 }, R|o16 },
	{ SHR, 0xd1, 5, 2, { rm32, 1 }, R|o32 }, // see above. o32
	{ SHR, 0xd3, 5, 2, { rm32, cl }, R|o32 },
	{ SHR, 0xc1, 5, 2, { rm32, imm8 }, R|o32 },

	{ SHLD, 0x0fa4, X, 3, { rm16, r16, imm8 }, R|o16 },
	{ SHLD, 0x0fa4, X, 3, { rm16, r32, imm8 }, R|o32 },
	{ SHLD, 0x0fa5, X, 3, { rm16, r16, cl }, R|o16 },
	{ SHLD, 0x0fa5, X, 3, { rm16, r32, cl }, R|o32 },

	{ SHRD, 0x0fac, X, 3, { rm16, r16, imm8 }, R|o16 },
	{ SHRD, 0x0fac, X, 3, { rm16, r32, imm8 }, R|o32 },
	{ SHRD, 0x0fad, X, 3, { rm16, r16, cl }, R|o16 },
	{ SHRD, 0x0fad, X, 3, { rm16, r32, cl }, R|o32 },

	{ SMI, 0xf1, X, 0, { 0 }, 0 },

	{ SMINT, 0x0f38, X, 0, { 0 }, 0 },
	{ SMINTOLD, 0x0f7e, X, 0, { 0 }, 0 },

	{ SMSW, 0x0f01, 4, 1, { rm16 }, R },

	{ STC, 0xf9, X, 0, { 0 }, 0 },
	{ STD, 0xfd, X, 0, { 0 }, 0 },
	{ STI, 0xfb, X, 0, { 0 }, 0 },

	{ STOSB, 0xaa, X, 0, { 0 }, 0 },
	{ STOSW, 0xab, X, 0, { 0 }, o16 },
	{ STOSD, 0xab, X, 0, { 0 }, o32 },

	{ STR, 0x0f00, 1, 1, { rm16 }, R },

	{ SUB, 0x28, X, 2, { rm8, r8 }, R },
	{ SUB, 0x29, X, 2, { rm16, r16 }, R|o16 },
	{ SUB, 0x29, X, 2, { rm32, r32 }, R|o32 },
	{ SUB, 0x2a, X, 2, { r8, rm8 }, R },
	{ SUB, 0x2a, X, 2, { r16, rm16 }, R|o16 },
	{ SUB, 0x2a, X, 2, { r32, rm32 }, R|o32 },
	{ SUB, 0x80, 5, 2, { rm8, imm8 }, R },
	{ SUB, 0x81, 5, 2, { rm16, imm16 }, R|o16 },
	{ SUB, 0x81, 5, 2, { rm32, imm32 }, R|o32 },
	{ SUB, 0x83, 5, 2, { rm16, imm8 }, R|o16 },
	{ SUB, 0x83, 5, 2, { rm32, imm8 }, R|o32 },
	{ SUB, 0x2c, X, 2, { al, imm8 }, 0 },
	{ SUB, 0x2d, X, 2, { ax, imm16 }, o16 },
	{ SUB, 0x2d, X, 2, { eax, imm32 }, o32 },

	{ SYSCALL, 0x0f05, X, 0, { 0 }, 0 },

	{ SYSENTER, 0x0f34, X, 0, { 0 }, 0 },

	{ SYSEXIT, 0x0f35, X, 0, { 0 }, 0 },

	{ SYSRET, 0x0f07, X, 0, { 0 }, 0 },

	{ TEST, 0x84, X, 2, { rm8, r8 }, R },
	{ TEST, 0x85, X, 2, { rm16, r16 }, R|o16 },
	{ TEST, 0x85, X, 2, { rm32, r32 }, R|o32 },
	{ TEST, 0xf6, 0, 2, { rm8, imm8 }, R },
	{ TEST, 0xf7, 0, 2, { rm16, imm16 }, R|o16 },
	{ TEST, 0xf7, 0, 2, { rm32, imm32 }, R|o32 },
	{ TEST, 0xa8, X, 2, { al, imm8 }, 0 },
	{ TEST, 0xa9, X, 2, { ax, imm16 }, o16 },
	{ TEST, 0xa9, X, 2, { eax, imm32 }, o32 },

	{ UD0, 0x0fff, X, 0, { 0 }, 0 },
	{ UD1, 0x0fb9, X, 0, { 0 }, 0 },
	{ UD2, 0x0f0b, X, 0, { 0 }, 0 },

	{ VERR, 0x0f00, 4, 1, { rm16 }, R },
	{ VERW, 0x0f00, 5, 1, { rm16 }, R },

	{ WAIT, 0x9b, X, 0, { 0 }, 0 },
	{ FWAIT, 0x9b, X, 0, { 0 }, 0 },

	{ WBINVD, 0x0f09, X, 0, { 0 }, 0 },

	{ WRMSR, 0x0f30, X, 0, { 0 }, 0 },

	{ WRSHR, 0x0f37, 0, 1, { rm32 }, R },

	{ XADD, 0x0fc0, X, 2, { rm8, r8 }, R },
	{ XADD, 0x0fc1, X, 2, { rm16, r16 }, R|o16 },
	{ XADD, 0x0fc1, X, 2, { rm32, r32 }, R|o32 },

	{ XBITS, 0x0fa6, X, 2, { r16, rm16 }, R|o16 },
	{ XBITS, 0x0fa6, X, 2, { r32, rm32 }, R|o32 },

	{ XCHG, 0x86, X, 2, { r8, rm8 }, R },
	{ XCHG, 0x87, X, 2, { r16, rm16 }, R|o16 },
	{ XCHG, 0x87, X, 2, { r32, rm32 }, R|o32 },
	{ XCHG, 0x86, X, 2, { rm8, r8 }, R },
	{ XCHG, 0x86, X, 2, { rm16, r16 }, R|o16 },
	{ XCHG, 0x86, X, 2, { rm32, r32 }, R|o32 },
	{ XCHG, 0x87, X, 2, { r16, rm16 }, R | o16 },
	{ XCHG, 0x87, X, 2, { r32, rm32 }, R | o32 },
	{ XCHG, 0x90, X, 2, { r16, ax }, rcode|o16 },
	{ XCHG, 0x90, X, 2, { r32, eax }, rcode|o32 },

	{ XLAT, 0xd7, X, 0, { 0 }, 0 },
	{ XLATB, 0xd7, X, 0, { 0 }, 0 },

	{ XOR, 0x30, X, 2, { rm8, r8 }, R },
	{ XOR, 0x31, X, 2, { rm16, r16 }, R|o16 },
	{ XOR, 0x31, X, 2, { rm32, r32 }, R|o32 },
	{ XOR, 0x32, X, 2, { r8, rm8 }, R },
	{ XOR, 0x33, X, 2, { r16, rm16 }, R|o16 },
	{ XOR, 0x33, X, 2, { r32, rm32 }, R|o32 },
	{ XOR, 0x80, 6, 2, { rm8, imm8 }, R },
	{ XOR, 0x81, 6, 2, { rm16, imm16 }, R|o16 },
	{ XOR, 0x81, 6, 2, { rm32, imm32 }, R|o32 },
	{ XOR, 0x83, 6, 2, { rm16, imm8 }, R|o16 },
	{ XOR, 0x83, 6, 2, { rm32, imm8 }, R|o32 },
	{ XOR, 0x34, X, 2, { al, imm8 }, 0 },
	{ XOR, 0x35, X, 2, { ax, imm16 }, o16 },
	{ XOR, 0x35, X, 2, { eax, imm32 }, o32 },

	//
	// FPU instructions
	//
	// Note: Some instructions include ModRM as the 2nd
	// byte of the opcode. Since the opcode extension is
	// already part of the opcode, we don't need to write
	// the ModRM byte (don't need R flag) for these
	//

	{ FBLD, 0xdf, 4, 1, { mem80 }, R },
	{ FBSTP, 0xdf, 6, 1, { mem80 }, R },
	{ FCMOVB, 0xda, 0, 2, { st0|I, sti }, R },
	{ FCMOVBE, 0xda, 2, 2, { st0|I, sti }, R },
	{ FCMOVE, 0xda, 1, 2, { st0|I, sti }, R },
	{ FCMOVNB, 0xdb, 0, 2, { st0|I, sti }, R },
	{ FCMOVNBE, 0xdb, 2, 2, { st0|I, sti }, R },
	{ FCMOVNE, 0xdb, 1, 2, { st0|I, sti }, R },
	{ FCMOVNU, 0xdb, 3, 2, { st0|I, sti }, R },
	{ FCMOVU, 0xda, 3, 2, { st0|I, sti }, R },
	{ FILD, 0xdf, 0, 2, { st0|I, mem16 }, R },
	{ FILD, 0xdb, 0, 2, { st0|I, mem32 }, R },
	{ FILD, 0xdf, 5, 2, { st0|I, mem64 }, R },
	{ FIST, 0xdf, 2, 1, { mem16 }, R },
	{ FISTP, 0xdf, 3, 1, { mem16 }, R },
	{ FISTP, 0xdb, 3, 1, { mem32 }, R },
	{ FISTP, 0xdf, 7, 1, { mem64 }, R },
	{ FLD, 0xdb, 5, 1, { mem80 }, R },
	{ FLD, 0xd9, 0, 1, { mem32 }, R },
	{ FLD, 0xd9, 0, 1, { sti }, R },
	{ FLD, 0xdd, 0, 1, { mem64 }, R },
	{ FST, 0xd9, 2, 1, { mem32 }, R },
	{ FST, 0xdd, 2, 1, { mem64 }, R },
	{ FST, 0xdd, 2, 1, { sti }, R },
	{ FSTP, 0xd9, 3, 1, { mem32 }, R },
	{ FSTP, 0xdb, 7, 1, { mem80 }, R },
	{ FSTP, 0xdd, 3, 1, { mem64 }, R },
	{ FSTP, 0xdd, 3, 1, { sti }, R },
	{ FXCH, 0xd9, 1, 1, { sti }, R },
	{ FABS, 0xd9e1, 4, 0, { 0 }, 0 },
	{ FADD, 0xdc, 0, 1, { mem64 }, R },
	{ FADD, 0xdc, 0, 2, { sti, st0|I }, R },
	{ FADD, 0xd8, 0, 1, { mem32 }, R },
	{ FADD, 0xd8, 0, 2, { st0|I, sti }, R },
	{ FADDP, 0xde, 0, 2, { sti, st0|I }, R },
	{ FCHS, 0xd9e0, X, 0, { 0 }, 0},
	{ FDIV, 0xdc, 6, 1, { mem64 }, R },
	{ FDIV, 0xdc, 7, 2, { sti, st0|I}, R },
	{ FDIV, 0xd8, 6, 1, { mem32 }, R },
	{ FDIV, 0xd8, 6, 2, { st0|I, sti }, R },
	{ FDIVP, 0xde, 7, 2, { sti, st0|I }, R },
	{ FDIVR, 0xd8, 7, 1, { mem32 }, R },
	{ FDIVR, 0xd8, 7, 2, { st0|I, sti }, R },
	{ FDIVR, 0xdc, 6, 2, { sti, st0|I }, R },
	{ FDIVR, 0xdc, 7, 1, { mem64 }, R },
	{ FDIVRP, 0xde, 6, 2, { sti, st0|I }, R },
	{ FIADD, 0xde, 0, 2, { st0|I, mem16 }, R },
	{ FIADD, 0xda, 0, 2, { st0|I, mem32 }, R },
	{ FIDIV, 0xda, 6, 1, { mem32 }, R },
	{ FIDIV, 0xde, 6, 1, { mem16 }, R },
	{ FIDIVR, 0xde, 7, 1, { mem16 }, R },
	{ FIDIVR, 0xda, 7, 1, { mem32 }, R },
	{ FIMUL, 0xde, 1, 1, { mem16 }, R },
	{ FIMUL, 0xda, 1, 1, { mem32 }, R },
	{ FISUB, 0xde, 4, 1, { mem16 }, R },
	{ FISUB, 0xda, 4, 1, { mem32 }, R },
	{ FISUBR, 0xde, 5, 1, { mem16 }, R },
	{ FISUBR, 0xda, 5, 1, { mem32 }, R },
	{ FMUL, 0xd8, 1, 1, { mem32 }, R },
	{ FMUL, 0xd8, 1, 2, { st0|I, sti }, R },
	{ FMUL, 0xdc, 1, 1, { mem64 }, R },
	{ FMUL, 0xdc, 1, 2, { sti, st0|I }, R },
	{ FMULP, 0xde, 1, 2, { sti, st0|I }, R },
	{ FPREM, 0xd9f8, 7, 0, { 0 }, 0 },
	{ FPREM1, 0xd9f5, 6, 0, { 0 }, 0 },
	{ FRNDINT, 0xd9fc, 7, 0, { 0 }, 0 },
	{ FSCALE, 0xd9fd, 7, 0, { 0 }, 0 },
	{ FSQRT, 0xd9fa, 7, 0, { 0 }, 0 },
	{ FSUB, 0xd8, 4, 1, { st0, mem32 }, R },
	{ FSUB, 0xd8, 4, 2, { st0|I, sti }, R },
	{ FSUB, 0xdc, 4, 1, { st0, mem64 }, R },
	{ FSUB, 0xdc, 5, 2, { sti, st0|I }, R },
	{ FSUBP, 0xde, 5, 2, { sti, st0|I }, R },
	{ FSUBR, 0xd8, 5, 1, { mem32 }, R },
	{ FSUBR, 0xd8, 5, 2, { st0|I, sti }, R },
	{ FSUBR, 0xdc, 4, 2, { sti, st0|I }, R },
	{ FSUBR, 0xdc, 5, 1, { mem64 }, R },
	{ FSUBRP, 0xde, 4, 2, { sti, st0|I }, R },
	{ FXTRACT, 0xd9f4, 6, 0, { 0 }, 0 },
	{ FCOM, 0xd8, 2, 1, { sti }, R },
	{ FCOM, 0xd8, 2, 1, { mem32 }, R },
	{ FCOM, 0xdc, 2, 1, { mem64 }, R },
	{ FCOMI, 0xdb, 6, 2, { st0|I, sti }, R },
	{ FCOMIP, 0xdf, 6, 2, { st0|I, sti }, R },
	{ FCOMP, 0xd8, 3, 1, { sti }, R },
	{ FCOMP, 0xd8, 3, 1, { mem32 }, R },
	{ FCOMP, 0xdc, 3, 1, { mem64 }, R },
	{ FCOMPP, 0xded9, 3, 0, { 0 }, 0 },
	{ FICOM, 0xde, 2, 1, { mem16 }, R },
	{ FICOM, 0xda, 2, 1, { mem32 }, R },
	{ FICOMP, 0xde, 3, 1, { mem16 }, R },
	{ FICOMP, 0xda, 3, 1, { mem32 }, R },
	{ FTST, 0xd9e4, 4, 0, { 0 }, 0 },
	{ FUCOM, 0xdd, 4, 1, { sti }, R },
	{ FUCOMI, 0xdb, 5, 2, { st0|I, sti }, R },
	{ FUCOMIP, 0xdf, 5, 2, { st0|I, sti }, R },
	{ FUCOMP, 0xdd, 5, 1, { sti }, R },
	{ FUCOMPP, 0xdae9, 5, 0, { 0 }, 0 },
	{ FXAM, 0xd9e5, 4, 0, { 0 }, 0 },
	{ F2XM1, 0xd9f0, 6, 0, { 0 }, 0 },
	{ FCOS, 0xd9ff, 7, 0, { 0 }, 0 },
	{ FPATAN, 0xd9f3, 6, 0, { 0 }, 0 },
	{ FPTAN, 0xd9f2, 6, 0, { 0 }, 0 },
	{ FSIN, 0xd9fe, 7, 0, { 0 }, 0 },
	{ FSINCOS, 0xd9fb, 7, 0, { 0 }, 0 },
	{ FYL2X, 0xd9f1, 6, 0, { 0 }, 0 },
	{ FYL2XP1, 0xd9f9, 7, 0, { 0 }, 0 },
	{ FLD1, 0xd9e8, 5, 0, { 0 }, 0 },
	{ FLDL2E, 0xd9ea, 5, 0, { 0 }, 0 },
	{ FLDL2T, 0xd9e9, 5, 0, { 0 }, 0 },
	{ FLDLG2, 0xd9ec, 5, 0, { 0 }, 0 },
	{ FLDLN2, 0xd9ed, 5, 0, { 0 }, 0 },
	{ FLDPI, 0xd9eb, 5, 0, { 0 }, 0 },
	{ FLDZ, 0xd9ee, 5, 0, { 0 }, 0 },
	{ FCLEX, 0xdbe2, 4, 0, { 0 }, 0 },
	{ FDECSTP, 0xd9f6, 6, 0, { 0 }, 0 },
	{ FFREE, 0xdd, 0, 1, { sti }, R },
	{ FINCSTP, 0xd9f7, 6, 0, { 0 }, 0 },
	{ FINIT, 0xdbe3, 4, 0, { 0 }, 0 },
	{ FLDCW, 0xd9, 5, 1, { mem16 }, R },
	{ FLDENV, 0xd9, 4, 1, { mem }, R },
	{ FNCLEX, 0xdbe2, 4, 0, { 0 }, 0 },
	{ FNINIT, 0xdbe3, 4, 0, { 0 }, 0 },
	{ FNOP, 0xd9d0, 2, 0, { 0 }, 0 },
	{ FNSAVE, 0xdd, 6, 1, { mem }, R },
	{ FNSTCW, 0xd9, 7, 1, { mem16 }, R },
	{ FNSTENV, 0xd9, 6, 1, { mem }, R },
	{ FNSTSW, 0xdfe0, 4, 1, { ax|I }, 0 },
	{ FNSTSW, 0xdd, 7, 1, { mem16 }, R },
	{ FRSTOR, 0xdd, 4, 0, { 0 }, 0 },
	{ FSAVE, 0xdd, 6, 1, { mem }, R },
	{ FSTCW, 0xd9, 7, 1, { mem16 }, R },
	{ FSTENV, 0xd9, 6, 1, { mem16 }, R },
	{ FSTSW, 0xdfe0, 4, 1, { ax|I }, 0 },
	{ FSTSW, 0xdd, 7, 1, { mem16 }, R },

	//
	// SIMD
	//

	//	{ FXRSTOR, 0, X, 0, { 0 }, 0 },
	//	{ FXSAVE, 0, X, 0, { 0 }, 0 },

	{ MOVD, 0x0f6e, X, 2, { mm, r32 }, R },
	{ MOVD, 0x0f6e, X, 2, { mm, mem32 }, R },
	{ MOVD, 0x0f7e, X, 2, { r32, mm }, R },
	{ MOVD, 0x0f7e, X, 2, { mem32, mm }, R },
	{ MOVD, 0x660f6e, X, 2, { xmm, r32 }, R },
	{ MOVD, 0x660f6e, X, 2, { xmm, mem32 }, R },
	{ MOVD, 0x660f7e, X, 2, { r32, xmm }, R },
	{ MOVD, 0x660f7e, X, 2, { mem32, xmm }, R },

	{ MOVQ, 0x0f6f, X, 2, { mm, mm }, R },
	{ MOVQ, 0x0f6f, X, 2, { mm, mem64 }, R },
	{ MOVQ, 0x0f7f, X, 2, { mm, mm }, R },
	{ MOVQ, 0x0f7f, X, 2, { mem64, mm }, R },
	{ MOVQ, 0xf30f7e, X, 2, { xmm, xmm }, R },
	{ MOVQ, 0xf30f7e, X, 2, { xmm, mem64 }, R },
	{ MOVQ, 0x660fd6, X, 2, { xmm, xmm }, R },
	{ MOVQ, 0x660fd6, X, 2, { mem64, xmm }, R },

	{ PACKSSDW, 0x0f6b, X, 2, { mm, mm }, R },
	{ PACKSSDW, 0x0f6b, X, 2, { mm, mem64 }, R },
	{ PACKSSDW, 0x660f6b, X, 2, { xmm, xmm }, R },
	{ PACKSSDW, 0x660f6b, X, 2, { xmm, mem128 }, R },

	{ PACKSSWB, 0x0f63, X, 2, { mm, mm }, R },
	{ PACKSSWB, 0x0f63, X, 2, { mm, mem64 }, R },
	{ PACKUSWB, 0x0f67, X, 2, { mm, mm }, R },
	{ PACKUSWB, 0x0f67, X, 2, { mm, mem64 }, R },
	{ PACKUSWB, 0x660f67, X, 2, { xmm, xmm }, R },
	{ PACKUSWB, 0x660f67, X, 2, { xmm, mem128 }, R },

	{ PUNPCKHBW, 0x0f68, X, 2, { mm, mm }, R },
	{ PUNPCKHBW, 0x0f68, X, 2, { mm, mem64 }, R },
	{ PUNPCKHBW, 0x660f68, X, 2, { xmm, xmm }, R },
	{ PUNPCKHBW, 0x660f68, X, 2, { xmm, mem128 }, R },

	{ PUNPCKHDQ, 0x0f6a, X, 2, { mm, mm }, R },
	{ PUNPCKHDQ, 0x0f6a, X, 2, { mm, mem64 }, R },
	{ PUNPCKHDQ, 0x660f6a, X, 2, { xmm, xmm }, R },
	{ PUNPCKHDQ, 0x660f6a, X, 2, { xmm, mem128 }, R },

	{ PUNPCKHWD, 0x660f69, X, 2, { mm, mm }, R },
	{ PUNPCKHWD, 0x660f69, X, 2, { mm, mem64 }, R },
	{ PUNPCKHWD, 0x660f69, X, 2, { xmm, xmm }, R },
	{ PUNPCKHWD, 0x660f69, X, 2, { xmm, mem128 }, R },

	{ PUNPCKLBW, 0x0f60, X, 2, { mm, mm }, R },
	{ PUNPCKLBW, 0x0f60, X, 2, { mm, mem64 }, R },
	{ PUNPCKLBW, 0x660f60, X, 2, { xmm, xmm }, R },
	{ PUNPCKLBW, 0x660f60, X, 2, { xmm, mem128 }, R },

	{ PUNPCKLDQ, 0x0f62, X, 2, { mm, mm }, R },
	{ PUNPCKLDQ, 0x0f62, X, 2, { mm, mem64 }, R },
	{ PUNPCKLDQ, 0x660f62, X, 2, { xmm, xmm }, R },
	{ PUNPCKLDQ, 0x660f62, X, 2, { xmm, mem128 }, R },

	{ PUNPCKLWD, 0x0f61, X, 2, { mm, mm }, R },
	{ PUNPCKLWD, 0x0f61, X, 2, { mm, mem64 }, R },
	{ PUNPCKLWD, 0x660f61, X, 2, { xmm, xmm }, R },
	{ PUNPCKLWD, 0x660f61, X, 2, { xmm, mem128 }, R },

	{ PADDB, 0x0ffc, X, 2, { mm, mm }, R },
	{ PADDB, 0x0ffc, X, 2, { mm, mem64 }, R },
	{ PADDB, 0x660ffc, X, 2, { xmm, xmm }, R },
	{ PADDB, 0x660ffc, X, 2, { xmm, mem128 }, R },

	{ PADDD, 0x0ffe, X, 2, { mm, mm }, R },
	{ PADDD, 0x0ffe, X, 2, { mm, mem64 }, R },
	{ PADDD, 0x660ffe, X, 2, { xmm, xmm }, R },
	{ PADDD, 0x660ffe, X, 2, { xmm, mem128 }, R },

	{ PADDSB, 0x0fec, X, 2, { mm, mm }, R },
	{ PADDSB, 0x0fec, X, 2, { mm, mem64 }, R },
	{ PADDSB, 0x660fec, X, 2, { xmm, xmm }, R },
	{ PADDSB, 0x660fec, X, 2, { xmm, mem128 }, R },

	{ PADDSW, 0x0fed, X, 2, { mm, mm }, R },
	{ PADDSW, 0x0fed, X, 2, { mm, mem64 }, R },
	{ PADDSW, 0x660fed, X, 2, { mm, mm }, R },
	{ PADDSW, 0x660fed, X, 2, { mm, mem128 }, R },

	{ PADDUSB, 0x0fdc, X, 2, { mm, mm }, R },
	{ PADDUSB, 0x0fdc, X, 2, { mm, mem64 }, R },
	{ PADDUSB, 0x660fdc, X, 2, { xmm, xmm }, R },
	{ PADDUSB, 0x660fdc, X, 2, { xmm, mem128 }, R },

	{ PADDUSW, 0x0fdd, X, 2, { mm, mm }, R },
	{ PADDUSW, 0x0fdd, X, 2, { mm, mem64 }, R },
	{ PADDUSW, 0x660fdd, X, 2, { xmm, xmm }, R },
	{ PADDUSW, 0x660fdd, X, 2, { xmm, mem128 }, R },

	{ PADDW, 0x0ffd, X, 2, { mm, mm }, R },
	{ PADDW, 0x0ffd, X, 2, { mm, mem64 }, R },
	{ PADDW, 0x660ffd, X, 2, { xmm, xmm }, R },
	{ PADDW, 0x660ffd, X, 2, { xmm, mem128 }, R },

	{ PMADDWD, 0x0ff5, X, 2, { mm, mm }, R },
	{ PMADDWD, 0x0ff5, X, 2, { mm, mem64 }, R },
	{ PMADDWD, 0x660ff5, X, 2, { xmm, xmm }, R },
	{ PMADDWD, 0x660ff5, X, 2, { xmm, mem128 }, R },

	{ PMULHW, 0x0fe5, X, 2, { mm, mm }, R },
	{ PMULHW, 0x0fe5, X, 2, { mm, mem64 }, R },
	{ PMULHW, 0x660fe5, X, 2, { xmm, xmm }, R },
	{ PMULHW, 0x660fe5, X, 2, { xmm, mem128 }, R },

	{ PMULLW, 0x0fd5, X, 2, { mm, mm }, R },
	{ PMULLW, 0x0fd5, X, 2, { mm, mem64 }, R },
	{ PMULLW, 0x660fd5, X, 2, { xmm, xmm }, R },
	{ PMULLW, 0x660fd5, X, 2, { xmm, mem128 }, R },

	{ PSUBB, 0x0ff8, X, 2, { mm, mm }, R },
	{ PSUBB, 0x0ff8, X, 2, { mm, mem64 }, R },
	{ PSUBB, 0x660ff8, X, 2, { xmm, xmm }, R },
	{ PSUBB, 0x660ff8, X, 2, { xmm, mem128 }, R },

	{ PSUBD, 0x0ffa, X, 2, { mm, mm }, R },
	{ PSUBD, 0x0ffa, X, 2, { mm, mem64 }, R },
	{ PSUBD, 0x660ffa, X, 2, { xmm, xmm }, R },
	{ PSUBD, 0x660ffa, X, 2, { xmm, mem128 }, R },

	{ PSUBSB, 0x0fe8, X, 2, { mm, mm }, R },
	{ PSUBSB, 0x0fe8, X, 2, { mm, mem64 }, R },
	{ PSUBSB, 0x660fe8, X, 2, { xmm, xmm }, R },
	{ PSUBSB, 0x660fe8, X, 2, { xmm, mem128 }, R },

	{ PSUBSW, 0x0fe9, X, 2, { mm, mm }, R },
	{ PSUBSW, 0x0fe9, X, 2, { mm, mem64 }, R },
	{ PSUBSW, 0x660fe9, X, 2, { xmm, xmm }, R },
	{ PSUBSW, 0x660fe9, X, 2, { xmm, mem128 }, R },

	{ PSUBUSB, 0x0fd8, X, 2, { mm, mm }, R },
	{ PSUBUSB, 0x0fd8, X, 2, { mm, mem64 }, R },
	{ PSUBUSB, 0x660fd8, X, 2, { xmm, xmm }, R },
	{ PSUBUSB, 0x660fd8, X, 2, { xmm, mem128 }, R },

	{ PSUBUSW, 0x0fd9, X, 2, { mm, mm }, R },
	{ PSUBUSW, 0x0fd9, X, 2, { mm, mem64 }, R },
	{ PSUBUSW, 0x660fd9, X, 2, { xmm, xmm }, R },
	{ PSUBUSW, 0x660fd9, X, 2, { xmm, mem128 }, R },

	{ PSUBW, 0x0ff9, X, 2, { mm, mm }, R },
	{ PSUBW, 0x0ff9, X, 2, { mm, mem64 }, R },
	{ PSUBW, 0x660ff9, X, 2, { xmm, xmm }, R },
	{ PSUBW, 0x660ff9, X, 2, { xmm, mem128 }, R },

	{ PCMPEQB, 0x0f74, X, 2, { mm, mm }, R },
	{ PCMPEQB, 0x0f74, X, 2, { mm, mem64 }, R },
	{ PCMPEQB, 0x660f74, X, 2, { xmm, xmm }, R },
	{ PCMPEQB, 0x660f74, X, 2, { xmm, mem128 }, R },

	{ PCMPEQD, 0x0f76, X, 2, { mm, mm }, R },
	{ PCMPEQD, 0x0f76, X, 2, { mm, mem64 }, R },
	{ PCMPEQD, 0x660f76, X, 2, { xmm, xmm }, R },
	{ PCMPEQD, 0x660f76, X, 2, { xmm, mem128 }, R },

	{ PCMPEQW, 0x0f75, X, 2, { mm, mm }, R },
	{ PCMPEQW, 0x0f75, X, 2, { mm, mem64 }, R },
	{ PCMPEQW, 0x660f75, X, 2, { xmm, xmm }, R },
	{ PCMPEQW, 0x660f75, X, 2, { xmm, mem128 }, R },

	{ PCMPGTB, 0x0f64, X, 2, { mm, mm }, R },
	{ PCMPGTB, 0x0f64, X, 2, { mm, mem64 }, R },
	{ PCMPGTB, 0x660f64, X, 2, { xmm, xmm }, R },
	{ PCMPGTB, 0x660f64, X, 2, { xmm, mem128 }, R },

	{ PCMPGTD, 0x0f66, X, 2, { mm, mm }, R },
	{ PCMPGTD, 0x0f66, X, 2, { mm, mem64 }, R },
	{ PCMPGTD, 0x660f66, X, 2, { xmm, xmm }, R },
	{ PCMPGTD, 0x660f66, X, 2, { xmm, mem128 }, R },

	{ PCMPGTW, 0x0f65, X, 2, { mm, mm }, R },
	{ PCMPGTW, 0x0f65, X, 2, { mm, mem64 }, R },
	{ PCMPGTW, 0x660f65, X, 2, { xmm, xmm }, R },
	{ PCMPGTW, 0x660f65, X, 2, { xmm, mem128 }, R },

	{ PAND, 0x0fdb, X, 2, { mm, mm }, R },
	{ PAND, 0x0fdb, X, 2, { mm, mem64 }, R },
	{ PAND, 0x660fdb, X, 2, { xmm, xmm }, R },
	{ PAND, 0x660fdb, X, 2, { xmm, mem128 }, R },

	{ PANDN, 0x0fdf, X, 2, { mm, mm }, R },
	{ PANDN, 0x0fdf, X, 2, { mm, mem64 }, R },
	{ PANDN, 0x660fdf, X, 2, { xmm, xmm }, R },
	{ PANDN, 0x660fdf, X, 2, { xmm, mem128 }, R },

	{ POR, 0x0feb, X, 2, { mm, mm }, R },
	{ POR, 0x0feb, X, 2, { mm, mem64 }, R },
	{ POR, 0x660feb, X, 2, { xmm, xmm }, R },
	{ POR, 0x660feb, X, 2, { xmm, mem128 }, R },

	{ PXOR, 0x0fef, X, 2, { mm, mm }, R },
	{ PXOR, 0x0fef, X, 2, { mm, mem64 }, R },
	{ PXOR, 0x660fef, X, 2, { xmm, xmm }, R },
	{ PXOR, 0x660fef, X, 2, { xmm, mem128 }, R },

	{ PSLLD, 0x0ff2, X, 2, { mm, mm }, R },
	{ PSLLD, 0x0ff2, X, 2, { mm, mem64 }, R },
	{ PSLLD, 0x660ff2, X, 2, { xmm, xmm }, R },
	{ PSLLD, 0x660ff2, X, 2, { xmm, mem128 }, R },
	{ PSLLD, 0x0f72, 6, 2, { mm, imm8 }, R },
	{ PSLLD, 0x660f72, 6, 2, { xmm, imm8 }, R },

	{ PSLLQ, 0x0ff3, X, 2, { mm, mm }, R },
	{ PSLLQ, 0x0ff3, X, 2, { mm, mem64 }, R },
	{ PSLLQ, 0x660ff3, X, 2, { xmm, xmm }, R },
	{ PSLLQ, 0x660ff3, X, 2, { xmm, mem128 }, R },
	{ PSLLQ, 0x0f73, 6, 2, { mm, imm8 }, R },
	{ PSLLQ, 0x660f73, 6, 2, { xmm, imm8 }, R },

	{ PSLLW, 0x0ff1, X, 2, { mm, mm }, R },
	{ PSLLW, 0x0ff1, X, 2, { mm, mem64 }, R },
	{ PSLLW, 0x660ff1, X, 2, { xmm, xmm }, R },
	{ PSLLW, 0x660ff1, X, 2, { xmm, mem128 }, R },
	{ PSLLW, 0x0f71, 6, 2, { mm, imm8 }, R },
	{ PSLLW, 0x660f71, 6, 2, { xmm, imm8 }, R },

	{ PSRAD, 0x0fe2, X, 2, { mm, mm }, R },
	{ PSRAD, 0x0fe2, X, 2, { mm, mem64 }, R },
	{ PSRAD, 0x660fe2, X, 2, { xmm, xmm }, R },
	{ PSRAD, 0x660fe2, X, 2, { xmm, mem128 }, R },
	{ PSRAD, 0x0f72, 4, 2, { mm, imm8 }, R },
	{ PSRAD, 0x660f72, 4, 2, { xmm, imm8 }, R },

	{ PSRAW, 0x0fe1, X, 2, { mm, mm }, R },
	{ PSRAW, 0x0fe1, X, 2, { mm, mem64 }, R },
	{ PSRAW, 0x660fe1, X, 2, { xmm, xmm }, R },
	{ PSRAW, 0x660fe1, X, 2, { xmm, mem128 }, R },
	{ PSRAW, 0x0f71, 4, 2, { mm, imm8 }, R },
	{ PSRAW, 0x660f71, 4, 2, { xmm, imm8 }, R },

	{ PSRLD, 0x0fd2, X, 2, { mm, mm }, R },
	{ PSRLD, 0x0fd2, X, 2, { mm, mem64 }, R },
	{ PSRLD, 0x660fd2, X, 2, { xmm, xmm }, R },
	{ PSRLD, 0x660fd2, X, 2, { xmm, mem128 }, R },
	{ PSRLD, 0x0f72, 2, 2, { mm, imm8 }, R },
	{ PSRLD, 0x660f72, 2, 2, { xmm, imm8 }, R },

	{ PSRLQ, 0x0fd3, X, 2, { mm, mm }, R },
	{ PSRLQ, 0x0fd3, X, 2, { mm, mem64 }, R },
	{ PSRLQ, 0x660fd3, X, 2, { xmm, xmm }, R },
	{ PSRLQ, 0x660fd3, X, 2, { xmm, mem128 }, R },
	{ PSRLQ, 0x0f73, 2, 2, { mm, imm8 }, R },

	{ PSRLW, 0x0fd1, X, 2, { mm, mm }, R },
	{ PSRLW, 0x0fd1, X, 2, { mm, mem64 }, R },
	{ PSRLW, 0x660fd1, X, 2, { xmm, xmm }, R },
	{ PSRLW, 0x660fd1, X, 2, { xmm, mem128 }, R },
	{ PSRLW, 0x0f71, 2, 2, { mm, imm8 }, R },
	{ PSRLW, 0x660f71, 2, 2, { xmm, imm8 }, R },

	{ EMMS, 0x0f77, X, 0, { 0 }, 0 },

	//
	// SSE 2
	//

	{ MOVAPD, 0x660f28, X, 2, { xmm, xmm }, R },
	{ MOVAPD, 0x660f28, X, 2, { xmm, mem128 }, R },
	{ MOVAPD, 0x660f29, X, 2, { xmm, xmm }, R },
	{ MOVAPD, 0x660f29, X, 2, { mem128, xmm }, R },

	{ MOVHPD, 0x660f16, X, 2, { xmm, mem64 }, R },
	{ MOVHPD, 0x660f17, X, 2, { mem64, xmm }, R },

	{ MOVLPD, 0x660f12, X, 2, { xmm, mem64 }, R },
	{ MOVLPD, 0x660f13, X, 2, { mem64, xmm }, R },

	{ MOVMSKPD, 0x660f50, X, 2, { r32, xmm }, R },

	{ MOVSD, 0xf20f10, X, 2, { xmm, xmm }, R },
	{ MOVSD, 0xf20f10, X, 2, { xmm, mem64 }, R },
	{ MOVSD, 0xf20f11, X, 2, { xmm, xmm }, R },
	{ MOVSD, 0xf20f11, X, 2, { mem64, xmm }, R },

	{ MOVUPD, 0x660f10, X, 2, { xmm, xmm }, R },
	{ MOVUPD, 0x660f10, X, 2, { xmm, mem128 }, R },
	{ MOVUPD, 0x660f11, X, 2, { xmm, xmm }, R },
	{ MOVUPD, 0x660f11, X, 2, { mem128, xmm }, R },

	{ ADDPD, 0x660f58, X, 2, { xmm, xmm }, R },
	{ ADDPD, 0x660f58, X, 2, { xmm, mem128 }, R },

	{ ADDSD, 0xf20ff8, X, 2, { xmm, xmm }, R },
	{ ADDSD, 0xf20ff8, X, 2, { xmm, mem64 }, R },

	{ DIVPD, 0x660f5e, X, 2, { xmm, xmm }, R },
	{ DIVPD, 0x660f5e, X, 2, { xmm, mem128 }, R },

	{ DIVSD, 0xf20f5e, X, 2, { xmm, xmm }, R },
	{ DIVSD, 0xf20f5e, X, 2, { xmm, mem64 }, R },

	{ MAXPD, 0x660f5f, X, 2, { xmm, xmm }, R },
	{ MAXPD, 0x660f5f, X, 2, { xmm, mem128 }, R },

	{ MAXSD, 0xf20f5f, X, 2, { xmm, xmm }, R },
	{ MAXSD, 0xf20f5f, X, 2, { xmm, mem64 }, R },

	{ MINPD, 0x660f5d, X, 2, { xmm, xmm }, R },
	{ MINPD, 0x660f5d, X, 2, { xmm, mem128 }, R },

	{ MINSD, 0xf20f5d, X, 2, { xmm, xmm }, R },
	{ MINSD, 0xf20f5d, X, 2, { xmm, mem64 }, R },

	{ MULPD, 0x660ff9, X, 2, { xmm, xmm }, R },
	{ MULPD, 0x660ff9, X, 2, { xmm, mem128 }, R },

	{ MULSD, 0xf20f59, X, 2, { xmm, xmm }, R },
	{ MULSD, 0xf20f59, X, 2, { xmm, mem64 }, R },

	{ SQRTPD, 0x660f51, X, 2, { xmm, xmm }, R },
	{ SQRTPD, 0x660f51, X, 2, { xmm, mem128 }, R },

	{ SQRTSD, 0x520f51, X, 2, { xmm, xmm }, R },
	{ SQRTSD, 0x520f51, X, 2, { xmm, mem64 }, R },

	{ SUBPD, 0x660f5c, X, 2, { xmm, xmm }, R },
	{ SUBPD, 0x660f5c, X, 2, { xmm, mem128 }, R },

	{ SUBSD, 0xf20f5c, X, 2, { xmm, xmm }, R },
	{ SUBSD, 0xf20f5c, X, 2, { xmm, mem64 }, R },

	{ ANDNPD, 0x660f55, X, 2, { xmm, xmm }, R },
	{ ANDNPD, 0x660f55, X, 2, { xmm, mem128 }, R },

	{ ANDPD, 0x660f54, X, 2, { xmm, xmm }, R },
	{ ANDPD, 0x660f54, X, 2, { xmm, mem128 }, R },

	{ ORPD, 0x660f56, X, 2, { xmm, xmm }, R },
	{ ORPD, 0x660f56, X, 2, { xmm, mem128 }, R },

	{ XORPD, 0x660f57, X, 2, { xmm, xmm }, R },
	{ XORPD, 0x660f57, X, 2, { xmm, mem128 }, R },

	{ CMPPD, 0x660fc2, X, 2, { xmm, xmm }, R },
	{ CMPPD, 0x660fc2, X, 2, { xmm, mem128 }, R },

	{ CMPSD, 0x520fc2, X, 2, { xmm, xmm }, R },
	{ CMPSD, 0x520fc2, X, 2, { xmm, mem64 }, R },

	{ COMISD, 0x660f2f, X, 2, { xmm, xmm }, R },
	{ COMISD, 0x660f2f, X, 2, { xmm, mem64 }, R },

	{ UCOMISD, 0x660f2e, X, 2, { xmm, xmm }, R },
	{ UCOMISD, 0x660f2e, X, 2, { xmm, mem64 }, R },

	{ SHUFPD, 0x660fc6, X, 3, { xmm, xmm, imm8 }, R },
	{ SHUFPD, 0x660fc6, X, 3, { xmm, mem128, imm8 }, R },

	{ UNPCKHPD, 0x660f15, X, 2, { xmm, xmm }, R },
	{ UNPCKHPD, 0x660f15, X, 2, { xmm, mem128 }, R },

	{ UNPCKLPD, 0x660f14, X, 2, { xmm, xmm }, R },
	{ UNPCKLPD, 0x660f14, X, 2, { xmm, mem128 }, R },

	{ CVTDQ2PD, 0xf30fe6, X, 2, { xmm, xmm }, R },
	{ CVTDQ2PD, 0xf30fe6, X, 2, { xmm, mem128 }, R },

	{ CVTPD2DQ, 0xf20fe6, X, 2, { xmm, xmm }, R },
	{ CVTPD2DQ, 0xf20fe6, X, 2, { xmm, mem128 }, R },

	{ CVTPD2PI, 0x660f2d, X, 2, { mm, xmm }, R },
	{ CVTPD2PI, 0x660f2d, X, 2, { mm, mem128 }, R },

	{ CVTPD2PS, 0x660ffa, X, 2, { xmm, xmm }, R },
	{ CVTPD2PS, 0x660ffa, X, 2, { xmm, mem128 }, R },

	{ CVTPI2PD, 0x660f2a, X, 2, { xmm, mm }, R },
	{ CVTPI2PD, 0x660f2a, X, 2, { xmm, mem64 }, R },

	{ CVTPS2PD, 0x0f5a, X, 2, { xmm, xmm }, R },
	{ CVTPS2PD, 0x0f5a, X, 2, { xmm, mem128 }, R },

	{ CVTSD2SI, 0xf20f2d, X, 2, { r32, xmm }, R },
	{ CVTSD2SI, 0xf20f2d, X, 2, { r32, mem64 }, R },

	{ CVTSD2SS, 0xf20f5a, X, 2, { xmm, xmm }, R },
	{ CVTSD2SS, 0xf20f5a, X, 2, { xmm, mem64 }, R },

	{ CVTSI2SD, 0xf20f2a, X, 2, { xmm, r32 }, R },
	{ CVTSI2SD, 0xf20f2a, X, 2, { xmm, mem32 }, R },

	{ CVTSS2SD, 0xf30f5a, X, 2, { xmm, xmm }, R },
	{ CVTSS2SD, 0xf30f5a, X, 2, { xmm, mem32 }, R },

	{ CVTTPD2DQ, 0x660fe6, X, 2, { xmm, xmm }, R },
	{ CVTTPD2DQ, 0x660fe6, X, 2, { xmm, mem128 }, R },

	{ CVTTPD2PI, 0x660f2c, X, 2, { mm, xmm }, R },
	{ CVTTPD2PI, 0x660f2c, X, 2, { mm, mem128 }, R },

	{ CVTTSD2SI, 0xf20f2c, X, 2, { r32, xmm }, R },
	{ CVTTSD2SI, 0xf20f2c, X, 2, { r32, mem64 }, R },

	{ CVTDQ2PS, 0x0f5b, X, 2, { xmm, xmm }, R },
	{ CVTDQ2PS, 0x0f5b, X, 2, { xmm, mem128 }, R },

	{ CVTPS2DQ, 0x660f6b, X, 2, { xmm, xmm }, R },
	{ CVTPS2DQ, 0x660f6b, X, 2, { xmm, mem128 }, R },

	{ CVTTPS2DQ, 0xf30f5b, X, 2, { xmm, xmm }, R },
	{ CVTTPS2DQ, 0xf30f5b, X, 2, { xmm, mem128 }, R },

	{ MOVDQ2Q, 0xf20fd6, X, 2, { mm, xmm }, R },

	{ MOVDQA, 0x660f6f, X, 2, { xmm, xmm }, R },
	{ MOVDQA, 0x660f6f, X, 2, { xmm, mem128 }, R },
	{ MOVDQA, 0x660f7f, X, 2, { xmm, xmm }, R },
	{ MOVDQA, 0x660f7f, X, 2, { mem128, xmm }, R },

	{ MOVDQU, 0xf30f6f, X, 2, { xmm, xmm }, R },
	{ MOVDQU, 0xf30f6f, X, 2, { xmm, mem128 }, R },
	{ MOVDQU, 0xf30f7f, X, 2, { xmm, xmm }, R },
	{ MOVDQU, 0xf30f7f, X, 2, { mem128, xmm }, R },

	{ MOVQ2DQ, 0xf30fd6, X, 2, { xmm, mm }, R },

	{ PADDQ, 0x0fd4, X, 2, { mm, mm }, R },
	{ PADDQ, 0x0fd4, X, 2, { mm, mem64 }, R },
	{ PADDQ, 0x660fd4, X, 2, { xmm, xmm }, R },
	{ PADDQ, 0x660fd4, X, 2, { xmm, mem128 }, R },

	{ PMULUDQ, 0x0ff4, X, 2, { mm, mm }, R },
	{ PMULUDQ, 0x0ff4, X, 2, { mm, mem64 }, R },
	{ PMULUDQ, 0x660ff4, X, 2, { xmm, xmm }, R },
	{ PMULUDQ, 0x660ff4, X, 2, { xmm, mem128 }, R },

	{ PSHUFD, 0x660f70, X, 3, { xmm, xmm, imm8 }, R },
	{ PSHUFD, 0x660f70, X, 3, { xmm, mem128, imm8 }, R },

	{ PSHUFHW, 0xf30f70, X, 3, { xmm, xmm, imm8 }, R },
	{ PSHUFHW, 0xf30f70, X, 3, { xmm, mem128, imm8 }, R },

	{ PSHUFLW, 0xf20f70, X, 3, { xmm, xmm, imm8 }, R },
	{ PSHUFLW, 0xf20f70, X, 3, { xmm, mem128, imm8 }, R },

	{ PSLLDQ, 0x660f73, 7, 2, { xmm, imm8 }, R },

	{ PSRLDQ, 0x660f73, 7, 2, { xmm, imm8 }, R },

	{ PSUBQ, 0x0ffb, X, 2, { mm, mm }, R },
	{ PSUBQ, 0x0ffb, X, 2, { mm, mem64 }, R },
	{ PSUBQ, 0x660ffb, X, 2, { xmm, xmm }, R },
	{ PSUBQ, 0x660ffb, X, 2, { xmm, mem128 }, R },

	{ PUNPCKHQDQ, 0x660f6d, X, 2, { xmm, xmm }, R },
	{ PUNPCKHQDQ, 0x660f6d, X, 2, { xmm, mem128 }, R },

	{ PUNPCKLQDQ, 0x660f6c, X, 2, { xmm, xmm }, R },
	{ PUNPCKLQDQ, 0x660f6c, X, 2, { xmm, mem128 }, R },

	{ CLFLUSH, 0x0fae, 7, 1, { mem8 }, R },

	{ LFENCE, 0x0fae, 5, 0, { 0 }, R },

	{ MASKMOVDQU, 0x660ff7, X, 2, { xmm, xmm }, R },

	{ MFENCE, 0x0fae, 6, 0, { 0 }, R },

	{ MOVNTDQ, 0x660fe7, X, 2, { mem128, xmm }, R },

	{ MOVNTI, 0x0fc3, X, 2, { mem32, r32 }, R },

	{ MOVNTPD, 0x660f2b, X, 2, { mem128, xmm }, R },

};

unsigned long _table_size = sizeof(_instructions) / sizeof(InstrTableEntry);

// for the shift by 1 instructions way above..
// we can add operand class "Constant" and put
// the constant in the low parts of the bit field.
// That, in addition to using I (if it is implied,
// only for matching) then they may work like this:
// C|I|1 for "Constant operand 1, ignore"

