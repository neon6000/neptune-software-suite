/************************************************************************
*
*	file.c - Source file reading.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <assert.h>
#include <malloc.h>
#include <string.h>
#include "nasm.h"

/* file stack. */
PRIVATE FileVector* _files = NULL;

PRIVATE COORD _coord;

/* pushes a file to file stack. */
PRIVATE void FilePush(IN PSOURCE file) {

	if (!_files)
		_files = NewVector(0, sizeof(PSOURCE));
	VectorPush(_files, &(uintptr_t)file);
}

/* pops file from file stack. */
PRIVATE void FilePop(IN PSOURCE file) {

	if (VectorLength(_files) > 0)
		VectorPop(_files, &file);
}

/* get current file. */
PRIVATE PSOURCE FileGetCurrent(void) {

	PSOURCE file;
	VectorLastElement(_files, &file);
	return file;
}

/* close file. */
PRIVATE void FileClose(IN PSOURCE file) {

	assert(file != NULL);
	if (file->stream)
		fclose(file->stream);
	free(file);
}

#define PEEK 1
#define GET  2

/* get character from file. */
PRIVATE int _FileGetc(int flag) {

	PSOURCE file;
	int     c;
	int     c2;

	file = FileGetCurrent();

	assert(file != NULL);

	if (file->pushback && GetBufferLength(file->pushback) > 0) {
		c = (int)BufferPop(file->pushback);
		return c;
	}

	if (file->pushback) {
		BufferFree(file->pushback);
		file->pushback = NULL;
	}

	if (file->eof)
		return EOF;

	if (file->eol) {
		file->eol = FALSE;
		file->loc.y++;
		file->loc.x = 1;
	}

	if (file->raw) {
		if (*file->raw == 0) {
			file->eof = TRUE;
			return EOF;
		}
		c = *file->raw++;
		if (c == '\r' || c == '\n') {
			if (c == '\r' && *file->raw == '\n')
				file->raw++; // eat CR and NL
			c = '\n';
			file->eol = TRUE;
			return c;
		}
	}
	else if (file->stream) {
		c = (int)getc(file->stream);
		if (c == '\r' || c == '\n') {
			c2 = (int) getc(file->stream);
			if (c != '\r' || c2 != '\n')
				ungetc(c2, file->stream);
			c = '\n';
			file->eol = TRUE;
			file->lineseek = ftell(file->stream);
			return c;
		}
	}
	else
		Fatal("*** BUGCHECK: FileGet: internal error");

	file->loc.x++;
	return c;
}

PRIVATE int _FileGet(int flag) {

	PSOURCE file;
	int     c;
	int     c2;

	file = FileGetCurrent();
	if (!file)
		return EOF;

	while (TRUE) {

		_coord = file->loc;

		c = _FileGetc(flag);

		if (c == EOF || c == '\0') {

			file->eof = TRUE;

			if (VectorLength(_files) > 1) {
				FileClose(file);
				VectorPop(_files, NULL);
				file = FileGetCurrent();
				continue;
			}

			return EOF;
		}
		/* line continue character. */
		if (c == '\\') {
			c2 = FilePeek();
			if (c2 == '\n') {
				FileGet();
				_coord = file->loc;
				c = FileGet();
				return c;
			}
		}
		return c;
	}
}

/* return coordinate. */
PUBLIC PCOORD FileCoord(void) {

	return &_coord;
}

/* peek at next character from file. */
PUBLIC int FilePeek(void) {

	PSOURCE file;
	int     c;

	file = FileGetCurrent();

	assert(file != NULL);

	if (file->eof) {
		if (VectorLength(_files) > 1)
			return '\n';
		return EOF;
	}
	c = _FileGet(PEEK);
	if (c == EOF) {
		if (VectorLength(_files) > 1)
			return '\n';
	}
	if (c != EOF)
		FileUnget(c);
	return c;
}

/* unget last character from file. */
PUBLIC int FileUnget(IN int c) {

	PSOURCE file;

	file = FileGetCurrent();

	assert(file != NULL);

	if (!file->pushback)
		file->pushback = NewBuffer();
	BufferPush(file->pushback, c);
	file->eof = FALSE;
	return c;
}

/* get character from file. */
PUBLIC int FileGet(void) {

	PSOURCE file;

	file = FileGetCurrent();
	if (!file)
		return EOF;

	return _FileGet(GET);
}

/* opens file. */
PUBLIC PSOURCE FileOpen(IN PSTRING filename) {

	PSOURCE file;
	FILE* stream;

	stream = (FILE*)fopen(filename->value, "r");
	if (!stream)
		return NULL;

	file = malloc(sizeof(SOURCE));
	if (!file) {
		fclose(stream);
		return NULL;
	}

	file->lineseek = 0;
	file->raw = NULL;
	file->loc.fname = filename;
	file->eof = FALSE;
	fseek(stream, 0, SEEK_END);
	fseek(stream, 0, SEEK_SET);
	file->stream = stream;
	file->pushback = NULL;
	file->eol = TRUE;
	file->loc.y = 0;
	file->loc.x = 0;

	FilePush(file);
	return file;
}

PUBLIC PSOURCE FileOpenRaw(IN char* filename, IN char* data) {

	PSOURCE file;

	file = malloc(sizeof(SOURCE));
	if (!file)
		return NULL;

	file->raw = data;
	file->loc.fname = InternString(filename);
	file->eof = FALSE;
	file->stream = NULL;
	file->pushback = NULL;
	file->eol = TRUE;
	file->loc.y = 0;
	file->loc.x = 0;

	FilePush(file);
	return file;
}

PUBLIC void FileGetLine(IN char* buf, IN int max) {

	int        pos;
	IN PSOURCE src;

	src = FileGetCurrent();
	if (!src->stream)
		return;

	pos = ftell(src->stream);
	fseek(src->stream, src->lineseek, SEEK_SET);
	fgets(buf, max, src->stream);
	fseek(src->stream, pos, SEEK_SET);
}
