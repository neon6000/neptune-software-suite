/************************************************************************
*
*	obj.c - PE/COFF Object File Support.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/*
	This implements the Portable Executable COFF target which is responsible for
	back-end section and symbol management, performing relocations, and writing
	the section data to a flat binary file. It may also define target-specific macros.
*/

#include <time.h>
#include <assert.h>
#include <string.h>
#include <malloc.h>
#include "nasm.h"
#include "pecoff.h"

char* _filename;

typedef struct Relocation {
	uint32_t virtualAddress;
	uint32_t symbolTableIndex;
	uint16_t type;
}Relocation;
typedef List RelocationList;

typedef struct SymbolOffset {
	unsigned long zero;
	unsigned long offset;
}SymbolOffset;

typedef struct Symbol {
	union {
		char shortname[8];
		SymbolOffset e;
	}u;
	int32_t  value;
	int16_t  section;
	int16_t  type;
	int8_t   storageClass;
	int8_t   numAuxSymbols;
	// section->symindex
	index_t  index;
	// unique segment ID - NextSegmentIndex()
	unsigned int seg;
}Symbol;

typedef struct SymbolAuxFunction {
	uint32_t tagIndex;
	uint32_t totalSize;
	uint32_t pointerToLineNumber;
	uint32_t pointerToNextFunction;
	uint16_t unused;
}SymbolAuxFunction;

typedef struct SymbolAuxBF_EF {
	uint32_t  unused;
	uint16_t  lineNumber;
	uint8_t   unused2[6];
	uint32_t  pointerToNextFunction; // SymbolAuxBF only.
	uint16_t  unused3;
}SymbolAuxBF, SymbolAuxEF;

typedef struct SymbolAuxWeakExternal {
	uint32_t  tagIndex;
	uint32_t  characteristics;
	uint8_t   unused[10];
}SymbolAuxWeakExternal;

typedef struct SymbolAuxFile {
	char      fileName[18];
}SymbolAuxFile;

typedef struct SymbolAuxSectionDef {
	uint32_t  length;
	uint16_t  numberOfRelocations;
	uint16_t  numberOfLineNumbers;
	uint32_t  checksum;
	uint16_t  number;  // one based index into section table. Used when COMDAT selection = 5.
	uint8_t   selection; // COMDAT selection number.
	uint8_t   unused[3];
}SymbolAuxSectionDef;

typedef List SymbolList;

typedef struct Section {
	char           magic[8];
	char           name[9];
	int            index;
	unsigned long  offset;
	PBUFFER        data;
	RelocationList relocs;
	int32_t        flags;
	int32_t        pos;
	int32_t        relpos;
	int            symindex;
	unsigned long  seg;
}Section;
typedef List SectionList;

typedef struct CoffStringTableHeader {
	uint32_t totalSize;
}CoffStringTableHeader;

/* default section flags. */
#define TEXT_FLAGS  (IMAGE_SCN_CNT_CODE|IMAGE_SCN_ALIGN_16BYTES|IMAGE_SCN_MEM_EXECUTE|IMAGE_SCN_MEM_READ)
#define DATA_FLAGS  (IMAGE_SCN_CNT_INITIALIZED_DATA|IMAGE_SCN_ALIGN_4BYTES|IMAGE_SCN_MEM_READ|IMAGE_SCN_MEM_WRITE)
#define BSS_FLAGS   (IMAGE_SCN_CNT_UNINITIALIZED_DATA|IMAGE_SCN_ALIGN_4BYTES|IMAGE_SCN_MEM_READ|IMAGE_SCN_MEM_WRITE)
#define RDATA_FLAGS (IMAGE_SCN_CNT_INITIALIZED_DATA|IMAGE_SCN_ALIGN_8BYTES|IMAGE_SCN_MEM_READ)
#define PDATA_FLAGS (IMAGE_SCN_CNT_INITIALIZED_DATA|IMAGE_SCN_ALIGN_4BYTES|IMAGE_SCN_MEM_READ)
#define XDATA_FLAGS (IMAGE_SCN_CNT_INITIALIZED_DATA|IMAGE_SCN_ALIGN_8BYTES|IMAGE_SCN_MEM_READ)
#define INFO_FLAGS  (IMAGE_SCN_ALIGN_1BYTES|IMAGE_SCN_LNK_INFO|IMAGE_SCN_LNK_REMOVE)


/* back end ------------------------- */


SectionList    _sections;
SymbolList     _symbols;
PBUFFER        _stringTable;
Section*       _currentSection;

/**
*	NewSymbol
*
*	Description : Allocates a new symbol
*	and adds it to the symbol table
*
*	Output : Symbol
*/
Symbol* NewSymbol(void) {

	static _symIndex = 0;
	Symbol* symbol;

	symbol = (Symbol*)malloc(sizeof(Symbol));

	memset(symbol, 0, sizeof(Symbol));
	symbol->index = _symIndex++;

	ListAdd(&_symbols, symbol);
	return symbol;
}

/**
*	NewFileSymbol
*
*	Description : The file symbol consists of
*	an initial .file symbol and an auxiliary file
*	name symbol. This creates both symbols.
*
*	Input : filename - Filename
*
*	Output : Pointer to .file Symbol
*/
Symbol* NewFileSymbol(PSTRING filename) {

	SymbolAuxFile* file;
	Symbol*        symbol;
	char           filepad[18];

	memcpy(filepad, filename->value, 18);
	filepad[17] = '\0';

	symbol = NewSymbol();
	file = (SymbolAuxFile*)NewSymbol();

	strcpy(symbol->u.shortname, ".file");
	symbol->storageClass = IMAGE_SYM_CLASS_FILE;
	symbol->numAuxSymbols = 1;

	strcpy(file->fileName, filepad);
	return symbol;
}

/**
*	NewSectionSymbol
*
*	Description : The section symbol consists of
*	an initial section symbol and an auxiliary section
*	symbol.
*
*	Input : section - Section object to add
*
*	Output : Pointer to section symbol
*/
Symbol* NewSectionSymbol(IN Section* section){

	Symbol*              symbol;
	SymbolAuxSectionDef* sectionDef;
	char                 sectionPadded[18];

	memcpy(sectionPadded, section->name, 18);
	sectionPadded[17] = '\0';

	symbol = NewSymbol();
	sectionDef = (SymbolAuxSectionDef*)NewSymbol();

	strcpy(symbol->u.shortname, sectionPadded);
	symbol->section = section->index;
	symbol->storageClass = IMAGE_SYM_CLASS_STATIC;
	symbol->numAuxSymbols = 1;

	sectionDef->length = GetBufferLength(section->data);
	sectionDef->numberOfRelocations = ListSize(&section->relocs);

	section->symindex = symbol->index;
	return symbol;
}

Symbol* NewAbsolutSymbol(void) {

	Symbol* s = NewSymbol();
	strcpy(s->u.shortname, ".absolut");
	s->section = IMAGE_SYM_ABSOLUTE;
	s->seg = s->section;
	s->storageClass = IMAGE_SYM_CLASS_STATIC;
	return s;
}

PRIVATE Relocation* NewRelocation(IN Section* section) {

	Relocation* reloc = (Relocation*)malloc(sizeof(Relocation));
	memset(reloc, 0, sizeof(Relocation));
	ListAdd(&section->relocs, reloc);
	return reloc;
}

PRIVATE offset_t AddString(IN char* s) {

	offset_t r = GetBufferLength(_stringTable);
	BufferAppend(_stringTable, s, strlen(s) + 1);
	/* the string table starts with CoffStringTableHeader,
	so we adjust for it here. */
	return r + sizeof(CoffStringTableHeader);
}

PRIVATE Section* GetSectionByIndex(IN int seg) {

	Section* s;
	ListNode* current;

	for (current = _sections.first; current; current = current->next) {
		s = (Section*)current->data;
		if (s->index == seg)
			return s;
	}
	return NULL;
}

PRIVATE Section* GetSectionBySeg(IN int seg) {

	Section* s;
	ListNode* current;

	for (current = _sections.first; current; current = current->next) {
		s = (Section*)current->data;
		if (s->seg == seg)
			return s;
	}
	return NULL;
}

PRIVATE Symbol* GetSymbolBySeg(IN int seg) {

	Symbol* s;
	ListNode* current;

	for (current = _symbols.first; current; current = current->next) {
		s = (Symbol*)current->data;
		if (s->seg == seg)
			return s;
	}
	return NULL;
}

PRIVATE Section* GetSectionByName(IN char* name) {

	Section* s;
	ListNode* current;

	for (current = _sections.first; current; current = current->next) {
		s = (Section*)current->data;
		if (!strcmp(s->name, name))
			return s;
	}
	return NULL;
}

PUBLIC int NextSectionIndex(void) {

	static int index = 1;
	return index++;
}

PRIVATE Section* NewSection(IN char* name, IN uint32_t flags) {

	Section* s;

	s = malloc(sizeof(Section));
	memset(s, 0, sizeof(Section));
	if (flags != BSS_FLAGS) {
		s->data = NewBuffer();
	}
	s->index = NextSectionIndex();
	strncpy(s->name, name, 8);
	s->name[8] = '\0';
	s->flags = flags;

	ListAdd(&_sections, s);
	return s;
}

PRIVATE void SectionWrite(IN Section* s, IN uint8_t* data, IN uint32_t length) {

	BufferAppend(s->data, data, length);
}

PRIVATE void AddRelocation(IN Section* section, IN long segto, IN int16_t type) {

	Relocation* reloc;
	Section* sect = GetSectionBySeg(segto);
	Symbol* sym = GetSymbolBySeg(segto);

	// relocations have 3 fields: symbol being relocated (each symbol has a unique
	// seg field set up by label.c), and the kind and offset of the relocation.
	// the relocation is added to the current section

	reloc = NewRelocation(section);
	reloc->type = type;
	reloc->virtualAddress = GetBufferLength(section->data);

	if (sym)
		reloc->symbolTableIndex = sym->index;
	else if (sect)
		reloc->symbolTableIndex = sect->symindex;
	else/* absolute */
		reloc->symbolTableIndex = 0;

}

PRIVATE Symbol* FindSymbol(IN char* name);

/**
*   AddSymbol
*
*   Description : Adds or Updates a Symbol to the Object Symbol Table. Segment is a unique
*   index from NextSegmentIndex or is EXTERN or ABSOLUTE. This uniquely identifies the Symbol.
*   The Symbol is always created in the Current Section with the value of "offset".
*
*   Input : name - Name of the symbol
*           segment - Unique ID for Section or Symbol
*           offset - Offset (Value) of symbol
*           flags - Symbol flags
*/
PRIVATE void AddSymbol(IN PSTRING name, IN handle_t segment, IN offset_t offset, IN SymbolFlags flags) {

	Symbol*  s;
	offset_t off;

	off = (offset_t)-1;

	s = FindSymbol(name->value);
	if (!s) {
		s = NewSymbol();
		if (strlen(name->value) > 8) {
			off = AddString(name->value);
			s->u.e.zero = 0;
			s->u.e.offset = off;
		}
		else{
			strcpy(s->u.shortname, name->value);
		}
	}

	s->value = offset;
	s->type = 0;

	if (flags & SYM_EXTERN || flags & SYM_GLOBAL)
		s->storageClass = IMAGE_SYM_CLASS_EXTERNAL;
	else
		s->storageClass = IMAGE_SYM_CLASS_STATIC;

	if (segment == ABS_SEG)
		s->section = 0xffff;
	else if (flags & SYM_EXTERN)
		s->section = 0;
	else
		s->section = _currentSection->index;

	s->seg = segment;
}

PRIVATE ListNode* GetSymbolByName(IN char* name) {

	ListNode* node;
	for (node = _symbols.first; node; node = node->next) {

		Symbol* s = (Symbol*)node->data;
		char* realname;
		int numAuxSymbols;
		int maxlen;

		if (s->u.e.zero == 0) {
			realname = &_stringTable->data[s->u.e.offset - sizeof(CoffStringTableHeader)];
			maxlen = strlen(realname);
		}
		else {
			realname = s->u.shortname; /// NOT 0 terminated!!
			maxlen = 8;
		}

		if (strnlen(realname, maxlen) == strnlen(name, maxlen) && strncmp(realname, name, maxlen) == 0)
			return node;

		numAuxSymbols = s->numAuxSymbols;
		while (numAuxSymbols-- > 0)
			node = node->next;
	}

	return NULL;
}

PRIVATE offset_t GetSectionOffset(IN PASS pass) {

	if (pass == PASS0)
		return _currentSection->offset;
	return GetBufferLength(_currentSection->data);
}

PRIVATE Symbol* FindSymbol(IN char* name) {

	ListNode* node;
	Symbol* symbol;

	node = GetSymbolByName(name);
	if (!node) return FALSE;
	symbol = node->data;
	return symbol;
}

PRIVATE BOOL GetSymbol(IN char* name, OUT offset_t* off) {

	ListNode* node;
	Symbol* symbol;
	
	node = GetSymbolByName(name);
	if (!node)
		return FALSE;
	symbol = node->data;
	if (off) *off = symbol->value;
	return TRUE;
}


/* writes to file ------------------- */


PRIVATE void WriteStringTable(IN FILE* file) {

	CoffStringTableHeader sth;

	/*
		PE/COFF Spec 5.6 -- At the beginning of the COFF string table are 4
		bytes containing the total size(in bytes) of the rest of the string
		table. This size includes the size field itself, so that the value
		in this location would be 4 if no strings were present.
	*/
	sth.totalSize = GetBufferLength(_stringTable) + sizeof(CoffStringTableHeader);

	fwrite(&sth, sizeof(CoffStringTableHeader), 1, file);
	fwrite(GetBufferData(_stringTable), GetBufferLength(_stringTable), 1, file);
}

PRIVATE void WriteSymbol(IN FILE* file, IN Symbol* s) {

	fwrite(s->u.shortname, 8, 1, file);
	fwrite(&s->value, sizeof(int32_t), 1, file);
	fwrite(&s->section, sizeof(int16_t), 1, file);
	fwrite(&s->type, sizeof(int16_t), 1, file);
	fwrite(&s->storageClass, 1, 1, file);
	fwrite(&s->numAuxSymbols, 1, 1, file);
}

PRIVATE void WriteSymbols(IN FILE* file) {

	ListNode* node;
	for (node = _symbols.first; node; node = node->next) {
		Symbol* s = (Symbol*)node->data;
		WriteSymbol(file, s);
	}
}

PRIVATE uint32_t WriteSectionHeader(IN FILE* file, IN Section* s) {

	CoffSectionHeader sh;

	memset(&sh, 0, sizeof(CoffSectionHeader));

	memcpy(sh.name, s->name, 8);

	sh.pointerToRawData = s->pos;
	sh.pointerToRelocations = s->relpos;

	sh.sizeOfRawData = GetBufferLength (s->data);
	sh.numberOfRelocations = ListSize(&s->relocs);
	sh.characteristics = s->flags;
	
	fwrite(&sh, sizeof(CoffSectionHeader), 1, file);

	return sh.sizeOfRawData;
}

PRIVATE void WriteFileHeader(FILE* file, IN uint32_t symbolTablePointer) {

	CoffFileHeader fh;

	memset(&fh, 0, sizeof(CoffFileHeader));
	fh.machine = IMAGE_FILE_MACHINE_I386;
	fh.numberOfSections = ListSize(&_sections);
	fh.timeDateStamp = (uint32_t) time(NULL);
	fh.pointerToSymbolTable = symbolTablePointer;
	fh.numberOfSymbols = ListSize(&_symbols);
	fh.sizeOfOptionalHeader = 0;
	fh.characteristics = 0x104;

	fwrite(&fh, sizeof(CoffFileHeader), 1, file);
}

PRIVATE void WriteSectionHeaders(IN FILE* file) {

	ListNode* node;
	for (node = _sections.first; node; node = node->next) {
		Section* k = (Section*)node->data;
		WriteSectionHeader(file, k);
	}
}

PRIVATE void WriteRelocs(IN FILE* file, IN Section* s) {

	ListNode* node;
	for (node = s->relocs.first; node; node = node->next) {
		Relocation* r = (Relocation*)node->data;
		fwrite(r, sizeof(Relocation), 1, file);
	}
}

PRIVATE void WriteSections(IN FILE* file) {

	ListNode* node;
	for (node = _sections.first; node; node = node->next) {
		Section* k = (Section*)node->data;
		fwrite(GetBufferData(k->data), GetBufferLength(k->data), 1, file);
		WriteRelocs(file, k);
	}
}

PRIVATE offset_t GetSymbolTableOffset(void) {

	offset_t offset;
	ListNode* node;

	/*
		the symbol table is right after the section data. So
		file header + (section header * numberOfSections) + (length_of_each_section)
	*/
	offset = sizeof(CoffFileHeader)+(sizeof(CoffSectionHeader) * ListSize(&_sections));
	for (node = _sections.first; node; node = node->next) {
		Section* s = (Section*)node->data;
		if (s->data)
			offset += GetBufferLength(s->data);
		offset += ListSize(&s->relocs) * sizeof(Relocation);
	}
	return offset;
}

PRIVATE void UpdateSectionSymbol(IN Section* section) {

	ListNode* node;
	Symbol* symbol;
	SymbolAuxSectionDef* def;

	node = GetSymbolByName(section->name);
	symbol = (Symbol*) node->data;
	def = (SymbolAuxSectionDef*)node->next->data;

	def->length = GetBufferLength(section->data);
	def->numberOfRelocations = ListSize(&section->relocs);
	symbol->section = section->index;
}

PRIVATE void UpdateSectionSymbols(void) {

	ListNode* section;
	for (section = _sections.first; section; section = section->next) {
		Section* s = section->data;
		if (s)
			UpdateSectionSymbol(s);
	}
}

PRIVATE void UpdateSectionOffsets(void) {

	offset_t offset;
	ListNode* node;

	/* the sections starts here. */
	offset = sizeof(CoffFileHeader)+(sizeof(CoffSectionHeader)* ListSize(&_sections));

	/* go through each section and update the section record
	to point to the correct file offset. */
	for (node = _sections.first; node; node = node->next) {

		Section* s = (Section*)node->data;
		s->pos = 0;
		s->relpos = 0;

		if (s->data) {

			/* adjust the segment file position. */
			s->pos = offset;
			offset += GetBufferLength(s->data);

			/* if there is more then one relocation, then
			adjust the relpos to point to the relocation table
			right after the section data: */
			if (s->relocs.first)
				s->relpos = offset;
		}
		offset += ListSize(&s->relocs) * sizeof(Relocation);
	}
	return;
}

PRIVATE void WriteFile (void) {

	FILE* out;
	offset_t symtable;

	out = fopen(_filename, "wb");

	UpdateSectionOffsets();
	UpdateSectionSymbols();

	symtable = GetSymbolTableOffset();

	WriteFileHeader(out, symtable);
	WriteSectionHeaders(out);
	WriteSections(out);
	WriteSymbols(out);
	WriteStringTable(out);

	fclose(out);
}

PRIVATE void Initialize(IN char* filename, IN PSTRING src) {

	/* initialize tables. */

	_filename = filename;
	ListInitialize(&_sections, FALSE);
	ListInitialize(&_symbols, FALSE);
	_stringTable = NewBuffer();

	_currentSection = NewSection(".text", TEXT_FLAGS);
	NewAbsolutSymbol();
	NewFileSymbol(src);
	NewSectionSymbol(_currentSection);
	_currentSection->seg = NextSegmentIndex();
}


PRIVATE void FreeReloc(IN ListNode* node) {

	Relocation* r;

	r = node->data;
	if (r)
		free(r);
}

PRIVATE void FreeSection(IN ListNode* node) {

	Section* s;

	s = node->data;
	if (s) {
		BufferFree(s->data);
		ListFree(&s->relocs, FreeReloc);
		free(s);
	}
}

PRIVATE void FreeSymbol(IN ListNode* node) {

	Symbol* s;

	s = node->data;
	if (s)
		free(s);
}

PRIVATE void Cleanup(void) {

	WriteFile();

	ListFree(&_sections, FreeSection);
	ListFree(&_symbols, FreeSymbol);
	BufferFree(_stringTable);
}


/* front end ------------------------ */

/**
*	Write
*
*	Description : Write data to the sepecified section. When type is ADDRESS or
*		REL, a relocation will be created in "seg" referencing "segto".
*
*	Input : seg - segment ID to write to
*			data - Data to write
*			type - RAWDATA, ADDRESS, or REL types
*			size - Number of bytes to write
*			segto - If type is ADDRESS or REL types, this is the segment being references
*			pass - In PASS0, this just adds to the section offset for code generator labels.
*				in PASSGEN, this will write the bytes.
*/
PRIVATE void Write(IN handle_t seg, IN void* data, IN output_t type, size_t size, IN int segto, IN PASS pass) {

	Section* section;
	uint8_t  zero[4];

	if (seg == ABS_SEG)
		return;

	memset(zero, 0, 4);
	section = GetSectionBySeg(seg);

	if (pass == PASS0) {
		section->offset += size;
		return;
	}

	switch (type) {
	case WRITE_RESERVE:
	case WRITE_RAWDATA:
		SectionWrite(section, data, size);
		break;
	case WRITE_ADDRESS: {
		if (size != 4)
			Error("PE/COFF does not support non-32 bit relocations");
		else
			AddRelocation(section, segto, IMAGE_REL_I386_DIR32);

		//
		// For IMAGE_REL_I386_DIR32... this only occurs on a Symbol Table item.
		// The Symbol already stores the location of itself w.r.t. its section.
		// The linker adds this to whatever value is already in the section.
		// To avoid double-adding, keep this 0.
		//

		SectionWrite(section, zero, 4);
		break;
	}
	case WRITE_REL1ADR:
		Error("PE/COFF does not support non-32 bit relative relocations");
		SectionWrite(section, data, 1); /* write something */
		break;
	case WRITE_REL2ADR:
		Error("PE/COFF does not support non-32 bit relative relocations");
		SectionWrite(section, data, 2); /* write something */
		break;
	case WRITE_REL4ADR:

		AddRelocation(section, segto, IMAGE_REL_I386_REL32);

		//
		// For IMAGE_REL_I386_REL32... this only occurs on a Symbol Table item.
		// The Symbol already stores the location of itself w.r.t. its section.
		// The linker adds this to whatever value is already in the section.
		// To avoid double-adding, keep this 0.
		//

		SectionWrite(section, zero, 4);
		break;
	};
}

PRIVATE handle_t SetSection(IN char* name, IN PASS pass, IN BITS mode) {

	Section* k;

	if (pass == PASS0) {
		/* .text is always present, so don't recreate. */
		if (strcmp(name, ".text")) {
			k = NewSection(name, 0);
			NewSectionSymbol(k);
			k->seg = NextSegmentIndex();
		}
	}

	k = GetSectionByName(name);
	if (!k)
		return INVALID_HANDLE;
	_currentSection = k;
	return k->seg;
}

PRIVATE handle_t GetSection(IN OPTIONAL char* name) {

	Section* k;

	if (!name) k = _currentSection;
	else k = GetSectionByName(name);

	if (!k)
		return INVALID_HANDLE;
	return (handle_t)k->seg;
}

PRIVATE handle_t CreateSection(IN char* name, IN int flags) {

	handle_t handle;
	Section* section;

	handle = GetSection(name);
	if (handle  != INVALID_HANDLE)
		return handle;

	section = NewSection(name, flags);
	NewSectionSymbol(section);
	section->seg = NextSegmentIndex();
	return section->seg;
}

/**
*	ProcessDirective
*
*	Description : Process target specific directives.
*
*	Input : name - Directive name
*	        scn - Scanner
*
*	Output : FALSE if error, TRUE if success
*/
PRIVATE BOOL ProcessDirective(IN PSTRING name, IN PLEX scn, IN OUT PTOKEN token, IN PASS pass) {

	return FALSE;
}

PRIVATE void StdMac(void) {

}

TARGET _coffOutputFormat = {
	"coff",
	"coff/pe object file",
	Initialize,
	Cleanup,
	AddSymbol,
	GetSectionOffset,
	Write,
	CreateSection,
	SetSection,
	GetSection,
	ProcessDirective,
	StdMac
};
