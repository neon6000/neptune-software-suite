/************************************************************************
*
*	expr.c - Expression evaluator
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/*
	The expression evaluator simplifies a PEXPR array into its
	most simplest form. A PEXPR array is a summation of its individual
	parts. For example, eax + ebx + 10 becomes:

	EXPR[0] = REG | EAX
	EXPR[1] = REG | EBX
	EXPR[2] = NUM | 10

	Segments are stored within each EXPR "kind" field where applicable. This
	field also stores control information such as "Forward Reference" indicators.

	The evaluator also processes the $ and $$ special tokens. Labels are looked
	up and their value is taken if possible.

	Foward references are marked on Labels in PASS0 and are expected to have been handled
	in PASS1. The evaluator shall throw an error in PASS1 if the indicator is still set.

	COFF Targets have a feature that will "auto-EXTERN" symbols that are not defined
	in the file. This logic is handled in label.c, however the expression evaluator must
	call to define the label as EXTERN in these cases and let label.c perform the final
	determination.

	A "Simple" expression is a PEXPR array with one item. A "Vector" expression is a
	PEXPR array with more than one item.
*/

#include <malloc.h>
#include <assert.h>
#include <string.h>
#include "nasm.h"

// gen.c
extern unsigned long _offset;
extern handle_t      _segment;
extern PASS          _pass;

PRIVATE TOKEN        tk;

PRIVATE PEXPR expr(IN PLEX);

PRIVATE PEXPR Scalar(IN int kind, IN long val) {

	PEXPR e = malloc(sizeof(EXPR)*2);
	strcmp(e[0].magic, "EXPR");
	e[0].kind = kind;
	e[0].val = val;
	e[1].kind = EXPR_END;
	e[1].val = 0;
	return e;
}

PUBLIC BOOL IsNumber(IN PEXPR e) {

	return (e[0].kind & EXPR_NUM) == EXPR_NUM && (e[1].kind == EXPR_END);
}

PUBLIC BOOL IsScalar(IN PEXPR e) {

	return e[1].kind == EXPR_END;
}

PUBLIC BOOL IsVector(IN PEXPR e) {

	return !IsScalar(e);
}

PUBLIC BOOL IsSimple(IN PEXPR e) {

	return !IsVector(e);
}

PRIVATE BOOL IsRegister(IN PEXPR e) {

	return e[0].kind & EXPR_REG;
}

PUBLIC BOOL IsReloc(IN PEXPR e) {

	return (e->kind & EXPR_FWREF) || (e->kind & 0xffff)
		|| (e->kind & 0xffff != ABS_SEG) != 0;
}

PUBLIC BOOL Reloc(IN PEXPR e) {

	if (!IsReloc(e)) return FALSE;
	return e->kind & 0xffff;
}

PRIVATE PEXPR Unary(IN PEXPR l, IN int op) {

	PEXPR e;
	long  val;

	if (!l) return NULL;
	if (!IsNumber(l)) {
		Error("Expr: non-numeric on unary operator");
		return NULL;
	}
	switch (op) {
	case TokenPlus: val = l[0].val; break;
	case TokenMinus: val = -1 * l[0].val; break;
	default: val = 0;
	}
	e = Scalar(l[0].kind, l[0].val);
	free(l);
	return e;
}

PRIVATE PEXPR ScaleRegister(IN PEXPR r, IN PEXPR scale) {

	r[0].val = scale->val;
	free(scale);
	return r;
}

PRIVATE size_t Size(IN PEXPR s) {

	size_t i = 0;
	for (; s->kind != EXPR_END; s++)
		i++;
	return i;
}

PRIVATE PEXPR AppendOne(IN PEXPR l, IN PEXPR r) {

	size_t sl;

	if (!l)
		sl = 0;
	else
		sl = Size(l);

	l = realloc(l, (sl + 1) * sizeof(EXPR));
	memcpy(l + sl, r, sizeof(EXPR));
}

PRIVATE PEXPR Append(IN PEXPR l, IN PEXPR r) {

	size_t sl;
	size_t sr;

	if (!l)
		sl = 0;
	else
		sl = Size(l);
	sr = Size(r);

	l = realloc(l, (sl + sr + 1) * sizeof(EXPR));
	memcpy(l + sl, r, (sr + 1) * sizeof(EXPR));
	free(r);
	return l;
}

PRIVATE PEXPR Negate(IN PEXPR l) {

	PEXPR e;
	if (!IsNumber(l)) Fatal("Expr: negate non-numeric");
	e = Scalar(l[0].kind, -1 * l[0].val);
	free(l);
	return e;
}

PRIVATE PEXPR VectorBinary(IN PEXPR l, IN PEXPR r, IN int op) {

	switch (op) {
	case TokenMinus:
//		if (!(IsRegister(l) && IsNumber(r)))
//			Error("Expr: invalid combination");
		r = Negate(r);
		return Append(l, r);
	case TokenPlus:
		return Append(l, r);
	default:
		Error("Expr: operator not allowed on vectors");
		return r;
	};
}

PRIVATE PEXPR Binary (IN PEXPR l, IN PEXPR r, IN int op) {

	PEXPR e;
	long  val;

	if (l == NULL || r == NULL) return NULL;

	if (op == TokenStar && IsRegister(l) && IsNumber(r))
		return ScaleRegister(l, r);
	else if (op == TokenStar && IsRegister(r) && IsNumber(l))
		return ScaleRegister(r, l);

	if (IsNumber(l) == FALSE || IsNumber(r) == FALSE)
		return VectorBinary(l, r, op);

	if (EXPR_SEG(l->kind) != EXPR_SEG(r->kind))
		return VectorBinary(l, r, op);

	if (!(IsNumber(l) && IsNumber(r))) {
		Fatal("*** BUGCHECK: Expr: non-numeric on binary operator");
		return NULL;
	}
	switch (op) {
	case TokenStar: val = l[0].val * r[0].val; break;
	case TokenPlus: val = l[0].val + r[0].val; break;
	case TokenMinus: val = l[0].val - r[0].val; break;
	case TokenLeftShift: val = l[0].val << r[0].val; break;
	case TokenRightShift: val = l[0].val >> r[0].val; break;
	case TokenBitAnd: val = l[0].val & r[0].val; break;
	case TokenBitOr: val = l[0].val | r[0].val; break;
	case TokenXor: val = l[0].val ^ r[0].val; break;
	case TokenFowardSlash: {
		if (!r[0].val) {
			Error("Expr: divide by 0");
			val = 0;
		}else
			val = l[0].val / r[0].val;
	} break;
	default: val = 0;
	}
	e = Scalar(EXPR_NUM, val);
//	e = Scalar(l[0].kind | l[1].kind, val);
	free(l);
	free(r);
	return e;
}

//expr6: {+,-} expr6 | identifier | $ | $$ | number | register
PRIVATE PEXPR expr6(IN PLEX scn) {

	int   op;
	int   seg;
	int   offset;
	PEXPR e;

	seg = offset = 0;
	e = NULL;

	if (tk.type == TokenPlus || tk.type == TokenMinus) {
		op = tk.type;
		scn(&tk);
		e = Unary(expr6(scn), op);
	}
	else if (tk.type == TokenLeftParen) {
		scn(&tk);
		e = expr(scn);
		if (tk.type != TokenRightParen)
			Fatal("Expr: expected ')'");
		scn(&tk);
	}
	else if (tk.type == TokenHere) {
		scn(&tk);
		e = Scalar(EXPR_NUM, _offset);
	}
	else if (tk.type == TokenSegBase) {
		scn(&tk);
		e = Scalar(EXPR_NUM, _segment);
	}
	else if (tk.type == TokenIdentifier) {

		//
		// If the label has been defined, get its Segment.
		// If this is not PASS0 however, then its an error.
		//

		if (!LabelGet(tk.value.s->value, &seg, &offset)) {
			seg = FWREF_SEG;
			if (_pass > PASS0)
				Error("Expr: undefined label %t", tk);
		}

		//
		// This symbol is not yet defined. If the TARGET is COFF,
		// define the label automatically as EXTERN.
		//

		if (!strcmp(GetOutputFormat()->name, "coff"))
			LabelDefineExtern(tk.value.s, TRUE);

		e = Scalar(EXPR_NUM + seg, offset);
		scn(&tk);
	}
	else if (tk.type == TokenInteger) {
		e = Scalar(EXPR_NUM, tk.value.inum);
		scn(&tk);
	}
	else if (tk.type == TokenRegister) {
		e = Scalar(EXPR_REG + tk.value.reg, 0);
		scn(&tk);
	}
	return e;
}

//expr5 : expr6 [ {*,/} expr6...]
PRIVATE PEXPR expr5(IN PLEX scn) {

	int   op;
	PEXPR l;

	l = expr6(scn);
	while (tk.type == TokenStar || tk.type == TokenFowardSlash) {
		op = tk.type;
		scn(&tk);
		l = Binary(l, expr6(scn), op);
	}
	return l;
}

//expr4 : expr5 [ {+,-} expr5...]
PRIVATE PEXPR expr4(IN PLEX scn) {

	int op;
	PEXPR l;

	l = expr5(scn);
	while (tk.type == TokenPlus || tk.type == TokenMinus) {
		op = tk.type;
		scn(&tk);
		l = Binary(l, expr5(scn), op);
	}
	return l;
}

//expr3 : expr4 [ {<<,>>} expr4...]
PRIVATE PEXPR expr3(IN PLEX scn) {

	int op;
	PEXPR l;

	l = expr4(scn);
	while (tk.type == TokenLeftShift || tk.type == TokenRightShift) {
		op = tk.type;
		scn(&tk);
		l = Binary(l, expr4(scn), op);
	}
	return l;
}

PRIVATE PEXPR expr2(IN PLEX scn) {

	int op;
	PEXPR l;

	l = expr3(scn);
	while (tk.type == TokenBitAnd) {
		op = tk.type;
		scn(&tk);
		l = Binary(l, expr3(scn), op);
	}
	return l;
}

PRIVATE PEXPR expr1(IN PLEX scn) {

	int op;
	PEXPR l;

	l = expr2(scn);
	while (tk.type == TokenXor) {
		op = tk.type;
		scn(&tk);
		l = Binary(l, expr2(scn), op);
	}
	return l;
}

PRIVATE PEXPR expr0(IN PLEX scn) {

	int op;
	PEXPR l;

	l = expr1(scn);
	while (tk.type == TokenBitOr) {
		op = tk.type;
		scn(&tk);
		l = Binary(l, expr1(scn), op);
	}
	return l;
}

// comparisions
PRIVATE PEXPR expr(IN PLEX scn) {

	return expr0(scn);
}

PUBLIC PEXPR Expr(IN PLEX scn, IN OUT PTOKEN tok) {

	PEXPR e;

	tk = *tok;
	e = expr(scn);
	*tok = tk;
	return e;
}

PUBLIC long Eval(IN PLEX scn, IN OUT PTOKEN tok) {

	PEXPR    e;
	PEXPR    s;
	long     res;

	tk = *tok;
	e = expr(scn);
	*tok = tk;
	res = 0;
	s = e;

	for (; s && s->kind != EXPR_END; s++) {
		if (s->kind & EXPR_NUM) {
			if (EXPR_SEG(s->kind)) {
				Error("Constant expression expected");
				free(e);
				return 0;
			}
			res += s->val;
		}
		else{
			Error("Constant expression expected");
			free(e);
			return 0;
		}
	}

	free(e);
	return res;
}

PUBLIC void BuildInternalInstrOp(IN PEXPR s, IN EXPRHINT hint, OUT InternalInstrOp* out) {

	//
	// This converts PEXPR array into INTERNALINSTROP used by GEN.C
	//

	out->imm = 0;
	out->scale = 1;
	out->segto = NO_SEG;

	if (!s) return;
	for (; s && s->kind != EXPR_END; s++) {
		if (hint == EHINT_SIMPLE && (s->kind & EXPR_NUM) == 0) {
			Error("Actual: non-simple expression, Expected: simple expression");
			break;
		}
		else if (s->kind & EXPR_NUM) {

			out->imm += s->val;

			if (_pass > PASS0 && s->kind & EXPR_FWREF)
				Fatal("Expr: unresolved forward ref on pass 0");

			if (EXPR_SEG(s->kind)) {

				if (out->segto == FWREF_SEG && EXPR_SEG(s->kind) != FWREF_SEG) {
					out->segto = EXPR_SEG(s->kind);
					continue;
				}

				if (out->segto != NO_SEG && EXPR_SEG(s->kind) == FWREF_SEG)
					continue;

				if (out->segto == ABS_SEG && EXPR_SEG(s->kind) != ABS_SEG) {
					out->segto = EXPR_SEG(s->kind);
					continue;
				}

				if (out->segto != NO_SEG && EXPR_SEG(s->kind) == ABS_SEG)
					continue;

				// this causes issues with ISTRUC. i.e.
				// mylabel:          <in previous segment>
				// istruc mystruc    <in absolute segment>
				// ... [mylabel + mystruc.val] ; error

//				if (out->segto != NO_SEG && EXPR_SEG(s->kind) != out->segto)
//					Error("Expr: Inconsistent segments in expression");

				out->segto = s->kind & 0xffff;
			}
		}
		else if (s->kind & EXPR_REG) {
			if (s->val) {
				if (out->index)
					Error("invalid combination of registers");
				else{
					out->index = GetRegister(s->kind & 0xffff);
					out->scale = s->val;
				}
			}
			else if (!out->base)
				out->base = GetRegister(s->kind & 0xffff);
			else if (!out->index)
				out->index = GetRegister(s->kind & 0xffff);
			else Error("invalid combination of registers");
		}
	}
}
