/************************************************************************
*
*	main.c - Entry point.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "nasm.h"
#ifdef _MSC_VER
#include <crtdbg.h>
#endif

// -f bin ../../test.asm -l test.lst -o test.bin
// -f bin "C:\Users\Michael\Desktop\NEPTUNE\neptune-software-suite\private\src\os\boot\nstart\entry.asm" -o "C:\Users\Michael\Desktop\NEPTUNE\nboot\nstart.bin" -l "C:\Users\Michael\Desktop\NEPTUNE\nboot\nstart.lst" -I "C:\Users\Michael\Desktop\NEPTUNE\neptune-software-suite\private\src\os\boot\nstart"
// -f bin "C:\Users\Michael\Desktop\NEPTUNE\neptune-software-suite\private\src\os\boot\bootsect\fat32.asm" -o "C:\Users\Michael\Desktop\NEPTUNE\nboot\fat32.bin" -l "C:\Users\Michael\Desktop\NEPTUNE\nboot\fat32.lst"

PUBLIC int NextSegmentIndex(void) {

	static int index = 1;
	return index++;
}

/* Modules. */
extern LISTING _listingNASM;
extern TARGET  _binOutputFormat;
extern TARGET  _coffOutputFormat;
extern DEBUG   _stabs;
extern DEBUG   _dwarf;

PRIVATE PSOURCE     file;
PRIVATE PTARGET    _of;
PRIVATE PDEBUG     _df;
PRIVATE PLISTING   _listing;
PRIVATE PSTRING    _primarySourceFile;
PRIVATE char*      _outputFileName;
PRIVATE char*      _listingFileName;
PRIVATE PSTRING    _logicalFilename;

/* Standard macros. These are "pre-defined". */
char* _standardMacros =
//
// section <ident>
//
"%macro section 1\n"
"%define _SECT_ [SECTION %1]\n"
"_SECT_\n"
"%endmacro\n"
//
// SECTION <ident>
//
"%macro SECTION 1\n"
"%define _SECT_ [section %1]\n"
"_SECT_\n"
"%endmacro\n"
//
// istruc <ident>
//
"%macro istruc 1\n"
"%push istruc\n"
"%endmacro\n"
//
// at <ident>, <instr>
//
"%macro at 2\n"
"%2\n"
"%endmacro\n"
//
// iend
//
"%macro iend 0\n"
"%pop\n"
"%endmacro\n"
//
// bits <expr>
//
"%macro bits 1\n"
"[bits %1]\n"
"%endmacro\n"
//
// global <ident>
//
"%macro global 1\n"
"[global %1]\n"
"%endmacro\n"
//
// extern <ident>
//
"%macro extern 1\n"
"[extern %1]\n"
"%endmacro\n"
//
// use16
//
"%define use16 [bits 16]\n"
//
// use32
//
"%define use32 [bits 32]\n"
//
// struc
//
"%macro struc 1\n"
"%push struc\n"
"%define _SECT_ [absolute 0]\n"
"_SECT_\n"
"%1:\n"
"%endmacro\n"
//
// endstruc
//
"%macro endstruc\n"
"%pop\n"
"_SECT_\n"
"%endmacro\n"
//
// absolute <ident>
//
"%macro absolute 1\n"
"[absolute %1]\n"
"%endmacro\n"
//
// align
//
"%macro align 1\n"
"[align %1]\n"
"%endmacro\n"
//
// other
//
"%define __NASM_MAJOR__ 0\n"
"%define __NASM_MINOR__ 1\n"
"%define __NASM_SUBMINOR__ 0\n"
"%define __NASM_VER__ \"0.1.0\"\n"
"%define _SECT_ [section .text]\n"
"%define short byte\n"
"%define near word\n"
"%define far dword\n"
"_SECT_\n";

/* Command line flags. */
typedef enum NASMFLAG {
	FLAG_F,
	FLAG_L,
	FLAG_O,
	FLAG_H,
	FLAG_D,
	FLAG_U,
	FLAG_E,
	FLAG_V,
	FLAG_I,
	FLAG_B,
	FLAG_FILE
}NASMFLAG;

/* Command line argument. */
typedef struct NASMPARAM {
	NASMFLAG     flag;
	unsigned int args;
	char*        s;
	char*        desc;
}NASMPARAM, *PNASMPARAM;

NASMPARAM _arguments[] = {

	{ FLAG_F, 1, "-f", "Selects output format. Example: -f bin" },
	{ FLAG_L, 1, "-l", "Selects listing file name. Example: -l test.lst" },
	{ FLAG_O, 1, "-o", "Selects output file name. Example: -o test.obj" },
	{ FLAG_H, 0, "-h", "Displays help information" },
	{ FLAG_D, 1, "-d", "Defines a macro. Example: -d ARCH=I386. Not yet supported" },
	{ FLAG_U, 1, "-u", "Undefine a macro. Example: -u ARCH. Not yet supported" },
	{ FLAG_V, 0, "-v", "Displays help information" },
	{ FLAG_FILE, 1, "-file", "Selects a source file. Example: -file test.asm" },
	{ FLAG_I, 1, "-I", "Add include path" },
	{ FLAG_B, 1, "-b", "Set debug format" },
};

/**
*	GetArgument
*
*	Description : Given the name of a supported program argument,
*	this returns the argument descriptor or NULL if not found.
*
*	Input : s Pointer to string containing the name of the argument.
*
*	Output : Pointer to argument descriptor or NULL.
*/
PRIVATE PNASMPARAM GetArgument(IN char* s) {

	int size = sizeof(_arguments) / sizeof(NASMPARAM);
	for (int i = 0; i < size; i++) {
		if (!strcmp(_arguments[i].s, s))
			return &_arguments[i];
	}
	return NULL;
}

PRIVATE void UndefineMacro(IN char* macro) {

}

PRIVATE void DefineMacro(IN char* macro) {

	//
	// due to FreeMacros each pass, this should
	// probably be treated as predefined strings
	// like stdmac. like turn it into %define or %undef
	//

//	__debugbreak();
}

/**
*	AddIncludePath
*
*	Description : Given a string, add it to the
*	preprocessor include path.
*/
extern void PProcAddIncludePath(IN char* s);
PRIVATE void AddIncludePath(IN char* path) {

	if (path)
		PProcAddIncludePath(path);
}

/**
*	DisplayHelp
*
*	Description : This processes the Help command line switch.
*
*	Output : Dislays help and version information to standard out.
*/
PRIVATE void DisplayHelp(void) {

	int max;
	int i;

	max = sizeof(_arguments) / sizeof (NASMPARAM);

	printf("\n\rNeptune Assembler");
	printf("\n\rCopyright (c) BrokenThorn Entertainment, Co.");
	printf("\n\rOptions:");
	for (i = 0; i < max; i++)
		printf("\n\r\t%s:\t%s", _arguments[i].s, _arguments[i].desc);
	printf("\n\r");
}

/**
*	ProcessCommandLine
*
*	Description : This processes all command line options.
*
*	Input : argc - Number of command line arguments.
*           argv - Array of command line strings.
*
*	Output : TRUE on error, FALSE if success.
*/
PRIVATE BOOL ProcessCommandLine(unsigned IN int argc, IN char* argv[]) {

	unsigned int i;
	char*        s;
	PNASMPARAM   arg;

	if (argc == 1)
		return FALSE;

	for (i = 1; i < argc; i++) {

		arg = GetArgument(argv[i]);

		if (arg) {

			if (argc - i < arg->args) {
				Error("*** %s requires %i arguments, ignoring...", arg->s, arg->args);
				continue;
			}
			s = argv[i + 1];
			switch (arg->flag) {

				/* Set output file format. */
				case FLAG_F:
					if (!strcmp(s, "obj"))
						_of = &_coffOutputFormat;
					else if (!strcmp(s, "bin"))
						_of = &_binOutputFormat;
					else {
						Error("\n\rInvalid output format %s", s);
						return FALSE;
					}
					break;

				/* Set output file name. */
				case FLAG_O:
					_outputFileName = s;
					break;

				/* Set listing file name. */
				case FLAG_L:
					_listingFileName = s;
					break;

				/* Set primary source file name. */
				case FLAG_FILE:
					if (_primarySourceFile)
						Error("\n\rMore then one input file specified, ignoring %s...", s);
					else
						_primarySourceFile = InternString(s);
					break;

				/* Display help and version info. */
				case FLAG_H:
				case FLAG_V:
					DisplayHelp();
					return FALSE;

				/* Define and Undefine Macro */
				case FLAG_D:
					DefineMacro(s);
					break;
				case FLAG_U:
					UndefineMacro(s);
					break;
				case FLAG_I:
					AddIncludePath(s);
					break;

				/* set debug format */
				case FLAG_B:
					if (!strcmp(s, "stabs"))
						_df = &_stabs;
					else if (!strcmp(s, "dwarf"))
						_df = &_dwarf;
					else
						Error("Unknown debug format. Should be stabs or dwarf");
			};

			i += arg->args;
		}
		else {
			if (_primarySourceFile) {
				Error("\n\rMore then one input file specified. Ignoring second file...");
				continue;
			}
			_primarySourceFile = InternString(argv[i]);
		}
	}
	return TRUE;
}

/**
*	GetOutputFormat
*
*	Description : Returns a pointer to the Output Format writer.
*
*	Output : Pointer to the Output Format writer.
*/
PUBLIC PTARGET GetOutputFormat(void) {

	return _of;
}

/**
*	GetDebugFormat
*
*	Description : Returns a pointer to the Debug format support.
*
*	Output : Pointer to the Debug format support.
*/
PUBLIC PDEBUG GetDebugFormat(void) {

	return _df;
}

/**
*	GetListGenerator
*
*	Description : Returns a pointer to the Listing generator.
*
*	Output : Pointer to the Listing generator.
*/
PUBLIC PLISTING GetListGenerator(void) {

	return _listing;
}

/**
*	GetSourceFileName
*
*	Description : Returns the logical file name.
*
*	Output : Pointer to the Listing generator.
*/
PUBLIC PSTRING GetLogicalFileName(void) {

	return _logicalFilename;
}

/**
*	DirectiveBits
*
*	Description : Process BITS n directive
*
*	Input: scn - Scanner function
*
*	Output: FALSE on error
*/
PRIVATE BOOL DirectiveBits(IN PLEX scn) {

	TOKEN tok;

	scn(&tok);
	if (tok.type != TokenInteger) return FALSE;
	return SetBits(tok.value.inum);
}

/**
*	DirectiveAbsolute
*
*	Description : Process ABSOLUTE n directive
*
*	Input: scn - Scanner function
*
*	Output: FALSE on error
*/
PRIVATE BOOL DirectiveAbsolute(IN PLEX scn) {

	TOKEN tok;

	scn(&tok);
	if (tok.type != TokenInteger) return FALSE;
	SetAbsolute(tok.value.inum);
	return TRUE;
}

/**
*	DirectiveDeclare
*
*	Description : Process GLOBAL and EXTERN directive
*
*	Input: scn - Scanner function
*	       extrn - TRUE if EXTERN, FALSE if GLOBAL
*
*	Output: FALSE on error
*/
PRIVATE BOOL DirectiveDeclare(IN PLEX scn, IN BOOL extrn) {

	TOKEN tok;

	scn(&tok);
	if (tok.type != TokenIdentifier) return FALSE;
	if (extrn) SetExtern(tok.value.s);
	else SetGlobal(tok.value.s);
	return TRUE;
}

/**
*	DirectiveSection
*
*	Description : Process SECTION directive
*
*	Input: scn - Scanner function
*	       pass - Current pass
*
*	Output: FALSE on error
*/
PRIVATE BOOL DirectiveSection(IN PLEX scn, IN PASS pass) {

	TOKEN tok;

	scn(&tok);
	if (tok.type != TokenIdentifier) return FALSE;
	SetSegment(tok.value.s, pass);
	return TRUE;
}

/**
*	DirectiveFile
*
*	Description : Process FILE directive
*
*	Input: scn - Scanner function
*	       pass - Current pass
*
*	Output: FALSE on error
*/
PRIVATE BOOL DirectiveFile(IN PLEX scn, IN PASS pass) {

	TOKEN tok;

	scn(&tok);
	if (tok.type != TokenString) return FALSE;
	_logicalFilename = tok.value.s;
	return TRUE;
}

PRIVATE BOOL DirectiveAlign(IN PLEX scn, IN PASS pass) {

	TOKEN tok;

	scn(&tok);
	if (tok.type != TokenInteger) return FALSE;
	SetAlign(tok.value.inum);
	return TRUE;
}

/**
*	Directive
*
*	Description : Process standard directives
*
*	Input : scn: Scanner
*		    tok: Current token: on INPUT, this is always '['
*                on OUTPUT this will replace the current token for the parser
*           pass: Current pass
*
*	Output : TRUE on success, FALSE on error
*/
PUBLIC BOOL Directive(IN PLEX scn, IN OUT PTOKEN out, IN PASS pass) {

	TOKEN name;
	TOKEN tok;
	BOOL  ok;

	ok = TRUE;

	//
	// We have already seen '['. The next token
	// must be an identifier (syntax '[ identifier ....)
	//

	scn(&name);

	if (name.type != TokenIdentifier) {
		Fatal("Directive syntax error near %t", name);
		return FALSE;
	}

	//
	// Process directive:
	//

	if (!_stricmp(name.value.s->value, "bits")) ok = DirectiveBits(scn);
	else if (!_stricmp(name.value.s->value, "absolute")) ok = DirectiveAbsolute(scn);
	else if (!_stricmp(name.value.s->value, "global")) ok = DirectiveDeclare(scn, FALSE);
	else if (!_stricmp(name.value.s->value, "extern")) ok = DirectiveDeclare(scn, TRUE);
	else if (!_stricmp(name.value.s->value, "section")) ok = DirectiveSection(scn, pass);
	else if (!_stricmp(name.value.s->value, "file")) ok = DirectiveFile(scn, pass);
	else if (!_stricmp(name.value.s->value, "align")) ok = DirectiveAlign(scn, pass);

	//
	// Output format or Debug format directive?
	//

	else if (!_of->directive(name.value.s, scn, out, pass)) ok = _df && _df->directive(name.value.s, scn, out, pass);

	//
	// Put the last token back:
	//

	scn(&tok);
	if (ok && tok.type == TokenRightBracket) {
		scn(out);
		return TRUE;
	}
	Fatal("Directive syntax error near %t", name);
	return FALSE;
}

/**
*	FreeInstrContent
*
*	Description : Free instruction
*
*	Input : i - Instruction to free
*/
PRIVATE void FreeInstrContent (IN PPARSEDINSTR i) {

	PPARSEDINSTROP op;
	PPARSEDINSTROP next;

	if (!i) return;

	for (op = i->operands; op; op = next) {
		if (op->expr) free(op->expr);
		next = op->next;
		free(op);
	}
	free(i);
}

/**
*	NasmExitCallback
*
*	Description : Exit callback handler.
*/
void NasmExitCallback(void) {

#ifdef _MSC_VER
// We are good on this now. The intent was to make
// sure memory leaks don't occur during the main loop:
//	_CrtDumpMemoryLeaks();
//	__debugbreak();
#endif
}

extern PASS _pass;

/**
*	main
*
*	Description : This is the entry point to the assembler.
*
*	Input : argc - Number of command line arguments.
*           argv - Array of pointers to strings containing the command line argument list.
*
*	Output : EXIT_SUCCESS or EXIT_FAILURE on error.
*/
int main(IN int argc, IN char* argv[]) {

	PPARSEDINSTR instr;
	int          pass;

#define MAX_LINE 180

	char         line[MAX_LINE];

	_listing = &_listingNASM;

	//
	// Additional help for debugging:
	//

	atexit(NasmExitCallback);

#ifdef _MSC_VER
	_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_DEBUG);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
#endif

	//
	// To start up, we first process all command line arguments.
	// We then need to initialize defaults.
	//

	if (!ProcessCommandLine(argc, argv))
		return EXIT_SUCCESS;

	/* if no source file specified, display help and exit. */
	if (!_primarySourceFile) {
		DisplayHelp();
		return EXIT_SUCCESS;
	}

	/* if no output file specified, display help and exit. */
	if (!_outputFileName) {
		DisplayHelp();
		return EXIT_SUCCESS;
	}

	// initial file name is the same as the primary source file
	// it can be changed with the file directive
	_logicalFilename = _primarySourceFile;

	//
	// Open source file.
	//

	file = FileOpen(_primarySourceFile);
	if (!file) {
		Error("File %s not found", _primarySourceFile->value);
		return EXIT_FAILURE;
	}

	//
	// Init modules.
	//

	_listing->init(_listingFileName);
	_of->init(_outputFileName, _primarySourceFile);
	if (_df) _df->init(_of);

	LexInit();

	//
	// Go through each pass and assemble. If any parse
	// errors in PASS0, we stop then. Final code is only
	// generated in PASSGEN.
	//

	instr = NULL;
	for (pass = PASS0; pass < PASSMAX; pass++) {

		_pass = pass;

		//
		// Define standard macros for this pass.
		//

		if (_of && _of->stdmac) _of->stdmac();
		if (_df && _df->stdmac) _df->stdmac();
		FileOpenRaw("stdmac", _standardMacros);

		//
		// Initialize labels for this pass.
		//

		LabelInit();

		//
		// Translate all instructions.
		//

		do{
			if (pass > PASS0 && NasmGetErrorCount() > 0) {
				Error("Errors in pass %i, stopping", pass);
				goto finish;
			}

			if (instr)
				FreeInstrContent(instr);

			instr = Parse(pass);

			GenerateCode(instr, pass);

//			if (_pass == PASSGEN) {
//				FileGetLine(line, MAX_LINE);
				_listing->line(" ", 1);
//			}

		}
		while (instr);

		//
		// Prepare for next pass.
		//

		FreeMacros();

		if (pass < PASSGEN)
			file = FileOpen(_primarySourceFile);
	}

	//
	// Write the final listing and output file and perform
	// any additional cleanup.
	//

	if (_df) _df->cleanup();
	_listing->cleanup();
	_of->cleanup();

finish:

	FreeLexer();
	FreeLabels();
	FreeStringTable();

	return EXIT_SUCCESS;
}


#if 0

DWARF macros

%macro dwpsc 4
	[_dwpsc %1, %2, %3, %4]
%endmacro
%macro dwtag 2
	section .debug_info
	%1:
	[_dwtag %1, %2]
%endmacro
%macro dwattr 2
	[_dwattr %1 %2]
%endmacro
%macro dwattr2 3
	[_dwattr %1, %2, %3]
%endmacro
%macro dwcfi 1
	[_dwcfi %1]
%endmacro
%macro dwcfi2 2
	[_dwcfi %1, %2]
%endmacro
%macro dwcfi3 3
	[_dwcfi %1, %2, %3]
%endmacro
%define dwendentry [_dwendentry]
%define dwendtag [_dwendtag]
%macro dwfde 2
	[_dwfde %1, %2]
%endmacro
%macro dwcie 2
	[_dwcie %1, %2]
%endmacro
%macro dwcfa2 2
	[_dwcfa %1, %2]
%endmacro
%macro dwcfa3 3
	[_dwcfa %1, %2, %3]
%endmacro

#endif

// http://ftp.deas.harvard.edu/techreports/tr-30-04.pdf
// https://www.diva-portal.org/smash/get/diva2:951540/FULLTEXT01.pdf







