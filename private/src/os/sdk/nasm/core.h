/************************************************************************
*
*	core.h - Core library services.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef NASM_CORE_H
#define NASM_CORE_H

#define IN
#define OUT
#define OPTIONAL
#define PRIVATE static
#define PUBLIC

/* basic types. */

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long uint64_t;
typedef signed char sint8_t;
typedef signed short sint16_t;
typedef signed int sint32_t;
typedef signed long long sint64_t;
typedef char int8_t;
typedef short int16_t;
typedef int int32_t;
typedef long long int64_t;
typedef int bool_t;
typedef bool_t BOOL;
#define TRUE 1
#define FALSE 0
#define INVALID_HANDLE ((unsigned int)0)
typedef unsigned int handle_t;
typedef unsigned int index_t;
typedef unsigned int offset_t;
typedef unsigned int size_t;
typedef void* addr_t;
typedef uint16_t wchar_t;
typedef uint32_t handle_t;

/* Buffer. */

typedef struct BUFFER {
	char magic[4];
	size_t elementCount;
	size_t allocCount;
	uint8_t* data;
}BUFFER, *PBUFFER;

PUBLIC PBUFFER  NewBuffer(void);
PUBLIC void     BufferFree(IN PBUFFER r);
PUBLIC uint8_t* GetBufferData(IN PBUFFER b);
PUBLIC size_t   GetBufferLength(IN PBUFFER b);
PUBLIC void     BufferWrite(IN PBUFFER b, IN char c);
PUBLIC void     BufferAppend(IN PBUFFER b, IN char* s, IN int len);
PUBLIC void     BufferPrintf(IN PBUFFER b, IN char* fmt, ...);
PUBLIC void     BufferPush(IN PBUFFER b, IN char c);
PUBLIC char     BufferPop(IN PBUFFER b);

/* Vector. */

typedef struct VECTOR {
	char magic[4];
	uint8_t* data;
	size_t elementCount;
	size_t numAllocations;
	size_t elementSize;
}VECTOR, *PVECTOR;
typedef VECTOR TokenVector;
typedef VECTOR FileVector;

PUBLIC PVECTOR NewVector(IN OPTIONAL unsigned int numAllocations, IN size_t elementSize);
PUBLIC void    ExtendVector(IN PVECTOR v, IN size_t numAllocations);
PUBLIC PVECTOR CopyVector(IN PVECTOR v);
PUBLIC void    VectorPush(IN PVECTOR v, IN void* e);
PUBLIC void    VectorPop(IN PVECTOR v, OUT void* data);
PUBLIC void    VectorGet(IN PVECTOR v, IN index_t index, OUT void* data);
PUBLIC void    VectorSet(IN PVECTOR v, IN index_t index, OUT void* data);
PUBLIC void    VectorFirstElement(IN PVECTOR v, OUT void* data);
PUBLIC BOOL    VectorLastElement(IN PVECTOR v, OUT void* data);
PUBLIC PVECTOR NewReverseVector(IN PVECTOR v);
PUBLIC void    VectorAppend(IN PVECTOR v, IN PVECTOR a);
PUBLIC void*   VectorData(IN PVECTOR v);
PUBLIC size_t  VectorLength(IN PVECTOR v);
PUBLIC void    FreeVector(IN PVECTOR v);

/* Set. */

typedef struct SET SET, *PSET;
typedef struct SET {
	char magic[4];
	PSET next;
	char* s;
}SET, *PSET;
typedef SET StringSet;

PUBLIC PSET SetInsert(IN PSET k, IN char* s);
PUBLIC BOOL SetFind(IN PSET k, IN char* s);
PUBLIC PSET SetUnion(IN PSET a, IN PSET b);
PUBLIC PSET SetIntersect(IN PSET a, IN PSET b);

/* Linked list. */

typedef void* list_element_t;

typedef struct ListNode {
	char magic[4];
	struct ListNode* next;
	struct ListNode* prev;
	list_element_t data;
}ListNode;

typedef struct List {
	char magic[4];
	BOOL duplicates;
	ListNode* first;
	ListNode* last;
}List;

typedef void list_free_callback_t(ListNode* node);

extern List*     ListInitialize(IN List* root, IN BOOL duplicates);
extern ListNode* ListAdd(IN List* root, IN list_element_t data);
extern size_t    ListSize(IN List* root);
extern BOOL      ListRemove(IN List* root, IN ListNode* node);
extern ListNode* ListGetFirst(IN List* root, IN list_element_t data);
extern ListNode* ListGetNext(IN ListNode* node);
extern void      ListFree(IN List* list, IN OPTIONAL list_free_callback_t callback);

/* Dictionary. */

typedef list_element_t bucket_element_t;
typedef void* bucket_key_t;

typedef struct BucketItem {
	bucket_key_t key;
	bucket_element_t value;
}BucketItem;

typedef struct BucketChain {
	List bucketItemList;
}BucketChain;

typedef BOOL compare_key_callback_t(bucket_key_t a, bucket_key_t b);
typedef BOOL compare_value_callback_t(list_element_t a, list_element_t b);
typedef unsigned long compute_hash_callback_t(bucket_key_t key);

typedef struct STRING STRING, *PSTRING;
typedef struct MAPELEMENT *PMAPELEMENT;
typedef struct MAPELEMENT {
	char        magic[8];
	PMAPELEMENT next;
	unsigned    bucket;
	PSTRING     key;
	void*       value;
}MAPELEMENT, *PMAPELEMENT;

typedef struct MAP {
	char         magic[4];
	PMAPELEMENT* buckets;
	size_t       numberOfBuckets;
	size_t       numberOfElements;
}MAP, *PMAP;

typedef void(*map_free_t)(PMAPELEMENT);

extern PUBLIC PMAP   MapCreate (IN unsigned long numberOfBuckets);
extern PUBLIC void*  MapGet    (IN PMAP map, IN bucket_key_t key);
extern PUBLIC BOOL   MapPut    (IN PMAP map, IN char* key, IN void* value);
extern PUBLIC void   MapFree   (IN PMAP map, IN OPTIONAL map_free_t callback);
extern PUBLIC long   MapElementCount(IN PMAP dictionary);
extern PUBLIC BOOL   MapRemove(IN PMAP map, IN char* key, IN void* value);

#endif
