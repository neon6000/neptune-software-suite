/************************************************************************
*
*	bin.c - Binary file support.
*
*   Copyright(c) BrokenThorn Entertainment Co.All Rights Reserved.
*
************************************************************************/

/*
	This implements the Binary target which is responsible for back-end section
	and symbol management, performing relocations, and writing the section data
	to a flat binary file. It may also define target-specific macros.
*/

#include <time.h>
#include <string.h>
#include <malloc.h>
#include <assert.h>
#include "nasm.h"

/* Private Definitions *******************************/

/**
*	Describes a single section.
*/
typedef struct SECTION {
	PSTRING  name;
	int      align;
	int      org;
	offset_t position;
	int      padding;
	index_t  index;
	PBUFFER  data;
	offset_t symbolOffset;
}SECTION, *PSECTION;

typedef List SECTION_LIST;

/**
*	Describes a symbol.
*/
typedef struct SYMBOL {
	PSTRING      name;
	PSECTION     section;
	uint32_t     value;
	unsigned int seg;
}SYMBOL, *PSYMBOL;

typedef List SYMBOL_LIST;

/**
*	Describes a relocation.
*/
typedef struct RELOCATION {
	addr_t   vaddr;
	long     bytes;
	long     secref;
	long     secrel;
	PSECTION section;
}RELOCATION, *PRELOCATION;

typedef List RELOC_LIST;

PRIVATE SECTION_LIST   _sections;
PRIVATE RELOC_LIST     _relocs;
PRIVATE SYMBOL_LIST    _symbols;
PRIVATE PSECTION       _currentSection;
PRIVATE PSECTION       _absoluteSection;
PRIVATE char*          _filename;

/**
*	GetSymbolBySeg
*
*	Description : Given a unique segment (ID),
*	return the SYMBOL with this ID.
*
*	Input : seg - Segment (ID) of SYMBOL to find
*
*	Output : PSYMBOL of Symbol or NULL if not found
*/
PRIVATE PSYMBOL GetSymbolBySeg(IN int seg) {

	PSYMBOL   symbol;
	ListNode* current;

	for (current = _symbols.first; current; current = current->next) {
		symbol = (PSYMBOL)current->data;
		if (symbol->seg == seg)
			return symbol;
	}
	return NULL;
}

/**
*	NewSection
*
*	Description : Given the name of a new section, this
*	allocates a new section and adds it to the Section list.
*
*	Input : name - The name of the section to create.
*
*	Output : Pointer to new Section or NULL on error.
*/
PRIVATE PSECTION NewSection(IN PSTRING name) {

	PSECTION section;
	PBUFFER  dataspace;

	assert(name != NULL);

	section = (PSECTION)calloc(1, sizeof(SECTION));
	if (!section)
		return NULL;
	dataspace = NewBuffer();
	if (!dataspace) {
		free(section);
		return NULL;
	}
	section->name = name;
	section->data = dataspace;
	section->index = NextSegmentIndex();
	ListAdd(&_sections, section);
	return section;
}

/**
*	SectionWrite
*
*	Description : This procedure appends the provided
*	data to the current section.
*
*	Input : data - Pointer to the raw data to append.
*	        len - The number of bytes to write.
*/
PRIVATE void SectionWrite(IN PSECTION section, IN char* data, IN size_t len) {

	assert(section && section->data);
	BufferAppend(section->data, data, len);
}

/**
*	GetSectionByIndex
*
*	Description : Given an index, this returns the
*	section at the given index, or NULL if not found.
*
*	Input : index - The index value of the section.
*	This should be the same number retuned from SetSection.
*
*	Output : Pointer to the Section or NULL if not found.
*/
PRIVATE PSECTION GetSectionByIndex(IN index_t index) {

	PSECTION  section;
	ListNode* current;

	if (index == ABS_SEG)
		return _absoluteSection;
	for (current = _sections.first; current; current = current->next) {
		section = (PSECTION)current->data;
		if (section->index == index)
			return section;
	}
	return NULL;
}

/**
*	GetSectionByName
*
*	Description : Given the name of a section, this
*	returns the corrosponding section.
*
*	Input : name - The name of the section to find.
*
*	Output : Pointer to the Section or NULL if not found.
*/
PRIVATE PSECTION GetSectionByName(IN char* name) {

	ListNode* current;
	PSECTION  section;

	for (current = _sections.first; current; current = current->next) {
		section = (PSECTION)current->data;
		if (!strcmp(section->name->value, name))
			return section;
	}
	return NULL;
}

/**
*	Swap
*
*	Description : Given two list node's, this swaps them.
*	This is used for sorting sections.
*
*	Input : a - List node.
*	        b - List node to swap with.
*/
PRIVATE void Swap(ListNode* a, ListNode* b) {

	list_element_t t;
	
	t = a->data;
	a->data = b->data;
	b->data = t;
}

/**
*	SortSections
*
*	Description : Sorts the current Section List
*	by their ORG value. This modifies the Section list.
*/
PRIVATE void SortSections(void) {

	PSECTION  section;
	PSECTION  prevSection;
	ListNode* n;
	ListNode* p;

	for (n = _sections.first; n; n = n->next) {

		for (p = n; p; p = p->prev) {

			if (!p->prev)
				break;

			section = p->data;
			prevSection = p->prev->data;
			if (prevSection->org > section->org)
				Swap(p->prev, p);
		}
	}
}

/**
*	NewRelocation
*
*	Description : Allocates a new Relocation.
*
*	Output : Pointer to new Relocation obejct or NULL on error.
*/
PRIVATE PRELOCATION NewRelocation(void) {

	return (PRELOCATION)calloc(1, sizeof(RELOCATION));
}

/**
*	AddRelocation
*
*	Description : Adds a new Relocation to the specified Section.
*
*	Input : section - Section to where the Relocation is to be applied.
*	        size - Number of bytes of the relocation.
*	        segto - Segment relocation is relative to.
*	        secrel - Not used.
*/
PRIVATE void AddRelocation(IN PSECTION section, IN int size, IN long segto, IN long secrel) {

	PRELOCATION reloc;

	// this might happen with GLOBAL since EXTERN nor GLOBAL
	// are supported with BIN output.
	if (!segto)
		Error("AddRelocation: secref = 0! EXTERN and GLOBAL not suppported");

	reloc = NewRelocation();
	reloc->secref = segto;
	reloc->bytes = size;
	reloc->secrel = secrel;
	reloc->section = section;
	reloc->vaddr = (addr_t) GetBufferLength(section->data);
	ListAdd(&_relocs, reloc);
}

/**
*	Initialize
*
*	Description : Initialize the BIN target.
*
*	Input : filename - The name of the output file.
*	        src - The name of the input source file.
*/
PRIVATE void Initialize(IN char* filename, IN PSTRING src) {

	_filename = filename;
	ListInitialize(&_sections, FALSE);
	ListInitialize(&_relocs, FALSE);
	ListInitialize(&_symbols, FALSE);
	_currentSection = NewSection(InternString(".text"));

	_absoluteSection = calloc(1, sizeof(SECTION));
	_absoluteSection->index = ABS_SEG;
	_absoluteSection->name = InternString(".absolut");
}

/**
*	Cleanup
*
*	Description : Flush data to the output file and release
*	resources.
*/
PRIVATE void Cleanup(void) {

	ListNode*     n, *p;
	ListNode*     relocListItem;
	PSECTION      initial;
	PSECTION      s;
	PSECTION      k;
	PSECTION      sect;
	PSECTION      relocSection;
	PSYMBOL       sym;
	PRELOCATION   reloc;
	FILE*         out;
	unsigned long value;
	int           size;
	char*         segmentData;

	/* open output file. */
	out = fopen(_filename, "wb");

	/* Step 1: Sort sections by ORG. */
	SortSections();

	/* start. */
	initial = _sections.first->data;
	s = NULL;
	k = NULL;

	/* Step 2: Calculate section locations. */
	for (n = _sections.first, p = NULL; n; n = n->next) {

		//
		// n: current section
		// p: previous section
		//   s: data of current section
		//   k: data of previous section
		//

		s = n->data;

		if (!strcmp(s->name->value, ".bss"))
			continue;

		if (p) {

			k = p->data;
			size = (int) GetBufferLength(k->data);

			if (s->org == -1)
				s->org = k->org + size;

			if (s->org - k->org < size)
				Warn("Sections %s and %s overlap", k->name->value, s->name->value);

			k->padding = ((k->position + size + k->align - 1)
				& ~(k->align - 1)) - (k->position + GetBufferLength(k->data));

			if (s->org - k->org > size) {

				Warn("Forced padding: %u\n", s->org - k->org - size);
				k->padding = s->org - k->org - size;
			}

			s->position += k->position + size + k->padding;
			s->org = s->position + initial->org;
		}

		p = n;
	}

	/* adjust .bss */
	s = GetSectionByName(".bss");
	if (s && k) {
		s->org = k->org + GetBufferLength(k->data) + k->padding;
	}

	/* Step 3: Apply relocations. */
	for (relocListItem = _relocs.first; relocListItem; relocListItem = relocListItem->next) {

		reloc = relocListItem->data;

		if (reloc->bytes > 4)
			Fatal("Weird relocation size %i", reloc->bytes);

		value = 0;
		segmentData = GetBufferData(reloc->section->data) + (unsigned long)reloc->vaddr;
		memcpy(&value, segmentData, reloc->bytes);

		sect = GetSectionByIndex(reloc->secref);
		sym = GetSymbolBySeg(reloc->secref);

		relocSection = NULL;
		if (sect)
			relocSection = sect;
		else if (sym)
			relocSection = sym->section;
		else
			Fatal("bin: relocation with no segment");

		value += relocSection->org;

		switch (reloc->bytes) {
		case 1:
			memcpy(segmentData, &value, 1);
			break;
		case 2:
			memcpy(segmentData, &value, 2);
			break;
		case 4:
			memcpy(segmentData, &value, 4);
			break;
		};
	}

	/* Step 4: Write sections. */
	for (n = _sections.first; n; n = n->next) {

		s = n->data;
		if (GetBufferLength(s->data) > 0 && strcmp(s->name->value, ".bss")) {

			fwrite(GetBufferData(s->data), 1, GetBufferLength(s->data), out);
			if (n->next) {
				while (s->padding-- > 0)
					fputc('\xcc', out);
			}
		}
	}

	fclose(out);

	/* Cleanup sections: */
	for (n = _sections.first; n; n = n->next) {
		s = n->data;
		BufferFree(s->data);
	}
	ListFree(&_sections, NULL);
}

/**
*	FindSymbol
*
*	Description : Given a name, lookup the SYMBOL
*
*	Input : name - Name of symbol to find
*
*	Output : PSYMBOL or NULL if not found
*/
PRIVATE PSYMBOL FindSymbol(IN PSTRING name) {

	PSYMBOL s;
	ListNode* n;
	for (n = _symbols.first; n; n = n->next) {
		s = (PSYMBOL)n->data;
		if (strlen(s->name->value) == strlen(name->value) && strcmp(s->name->value, name->value) == 0)
			return s;
	}
	return NULL;
}

/**
*	AddSymbol
*
*	Description : Adds or Updates a Symbol to the Object Symbol Table. Segment is a unique
*   index from NextSegmentIndex or is EXTERN or ABSOLUTE. This uniquely identifies the Symbol.
*   The Symbol is always created in the Current Section with the value of "offset".
*
*	Input : name - Name of the symbol to add.
*	        segment - Segment the symbol is located in.
*	        offset - Value of the symbol.
*	        flags - Symbol extended flags.
*/
PRIVATE void AddSymbol(IN PSTRING name, IN handle_t segment, IN offset_t offset, IN SymbolFlags flags) {

	PSYMBOL  symbol;
	PSECTION section;

	symbol = FindSymbol(name);

	if (!symbol) {
		symbol = (PSYMBOL)malloc(sizeof(SYMBOL));
		symbol->name = name;
		symbol->value = offset;
		ListAdd(&_symbols, symbol);
	}

	if (segment == ABS_SEG)
		symbol->section = _absoluteSection;
	else
		symbol->section = _currentSection;

	symbol->seg = segment;
}

/**
*	GetSymbol
*
*	Description : Given a name of a symbol, get its value.
*
*	Input : name - Name of the symbol to get.
*	        off - Pointer to offset to receive the symbol value.
*
*	Output : TRUE if the symbol was found, FALSE if not.
*/
PRIVATE BOOL GetSymbol(IN char* name, OUT offset_t* off) {

	PSYMBOL   s;
	ListNode* n;

	for (n = _symbols.first; n; n = n->next) {
		s = (PSYMBOL)n->data;
		if (!strcmp(s->name->value, name)) {
			*off = s->value;
			return TRUE;
		}
	}
	return FALSE;
}

/**
*	GetSectionOffset
*
*	Description : Returns the current section offset.
*
*	Input : pass - The Code Genertor pass.
*
*	Output : The offset of the last byte written
*	to the current section.
*/
PRIVATE offset_t GetSectionOffset(IN PASS pass) {

	if (pass == PASS0)
		return _currentSection->symbolOffset;
	return GetBufferLength(_currentSection->data);
}

/**
*	SetSection
*
*	Description : Set a new section as the current section, or create one.
*
*	Input : name - The name of the section to set.
*	        pass - The Code Generator pass.
*	        mode - The current Operation Mode.
*
*	Output : Section index value.
*/
PRIVATE handle_t SetSection(IN char* name, IN PASS pass, IN BITS mode) {

	PSECTION k;

	if (pass == PASS0) {
		/* .text is always present, so don't recreate. */
		if (strcmp(name, ".text"))
			k = NewSection(InternString(name));
	}

	k = GetSectionByName(name);

	if (!k)
		return INVALID_HANDLE;

	_currentSection = k;

	return (handle_t)k->index;
}

/**
*	GetSection
*
*	Description : Return current section handle.
*
*	Input : name - Section name
*
*	Output : Handle to section or INVALID_HANDLE
*/
PRIVATE handle_t GetSection(IN OPTIONAL char* name) {

	PSECTION k;

	if (!name)
		k = _currentSection;
	else
		k = GetSectionByName(name);

	if (!k)
		return INVALID_HANDLE;

	return (handle_t)k->index;
}

/**
*	CreateSection
*
*	Description : Creates a new section (if needed) and
*	return its handle. Does NOT change segments.
*
*	Input : name - Section name
*
*	Output : Handle to section or INVALID_HANDLE
*/
PRIVATE handle_t CreateSection(IN char* name, IN int flags) {

	handle_t handle;
	PSECTION section;

	handle = GetSection(name);

	if (handle != INVALID_HANDLE)
		return handle;

	section = NewSection(InternString(name));
	return (handle_t)section->index;
}

/**
*	Write
*
*	Description : Write data to the specified section.
*
*	Input : seg - Index of the segment to write to.
*	        data - Pointer to data to write.
*	        type - Output type.
*	        size - Number of bytes to write.
*	        segto - Relative segment.
*	        pass - Current Code Generator pass.
*/
PRIVATE void Write(IN handle_t seg, IN void* data, IN output_t type, size_t size, IN int segto, IN PASS pass) {

	PSECTION section;

	if (seg == ABS_SEG)
		return;

	section = GetSectionByIndex(seg);

	if (pass == PASS0) {
		section->symbolOffset += size;
		return;
	}

	if (!strcmp(section->name, ".bss")) {
		Warn("Attempt to initialize data in .bss, ignoring...");
		return;
	}

	switch (type) {
	case WRITE_RESERVE:
	case WRITE_RAWDATA:
		SectionWrite(section, data, size);
		break;
	case WRITE_ADDRESS:
	case WRITE_REL1ADR:
	case WRITE_REL2ADR:
	case WRITE_REL4ADR:
		AddRelocation(section, size, segto, -1L);
		SectionWrite(section, data, size);
		break;
	default:
		Fatal("Write attempted with unknown output type %x", type);
	};
}

/**
*	ProcessDirective
*
*	Description : Process target specific directives.
*
*	Input : name - Directive name
*	        scn - Scanner
*
*	Output : TRUE if success else FALSE
*/
PRIVATE BOOL ProcessDirective(IN PSTRING name, IN PLEX scn, IN OUT PTOKEN token, IN PASS pass) {

	TOKEN tok;

	if (!_stricmp(name->value, "org")) {
		scn(&tok);
		if (tok.type != TokenInteger) return FALSE;
		_currentSection->org = tok.value.inum;
		return TRUE;
	}
	return FALSE;
}

//
// Target specific macros
//

PRIVATE char* _stdmac =
	"%macro org 1\n"
	"[org %1]\n"
	"%endmacro\n";

/**
*	StdMac
*
*	Description : Opens target-specific macros as a Special File. This
*	string is read as input into the assembler to process to define
*	the macros for use in User's own program. The front-end calls
*	ProcessDirective as needed for directives that are non-standard
*/
PRIVATE void StdMac(void) {

	FileOpenRaw("stdbin", _stdmac);
}

/* Public Definitions *******************************/

PUBLIC TARGET _binOutputFormat = {
	"bin",
	"Flat binary format",
	Initialize,
	Cleanup,
	AddSymbol,
	GetSectionOffset,
	Write,
	CreateSection,
	SetSection,
	GetSection,
	ProcessDirective,
	StdMac
};
