/************************************************************************
*
*	reg.c - Register list.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <string.h>
#include "nasm.h"

/* PRIVATE definitions ********************************/

Register _registers[] = {

	{ "al", RegAl, 0, OperandAl },
	{ "ax", RegAx, 0, OperandAx },
	{ "eax", RegEax, 0, OperandEax },
	{ "rax", RegRax, 0, OperandRax },
	{ "cl", RegCl, 1, OperandCl },
	{ "cx", RegCx, 1, OperandCx },
	{ "ecx", RegEcx, 1, OperandEcx },
	{ "rcx", RegRcx, 1, OperandRcx },
	{ "dl", RegDl, 2, OperandDl },
	{ "dx", RegDx, 2, OperandDx },
	{ "edx", RegEdx, 2, OperandEdx },
	{ "rdx", RegRdx, 2, OperandRdx },
	{ "bl", RegBl, 3, OperandBl },
	{ "bx", RegBx, 3, OperandBx },
	{ "ebx", RegEbx, 3, OperandEbx },
	{ "rbx", RegRbx, 3, OperandRbx },
	{ "ah", RegAh, 4, OperandAh },
	{ "sp", RegSp, 4, OperandSp },
	{ "esp", RegEsp, 4, OperandEsp },
	{ "rsp", RegRsp, 4, OperandRsp },
	{ "ch", RegCh, 5, OperandCh },
	{ "bp", RegBp, 5, OperandBp },
	{ "ebp", RegEbp, 5, OperandEbp },
	{ "rbp", RegRbp, 5, OperandRbp },
	{ "dh", RegDh, 6, OperandDh },
	{ "si", RegSi, 6, OperandSi },
	{ "esi", RegEsi, 6, OperandEsi },
	{ "rsi", RegRsi, 6, OperandRsi },
	{ "bh", RegBh, 7, OperandBh },
	{ "di", RegDi, 7, OperandDi },
	{ "edi", RegEdi, 7, OperandEdi },
	{ "rdi", RegRdi, 7, OperandRdi },
	{ "es", RegEs, 0, OperandEs },
	{ "cs", RegCs, 1, OperandCs },
	{ "ss", RegSs, 2, OperandSs },
	{ "ds", RegDs, 3, OperandDs },
	{ "fs", RegFs, 4, OperandFs },
	{ "gs", RegGs, 5, OperandGs },
	{ "cr0", RegCr0, 0, OperandCr0 },
	{ "cr1", RegCr1, 1, OperandCr1 },
	{ "cr2", RegCr2, 2, OperandCr2 },
	{ "cr3", RegCr3, 3, OperandCr3 },
	{ "cr4", RegCr4, 4, OperandCr4 },
	{ "cr5", RegCr5, 5, OperandCr5 },
	{ "cr6", RegCr6, 6, OperandCr6 },
	{ "cr7", RegCr7, 7, OperandCr7 },
	{ "cr8", RegCr8, 8, OperandCr8 },
	{ "cr9", RegCr9, 9, OperandCr9 },
	{ "cr10", RegCr10, 10, OperandCr10 },
	{ "cr11", RegCr11, 11, OperandCr11 },
	{ "cr12", RegCr12, 12, OperandCr12 },
	{ "cr13", RegCr13, 13, OperandCr13 },
	{ "cr14", RegCr14, 14, OperandCr14 },
	{ "cr15", RegCr15, 15, OperandCr15 },
	{ "r8l", RegR8l, 8, OperandR8l },
	{ "r9l", RegR9l, 9, OperandR9l },
	{ "r10l", RegR10l, 10, OperandR10l },
	{ "r11l", RegR11l, 11, OperandR11l },
	{ "r12l", RegR12l, 12, OperandR12l },
	{ "r13l", RegR13l, 13, OperandR13l },
	{ "r14l", RegR14l, 14, OperandR14l },
	{ "r15l", RegR15l, 15, OperandR15l },
	{ "r8w", RegR8w, 8, OperandR8w },
	{ "r9w", RegR9w, 9, OperandR9w },
	{ "r10w", RegR10w, 10, OperandR10w },
	{ "r11w", RegR11w, 11, OperandR11w },
	{ "r12w", RegR12w, 12, OperandR12w },
	{ "r13w", RegR13w, 13, OperandR13w },
	{ "r14w", RegR14w, 14, OperandR14w },
	{ "r15w", RegR15w, 15, OperandR15w },
	{ "r8d", RegR8d, 8, OperandR8d },
	{ "r9d", RegR9d, 9, OperandR9d },
	{ "r10d", RegR10d, 10, OperandR10d },
	{ "r11d", RegR11d, 11, OperandR11d },
	{ "r12d", RegR12d, 12, OperandR12d },
	{ "r13d", RegR13d, 13, OperandR13d },
	{ "r14d", RegR14d, 14, OperandR14d },
	{ "r15d", RegR15d, 15, OperandR15d },
	{ "r8", RegR8, 8, OperandR8 },
	{ "r9", RegR9, 9, OperandR9 },
	{ "r10", RegR10, 10, OperandR10 },
	{ "r11", RegR11, 11, OperandR11 },
	{ "r12", RegR12, 12, OperandR12 },
	{ "r13", RegR13, 13, OperandR13 },
	{ "r14", RegR14, 14, OperandR14 },
	{ "r15", RegR15, 15, OperandR15 },
	{ "st0", RegSt0, 0, OperandST0 },
	{ "st1", RegSt1, 1, OperandST1 },
	{ "st2", RegSt2, 2, OperandST2 },
	{ "st3", RegSt3, 3, OperandST3 },
	{ "st4", RegSt4, 4, OperandST4 },
	{ "st5", RegSt5, 5, OperandST5 },
	{ "st6", RegSt6, 6, OperandST6 },
	{ "st7", RegSt7, 7, OperandST7 },
	{ "mm0", RegMmx0, 0, OperandMmx0 },
	{ "mm1", RegMmx1, 1, OperandMmx1 },
	{ "mm2", RegMmx2, 2, OperandMmx2 },
	{ "mm3", RegMmx3, 3, OperandMmx3 },
	{ "mm4", RegMmx4, 4, OperandMmx4 },
	{ "mm5", RegMmx5, 5, OperandMmx5 },
	{ "mm6", RegMmx6, 6, OperandMmx6 },
	{ "mm7", RegMmx7, 7, OperandMmx7 },
	{ "xmm0", RegXmm0, 0, OperandXmm0 },
	{ "xmm1", RegXmm1, 1, OperandXmm1 },
	{ "xmm2", RegXmm2, 2, OperandXmm2 },
	{ "xmm3", RegXmm3, 3, OperandXmm3 },
	{ "xmm4", RegXmm4, 4, OperandXmm4 },
	{ "xmm5", RegXmm5, 5, OperandXmm5 },
	{ "xmm6", RegXmm6, 6, OperandXmm6 },
	{ "xmm7", RegXmm7, 7, OperandXmm7 },
	{ "xmm8", RegXmm8, 8, OperandXmm8 },
	{ "xmm9", RegXmm9, 9, OperandXmm9 },
	{ "xmm10", RegXmm10, 10, OperandXmm10 },
	{ "xmm11", RegXmm11, 11, OperandXmm11 },
	{ "xmm12", RegXmm12, 12, OperandXmm12 },
	{ "xmm13", RegXmm13, 13, OperandXmm13 },
	{ "xmm14", RegXmm14, 14, OperandXmm14 },
	{ "xmm15", RegXmm15, 15, OperandXmm15 },
};

/* note this must match same order as RegisterType. */
char* _register[] = {
	"", /* RegInvalid. */
	"ss", "cs", "ds", "es", "fs", "gs",
	"ah", "al", "bh", "bl", "ch", "cl", "dh", "dl",
	"si", "di", "bp", "sp", "ax", "bx", "cx", "dx",
	"eax", "ebx", "ecx", "edx",
	"esi", "edi", "ebp", "esp",
	"rax", "rbx", "rcx", "rdx",
	"rsi", "rdi", "rbp", "rsp",
	"cr0", "cr1", "cr2", "cr3", "cr4", "cr5", "cr6", "cr7",
	"cr8", "cr9", "cr10", "cr11", "cr12", "cr13", "cr14", "cr15",
	"dr0", "dr1", "dr2", "dr3", "dr4", "dr5", "dr6", "dr7",
	"dr8", "dr9", "dr10", "dr11", "dr12", "dr13", "dr14", "dr15",
	"r8l", "r9l", "r10l", "r11l", "r12l", "r13l", "r14l", "r15l",
	"r8w", "r9w", "r10w", "r11w", "r12w", "r13w", "r14w", "r15w",
	"r8d", "r9d", "r10d", "r11d", "r12d", "r13d", "r14d", "r15d",
	"r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15",
	"mm0", "mm1", "mm2", "mm3", "mm4", "mm5", "mm6", "mm7",
	"xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
	"xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15",
	"ymm0", "ymm1", "ymm2", "ymm3", "ymm4", "ymm5", "ymm6", "ymm7",
	"ymm8", "ymm9", "ymm10", "ymm11", "ymm12", "ymm13", "ymm14", "ymm15",
	"st0", "st1", "st2", "st3", "st4", "st5", "st6", "st7"
};

/* PUBLIC Definitions *********************************/

PUBLIC Register* GetRegister(IN RegisterType type) {

	int max = sizeof(_registers) / sizeof(Register);
	int k;
	for (k = 0; k < max; k++) {
		if (_registers[k].type == type)
			return &_registers[k];
	}
	return NULL;
}

PUBLIC RegisterType IsRegister(IN char* str) {

	int max = sizeof(_register) / sizeof(char*);
	int i = 0;
	for (i = 0; i < max; i++) {
		if (strlen(str) != strlen(_register[i]))
			continue;
		if (_strnicmp(str, _register[i], strlen(str)) == 0)
			return (RegisterType)i;
	}
	return RegInvalid;
}
