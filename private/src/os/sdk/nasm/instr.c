/************************************************************************
*
*	instr.c - Instruction list.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <string.h>
#include <assert.h>
#include "instr.h"
#define IN
#define OUT

/* generate mnemonic string table. Note the
ordering of this table is dependent on MNEMONICS. */
#undef C
#define C(x) #x,
const char* const _mnemonics[] = { MNEMONICS };

/* given a string, get the mnemonic id. */
PUBLIC Mnemonic GetMnemonic(IN char* s) {

	Mnemonic res;
	int max;
	int min;
	int mid;

	res = MnemonicInvalid;
	max = sizeof(_mnemonics) / sizeof(char*) - 1;
	min = 0;

	while (min <= max) {
		int result;

		mid = (min + max) / 2;
		result = _stricmp(_mnemonics[mid], s);

		if (result == 0) {
			res = (Mnemonic)mid;
			break;
		}
		else if (result > 0)
			max = mid - 1;
		else
			min = min + 1;
	}

	if (strlen(_mnemonics[res]) != strlen(s))
		return MnemonicInvalid;
	return res;
}

/* given the mnemonic id, get its string. */
PUBLIC const char* FromMnemonic(IN Mnemonic m) {

	assert(m < MnemonicInvalid);
	return _mnemonics[m];
}

