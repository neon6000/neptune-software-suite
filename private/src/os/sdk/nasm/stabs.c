/************************************************************************
*
*	stabs.c - Stabs debugging support.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nasm.h"

// https://docs.freebsd.org/info/stabs/stabs.pdf
// https://sourceware.org/gdb/current/onlinedocs/stabs.html#Stab-Sections

//
// Stab structure
//
typedef struct STABHDR {
	// offset into STABSTR or 0
	uint32_t  strx;
	uint8_t   type;
	uint8_t   other;
	uint16_t  desc;
	// "value" is written separately from the header
	// as it can be a relocatable address
//	uint32_t  value;
}STABHDR, *PSTABHDR;

//
// Current section offset
//
extern unsigned long  _offset;

PRIVATE PTARGET  target;
PRIVATE index_t  stabCount;
PRIVATE offset_t strtabSize;
PRIVATE BOOL     firstStab;
PRIVATE handle_t STABS;
PRIVATE handle_t STRINGS;

//
// type codes pre-defines
//
PRIVATE char* _typecodes =
"\n%define N_UNDF 0x0\n"
"%define N_EXT 0x1\n"
"%define N_ABS 0x2\n"
"%define N_TEXT 0x4\n"
"%define N_DATA 0x6\n"
"%define N_BSS 0x8\n"
"%define N_FN_SEQ 0xc\n"
"%define N_INDR 0x0a\n"
"%define N_COMM 0x12\n"
"%define N_SETA 0x14\n"
"%define N_SETT 0x16\n"
"%define N_SETD 0x18\n"
"%define N_SETB 0x1a\n"
"%define N_SETV 0x1c\n"
"%define N_WARNING 0x1e\n"
"%define N_FN 0x1f\n"
"%define N_GSYM 0x20\n"
"%define N_FNAME 0x22\n"
"%define N_FUNC 0x24\n"
"%define N_STSYM 0x26\n"
"%define N_LCSYM 0x28\n"
"%define N_MAIN 0x2a\n"
"%define N_ROSYM 0x2c\n"
"%define N_PC 0x30\n"
"%define N_NSYMS 0x32\n"
"%define N_NOMAP 0x34\n"
"%define N_MAC_DEFINE 0x36\n"
"%define N_OBJ 0x38\n"
"%define N_MAC_UNDEF 0x3a\n"
"%define N_OPT 0x3c\n"
"%define N_RSYM 0x40\n"
"%define N_M2C 0x42\n"
"%define N_SLINE 0x44\n"
"%define N_DSLINE 0x46\n"
"%define N_BSLINE 0x48\n"
"%define N_BROWS 0x48\n"
"%define N_DEFD 0x4a\n"
"%define N_FLINE 0x4c\n"
"%define N_EHDECL 0x50\n"
"%define N_MOD2 0x50\n"
"%define N_CATCH 0x54\n"
"%define N_SSYM 0x60\n"
"%define N_ENDM 0x62\n"
"%define N_SO 0x64\n"
"%define N_LSYM 0x80\n"
"%define N_BINCL 0x82\n"
"%define N_SOL 0x84\n"
"%define N_PSYM 0xa0\n"
"%define N_EINCL 0xa2\n"
"%define N_ENTRY 0xa4\n"
"%define N_LBRACE 0xc0\n"
"%define N_EXCL 0xc2\n"
"%define N_SCOPE 0xc4\n"
"%define N_RBRACE 0xe0\n"
"%define N_BCOMM 0xe2\n"
"%define N_ECOMM 0xe4\n"
"%define N_ECOML 0xe8\n"
"%define N_WITH 0xea\n"
"%define N_NBTEXT 0xf0\n"
"%define N_NBDATA 0xf2\n"
"%define N_NBBSS 0xf4\n"
"%define N_NBSTS 0xf6\n"
"%define N_NBLCS 0xf8\n";

//
// pre-defined macros
//
PRIVATE char* _macros =
"\n%macro stabs 5\n"
"[stabs %1, %2, %3, %4, %5]\n"
"%endmacro\n"
"%macro stabn 4\n"
"[stabn %1, %2, %3, %4]\n"
"%endmacro\n"
"%macro stabd 3\n"
"[stabd %1, %2, %3]\n"
"%endmacro\n"
"%macro stabx 4\n"
"[stabx %1, %2, %3, %3]\n"
"%endmacro\n";

/**
*	Cleanup
*
*	Description : Cleans up any resources
*/
PRIVATE void Cleanup(void) {}

/**
*	AddString
*
*	Description : During PASS 0: writes the string to the STRINGS section
*	while avoiding duplicates. During successive passes, this only returns
*	the offset of the string that was written from a previous pass.
*
*	Input : string - String to append
*         pass - Current pass
*
*	Output : Offset after appending string
*/
PRIVATE offset_t AddString(IN PSTRING string, IN PASS pass) {

	size_t len;

	if (!string)
		return 0;

	//
	// if the string was already added, return
	//
	if (string->offset2)
		return string->offset2;

	//
	// strings are only added in PASS0
	//
	len = strlen(string->value) + 1;
	if (pass == PASS0) {
		string->offset2 = strtabSize;
		target->out(STRINGS, string->value, WRITE_RAWDATA, len, 0, PASSGEN);
		strtabSize += len;
	}
	return string->offset2;
}

/**
*	Init
*
*	Description : Initialize pre-defined macros
*	and creates STAB and STABSTR section
*
*	Input : out - Target
*/
PRIVATE void Init(IN PTARGET out) {

	target = out;
	STABS = target->newSection(".stab", 0);
	STRINGS = target->newSection(".stabstr", 0);

	AddString(InternString(""), PASS0);
	AddString(GetLogicalFileName(), PASS0);
}

/**
*	MabeyWriteFirstStab
*
*	Description : The first stab is written
*	automatically only once during PASSGEN
*/
PRIVATE void MabeyWriteFirstStab(IN PASS pass) {

	STABHDR hdr;

	if (pass != PASSGEN) return;
	if (firstStab) return;

	firstStab = TRUE;

	/*
	The first stab contains some basic information about the
	data and original file name. We get this information in PASS 0
	and write it out in PASSGEN before any other stab.
	+-----------+---------+-------+-----------+-------+
	| strx      | type    | other | desc      | value |
	+-----------+---------+-------+-----------+-------+
		strx : offset of source file name
		type : N_UNDF
		other : 0 (can be overflow of desc)
		desc : number of upcoming stabs
		value : size of string table I think
	*/

	hdr.strx = AddString(GetLogicalFileName(), PASS0);
	hdr.type = 0; // N_UNDF
	hdr.other = 0;
	hdr.desc = stabCount;

	target->out(STABS, &hdr, WRITE_RAWDATA, sizeof(STABHDR), 0, pass);
	target->out(STABS, &strtabSize, WRITE_RAWDATA, sizeof(uint32_t), 0, pass);
}

/**
*	Write
*
*	Description : Writes a stab to STABS section. Strings
*   are stored in the string table if and only if it has not
*   already been added. This will also write value as a relocatable
*   address if needed.
*
*	Input : string - stab.strx string
*           type - type code
*           other - other value or code
*           desc - desc value
*           value - value expression
*           pass - current pass
*/
PRIVATE void Write(IN PSTRING string, IN int type,
	IN int other, IN int desc, IN PEXPR value, IN PASS pass) {

	STABHDR hdr;

	//
	// for PASS 0, add number of stabs and create the string table:
	//
	if (pass == PASS0) {
		AddString(string, pass);
		stabCount++;
		return;
	}

	if (pass != PASSGEN)
		return;

	MabeyWriteFirstStab(pass);

	hdr.strx = AddString(string, pass);
	hdr.desc = desc;
	hdr.type = type;
	hdr.other = other;

	target->out(STABS, &hdr, WRITE_RAWDATA, sizeof(STABHDR), 0, pass);
	if (IsReloc(value))
		target->out(STABS, &value->val, WRITE_ADDRESS, 4, value->kind & 0xffff, pass);
	else
		target->out(STABS, &value->val, WRITE_RAWDATA, 4, 0, pass);
}

/**
*	Get
*
*	Description : Scans the next token and test the token type.
*
*	Input : scn - Scanner
*         token - This is the output token just read
*         type - Expected token type of "token"
*
*	Output : TRUE if "token" is "type", FALSE otherwise
*/
PRIVATE BOOL Get(IN PLEX scn, IN PTOKEN token, IN TOKENTYPE type) {

	scn(token);
	return token->type == type;
}

/**
*	Stabs
*
*	Description : Process stabs directive
*	This directive creates a new stab entry
*
*	Syntax : [stabs "string",  type, other, desc, value]
*
*	Input : scn - Scanner
*         token - Current parser token. On INPUT, this is the current
*         token being processed. On OUTPUT this will replace the active token
*         pass - Current pass
*
*	Output : TRUE if success, FALSE on error
*/
PRIVATE BOOL Stabs(IN PLEX scn, IN PTOKEN token, IN PASS pass) {

	TOKEN   comma;
	TOKEN   str;
	TOKEN   type;
	TOKEN   other;
	TOKEN   desc;
	TOKEN   valueTok;
	PEXPR   value;

	if (!Get(scn, &str, TokenString)) return FALSE;
	if (!Get(scn, &comma, TokenComma)) return FALSE;
	if (!Get(scn, &type, TokenInteger)) return FALSE;
	if (!Get(scn, &comma, TokenComma)) return FALSE;
	if (!Get(scn, &other, TokenInteger)) return FALSE;
	if (!Get(scn, &comma, TokenComma)) return FALSE;
	if (!Get(scn, &desc, TokenInteger)) return FALSE;
	if (!Get(scn, &comma, TokenComma)) return FALSE;

	scn(&valueTok);
	value = Expr(scn, &valueTok);

	Write(str.value.s, type.value.inum, other.value.inum, desc.value.inum, value, pass);

	LexUnget(valueTok);

	free(value);
	return TRUE;
}

/**
*	Stabn
*
*	Description : Process stabn directive
*	This directive creates a new stab entry
*
*	Syntax : [stabn type, other, desc, value]
*
*	Input : scn - Scanner
*         token - Current parser token. On INPUT, this is the current
*         token being processed. On OUTPUT this will replace the active token
*         pass - Current pass
*
*	Output : TRUE if success, FALSE on error
*/
PRIVATE BOOL Stabn(IN PLEX scn, IN PTOKEN token, IN PASS pass) {

	TOKEN   comma;
	TOKEN   type;
	TOKEN   other;
	TOKEN   desc;
	TOKEN   valueTok;
	PEXPR   value;

	if (!Get(scn, &type, TokenInteger)) return FALSE;
	if (!Get(scn, &comma, TokenComma)) return FALSE;
	if (!Get(scn, &other, TokenInteger)) return FALSE;
	if (!Get(scn, &comma, TokenComma)) return FALSE;
	if (!Get(scn, &desc, TokenInteger)) return FALSE;
	if (!Get(scn, &comma, TokenComma)) return FALSE;

	scn(&valueTok);
	value = Expr(scn, &valueTok);

	Write(NULL, type.value.inum, other.value.inum, desc.value.inum, value, pass);

	LexUnget(valueTok);

	free(value);
	return TRUE;
}

/**
*	Stabd
*
*	Description : Process stabd directive
*	This directive creates a new stab entry
*
*	Syntax : [stabn type, other, desc]
*	"value" is implicate and has current file location
*
*	Input : scn - Scanner
*         token - Current parser token. On INPUT, this is the current
*         token being processed. On OUTPUT this will replace the active token
*         pass - Current pass
*
*	Output : TRUE if success, FALSE on error
*/
PRIVATE BOOL Stabd(IN PLEX scn, IN PTOKEN token, IN PASS pass) {

	TOKEN   comma;
	TOKEN   type;
	TOKEN   other;
	TOKEN   desc;
	EXPR    value;

	if (!Get(scn, &type, TokenInteger)) return FALSE;
	if (!Get(scn, &comma, TokenComma)) return FALSE;
	if (!Get(scn, &other, TokenInteger)) return FALSE;
	if (!Get(scn, &comma, TokenComma)) return FALSE;
	if (!Get(scn, &desc, TokenInteger)) return FALSE;

	value.kind = EXPR_NUM;
	value.val = _offset;

	Write(NULL, type.value.inum, other.value.inum, desc.value.inum, &value, pass);
	return TRUE;
}

/**
*	Stabx
*
*	Description : Process stabx directive
*	This directive creates a new stab entry
*
*	Syntax : [stabn string, value, type, sdb-type]
*	 sdb-type is unused and should always be 0
*
*	Input : scn - Scanner
*         token - Current parser token. On INPUT, this is the current
*         token being processed. On OUTPUT this will replace the active token
*         pass - Current pass
*
*	Output : TRUE if success, FALSE on error
*/
PRIVATE BOOL Stabx(IN PLEX scn, IN PTOKEN token, IN PASS pass) {

	TOKEN   comma;
	TOKEN   str;
	TOKEN   type;
	TOKEN   sdbType;
	TOKEN   valueTok;
	PEXPR   value;

	if (!Get(scn, &str, TokenString)) return FALSE;
	if (!Get(scn, &comma, TokenComma)) return FALSE;

	scn(&valueTok);
	value = Expr(scn, &valueTok);
	LexUnget(valueTok);

	if (!Get(scn, &comma, TokenComma)) goto err;
	if (!Get(scn, &type, TokenInteger)) goto err;
	if (!Get(scn, &comma, TokenComma)) goto err;
	if (!Get(scn, &sdbType, TokenInteger)) goto err;

	Write(str.value.s, type.value.inum, 0, NULL, value, pass);

	free(value);
	return TRUE;

err:
	free(value);
	return FALSE;

}

/**
*	ProcessDirective
*
*	Description : Handle directives for stabs debugging.
*
*	Input : name - Name of the directive
*	        scn - Scanner function
*			token - Parser current token
*			pass - Current pass
*
*	Output : TRUE if directive was handled, FALSE if not supported or error
*/
PRIVATE BOOL ProcessDirective(IN PSTRING name, IN PLEX scn, IN OUT PTOKEN token, IN PASS pass) {

	if (!stricmp(name->value, "stabs"))      return Stabs(StdScan, token, pass);
	else if (!stricmp(name->value, "stabn")) return Stabn(scn, token, pass);
	else if (!stricmp(name->value, "stabd")) return Stabd(scn, token, pass);
	else if (!stricmp(name->value, "stabx")) return Stabx(scn, token, pass);
	return FALSE;
}

PRIVATE void StdMac(void) {

	FileOpenRaw("stabTypeCodes", _typecodes);
	FileOpenRaw("stabMacros", _macros);
}

PUBLIC DEBUG _stabs = {
	"stabs",
	"STABS debugging support",
	Init,
	Cleanup,
	ProcessDirective,
	StdMac
};
