/************************************************************************
*
*	err.c - Error management.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include "nasm.h"

#ifdef _MSC_VER
typedef int HANDLE;
#define INVALID_HANDLE_VALUE (-1)
#define STD_OUTPUT_HANDLE (-11)
#define FOREGROUND_BLUE      0x0001 // text color contains blue.
#define FOREGROUND_GREEN     0x0002 // text color contains green.
#define FOREGROUND_RED       0x0004 // text color contains red.
#define FOREGROUND_INTENSITY 0x0008 // text color is intensified.
#define BACKGROUND_BLUE      0x0010 // background color contains blue.
#define BACKGROUND_GREEN     0x0020 // background color contains green.
#define BACKGROUND_RED       0x0040 // background color contains red.
#define BACKGROUND_INTENSITY 0x0080 // background color is intensified.
#define COMMON_LVB_LEADING_BYTE    0x0100 // Leading Byte of DBCS
#define COMMON_LVB_TRAILING_BYTE   0x0200 // Trailing Byte of DBCS
#define COMMON_LVB_GRID_HORIZONTAL 0x0400 // DBCS: Grid attribute: top horizontal.
#define COMMON_LVB_GRID_LVERTICAL  0x0800 // DBCS: Grid attribute: left vertical.
#define COMMON_LVB_GRID_RVERTICAL  0x1000 // DBCS: Grid attribute: right vertical.
#define COMMON_LVB_REVERSE_VIDEO   0x4000 // DBCS: Reverse fore/back ground attribute.
#define COMMON_LVB_UNDERSCORE      0x8000 // DBCS: Underscore.
#endif

PUBLIC unsigned int _errorCount;
PUBLIC unsigned int _warnCount;

#if 0

PRIVATE void SetColor(IN ERRLEVEL level) {

#ifdef _MSC_VER
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	int color;
	if (h == INVALID_HANDLE_VALUE)
		return;
	switch (level) {
	case LevelInfo:
		color = FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_BLUE;
		break;
	case LevelWarn:
		color = FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY;
		break;
	case LevelError:
	case LevelFatal:
		color = FOREGROUND_RED | FOREGROUND_INTENSITY;
		break;
	};
	SetConsoleTextAttribute(h, color);
#endif
}
#endif

PUBLIC unsigned int NasmGetErrorCount(void) {

	return _errorCount;
}

PUBLIC unsigned int NasmGetWarnCount(void) {

	return _warnCount;
}

FILE* _log;

PUBLIC void NasmLogInit(IN PSTRING name) {

	_log = fopen(name->value, "w");
}

typedef void(*msg_handler_t)(IN PBUFFER, IN va_list*);

/* message format type. */
typedef struct FORMAT {
	char* format;
	msg_handler_t callback;
}FORMAT, *PFORMAT;

PRIVATE void MsgDisplayString(IN PBUFFER buf, IN va_list* args) {

	BufferPrintf(buf, "%s", va_arg(*args, char*));
}

PRIVATE void MsgDisplayLong(IN PBUFFER buf, IN va_list* args) {

	BufferPrintf(buf, "%i", va_arg(*args, long));
}

PRIVATE void MsgDisplayUnsignedLong(IN PBUFFER buf, IN va_list* args) {

	BufferPrintf(buf, "%u", va_arg(*args, unsigned long));
}

PRIVATE void MsgDisplayHexLower(IN PBUFFER buf, IN va_list* args) {

	BufferPrintf(buf, "%x", va_arg(*args, unsigned long));
}

PRIVATE void MsgDisplayHexUpper(IN PBUFFER buf, IN va_list* args) {

	BufferPrintf(buf, "%X", va_arg(*args, unsigned long));
}

PRIVATE void MsgDisplayChar(IN PBUFFER buf, IN va_list* args) {

	BufferPrintf(buf, "%c", va_arg(*args, char));
}

PRIVATE void MsgDisplayPercent(IN PBUFFER buf, IN va_list* args) {

	BufferPrintf(buf, "%s", "%");
}

PRIVATE void MsgDisplayToken(IN PBUFFER buf, IN va_list* args) {

	PTOKEN tok;

	tok = va_arg(args, PTOKEN);
	switch (tok->type) {
	case TokenInvalid: BufferPrintf(buf, "<invalid>"); break;
	case TokenEndOfFile: BufferPrintf(buf, "<EOF>"); break;
	case TokenEndOfLine: BufferPrintf(buf, "<EOL>"); break;
	case TokenInteger: BufferPrintf(buf, "%i", tok->value.inum); break;
	case TokenFloat:BufferPrintf(buf, "%s", tok->value.s->value); break;
	case TokenIdentifier:BufferPrintf(buf, "%s", tok->value.s->value); break;
	case TokenString:BufferPrintf(buf, "\"%s\"", tok->value.s->value); break;
	case TokenPrefix:BufferPrintf(buf, "<prefix>");  break;
	case TokenRegister:BufferPrintf(buf, "<register>"); break;
	case TokenMnemonic:BufferPrintf(buf, "<mnemonic>"); break;
	case TokenDirective:BufferPrintf(buf, "<directive>"); break;
	case TokenSizeSpecifier: BufferPrintf(buf, "<specifier>"); break;
	case TokenTimes:BufferPrintf(buf, "times"); break;
	case TokenHere:BufferPrintf(buf, "$"); break;
	case TokenSegBase:BufferPrintf(buf, "$$"); break;
	case TokenRightShift:BufferPrintf(buf, ">>"); break;
	case TokenLeftShift:BufferPrintf(buf, "<<"); break;
	case TokenEqual:BufferPrintf(buf, "="); break;
	case TokenBitOr:BufferPrintf(buf, "|"); break;
	case TokenBitAnd:BufferPrintf(buf, "&"); break;
	case TokenNot:BufferPrintf(buf, "!"); break;
	case TokenXor:BufferPrintf(buf, "^"); break;
	case TokenMod:BufferPrintf(buf, "%"); break;
	case TokenPlus:BufferPrintf(buf, "+"); break;
	case TokenMinus:BufferPrintf(buf, "-"); break;
	case TokenStar:BufferPrintf(buf, "*"); break;
	case TokenFowardSlash:BufferPrintf(buf, "//"); break;
	case TokenLeftBracket:BufferPrintf(buf, "["); break;
	case TokenRightBracket:BufferPrintf(buf, "]"); break;
	case TokenLeftParen:BufferPrintf(buf, "("); break;
	case TokenRightParen:BufferPrintf(buf, ")"); break;
	case TokenComma:BufferPrintf(buf, ","); break;
	case TokenColon:BufferPrintf(buf, ":"); break;
	case TokenMacroOperand:BufferPrintf(buf, "<MACRO OP>"); break;
	default:BufferPrintf(buf, "<???>"); break;
	};
}

PRIVATE FORMAT _formats[] = {

	{ "%%", MsgDisplayPercent },
	{ "%c", MsgDisplayChar },
	{ "%x", MsgDisplayHexLower },
	{ "%X", MsgDisplayHexUpper },
	{ "%u", MsgDisplayUnsignedLong },
	{ "%i", MsgDisplayLong },
	{ "%s", MsgDisplayString },
	{ "%t", MsgDisplayToken },
	{ NULL, NULL }
};

PRIVATE PFORMAT GetFormat(IN char* s) {

	int max;
	int i;

	max = sizeof(_formats) / sizeof(FORMAT);
	for (i = 0; _formats[i].format && i < max; i++) {
		if (strncmp(_formats[i].format, s, 2) == 0)
			return &_formats[i];
	}

	return NULL;
}

PRIVATE void Print(IN FILE* stream, IN char* s, IN va_list args) {

	PFORMAT spec;
	PBUFFER  buf;

	buf = NewBuffer();

	while (*s) {
		if (*s == '%') {
			spec = GetFormat(s);
			if (spec && spec->callback) {
				spec->callback(buf, &args);
				s += strlen(spec->format);
				continue;
			}
		}
		BufferPrintf(buf, "%c", *s++);
	}
	BufferPrintf(buf, "\0");
	fprintf(stream, "%s", buf->data);
	BufferFree(buf);
}

PUBLIC void NasmError(IN ERRLEVEL level, IN char* msg, ...) {

	va_list  args;
	FILE*    file;
	PCOORD   src;

	src = FileCoord();

//	SetColor(level);

	if (_log)
		file = _log;
	else
		file = stdout;

	fprintf(file, "\n\r");
	if (src->fname)
		fprintf(file, "%s:%i: ", src->fname->value, src->y);
	switch (level) {
	case LevelInfo:
		fprintf(file, "info: ");
		break;
	case LevelWarn:
		fprintf(file, "warn: ");
		_warnCount++;
		break;
	case LevelError:
		fprintf(file, "error: ");
		_errorCount++;
		break;
	case LevelFatal:
		fprintf(file, "panic: ");
		_errorCount++;
	};

	va_start(args, msg);
	Print(file, msg, args);
	va_end(args);

	if (level == LevelFatal) {
#ifdef _MSC_VER
		__debugbreak();
#endif
		exit(-1);
	}
}
