/************************************************************************
*
*	match.c - Instruction matching.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nasm.h"

/* instruction table. */
extern InstrTableEntry _instructions[];
extern unsigned long _table_size;

/* compares operand types (one from table, one given from user)
and compares them to see if they are compatible. */
PRIVATE BOOL MatchOperand(IN SIZE size, IN OperandType a, IN OperandType b) {

	OperandClass ca = GetOperandClass(a);
	OperandClass cb = GetOperandClass(b);

	OperandSize sa = GetOperandSize(a);
	OperandSize sb = GetOperandSize(b);

	if ((ca == OperandClassImm && (GetOperandSubclass(a) & OperandImmPtr) == OperandImmPtr)
		&& (cb == OperandClassImm && (GetOperandSubclass(b) & OperandImmPtr) != OperandImmPtr)) {

		return FALSE;
	}

	/* if the user requested a pointer operand, we must only match ptr16:16 or ptr16:32. */
	if (cb == OperandClassImm && (GetOperandSubclass(b) & OperandImmPtr) == OperandImmPtr) {
		if (ca == OperandClassImm && (GetOperandSubclass(a) & OperandImmPtr) == OperandImmPtr) {

			if (sa == sb)
				return TRUE;
		}
		return FALSE;
	}

	/* if both operand types are the same, its a match. */
	if (ca == OperandClassMem && cb == OperandClassMem)
		return TRUE;

	else if (ca == OperandClassImm && cb == OperandClassImm) {
		return TRUE;
	}

	/* Register operands. */
	else if (ca == OperandClassReg && cb == OperandClassReg) {

		/* Make sure the type of register matches. */
		if ((GetOperandSubclass(a) != GetOperandSubclass(b)))
			return FALSE;

		/* If the table entry field also has a specific register... */
		if (GetOperandSubstruct(a) != 0) {

			/* ...Check to make sure the register id's match. */
			if (GetOperandSubstruct(a) != GetOperandSubstruct(b))
				return FALSE;

			return TRUE;
		}

		/* If this instruction accepts a generic register, the sizes must match. */
		if (GetOperandSize(a) == GetOperandSize(b))
			return TRUE;

		/* Does not match. */
		return FALSE;
	}

	/* memory or memory/reg operand. */
	else if (ca == OperandClassMem) {

		/* memory/reg operand. */
		if ((a == OperandRm8 || a == OperandRm16 || a == OperandRm32 || a == OperandRm64)) {
			
			if (cb == OperandClassReg) {
				if (GetOperandSubclass(a) == GetOperandSubclass(b)) {
					if (GetOperandSize(a) == GetOperandSize(b))
						return TRUE;
				}
				return FALSE;
			}

			else if (cb == OperandClassMem)
				return TRUE;

			return FALSE;
		}

		/* memory only operand. */
		else if (cb == OperandClassMem) {
			return TRUE;
		}

		return FALSE;
	}

	/* no match. */
	else return FALSE;
}

PRIVATE BOOL IsBetterMatch(IN InstrTableEntry* currentBest, IN InstrTableEntry* compareWith,
	IN OPTIONAL OperandType op1, IN OPTIONAL OperandType op2, IN OPTIONAL OperandType op3) {

	if (!currentBest)
		return TRUE;

	/* only look at table enteries that are greater then or equal to what we want to match: */
	if (compareWith) {
		if (compareWith->numParms > 0 && GetOperandSize(compareWith->parms[0]) < GetOperandSize(op1)) return FALSE;
		if (compareWith->numParms > 1 && GetOperandSize(compareWith->parms[1]) < GetOperandSize(op2)) return FALSE;
		if (compareWith->numParms > 2 && GetOperandSize(compareWith->parms[2]) < GetOperandSize(op3)) return FALSE;
	}

	/* Use Pigeonhole Principle to find the best match. */
	if (compareWith->numParms > 0 && GetOperandSize(currentBest->parms[0]) > GetOperandSize(op1)
		&& GetOperandSize(compareWith->parms[0]) <= GetOperandSize(currentBest->parms[0])) {
			return TRUE;
	}
	if (compareWith->numParms > 0 && GetOperandSize(currentBest->parms[0]) < GetOperandSize(op1)
		&& GetOperandSize(compareWith->parms[0]) >= GetOperandSize(currentBest->parms[0])) {
			return TRUE;
	}
	if (compareWith->numParms > 1 && GetOperandSize(currentBest->parms[1]) > GetOperandSize(op2)
		&& GetOperandSize(compareWith->parms[1]) <= GetOperandSize(currentBest->parms[1])) {
		return TRUE;
	}
	if (compareWith->numParms > 1 && GetOperandSize(currentBest->parms[1]) < GetOperandSize(op2)
		&& GetOperandSize(compareWith->parms[1]) >= GetOperandSize(currentBest->parms[1])) {
		return TRUE;
	}
	if (compareWith->numParms > 2 && GetOperandSize(currentBest->parms[1]) > GetOperandSize(op2)
		&& GetOperandSize(compareWith->parms[1]) <= GetOperandSize(currentBest->parms[1])) {
		return TRUE;
	}
	if (compareWith->numParms > 2 && GetOperandSize(currentBest->parms[2]) < GetOperandSize(op3)
		&& GetOperandSize(compareWith->parms[2]) >= GetOperandSize(currentBest->parms[2])) {
		return TRUE;
	}

	/* if "compareWith" specifies register codes and "currentBest" does not. */
	if (compareWith->numParms > 0 && GetOperandClass(compareWith->parms[0]) == OperandClassReg
		&& GetOperandSubstruct(compareWith->parms[0]) != 0) {
			if (GetOperandSubstruct(compareWith->parms[0]) != GetOperandSubstruct(currentBest->parms[0]))
				return TRUE;
	}
	if (compareWith->numParms > 1 && GetOperandClass(compareWith->parms[1]) == OperandClassReg
		&& GetOperandSubstruct(compareWith->parms[1]) != 0) {
		if (GetOperandSubstruct(compareWith->parms[1]) != GetOperandSubstruct(currentBest->parms[1]))
			return TRUE;
	}

	/* keep the current best match. */
	return FALSE;
}

/* matches instruction that satisfies the specified operand types and operand count.
Example of use: MatchInstruction(ADD, 2, OperandClassMem, OperandImm32, OperandNone); */
PUBLIC InstrTableEntry* MatchInstruction(IN Mnemonic mnemonic, IN SIZE s, IN int operandCount,
	IN OPTIONAL OperandType op1, IN OPTIONAL OperandType op2, IN OPTIONAL OperandType op3) {

	int c;
	int max = _table_size;
	InstrTableEntry* best;

	best = NULL;
	for (c = 0; c < max; c++) {

		InstrTableEntry* k = &_instructions[c];

		if (best && (k->mnemonic != mnemonic))
			break;

		/* if we match a psuedo instruction, we ignore the operands. */
		if (k->mnemonic == mnemonic && k->modifier & ModPsuedo)
			return k;

		/* match real instructions: */
		if (k->mnemonic == mnemonic && k->numParms == operandCount) {

			OperandClass parm0 = k->parms[0];
			OperandClass parm1 = k->parms[1];
			OperandClass parm2 = k->parms[2];

			if (operandCount > 0 && !MatchOperand(s, parm0,op1)) continue;
			if (operandCount > 1 && !MatchOperand(s, parm1,op2)) continue;
			if (operandCount > 2 && !MatchOperand(s, parm2,op3)) continue;

			if (IsBetterMatch(best, k, op1, op2, op3))
				best = k;
		}
	}

	return best;
}
