/************************************************************************
*
*	lex.c - Lexical Analysis 
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nasm.h"
#include "instr.h"
#define IN
#define OUT
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <assert.h>

/* PRIVATE Definitions ***************************/

PRIVATE TokenVector* _pushbackBuffer;
PRIVATE PVECTOR      _tokenStreams;

/* skip whitespace. Returns TRUE if EOF, FALSE otherwise. */
PRIVATE BOOL SkipWhitespace(void) {

	int c;

	while ((c = FilePeek()) != EOF) {
		if (c == ' ' || c == '\t')  {
			FileGet();
			continue;
		}
		else return FALSE;
	}
	return TRUE;
}

/* test if character is valid for identifiers.
Note that all identifiers must start with ., _, $, @, or an alpha character. */
PRIVATE BOOL isident(int c) {

	return c == '_' || c == '$' || isalpha(c) || c == '@' || isdigit(c) || c == '.' || c == '?';
}

/* test if character is a supported operator. */
PRIVATE BOOL isoper(int c) {

	switch (c) {
	case '+': case '-': case '*': case '/': case '[': case ']':
	case '(': case ')': case ',': case ':':
	case '>': case '<': case '|': case '&': case '~': case '!':
	case '%':
	case '=':
		return TRUE;
	default:
		return FALSE;
	};
}

/* initialize TOKEN object. */
PRIVATE TOKEN InitToken(IN const char* lexeme, IN TOKENTYPE type, IN unsigned long value) {

	TOKEN t;

	memset(&t, 0, sizeof(TOKEN));
	if (type == TokenFloat || type == TokenString || type == TokenIdentifier)
		t.value.s = InternString(lexeme);
	else
		t.value.inum = value;

	t.type = type;
	return t;
}

/* scans end-of-line comments. */
PRIVATE TOKEN ScanComment(void) {

	int c;

	c = FileGet();
	while (TRUE) {
		c = FileGet();
		if (c == '\n' || c == EOF)
			break;
	}

	return InitToken("EOL", TokenEndOfLine, 0);
}

/*
scans numbers of the form:
12345           - Dec
0x12345         - Hex
0X12345         - Hex
0h12345         - Hex
0b10110         - Bin
012345          - Octal
101101b         - Bin
012345h         - Hex

to do:
1010_0010_1001b - Bin
1.234           - Double
1.234f          - Float
123e10          - Exponent
123e+10         - Exponent
123e-10         - Exponent
*/
PRIVATE TOKEN ScanNumber(void) {

	char  str[32];
	char* p;
	int   i;
	int   base;
	BOOL  suffix;
	BOOL  isFloat; /* TRUE if float, FALSE if double. */
	BOOL  isFloatingPoint; /* TRUE if above is valid. */

	p = str;
	suffix = FALSE;
	base = 10;

	isFloat = FALSE;
	isFloatingPoint = FALSE;

	for (i = 0; i < 31; i++) {

		int c = FilePeek();

		if (c == EOF)   break;
		if (c == ';')   break;
		if (isspace(c)) break;
		if (isoper(c))  break;
		if (c == '.')
			isFloatingPoint = TRUE;
		if (isFloatingPoint && c == 'f') {
			isFloat = TRUE;
		}
		str[i] = FileGet();
	}
	str[i] = 0;

	if (isFloatingPoint)
		return InitToken(str, TokenFloat, 0);

	/* check prefix. */
	if (str[0] == '0' && (str[1] == 'x' || str[1] == 'X')) {
		base = 16;
		p += 2;
	}
	else if (str[0] == '0' && str[1] == 'h') {
		base = 16;
		p += 2;
	}
	else if (str[0] == '0' && str[1] == 'b') {
		base = 2;
		p += 2;
	}
	else suffix = TRUE;

	/* check suffix. */
	if (suffix && isalpha(str[i-1])) {
		if (str[i - 1] == 'h')
			base = 16;
		else if (str[i - 1] == 'q')
			base = 8;
		else if (str[i - 1] == 'b')
			base = 2;
		else
			Error("Bad suffix on number");
	}

	return InitToken(str, TokenInteger, strtoul(p, NULL, base));
}

/* scan identifier TOKEN. */
PRIVATE TOKEN ScanIdentifier(void) {

	char str[64];
	int i;
	int modifier;
	TOKEN tok;

	modifier = FilePeek();

	for (i = 0; i < 63; i++) {
		int c = FilePeek();
		if (!isalnum(c) && !isident(c))
			break;
		str[i] = FileGet();
	}
	str[i] = 0;

	tok = InitToken(str, TokenIdentifier, (int)NULL);
	if (i == 62)
		Error("Identifier cannot exceed 62 characters");

	if (modifier != '@') {
		if (!_stricmp(str, "ptr"))
			return InitToken(str, TokenPtr, 0);
		if (!_stricmp(str, "times"))
			return InitToken(str, TokenTimes, 0);
		if (IsPrefix(str) != PrefixInvalid)
			return InitToken(str, TokenPrefix, IsPrefix(str));
		if (IsRegister(str) != RegInvalid)
			return InitToken(str, TokenRegister, IsRegister(str));
		else if (GetMnemonic(str) != MnemonicInvalid)
			return InitToken(FromMnemonic(GetMnemonic(str)), TokenMnemonic, GetMnemonic(str));
		else if (IsSizeSpecifier(str) != SizeInvalid)
			return InitToken(str, TokenSizeSpecifier, IsSizeSpecifier(str));
	}

	return InitToken(str, TokenIdentifier, (int) NULL);
}

/* scan string TOKEN. */
PRIVATE TOKEN ScanString(void) {

	static char str[124];
	int i;
	int term=0;
	TOKEN tok;

	term = FileGet();
	tok = InitToken(str, TokenString, (uint32_t)str);

	for (i = 0; i < 123; i++) {
		int c = FilePeek();
		if (c == term) {
			FileGet();
			break;
		}
		else if (c == '\n' || c == EOF)  {
			Error("Trailing symbol %x", term);
		}
		str[i] = FileGet();
	}
	str[i] = 0;
	return InitToken(str, TokenString, (uint32_t) NULL);
}

/* scan operator TOKEN. */
PRIVATE TOKEN ScanOperator(void) {

	char str[4];
	POPERATOR op;
	int i;

	for (i = 0; i < 1; i++) {
		int c = FilePeek();
		if (!isoper(c))
			break;
		str[i] = FileGet();
	}
	str[i] = 0;
	op = GetOperator(str);
	return InitToken(str, op->type, 0);
}

/* scan new line TOKEN. */
PRIVATE TOKEN ScanNewLine(void) {

	FileGet();
	return InitToken("EOL", TokenEndOfLine, 0);
}

/* scan end of file TOKEN. */
PRIVATE TOKEN ScanEndOfFile(void) {

	return InitToken("EOF", TokenEndOfFile, 0);
}

/* get next TOKEN. */
PRIVATE TOKEN ScanGet(void) {

	int c;
	TOKEN blank;

	/* if there are additional tokens streams to read from: */
	if (VectorLength(_tokenStreams) > 0) {

		/* _tokenStreams is a stack of TokenVector's. */
		TokenVector* v;
	
		/* get the current TOKEN stream. */
		VectorLastElement(_tokenStreams, &v);

		/* if this TOKEN stream is empty, return EOF. */
		if (VectorLength(v) == 0) {
			TOKEN eof;
			eof.type = TokenEndOfFile;
			return eof;
		}

		/* get the next TOKEN from this stream. */
		VectorPop(v, &blank);
		return blank;
	}
	
	/* if there are any tokens in the pushback buffer, get it: */
	if (VectorLength(_pushbackBuffer) > 0) {
		VectorPop(_pushbackBuffer, &blank);
		return blank;
	}

	/* read from primary source file: */
	SkipWhitespace();
	c = FilePeek();
	if (c == EOF)        return ScanEndOfFile();
	else if (c == '\n')  return ScanNewLine();
	else if (c == ';')   return ScanComment();
	else if (c == '/') {
		int k;
		FileGet();
		k = FilePeek();
		if (k == '/') {
			FileUnget('/');
			return ScanComment();
		}
		FileUnget('/');
		return ScanOperator();
	}
	else if (isoper(c))  return ScanOperator();
	else if (c == '@')   return ScanIdentifier();
	else if (c == '.')   return ScanIdentifier();
	else if (c == '_')   return ScanIdentifier();
	else if (c == '?')   return ScanIdentifier();
	else if (isalpha(c)) return ScanIdentifier();
	else if (c == '$') {
		int k;
		FileGet();
		k = FilePeek();
		// $$ operator
		if (k == '$') {
			FileGet();
			return InitToken("$$", TokenSegBase, 0);
		}
		// $<some chars> can be an identifier
		if (isalpha(k) || k == '_' || k == '@' || k == '.')
			return ScanIdentifier();
		// $ operator
		return InitToken("$", TokenHere, 0);
	}
	else if (isdigit(c)) return ScanNumber();
	else if (c == '\"')  return ScanString();
	else if (c == '\'')  return ScanString();
	else {
		Fatal("Scan: Unrecognized symbol '%c'", c);
	}

	memset(&blank, 0, sizeof(TOKEN));
	return blank;
}

/* prepare lexer for scanning. */
PUBLIC void LexInit(void) {

	if (!_pushbackBuffer)
		_pushbackBuffer = NewVector(0, sizeof(TOKEN));
	_pushbackBuffer->elementCount = 0;
	if (!_tokenStreams)
		_tokenStreams = NewVector(0, sizeof(TokenVector*));
}

PUBLIC void FreeLexer(void) {

	FreeVector(_tokenStreams);
	FreeVector(_pushbackBuffer);
}

PUBLIC int Lex(OUT PTOKEN tok) {

	*tok = ScanGet();
	return tok->type;
}

PUBLIC void LexUnget(IN TOKEN token) {

	if (_tokenStreams->elementCount > 0) {

		TokenVector* v;
		VectorLastElement(_tokenStreams, &v);
		VectorPush(v, &token);

		// crashed with %macro stabs (*v points to first string
		// of vector object). keeping here for now just in case:

//		TokenVector** v;
//		VectorLastElement(_tokenStreams, &v);
//		VectorPush(*v, &token);
	}
	else
		VectorPush(_pushbackBuffer, &token);
}

PUBLIC TOKEN LexPeek(IN int count) {

	TOKEN tok[CONTEXT_PUSHBACK_MAX];
	int i;

	assert(count >= 0 && count < CONTEXT_PUSHBACK_MAX);

	for (i = 0; i <= count; i++)
		Lex(&tok[i]);

	for (i = count; i >= 0; i--)
		LexUnget(tok[i]);

	return tok[count];
}

PUBLIC void LexPushStream(IN TokenVector** in) {

	VectorPush(_tokenStreams, in);
}

PUBLIC void LexPopStream(IN TokenVector** in) {

	VectorPop(_tokenStreams, in);
}
