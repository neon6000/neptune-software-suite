/************************************************************************
*
*	gen.c - Code generator
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nasm.h"
#include "instr.h"
#define IN
#define OUT
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

/*
	The Code Generator has the following functions:

		* Pass 0       -- Define Symbols to the Target.
		* Pass CODEGEN -- Write code to the target.

	The Code Generator translates instructions and directives during both passes
	to insure correct offsets are generated for Symbols for Pass 1. This involves
	two scans of the AST.

	The IA32 ISA defines instructions that have the following format.

	+-----------------------------------------------------------------------------------------------+
	| prefix    | REX prefix |     OPCode     | Mod R/M |   SIB  | Displacement   | Immediate       |
	| 1-4 bytes |   1 byte   | 1,2,or 3 bytes |  1 byte | 1 byte | 1,2, or 4 byte | 1,2, or 4 bytes |
	+-----------------------------------------------------------------------------------------------+
	optional     64bit only      required               32 bit only

	2 and 3 byte OPCodes
	---------------------------------

	1. Two byte opcodes always began with 0F.
		+----+--------+
		| 0F | opcode |
`		+----+--------+

	2. Three byte opcodes include a "fixed extraordinary prefix" byte.
	This is a prefix byte that is considered part of the opcode. They
	have the format:
	    +--------+----+--------+
		| prefix | 0F | opcode |
		+--------+----+--------+
	The prefix must be present, although it may be accompanied by other
	prefix bytes.

	OpCode rcode
	-----------------------------
	Instructions with the "rcode" modifier store a register ID
	into the low 3 bits of the opcode byte.

	Mod R/M
	7                           0
	+---+---+---+---+---+---+---+---+
	|  mod  |    reg    |     rm    |
	+---+---+---+---+---+---+---+---+
	2 bits   3 bits       3 bits

	OpCode Extension
	--------------------------------

	1. ModRM.reg contains an opcode extension for all instructions
	that support it.

	SIB
	7                           0
	+---+---+---+---+---+---+---+---+
	| scale |   index   |    base   |
	+---+---+---+---+---+---+---+---+
	2 bits   3 bits       3 bits

	The code generator has two parts:

	1. Translator

		This part translates PARSEDINSTR and PARSEDINSTROP
		into INSTR.

	2. Assembler

		Writes INSTR to the output target. The output target is expected to
		only write the code during the CODEGEN pass. This is also where the
		Listing output gets called if needed.
*/

/* Public Definitions *********************************/

/* These might be referenced from other files for:
	Target file generation
	Llisting file generation
	Expression evaluator (label lookup)
	Error / Warning info
*/
PUBLIC BITS           _mode = Bits32;
PUBLIC PASS           _pass = PASS0;
PUBLIC unsigned long  _offset = 0;
PUBLIC PTARGET        _output;
PUBLIC handle_t       _segment = 0;

/* Private Definitions *******************************/

/* Globals. */
PRIVATE PLISTING          _listing;

/**
*	ModR/M mode values. This describes address formats.
*	MemoryDisp32 and MemoryDisp16 have same value; its
*	meaning depends on operand size.
*/
typedef enum ModRM_Mode {
	ModRM_Memory = 0,                        /* [rm] */
	ModRM_MemoryDisp8,                       /* [rm + disp8] */
	ModRM_MemoryDisp16,                      /* [rm + disp16] */
	ModRM_MemoryDisp32 = ModRM_MemoryDisp16, /* [rm + disp32] */
	ModRM_Register                           /* reg */
}ModRM_Mode;

/**
*	This describes all NASM supported addressing modes.
*/
typedef enum AddressModeType {
	Addr_Imm, /* [immediate] */
	/* 16 bit addressing modes. */
	Addr_Bx_Si, /* [bx+si] */
	Addr_Bx_Di,
	Addr_Bp_Si,
	Addr_Bp_Di,
	Addr_Si, /* [si] */
	Addr_Di,
	Addr_Bp,
	Addr_Bx,
	/* 32/64 bit addressing modes. */
	Addr_Eax,
	Addr_Ecx,
	Addr_Edx,
	Addr_Ebx,
	Addr_Ebp,
	Addr_Esp,
	Addr_Esi,
	Addr_Edi,
	Addr_Sp,
	Addr_SIB,/* [sib] */
	/* 64 bit only */
	Addr_Rax,
	Addr_Rcx,
	Addr_Rdx,
	Addr_Rbx,
	Addr_Rbp,
	Addr_Rsp,
	Addr_Rsi,
	Addr_Rdi,
	Addr_R8,
	Addr_R9,
	Addr_R10,
	Addr_R11,
	Addr_R12,
	Addr_R13,
	Addr_R14,
	Addr_R15,
	Addr_R8d,
	Addr_R9d,
	Addr_R10d,
	Addr_R11d,
	Addr_R12d,
	Addr_R13d,
	Addr_R14d,
	Addr_R15d,
	Addr_Invalid
}AddressModeType;

/**
*	This describes ModRM 16 bit addressing modes.
*/
typedef enum ModRM_AddressMode16 {
	ModRM16_Bx_Si = 0, /* [bx+si] */
	ModRM16_Bx_Di,
	ModRM16_Bp_Si,
	ModRM16_Bp_Di,
	ModRM16_Si,
	ModRM16_Di,
	ModRM16_Bp,    /* mode 0: use imm, mode 1,2 uses bp. */
	ModRM16_Imm = ModRM16_Bp,
	ModRM16_Bx
}ModRM_AddressMode16;

/**
*	This describes ModRM 32 bit addressing modes.
*/
typedef enum ModRM_AddressMode32 {
	ModRM32_Eax = 0, /* [eax] */
	ModRM32_Ecx, /* [ecx] */
	ModRM32_Edx,
	ModRM32_Ebx,
	ModRM32_SIB, /* sib byte follows. */
	ModRM32_Ebp,
	ModRM32_Imm = ModRM32_Ebp,
	ModRM32_Esi,
	ModRM32_Edi
}ModRM_AddressMode32;

/**
*	write
*
*	Description : This procedure writes data to the Target.
*	and List Generator (if pass 1).
*
*	Input : offset - Offset into current segment.
*           segto - Relative segment.
*	        data - Pointer to data to write.
*	        out - Output type.
*	        size - Number of bytes to write.
*	        segment - Not used.
*/
PRIVATE void write(IN offset_t offset, IN int32_t segto, IN void* data,
	IN output_t out, IN size_t size, IN int32_t segment) {

	/* write it to output file. */
	_output->out(_segment, data, out, size, segto, _pass);

	/* if PASSGEN, write it to listing generator. */
	if (_pass == PASSGEN && _listing)
		_listing->out(offset, data, out, size);
}

/**
*	IsPrefix
*
*	Description : Given a value, return TRUE if it is
*	a valid prefix, FALSE otherwise
*
*	Input : val - Value to check
*
*	Return : TRUE if val is a valid prefix
*/
PRIVATE BOOL IsPrefix(IN int val) {

	switch (val) {
	case PrefixClass1_Lock:
	case PrefixClass1_Repne:
	case PrefixClass1_Rep:
	case PrefixClass2_Cs:
	case PrefixClass2_Ss:
	case PrefixClass2_Ds:
	case PrefixClass2_Es:
	case PrefixClass2_Fs:
	case PrefixClass2_Gs:
	case PrefixClass3_Opsize:
	case PrefixClass4_Addrsize:
		return TRUE;
	}
	return FALSE;
}


/**
*	AddPrefix
*
*	Description : Adds a new prefix to the specified instruction. Duplicate
*	prefixes are ignored.
*
*	Input : instr - Instruction to add prefix to.
*	        prefix - Prefix type to add.
*/
PRIVATE void AddPrefix(IN PINSTR instr, IN InternalPrefixType prefix) {

	/* avoid duplicate prefix bytes. */
	for (unsigned int c = 0; c < instr->prefixCount; c++) {
		if (instr->prefix[c] == prefix)
			return;
	}

	/* add it. */
	if (instr->prefixCount < 4)
		instr->prefix[instr->prefixCount++] = prefix;
}

/**
*	GetMemoryOperand
*
*	Description : The x86 Instruction Set only supports at most one memory
*	operand in an instruction. This procedure returns the memory operand
*	or NULL if none.
*
*	Input : instr - Instruction.
*
*	Output : Pointer to the memory operand or NULL.
*/
PRIVATE InstrOperand* GetMemoryOperand(IN PINSTR instr) {

	InstrTableEntry* e;
	unsigned int i;
	
	assert(instr != NULL);

	e = instr->entry;
	for (i = 0; i < e->numParms; i++) {
		if (GetOperandIgnore(e->parms[i]))
			continue;
		if (GetOperandClass(e->parms[i]) == OperandClassMem)
			return &instr->operands[i];
	}
	return NULL;
}

/**
*	GetFirstNonMemoryOperand
*
*	Description : This procedure returns a pointer to the first non-memory operand
*	in a specified instruction or NULL if not found.
*
*	Input : instr - Instruction.
*
*	Output : Pointer to the non-memory operand or NULL.
*/
PRIVATE InstrOperand* GetFirstNonMemoryOperand(IN PINSTR instr) {

	InstrTableEntry* e;
	unsigned int i;

	assert(instr != NULL);

	e = instr->entry;
	for (i = 0; i < e->numParms; i++) {
		if (GetOperandIgnore(e->parms[i]))
			continue;
		if (GetOperandClass(e->parms[i]) != OperandClassMem)
			return &instr->operands[i];
	}
	return NULL;
}

/**
*	GetNextNonMemoryOperand
*
*	Description : This procedure returns a pointer to the next non-memory operand
*	in a specified instruction or NULL if not found.
*
*	Input : instr - Instruction.
*	        last - Pointer to the previous operand (typically returned from
*	        GetFirstNonMemoryOperand).
*
*	Output : Pointer to the non-memory operand or NULL.
*/
PRIVATE InstrOperand* GetNextNonMemoryOperand(IN PINSTR instr, IN InstrOperand* last) {

	InstrTableEntry* e;
	unsigned int i;

	assert(instr != NULL);

	if (!last)
		return GetFirstNonMemoryOperand(instr);

	e = instr->entry;
	for (i = 0; i < e->numParms; i++) {
		if (&instr->operands[i] == last)
			break;
	}
	for (i = i+1; i < e->numParms; i++) {
		if (GetOperandIgnore(e->parms[i]))
			continue;
		if (GetOperandClass(e->parms[i]) != OperandClassMem)
			return &instr->operands[i];
	}
	return NULL;
}

/**
*	GetAddressMode
*
*	Description : Given a memory operand, this procedure attempts to
*	              get the address mode that best matches it.
*
*	Input : mem - Pointer to a memory operand.
*
*	Output : Address mode type for the memory operand or Addr_Invalid
*	if the operand is not a memory type.
*/
PRIVATE AddressModeType GetAddressMode(IN InstrOperand* mem) {

	if (mem == NULL)
		Fatal("*** BUGCHECK: GetAddressMode mem = NULL!");

	assert(mem != NULL);

	if (GetOperandClass(mem->type) != OperandClassMem)
		return Addr_Invalid;

	if (mem->memory.base && mem->memory.index) {
		if (mem->memory.base->type == RegBx && mem->memory.index->type == RegSi) return Addr_Bx_Si;
		else if (mem->memory.base->type == RegBx && mem->memory.index->type == RegDi) return Addr_Bx_Di;
		else if (mem->memory.base->type == RegBp && mem->memory.index->type == RegSi) return Addr_Bp_Si;
		else if (mem->memory.base->type == RegBp && mem->memory.index->type == RegDi) return Addr_Bp_Di;
		else return Addr_SIB;
	}
	else if (mem->memory.base) {
		if (mem->memory.base->type == RegSi) return Addr_Si;

		else if (mem->memory.base->type == RegDi) return Addr_Di;
		else if (mem->memory.base->type == RegBp) return Addr_Bp;
		else if (mem->memory.base->type == RegBx) return Addr_Bx;

		else if (mem->memory.base->type == RegEax) return Addr_Eax;
		else if (mem->memory.base->type == RegRax) return Addr_Rax;
		else if (mem->memory.base->type == RegR8d) return Addr_R8d;
		else if (mem->memory.base->type == RegR8) return Addr_R8;

		else if (mem->memory.base->type == RegEcx) return Addr_Ecx;
		else if (mem->memory.base->type == RegRcx) return Addr_Rcx;
		else if (mem->memory.base->type == RegR9d) return Addr_R9d;
		else if (mem->memory.base->type == RegR9) return Addr_R9;

		else if (mem->memory.base->type == RegEdx) return Addr_Edx;
		else if (mem->memory.base->type == RegRdx) return Addr_Rdx;
		else if (mem->memory.base->type == RegR10d) return Addr_R10d;
		else if (mem->memory.base->type == RegR10) return Addr_R10;

		else if (mem->memory.base->type == RegEbx) return Addr_Ebx;
		else if (mem->memory.base->type == RegRbx) return Addr_Rbx;
		else if (mem->memory.base->type == RegR11d) return Addr_R11d;
		else if (mem->memory.base->type == RegR11) return Addr_R11;

		else if (mem->memory.base->type == RegEbp) return Addr_Ebp;
		else if (mem->memory.base->type == RegRbp) return Addr_Rbp;
		else if (mem->memory.base->type == RegR13d) return Addr_R13d;
		else if (mem->memory.base->type == RegR13) return Addr_R13;

		else if (mem->memory.base->type == RegEsp) return Addr_Esp;
		else if (mem->memory.base->type == RegRsp) return Addr_Rsp;
		else if (mem->memory.base->type == RegR12d) return Addr_R12d;
		else if (mem->memory.base->type == RegR12) return Addr_R12;

		else if (mem->memory.base->type == RegEsi) return Addr_Esi;
		else if (mem->memory.base->type == RegRsi) return Addr_Rsi;
		else if (mem->memory.base->type == RegR14d) return Addr_R14d;
		else if (mem->memory.base->type == RegR14) return Addr_R14;

		else if (mem->memory.base->type == RegEdi) return Addr_Edi;
		else if (mem->memory.base->type == RegRdi) return Addr_Rdi;
		else if (mem->memory.base->type == RegR15d) return Addr_R15d;
		else if (mem->memory.base->type == RegR15) return Addr_R15;

		else {
			Fatal("GetAddressMode: Unknown mode type");
		}
	}
	return Addr_Imm;
}

/**
*	GetAddrOperationMode
*
*	Description : This procedure gets the Address Size for the specified
*	address mode.
*
*	Input : mode - Address mode type.
*
*	Output : Bits16, Bits32, Bits64 depending on the address mode type.
*/
PRIVATE BITS GetAddrOperationMode(IN AddressModeType mode) {

	BITS m;

	switch (mode) {
	case Addr_Bx_Si:
	case Addr_Bx_Di:
	case Addr_Bp_Si:
	case Addr_Bp_Di:
	case Addr_Si:
	case Addr_Di:
	case Addr_Bp:
	case Addr_Bx:
		m = Bits16; /* 16 bit addressing mode. */
		break;
	case Addr_Imm:
		return _mode; /* [imm] */
	case Addr_Invalid:
		return _mode; /* not a memory operand. */
	case Addr_Eax:
	case Addr_Ecx:
	case Addr_Edx:
	case Addr_Ebx:
	case Addr_Ebp:
	case Addr_Esp:
	case Addr_Esi:
	case Addr_Edi:
	case Addr_Sp:
	case Addr_SIB: // FIXME: size is dependent on base reg
		m = Bits32;
		break;
	case Addr_R8d: /* extended 32 bit registers */
	case Addr_R9d:
	case Addr_R10d:
	case Addr_R11d:
	case Addr_R12d:
	case Addr_R13d:
	case Addr_R14d:
	case Addr_R15d:
		m = Bits32;
		break;
	default:
		m = Bits64;
	};
	return m;
}

/**
*	UpdateOpcodeRegisterField
*
*	Description : Instructions with the ModRegOpcode modifier store
*	a register id of the first operand in the opcode byte. This procedure
*	modifies the opcode byte to add the register code if needed.
*
*	Input : instr - Pointer to instruction to modify.
*/
PRIVATE void UpdateOpcodeRegisterField(IN PINSTR instr) {

	InstrOperand* op;

	if (!(instr->modifier & ModRegOpcode))
		return;

	op = GetFirstNonMemoryOperand(instr);

	if (!op)
		Fatal("*** BUGCHECK: UpdateOpcodeRegisterCode with no non-mem operand!");

	if (!op->reg)
		Fatal("*** BUGCHECK: UpdateOpcodeRegisterCode with no non-register operand!");

	instr->opcode |= op->reg->id;
}

/**
*	UpdateOpcodeExtension
*
*	Description : Instructions with an opcode extension require writing
*	the opcode extension to the ModRM.reg field. This procedure modifies
*	ModRM.reg to the opcode extension if needed.
*
*	Input : instr - Pointer to instruction to modify.
*/
PRIVATE void UpdateOpcodeExtension(IN PINSTR instr) {

	if (instr->opcodeExt == OpcodeExtNone)
		return;

	if (instr->modRM.reg > 0)
		Fatal("*** BUGCHECK: UpdateOpcodeExtension with modMR.reg > 0!");

	instr->modRM.reg = instr->opcodeExt;
}

/**
*	AddOpsizeOverride
*
*	Description : This procedure attempts to add an operand size override
*	              prefix if needed.
*
*	Input : instr - Pointer to instruction to modify.
*/
PRIVATE void AddOpsizeOverride(IN PINSTR instr) {

	if (_mode == Bits64 && (instr->modifier & ModSizeWord))
		AddPrefix(instr, PrefixClass3_Opsize);

	else if (_mode == Bits32 && (instr->modifier & ModSizeWord))
		AddPrefix(instr, PrefixClass3_Opsize);

	else if (_mode == Bits16 && (instr->modifier & ModSizeDword))
		AddPrefix(instr, PrefixClass3_Opsize);
}

/**
*	AddAddrOverride
*
*	Description : This procedure attempts to add an address size override
*	              prefix if needed. There are two cases here:
*	              Case 1 - Explicate address size overrides (for example, "a32 mov eax, [0]")
*	              Case 2 - Using a 16 bit address mode in 32 bit mode or vice versa.
*
*	Input : instr - Pointer to instruction to modify.
*/
PRIVATE void AddAddrOverride(IN PINSTR instr) {

	BITS m;
	InstrOperand* regMem;
	AddressModeType addrMode;

	assert(instr != NULL);

	//
	// case 1 - Explicate address size override.
	//

	if ((instr->modifier & ModAddrSizeDword) && _mode == Bits16)
		AddPrefix(instr, PrefixClass4_Addrsize);
	else if ((instr->modifier & ModAddrSizeWord) && _mode == Bits32)
		AddPrefix(instr, PrefixClass4_Addrsize);

	else{

		//
		// case 2 - Memory address mode.
		//

		regMem = GetMemoryOperand(instr);
		if (!regMem)
			return;

		addrMode = GetAddressMode(regMem);
		m = GetAddrOperationMode(addrMode);

		// addrMode [SIB] is Bits32

		if (_mode == Bits64 && m == Bits32)
			AddPrefix(instr, PrefixClass4_Addrsize);
		else if (_mode == Bits16 && m == Bits32)
			AddPrefix(instr, PrefixClass4_Addrsize);
		else if (_mode == Bits32 && m == Bits16)
			AddPrefix(instr, PrefixClass4_Addrsize);
		else if (_mode == Bits64 && m == Bits16)
			AddPrefix(instr, PrefixClass4_Addrsize);
	}
}

/**
*	BuildModRM16
*
*	Description : Given a completed instruction, this attempts
*	              to build the ModR/M byte for 16 bit addressing modes.
*
*	Input : instr - Pointer to instruction to modify.
*/
PRIVATE void BuildModRM16(IN PINSTR instr) {

	InstrOperand*    regMem;
	InstrOperand*    reg;
	AddressModeType  mode;

	assert(instr != NULL);

	reg = NULL;
	mode = Addr_Invalid;

	regMem = GetMemoryOperand(instr);

	if (instr->opcodeExt == OpcodeExtNone)
		reg = GetFirstNonMemoryOperand(instr);

	if (regMem)
		mode = GetAddressMode(regMem);
	else
		regMem = GetNextNonMemoryOperand(instr, reg);

	instr->modRM.mode = 0;
	instr->modRM.reg = 0;
	instr->modRM.regMem = 0;

	switch (mode) {
	case Addr_Bx_Si: instr->modRM.regMem = ModRM16_Bx_Si; break;
	case Addr_Bx_Di: instr->modRM.regMem = ModRM16_Bx_Di; break;
	case Addr_Bp_Si: instr->modRM.regMem = ModRM16_Bp_Si; break;
	case Addr_Bp_Di: instr->modRM.regMem = ModRM16_Bp_Di; break;
	case Addr_Si: instr->modRM.regMem = ModRM16_Si; break;
	case Addr_Di: instr->modRM.regMem = ModRM16_Di; break;
	case Addr_Bp: instr->modRM.regMem = ModRM16_Bp; break;
	case Addr_Bx: instr->modRM.regMem = ModRM16_Bx; break;
	case Addr_Invalid:
		/* this is not a memory operand. */
		if (regMem) {
			instr->modRM.regMem = regMem->reg ? regMem->reg->id : 0;
		}
		break;
	case Addr_Imm:
		instr->modRM.regMem = ModRM16_Imm;
		instr->modRM.mode = ModRM_Memory;
	};

	if (reg && reg->reg)
		instr->modRM.reg = reg->reg->id;

	if (regMem && regMem->reg)
		instr->modRM.mode = ModRM_Register;

	if (regMem && mode != Addr_Imm && regMem->reg == NULL) {

		if (regMem->disp == Disp32)
			instr->modRM.mode = ModRM_MemoryDisp32;
		else if (regMem->disp == Disp16)
			instr->modRM.mode = ModRM_MemoryDisp16;
		else if (regMem->disp == Disp8)
			instr->modRM.mode = ModRM_MemoryDisp8;
		else
			instr->modRM.mode = ModRM_Memory;
	}
	instr->modifier |= ModMODRM;
}

/**
*	BuildSIB
*
*	Description : Given a completed instruction, this attempts
*	              to build the SIB byte for 32 bit addressing modes.
*
*	Input : instr - Pointer to instruction to modify.
*/
PRIVATE void BuildSIB(IN PINSTR instr) {

	InstrOperand*   regMem;
	AddressModeType mode;

	assert(instr != NULL);

	if ((instr->modifier & ModSIB) != ModSIB)
		return;

	regMem = GetMemoryOperand(instr);

	assert(regMem != NULL);

	mode = GetAddressMode(regMem);

	instr->sib.base = regMem->memory.base->id;
	instr->sib.index = 4; /* 4 = No index register. */

	if (regMem->memory.index) {
		if (regMem->memory.index->id == 4)
			Error("%s cannot be used as an index register", regMem->memory.index->name);
		else
			instr->sib.index = regMem->memory.index->id;
	}

	switch (regMem->memory.scale) {
	case 0:
	case 1: instr->sib.scale = 0; break;
	case 2: instr->sib.scale = 1; break;
	case 4: instr->sib.scale = 2; break;
	case 8: instr->sib.scale = 3; break;
	default:
		Error("Invalid scale factor %i", regMem->memory.scale);
	};
}

/**
*	BuildModRM32
*
*	Description : Given a completed instruction, this
*	              attempts to build the ModRM byte for
*	              32 bit addressing modes.
*
*	Input : instr - Pointer to instruction to modify.
*/
PRIVATE void BuildModRM32(IN PINSTR instr) {

	InstrOperand*   regMem;
	InstrOperand*   reg;
	AddressModeType mode;
	Register*       mem;

	assert(instr != NULL);

	mem = NULL;
	reg = NULL;
	mode = Addr_Invalid;

	regMem = GetMemoryOperand(instr);

	if (instr->opcodeExt == OpcodeExtNone)
		reg = GetFirstNonMemoryOperand(instr);

	if (regMem) {
		mode = GetAddressMode(regMem);
		mem = regMem->memory.base;
	}
	else
		regMem = GetNextNonMemoryOperand(instr, reg);

	instr->modRM.mode = 0;
	instr->modRM.reg = 0;
	instr->modRM.regMem = 0;

	switch (mode) {

	// [ESP]
	case Addr_Rsp:
	case Addr_R12:
	case Addr_R12d:
	case Addr_Esp:
		instr->modRM.regMem = mem->id;
		instr->modifier |= ModSIB;
		break;

	// [SIB]
	case Addr_SIB:
		instr->modRM.regMem = ModRM32_SIB;
		instr->modifier |= ModSIB;
		break;

	// register (non-memory)
	case Addr_Invalid:

		/* this is not a memory operand. */
		if (regMem) {
			instr->modRM.regMem = regMem->reg ? regMem->reg->id : 0;
		}
		break;

	// [IMM]
	case Addr_Imm:
		instr->modRM.regMem = ModRM32_Imm;
		instr->modRM.mode = ModRM_Memory;
		break;

	// [REG]
	default:
		instr->modRM.regMem = mem->id;
	};

	if (reg && reg->reg)
		instr->modRM.reg = reg->reg->id;

	if (regMem && regMem->reg)
		instr->modRM.mode = ModRM_Register;

	if (mode != Addr_Imm && regMem && regMem->reg == NULL) {

		if (regMem->disp == Disp32)
			instr->modRM.mode = ModRM_MemoryDisp32;
		else if (regMem->disp == Disp16)
			instr->modRM.mode = ModRM_MemoryDisp16;
		else if (regMem->disp == Disp8)
			instr->modRM.mode = ModRM_MemoryDisp8;
		else
			instr->modRM.mode = ModRM_Memory;
	}

	instr->modifier |= ModMODRM;
}

/**
*	BuildModRM
*
*	Description : Given a completed instruction, this attempts
*	              to build the ModRM byte.
*
*	Input : instr - Pointer to instruction to modify.
*/
PRIVATE void BuildModRM(IN PINSTR instr) {

	InstrOperand* mem;
	BITS m;

	//
	// if there is an opcode extension but no operands,
	// we still need to generate ModRM and set its mode
	//
	if (instr->operandCount == 0 && instr->opcodeExt != OpcodeExtNone) {

		instr->modRM.mode = ModRM_Register;
		instr->modifier |= ModMODRM;
		return;
	}

	//
	// get addressing mode
	//
	mem = GetMemoryOperand(instr);
	if (!mem)
		m = _mode;
	else
		m = GetAddrOperationMode(GetAddressMode(mem));

	//
	// build ModRM based on addressing mode
	//
	switch (m) {
	case Bits16:
		BuildModRM16(instr);
		break;
	default:
		BuildModRM32(instr);
	};
}

/**
*	GetOpcodeSize
*
*	Description : Given an opcode, this procedure returns
*	              the number of bytes required for storing it.
*
*	Input : opcode - Opcode.
*
*	Output : Number of bytes required by the opcode.
*/
PRIVATE size_t GetOpcodeSize(IN unsigned int opcode) {

	if (opcode <= 0xff)
		return 1;
	else if (opcode > 0xff && opcode <= 0xffff)
		return 2;
	else
		return 3;
}

/**
*	WritePrefix
*
*	Description : Given an instruction, this writes
*	              the prefix bytes to the Target.
*
*	Input : instr - Pointer to instruction.
*
*	Output : Number of bytes written to Target.
*/
PRIVATE size_t WritePrefix(IN PINSTR instr) {

	size_t c = 0;
	for (c = 0; c < instr->prefixCount; c++) 
		write(_offset, 0, &instr->prefix[c], WRITE_RAWDATA, 1, 0);
	return c;
}

/**
*	SwapBytes2
*
*	Description : This procuedure swaps the low and high bytes.
*
*	Input : in - Value.
*
*	Output : Value with low and high bytes swapped.
*/
PRIVATE uint16_t SwapBytes2(IN uint16_t in) {

	return ((in & 0xff00) >> 8) | ((in & 0xff) << 8);
}

/**
*	WriteOpcode
*
*	Description : Given an instruction, this writes
*	              the opcode bytes to the Target.
*
*	Input : instr - Pointer to instruction.
*
*	Output : Number of bytes written to Target.
*/
PRIVATE size_t WriteOpcode(IN PINSTR instr) {

	size_t c = GetOpcodeSize(instr->opcode);
	uint32_t opcode;

	switch (c) {
		case 1: opcode = instr->opcode; break;
		case 2: opcode = SwapBytes2(instr->opcode); break;
		case 3: opcode = instr->opcode; break;
	};

	write(_offset, 0, &opcode, WRITE_RAWDATA, c, 0);
	return c;
}

/**
*	WriteRex
*
*	Description : Given an instruction, this writes
*	              the REX prefix byte, if needed.
*
*	Input : instr - Pointer to instruction.
*
*	Output : Number of bytes written to Target.
*/
PRIVATE size_t WriteRex(IN PINSTR instr) {

	//
	// -  bits 7-4: 0100
	// W: bit 3: 0: Operand size determined by CS.d, 1 = 64 bit
	// R: bit 2: Extension of ModRM.reg
	// X: bit 1: Extension of SIB.index
	// B: bit 0: Extension of ModRM.rm, SIB.base, or Opcode.reg
	//
#define REX_RESBIT 0x40
#define REX_64BIT 8

	uint8_t     value;

	// is this right? IRETQ needs REX byte but has no operands

	//	if (instr->operandCount == 0) return 0;

	// set REX.W
	value = 0;
	if (instr->operandCount == 0 && (instr->modifier & ModSizeQword))
		value = REX_64BIT;
	if (instr->operandCount == 1 && GetOperandSize(instr->operands[0].type) == OperandSize64)
		value = REX_64BIT;
	else if (instr->operandCount == 2 && (GetOperandSize(instr->operands[0].type) == OperandSize64)
		|| (GetOperandSize(instr->operands[0].type) == OperandSize64)) {
		value = REX_64BIT;
	}

	// set REX.R and REX.X
	value |= (instr->modRM.reg >> 3) << 2;
	value |= (instr->sib.index >> 3) << 1;

	// set REX.B
	if (instr->modifier & ModSIB)
		value |= instr->sib.base >> 3;
	else if (instr->modifier & ModMODRM)
		value |= instr->modRM.regMem >> 3;
	else if (instr->modifier & ModEXT)
		value |= instr->opcodeExt >> 3;

	if (value > 0) {
		value |= REX_RESBIT;
		write(_offset, 0, &value, WRITE_RAWDATA, 1, 0);
		return 1;
	}
	return 0;
}

/**
*	WriteModRM
*
*	Description : Given an instruction, this writes
*	              the ModRM byte to the Target if needed.
*
*	Input : instr - Pointer to instruction.
*
*	Output : Number of bytes written to Target.
*/
PRIVATE size_t WriteModRM(IN PINSTR instr) {
	
	ModRM* modrm = &instr->modRM;
	uint8_t value;
	if (instr->modifier & ModMODRM) {
		value = ((modrm->mode & 0x3) << 6) | ((modrm->reg & 0x7) << 3) | (modrm->regMem & 0x7);
		write(_offset, 0, &value, WRITE_RAWDATA, 1, 0);
		return 1;
	}
	return 0;
}

/**
*	WriteSIB
*
*	Description : Given an instruction, this writes
*	              the SIB byte to the Target if needed.
*
*	Input : instr - Pointer to instruction.
*
*	Output : Number of bytes written to Target.
*/
PRIVATE size_t WriteSIB(IN PINSTR instr) {

	ScaleIndexBase* sib = &instr->sib;
	uint8_t value;
	if (instr->modifier & ModSIB) {
		value = ((sib->scale & 0x3) << 6) | ((sib->index & 0x7) << 3) | (sib->base & 0x7);
		write(_offset, 0, &value, WRITE_RAWDATA, 1, 0);
		return 1;
	}
	return 0;
}

/**
*	WriteDisp
*
*	Description : Given an instruction, this writes
*	              the Displacement bytes to the Target
*	              if needed.
*
*	Input : instr - Pointer to instruction.
*
*	Output : Number of bytes written to Target.
*/
PRIVATE size_t WriteDisp(IN PINSTR instr) {

	InstrOperand*   mem;
	BITS            m;
	AddressModeType mode;
	output_t        out;

	out = WRITE_RAWDATA;

	mem = GetMemoryOperand(instr);
	if (!mem)
		return 0;

	/* if there is no displacement, nothing to do. */
	if (mem->disp == DispNone)
		return 0;

	mode = GetAddressMode(mem);
	m = GetAddrOperationMode(mode);

	/* if this displacement references a symbol, we need
	to generate a relocation. */
	if (mem->segto != ABS_SEG && mem->segto != NO_SEG)
		out = WRITE_ADDRESS;

	switch (instr->modRM.mode) {
	case ModRM_Memory:
		if (_mode == Bits16) {
			write(_offset, mem->segto, &mem->memory.displacement, out, 2, _segment);
			return 2;
		}
		else if (_mode == Bits32) {
			write(_offset, mem->segto, &mem->memory.displacement, out, 4, _segment);
			return 4;
		}
	case ModRM_MemoryDisp8:
		write(_offset, mem->segto, &mem->memory.displacement, out, 1, _segment);
		return 1;
/*	case ModRM_MemoryDisp16: */
	case ModRM_MemoryDisp32:
		if (m == Bits16) {
			write(_offset, mem->segto, &mem->memory.displacement, out, 2, _segment);
			return 2;
		}
		else if (m == Bits32) {
			write(_offset, mem->segto, &mem->memory.displacement, out, 4, _segment);
			return 4;
		}
	}

	return 0;
}

/**
*	WriteImmPtr
*
*	Description : Given an instruction, this writes
*	              the ptr16:16 or ptr16:32 bytes to
*	              the Target if needed.
*
*	Input : instr - Pointer to instruction.
*	        imm - Pointer to the immediate operand.
*
*	Output : Number of bytes written to Target.
*/
PRIVATE size_t WriteImmPtr(IN PINSTR instr, IN InstrOperand* imm) {

	OperandType t = imm->type;
	OperandSize s = GetOperandSize(t);
	output_t out  = WRITE_RAWDATA;

	/* if this references a symbol, we need to create a relocation. */
	if (imm->segto != NO_SEG && imm->segto != ABS_SEG)
		out = WRITE_ADDRESS;

	switch (s) {
	case OperandSize16:
		write(_offset, imm->segto, &imm->val.ival, out, 2, _segment);
		write(_offset, imm->segto, &instr->farPtrSeg, WRITE_RAWDATA, 2, _segment);
		return 4;
	case OperandSize32:
		write(_offset, imm->segto, &imm->val.ival, out, 4, _segment);
		write(_offset, imm->segto, &instr->farPtrSeg, WRITE_RAWDATA, 2, _segment);
		return 6;
	};

	return 0;
}

/**
*	WriteImmRel
*
*	Description : Given an instruction, this writes
*	              the rel8, rel16, rel32 bytes to
*	              the Target if needed.
*
*	Input : instr - Pointer to instruction.
*	        imm - Pointer to the immediate operand.
*
*	Output : Number of bytes written to Target.
*/
PRIVATE size_t WriteImmRel(IN PINSTR instr, IN InstrOperand* imm) {

	OperandType t = imm->type;
	OperandSize s = GetOperandSize(t);
	int size;
	int realaddr;

	switch (s) {
	case OperandSize8: size = 1; break;
	case OperandSize16: size = 2; break;
	case OperandSize32: size = 4; break;
	};

	/* this is the relative value to write. */
	realaddr = imm->val.ival - (_offset + size + 1);

	switch (s) {
	case OperandSize8:
		if (_segment == imm->segto || imm->segto == NO_SEG)
			write(_offset, imm->segto, &realaddr, WRITE_RAWDATA, 1, _segment);
		else
			write(_offset, imm->segto, &imm->val.ival, WRITE_REL1ADR, 1, _segment);
		return 1;
	case OperandSize16:
		if (_segment == imm->segto || imm->segto == NO_SEG)
			write(_offset, imm->segto, &realaddr, WRITE_RAWDATA, 2, _segment);
		else
			write(_offset, imm->segto, &imm->val.ival, WRITE_REL2ADR, 2, _segment);
		return 2;
	case OperandSize32:
		if (_segment == imm->segto || imm->segto == NO_SEG)
			write(_offset, imm->segto, &realaddr, WRITE_RAWDATA, 4, _segment);
		else
			write(_offset, imm->segto, &imm->val.ival, WRITE_REL4ADR, 4, _segment);
		return 4;
	};

	return 0;
}

/**
*	WriteImm
*
*	Description : Writes IMM part of bytecode to TARGET
*
*	Input : instr - Instruction being written
*
*	Output : Number of bytes written to TARGET
*/
PRIVATE size_t WriteImm(IN PINSTR instr) {

	InstrOperand* a;
	InstrOperand* b;
	InstrOperand* c;
	InstrOperand* mem;
	InstrOperand* imm;
	size_t s;

	a = GetFirstNonMemoryOperand(instr);
	b = GetNextNonMemoryOperand(instr, a);
	c = GetNextNonMemoryOperand(instr, b);
	mem = GetMemoryOperand(instr);

	/* get the imm operand, if any. */
	if (a && GetOperandClass(a->type) == OperandClassImm) imm = a;
	else if (b && GetOperandClass(b->type) == OperandClassImm) imm = b;
	else if (c && GetOperandClass(c->type) == OperandClassImm) imm = c;
	/* [imm] forms. */
//	else if (mem && GetOperandClass(mem->type) == OperandClassMem && mem->disp == DispNone && !mem->memory.base) imm = mem;
	else return 0;

	if (GetOperandClass(imm->type) == OperandClassImm) {

		if ((GetOperandSubclass(imm->type) & OperandImmRel) == OperandImmRel)
			return WriteImmRel(instr, imm);
		else if ((GetOperandSubclass(imm->type) & OperandImmPtr) == OperandImmPtr)
			return WriteImmPtr(instr, imm);

		switch (GetOperandSize(imm->type)) {
		case OperandSize8: s = 1; break;
		case OperandSize16: s = 2; break;
		case OperandSize32: s = 4; break;
		case OperandSize64: s = 8; break;
		default:
			Fatal("*** BUGCHECK: WriteImm invalid operand size");
		};
	}
	else if (GetOperandClass(imm->type) == OperandClassMem) {
		s = _mode == Bits16 ? 2 : 4;
	}

	if (imm->segto != NO_SEG && imm->segto != ABS_SEG)
		write(_offset, imm->segto, &imm->val.ival, WRITE_ADDRESS, s, _segment);
	else
		write(_offset, imm->segto, &imm->val.ival, WRITE_RAWDATA, s, _segment);

	return s;
}

/**
*	Assemble
*
*	Description : Writes the bytecode of "instr"
*	into TARGET
*
*	Input : instr - Instrution to write
*
*	Output : Number of bytes written
*/
PRIVATE size_t Assemble(IN PINSTR instr) {

	size_t c = 0;

	c += WritePrefix(instr);
	if (_mode == Bits64)
		c += WriteRex(instr);
	c += WriteOpcode(instr);

	if ((instr->modifier & ModEmitOpcodeOnly) == 0) {
		c += WriteModRM(instr);
		c += WriteSIB(instr);
		c += WriteDisp(instr);
		c += WriteImm(instr);
	}
	return c;
}

/* front end */

/**
*	PromoteOperandSize
*
*	Description : Given an instruction, this sets
*	the size of each operand based on the following:
*		1. If a size override is provided (byte, word, dword)
*		the type is promoted to the requested size.
*		2. If this is an IMM value but no size override,
*		we determine the size from the smallest size that
*		will fit within the IMM value.
*		3. If this is an IMM value that is relocatable, it
*		will always be 16 or 32 bits depending on current BITS
*		mode (the expected address size).
*/
PRIVATE void PromoteOperandSize(IN PINSTR i) {

	InstrOperand* a = &i->operands[0];
	InstrOperand* b = &i->operands[1];
	InstrOperand* c = &i->operands[2];

	if (i->operandCount == 1) {

		/* watch for size overrides. */
		i->opsize = a->sizeover;

		/* if this is a memory operand and a size specifier is
		defined, set its size. */
		if (GetOperandClass(a->type) == OperandClassMem) {
			if (a->sizeover != SizeInvalid) {
				if (a->sizeover == SizeByte) a->type |= OperandSize8;
				else if (a->sizeover == SizeWord) a->type |= OperandSize16;
				else if (a->sizeover == SizeDword) a->type |= OperandSize32;
				else if (a->sizeover == SizeTbyte) a->type |= OperandSize80;
				else a->type |= OperandSize64;
			}
		}

		/* if this is an immediate value, we promote the size
		to the size override (if any.) */
		else if (GetOperandClass(a->type) == OperandClassImm) {

			if (a->sizeover == SizeInvalid) {

				if (a->segto != NO_SEG || i->farPtrSeg != InvalidFarPtrSegment) {
					if (_mode == Bits16) a->type |= OperandSize16;
					else a->type |= OperandSize32;
				}

				else if (a->val.ival <= 0xff)  a->type |= OperandSize8;
				else if (a->val.ival <= 0xffff) a->type |= OperandSize16;
				else a->type |= OperandSize32;
			}
			else{
				if (a->sizeover == SizeByte) a->type |= OperandSize8;
				else if (a->sizeover == SizeWord) a->type |= OperandSize16;
				else if (a->sizeover == SizeDword) a->type |= OperandSize32;
				else if (a->sizeover == SizeTbyte) a->type |= OperandSize80;
				else a->type |= OperandSize64;
			}

			if (((a->type & OperandSize8) && a->val.ival > 0xff)
				|| ((a->type & OperandSize16) && a->val.ival > 0xffff)) {
					Warn("Immediate value exceeds bounds");
			}
		}
	}

	if (i->operandCount == 2) {

		/* watch for size overrides. */
		if (a->sizeover != SizeInvalid && b->sizeover != SizeInvalid) {
			if (a->sizeover != b->sizeover)
				Fatal("PromoteOperandSize mismatch in operand sizes");
			i->opsize = a->sizeover;
		}
		else if (a->sizeover != SizeInvalid)
			i->opsize = a->sizeover;
		else if (b->sizeover != SizeInvalid)
			i->opsize = b->sizeover;
		else
			i->opsize = SizeInvalid;

		/* if one operand is a register and the second operand is
		a memory reference, apply the size overrides if any. */

		if (GetOperandClass(a->type) == OperandClassReg && GetOperandClass(b->type) == OperandClassMem) {

			if (b->sizeover == SizeByte) b->type |= OperandSize8;
			else if (b->sizeover == SizeWord) b->type |= OperandSize16;
			else if (b->sizeover == SizeDword) b->type |= OperandSize32;
			else if (b->sizeover == SizeQword) b->type |= OperandSize64;
			else if (a->sizeover == SizeTbyte) a->type |= OperandSize80;
		}
		else if (GetOperandClass(b->type) == OperandClassReg && GetOperandClass(a->type) == OperandClassMem) {

			if (a->sizeover == SizeByte) b->type |= OperandSize8;
			else if (a->sizeover == SizeWord) b->type |= OperandSize16;
			else if (a->sizeover == SizeDword) b->type |= OperandSize32;
			else if (b->sizeover == SizeQword) b->type |= OperandSize64;
			else if (a->sizeover == SizeTbyte) a->type |= OperandSize80;
		}

		/* if one operand is a register, and the second operand is
		an immediate value, promote the size of the immediate operand
		to the "smallest" possible size. */

		if (GetOperandClass(a->type) == OperandClassReg && GetOperandClass(b->type) == OperandClassImm) {

			if (b->sizeover == SizeInvalid) {

				if (b->segto != NO_SEG) {
					if (_mode == Bits16) b->type |= OperandSize16;
					else b->type |= OperandSize32;
				}

				else if (b->val.ival <= 0xff) b->type |= OperandSize8;
				else if (b->val.ival <= 0xffff) b->type |= OperandSize16;
				else b->type |= OperandSize32;
			}
			else{
				if (b->sizeover == SizeByte) b->type |= OperandSize8;
				else if (b->sizeover == SizeWord) b->type |= OperandSize16;
				else if (b->sizeover == SizeDword) b->type |= OperandSize32;
				else if (b->sizeover == SizeQword) b->type |= OperandSize64;
				else if (a->sizeover == SizeTbyte) a->type |= OperandSize80;
			}

			if (((b->type & OperandSize8) && a->val.ival > 0xff)
				|| ((b->type & OperandSize16) && a->val.ival > 0xffff)) {
				Warn("Immediate value exceeds bounds");
			}
		}

		/* if both operands are registers, they must be same size. */
		if (GetOperandClass(a->type) == OperandClassReg && GetOperandClass(b->type) == OperandClassReg) {

			if (GetOperandSize(a->reg->flags) != GetOperandSize(a->reg->flags))
				Error("Size mismatch error");
		}

		/* if one operand is a memory operand and the other is an immediate value,
		an operand size override must be present. If it is present on both operands,
		then it must be the same size. */

		if (GetOperandClass(a->type) == OperandClassMem && GetOperandClass(b->type) == OperandClassImm) {

			if (a->sizeover != SizeInvalid && b->sizeover == SizeInvalid)
				i->opsize = a->sizeover;
			else if (a->sizeover == SizeInvalid && b->sizeover != SizeInvalid)
				i->opsize = b->sizeover;
			else if (a->sizeover != SizeInvalid && b->sizeover != SizeInvalid) {
				if (a->sizeover != b->sizeover)
					Error("Size mismatch error");
				else
					i->opsize = b->sizeover;
			}

			if (a->sizeover == SizeInvalid && b->sizeover == SizeInvalid) {

				if (a->segto != NO_SEG) {

					if (_mode == Bits16) {
						a->type |= OperandSize16;
						b->type |= OperandSize16;
					}
					else {
						a->type |= OperandSize32;
						b->type |= OperandSize32;
					}
				}
				else
					Error("Instruction requires operand size specifier");
			}
			else{
				switch (i->opsize) {
				case SizeByte:
					a->type |= OperandSize8;
					b->type |= OperandSize8;
					break;
				case SizeWord:
					a->type |= OperandSize16;
					b->type |= OperandSize16;
					break;
				case SizeDword:
					a->type |= OperandSize32;
					b->type |= OperandSize32;
					break;
				case SizeQword:
					a->type |= OperandSize64;
					b->type |= OperandSize64;
					break;
				default:
					Error("Unsupported operand size");
				};
			}
		}
	}

	if (i->operandCount == 3) {

		/* the only instruction currently supported is IMUL.
		For this instruction, the third operand must be an imm.
		Because of this, we don't need to look at other operands here:*/
		if (GetOperandClass(c->type) == OperandClassImm) {

			if (c->sizeover == SizeInvalid) {

				if (c->segto != NO_SEG) {
					if (_mode == Bits16) c->type |= OperandSize16;
					else c->type |= OperandSize32;
				}

				else if (c->val.ival <= 0xff)  c->type |= OperandSize8;
				else if (c->val.ival <= 0xffff) c->type |= OperandSize16;
				else c->type |= OperandSize32;
			}
			else{
				if (c->sizeover == SizeByte) c->type |= OperandSize8;
				else if (c->sizeover == SizeWord) c->type |= OperandSize16;
				else if (c->sizeover == SizeDword) c->type |= OperandSize32;
				else c->type |= OperandSize64;
			}
		}
	}
}

extern void BuildInternalInstrOp(IN PEXPR s, IN EXPRHINT hint, OUT InternalInstrOp* out);

/**
*	TranslateInstrOpVerify64
*
*	Description : If the operand matches criteria that
*	requires 64 bit mode enabled, verify it or throw an error
*
*	Input : op - Operand
*/
PRIVATE void TranslateInstrOpVerify64(IN InstrOperand* op) {

	bool_t bits64;

	// REX prefix byte is 64 bit only. This byte gets written
	// on extended registers or 64 bit operands. So these are not
	// allowed in non-64 bit modes.
	bits64 = FALSE;
	if (op->reg && (op->reg->id > 7 || GetOperandSize(op->reg->type) == OperandSize64))
		bits64 = TRUE;
	if (op->memory.base && (op->memory.base->id > 7 || GetOperandSize(op->memory.base->type) == OperandSize64))
		bits64 = TRUE;
	if (op->memory.index && (op->memory.index->id > 7 || GetOperandSize(op->memory.index->type) == OperandSize64))
		bits64 = TRUE;
	if (_mode != Bits64 && bits64)
		Error("Invalid operands in non-64 bit mode");
}

/**
*	TranslateInstrOp
*
*	Description : Translate PARSEDINSTROP to INSTROP and adds it to INSTR
*
*	Input : i - Instruction
*			op - Operand
*			pass - Pass
*/
PRIVATE void TranslateInstrOp(IN PINSTR i, IN PPARSEDINSTROP op, IN PASS pass) {

	InstrOperand*   operand;
	InternalInstrOp o;
	PEXPR           e;

	e = NULL;

	switch (op->segmentOverride) {
	case RegCs:AddPrefix(i, PrefixClass2_Cs); break;
	case RegSs:AddPrefix(i, PrefixClass2_Ss); break;
	case RegEs:AddPrefix(i, PrefixClass2_Es); break;
	case RegDs:AddPrefix(i, PrefixClass2_Ds); break;
	case RegGs:AddPrefix(i, PrefixClass2_Gs); break;
	case RegFs:AddPrefix(i, PrefixClass2_Fs); break;
	};

	if (i->farPtrSeg == InvalidFarPtrSegment)
		i->farPtrSeg = op->farPtrSeg;

	memset(&o, 0, sizeof(InternalInstrOp));

	BuildInternalInstrOp(op->expr, EHINT_NONE, &o);

	operand = &i->operands[i->operandCount++];
	memset(operand, 0, sizeof(InstrOperand));
	memcpy(operand->magic, "OPER", 4);

	operand->isMemory = op->isMemory;
	operand->wrt = o.wrt;
	operand->segto = o.segto;

	/* size override, if any. */
	operand->sizeover = op->size;

	if (!op->isMemory) {

		/* non-memory operand. */
		operand->val.ival = o.imm;
		operand->reg = o.base;

		if (operand->reg) {

			operand->type |= operand->reg->flags;
			operand->type |= OperandClassReg;
		}
		else {

			operand->type |= OperandClassImm;

			if (op->farPtrSeg != InvalidFarPtrSegment)
				operand->type |= OperandImmPtr;
		}
	}
	else {

		/* memory operand. */
		operand->memory.base = o.base;
		operand->memory.index = o.index;
		operand->memory.scale = o.scale;
		operand->type |= OperandClassMem;

		if (IsSimple(op->expr)) {

			/* this is [imm] or [reg] form. */
			operand->disp = DispNone;
			operand->val.ival = o.imm;
			operand->memory.displacement = o.imm;

			if (o.segto != NO_SEG)
				operand->disp = _mode == Bits16 ? Disp16 : Disp32;

		//	else if (o.imm == 0) operand->disp = DispNone; // we still want to write it if [0]. e.g. mov eax, [0]
			else if (o.imm <= 0xff) operand->disp = Disp8;
			else if (o.imm <= 0xffff) operand->disp = Disp16;
			else operand->disp = Disp32;
	    }
		else {

			/* this is [reg + disp] */
			operand->memory.displacement = o.imm;

			if (o.segto != NO_SEG)
				operand->disp = _mode == Bits16 ? Disp16 : Disp32;

			else if (o.imm == 0) operand->disp = DispNone;
			else if (o.imm <= 0xff) operand->disp = Disp8;
			else if (o.imm <= 0xffff) operand->disp = Disp16;
			else operand->disp = Disp32;
		}
	}
	TranslateInstrOpVerify64(operand);
}

/**
*	DeclareExpr
*
*	Description : Write DB, DD, DW etc with an <EXPR> operand
*
*	Input : size - Operand size
*		    in - Parsed instruction to write
*			pass - Pass
*
*	Output : TRUE on success, FALSE on error
*/
PRIVATE BOOL DeclareExpr(IN int size, IN PPARSEDINSTROP in, IN PASS pass) {

	output_t format;
	InternalInstrOp op;

	BuildInternalInstrOp(in->expr, EHINT_SIMPLE, &op);
	if (op.segto == NO_SEG)
		format = WRITE_RAWDATA;
	else
		format = WRITE_ADDRESS;
	write(_offset, op.segto, &op.imm, format, size, _segment);
	_offset += size;
	return TRUE;
}

/**
*	DeclareStr
*
*	Description : Write STRING to TARGET
*
*	Input : size - Operand size
*		    in - Parsed instruction to write
*			pass - Pass
*
*	Output : TRUE on success, FALSE on error
*/
PRIVATE BOOL DeclareStr(IN int size, IN PPARSEDINSTROP in, IN PASS pass) {

	// FIXME: All characters treated as DB
	size_t len = strlen(in->str->value);
	for (int i=0; i<len; i++)
		write(_offset, NO_SEG, &in->str->value[i], WRITE_RAWDATA, 1, _segment);
	_offset += len;
	return TRUE;
}

/**
*	DeclareFloat
*
*	Description : Write FLOAT or DOUBLE type to TARGET
*
*	Input : size - Operand size
*		    in - Parsed instruction to write
*			pass - Pass
*
*	Output : TRUE on success, FALSE on error
*/
PRIVATE BOOL DeclareFloat(IN int size, IN PPARSEDINSTROP in, IN PASS pass) {

	float   f;
	double  d;

	// FIXME: would like to support for all types
	switch(size) {
	case 4:
		f = strtof(in->str->value, NULL);
		write(_offset, NO_SEG, &f, WRITE_RAWDATA, sizeof(float), _segment);
		_offset += sizeof(float);
		return TRUE;
	case 8:
		d = strtod(in->str->value, NULL);
		write(_offset, NO_SEG, &d, WRITE_RAWDATA, sizeof(double), _segment);
		_offset += sizeof(double);
		return TRUE;
	};
	Error("Only DD, DQ supported for float/double types");
	return FALSE;
}

/**
*	TranslateDeclare
*
*	Description : Write DB, DW, ... to TARGET
*
*	Input : m - Mnemonic (DB, DW, etc)
*			in - Operand to write
*			pass - Pass
*
*	Output : TRUE on success, FALSE on error
*/
PRIVATE BOOL TranslateDeclare(IN Mnemonic m, IN PPARSEDINSTROP in, IN PASS pass) {

	int      size;

	if (_segment == ABS_SEG) {
		Error("Only RESB family allowed in absolute");
		return FALSE;
	}
	switch (m) {
	case DB: size = 1; break;	
	case DW: size = 2; break;
	case DD: size = 4; break;
	case DQ: size = 8; break;
	case DT: size = 10; break;
	case DO: size = 16; break;
//	case DY: size = 32; break;
//	case DZ: size = 64; break;
	}
	switch (in->type) {
	case OPEXPR:  return DeclareExpr(size, in, pass);
	case OPSTR:   return DeclareStr(size, in, pass);
	case OPFLOAT: return DeclareFloat(size, in, pass);
	default: return FALSE;
	}
}

/**
*	TranslateReserve
*
*	Description : Write RESB, RESW, etc, to TARGET
*
*	Input : m - Mnemonic (DB, DW, etc)
*			in - Operand to write
*			pass - Pass
*
*	Output : TRUE on success, FALSE on error
*/
PRIVATE BOOL TranslateReserve(IN Mnemonic m, IN PPARSEDINSTROP in, IN PASS pass) {

	InternalInstrOp op;
	size_t size;

	BuildInternalInstrOp(in->expr, EHINT_SIMPLE, &op);

	switch (m) {
	case RESB: size = 1; break;
	case RESW: size = 2; break;
	case RESD: size = 4; break;
	};
	_offset += size;
	return TRUE;
}

/**
*	TranslatePusedoOperation
*
*	Description : Given an instruction, this will write the psuedo-
*	operation to TARGET.
*
*	Input : in - Instruction to write
*			pass - Pass
*
*	Output : TRUE on success, FALSE otherwise
*/
PRIVATE BOOL TranslatePusedoOperation(IN PPARSEDINSTR in, IN PASS pass) {

	PPARSEDINSTROP op;

	for (op = in->operands; op; op = op->next) {

		switch(in->mnemonic) {
		case DB: case DW: case DD:
		case DQ: case DT: case DO:
			TranslateDeclare(in->mnemonic, op, pass);
			break;
		case RESB: case RESW: case RESD:
			TranslateReserve(in->mnemonic, op, pass);
			break;
		}
	}
	return FALSE;
}

/**
*	TranslateInstr
*
*	Description : During PASS0, this will only
*   adjust _offset used for label calculations.
*   During PASSGEN, writes the machine code to
*   the Target.
*
*	Input : in - Parsed instruction to assemble
*	        pass - Current pass
*
*	Output : TRUE if success, FALSE on error
*/
PRIVATE BOOL TranslateInstr(IN PPARSEDINSTR in, IN PASS pass) {

	int             k;
	size_t          c;
	INSTR           i;
	PPARSEDINSTROP op;
	InstrTableEntry* e;

	int repeat     = 1;
	int opcount    = 0;
	OperandType t1 = 0;
	OperandType t2 = 0;
	OperandType t3 = 0;

	repeat = in->times;

	if (in->label)
		LabelDefine(in->label, _segment, _offset, FALSE, TRUE);

	if (in->mnemonic == MnemonicInvalid)
		return TRUE;

	//
	// psuedo mnemonics
	//

	switch (in->mnemonic) {
	case DB: case DW: case DD: case DQ: case DT: case DO:
	case RESB: case RESW: case RESD: case RESQ:

		while (repeat-- > 0)
			TranslatePusedoOperation(in, pass);
		return TRUE;
	}

	//
	// promote operands and attempt to match instruction:
	//

	memset(&i, 0, sizeof(INSTR));
	i.mnemonic  = in->mnemonic;
	i.opsize    = SizeInvalid;
	i.farPtrSeg = InvalidFarPtrSegment;
	i.operands  = NULL;

	for (op = in->operands; op; op = op->next)
		opcount++;

	i.operands = malloc(sizeof(InstrOperand) * opcount);

	for (op = in->operands; op; op = op->next)
		TranslateInstrOp(&i, op, pass);

	PromoteOperandSize(&i);

	if (i.operandCount > 0)  t1 = i.operands[0].type;
	if (i.operandCount > 1)  t2 = i.operands[1].type;
	if (i.operandCount > 2)  t3 = i.operands[2].type;

	e = MatchInstruction(i.mnemonic, i.opsize, i.operandCount, t1, t2, t3);

	if (!e) {

		Error("Instruction not found");
		free(i.operands);
		return FALSE;
	}

	if (_pass == PASSGEN && _segment == ABS_SEG && (e->modifier & ModPsuedo) == 0)
		Error( "Attempt to assemble code in absolute segment");

	//
	// process the matched instruction and create INSTR
	//

	i.entry = e;
	i.opcode = e->opcode;
	i.opcodeExt = e->opcodeExt;
	i.line = 0;
	i.modifier = e->modifier;

	// adjust for "fixed extraordinary prefix" in opcode.
	// this is used a lot in SSE
	if (IsPrefix((i.opcode >> 16) & 0xff)) {
		AddPrefix(&i, (i.opcode >> 16) & 0xff);
		i.opcode &= 0xffff;
	}
	
	// set ignore bit
	if (opcount > 0) i.operands[0].type |= GetOperandIgnore(e->parms[0]);
	if (opcount > 1) i.operands[1].type |= GetOperandIgnore(e->parms[1]);
	if (opcount > 2) i.operands[2].type |= GetOperandIgnore(e->parms[2]);

	// the subclass must always match.
	if (opcount > 0) i.operands[0].type |= GetOperandSubclass(e->parms[0]);
	if (opcount > 1) i.operands[1].type |= GetOperandSubclass(e->parms[1]);
	if (opcount > 2) i.operands[2].type |= GetOperandSubclass(e->parms[2]);

	// now we prepare the real operand size.
	if (opcount > 0) i.operands[0].type &= ~(OperandSize8 | OperandSize16 | OperandSize32 | OperandSize64 | OperandSize128);
	if (opcount > 1) i.operands[1].type &= ~(OperandSize8 | OperandSize16 | OperandSize32 | OperandSize64 | OperandSize128);
	if (opcount > 2) i.operands[2].type &= ~(OperandSize8 | OperandSize16 | OperandSize32 | OperandSize64 | OperandSize128);

	// and set it.
	if (opcount > 0) i.operands[0].type |= GetOperandSize(e->parms[0]);
	if (opcount > 1) i.operands[1].type |= GetOperandSize(e->parms[1]);
	if (opcount > 2) i.operands[2].type |= GetOperandSize(e->parms[2]);

	// add prefixes
	for (k = 0; k < 4; k++) {
		switch (in->prefix[k]) {
		case PrefixLock: AddPrefix(&i, PrefixClass1_Lock); break;
		case PrefixRep:
		case PrefixRepe:
		case PrefixRepz: AddPrefix(&i, PrefixClass1_Rep); break;
		case PrefixRepne:
		case PrefixRepnz: AddPrefix(&i, PrefixClass1_Repne); break;
		case PrefixA16:
			if (_mode == Bits32)
				AddPrefix(&i, PrefixClass4_Addrsize);
			break;
		case PrefixA32:
			if (_mode == Bits16)
				AddPrefix(&i, PrefixClass4_Addrsize);
			break;
		case PrefixO16:
			if (_mode == Bits32)
				AddPrefix(&i, PrefixClass3_Opsize);
			break;
		case PrefixO32:
			if (_mode == Bits16)
				AddPrefix(&i, PrefixClass3_Opsize);
			break;
		case PrefixDS: AddPrefix(&i, PrefixClass2_Ds); break;
		case PrefixES: AddPrefix(&i, PrefixClass2_Es); break;
		case PrefixSS: AddPrefix(&i, PrefixClass2_Ss); break;
		case PrefixCS: AddPrefix(&i, PrefixClass2_Cs); break;
		case PrefixFS: AddPrefix(&i, PrefixClass2_Fs); break;
		case PrefixGS: AddPrefix(&i, PrefixClass2_Gs); break;
		case PrefixInvalid:
		default:
			break;
		};
	}

	// add operand size override if needed.
	AddOpsizeOverride(&i);

	// add address size override if needed.
	AddAddrOverride(&i);

	// add mod r/m and sib bytes if needed. 
	if (e->modifier & ModMODRM) {
		BuildModRM(&i);
		BuildSIB(&i);
	}

	// apply ModRegOpcode modifier and opcode extension.
	UpdateOpcodeExtension(&i);
	UpdateOpcodeRegisterField(&i);

	// generate code.
	while (repeat-- > 0) {
		c = Assemble(&i);
		_offset += c;
	}

	free(i.operands);
	return TRUE;
}

PRIVATE void SourceLine(void) {

	char line[100];

	return;

	if (!_listing)
		return;

//	if (current->fpos == EOF) {
//		/* preprocessor tokens don't have a file position. */
//		return;
//	}

	/* ignore preprocessor lines. */
//	if (!current->source->stream)
//		return;

//	if (fseek(current->source->stream, current->fpos, SEEK_SET) != -1) {
//		fgets(line, 100, current->source->stream);
//		_listing->line(line, current->number);
//	}
}

/**
*	Translate
*
*	Description : In PASS 0, calculates offsets and adds labels.
*	IN PASSGEN, writes the bytecode of "instr" into TARGET
*
*	Input : instr - Instruction
*			pass - Pass
*
*	Output : TRUE on success, FALSE otherwise
*/
PRIVATE BOOL Translate(IN PPARSEDINSTR instr, PASS pass) {

	BOOL result;
	result = FALSE;

	if (!instr) return TRUE;

	if (!TranslateInstr(instr, pass)) result = TRUE;

//	if (_listing) _listing->line(NULL, 0);

	return result;
}

/**
*	Init
*
*	Description : Sets TARGET and LISTING
*/
PRIVATE void Init(void) {

	_listing = GetListGenerator();
	_output = GetOutputFormat();
}

/* PUBLIC Definitions ************************/

/**
*	SetBits
*
*	Description : Process BITS <number>
*
*	Input : bits - Operation mode
*
*	Output : TRUE on success, FALSE on error
*/
PUBLIC BOOL SetBits(IN int bits) {

	switch (bits) {
	case 16: _mode = Bits16; break;
	case 32: _mode = Bits32; break;
	case 64: _mode = Bits64; break;
	default: return FALSE;
	};
	return TRUE;
}

/**
*	SetSegment
*
*	Process SEGMENT <ident> or SECTION <ident>
*
*	Input : name - Name of new segment
*			pass - Pass
*/
PUBLIC void SetSegment(IN PSTRING name, IN PASS pass) {

	Init();
	_segment = _output->section(name->value, pass, _mode);
	_offset = _output->getOffset(pass);
}

/**
*	SetAbsolute
*
*	Description : Switch to ABS segment
*	and reset current offset
*
*	Input : origin - Origin offset
*/
PUBLIC void SetAbsolute(IN int origin) {

	_segment = ABS_SEG;
	_offset = origin;
}

/**
*	SetExtern
*
*	Description : Process EXTERN <symbol>.
*	EXTERN symbols have no segment. Every
*	label must have a segment/offset, so a
*	unique segment index is created for it.
*/
PUBLIC void SetExtern(IN PSTRING ident) {

	int seg;
	int off;

	LabelDefineExtern(ident, FALSE);

//	if (! LabelGet(ident->value, &seg, &off))
//		LabelDefine(ident, NextSegmentIndex(), 0, TRUE, FALSE);
//	else
//		LabelDefine(ident, seg, off, TRUE, FALSE);
}

/**
*	SetGlobal
*
*	Description : Process GLOBAL <ident>
*/
PUBLIC void SetGlobal(IN PSTRING ident) {

	LabelDefineGlobal(ident, _segment, _offset);
}

/**
*	AlignAddress
*
*	Description : Given an address, align it
*
*	Input : addr - Address
*			align - Alignment
*
*	Output : Aligned address
*/
PRIVATE uint32_t AlignAddress(IN uint32_t addr, IN uint32_t align) {

	if (addr % align != 0)
		addr += align - addr % align;

	return addr;
}

/**
*	SetAlign
*
*	Process ALIGN <number>. Padding writes
*	INT3 to TARGET
*
*	Input : align - Alignment
*/
PUBLIC void SetAlign(IN int align) {

	unsigned long upperaddr;
	unsigned char int3 = 0xCC;

	if (align > 0 && (align & (align - 1)) != 0) {
		Error("alignment must be a power of 2");
		return;
	}

	upperaddr = AlignAddress(_offset, align);

	//
	// pad with int3 (breakpoint) instruction
	//

	while (_offset < upperaddr) {
		write(_offset, NO_SEG, &int3, WRITE_RAWDATA, 1, _segment);
		_offset++;
	}
}

/**
*	GenerateCode
*
*	Description : In PASS 0, calculates offsets and adds labels;
*	in PASSGEN writes the bytecode of "ir" into TARGET
*/
PUBLIC void GenerateCode(IN PPARSEDINSTR ir, IN PASS pass) {

	Init();

	// reset if starting a new pass:
	if (_pass != pass) {
		// stdmac already switches to .text before each pass
		// since GenerateCode only gets called on instructions,
		// directives may switch the segment before this ever
		// gets called
		_offset = 0;
		_pass = pass;
	}
	Translate(ir, pass);
}

/*
	Optimize --

	In MatchInstruction, should watch for rcode
	to select smaller instruction:
		NASM: BF0500 mov     di, 0x0005
		Us: C7C70500 mov     di, 0x0005

	Try to also support & match moffset form of MOV
*/
