/************************************************************************
*
*	dict.c - Dictionary ADT.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <assert.h>
#include <string.h>
#include <malloc.h>
#include "nasm.h"

/**
*	Hash
*
*	Description : Computes a hash of a CSTRING
*
*	Input : key - String to hash
*
*	Output : Hash of key
*/
PRIVATE unsigned Hash(IN char* key) {

	const unsigned char *str = (const unsigned char *)key;
	unsigned long hash = 5381;
	int c;

	/* djb2 algorithm */
	while ((c = *str++) != '\0')
		hash = hash * 33 + c;

	return hash;
}

/**
*	MapCreate
*
*	Description : Creates a MAP object
*
*	Input : numberOfBuckets - Number of buckets to allocate
*
*	Output : MAP object
*/
PUBLIC PMAP MapCreate(IN unsigned long numberOfBuckets) {

	PMAP map;
	
	map = malloc(sizeof(MAP));
	strcpy(map->magic, "MAP");
	map->numberOfElements = 0;
	map->numberOfBuckets = numberOfBuckets;
	map->buckets = calloc(numberOfBuckets, sizeof(PMAPELEMENT));
	return map;
}

/**
*	MapGet
*
*	Description : Given a key, return the associated value
*
*	Input : map - MAP to look up
*	        key - Key
*
*	Output : Pointer to value
*/
PUBLIC void* MapGet(IN PMAP map, IN bucket_key_t key) {

	unsigned     hash;
	PMAPELEMENT  firstItem;
	PMAPELEMENT  item;

	if (!map) return NULL;

	hash = Hash(key) % map->numberOfBuckets;
	firstItem = map->buckets[hash];

	for (item = firstItem; item; item = item->next) {
		if (!strcmp(item->key->value, key))
			return item->value;
	}
	return NULL;
}

/**
*	MapPut
*
*	Description : Puts or updates a (key, value) pair into a map
*
*	Input : map - Map to insert into
*	        key - Key
*		    value - Value
*
*	Output : TRUE on success
*/
PUBLIC BOOL MapPut(IN PMAP map, IN char* key, IN void* value) {

	unsigned     hash;
	PMAPELEMENT  firstItem;
	PMAPELEMENT  item;

	hash = Hash(key) % map->numberOfBuckets;
	firstItem = map->buckets[hash];

	// if its already in, we need to UPDATE it
	for (item = firstItem; item; item = item->next) {
		if (!strcmp(item->key->value, key)) {
			item->value = value;
			return TRUE;
		}
	}

	item = malloc(sizeof(MAPELEMENT));
	item->key = InternString(key);
	strcpy(item->magic, "MAPEL");
	item->value = value;
	item->next = NULL;
	item->bucket = hash;

	if (firstItem) {
		map->buckets[hash] = item;
		item->next = firstItem;
	}
	else
		map->buckets[hash] = item;

	map->numberOfElements++;
	return TRUE;
}

/**
*	MapRemove
*
*	Description : Removes a (key, value) pair
*
*	Input : map - Map to insert into
*	        key - Key
*		    value - Value
*
*	Output : TRUE on success
*/
PUBLIC BOOL MapRemove(IN PMAP map, IN char* key, IN void* value) {

	unsigned     hash;
	PMAPELEMENT  firstItem;
	PMAPELEMENT  item;
	PMAPELEMENT  prev;

	assert(map != NULL);
	assert(key != NULL);

	hash = Hash(key) % map->numberOfBuckets;
	firstItem = map->buckets[hash];

	if (!strcmp(firstItem->key->value, key)) {

		item = firstItem->next;
		free(firstItem);
		map->buckets[hash] = item;
		map->numberOfElements--;
		return TRUE;
	}

	prev = firstItem;
	for (item = firstItem->next; item; item = item->next, prev = prev->next) {
		if (!strcmp(item->key->value, key)) {
			prev->next = item->next;
			free(item);
			map->numberOfElements--;
			return TRUE;
		}
	}

	return FALSE;
}

/**
*	MapFree
*
*	Description : Frees a map
*
*	Input : map - Map to free
*	        callback - Function to call to free each item
*/
PUBLIC void MapFree(IN PMAP map, IN OPTIONAL map_free_t callback) {

	size_t       bucket;
	PMAPELEMENT  firstItem;
	PMAPELEMENT  item;
	PMAPELEMENT  next;

	if (!map) return;

	for (bucket = 0; bucket < map->numberOfBuckets; bucket++) {

		firstItem = map->buckets[bucket];

		if (!firstItem) continue;

		next = NULL;
		for (item = firstItem; item; item = next) {

			next = item->next;
			if (callback)
				callback(item);
			free(item);
		}
	}
	free(map->buckets);
	free(map);
}

/**
*	MapElementCount
*
*	Description : Returns number of elements in use by a map
*
*	Input : map - Map to check
*
*	Output : Number of elements in map
*/
PUBLIC long MapElementCount(IN PMAP map) {

	return map->numberOfElements;
}
