/************************************************************************
*
*	buffer.c - Dynamic buffer.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nasm.h"
#include <assert.h>
#include <malloc.h>
#include <stdarg.h>
#include <string.h>

#define BUFFER_SIZE 8

PUBLIC PBUFFER NewBuffer(void) {

	PBUFFER r = malloc(sizeof(BUFFER));
	memcpy(r->magic, "BUF ", 4);
	r->data = malloc(BUFFER_SIZE);
	r->allocCount = BUFFER_SIZE;
	r->elementCount = 0;
	return r;
}

PUBLIC void BufferFree(IN PBUFFER r) {

	if (!r)
		return;
	free(r->data);
	free(r);
}

PRIVATE void Resize(IN PBUFFER b) {

	int newsize = b->allocCount * 2;
	uint8_t* data = realloc(b->data, newsize);
	b->data = data;
	b->allocCount = newsize;
}

PUBLIC uint8_t* GetBufferData(IN PBUFFER b) {

	return b->data;
}

PUBLIC size_t GetBufferLength(IN PBUFFER b) {

	return b->elementCount;
}

PUBLIC void BufferWrite(IN PBUFFER b, IN char c) {

	if (b->allocCount == (b->elementCount + 1))
		Resize(b);
	b->data[b->elementCount++] = c;
}

PUBLIC void BufferPush(IN PBUFFER b, IN char c) {

	BufferWrite(b, c);
}

PUBLIC char BufferPop(IN PBUFFER b) {

	assert(b->elementCount != 0);
	return b->data[--b->elementCount];
}

PUBLIC void BufferAppend(IN PBUFFER b, IN char* s, IN int len) {

	for (int i = 0; i < len; i++)
		BufferWrite(b, s[i]);
}


PUBLIC void BufferPrintf(IN PBUFFER b, IN char* fmt, ...) {

	va_list args = NULL;
	if (strlen(fmt) == 1) {
		BufferPush(b, *fmt);
		BufferPush(b, '\0');
		return;
	}
	while (TRUE) {
		int avail = b->allocCount - b->elementCount;
		va_start(args, fmt);
		int written = vsnprintf(b->data + b->elementCount, avail, fmt, args);
		va_end(args);
		if (written == -1 || (avail <= written)) {
			Resize(b);
			continue;
		}
		b->elementCount += written;
		return;
	}
}
