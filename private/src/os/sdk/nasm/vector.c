/************************************************************************
*
*	vector.c - Vector support
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nasm.h"
#include <assert.h>
#include <string.h>
#include <malloc.h>

/* vectors are dynamic sized arrays of arbitrary elements. */

PUBLIC PVECTOR NewVector(IN OPTIONAL unsigned int numAllocations, IN size_t elementSize) {

	PVECTOR v;
	
	assert(elementSize > 0);

	v = (PVECTOR)malloc(sizeof(VECTOR));
	if (!v)
		return NULL;
	memcpy(v->magic, "VECT", 4);
	v->data = NULL;
	if (numAllocations > 0)
		v->data = (uint8_t*)realloc(NULL, numAllocations * elementSize);
	v->elementSize = elementSize;
	v->elementCount = 0;
	v->numAllocations = numAllocations;
	return v;
}

PUBLIC void ExtendVector(IN PVECTOR v, IN size_t numAllocations) {

	assert(v != NULL);

	if (v->elementCount + numAllocations <= v->numAllocations)
		return; /* don't need to extend. */

	v->numAllocations += numAllocations;
	v->data = realloc(v->data, v->numAllocations * v->elementSize);
}

PUBLIC PVECTOR CopyVector(IN PVECTOR v) {

	PVECTOR n;

	assert(v != NULL);

	n = NewVector(v->elementCount, v->elementSize);
	if (n) {
		memcpy(n->data, v->data, v->elementCount * v->elementSize);
		n->elementCount = v->elementCount;
	}
	return n;
}

PUBLIC void VectorPush(IN PVECTOR v, IN void* e) {

	assert(v != NULL);
	assert(e != NULL);
	ExtendVector(v, 1);
	memcpy(&v->data[v->elementCount * v->elementSize], e, v->elementSize);
	v->elementCount++;
}

PUBLIC void VectorPop(IN PVECTOR v, OUT OPTIONAL void* data) {

	assert(v != NULL);
	assert(v->elementCount > 0);
	v->elementCount--;
	if (data)
		memcpy(data, &v->data[v->elementCount * v->elementSize], v->elementSize);
}

PUBLIC void VectorGet(IN PVECTOR v, IN index_t index, OUT void* data) {

	assert(v != NULL);
	assert(data != NULL);
	assert(0 <= index && index < v->numAllocations);
	memcpy(data, &v->data[index * v->elementSize], v->elementSize);
}

PUBLIC void VectorSet(IN PVECTOR v, IN index_t index, OUT void* data) {

	assert(v != NULL);
	assert(data != NULL);
	assert(0 <= index && index < v->numAllocations);
	memcpy(&v->data[index * v->elementSize], data, v->elementSize);
}

PUBLIC void VectorFirstElement(IN PVECTOR v, OUT void* data) {

	assert(v != NULL);
	assert(data != NULL);
	assert(v->elementCount > 0);
	memcpy(data, &v->data[0], v->elementSize);
}

PUBLIC BOOL VectorLastElement(IN PVECTOR v, OUT void* data) {

	assert(v != NULL);
	assert(data != NULL);
	if (v->elementCount == 0)
		return TRUE;
	memcpy(data, &v->data[(v->elementCount - 1) * v->elementSize], v->elementSize);
	return FALSE;
}

PUBLIC PVECTOR NewReverseVector(IN PVECTOR v) {

	PVECTOR n;
	index_t i;

	assert(v != NULL);

	n = NewVector(v->elementCount, v->elementSize);
	if (!n)
		return NULL;
	n->elementCount = v->elementCount;
	for (i = 0; i < n->elementCount; i++) {
		memcpy(&n->data[i * n->elementSize],
			&v->data[(v->elementCount - i - 1) * v->elementSize],
			n->elementSize);
	}
	return n;
}

PUBLIC void VectorAppend(IN PVECTOR v, IN PVECTOR a) {

	assert(v != NULL);
	assert(v->elementSize == a->elementSize);

	ExtendVector(v, a->elementCount);
	memcpy(&v->data[v->elementCount * v->elementSize], a->data,
		a->elementSize * a->elementCount);
	v->elementCount += a->elementCount;
}

PUBLIC void* VectorData(IN PVECTOR v) {

	assert(v != NULL);
	return v->data;
}

PUBLIC size_t VectorLength(IN PVECTOR v) {

	assert(v != NULL);
	return v->elementCount;
}

PUBLIC void FreeVector(IN PVECTOR v) {

	assert(v != NULL);
	free(v->data);
	free(v);
}
