/************************************************************************
*
*	nasm.h - Master header file.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <stdio.h>
#include "core.h"

#ifndef NASM_H
#define NASM_H

#if defined(WIN32) || defined(_WIN32) 
#  define PATH_SEPARATOR '\\'
#else 
#  define PATH_SEPARATOR '/'
#endif 


//
// main.c
// Returns a static incrementing integer count.
// Used by TARGET's when creating segments.
//
extern PUBLIC int NextSegmentIndex(void);

//
// Current assembler pass. PASS0 generates labels
// and calculates offsets. PASSGEN is the final code
// generation pass. In between passes may better determine
// label offsets for optimisations in the future.
//
// PASSNUL is used internally by the Code Generator for
// detecting pass changes. Only start with PASS0.
//
typedef enum PASS {
	PASS0,
	PASSGEN,
	PASSMAX,
	PASSNUL
}PASS;

//
// Code generation mode.
//
typedef enum BITS {
	Bits16,
	Bits32,
	Bits64
}BITS;

//
// String table
//

typedef struct STRING {
	char     magic[4];
	unsigned refcount;
	char*    value;
	offset_t offset1;
	offset_t offset2;
	PSTRING  next;
}STRING, *PSTRING;

extern PUBLIC PSTRING InternString    (IN char* string);
extern PUBLIC void    FreeStringTable (void);

//
// Message output
//

typedef enum ERRLEVEL {
	LevelInfo,
	LevelError,
	LevelWarn,
	LevelFatal
}ERRLEVEL;

extern PUBLIC void         NasmError         (IN ERRLEVEL level, IN char* msg, ...);
extern PUBLIC unsigned int NasmGetErrorCount (void);
extern PUBLIC unsigned int NasmGetWarnCount  (void);

#define Error(msg,...) NasmError(LevelError, msg, __VA_ARGS__)
#define Warn(msg,...) NasmError(LevelWarn, msg, __VA_ARGS__)
#define Info(msg,...) NasmError(LevelInfo, msg, __VA_ARGS__)
#define Fatal(msg,...) NasmError(LevelFatal, msg, __VA_ARGS__)

//
// Source file
//

typedef struct COORD {
	PSTRING fname;
	unsigned x;
	unsigned y;
}COORD, *PCOORD;

typedef struct SOURCE {
	FILE*    stream;
	int      lineseek;
	char*    raw;
	COORD    loc;
	PBUFFER  pushback;
	BOOL     eof;
	BOOL     eol;
}SOURCE, *PSOURCE;

extern PUBLIC PCOORD  FileCoord(void);
extern PUBLIC PSOURCE FileOpen(IN PSTRING filename);
extern PUBLIC PSOURCE FileOpenRaw(IN char* filename, IN char* data);
extern PUBLIC int     FileGet(void);
extern PUBLIC int     FileUnget(IN int c);
extern PUBLIC int     FilePeek(void);
extern PUBLIC void    FileGetLine(IN char* buf, IN int max);

//
// Expressions
//
// Expressions are represented as an array of TERMS (that is,
// an array of EXPR.) A "BASIC" expression is one that has a single
// element. A "VECTOR" expression has multiple elements. Typically
// a "VECTOR" element is only applicable for memory operands using
// a base+index addressing mode.
//
// EXPRKIND is a composite: it stores the KIND in addition to information
// such as the register id and segment id (if applicable.)
//
// +------------------+
// | <kind> |   <id>  |
// +------------------+
// 32      16         0
// <kind> + <id> => EXPR.kind value
//
// Forward References:
//
// Forward references use a special segment, FWREF_SEG,
// to act as an "Unknown segment". In all cases, forward
// references MUST have the Kind:
//
//     EXPR_FWREF + EXPR_NUM + FWREF_SEG
//
// When forward references are resolved, FWREF_SEG will be
// replaced with the actual segment in a later pass. The
// Code Generator will see FWREF_SEG as a valid segment to
// ensure correct offsets are calculated in pass 0
//
typedef enum EXPRKIND {
	EXPR_END   = 0,
	EXPR_FWREF = 0x02000000,
	EXPR_NUM   = 0x40000000, // EXPR_NUM + <seg id>
	EXPR_REG   = 0x80000000  // EXPR_REG + <register id>
}EXPRKIND;

// segment id's:
#define NO_SEG       0
#define ABS_SEG      0xffff
#define FWREF_SEG    0xfffe

typedef enum EXPRHINT {
	EHINT_NONE,
	EHINT_SIMPLE
}EXPRHINT;

typedef struct EXPR {
	char     magic[5];
	int      kind;
	long     val;
}EXPR, *PEXPR;

#define EXPR_SEG(x) (x & 0xffff)
#define EXPR_REG(x) (x & 0xffff)

typedef struct InternalInstrOp InternalInstrOp;

extern PUBLIC BOOL IsNumber(IN PEXPR e);
extern PUBLIC BOOL IsScalar   (IN PEXPR e);
extern PUBLIC BOOL IsVector   (IN PEXPR e);
extern PUBLIC BOOL IsSimple   (IN PEXPR e);
extern PUBLIC BOOL IsReloc    (IN PEXPR e);
extern PUBLIC BOOL Reloc      (IN PEXPR e);
extern PUBLIC void BuildInternalInstrOp(IN PEXPR s, IN EXPRHINT hint, OUT InternalInstrOp* out);

//
// Labels
//

extern PUBLIC BOOL  LabelGet          (IN char* name, OUT int* seg, OUT int* offset);
extern PUBLIC BOOL  LabelIsExtern     (IN char* name);
extern PUBLIC BOOL  LabelDefine       (IN PSTRING name, IN int seg, IN int offset);
extern PUBLIC void  LabelDefineGlobal (IN PSTRING name, IN int seg, IN int offset);
extern PUBLIC void  LabelDefineExtern (IN PSTRING name, BOOL isAuto);
extern PUBLIC BOOL  LabelInit         (void);
extern PUBLIC void  FreeLabels        (void);
extern PUBLIC char* LabelScope        (IN char* name);

//
// Lexer
//

typedef enum TOKENTYPE{
	TokenInvalid,
	TokenEndOfFile,
	TokenEndOfLine,
	TokenInteger,
	TokenFloat,
	TokenIdentifier,
	TokenString,
	TokenPrefix,
	TokenRegister,
	TokenMnemonic,
	TokenDirective,
	TokenSizeSpecifier,
	TokenPtr,
	TokenTimes, /* TIMES token. */
	TokenHere, /* $ token. */
	TokenSegBase, /* $$ token. */
	TokenRightShift,
	TokenLeftShift,
	TokenEqual = '=',
	TokenBitOr = '|',
	TokenBitAnd = '&',
	TokenNot = '!',
	TokenXor = '~',
	TokenMod = '%',
	TokenPlus = '+',
	TokenMinus = '-',
	TokenStar = '*',
	TokenFowardSlash = '/',
	TokenLeftBracket = '[',
	TokenRightBracket = ']',
	TokenLeftParen = '(',
	TokenRightParen = ')',
	TokenComma = ',',
	TokenColon = ':',
	TokenMacroOperand = 0xff
}TOKENTYPE;

typedef struct OPERATOR {
	TOKENTYPE    type;
	char*        s;
	unsigned int optype;
	unsigned int prec;
} OPERATOR, *POPERATOR;

#define OP_NONE   0
#define OP_UNARY  1
#define OP_BINARY 2

typedef union VALUE {
	long ival;
	char* s;
}VALUE, *PVALUE;

typedef enum SIZE {
	SizeByte,
	SizeWord,
	SizeDword,
	SizeQword,
	SizeTbyte,
	SizeInvalid
}SIZE;

typedef enum PREFIX {
	PrefixLock = 0,
	PrefixRep,
	PrefixRepe,
	PrefixRepz,
	PrefixRepne,
	PrefixRepnz,
	PrefixXacquire,
	PrefixXrelease,
	PrefixBnd,
	PrefixNobnd,
	PrefixA16,
	PrefixA32,
	PrefixO16,
	PrefixO32,
	PrefixDS,
	PrefixES,
	PrefixSS,
	PrefixCS,
	PrefixFS,
	PrefixGS,
	PrefixInvalid
}PREFIX;

typedef enum RegisterType {
	RegInvalid,
	RegSs, RegCs, RegDs, RegEs, RegFs, RegGs,
	RegAh, RegAl, RegBh, RegBl, RegCh, RegCl, RegDh, RegDl,
	RegSi, RegDi, RegBp, RegSp, RegAx, RegBx, RegCx, RegDx,
	RegEax, RegEbx, RegEcx, RegEdx,
	RegEsi, RegEdi, RegEbp, RegEsp,
	RegRax, RegRbx, RegRcx, RegRdx,
	RegRsi, RegRdi, RegRbp, RegRsp,
	RegCr0, RegCr1, RegCr2, RegCr3, RegCr4, RegCr5, RegCr6, RegCr7,
	RegCr8, RegCr9, RegCr10, RegCr11, RegCr12, RegCr13, RegCr14, RegCr15,
	RegDr0, RegDr1, RegDr2, RegDr3, RegDr4, RegDr5, RegDr6, RegDr7,
	RegDr8, RegDr9, RegDr10, RegDr11, RegDr12, RegDr13, RegDr14, RegDr15,
	RegR8l, RegR9l, RegR10l, RegR11l, RegR12l, RegR13l, RegR14l, RegR15l,
	RegR8w, RegR9w, RegR10w, RegR11w, RegR12w, RegR13w, RegR14w, RegR15w,
	RegR8d, RegR9d, RegR10d, RegR11d, RegR12d, RegR13d, RegR14d, RegR15d,
	RegR8, RegR9, RegR10, RegR11, RegR12, RegR13, RegR14, RegR15,
	RegMmx0, RegMmx1, RegMmx2, RegMmx3, RegMmx4, RegMmx5, RegMmx6, RegMmx7,
	RegXmm0, RegXmm1, RegXmm2, RegXmm3, RegXmm4, RegXmm5, RegXmm6, RegXmm7,
	RegXmm8, RegXmm9, RegXmm10, RegXmm11, RegXmm12, RegXmm13, RegXmm14, RegXmm15,
	RegYmm0, RegYmm1, RegYmm2, RegYmm3, RegYmm4, RegYmm5, RegYmm6, RegYmm7,
	RegYmm8, RegYmm9, RegYmm10, RegYmm11, RegYmm12, RegYmm13, RegYmm14, RegYmm15,
	RegSt0, RegSt1, RegSt2, RegSt3, RegSt4, RegSt5, RegSt6, RegSt7,
}RegisterType, REGTYPE;

typedef enum Mnemonic Mnemonic;

typedef union TOKENVALUE {
	unsigned long long inum;
	PSTRING       s;
	char          oper;
	RegisterType  reg;
	PREFIX        prefix;
	SIZE          size;
	Mnemonic      mnemonic;
}TOKENVALUE;

typedef struct TOKEN {
	char         magic[4];
	TOKENTYPE    type;
	TOKENVALUE   value;
	/* used by preprocessor. */
	int          position;
	BOOL         isvararg;
	PSET         hideset;
}TOKEN, *PTOKEN;

/* Current lexer state. */
#define CONTEXT_PUSHBACK_MAX 4

//
// Preprocessor
//

typedef enum PreprocTokenType {
	PreprocTokenInvalid,
	TokenDefine,
	TokenUndef,
	TokenInclude,
	TokenEndif,
	TokenElse,
	TokenIfdef,
	TokenIfndef,
	TokenMacro,
	TokenEndmacro,
	TokenPush,
	TokenPop,
	TokenIf
}PreprocTokenType;

//
// Parser
//

typedef enum PARSEDINSTROPTYPE {
	OPEXPR, OPFLOAT, OPSTR
}PARSEDINSTROPTYPE, *PPARSEDINSTROPTYPE;

typedef struct PARSEDINSTROP *PPARSEDINSTROP;
typedef struct PARSEDINSTROP {
	PARSEDINSTROPTYPE type;
	PSTRING           str; // OPFLOAT, OPSTR
	PEXPR             expr; // OPEXPR
	BOOL              isMemory;
	SIZE              size;
	RegisterType      segmentOverride;
	unsigned long     farPtrSeg;
	PPARSEDINSTROP    next;
}PARSEDINSTROP, *PPARSEDINSTROP;

typedef struct PARSEDINSTR {
	PSTRING         label;
	long            times;
	PREFIX          prefix[4];
	Mnemonic        mnemonic;
	PPARSEDINSTROP  operands;
}PARSEDINSTR, *PPARSEDINSTR;

//
// PUBLIC definitions
//

typedef int(*PLEX)(OUT PTOKEN);

extern PUBLIC PEXPR     Expr(IN PLEX, IN OUT PTOKEN);
extern PUBLIC long      Eval(IN PLEX, IN OUT PTOKEN);
extern PUBLIC int       StdScan(OUT PTOKEN);
extern PUBLIC int       Lex(OUT PTOKEN);
extern PUBLIC void      LexUnget(IN TOKEN token);
extern PUBLIC TOKEN     LexPeek(IN int count);
extern PUBLIC void      LexPushStream(IN TokenVector** in);
extern PUBLIC void      LexPopStream(IN TokenVector** in);
extern PUBLIC void      LexInit(void);
extern PUBLIC void      FreeLexer(voi);
extern PUBLIC PPARSEDINSTR Parse(IN PASS pass);
extern PUBLIC void      GenerateCode(IN PPARSEDINSTR ir, IN PASS pass);
extern PUBLIC BOOL      Directive(IN PLEX scn, IN OUT PTOKEN tok, IN PASS pass);
extern PUBLIC BOOL      SetBits(IN int bits);
extern PUBLIC void      SetSegment(IN PSTRING name, IN PASS pass);
extern PUBLIC void      SetAbsolute(IN int origin);
extern PUBLIC void      SetExtern(IN PSTRING ident);
extern PUBLIC void      SetGlobal(IN PSTRING ident);
extern PUBLIC void      SetAlign(IN int align);
extern PUBLIC void      FreeMacros (void);

/* code generator. */

typedef enum DisplacementType {
	DispNone,
	Disp8,
	Disp16,
	Disp32
}DisplacementType;

/*
    OperandType bit field:

    OperandType encodes the operand flags supported. Due
    to the many different types of operands and some
    being subsets of others (for example, RegAl is a RegGPR),
    the values in the OperandType enum is stored as a bitfield.

    31                                                        0
    +-------+-------+----------+---------------+---------------+
    |   0   |  000  | 000 000  | 0 0000 0000 0 | 000 0000 0000 |
    +-------+-------+----------+---------------+---------------+
     Ignore | Class | Subclass | Operand Size  | Substructure

	                  111111000000000000000000000
					       1000000000000000000000
	Ignore
	------------------------------
	Some operands are used only to match instructions but are implied
	and not used to encode. If this bit is set, the operand will only
	be used for matching the instruction operand type.

	Class
	------------------------------
	This field specifies the type of operand:

		Memory, Immediate, Register, Pointer

	Subclass
	-------------------------------
	The subclass provides additional type information depending
	on the class.

		Immediate subclasses:

			Relative Immediate
				The immediate value is a relative offset from current location
			Immediate Pointer
				The immediate is a ptr16:16 or ptr32:32 form
			If the subclass is 0, this is just an immediate value

		Register subclasses:

			Stores the type of regiseter: control/data/test, GPR,
			segment, FPU, and others

	Operand size
	-------------------------------
	This encodes operand size information.

	Substructure
	-------------------------------
	Depending on Class and Subclass, this field includes additional
	information or a register type code.
*/

#define GetOperandIgnore(x)    (x & 0x80000000)
#define GetOperandClass(x)     (x & 0x38000000)
#define GetOperandSubclass(x)  (x & 0x7e00000)
#define GetOperandSubstruct(x) (x & 0x7ff)
#define GetOperandSize(x)      (x & 0x1ff800)
#define GetOperandSizeMask()   (0x1ff800)

// ignore bit
#define OperandIgnore 0x80000000

/* operand classes */
typedef enum OperandClass {
	OperandClassMem = 1 << 27,
	OperandClassImm = 2 << 27,
	OperandClassReg = 3 << 27,
	OperandClassPointer = 4 << 27
}OperandClass;

typedef enum OperandImmSubClass{
	OperandImmRel = 1 << 21, // rel8, rel16, and rel32 types.
	OperandImmPtr = 1 << 22  // ptr16:16, ptr16:32 types.
}OperandImmSubClass;

typedef enum OperandRegSubClass {
	OperandRegCDT = 1 << 21, // control, data, or test register.
	OperandRegGPR = 2 << 21, // general purpose register.
	OperandRegSeg = 3 << 21,
	OperandRegFpu = 4 << 21,
	OperandRegMmx = 5 << 21,
	OperandRegXmm = 6 << 21,
	OperandRegYmm = 7 << 21,
}OperandRegSubClass;

//typedef enum OperandStructureMem {
//	OperandMemOffs  = 1 << 20, // memory offset.
//	OperandMemIpRel = 2 << 20  // ip relative.
//}OperandStructureMem;

/* operand sizes. */
typedef enum OperandSize {
	OperandSize8 = 1 << 11,
	OperandSize16 = 1 << 12,
	OperandSize32 = 1 << 13,
	OperandSize64 = 1 << 14,
	OperandSize80 = 1 << 15,
	OperandSize128 = 1 << 16,
	OperandSize256 = 1 << 17,
	OperandSizeFar = 1 << 18,
	OperandSizeNear = 1 << 19,
	OperandSizeShort = 1 << 20
}OperandSize;

/* operand types. */
typedef enum OperandType {

	//
	// Instruction operand types
	//

	OperandPointer = OperandClassPointer,

	OperandImm8 = OperandClassImm | OperandSize8,
	OperandImm16 = OperandClassImm | OperandSize16,
	OperandImm32 = OperandClassImm | OperandSize32,
	OperandImm64 = OperandClassImm | OperandSize64,

	OperandSeg = OperandClassReg | OperandRegSeg | OperandSize16,

	OperandReg8 = OperandClassReg | OperandRegGPR | OperandSize8,
	OperandReg16 = OperandClassReg | OperandRegGPR | OperandSize16,
	OperandReg32 = OperandClassReg | OperandRegGPR | OperandSize32,
	OperandReg64 = OperandClassReg | OperandRegGPR | OperandSize64,

	OperandRm8 = OperandClassMem | OperandRegGPR | OperandSize8,
	OperandRm16 = OperandClassMem | OperandRegGPR | OperandSize16,
	OperandRm32 = OperandClassMem | OperandRegGPR | OperandSize32,
	OperandRm64 = OperandClassMem | OperandRegGPR | OperandSize64,

	OperandMem8  = OperandClassMem | OperandSize8,
	OperandMem16 = OperandClassMem | OperandSize16,
	OperandMem32 = OperandClassMem | OperandSize32,
	OperandMem64 = OperandClassMem | OperandSize64,
	OperandMem80 = OperandClassMem | OperandSize80,
	OperandMem128 = OperandClassMem | OperandSize128,

	OperandRel8 = OperandClassImm | OperandImmRel | OperandSize8,
	OperandRel16 = OperandClassImm | OperandImmRel | OperandSize16,
	OperandRel32 = OperandClassImm | OperandImmRel | OperandSize32,
	OperandRel64 = OperandClassImm | OperandImmRel | OperandSize64,

	OperandPtr16_16 = OperandClassImm | OperandImmPtr | OperandSize16,
	OperandPtr16_32 = OperandClassImm | OperandImmPtr | OperandSize32,

	OperandCs = OperandClassReg | OperandRegSeg | RegCs | OperandSize16,
	OperandDs = OperandClassReg | OperandRegSeg | RegDs | OperandSize16,
	OperandEs = OperandClassReg | OperandRegSeg | RegEs | OperandSize16,
	OperandSs = OperandClassReg | OperandRegSeg | RegSs | OperandSize16,
	OperandFs = OperandClassReg | OperandRegSeg | RegFs | OperandSize16,
	OperandGs = OperandClassReg | OperandRegSeg | RegGs | OperandSize16,
	OperandAl = OperandClassReg | OperandRegGPR | RegAl | OperandSize8,
	OperandAh = OperandClassReg | OperandRegGPR | RegAh | OperandSize8,
	OperandAx = OperandClassReg | OperandRegGPR | RegAx | OperandSize16,
	OperandEax = OperandClassReg | OperandRegGPR | RegEax | OperandSize32,
	OperandRax = OperandClassReg | OperandRegGPR | RegRax | OperandSize64,
	OperandBl = OperandClassReg | OperandRegGPR | RegBl | OperandSize8,
	OperandBh = OperandClassReg | OperandRegGPR | RegBh | OperandSize8,
	OperandBx = OperandClassReg | OperandRegGPR | RegBx | OperandSize16,
	OperandEbx = OperandClassReg | OperandRegGPR | RegEbx | OperandSize32,
	OperandRbx = OperandClassReg | OperandRegGPR | RegRbx | OperandSize64,
	OperandDl = OperandClassReg | OperandRegGPR | RegDl | OperandSize8,
	OperandDh = OperandClassReg | OperandRegGPR | RegDh | OperandSize8,
	OperandDx = OperandClassReg | OperandRegGPR | RegDx | OperandSize16,
	OperandEdx = OperandClassReg | OperandRegGPR | RegEdx | OperandSize32,
	OperandRdx = OperandClassReg | OperandRegGPR | RegRdx | OperandSize64,
	OperandCl = OperandClassReg | OperandRegGPR | RegCl | OperandSize8,
	OperandCh = OperandClassReg | OperandRegGPR | RegCh | OperandSize8,
	OperandCx = OperandClassReg | OperandRegGPR | RegCx | OperandSize16,
	OperandEcx = OperandClassReg | OperandRegGPR | RegEcx | OperandSize32,
	OperandRcx = OperandClassReg | OperandRegGPR | RegRcx | OperandSize64,
	OperandSp = OperandClassReg | OperandRegGPR | RegSp | OperandReg16,
	OperandEsp = OperandClassReg | OperandRegGPR | RegEsp | OperandReg32,
	OperandRsp = OperandClassReg | OperandRegGPR | RegRsp | OperandSize64,
	OperandBp = OperandClassReg | OperandRegGPR | RegBp | OperandReg16,
	OperandEbp = OperandClassReg | OperandRegGPR | RegEbp | OperandReg32,
	OperandRbp = OperandClassReg | OperandRegGPR | RegRbp | OperandSize64,
	OperandSi = OperandClassReg | OperandRegGPR | RegSi | OperandReg16,
	OperandEsi = OperandClassReg | OperandRegGPR | RegEsi | OperandReg32,
	OperandRsi = OperandClassReg | OperandRegGPR | RegRsi | OperandSize64,
	OperandDi = OperandClassReg | OperandRegGPR | RegDi | OperandReg16,
	OperandEdi = OperandClassReg | OperandRegGPR | RegEdi | OperandReg32,
	OperandRdi = OperandClassReg | OperandRegGPR | RegRdi | OperandSize64,
	OperandCr0 = OperandClassReg | OperandRegCDT | RegCr0,
	OperandCr1 = OperandClassReg | OperandRegCDT | RegCr1,
	OperandCr2 = OperandClassReg | OperandRegCDT | RegCr2,
	OperandCr3 = OperandClassReg | OperandRegCDT | RegCr3,
	OperandCr4 = OperandClassReg | OperandRegCDT | RegCr4,
	OperandCr5 = OperandClassReg | OperandRegCDT | RegCr5,
	OperandCr6 = OperandClassReg | OperandRegCDT | RegCr6,
	OperandCr7 = OperandClassReg | OperandRegCDT | RegCr7,
	OperandCr8 = OperandClassReg | OperandRegCDT | RegCr8,
	OperandCr9 = OperandClassReg | OperandRegCDT | RegCr9,
	OperandCr10 = OperandClassReg | OperandRegCDT | RegCr10,
	OperandCr11 = OperandClassReg | OperandRegCDT | RegCr11,
	OperandCr12 = OperandClassReg | OperandRegCDT | RegCr12,
	OperandCr13 = OperandClassReg | OperandRegCDT | RegCr13,
	OperandCr14 = OperandClassReg | OperandRegCDT | RegCr14,
	OperandCr15 = OperandClassReg | OperandRegCDT | RegCr15,
	OperandR8l = OperandClassReg | OperandRegGPR | RegR8l | OperandSize8,
	OperandR9l = OperandClassReg | OperandRegGPR | RegR9l | OperandSize8,
	OperandR10l = OperandClassReg | OperandRegGPR | RegR10l | OperandSize8,
	OperandR11l = OperandClassReg | OperandRegGPR | RegR11l | OperandSize8,
	OperandR12l = OperandClassReg | OperandRegGPR | RegR12l | OperandSize8,
	OperandR13l = OperandClassReg | OperandRegGPR | RegR13l | OperandSize8,
	OperandR14l = OperandClassReg | OperandRegGPR | RegR14l | OperandSize8,
	OperandR15l = OperandClassReg | OperandRegGPR | RegR15l | OperandSize8,
	OperandR8w = OperandClassReg | OperandRegGPR | RegR8w | OperandSize16,
	OperandR9w = OperandClassReg | OperandRegGPR | RegR9w | OperandSize16,
	OperandR10w = OperandClassReg | OperandRegGPR | RegR10w | OperandSize16,
	OperandR11w = OperandClassReg | OperandRegGPR | RegR11w | OperandSize16,
	OperandR12w = OperandClassReg | OperandRegGPR | RegR12w | OperandSize16,
	OperandR13w = OperandClassReg | OperandRegGPR | RegR13w | OperandSize16,
	OperandR14w = OperandClassReg | OperandRegGPR | RegR14w | OperandSize16,
	OperandR15w = OperandClassReg | OperandRegGPR | RegR15w | OperandSize16,
	OperandR8d = OperandClassReg | OperandRegGPR | RegR8d | OperandSize32,
	OperandR9d = OperandClassReg | OperandRegGPR | RegR9d | OperandSize32,
	OperandR10d = OperandClassReg | OperandRegGPR | RegR10d | OperandSize32,
	OperandR11d = OperandClassReg | OperandRegGPR | RegR11d | OperandSize32,
	OperandR12d = OperandClassReg | OperandRegGPR | RegR12d | OperandSize32,
	OperandR13d = OperandClassReg | OperandRegGPR | RegR13d | OperandSize32,
	OperandR14d = OperandClassReg | OperandRegGPR | RegR14d | OperandSize32,
	OperandR15d = OperandClassReg | OperandRegGPR | RegR15d | OperandSize32,
	OperandR8 = OperandClassReg | OperandRegGPR | RegR8 | OperandSize64,
	OperandR9 = OperandClassReg | OperandRegGPR | RegR9 | OperandSize64,
	OperandR10 = OperandClassReg | OperandRegGPR | RegR10 | OperandSize64,
	OperandR11 = OperandClassReg | OperandRegGPR | RegR11 | OperandSize64,
	OperandR12 = OperandClassReg | OperandRegGPR | RegR12 | OperandSize64,
	OperandR13 = OperandClassReg | OperandRegGPR | RegR13 | OperandSize64,
	OperandR14 = OperandClassReg | OperandRegGPR | RegR14 | OperandSize64,
	OperandR15 = OperandClassReg | OperandRegGPR | RegR15 | OperandSize64,

	OperandST0 = OperandClassReg | OperandRegFpu | RegSt0,
	OperandST1 = OperandClassReg | OperandRegFpu | RegSt1,
	OperandST2 = OperandClassReg | OperandRegFpu | RegSt2,
	OperandST3 = OperandClassReg | OperandRegFpu | RegSt3,
	OperandST4 = OperandClassReg | OperandRegFpu | RegSt4,
	OperandST5 = OperandClassReg | OperandRegFpu | RegSt5,
	OperandST6 = OperandClassReg | OperandRegFpu | RegSt6,
	OperandST7 = OperandClassReg | OperandRegFpu | RegSt7,

	OperandMmx0 = OperandClassReg | OperandRegMmx | RegMmx0,
	OperandMmx1 = OperandClassReg | OperandRegMmx | RegMmx1,
	OperandMmx2 = OperandClassReg | OperandRegMmx | RegMmx2,
	OperandMmx3 = OperandClassReg | OperandRegMmx | RegMmx3,
	OperandMmx4 = OperandClassReg | OperandRegMmx | RegMmx4,
	OperandMmx5 = OperandClassReg | OperandRegMmx | RegMmx5,
	OperandMmx6 = OperandClassReg | OperandRegMmx | RegMmx6,
	OperandMmx7 = OperandClassReg | OperandRegMmx | RegMmx7,

	OperandXmm0 = OperandClassReg | OperandRegXmm | RegXmm0,
	OperandXmm1 = OperandClassReg | OperandRegXmm | RegXmm1,
	OperandXmm2 = OperandClassReg | OperandRegXmm | RegXmm2,
	OperandXmm3 = OperandClassReg | OperandRegXmm | RegXmm3,
	OperandXmm4 = OperandClassReg | OperandRegXmm | RegXmm4,
	OperandXmm5 = OperandClassReg | OperandRegXmm | RegXmm5,
	OperandXmm6 = OperandClassReg | OperandRegXmm | RegXmm6,
	OperandXmm7 = OperandClassReg | OperandRegXmm | RegXmm7,
	OperandXmm8 = OperandClassReg | OperandRegXmm | RegXmm8,
	OperandXmm9 = OperandClassReg | OperandRegXmm | RegXmm9,
	OperandXmm10 = OperandClassReg | OperandRegXmm | RegXmm10,
	OperandXmm11 = OperandClassReg | OperandRegXmm | RegXmm11,
	OperandXmm12 = OperandClassReg | OperandRegXmm | RegXmm12,
	OperandXmm13 = OperandClassReg | OperandRegXmm | RegXmm13,
	OperandXmm14 = OperandClassReg | OperandRegXmm | RegXmm14,
	OperandXmm15 = OperandClassReg | OperandRegXmm | RegXmm15,

}OperandType;

typedef struct Register {
	char*        name;
	RegisterType type;
	int          id;
	OperandType  flags;
}Register, REG, *PREG;

typedef struct AddressMode {
	// default: 0 - indicates invalid value.
	int scale;
	// default: 0 - default.
	int displacement;
	Register* base;
	Register* index;
}AddressMode;

typedef struct InstrOperand {
	char               magic[4];
	OperandType        type;
	Register*          reg;
	DisplacementType   disp;
	SIZE               sizeover;
	AddressMode        memory;
	BOOL               isMemory;
	VALUE              val;
	char*              wrt;
	int                segto;
}InstrOperand;

typedef struct ModRM {
	int      mode;
	int      reg;
	int      regMem;
}ModRM;

typedef struct ScaleIndexBase {
	int     scale;
	int     base;
	int     index;
}ScaleIndexBase;

typedef enum InstrModifier {
	ModRegOpcode = 1,
	ModTTTN = 2,
	ModSegReg2Bit = 4,
	ModSegReg3Bit = 8,
	ModMemFormat = 0x10,
	ModMODRM = 0x20,
	ModSIB = 0x40,
	ModJmp8 = 0x80,
	ModEXT = 0x100,
	ModJmp = 0x200,
	ModSizeWord = 0x400,
	ModSizeDword = 0x800,
	ModSizeQword = 0x1000,
	ModEmitOpcodeOnly = 0x2000,
	ModPsuedo = 0x4000,
	ModAddrSizeWord = 0x8000,
	ModAddrSizeDword = 0x10000
}InstrModifier;

#define OpcodeExtNone (unsigned int)-1

typedef struct InstrTableEntry {
	Mnemonic mnemonic;
	unsigned int opcode;
	unsigned int opcodeExt;
	unsigned int numParms;
	OperandType parms[4];
	InstrModifier modifier;
}InstrTableEntry;

typedef enum InternalPrefixType {
	PrefixClass1_Lock = 0xf0,
	PrefixClass1_Repne = 0xf2,
	PrefixClass1_Rep = 0xf3,
	PrefixClass2_Cs = 0x2e,
	PrefixClass2_Ss = 0x36,
	PrefixClass2_Ds = 0x3e,
	PrefixClass2_Es = 0x26,
	PrefixClass2_Fs = 0x64,
	PrefixClass2_Gs = 0x65,
	PrefixClass2_NoBranch = 0x2e,
	PrefixClass2_Branch = 0x3e,
	PrefixClass3_Opsize = 0x66,
	PrefixClass4_Addrsize = 0x67,
	InternalPrefixTypeInvalid
}InternalPrefixType;

#define INSTR_OPERAND_MAX 4
#define INSTR_PREFIX_MAX 4

#define InvalidFarPtrSegment ((uint32_t)-1)

typedef struct INSTR {
	uint32_t           line;
	Mnemonic           mnemonic;
	InstrModifier      modifier;
	unsigned int       opcode;
	unsigned int       opcodeExt;
	ScaleIndexBase     sib;
	ModRM              modRM;
	size_t             prefixCount;
	size_t             operandCount;
	InternalPrefixType prefix[INSTR_PREFIX_MAX];
	InstrOperand*      operands;
	InstrTableEntry*   entry;
	SIZE               opsize;
	uint32_t           farPtrSeg;
}INSTR, *PINSTR;

/* Code Generator. */

typedef struct InternalInstrOp {
	Register*        base;
	Register*        index;
	unsigned int     scale;
	DisplacementType disp;
	TOKENTYPE        type;
	unsigned long long imm;
	char* wrt;
	/* segment relative to. */
	int segto;
}InternalInstrOp;

extern InstrTableEntry* MatchInstruction(IN Mnemonic mnemonic, IN SIZE s, IN int operandCount,
	IN OPTIONAL OperandType op1, IN OPTIONAL OperandType op2, IN OPTIONAL OperandType op3);
extern Register* GetRegister(IN RegisterType type);

/* listing.c */
extern PUBLIC void CreateListingFile(IN char* s);
extern PUBLIC void CloseListingFile(void);
extern void WriteListingFile(IN uint32_t offset, IN uint8_t* code, IN uint32_t size, IN PINSTR instr);

/* reg.c */
extern RegisterType IsRegister(IN char* str);
extern Register* GetRegister(IN RegisterType type);

/* key.c */
extern SIZE IsSizeSpecifier(IN char* str);
extern PREFIX IsPrefix(IN char* str);
extern POPERATOR GetOperator(IN char* s);

/*
=========================================

	TARGET

	Description: Common interface between the code
	generator and object file output.

=========================================
*/

typedef enum output_t {
	WRITE_RAWDATA,
	WRITE_ADDRESS,
	WRITE_RESERVE,
	WRITE_REL1ADR,
	WRITE_REL2ADR,
	WRITE_REL4ADR,
	WRITE_REL8ADR,
}output_t;

typedef enum SymbolFlags {
	SYM_DEFINED = 1,
	SYM_EXTERN = 2,
	SYM_GLOBAL = 4,
	SYM_AUTO = 8
}SymbolFlags;

typedef void      (*out_write_t)         (IN handle_t seg, IN void*, IN output_t, size_t, IN int segto, IN PASS);
typedef void      (*out_init_t)          (IN char* filename, IN PSTRING src);
typedef void      (*out_cleanup_t)       (void);
typedef void      (*out_def_t)           (IN PSTRING, IN handle_t, IN offset_t, IN SymbolFlags);
typedef handle_t  (*out_new_section_t)   (IN char* name, IN int flags);
typedef handle_t  (*out_section_t)       (IN char* name, IN PASS, IN BITS);
typedef void      (*out_section_align_t) (IN int32_t, IN unsigned int);
typedef offset_t  (*out_sect_off)        (IN PASS);
typedef handle_t  (*out_get_section_t)   (IN OPTIONAL char* name);
typedef BOOL      (*out_directive_t)     (IN PSTRING name, IN PLEX scn, IN OUT PTOKEN, IN PASS);
typedef void      (*out_stdmac_t)        (void);

typedef struct TARGET {
	char*             name;
	char*             info;
	out_init_t        init;
	out_cleanup_t     cleanup;
	out_def_t         addSymbol;
	out_sect_off      getOffset;
	out_write_t       out;
	out_new_section_t newSection;
	out_section_t     section;
	out_get_section_t getSection;
	out_directive_t   directive;
	out_stdmac_t      stdmac;
}TARGET, *PTARGET;

extern PUBLIC PTARGET GetOutputFormat(void);

/*
=========================================

	DEBUG

	Description: Common interface for symbolic
	debugging support.

=========================================
*/

typedef void  (*debug_init_t)      (IN PTARGET out);
typedef void  (*debug_cleanup_t)   (void);
typedef BOOL  (*debug_directive_t) (IN PSTRING name, IN PLEX scn, IN PTOKEN tok, IN PASS pass);
typedef void  (*debug_stdmac_t)    (void);

typedef struct DEBUG {
	char*             name;
	char*             info;
	debug_init_t      init;
	debug_cleanup_t   cleanup;
	debug_directive_t directive;
	debug_stdmac_t    stdmac;
}DEBUG, *PDEBUG;

extern PUBLIC PDEBUG GetDebugFormat(void);

/*
=========================================

	LISTING

	Description: Common interface between the
	code generator and listing file output.

=========================================
*/

typedef void   (*listgen_init_t)     (IN char*);
typedef void   (*listgen_cleanup_t)  (void);
typedef void   (*listgen_write_t)    (IN offset_t, IN void*, IN output_t, size_t);
typedef void   (*listgen_line_t)     (IN char* line, IN uint32_t lnum);

typedef struct LISTING {
	char*             name;
	listgen_init_t    init;
	listgen_cleanup_t cleanup;
	listgen_write_t   out;
	listgen_line_t    line;
}LISTING, *PLISTING;

extern PUBLIC PLISTING GetListGenerator(void);
extern PUBLIC PSTRING  GetLogicalFileName(void);

#endif
