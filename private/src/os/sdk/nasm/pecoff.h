/************************************************************************
*
*	pecoff.h - Portable Executable / Common Object File Format
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef PECOFF_H
#define PECOFF_H

typedef struct CoffFileHeader {
	uint16_t machine;
	uint16_t numberOfSections;
	uint32_t timeDateStamp;
	uint32_t pointerToSymbolTable;
	uint32_t numberOfSymbols;
	uint16_t sizeOfOptionalHeader;
	uint16_t characteristics;
}CoffFileHeader;

/* machine types. */
#define IMAGE_FILE_MACHINE_UNKNOWN 0
#define IMAGE_FILE_MACHINE_I386  0x14c
#define IMAGE_FILE_MACHINE_IA64 0x200
/* we don't support other machine types at this time. */

/* characteristics. */
#define IMAGE_FILE_RELOCS_STRIPPED 1
#define IMAGE_FILE_EXECUTABLE_IMAGE 2
#define IMAGE_FILE_LINE_NUMS_STRIPPED 4
#define IMAGE_FILE_LOCAL_SYMS_STRIPPED 8
#define IMAGE_FILE_AGGRESSIVE_WS_TRIM 0x10
#define IMAGE_FILE_LARGE_ADDRESS_AWARE 0x20
#define IMAGE_FILE_16BIT_MACHINE 0x40
#define IMAGE_FILE_BYTES_REVERSED_LO 0x80
#define IMAGE_FILE_32BIT_MACHINE 0x100
#define IMAGE_FILE_DEBUG_STRIPPED 0x200
#define IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP 0x400
#define IMAGE_FILE_SYSTEM 0x1000
#define IMAGE_FILE_DLL 0x2000
#define IMAGE_FILE_UP_SYSTEM_ONLY 0x4000
#define IMAGE_FILE_BYTES_REVERSED_HI 0x8000

typedef struct CoffSectionHeader {
	uint8_t name[8];
	uint32_t virtualSize;
	uint32_t virtualAddress;
	uint32_t sizeOfRawData;
	uint32_t pointerToRawData;
	uint32_t pointerToRelocations;
	uint32_t pointerToLineNumbers;
	uint16_t numberOfRelocations;
	uint16_t numberOfLineNumbers;
	uint32_t characteristics;
}CoffSectionHeader;

/* characteristics. */
#define IMAGE_SCN_TYPE_NO_PAD 8
#define IMAGE_SCN_CNT_CODE 0x20
#define IMAGE_SCN_CNT_INITIALIZED_DATA 0x40
#define IMAGE_SCN_CNT_UNINITIALIZED_DATA 0x80
#define IMAGE_SCN_LNK_INFO 0x200
#define IMAGE_SCN_LNK_REMOVE 0x800
#define IMAGE_SCN_LNK_COMDAT 0x1000
#define IMAGE_SCN_ALIGN_1BYTES 0x100000
#define IMAGE_SCN_ALIGN_2BYTES 0x200000
#define IMAGE_SCN_ALIGN_4BYTES 0x300000
#define IMAGE_SCN_ALIGN_8BYTES 0x400000
#define IMAGE_SCN_ALIGN_16BYTES 0x500000
#define IMAGE_SCN_ALIGN_32BYTES 0x600000
#define IMAGE_SCN_ALIGN_64BYTES 0x700000
#define IMAGE_SCN_ALIGN_128BYTES 0x800000
#define IMAGE_SCN_ALIGN_256BYTES 0x900000
#define IMAGE_SCN_ALIGN_512BYTES 0xa00000
#define IMAGE_SCN_ALIGN_1024BYTES 0xb00000
#define IMAGE_SCN_ALIGN_2048BYTES 0xc00000
#define IMAGE_SCN_ALIGN_4096BYTES 0xd00000
#define IMAGE_SCN_ALIGN_8192BYTES 0xe00000
#define IMAGE_SCN_LNK_NRELOC_OVFL 0x1000000
#define IMAGE_SCN_MEM_DISCARDABLE 0x2000000
#define IMAGE_SCN_MEM_NOT_CACHED 0x4000000
#define IMAGE_SCN_MEM_NOT_PAGED 0x8000000
#define IMAGE_SCN_MEM_SHARED 0x10000000
#define IMAGE_SCN_MEM_EXECUTE 0x20000000
#define IMAGE_SCN_MEM_READ 0x40000000
#define IMAGE_SCN_MEM_WRITE 0x80000000

/* relocation types. */

/* absolute */
#define IMAGE_REL_I386_ABSOLUTE 0

/* 32 bit target VA */
#define IMAGE_REL_I386_DIR32 6

/* 32 bit target RVA */
#define IMAGE_REL_I386_DIR32NB 7

/* 16 bit section index of the section that contains the target. Used
for debugging information */
#define IMAGE_REL_I386_SECTION 0xa

/* 32 bit offset of the target from the beginning of its section.
Used for debugging information and static thread local storage */
#define IMAGE_REL_I386_SECREL 0xb

/* 32 bit relative displacement to the target. This supports
x86 relative branch and call instructions */
#define IMAGE_REL_I386_REL32 0x14

/* symbol number values. */
#define IMAGE_SYM_UNDEFINED 0
#define IMAGE_SYM_ABSOLUTE -1
#define IMAGE_SYM_DEBUG -2

/* complex type. */
#define IMAGE_SYM_DTYPE_NULL 0
#define IMAGE_SYM_DTYPE_POINTER 1
#define IMAGE_SYM_DTYPE_FUNCTION 2
#define IMAGE_SYM_DTYPE_ARRAY 3

/* storage class values. */
#define IMAGE_SYM_CLASS_END_OF_FUNCTION -1
#define IMAGE_SYM_CLASS_NULL 0
#define IMAGE_SYM_CLASS_AUTOMATIC 1
#define IMAGE_SYM_CLASS_EXTERNAL 2
#define IMAGE_SYM_CLASS_STATIC 3
#define IMAGE_SYM_CLASS_REGISTER 4
#define IMAGE_SYM_CLASS_EXTERNAL_DEF 5
#define IMAGE_SYM_CLASS_LABEL 6
#define IMAGE_SYM_CLASS_UNDEFINED_LABEL 7
#define IMAGE_SYM_CLASS_MEMBER_OF_STRUCT 8
#define IMAGE_SYM_CLASS_ARGUMENT 9
#define IMAGE_SYM_CLASS_STRUCT_TAG 10
#define IMAGE_SYM_CLASS_MEMBER_OF_UNION 11
#define IMAGE_SYM_CLASS_UNION_TAG 12
#define IMAGE_SYM_CLASS_TYPE_DEFINITION 13
#define IMAGE_SYM_CLASS_UNDEFINED_STATIC 14
#define IMAGE_SYM_CLASS_ENUM_TAG 15
#define IMAGE_SYM_CLASS_MEMBER_OF_ENUM 16
#define IMAGE_SYM_CLASS_REGISTER_PARAM 17
#define IMAGE_SYM_CLASS_BIT_FIELD 18
#define IMAGE_SYM_CLASS_BLOCK 100
#define IMAGE_SYM_CLASS_FUNCTION 101
#define IMAGE_SYM_CLASS_END_OF_STRUCT 102
#define IMAGE_SYM_CLASS_FILE 103
#define IMAGE_SYM_CLASS_SECTION 104
#define IMAGE_SYM_CLASS_WEAK_EXTERNAL 105

#endif
