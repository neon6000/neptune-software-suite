/************************************************************************
*
*	parse.c - Parser.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nasm.h"
#include "instr.h"
#define IN
#define OUT
#include <assert.h>
#include <malloc.h>
#include <string.h>

PRIVATE char* GetRegisterName(IN RegisterType type) {

	/* we use a map here rather then a direct lookup table
	so its not dependent on the constant values of RegisterType. */
	struct {
		RegisterType t; char* n;
	}map[] = {
		{ RegAh, "ah" }, { RegBh, "bh" }, { RegCh, "ch" }, { RegDh, "dh" },
		{ RegAl, "al" }, { RegBl, "bl" }, { RegCl, "cl" }, { RegDl, "dl" },
		{ RegSi, "si" }, { RegDi, "di" }, { RegBp, "bp" }, { RegSp, "sp" },
		{ RegAx, "ax" }, { RegBx, "bx" }, { RegCx, "cx" }, { RegDx, "dx" },
		{ RegEax, "eax" }, { RegEbx, "ebx" }, { RegEcx, "ecx" }, { RegEdx, "edx" },
		{ RegEsi, "esi" }, { RegEdi, "edi" }, { RegEbp, "ebp" }, { RegEsp, "esp" },
		{ RegSs, "ss" }, { RegCs, "cs" }, { RegDs, "ds" }, { RegEs, "es" },
		{ RegFs, "fs" }, { RegGs, "gs" }, { RegCr0, "cr0" }, { RegCr1, "cr1" },
		{ RegCr2, "cr2" }, { RegCr3, "cr3" }, { RegCr4, "cr4" }, { RegInvalid, "" }
	};
	int c = 0;
	if (type == RegInvalid)
		return "<RegInvalid>";
	while (TRUE) {
		if (map[c].t == RegInvalid)
			return NULL;
		if (map[c].t == type)
			return map[c].n;
		c++;
	}
}

PRIVATE char GetOper(IN TOKENTYPE type) {

	switch (type) {
	case TokenPlus:
	case TokenMinus:
	case TokenStar:
	case TokenFowardSlash: return (char)type;
	default: return '?';
	};
}

PRIVATE char* GetSizeSpecifier(IN SIZE sp) {
	switch (sp) {
	case SizeByte: return "byte";
	case SizeWord: return "word";
	case SizeDword: return "dword";
	case SizeQword: return "qword";
	case SizeTbyte: return "tbyte";
	default: return "<SizeInvalid>";
	};
}

PRIVATE PPARSEDINSTROP new_instr_oper_str(IN PARSEDINSTROPTYPE type, IN PSTRING val) {

	PPARSEDINSTROP op = (PPARSEDINSTROP)malloc(sizeof(PARSEDINSTROP));
	memset(op, 0, sizeof(PARSEDINSTROP));
	op->type = type;
	op->str = val;
	return op;
}

PRIVATE PPARSEDINSTROP new_instr_oper(IN BOOL isMemory, IN SIZE size,
	IN RegisterType segmentOverride, IN unsigned long farPtrSeg, IN PEXPR expr) {

	PPARSEDINSTROP op = (PPARSEDINSTROP)malloc(sizeof(PARSEDINSTROP));
	op->type = OPEXPR;
	op->expr = expr;
	op->farPtrSeg = farPtrSeg;
	op->isMemory = isMemory;
	op->segmentOverride = segmentOverride;
	op->size = size;
	op->next = NULL;
	return op;
}

PRIVATE PPARSEDINSTR new_instr(IN PSTRING label, IN long times, IN PREFIX* prefix, IN size_t prefixCount, IN Mnemonic mnemonic, IN PPARSEDINSTROP operands) {

	PPARSEDINSTR instr;
	size_t i = 0;
	assert(prefixCount <= 4);

	instr = (PPARSEDINSTR)malloc(sizeof(PARSEDINSTR));
	instr->label = label;
	instr->mnemonic = mnemonic;
	instr->operands = operands;
	instr->times = times;
	for (i = 0; i < 4; i++)
		instr->prefix[i] = PrefixInvalid;
	for (i = 0; i < prefixCount; i++)
		instr->prefix[i] = prefix[i];
	return instr;
}

/* Back end ----- */

TOKEN _currentToken;
BOOL  _errors;
BOOL _initialized = FALSE;

PRIVATE TOKEN get(void) {

	return _currentToken;
}

PRIVATE TOKEN next(void) {

	StdScan(&_currentToken);
	return _currentToken;
}

PRIVATE BOOL accept(IN TOKENTYPE type) {

	if (_currentToken.type == type)
		return TRUE;
	return FALSE;
}

PRIVATE BOOL expect(IN TOKENTYPE type) {

	if (accept(type))
		return TRUE;
	Error("Syntax error near %t", _currentToken);
	return FALSE;
}

/* Grammar --- */

// operand := [SizeSpecifier] '[' [segment_register ':'] expression ']'
// operand := [SizeSpecifier] [number ':'] expression)
PRIVATE PPARSEDINSTROP Operand(void) {

	SIZE           sp;
	RegisterType   segmentOverride;
	PEXPR          expr;
	unsigned long  farPtrOff;

	sp = SizeInvalid;
	farPtrOff = InvalidFarPtrSegment;
	segmentOverride = RegInvalid;

	//
	// get the size specifier (like "word" or "word ptr")
	//
	if (accept(TokenSizeSpecifier)) {

		sp = get().value.size;
		next();

		// if this is someting like "word ptr" silently
		// accept the "ptr" for compatibility:
		if (accept(TokenPtr))
			next();
	}

	//
	// '[' <expr> ']' => Memory operand
	//
	if (accept(TokenLeftBracket)) {
		next();
		/* look ahead for [segment_register ':']. This
		is for segment overrides. */

		RegisterType t;
		TOKEN        tok;

		t = 0;
		tok = get();

		if (tok.type == TokenRegister) {
			switch (tok.value.reg){
			case RegCs: t = RegCs; break;
			case RegDs: t = RegDs; break;
			case RegSs: t = RegSs; break;
			case RegEs: t = RegEs; break;
			case RegFs: t = RegFs; break;
			case RegGs: t = RegGs; break;
			};
			if (t) {
				segmentOverride = t;
				next();
				expect(TokenColon);
				next();
				tok = _currentToken;
			}
		}

		expr = Expr(StdScan, &tok);
		_currentToken = tok;

		expect(TokenRightBracket);
		next();
		return new_instr_oper(TRUE, sp, segmentOverride, farPtrOff, expr);
	}

	//
	//  [number ':'] <expr> => Non-memory operand
	//
	else {
		expr = Expr(StdScan, &_currentToken);

//		if (!IsScalar(expr))
//			Error("Scalar expression expected");

		// for instructions like jmp 0x7c0:0
		if (accept(TokenColon)) {
			if (!IsNumber(expr)) {
				Error("Scalar must be numeric");
			}
			else{
				farPtrOff = expr->val;
				next();
				expr = Expr(StdScan, &_currentToken);
			}
			if (!IsNumber(expr))
				Error("Scalar must be numeric");
		}
		return new_instr_oper(FALSE, sp, RegInvalid, farPtrOff, expr);
	}
}

// operand := string | float | expression
PRIVATE PPARSEDINSTROP PseudoOperand(void) {

	PEXPR expr;
	PPARSEDINSTROP op;

	switch (get().type) {
	case TokenString:
		op = new_instr_oper_str(OPSTR, get().value.s);
		next();
		break;
	case TokenFloat:
		op = new_instr_oper_str(OPFLOAT, get().value.s);
		next();
		break;
	default:
		expr = Expr(StdScan, &_currentToken);
		op = new_instr_oper(FALSE, 0, RegInvalid, 0, expr);
	}
	return op;
}

// operand_list := operand {',' operand}
PRIVATE PPARSEDINSTROP OperandList(IN BOOL isPsuedo) {

	PPARSEDINSTROP a, b;
	PPARSEDINSTROP first;

	first = isPsuedo ? PseudoOperand() : Operand();
	a = first->next;
	b = first;

	while (accept(TokenComma)) {
		next();
		a = isPsuedo ? PseudoOperand() : Operand();
		b->next = a;
		a = a->next;
		b = b->next;
	}
	return first;
}

extern int _pass;

// instruction_line := ['times' expression] [{Prefix}] Mnemonic [{operand_list}]
PRIVATE PPARSEDINSTR InstructionLine(IN OPTIONAL PSTRING label, IN OPTIONAL Mnemonic mnemonic) {

	size_t         prefixCount;
	long           times;

	PREFIX prefix[4] = {
		PrefixInvalid, PrefixInvalid,
		PrefixInvalid, PrefixInvalid
	};

	if (mnemonic == MnemonicInvalid) {

		if (!(accept(TokenPrefix) || accept(TokenMnemonic) || accept(TokenTimes)))
			return new_instr(label, 0, NULL, 0, MnemonicInvalid, NULL);

		times = 1;
		if (accept(TokenTimes)) {
			next();
			times = Eval(StdScan, &_currentToken);
		}

		prefixCount = 0;
		while (accept(TokenPrefix)) {
			if (prefixCount == 4)
				Error("Prefix count exceeded near %t", get());
			prefix[prefixCount++] = get().value.prefix;
			next();
		}

		if (!expect(TokenMnemonic))
			Error("Expected instruction near %t", get());

		mnemonic = get().value.mnemonic;
		next();
	}

	if (accept(TokenEndOfLine) || accept(TokenEndOfFile))
		return new_instr(label, times, prefix, prefixCount, mnemonic, NULL);

	switch (mnemonic) {
	case DB: case DW: case DD: case DQ: case DO:
	case RESB: case RESW: case RESD: case RESQ:
		return new_instr(label, times, prefix, prefixCount, mnemonic, OperandList(TRUE));
	}

	return new_instr(label, times, prefix, prefixCount, mnemonic, OperandList(FALSE));
}

// line_label := [identifier [':']] [line] EOL;
PRIVATE PPARSEDINSTR LineLabel(void) {

	PSTRING      label;
	Mnemonic     m;
	PPARSEDINSTR line;
	BOOL         seenColon;

	line = NULL;
	label = NULL;
	seenColon = FALSE;
	m = MnemonicInvalid;

	if (accept(TokenIdentifier)) {
		label = get().value.s;
		next();
		if (accept(TokenColon)) {
			seenColon = TRUE;
			next();
		}
		m = GetMnemonic(label->value);
	}

	//
	// If the label matches an instruction (like "add: ...."
	// and is followed by a colon : we treat it as a label:
	//
	if (label && seenColon && m != MnemonicInvalid)
		m = MnemonicInvalid;
	
	//
	// If the label matches an instruction and is NOT
	// followed by a colon : we treat it as a mnemonic:
	//
	else if (label && m != MnemonicInvalid)
		label = NULL;

	line = InstructionLine(label, m);

	if (accept(TokenEndOfFile)) return line;
	expect(TokenEndOfLine);
	return line;
}

PRIVATE BOOL SkipLine(void) {

	while (get().type != TokenEndOfLine) {
		next();
		if (accept(TokenEndOfFile))
			return FALSE;
	}
	return TRUE;
}

/* bugfix for Start -- this skips over empty lines. */
PRIVATE BOOL SkipBlankLines(void) {

	while (get().type == TokenEndOfLine) {
		next();
		if (accept(TokenEndOfFile))
			return FALSE;
	}
	return TRUE;
}

/* Front end ---- */

//
// preproc.c: line start flag
//
extern PUBLIC BOOL _lineStart;

PUBLIC PPARSEDINSTR Parse(IN PASS pass) {

	PPARSEDINSTR instr;

	instr = NULL;
	_lineStart = TRUE;

	next();
	do {
		SkipBlankLines();
		if (_currentToken.type == TokenLeftBracket) {
			if (!Directive(Lex, &_currentToken, pass))
				SkipLine();
			continue;
		}
		if (accept(TokenEndOfLine)) {
			next();
			continue;
		}
		else if (accept(TokenEndOfFile)) break;
		instr = LineLabel();
	} while (!instr);
	_lineStart = FALSE;
	return instr;
}

PUBLIC void ResetCurrentToken(void) {

	memset(&_currentToken, 0, sizeof(TOKEN));
}

/*

Current Grammar:

    operand          := [SizeSpecifier] '[' [segment_register ':'] expression ']'
	operand          := [SizeSpecifier] expression [':' expression]
	operand_list     := operand { ',' operand }
	instruction_line := ['times' expression] {Prefix} ] Mnemonic [ {operand_list} ]

	line_label  := [identifier [':']] [line] EOL;
	start       := { line_label };

Expressions:

	expression  := term { ("+" | "-") term }
	term        := factor { ("*" | "/") factor }
	factor      := expr_base | "(" expression ")"
	expr_base   := Integer | Identifier | String | Register | '$' | '$$'
	|  ("+" | "-") expression
*/

