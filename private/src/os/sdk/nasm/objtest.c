/************************************************************************
*
*	obj.c - PE/COFF Object File Support.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <malloc.h>
#include <string.h>
#include "nasm.h"
#include "pecoff.h"

typedef struct Relocation {
	uint32_t virtualAddress;
	uint32_t symbolTableIndex;
	uint16_t type;
}Relocation;

typedef struct SymbolOffset {
	unsigned long zero;
	unsigned long offset;
}SymbolOffset;

typedef struct Symbol {
	union {
		char shortname[8];
		SymbolOffset e;
	}u;
	int32_t value;
	int16_t section;
	int16_t type;
	int8_t storageClass;
	int8_t numAuxSymbols;
}Symbol;

typedef struct SymbolAuxFunction {
	uint32_t tagIndex;
	uint32_t totalSize;
	uint32_t pointerToLineNumber;
	uint32_t pointerToNextFunction;
	uint16_t unused;
}SymbolAuxFunction;

typedef struct SymbolAuxBF_EF {
	uint32_t unused;
	uint16_t lineNumber;
	uint8_t unused2[6];
	uint32_t pointerToNextFunction; // SymbolAuxBF only.
	uint16_t unused3;
}SymbolAuxBF, SymbolAuxEF;

typedef struct SymbolAuxWeakExternal {
	uint32_t tagIndex;
	uint32_t characteristics;
	uint8_t unused[10];
}SymbolAuxWeakExternal;

typedef struct SymbolAuxFile {
	char fileName[18];
}SymbolAuxFile;

typedef struct SymbolAuxSectionDef {
	uint32_t length;
	uint16_t numberOfRelocations;
	uint16_t numberOfLineNumbers;
	uint32_t checksum;
	uint16_t number;  // one based index into section table. Used when COMDAT selection = 5.
	uint8_t selection; // COMDAT selection number.
	uint8_t unused[3];
}SymbolAuxSectionDef;

typedef struct Section {
	char name[9];
	int index;
	unsigned long offset;
	PBUFFER data;
	int32_t flags;
	int32_t pos;
	int32_t relpos;
}Section;

typedef struct CoffStringTableHeader {
	uint32_t totalSize;
}CoffStringTableHeader;

/* The string table follows the symbol table. */
offset_t CoffGetStringTable(IN uint32_t pointerToSymbolTable, IN uint32_t numberOfSymbols) {

	return pointerToSymbolTable + numberOfSymbols * sizeof(Symbol);
}

char* _stringTable;

/* Loads the string table. */
char* CoffLoadStringTable(IN FILE* file, IN CoffFileHeader header) {

	CoffStringTableHeader stringTable;
	offset_t symbolTableOffset;
	long location;
	char* strings;

	strings = NULL;
	symbolTableOffset = CoffGetStringTable(header.pointerToSymbolTable, header.numberOfSymbols);

	location = ftell(file);
	fseek(file, symbolTableOffset, SEEK_SET);
	fread(&stringTable, sizeof(CoffStringTableHeader), 1, file);

	strings = malloc(stringTable.totalSize);
	fread(strings, 1, stringTable.totalSize, file);
	fseek(file, location, SEEK_SET);

	_stringTable = strings;

	return strings;
}

/* Given an offset of a string, return the string from the String Table. */
char* CoffGetStringFromStringTable(IN offset_t offset) {

	/* the offset of the string is relative to the base of the entire string
	table, including header. */
	return _stringTable + offset - sizeof(CoffStringTableHeader);
}

/* Dump the current symbol information. */
Symbol dumpCoffSymbol(IN FILE* file) {

	Symbol symbol;
	fread(&symbol, sizeof(Symbol), 1, file);
	if (symbol.u.shortname[0] != 0) {
		char name[9];
		memcpy(name, symbol.u.shortname, 8);
		name[8] = 0;
		printf("\n\r%s", name);
	}
	else
		printf("\n\r%s", CoffGetStringFromStringTable(symbol.u.e.offset));
	printf("\n\r   Value: %x (%i)", symbol.value, symbol.value);
	printf("\n\r   Section number: %i", symbol.section);
	printf("\n\r   Base type: %x", symbol.type);
	printf("\n\r   Storage class: %x", symbol.storageClass);
	printf("\n\r   Number of aux symbols: %i", symbol.numAuxSymbols);
	return symbol;
}

/* Dump symbol table. */
void dumpFileSymbolTable(IN FILE* file, IN int pointerToSymbolTable, IN int numberOfSymbols) {

	Symbol symbol;

	printf("\n\n\rSymbol table ----------------\n\n\r");
	fseek(file, pointerToSymbolTable, SEEK_SET);
	while (numberOfSymbols--) {
		symbol = dumpCoffSymbol(file);
		/* skip over auxiliary symbol information. */
		while (symbol.numAuxSymbols--) {
			Symbol aux;
			fread(&aux, sizeof(Symbol), 1, file);
			numberOfSymbols--;
		}
	}
}

/* Dump relocation information. */
void CoffDumpRelocations(IN FILE* file, IN long relocCount, IN offset_t sectionRelocOffset) {

	long location;
	Relocation* reloc;
	long i;

	reloc = (Relocation*)malloc(relocCount * sizeof(Relocation));

	location = ftell(file);
	fseek(file, sectionRelocOffset, SEEK_SET);
	fread((void*)reloc, sizeof(Relocation), relocCount, file);
	fseek(file, location, SEEK_SET);

	for (i = 0; i < relocCount; i++) {

		printf("\n\r  Relocation:\n\r");
		printf("\n\r     Virtual Address: 0x%x", reloc[i].virtualAddress);
		printf("\n\r     Type: %i", reloc[i].type);
		printf("\n\r     Symbol Table Index: %i", reloc[i].symbolTableIndex);
	}
}

void dumpStabs(IN FILE* file, IN CoffSectionHeader* stab, IN CoffSectionHeader* stabstr) {

	// from stab.c
	typedef struct STABHDR {
		uint32_t  strx;
		uint8_t   type;
		uint8_t   other;
		uint16_t  desc;
		uint32_t  value;
	}STABHDR, *PSTABHDR;

	long     location;
	char*    strtable;
	STABHDR  hdr;
	offset_t offset;

	location = ftell(file);

	strtable = malloc(stabstr->sizeOfRawData);

	fseek(file, stabstr->pointerToRawData, SEEK_SET);
	fread(strtable, 1, stabstr->sizeOfRawData, file);

	printf("\n\rSTABS");
	printf("\n\r-------------------------\n\r");

	printf("\n\r%-8s%-8s%-8s%-10s%s", "TYPE", "DESC", "OTHER", "VALUE", "STRX");

	fseek(file, stab->pointerToRawData, SEEK_SET);
	for (offset = 0; offset < stab->sizeOfRawData; offset += sizeof(STABHDR)) {

		fread(&hdr, 1, sizeof(STABHDR), file);

		printf("\n\r%-8X%-8i%-8i%-10X%s",
			hdr.type, hdr.desc, hdr.other, hdr.value,
			hdr.strx ? &strtable[hdr.strx] : "0");
	}

	free(strtable);
	fseek(file, location, SEEK_SET);
}

/* Dump current section header. */
CoffSectionHeader dumpFileSection(IN FILE* file) {

	CoffSectionHeader header;

	fread(&header, sizeof(CoffSectionHeader), 1, file);

	printf("\n\rSection name: %s", header.name);
	printf("\n\r   Virtual size: %x", header.virtualSize);
	printf("\n\r   Virtual address: %x", header.virtualAddress);
	printf("\n\r   Size to raw data: %x", header.sizeOfRawData);
	printf("\n\r   Pointer to raw data: %x", header.pointerToRawData);
	printf("\n\r   Pointer to relocations: %x", header.pointerToRelocations);
	printf("\n\r   Pointer to line numbers: %x", header.pointerToLineNumbers);
	printf("\n\r   Number of relocations: %x", header.numberOfRelocations);
	printf("\n\r   Number of line numbers: %x", header.numberOfLineNumbers);
	printf("\n\r   Characteristics: %x", header.characteristics);
	if (header.characteristics & IMAGE_SCN_TYPE_NO_PAD)
		printf(" IMAGE_SCN_TYPE_NO_PAD");
	if ((header.characteristics & IMAGE_SCN_CNT_CODE) == IMAGE_SCN_CNT_CODE)
		printf(" IMAGE_SCN_CNT_CODE");
	if ((header.characteristics & IMAGE_SCN_CNT_INITIALIZED_DATA) == IMAGE_SCN_CNT_INITIALIZED_DATA)
		printf(" IMAGE_SCN_CNT_INITIALIZED_DATA");
	if ((header.characteristics & IMAGE_SCN_CNT_UNINITIALIZED_DATA) == IMAGE_SCN_CNT_UNINITIALIZED_DATA)
		printf(" IMAGE_SCN_CNT_UNINITIALIZED_DATA");
	if ((header.characteristics & IMAGE_SCN_LNK_INFO) == IMAGE_SCN_LNK_INFO)
		printf(" IMAGE_SCN_LNK_INFO");
	if ((header.characteristics & IMAGE_SCN_LNK_REMOVE) == IMAGE_SCN_LNK_REMOVE)
		printf(" IMAGE_SCN_CNT_CODE");
	if ((header.characteristics & IMAGE_SCN_LNK_COMDAT) == IMAGE_SCN_LNK_COMDAT)
		printf(" IMAGE_SCN_LNK_COMDAT");
	if ((header.characteristics & IMAGE_SCN_ALIGN_1BYTES) == IMAGE_SCN_ALIGN_1BYTES)
		printf(" IMAGE_SCN_ALIGN_1BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_2BYTES) == IMAGE_SCN_ALIGN_2BYTES)
		printf(" IMAGE_SCN_ALIGN_2BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_4BYTES) == IMAGE_SCN_ALIGN_4BYTES)
		printf(" IMAGE_SCN_ALIGN_4BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_8BYTES) == IMAGE_SCN_ALIGN_8BYTES)
		printf(" IMAGE_SCN_ALIGN_8BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_16BYTES) == IMAGE_SCN_ALIGN_16BYTES)
		printf(" IMAGE_SCN_ALIGN_16BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_32BYTES) == IMAGE_SCN_ALIGN_32BYTES)
		printf(" IMAGE_SCN_ALIGN_32BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_64BYTES) == IMAGE_SCN_ALIGN_64BYTES)
		printf(" IMAGE_SCN_ALIGN_64BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_128BYTES) == IMAGE_SCN_ALIGN_128BYTES)
		printf(" IMAGE_SCN_ALIGN_128BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_256BYTES) == IMAGE_SCN_ALIGN_256BYTES)
		printf(" IMAGE_SCN_ALIGN_256BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_1024BYTES) == IMAGE_SCN_ALIGN_1024BYTES)
		printf(" IMAGE_SCN_ALIGN_1024BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_2048BYTES) == IMAGE_SCN_ALIGN_2048BYTES)
		printf(" IMAGE_SCN_ALIGN_2048BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_4096BYTES) == IMAGE_SCN_ALIGN_4096BYTES)
		printf(" IMAGE_SCN_ALIGN_4096BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_8192BYTES) == IMAGE_SCN_ALIGN_8192BYTES)
		printf(" IMAGE_SCN_ALIGN_8192BYTES");
	if ((header.characteristics & IMAGE_SCN_LNK_NRELOC_OVFL) == IMAGE_SCN_LNK_NRELOC_OVFL)
		printf(" IMAGE_SCN_LNK_NRELOC_OVFL");
	if ((header.characteristics & IMAGE_SCN_MEM_DISCARDABLE) == IMAGE_SCN_MEM_DISCARDABLE)
		printf(" IMAGE_SCN_MEM_DISCARDABLE");
	if ((header.characteristics & IMAGE_SCN_MEM_NOT_CACHED) == IMAGE_SCN_MEM_NOT_CACHED)
		printf(" IMAGE_SCN_MEM_NOT_CACHED");
	if ((header.characteristics & IMAGE_SCN_MEM_NOT_PAGED) == IMAGE_SCN_MEM_NOT_PAGED)
		printf(" IMAGE_SCN_MEM_NOT_PAGED");
	if ((header.characteristics & IMAGE_SCN_MEM_SHARED) == IMAGE_SCN_MEM_SHARED)
		printf(" IMAGE_SCN_MEM_SHARED");
	if ((header.characteristics & IMAGE_SCN_MEM_EXECUTE) == IMAGE_SCN_MEM_EXECUTE)
		printf(" IMAGE_SCN_MEM_EXECUTE");
	if ((header.characteristics & IMAGE_SCN_MEM_READ) == IMAGE_SCN_MEM_READ)
		printf(" IMAGE_SCN_MEM_READ");
	if ((header.characteristics & IMAGE_SCN_MEM_WRITE) == IMAGE_SCN_MEM_WRITE)
		printf(" IMAGE_SCN_MEM_WRITE");

	if (header.numberOfRelocations > 0)
		CoffDumpRelocations(file, header.numberOfRelocations, header.pointerToRelocations);

	return header;
}

/* Dump all sections. */
BOOL dumpFileSections(IN FILE* file, IN int sectionCount, OUT CoffSectionHeader* stab,
	OUT CoffSectionHeader* stabstr) {

	CoffSectionHeader hdr;
	BOOL debug;

	debug = FALSE;
	while (sectionCount--) {
		hdr = dumpFileSection(file);
		if (!strcmp(hdr.name, ".stab")) {
			memcpy(stab, &hdr, sizeof(CoffSectionHeader));
			debug = TRUE;
		}
		if (!strcmp(hdr.name, ".stabstr")) {
			memcpy(stabstr, &hdr, sizeof(CoffSectionHeader));
			debug = TRUE;
		}
		printf("\n\r---------------------------\n\r");
	}
	return debug;
}

/* Dump COFF file header. */
CoffFileHeader dumpCoffFileHeader(IN FILE* file) {

	CoffFileHeader header;

	fread(&header, sizeof(CoffFileHeader), 1, file);
	printf("\n\rMachine: %x", header.machine);
	if (header.machine == 0) printf(" unknown");
	else if (header.machine == IMAGE_FILE_MACHINE_I386) printf(" i386");
	else if (header.machine == IMAGE_FILE_MACHINE_IA64) printf(" ia64");
	else printf("unsupported machine type.");
	printf("\n\rNumber of sections: %x", header.numberOfSections);
	printf("\n\rTime date stamp: %x", header.timeDateStamp);
	printf("\n\rPointer to symbol table: %x", header.pointerToSymbolTable);
	printf("\n\rNumber of symbols: %x", header.numberOfSymbols);
	printf("\n\rSize of optional header: %x", header.sizeOfOptionalHeader);
	printf("\n\rCharacteristics: %x", header.characteristics);
	if ((header.characteristics & IMAGE_FILE_RELOCS_STRIPPED) != 0)
		printf(" IMAGE_FILE_RELOCS_STRIPPED");
	if ((header.characteristics & IMAGE_FILE_EXECUTABLE_IMAGE) != 0)
		printf(" IMAGE_FILE_EXECUTABLE_IMAGE");
	if ((header.characteristics & IMAGE_FILE_LINE_NUMS_STRIPPED) != 0)
		printf(" IMAGE_FILE_LINE_NUMS_STRIPPED");
	if ((header.characteristics & IMAGE_FILE_LOCAL_SYMS_STRIPPED) != 0)
		printf(" IMAGE_FILE_LOCAL_SYMS_STRIPPED");
	if ((header.characteristics & IMAGE_FILE_AGGRESSIVE_WS_TRIM) != 0)
		printf(" IMAGE_FILE_AGGRESSIVE_WS_TRIM");
	if ((header.characteristics & IMAGE_FILE_LARGE_ADDRESS_AWARE) != 0)
		printf(" IMAGE_FILE_LARGE_ADDRESS_AWARE");
	if ((header.characteristics & IMAGE_FILE_16BIT_MACHINE) != 0)
		printf(" IMAGE_FILE_16BIT_MACHINE");
	if ((header.characteristics & IMAGE_FILE_BYTES_REVERSED_LO) != 0)
		printf(" IMAGE_FILE_BYTES_REVERSED_LO");
	if ((header.characteristics & IMAGE_FILE_32BIT_MACHINE) != 0)
		printf(" IMAGE_FILE_32BIT_MACHINE");
	if ((header.characteristics & IMAGE_FILE_DEBUG_STRIPPED) != 0)
		printf(" IMAGE_FILE_DEBUG_STRIPPED");
	if ((header.characteristics & IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP) != 0)
		printf(" IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP");
	if ((header.characteristics & IMAGE_FILE_SYSTEM) != 0)
		printf(" IMAGE_FILE_SYSTEM");
	if ((header.characteristics & IMAGE_FILE_DLL) != 0)
		printf(" IMAGE_FILE_DLL");
	if ((header.characteristics & IMAGE_FILE_UP_SYSTEM_ONLY) != 0)
		printf(" IMAGE_FILE_UP_SYSTEM_ONLY");
	if ((header.characteristics & IMAGE_FILE_BYTES_REVERSED_HI) != 0)
		printf(" IMAGE_FILE_BYTES_REVERSED_HI");

	return header;
}

/* Dump COFF imagge. */
void dumpCoffImage(IN FILE* file) {

	CoffFileHeader    coffFileHeader;
	CoffSectionHeader stab;
	CoffSectionHeader stabstr;
	BOOL              debug;

	if (!file)
		return;

	fseek(file, 0, SEEK_SET);
	coffFileHeader = dumpCoffFileHeader(file);
	/* comment for now since its a lot of info. */
	debug = dumpFileSections(file, coffFileHeader.numberOfSections, &stab, &stabstr);
	CoffLoadStringTable(file, coffFileHeader);
	dumpFileSymbolTable(file, coffFileHeader.pointerToSymbolTable, coffFileHeader.numberOfSymbols);
	if (debug) dumpStabs(file, &stab, &stabstr);

	printf("\n\r=== DONE ===\n\r");
	return;
}

