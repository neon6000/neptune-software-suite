/************************************************************************
*
*	label.c - Label management.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nasm.h"
#include <assert.h>
#include <malloc.h>
#include <string.h>

#define LABEL_BUCKETS 10

extern PASS _pass;

typedef struct Label {
	char        magic[6];
	int         seg;
	int         offset;
	PSTRING     name;
	SymbolFlags flags;
	BOOL        defined;
}Label;

typedef MAP LabelMap;

LabelMap* _labels;
PSTRING   _parentLabel;

#define ISLOCAL(x) ((x)[0] == '.')
#define IDENT_MAX 100

PRIVATE Label* _NewLabel(IN char* name) {

	Label* label = malloc(sizeof(Label));
	if (!label)
		return NULL;
	memset(label, 0, sizeof(Label));
	strcpy(label->magic, "LABEL");
	label->name = InternString(name);
	label->defined = FALSE;
	return label;
}

PRIVATE BOOL GetLabelName(IN char* name, OUT char* realname, IN size_t maxsize) {

	size_t len;

	if (ISLOCAL(name)) {

		if (!_parentLabel) {
			Error("_LabelGet: Attempt to define %s before any global label", name);
			return FALSE;
		}

		len = strlen(_parentLabel->value);
		if (len + strlen(name) + 1 > maxsize) {
			Fatal("*** BUGCHECK: _LabelGet: %s.%s exceeds %i bytes", _parentLabel->value, name, maxsize);
			return FALSE;
		}
		memcpy(realname, _parentLabel->value, len);
		memcpy(realname + len, name, strlen(name) + 1);
	}
	else{

		if (strlen(name) > maxsize) {
			Fatal("*** BUGCHECK: _LabelGet: %s exceeds %i bytes", name, maxsize);
			return FALSE;
		}
		strcpy(realname, name);
	}
	return TRUE;
}

#define GET_LABEL  1
#define GET_GLOBAL 2

PRIVATE Label* _LabelGet(IN char* name) {

	char   realname[IDENT_MAX];
	Label* label;
	int    len;

	if (!GetLabelName(name, realname, IDENT_MAX))
		return NULL;
	label = (Label*)MapGet(_labels, realname);
	return label;
}

PUBLIC BOOL LabelGet(IN char* name, OUT int* seg, OUT int* offset) {

	Label* label = _LabelGet(name);

	if (!label)
		return FALSE;
	*seg = label->seg;
	*offset = label->offset;
	return TRUE;
}

PUBLIC BOOL LabelIsExtern(IN char* name) {

	Label* label = _LabelGet(name);
	if (label && label->flags & SYM_EXTERN)
		return TRUE;
	return FALSE;
}

PUBLIC Label* CreateNewLabel(IN PSTRING name) {

	char   realname[IDENT_MAX];
	Label* label;

	if (!GetLabelName(name->value, realname, IDENT_MAX)) {
		Fatal("Unable to obtain full label name");
		return NULL;
	}
	label = _NewLabel(realname);
	if (!label) {
		Fatal("Internal error, unable to create any new labels");
		return NULL;
	}
	label->flags = 0;
	MapPut(_labels, label->name->value, label);
	return label;
}

/**
*	LabelDefine
*	
*	Description : This processes label lines (i.e. "myLabel :")
*/
PUBLIC BOOL LabelDefine(IN PSTRING name, IN int seg, IN int off, IN BOOL extrn) {

	Label*  label;
	PTARGET of;

	label = _LabelGet(name->value);
	of = GetOutputFormat();

	if (_pass == PASS0 && label && label->defined) {

		// mylabel:
		// mylabel:

		Error("Label redefined: %s", label->name->value);
		return FALSE;
	}
	else if (_pass == PASS0 && label && (label->flags & SYM_EXTERN)) {

		// extern myLabel
		// myLabel:

		label->flags &= ~SYM_EXTERN;
		if ((label->flags & SYM_AUTO) == 0) {
			Error("Label %s previous defined as EXTERN", label->name->value);
			return FALSE;
		}
	}

	if (!label)
		label = CreateNewLabel(name);

	if (extrn)
		label->flags = SYM_EXTERN;

	// label->seg could be set if this was declared extern previously
	if (_pass == PASS0 && label->seg == 0)
		label->seg = NextSegmentIndex();

	label->offset = off;
	label->defined = TRUE;

	if (of)
			of->addSymbol(label->name, label->seg, label->offset, label->flags);
	label->flags |= SYM_DEFINED;

	if (!ISLOCAL(name->value))
		_parentLabel = label->name;

	return TRUE;
}

/**
*	LabelDefineExtern
*
*	Description : This prcoesses exteren directive (i.e. "extern myLabel").
*	If the "auto declare extern" flag is set, this may also be called from the
*	expression evaluator on PASSGEN.
*
*	Input : name - Name of label to define
*           isAuto - TRUE if NASM auto-declares this as EXTERN, FALSE otherwise
*/
PUBLIC void LabelDefineExtern(IN PSTRING name, BOOL isAuto) {

	Label*  label;
	PTARGET of;

	label = _LabelGet(name->value);
	of = GetOutputFormat();

	// that is, if we are in PASSGEN and have "mov eax, mylabel" where
	// the symbol doesnt get defined yet. When we generate the code & relocation
	// it wont have a symbol to reference

	// if the EXTERN flag was removed in an earlier pass then ignore this
	if (_pass > PASS0 && label && (label->flags & SYM_EXTERN) == 0)
		return;

	if (_pass == PASS0 && label && label->defined) {

		// mylabel:
		// extern mylabel
		if (!isAuto)
			Error("Label redefined was previously EXTERN: %s", label->name->value);
		return;
	}
	else if (_pass == PASS0 && label && (label->flags & SYM_EXTERN)) {

		// extern mylabel
		// extern mylabel
		return;
	}
	else if (_pass == PASS0 && label && (label->flags & SYM_GLOBAL)) {

		// global mylabel
		// extern mylabel
		if (!isAuto)
			Warn("Label declared extern was alredy declared global: %s, ignoring redefinition", label->name->value);
		return;
	}

	if (!label)
		label = CreateNewLabel(name);

	if (_pass == PASS0) {

		// label->seg must be a unique value for relocations
		label->seg = NextSegmentIndex();
		label->offset = 0;
		label->flags = SYM_EXTERN;
		if (isAuto)
			label->flags |= SYM_AUTO;
	}

	if (of)
		of->addSymbol(label->name, label->seg, label->offset, label->flags);

	if (!isAuto && !ISLOCAL(name->value))
		_parentLabel = label->name;

	return TRUE;
}

/**
*	LabelDefineGlobal
*
*	Description : This processes global directive (i.e. "global myLabel")
*/
PUBLIC void LabelDefineGlobal(IN PSTRING name, IN int seg, IN int offset) {

	Label* label;
	PTARGET of;

	label = _LabelGet(name->value);
	of = GetOutputFormat();

	if (label) {

		// this handles the case where the label was previously declared with EXTERN in PASS0.
		// extern mylabel
		// global mylabel

		if (_pass == PASS0 && label->flags & SYM_EXTERN) {
			if ((label->flags & SYM_AUTO) == 0)
				Warn("previous external declaration for label %s ignored", label->name->value);
			label->flags &= ~SYM_EXTERN;
		}

		// first time seeing "global mylabel"

//		label->seg = seg;
		label->offset = offset;
		label->flags |= SYM_GLOBAL;

		if (of)
			of->addSymbol(label->name, label->seg, label->offset, label->flags);

		return;
	}

	// here we handle the case where the label does not yet exist

	label = CreateNewLabel(name);
	if (!label)
		return;

	label->name = name;
//	label->seg = seg;
	label->seg = NextSegmentIndex();
	label->offset = offset;
	label->flags |= SYM_GLOBAL;
	label->defined = FALSE;

	if (of)
		of->addSymbol(label->name, label->seg, label->offset, label->flags);
}

PUBLIC BOOL LabelInit(void) {

	_parentLabel = NULL;
	if (!_labels)
		_labels = MapCreate(LABEL_BUCKETS);
	return _labels != NULL;
}

PRIVATE void FreeLabel(IN PMAPELEMENT e) {

	Label* l;

	l = e->value;
	if (l)
		free(l);
}

PUBLIC void FreeLabels(void) {

	if (_labels)
		MapFree(_labels, FreeLabel);
}

PUBLIC char* LabelScope(IN char* name) {

	return ISLOCAL(name) ? _parentLabel->value : "";
}



#if 0


/************************************************************************
*
*	label.c - Label management.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nasm.h"
#include <assert.h>
#include <malloc.h>
#include <string.h>

#define LABEL_BUCKETS 10

extern PASS _pass;

typedef struct Label {
	char        magic[6];
	int         seg;
	int         offset;
	PSTRING     name;
	SymbolFlags flags;
	BOOL        defined;
}Label;

typedef MAP LabelMap;

LabelMap* _labels;
PSTRING   _parentLabel;

#define ISLOCAL(x) ((x)[0] == '.')
#define IDENT_MAX 100

PRIVATE Label* _NewLabel(IN char* name) {

	Label* label = malloc(sizeof(Label));
	if (!label)
		return NULL;
	memset(label, 0, sizeof(Label));
	strcpy(label->magic, "LABEL");
	label->name = InternString (name);
	label->defined = FALSE;
	return label;
}

PRIVATE BOOL GetLabelName(IN char* name, OUT char* realname, IN size_t maxsize) {

	size_t len;

	if (ISLOCAL(name)) {

		if (!_parentLabel) {
			Error("_LabelGet: Attempt to define %s before any global label", name);
			return FALSE;
		}

		len = strlen(_parentLabel->value);
		if (len + strlen(name) + 1 > maxsize) {
			Fatal("*** BUGCHECK: _LabelGet: %s.%s exceeds %i bytes", _parentLabel->value, name, maxsize);
			return FALSE;
		}
		memcpy(realname, _parentLabel->value, len);
		memcpy(realname + len, name, strlen(name) + 1);
	}
	else{

		if (strlen(name) > maxsize) {
			Fatal("*** BUGCHECK: _LabelGet: %s exceeds %i bytes", name, maxsize);
			return FALSE;
		}
		strcpy(realname, name);
	}
	return TRUE;
}

#define GET_LABEL  1
#define GET_GLOBAL 3

PRIVATE Label* _LabelGet(IN char* name, IN int flags) {

	char   realname[IDENT_MAX];
	Label* label;
	int    len;

	if (!GetLabelName(name, realname, IDENT_MAX))
		return NULL;

	label = (Label*)MapGet(_labels, realname);

	// if we are just querying we are done
	if (flags == GET_LABEL)
		return label;

	// we are to actually define this label here:
	if (label) {
		if (!ISLOCAL(name)) {
			_parentLabel = label->name;
		}
	}

	else if (_pass == PASS0) {
		if (!label && flags == GET_GLOBAL)) {
			label = _NewLabel(realname);
			MapPut(_labels, label->name->value, label);
			if (!ISLOCAL(name)) {
				_parentLabel = label->name;
			}
		}
	}

	return label;
}

PUBLIC BOOL LabelGet(IN char* name, OUT int* seg, OUT int* offset) {

	Label* label = _LabelGet(name, GET_LABEL);

	if (!label)
		return FALSE;
	*seg = label->seg;
	*offset = label->offset;
	return TRUE;
}

PUBLIC BOOL LabelIsExtern(IN char* name) {

	Label* label = _LabelGet(name, FALSE);
	if (label && label->flags & SYM_EXTERN)
		return TRUE;
	return FALSE;
}

// this is called by GEN on lines that have labels:

PUBLIC BOOL LabelDefine(IN PSTRING name, IN int seg, IN int off) {

	Label*  label;
	PTARGET of;

	of = GetOutputFormat();
	if (!of)
		return FALSE;

	label = _LabelGet(name->value, GET_LABEL);

	if (_pass == PASS0 && label && label->defined) {
		
		// myLabel: (defined)
		// myLabel: (defined)

		Error("label redefined: %s", label->name->value);
		return FALSE;
	}
	else if (_pass == PASS0 && label && (label->flags & SYM_EXTERN)) {

		// if a label was previously defined EXTERN but then defined
		// this is fine. We silently allow it and adjust the flag as needed.

		// call myLabel (implicate extern)
		// myLabel: (define) <-- this case

		// extern myLabel
		// myLabel: (define) <-- this case

		label->flags &= ~SYM_EXTERN;
	}

	if (!label) {

		// this is the case where the label does not yet exist.

		label = _LabelGet(name->value, GET_DEFINE);
		if (!label) {
			Fatal("Internal error, unable to create any new labels");
			return FALSE;
		}
		label->flags = 0;
	}

	label->seg = seg;
	label->offset = off;

	if (_pass == PASS0) {
		of->addSymbol(label->name, label->seg, label->offset, label->flags);
		label->flags |= SYM_DEFINED;
	}

	return TRUE;
}

// this is used by EXTERN directive.

PUBLIC void LabelDefineExtern(IN PSTRING name, IN int seg, IN int offset) {

	Label* label;
	PTARGET of;

	of = GetOutputFormat();
	if (!of)
		return FALSE;

	label = _LabelGet(name->value, GET_LABEL);
	if (label && label->defined) {

		// this handles the case where EXTERN is declared on a Label
		// that has already been defined.

		// myLabel:  (defined)
		// extern myLabel <-- this case

		Error("Label %s already defined", label->name->value);
		return FALSE;
	}
	else if (label && (label->flags & SYM_GLOBAL)) {

		// this handles the case where EXTERN directive is used on a Label
		// that was already declared with GLOBAL

		// global myLabel (defined)
		// extern myLabel <-- this case

		Warn("extern directive overrides global for label %s", label->name->value);
		label->flags &= ~SYM_GLOBAL;
	}

	// this handles the general case

	// extern myLabel <-- this case

	if (label) {

		// if the label already exists, we can update the symbol here

		label->flags |= SYM_EXTERN;
		if (_pass == PASS0) {
			of->addSymbol(label->name, label->seg, label->offset, label->flags);
			label->flags |= SYM_DEFINED;
		}
		return;
	}
	else{

		// if the label does not already exist, create it

		label = _LabelGet(name->value, GET_DEFINE);
		if (!label) {
			Fatal("Internal error, unable to create any new labels");
			return FALSE;
		}
		label->seg = NextSegmentIndex();
		label->offset = 0;
		label->flags = SYM_EXTERN;
		if (_pass == PASS0) {
			of->addSymbol(label->name, label->seg, label->offset, label->flags);
			label->flags |= SYM_DEFINED;
		}
	}
}

// this is used by GLOBAL directive

PUBLIC void LabelDefineGlobal(IN PSTRING name, IN int seg, IN int offset) {

	Label* label;
	PTARGET of;

	// we dont want to create a Symbol object for this Label until
	// the Label is actually defined. This is to avoid uneccessary Symbols
	// to the Symbol table

	of = GetOutputFormat();
	if (!of)
		return FALSE;

	label = _LabelGet(name->value, GET_LABEL);
	if (label) {

		// here we handle the case where GLOBAL directive is
		// after the label has been defined

		// myLabel: (defined)
		// global myLabel <-- this case, trigger warning

		// extern myLabel (implicate define)
		// global myLabel <-- this case, do not trigger warning

		// Labels may be defined EXTERN before we know they are GLOBAL.
		// so long as the Label is not defined yet we silenty just remove
		// the flag if needed.

		if (label->flags & SYM_EXTERN) {
			if (label->defined) Warn("global directive overrides external for label %s", label->name->value);
			label->flags &= ~SYM_EXTERN;
		}

		label->seg = seg;
		label->offset = offset;
		label->flags |= SYM_GLOBAL;

		// update this symbol
		if (_pass == PASS0 && (label->flags & SYM_DEFINED))
			of->addSymbol(label->name, label->seg, label->offset, label->flags);

		return;
	}

	// here we handle the case where the label does not yet exist

	// global myLabel <-- this case
	// myLabel: (defined)

	label = _LabelGet(name->value, GET_GLOBAL);
	if (!label)
		return;
	label->name = name;
	label->seg = seg;
	label->offset = offset;
	label->flags |= SYM_GLOBAL;
}

PUBLIC BOOL LabelInit(void) {

	_parentLabel = NULL;
	if (!_labels)
		_labels = MapCreate(LABEL_BUCKETS);
	return _labels != NULL;
}

PRIVATE void FreeLabel(IN PMAPELEMENT e) {

	Label* l;

	l = e->value;
	if (l)
		free(l);
}

PUBLIC void FreeLabels(void) {

	if (_labels)
		MapFree(_labels, FreeLabel);
}

PUBLIC char* LabelScope(IN char* name) {

	return ISLOCAL(name) ? _parentLabel->value : "";
}

#endif
