/************************************************************************
*
*	dwarf.c - Dwarf debugging support.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "nasm.h"

// Build the sections here & just merge them with the output file
// before writing out the OBJ file.

// http://downloads.ti.com/docs/esd/SPRU513J/Content/SPRU513J_HTML/symbolic_debugging_directives.html
// https://e2e.ti.com/support/tools/ccs/f/81/t/691691?CCS-MSP430F5529-Questions-about-asm-hex-file
// https://e2e.ti.com/support/tools/ccs/f/81/t/434136?Syntax-Error-in-Inline-Assembly-Language-TMS320F28069-and-CCS-v5-5-
// http://read.pudn.com/downloads443/sourcecode/app/1869385/PWM%E2%80%94MOROR-CTRL/DSP28_ECan.asm__.htm
// https://gist.github.com/tomrittervg/c5e05ae1d1f470c081cfe8af4c12d4e5

/*
Non-standard macros should be defined:

%macro dwpsc 4
	[_dwpsc %1, %2, %3, %4]
%endmacro
%macro dwtag 2
	section .debug_info
	%1:
	[_dwtag %1, %2]
%endmacro
%macro dwattr 2
	[_dwattr %1 %2]
%endmacro
%macro dwattr2 3
	[_dwattr %1, %2, %3]
%endmacro
%macro dwcfi 1
	[_dwcfi %1]
%endmacro
%macro dwcfi2 2
	[_dwcfi %1, %2]
%endmacro
%macro dwcfi3 3
	[_dwcfi %1, %2, %3]
%endmacro
%define dwendentry [_dwendentry]
%define dwendtag [_dwendtag]
%macro dwfde 2
	[_dwfde %1, %2]
%endmacro
%macro dwcie 2
	[_dwcie %1, %2]
%endmacro
%macro dwcfa2 2
	[_dwcfa %1, %2]
%endmacro
%macro dwcfa3 3
	[_dwcfa %1, %2, %3]
%endmacro
Sample assembly file:

section .text

dwpsc 'file.c', 1, 2, 3

; DWTAG switches to debug_info & will write
; data to debug_abbrev.
dwtag label_HI, DW_TAG_variable
	dwattr2 type:ref4, DW_AT_type:label_2

dwtag label_HI2, DW_TAG_variable
	dwattr DW_AT_type:label_2

segment .text ; switch back when done.

During pass0, we only need the SIZE to add to offset.
Generate data tables in pass1.

PRIVATE void Write(IN handle_t seg, IN void* data, IN output_t type, size_t size, IN int segto, IN PASSpass) {
	WRITE_REL4ADR WRITE_ADDRESS WRITE_RAWDATA

*/

#define DIR_DWPSC       0x8000
#define DIR_DWTAG       0x8001
#define DIR_DWENDTAG    0x8002
#define DIR_DWATTR      0x8003
#define DIR_DWCIE       0x8004
#define DIR_DWFDE       0x8005
#define DIR_DWENDENTRY  0x8006
#define DIR_DWCFI       0x8007
#define DIR_DWCFA       0x8008

BUFFER debug_abbrev;
BUFFER debug_info;
BUFFER debug_frame;
BUFFER debug_arranges;
BUFFER debug_pubnames;
BUFFER debug_pubtypes;
BUFFER debug_line;

// _segment = _output->section(op->value.s, pass, _mode);
// _offset = _output->getOffset(pass);

extern unsigned long  _offset;
extern PTARGET        _output;
extern handle_t       _segment;

PRIVATE void Init(void) {

}

PRIVATE void Cleanup(void) {

}

#if 0
PRIVATE BOOL ProcessDirective(IN PASS pass, IN Directive d, IN ast_directive_oper* op) {

	switch (d) {
	case DIR_DWPSC:
	case DIR_DWTAG:
	case DIR_DWENDTAG:
	case DIR_DWATTR:
	case DIR_DWCIE:
	case DIR_DWFDE:
	case DIR_DWENDENTRY:
	case DIR_DWCFI:
	case DIR_DWCFA:
		return TRUE;
	}
	return FALSE;
}

PRIVATE Directive MatchDirective(IN char* name) {

	if (!strcmp(name, "_dwpsc"))
		return DIR_DWPSC;
	if (!strcmp(name, "_dwtag"))
		return DIR_DWTAG;
	if (!strcmp(name, "_dwattr"))
		return DIR_DWATTR;
	if (!strcmp(name, "_dwcie"))
		return DIR_DWCIE;
	if (!strcmp(name, "_dwfde"))
		return DIR_DWFDE;
	if (!strcmp(name, "_dwcfi"))
		return DIR_DWCFI;
	if (!strcmp(name, "_dwcfa"))
		return DIR_DWCFA;
	if (!strcmp(name, "_dwendentry"))
		return DIR_DWENDENTRY;
	if (!strcmp(name, "_dwendtag"))
		return DIR_DWENDTAG;
	return DirInvalid;
}
#endif

PRIVATE BOOL ProcessDirective(IN PSTRING name, IN PLEX scn, IN OUT PTOKEN token, IN PASS pass) {

	return FALSE;
}

PRIVATE void StdMac(void) {

}

PUBLIC DEBUG _dwarf = {
	"dwarf",
	"DWARF debugging support",
	Init,
	Cleanup,
	ProcessDirective,
	StdMac
};

