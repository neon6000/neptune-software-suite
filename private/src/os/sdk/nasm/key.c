/************************************************************************
*
*	key.c - Keyword list.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <string.h>
#include "nasm.h"

/* PRIVATE Definitions ******************************/

//
// should be in same order as SIZE in nasm.h
//
char* _sizeSpecifiers[] = {
	"byte", "word", "dword", "qword", "tbyte"
};

//
// should be in same order as PREFIX in nasm.h
//
char* _prefix[] = {
	"lock", "rep", "repe", "repz", "repne", "repnz", "xacquire", "xrelease", "bnd", "nobnd", "a16", "a32", "o16", "o32"
};

OPERATOR _operators[] = {
	{ TokenLeftShift,    "<<", OP_BINARY,  5 },
	{ TokenRightShift,   ">>", OP_BINARY,  5 },
	{ TokenBitOr,        "|",  OP_BINARY,  1 },
	{ TokenBitAnd,       "&",  OP_BINARY,  8 },
	{ TokenNot,          "!",  OP_UNARY,   1 },
	{ TokenXor,          "~",  OP_UNARY,   1 },
	{ TokenMod,          "%",  OP_BINARY,  3 },
	{ TokenPlus,         "+",  OP_BINARY,  4 },
	{ TokenMinus,        "-",  OP_BINARY,  4 },
	{ TokenStar,         "*",  OP_BINARY,  3 },
	{ TokenFowardSlash,  "/",  OP_BINARY,  3 },
	{ TokenLeftBracket,  "[",  OP_NONE,    0 },
	{ TokenRightBracket, "]",  OP_NONE,    0 },
	{ TokenLeftParen,    "(",  OP_NONE,    0 },
	{ TokenRightParen,   ")",  OP_NONE,    0 },
	{ TokenComma,        ",",  OP_NONE,    0 },
	{ TokenColon,        ":",  OP_NONE,    0 },
	{ TokenEqual,        "=",  OP_BINARY,  1 }
};

/* PUBLIC Definitions ******************************/

PUBLIC SIZE IsSizeSpecifier(IN char* str) {

	int max = sizeof(_sizeSpecifiers) / sizeof(char*);
	int i = 0;

	for (i = 0; i < max; i++) {
		if (strlen(_sizeSpecifiers[i]) != strlen(str))
			continue;
		if (_strnicmp(str, _sizeSpecifiers[i], strlen(str)) == 0)
			return (SIZE)i;
	}
	return SizeInvalid;
}

PUBLIC PREFIX IsPrefix(IN char* str) {

	int max = sizeof(_prefix) / sizeof(char*);
	int i = 0;
	for (i = 0; i < max; i++) {
		if (strlen(_prefix[i]) != strlen(str))
			continue;
		if (_strnicmp(str, _prefix[i], strlen(str)) == 0)
			return (PREFIX)i;
	}
	return PrefixInvalid;
}

PUBLIC POPERATOR GetOperator(IN char* s) {

	int max = sizeof(_operators) / sizeof(OPERATOR);
	int i = 0;
	for (i = 0; i < max; i++) {
		if (_strnicmp(s, _operators[i].s, strlen(s)) == 0)
			return &_operators[i];
	}
	return NULL;
}
