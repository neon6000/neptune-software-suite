@rem /************************************************************************
@rem *
@rem *	geninstr - Generates instr.h from instrlist.txt. This first sorts
@rem *   instrlist.txt since nasm uses a binary search in the scanner for performance.
@rem *
@rem *   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
@rem *
@rem ************************************************************************/

echo This first sorts instrlist.txt and then generates instr.h.

@sort < instrlist.txt /o instrlist.txt
@echo off
@echo /************************************************************************ > instr.h
@echo * >> instr.h
@echo *	instr.h - Instruction list.>> instr.h
@echo *>> instr.h
@echo *   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.>> instr.h
@echo *>> instr.h
@echo ************************************************************************/ >> instr.h
@echo /* Generated from gen_instr */ >> instr.h
@echo #include "nasm.h" >> instr.h
@echo #ifndef INSTR_H >> instr.h
@echo #define INSTR_H >> instr.h
@echo /* for in/out instructions we need to >> instr.h
@echo temporarily disable this. */ >> instr.h
@echo #undef IN >> instr.h
@echo #undef OUT >> instr.h
@echo #define C(x) x, >> instr.h
@echo /* mnemonic table. */ >> instr.h
@echo #define MNEMONICS \>>instr.h

for /f "tokens=* delims=" %%x in (instrlist.txt) do echo C(%%x)\>>instr.h

@echo	C(MnemonicInvalid) >> instr.h
@echo typedef enum Mnemonic { MNEMONICS } Mnemonic; >> instr.h
@echo Mnemonic GetMnemonic(char* s); >> instr.h
@echo const char* FromMnemonic(Mnemonic m); >> instr.h
@echo #endif >> instr.h
pause
