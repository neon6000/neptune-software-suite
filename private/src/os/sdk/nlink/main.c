/************************************************************************
*
*	main.c - Neptune Linker entry point
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <stdlib.h>
#include <memory.h>
#include "nlink.h"

PUBLIC PARCHIVE _archives;
PUBLIC PMODULE  _modules;

PUBLIC int main(IN int argc, IN char** argv, IN char** envp) {

	PMODULE       mod;
	PARCHIVE      arch;
	PSTRING       obj;
	TARGET       target;
	offset_t     ofs;
	char*        sym;
	index_t       i;

//	obj = InternString("Debug/main.obj");

	arch = InternString("Debug/nbl_bios.lib");

//	AddModule("../ncc/nasm/Debug/buffer.obj");

//	mod = CoffRead(obj);


	arch = LibOpen(arch);

	LibReadFile(arch, InternString("_io_services"));


//	target.name = InternString("Debug/TEST.EXE");

//	ImageWrite32(&target);

	__debugbreak();
	return 0;
}
