/************************************************************************
*
*	file.c
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <malloc.h>
#include <stdio.h>
#include <sys/stat.h>
#include <string.h>
#include "nlink.h"

/* Private Definitions ********************/


PRIVATE size_t _FileSize(IN FILE* stream) {

	size_t size;

	fseek(stream, 0, SEEK_END);
	size = ftell(stream);
	fseek(stream, 0, SEEK_SET);
	return size;
}

/* Public Definitions *********************/

PUBLIC PRECORD OpenFile(IN PSTRING fname) {

	PRECORD file;
	FILE* stream;

	stream = fopen(fname->value, "rb");
	if (!stream)
		return NULL;
	file = ALLOC_OBJ(RECORD);
	file->name = fname;
	file->stream = (handle_t) stream;
	file->size = _FileSize(stream);
	return file;
}

PUBLIC void CloseFile(IN PRECORD file) {

	if (!(file || file->stream))
		return;
	fclose((FILE*)file->stream);
	free(file);
}

PUBLIC BOOL FileSeek(IN PRECORD file, IN index_t loc) {

	FILE* stream;

	if (loc >= file->size)
		return FALSE;
	stream = (FILE*)file->stream;
	fseek(stream, loc, SEEK_SET);
	return TRUE;
}

PUBLIC PRECORD FileCreateTemp(IN char* base) {

	char*  tempname;
	PRECORD  file;

	tempname = _tempnam(NULL, base);
	file = (PRECORD)ALLOC_OBJ(RECORD);
	file->stream = fopen(tempname, "wb");
	file->name = _strdup(tempname);
	file->size = 0;
	return file;
}

PUBLIC BOOL FileRename(IN char* from, IN char* to) {

	if (!rename(from, to))
		return TRUE;
	return FALSE;
}

PUBLIC BOOL FileRemove(IN char* file) {

	if (!remove(file))
		return TRUE;
	return FALSE;
}

PUBLIC PRECORD FileCreate(IN char* name) {

	PRECORD    file;
	FILE*    stream;

	if (!name) return NULL;

	stream = fopen(name, "wb");
	if (!stream)
		return NULL;

	file = (PRECORD)ALLOC_OBJ(RECORD);
	file->stream = stream;
	file->name = name;
	file->size = 0;
	return file;
}

PUBLIC size_t FileRead(IN PRECORD file, IN size_t size, OUT void* data) {

	return fread(data, 1, size, file->stream);
}

PUBLIC size_t FileWrite(IN PRECORD file, IN size_t size, IN void* data) {

	return fwrite(data, 1, size, file->stream);
}

PUBLIC long FileTell(IN PRECORD file) {

	return ftell(file->stream);
}

PUBLIC size_t FileSize(IN PRECORD file) {

	size_t size;
	fseek(file->stream, 0, SEEK_END);
	size = ftell(file->stream);
	fseek(file->stream, 0, SEEK_SET);
	return size;
}

PUBLIC time_t FileModifyTime(IN PRECORD file) {

	struct stat st;

	if (stat(file->name, &st) == -1)
		return (time_t)0L;

	return st.st_mtime;
}

PUBLIC BOOL FileExists(IN char* filename) {

	struct stat st;
	if (stat(filename, &st) == -1)
		return FALSE;
	return TRUE;
}
