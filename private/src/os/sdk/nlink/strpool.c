/************************************************************************
*
*	strpool.c - String pool
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This component implements a pool for literals and identifiers.
The intent is to lower the memory footprint and speed up string
comparisions during the compilation process.
*/

#include "nlink.h"
#include <malloc.h>
#include <stdio.h>

#define BUCKETS 10000

typedef struct STRMAP {
	PSTRING*     buckets;
	size_t       numberOfBuckets;
	size_t       numberOfElements;
}STRMAP, *PSTRMAP;

PRIVATE STRMAP _internTable;

/**
*	NewString
*
*	Description : Allocates a new PSTRING
*
*	Input : value - CSTRING value. The value is
*	duplicated internally.
*
*	Output : PSTRING object
*/
PRIVATE PSTRING NewString(IN char* value) {

	PSTRING s;

	s = malloc(sizeof(STRING));
	s->refcount = 0;
	s->value = _strdup(value);
	s->offset1 = 0;
	s->offset2 = 0;
	s->next = NULL;
	return s;
}

PRIVATE unsigned Hash(IN char* key) {

	const unsigned char *str = (const unsigned char *)key;
	unsigned long hash = 5381;
	int c;

	/* djb2 algorithm */
	while ((c = *str++) != '\0')
		hash = hash * 33 + c;

	return hash;
}

PRIVATE PSTRING StrMapGet(IN char* key) {

	unsigned  hash;
	PSTRING   firstItem;
	PSTRING   item;

	hash = Hash(key) % _internTable.numberOfBuckets;
	firstItem = _internTable.buckets[hash];

	for (item = firstItem; item; item = item->next) {
		if (!strcmp(item->value, key)) {
			item->refcount++;
			return item;
		}
	}
	return NULL;
}

PRIVATE PSTRING StrMapPut(IN char* string) {

	unsigned hash;
	PSTRING  firstItem;
	PSTRING  item;

	item = NewString(string);

	hash = Hash(string) % _internTable.numberOfBuckets;
	firstItem = _internTable.buckets[hash];

	if (firstItem) {
		_internTable.buckets[hash] = item;
		item->next = firstItem;
	}
	else
		_internTable.buckets[hash] = item;

	_internTable.numberOfElements++;
	return item;
}

/**
*	DumpStringTable
*
*	Description : Displays the string table
*/
PUBLIC void DumpStringTable() {

	PSTRING item;

	Info("------------ String table ------------");
	for (int bucket = 0; bucket < _internTable.numberOfBuckets; bucket++) {
		if (!_internTable.buckets[bucket]) continue;
		Info("BUCKET %i", bucket);
		for (item = _internTable.buckets[bucket]; item; item = item->next) {
			Info("    STR '%s' ADDR 0x%x", item->value, item);
		}
	}
	Info("------------ String table END ------------");
}

/**
*	FreeStrings
*
*	Description : Free string table
*/
PUBLIC void FreeStringTable(void) {

	PSTRING item;
	PSTRING next;

	for (int bucket = 0; bucket < _internTable.numberOfBuckets; bucket++) {
		if (!_internTable.buckets[bucket]) continue;
		for (item = _internTable.buckets[bucket]; item; item = next) {
			next = item->next;
			free(item->value);
			free(item);
		}
	}
	free(_internTable.buckets);
	_internTable.buckets = NULL;
	_internTable.numberOfBuckets = 0;
	_internTable.numberOfElements = 0;
}

/**
*	InternString
*
*	Description : Given a CSTRING, return
*	the PSTRING that it references
*
*	Input : string - CSTRING to look up or insert
*
*	Output : PSTRING object referencing string.
*/
PUBLIC PSTRING InternString(IN char* string) {

	PSTRING s;
	PSTRING k;

	if (!_internTable.buckets) {
		_internTable.buckets = calloc(BUCKETS, sizeof(PSTRING));
		_internTable.numberOfBuckets = BUCKETS;
	}

	s = StrMapGet(string);

	if (s && strcmp(s->value, string)) {
		DumpStringTable();
		Fatal("*** BUGCHECK: InternString: table corrupted (original s->value may have been free'd");
	}

	if (s) return s;

	return StrMapPut(string);
}

