/************************************************************************
*
*	pecoff.h
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef PECOFF_H
#define PECOFF_H


#define COFF_DOS_SIGNATURE                 0x5A4D      // MZ
#define COFF_OS2_SIGNATURE                 0x454E      // NE
#define COFF_OS2_SIGNATURE_LE              0x454C      // LE
#define COFF_VXD_SIGNATURE                 0x454C      // LE
#define COFF_NT_SIGNATURE                  0x00004550  // PE00

#define COFF_NT_OPTIONAL_HDR32_MAGIC      0x10b
#define COFF_NT_OPTIONAL_HDR64_MAGIC      0x20b
#define COFF_ROM_OPTIONAL_HDR_MAGIC       0x107

#define COFF_FILE_MACHINE_I386   0x14c
#define COFF_FILE_MACHINE_IA64   0x200

#define COFF_FILE_RELOCS_STRIPPED           0x0001  // Relocation info stripped from file.
#define COFF_FILE_EXECUTABLE_IMAGE          0x0002  // File is executable  (i.e. no unresolved external references).
#define COFF_FILE_LINE_NUMS_STRIPPED        0x0004  // Line nunbers stripped from file.
#define COFF_FILE_LOCAL_SYMS_STRIPPED       0x0008  // Local symbols stripped from file.
#define COFF_FILE_AGGRESIVE_WS_TRIM         0x0010  // Aggressively trim working set
#define COFF_FILE_LARGE_ADDRESS_AWARE       0x0020  // App can handle >2gb addresses
#define COFF_FILE_BYTES_REVERSED_LO         0x0080  // Bytes of machine word are reversed.
#define COFF_FILE_32BIT_MACHINE             0x0100  // 32 bit word machine.
#define COFF_FILE_DEBUG_STRIPPED            0x0200  // Debugging info stripped from file in .DBG file
#define COFF_FILE_REMOVABLE_RUN_FROM_SWAP   0x0400  // If Image is on removable media, copy and run from the swap file.
#define COFF_FILE_NET_RUN_FROM_SWAP         0x0800  // If Image is on Net, copy and run from the swap file.
#define COFF_FILE_SYSTEM                    0x1000  // System File.
#define COFF_FILE_DLL                       0x2000  // File is a DLL.
#define COFF_FILE_UP_SYSTEM_ONLY            0x4000  // File should only be run on a UP machine
#define COFF_FILE_BYTES_REVERSED_HI         0x8000  // Bytes of machine word are reversed.


#define COFF_SUBSYSTEM_UNKNOWN              0   // Unknown subsystem.
#define COFF_SUBSYSTEM_NATIVE               1   // Image doesn't require a subsystem.
#define COFF_SUBSYSTEM_WINDOWS_GUI          2   // Image runs in the Windows GUI subsystem.
#define COFF_SUBSYSTEM_WINDOWS_CUI          3   // Image runs in the Windows character subsystem.
#define COFF_SUBSYSTEM_OS2_CUI              5   // image runs in the OS/2 character subsystem.
#define COFF_SUBSYSTEM_POSIX_CUI            7   // image runs in the Posix character subsystem.
#define COFF_SUBSYSTEM_NATIVE_WINDOWS       8   // image is a native Win9x driver.
#define COFF_SUBSYSTEM_WINDOWS_CE_GUI       9   // Image runs in the Windows CE subsystem.
#define COFF_SUBSYSTEM_EFI_APPLICATION      10  //
#define COFF_SUBSYSTEM_EFI_BOOT_SERVICE_DRIVER  11   //
#define COFF_SUBSYSTEM_EFI_RUNTIME_DRIVER   12  //
#define COFF_SUBSYSTEM_EFI_ROM              13
#define COFF_SUBSYSTEM_XBOX                 14
#define COFF_SUBSYSTEM_WINDOWS_BOOT_APPLICATION 16


#define COFF_DLLCHARACTERISTICS_HIGH_ENTROPY_VA    0x0020  // Image can handle a high entropy 64-bit virtual address space.
#define COFF_DLLCHARACTERISTICS_DYNAMIC_BASE 0x0040     // DLL can move.
#define COFF_DLLCHARACTERISTICS_FORCE_INTEGRITY    0x0080     // Code Integrity Image
#define COFF_DLLCHARACTERISTICS_NX_COMPAT    0x0100     // Image is NX compatible
#define COFF_DLLCHARACTERISTICS_NO_ISOLATION 0x0200     // Image understands isolation and doesn't want it
#define COFF_DLLCHARACTERISTICS_NO_SEH       0x0400     // Image does not use SEH.  No SE handler may reside in this image
#define COFF_DLLCHARACTERISTICS_NO_BIND      0x0800     // Do not bind this image.
#define COFF_DLLCHARACTERISTICS_APPCONTAINER 0x1000     // Image should execute in an AppContainer
#define COFF_DLLCHARACTERISTICS_WDM_DRIVER   0x2000     // Driver uses WDM model
#define COFF_DLLCHARACTERISTICS_GUARD_CF     0x4000     // Image supports Control Flow Guard.
#define COFF_DLLCHARACTERISTICS_TERMINAL_SERVER_AWARE     0x8000

typedef struct COFF_FILE_HDR {
	uint16_t machine;
	uint16_t numberOfSections;
	uint32_t timeDateStamp;
	uint32_t pointerToSymbolTable;
	uint32_t numberOfSymbols;
	uint16_t sizeOfOptionalHeader;
	uint16_t characteristics;
}COFF_FILE_HDR, *PCOFF_FILE_HDR;

//
// sections.
//
#define COFF_SCN_TYPE_NO_PAD                0x00000008  // Reserved.
//      COFF_SCN_TYPE_COPY                  0x00000010  // Reserved.

#define COFF_SCN_CNT_CODE                   0x00000020  // Section contains code.
#define COFF_SCN_CNT_INITIALIZED_DATA       0x00000040  // Section contains initialized data.
#define COFF_SCN_CNT_UNINITIALIZED_DATA     0x00000080  // Section contains uninitialized data.

#define COFF_SCN_LNK_OTHER                  0x00000100  // Reserved.
#define COFF_SCN_LNK_INFO                   0x00000200  // Section contains comments or some other type of information.
//      COFF_SCN_TYPE_OVER                  0x00000400  // Reserved.
#define COFF_SCN_LNK_REMOVE                 0x00000800  // Section contents will not become part of image.
#define COFF_SCN_LNK_COMDAT                 0x00001000  // Section contents comdat.
//                                           0x00002000  // Reserved.
//      COFF_SCN_MEM_PROTECTED - Obsolete   0x00004000
#define COFF_SCN_NO_DEFER_SPEC_EXC          0x00004000  // Reset speculative exceptions handling bits in the TLB entries for this section.
#define COFF_SCN_GPREL                      0x00008000  // Section content can be accessed relative to GP
#define COFF_SCN_MEM_FARDATA                0x00008000
//      COFF_SCN_MEM_SYSHEAP  - Obsolete    0x00010000
#define COFF_SCN_MEM_PURGEABLE              0x00020000
#define COFF_SCN_MEM_16BIT                  0x00020000
#define COFF_SCN_MEM_LOCKED                 0x00040000
#define COFF_SCN_MEM_PRELOAD                0x00080000

#define COFF_SCN_ALIGN_1BYTES               0x00100000  //
#define COFF_SCN_ALIGN_2BYTES               0x00200000  //
#define COFF_SCN_ALIGN_4BYTES               0x00300000  //
#define COFF_SCN_ALIGN_8BYTES               0x00400000  //
#define COFF_SCN_ALIGN_16BYTES              0x00500000  // Default alignment if no others are specified.
#define COFF_SCN_ALIGN_32BYTES              0x00600000  //
#define COFF_SCN_ALIGN_64BYTES              0x00700000  //
#define COFF_SCN_ALIGN_128BYTES             0x00800000  //
#define COFF_SCN_ALIGN_256BYTES             0x00900000  //
#define COFF_SCN_ALIGN_512BYTES             0x00A00000  //
#define COFF_SCN_ALIGN_1024BYTES            0x00B00000  //
#define COFF_SCN_ALIGN_2048BYTES            0x00C00000  //
#define COFF_SCN_ALIGN_4096BYTES            0x00D00000  //
#define COFF_SCN_ALIGN_8192BYTES            0x00E00000  //
// Unused                                    0x00F00000
#define COFF_SCN_ALIGN_MASK                 0x00F00000

#define COFF_SCN_LNK_NRELOC_OVFL            0x01000000  // Section contains extended relocations.
#define COFF_SCN_MEM_DISCARDABLE            0x02000000  // Section can be discarded.
#define COFF_SCN_MEM_NOT_CACHED             0x04000000  // Section is not cachable.
#define COFF_SCN_MEM_NOT_PAGED              0x08000000  // Section is not pageable.
#define COFF_SCN_MEM_SHARED                 0x10000000  // Section is shareable.
#define COFF_SCN_MEM_EXECUTE                0x20000000  // Section is executable.
#define COFF_SCN_MEM_READ                   0x40000000  // Section is readable.
#define COFF_SCN_MEM_WRITE                  0x80000000  // Section is writeable.

typedef struct COFF_SECTION_HDR {
	uint8_t name[8];
	uint32_t virtualSize;
	uint32_t virtualAddress;
	uint32_t sizeOfRawData;
	uint32_t pointerToRawData;
	uint32_t pointerToRelocations;
	uint32_t pointerToLineNumbers;
	uint16_t numberOfRelocations;
	uint16_t numberOfLineNumbers;
	uint32_t characteristics;
}COFF_SECTION_HDR, *PCOFF_SECTION_HDR;

/* symbol number values. */
#define COFF_SYM_UNDEFINED  0
#define COFF_SYM_ABSOLUTE  -1
#define COFF_SYM_DEBUG     -2

/* symbol data types. */
#define COFF_DT_FUNCTION   0x20

/* storage class values. */
#define COFF_SYM_CLASS_END_OF_FUNCTION -1
#define COFF_SYM_CLASS_NULL             0
#define COFF_SYM_CLASS_AUTOMATIC        1
#define COFF_SYM_CLASS_EXTERNAL         2
#define COFF_SYM_CLASS_STATIC           3
#define COFF_SYM_CLASS_REGISTER         4
#define COFF_SYM_CLASS_EXTERNAL_DEF     5
#define COFF_SYM_CLASS_LABEL            6
#define COFF_SYM_CLASS_UNDEFINED_LABEL  7
#define COFF_SYM_CLASS_MEMBER_OF_STRUCT 8
#define COFF_SYM_CLASS_ARGUMENT         9
#define COFF_SYM_CLASS_STRUCT_TAG       10
#define COFF_SYM_CLASS_MEMBER_OF_UNION  11
#define COFF_SYM_CLASS_UNION_TAG        12
#define COFF_SYM_CLASS_TYPE_DEFINITION  13
#define COFF_SYM_CLASS_UNDEFINED_STATIC 14
#define COFF_SYM_CLASS_ENUM_TAG         15
#define COFF_SYM_CLASS_MEMBER_OF_ENUM   16
#define COFF_SYM_CLASS_REGISTER_PARAM   17
#define COFF_SYM_CLASS_BIT_FIELD        18
#define COFF_SYM_CLASS_BLOCK            100
#define COFF_SYM_CLASS_FUNCTION         101
#define COFF_SYM_CLASS_END_OF_STRUCT    102
#define COFF_SYM_CLASS_FILE             103
#define COFF_SYM_CLASS_SECTION          104
#define COFF_SYM_CLASS_WEAK_EXTERNAL    105

typedef struct COFF_SYMBOL_OFFSET {
	unsigned long zero;
	unsigned long offset;
}COFF_SYMBOL_OFFSET, *PCOFF_SYMBOL_OFFSET;

typedef struct COFF_SYMBOL {
	union {
		char shortname[8];
		COFF_SYMBOL_OFFSET e;
	}u;
	int32_t value;
	int16_t section;
	int16_t type;
	int8_t storageClass;
	int8_t numAuxSymbols;
}COFF_SYMBOL, *PCOFF_SYMBOL;

typedef struct COFF_SYMBOL_AUX_FUNCTION {
	uint32_t tagIndex;
	uint32_t totalSize;
	uint32_t pointerToLineNumber;
	uint32_t pointerToNextFunction;
	uint16_t unused;
}COFF_SYMBOL_AUX_FUNCTION, *PCOFF_SYMBOL_AUX_FUNCTION;

typedef struct COFF_SYMBOL_AUX_BF_EF {
	uint32_t unused;
	uint16_t lineNumber;
	uint8_t unused2[6];
	uint32_t pointerToNextFunction; // SymbolAuxBF only.
	uint16_t unused3;
}COFF_SYMBOL_AUX_BF_EF, *PCOFF_SYMBOL_AUX_BF_EF;

typedef struct COFF_SYMBOL_AUX_WEAK_EXTERNAL {
	uint32_t tagIndex;
	uint32_t characteristics;
	uint8_t unused[10];
}COFF_SYMBOL_AUX_WEAK_EXTERNAL, *PCOFF_SYMBOL_AUX_WEAK_EXTERNAL;

typedef struct COFF_SYMBOL_AUX_FILE {
	char fileName[18];
}COFF_SYMBOL_AUX_FILE, *PCOFF_SYMBOL_AUX_FILE;

typedef struct COFF_AUX_SECTION_DEF {
	uint32_t length;
	uint16_t numberOfRelocations;
	uint16_t numberOfLineNumbers;
	uint32_t checksum;
	uint16_t number;  // one based index into section table. Used when COMDAT selection = 5.
	uint8_t selection; // COMDAT selection number.
	uint8_t unused[3];
}COFF_AUX_SECTION_DEF, *PCOFF_AUX_SECTION_DEF;

typedef struct COFF_STRING_TABLE_HDR {
	uint32_t totalSize;
}COFF_STRING_TABLE_HDR, *PCOFF_STRING_TABLE_HDR;

typedef struct COFF_DATA_DIR {
	uint32_t rva;
	uint32_t size;
}COFF_DATA_DIR, *PCOFF_DATA_DIR;

// Directory Entries

#define COFF_DIRECTORY_ENTRY_EXPORT          0   // Export Directory
#define COFF_DIRECTORY_ENTRY_IMPORT          1   // Import Directory
#define COFF_DIRECTORY_ENTRY_RESOURCE        2   // Resource Directory
#define COFF_DIRECTORY_ENTRY_EXCEPTION       3   // Exception Directory
#define COFF_DIRECTORY_ENTRY_SECURITY        4   // Security Directory
#define COFF_DIRECTORY_ENTRY_BASERELOC       5   // Base Relocation Table
#define COFF_DIRECTORY_ENTRY_DEBUG           6   // Debug Directory
//      COFF_DIRECTORY_ENTRY_COPYRIGHT       7   // (X86 usage)
#define COFF_DIRECTORY_ENTRY_ARCHITECTURE    7   // Architecture Specific Data
#define COFF_DIRECTORY_ENTRY_GLOBALPTR       8   // RVA of GP
#define COFF_DIRECTORY_ENTRY_TLS             9   // TLS Directory
#define COFF_DIRECTORY_ENTRY_LOAD_CONFIG    10   // Load Configuration Directory
#define COFF_DIRECTORY_ENTRY_BOUND_IMPORT   11   // Bound Import Directory in headers
#define COFF_DIRECTORY_ENTRY_IAT            12   // Import Address Table
#define COFF_DIRECTORY_ENTRY_DELAY_IMPORT   13   // Delay Load Import Descriptors
#define COFF_DIRECTORY_ENTRY_COM_DESCRIPTOR 14   // COM Runtime descriptor

#define COFF_NUMBEROF_DIRECTORY_ENTRIES     16

typedef struct COFF_IMPORT_DIRECTORY_ENTRY {
	uint32_t importLookupTableRVA;
	uint32_t timeDateTimestamp; //date time stamp when bounded by loader
	uint32_t forwarderChain;
	uint32_t nameRVA;
	uint32_t importAddressTableRVA;
}COFF_IMPORT_DIRECTORY_ENTRY, *PCOFF_IMPORT_DIRECTORY_ENTRY;

//
// relocation types
//
#define IMAGE_REL_BASED_ABSOLUTE              0
#define IMAGE_REL_BASED_HIGH                  1
#define IMAGE_REL_BASED_LOW                   2
#define IMAGE_REL_BASED_HIGHLOW               3
#define IMAGE_REL_BASED_HIGHADJ               4
#define IMAGE_REL_BASED_MIPS_JMPADDR          5
#define IMAGE_REL_BASED_MIPS_JMPADDR16        9
#define IMAGE_REL_BASED_IA64_IMM64            9
#define IMAGE_REL_BASED_DIR64                 10

typedef struct COFF_BASE_RELOCATION {
	uint32_t virtualAddress;
	uint32_t symbolTableIndex;
	uint16_t type;
	// COFF_RELOCATION_ENTRY entries[];
}COFF_BASE_RELOCATION, *PCOFF_BASE_RELOCATION;

typedef struct _COFF_RELOCATION_ENTRY {
	unsigned short offset: 12;
	unsigned short type: 4;
}COFF_RELOCATION_ENTRY, *PCOFF_RELOCATION_ENTRY;

typedef struct _COFF_EXPORT_DIRECTORY {
	uint32_t characteristics;
	uint32_t timeDateStamp;
	uint16_t majorVersion;
	uint16_t minorVersion;
	uint32_t name;
	uint32_t base;
	uint32_t numberOfFunctions;
	uint32_t numberOfNames;
	// address of Export Address Table relative to image base
	uint32_t** addressOfFunctions;
	// address of Export Name Pointer Table relative to image base
	uint32_t** addressOfNames;
	// address of Ordinal Table relative to image base
	uint16_t** addressOfNameOrdinal;
}COFF_EXPORT_DIRECTORY, *PCOFF_EXPORT_DIRECTORY;

typedef struct COFF_OPTIONAL_HDR64 {

	COFF_DATA_DIR directories[COFF_NUMBEROF_DIRECTORY_ENTRIES];
}COFF_OPTIONAL_HDR64, *PCOFF_OPTIONAL_HDR64;

typedef struct COFF_OPTIONAL_HDR32 {
	uint16_t magic;
	uint8_t  majorLinkerVersion;
	uint8_t  minorLinkerVersion;
	uint32_t sizeOfCode;
	uint32_t sizeOfInitializedData;
	uint32_t sizeOfUninitializedData;
	uint32_t addressOfEntryPoint;
	uint32_t baseOfCode;
	uint32_t baseOfData;
	uint32_t imageBase;
	uint32_t sectionAlignment;
	uint32_t fileAlignment;
	uint16_t majorOperatingSystemVersion;
	uint16_t minorOperatingSystemVersion;
	uint16_t majorImageVersion;
	uint16_t minorImageVersion;
	uint16_t majorSubsystemVersion;
	uint16_t minorSubsystemVersion;
	uint32_t win32VersionValue;
	uint32_t sizeOfImage;
	uint32_t sizeOfHeaders;
	uint32_t checksum;
	uint16_t subsystem;
	uint16_t dllCharacteristics;
	uint32_t sizeOfStackReserve;
	uint32_t sizeOfStackCommit;
	uint32_t sizeOfHeapReserve;
	uint32_t sizeOfHeapCommit;
	uint32_t loaderFlags;
	uint32_t numberOfDataDirectories;
	COFF_DATA_DIR directories[COFF_NUMBEROF_DIRECTORY_ENTRIES];
}COFF_OPTIONAL_HDR32, *PCOFF_OPTIONAL_HDR32;

typedef struct COFF_HDRS32 {
	uint32_t         signature;
	COFF_FILE_HDR    fileHeader;
	COFF_OPTIONAL_HDR32 optionalHeader;
}COFF_HDRS32, *PCOFF_HDRS32;

typedef struct COFF_HDRS64 {
	uint32_t         signature;
	COFF_FILE_HDR    fileHeader;
	COFF_OPTIONAL_HDR64 optionalHeader;
}COFF_HDRS64, *PCOFF_HDRS64;

typedef struct COFF_DOS_HDR {
	uint16_t magic;
	uint16_t cblp;
	uint16_t cp;
	uint16_t crlc;
	uint16_t cparhdr;
	uint16_t minalloc;
	uint16_t maxalloc;
	uint16_t ss;
	uint16_t sp;
	uint16_t csum;
	uint16_t ip;
	uint16_t cs;
	uint16_t lfarlc;
	uint16_t ovno;
	uint16_t res[4];
	uint16_t oemid;
	uint16_t oeminfo;
	uint16_t res2[10];
	uint32_t lfanew;
}COFF_DOS_HDR, *PCOFF_DOS_HDR;

//
// static library (ARCHIVE)
//

// import types:
#define ARCHIVE_IMPORT_CODE  0
#define ARCHIVE_IMPORT_DATA  1
#define ARCHIVE_IMPORT_CONST 2

// import name type:
#define ARCHIVE_IMPORT_ORDINAL         0
#define ARCHIVE_IMPORT_NAME            1
#define ARCHIVE_IMPORT_NAME_NOPREFIX   2
#define ARCHIVE_IMPORT_NAME_UNDECORATE 3

#define ARCHIVE_SIGNATURE              "!<arch>\n"
#define ARCHIVE_PAD                    "\n"
#define ARCHIVE_LINKER_MEMBER          "/               "
#define ARCHIVE_LONGNAMES_MEMBER       "//              "

typedef struct ARCHIVE_HDR {
	uint8_t name[16];                          // File member name - `/' terminated.
	uint8_t date[12];                          // File member date - decimal.
	uint8_t userID[6];                         // File member user id - decimal.
	uint8_t groupID[6];                        // File member group id - decimal.
	uint8_t mode[8];                           // File member mode - octal.
	uint8_t size[10];                          // File member size - decimal.
	uint8_t endHeader[2];                      // String to end header.
} ARCHIVE_HDR, *PARCHIVE_HDR;

#define SIZEOF_ARCHIVE_MEMBER_HDR      60
#define ALIGN(x)                       ((x + 1) & ~1)


#endif
