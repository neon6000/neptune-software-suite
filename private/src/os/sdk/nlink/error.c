/************************************************************************
*
*	error.c - Error and warning output
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <stdio.h>
#include <stdarg.h>
#include "nlink.h"

// interesting LNK1237
// at LNK1314
// https://docs.microsoft.com/en-us/cpp/error-messages/tool-errors/linker-tools-error-lnk1000
typedef struct MESSAGE {
	int      type;
	char*    s;
}MESSAGE, *PMESSAGE;

MESSAGE _messages[] = {

	{ ERR_UNKNOWN, "Unknown error" },
	{ ERR_INFO, "%s" },
	{ ERR_DEBUG_CORRUPT, "Debug information corrupt, recompile module" },
	{ ERR_FILE_OPEN, "Unable to open file '%s'" },
	{ ERR_FILE_READ, "Unable to read file '%s'" },
	{ ERR_MACHINE_TYPE_MISMATCH, "Module machine type '%s' conflicts with target machine type '%s'" },
	{ ERR_MACHINE_TYPE, "Invalid machine type in module '%s'" },
	{ ERR_UNRESOLVED_COUNT, "%u unresolved externals" },
	{ ERR_LIBRARY_CORRUPT, "Library '%s' is corrupt" },
	{ ERR_HEADER_CORRUPT, "Input file '%s' is corrupt" },
	{ ERR_COMDAT_CORRUPT, "Invalid or corrupt file: no symbol for COMDAT section number" },
	{ ERR_UNDEC_UNRESOLVED, "Cannot resolve one or more undecorated symbols" },
	{ ERR_SECTION_ALIGN, "'%s' section alignment %i greater then /ALIGN value" },
	{ ERR_CODE_ADJUST, "Cannot adjust code at offset=0x%x, va=0x%x" },
	{ ERR_FILE_WRITE, "Cannot open file '%s' for writing" },
	{ ERR_MULTIPLE_DEFINITIONS, "One or more multiply defined symbols found" },
	{ ERR_DUPLICATE_COMDAT, "Invalid or corrupt file: Duplicate COMDAT '%s'" },
	{ ERR_INPUT_FILE, "Cannot open input file '%s'"},
	{ ERR_SUBSYSTEM, "A subsystem can't be inferred and must be defined" },
	{ ERR_PDATA_CORRUPT, "Invalid or corrupt file '%s': file contains invalid .pdata contributions" },
	{ ERR_IMAGE_BASE, "Invalid image base address 0x%x" },
	{ ERR_IMAGE_SIZE, "Image size exceeds maximum allowable size (0x80000000)" },
	{ WRN_LIBRARIES_ONLY, "No object files specified, libraries used" }
};

PUBLIC unsigned int _errorCount;
PUBLIC unsigned int _warnCount;

PUBLIC unsigned int NasmGetErrorCount(void) {

	return _errorCount;
}

PUBLIC unsigned int NasmGetWarnCount(void) {

	return _warnCount;
}

typedef void(*msg_handler_t)(IN FILE*, IN va_list*);

/* message format type. */
typedef struct FORMAT {
	char* format;
	msg_handler_t callback;
}FORMAT, *PFORMAT;

PRIVATE void MsgDisplayStringObject(IN FILE* buf, IN va_list* args) {

	PSTRING val;

	val = va_arg(*args, PSTRING);
	if (val)
		fprintf(buf, "%s", val->value);
}

PRIVATE void MsgDisplayString(IN FILE* buf, IN va_list* args) {

	fprintf(buf, "%s", va_arg(*args, char*));
}

PRIVATE void MsgDisplayLong(IN FILE* buf, IN va_list* args) {

	fprintf(buf, "%i", va_arg(*args, long));
}

PRIVATE void MsgDisplayUnsignedLong(IN FILE* buf, IN va_list* args) {

	fprintf(buf, "%u", va_arg(*args, unsigned long));
}

PRIVATE void MsgDisplayHexLower(IN FILE* buf, IN va_list* args) {

	fprintf(buf, "%x", va_arg(*args, unsigned long));
}

PRIVATE void MsgDisplayHexUpper(IN FILE* buf, IN va_list* args) {

	fprintf(buf, "%X", va_arg(*args, unsigned long));
}

PRIVATE void MsgDisplayChar(IN FILE* buf, IN va_list* args) {

	fprintf(buf, "%c", va_arg(*args, char));
}

PRIVATE void MsgDisplayPercent(IN FILE* buf, IN va_list* args) {

	fprintf(buf, "%s", "%");
}

PRIVATE FORMAT _formats[] = {

	{ "%%", MsgDisplayPercent },
	{ "%c", MsgDisplayChar },
	{ "%x", MsgDisplayHexLower },
	{ "%X", MsgDisplayHexUpper },
	{ "%u", MsgDisplayUnsignedLong },
	{ "%i", MsgDisplayLong },
	{ "%s", MsgDisplayString },
	{ "%S", MsgDisplayStringObject },
	{ NULL, NULL }
};

PRIVATE PFORMAT GetFormat(IN char* s) {

	int max;
	int i;

	max = sizeof(_formats) / sizeof(FORMAT);
	for (i = 0; _formats[i].format && i < max; i++) {
		if (strncmp(_formats[i].format, s, 2) == 0)
			return &_formats[i];
	}

	return NULL;
}

PRIVATE PMESSAGE GetMessage(IN int code) {

	int max;
	int i;

	max = sizeof(_messages) / sizeof(MESSAGE);
	for (i = 0; _messages[i].type && i < max; i++) {
		if (_messages[i].type == code)
			return _messages[i].s;
	}

	return NULL;
}

PRIVATE void Print(IN FILE* stream, IN char* s, IN va_list args) {

	PFORMAT spec;

	while (*s) {
		if (*s == '%') {
			spec = GetFormat(s);
			if (spec && spec->callback) {
				spec->callback(stream, &args);
				s += strlen(spec->format);
				continue;
			}
		}
		fprintf(stream, "%c", *s++);
	}
}

PUBLIC void NlinkError(IN ERRLEVEL level, IN long code, ...) {

	va_list  args;
	FILE*    file;
	char*    msg;

	file = stdout;

	msg = GetMessage(code);
	if (!msg)
		msg = "*** BUGCHECK: (Unknown message)";

	switch (level) {
	case LevelInfo:
		file = stdout;
		fprintf(file, "\n\rinfo: ");
		break;
	case LevelWarn:
		file = stdout;
		fprintf(file, "\n\rwarn: ");
		_warnCount++;
		break;
	case LevelError:
		file = stderr;
		fprintf(file, "\n\rerror: ");
		_errorCount++;
		break;
	case LevelFatal:
		file = stderr;
		fprintf(file, "\n\rpanic: ");
		_errorCount++;
	};

	va_start(args, code);
	Print(file, msg, args);
	va_end(code);

	if (level == LevelFatal) {
#ifdef _MSC_VER
		__debugbreak();
#endif
		exit(-1);
	}
}

