/************************************************************************
*
*	nlink.h
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <time.h>

#ifndef NLINK_H
#define NLINK_H

#define IN
#define OUT
#define OPTIONAL
#define PRIVATE static
#define PUBLIC
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))
#define LITTLE_ENDIAN(x) ((x >> 24) | ((x & 0xff0000) >> 16) | ((x & 0xff00) >> 8) | (x & 0xff))

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long uint64_t;
typedef signed char sint8_t;
typedef signed short sint16_t;
typedef signed int sint32_t;
typedef signed long long sint64_t;
typedef char int8_t;
typedef short int16_t;
typedef int int32_t;
typedef long long int64_t;
typedef int bool_t;
typedef bool_t BOOL;
#define TRUE 1
#define FALSE 0
#define INVALID_HANDLE ((unsigned int)0)
typedef unsigned int index_t;
typedef unsigned int offset_t;
typedef unsigned int size_t;
typedef void* addr_t;
typedef void* handle_t;

#define PRIVATE static
#define PUBLIC

#define ALLOC_OBJ(t) (malloc(sizeof(t)))

#define ON(v,n)    ((v&n)==(n))
#define OFF(A,B)   (!ON(A,B)) 
#define CLEAR(v,n) (v &= ~(n))
#define SET(v,n)   (v |= (n))
#define FLIP(v,n)  (ON(v,(n)) ? CLEAR(v,(n)) : SET(v,(n)))

#define ALIGN(x)   ((x + 1) & ~1)

#define ERR_BASE           0x1000
#define WRN_BASE           0x4000

#define ERR_UNKNOWN                (ERR_BASE)
#define ERR_DEBUG_CORRUPT          (ERR_BASE+1)
#define ERR_FILE_OPEN              (ERR_BASE+2)
#define ERR_FILE_READ              (ERR_BASE+3)
#define ERR_MACHINE_TYPE_MISMATCH  (ERR_BASE+4)
#define ERR_MACHINE_TYPE           (ERR_BASE+5)
#define ERR_UNRESOLVED_COUNT       (ERR_BASE+6)
#define ERR_LIBRARY_CORRUPT        (ERR_BASE+7)
#define ERR_HEADER_CORRUPT         (ERR_BASE+8)
#define ERR_COMDAT_CORRUPT         (ERR_BASE+9)
#define ERR_UNDEC_UNRESOLVED       (ERR_BASE+10)
#define ERR_SECTION_ALIGN          (ERR_BASE+11)
#define ERR_CODE_ADJUST            (ERR_BASE+12)
#define ERR_FILE_WRITE             (ERR_BASE+13)
#define ERR_MULTIPLE_DEFINITIONS   (ERR_BASE+14)
#define ERR_DUPLICATE_COMDAT       (ERR_BASE+15)
#define ERR_INPUT_FILE             (ERR_BASE+16)
#define ERR_SUBSYSTEM              (ERR_BASE+17)
#define ERR_PDATA_CORRUPT          (ERR_BASE+18)
#define ERR_IMAGE_BASE             (ERR_BASE+19)
#define ERR_IMAGE_SIZE             (ERR_BASE+20)
#define ERR_INFO                   (ERR_BASE+21)

#define WRN_LIBRARIES_ONLY  (WRN_BASE + 1)

typedef enum ERRLEVEL {
	LevelInfo,
	LevelWarn,
	LevelError,
	LevelFatal
}ERRLEVEL;

PUBLIC void NlinkError(IN ERRLEVEL level, IN long code, ...);

#define Info(code, ...) NlinkError(LevelInfo, code, __VA_ARGS__)
#define Warn(code, ...) NlinkError(LevelWarn, code, __VA_ARGS__)
#define Error(code, ...) NlinkError(LevelError, code, __VA_ARGS__)
#define Fatal(code, ...) NlinkError(LevelFatal, code, __VA_ARGS__)

typedef struct STRING STRING, *PSTRING;
typedef struct STRING {
	char*    value;
	unsigned refcount;
	offset_t offset1;
	offset_t offset2;
	PSTRING  next;
}STRING, *PSTRING;

extern PUBLIC PSTRING InternString(IN char* string);
extern PUBLIC void FreeStringTable(void);
extern PUBLIC void DumpStringTable(void);

typedef struct STRLIST STRLIST, *PSTRLIST;
typedef struct STRLIST {
	PSTRING  str;
	PSTRLIST next;
}STRLIST, *PSTRLIST;

typedef struct BUFFER {
	char magic[4];
	size_t elementCount;
	size_t allocCount;
	uint8_t* data;
}BUFFER, *PBUFFER;

PUBLIC PBUFFER  NewBuffer(void);
PUBLIC void     BufferFree(IN PBUFFER r);
PUBLIC uint8_t* GetBufferData(IN PBUFFER b);
PUBLIC size_t   GetBufferLength(IN PBUFFER b);
PUBLIC void     BufferWrite(IN PBUFFER b, IN char c);
PUBLIC void     BufferAppend(IN PBUFFER b, IN char* s, IN int len);
PUBLIC void     BufferPrintf(IN PBUFFER b, IN char* fmt, ...);
PUBLIC void     BufferPush(IN PBUFFER b, IN char c);
PUBLIC char     BufferPop(IN PBUFFER b);

typedef struct STRING STRING, *PSTRING;
typedef struct MAPELEMENT *PMAPELEMENT;
typedef struct MAPELEMENT {
	PMAPELEMENT next;
	unsigned    bucket;
	PSTRING     key;
	void*       value;
}MAPELEMENT, *PMAPELEMENT;

typedef struct MAP {
	PMAPELEMENT* buckets;
	size_t       numberOfBuckets;
	size_t       numberOfElements;
}MAP, *PMAP;

typedef void(*map_free_t)(PMAPELEMENT);

extern PUBLIC PMAP   MapCreate(IN unsigned long numberOfBuckets);
extern PUBLIC void*  MapGet(IN PMAP map, IN char* key);
extern PUBLIC BOOL   MapPut(IN PMAP map, IN char* key, IN void* value);
extern PUBLIC void   MapFree(IN PMAP map, IN OPTIONAL map_free_t callback);
extern PUBLIC long   MapElementCount(IN PMAP dictionary);
extern PUBLIC BOOL   MapRemove(IN PMAP map, IN char* key, IN void* value);

typedef struct RECORD {
	PSTRING  name;
	uint32_t size;
	handle_t stream;
}RECORD, *PRECORD;

typedef struct MODULE MODULE, *PMODULE;
typedef struct ARCHIVE ARCHIVE, *PARCHIVE;
typedef struct SECTION SECTION, *PSECTION;
typedef struct RELOC RELOC, *PRELOC;

typedef struct RELOC {
	PSECTION  parent;
	offset_t  addr;
	int       type;
	PRELOC    next;
}RELOC, *PRELOC;

typedef struct SECTION {
	PMODULE   parent;
	PSTRING   name;
	offset_t  modstart;
	offset_t  modend;
	offset_t  virtstart;
	offset_t  virtend;
	unsigned  virtalign;
	PBUFFER   data;
	PSECTION  next;
}SECTION, *PSECTION;

typedef struct MODULE {
	PSTRING  name;
	PRECORD  srcfile;
	PARCHIVE archive;
	offset_t archiveOffset;
	PSECTION sections;
	PSTRLIST symbols;
	time_t   date;
	PMODULE  next;
}MODULE, *PMODULE;

typedef struct TARGET {
	PSTRING  name;
	PSECTION sections;
}TARGET, *PTARGET;

typedef struct ARCHIVE {
	PRECORD   file;
	PARCHIVE  next;
	char*     longnames;
	uint32_t  modcount;
	uint32_t  symcount;
	// these all point inside "raw":
	offset_t* modoffsets;
	uint16_t* idxtab;
	char**    symnames;
	char*     raw;
}ARCHIVE, *PARCHIVE;

typedef struct SYMLINK {
	PSTRING sym;
	PMODULE export;
	PMODULE imports;
}SYMLINK;

extern PARCHIVE _archives;
extern PMODULE  _modules;

// pecoff.c:

extern PMODULE AddModule  (IN PSTRING file);

// file.c:

PRECORD  OpenFile   (IN PSTRING fname);
void   CloseFile  (IN PRECORD file);
BOOL   FileSeek   (IN PRECORD file, IN index_t loc);
BOOL   FileRename (IN char* from, IN char* to);
BOOL   FileRemove (IN char* file);
PRECORD  FileCreate (IN char* name);
size_t FileRead   (IN PRECORD file, IN size_t size, OUT void* data);
size_t FileWrite  (IN PRECORD file, IN size_t size, IN void* data);
long   FileTell   (IN PRECORD file);
size_t FileSize   (IN PRECORD file);
BOOL   FileExists (IN char* filename);
time_t FileModifyTime  (IN PRECORD file);
PRECORD  FileCreateTemp  (IN char* base);

#endif
