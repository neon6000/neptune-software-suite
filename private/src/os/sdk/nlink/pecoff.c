/************************************************************************
*
*	pecoff.c
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <assert.h>
#include <memory.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "nlink.h"
#include "pecoff.h"

char* CoffGetStringTable(IN PRECORD rec, IN PCOFF_FILE_HDR hdr) {

	COFF_STRING_TABLE_HDR strtab;
	char*    strings;
	offset_t base;
	long     loc;

	loc = FileTell(rec);

	// string table is always after the symbol table:

	base = hdr->pointerToSymbolTable
		+ hdr->numberOfSymbols * sizeof(COFF_SYMBOL);

	FileSeek(rec, base);
	FileRead(rec, sizeof(COFF_STRING_TABLE_HDR), &strtab);

	// strings pointer must include STRING_TABLE_HDR at the start
	// so we must go back and re-read from base:

	strings = malloc(strtab.totalSize);
	FileSeek(rec, base);
	FileRead(rec, strtab.totalSize, strings);

	FileSeek(rec, loc);

	return strings;
}

PRIVATE PMODULE _CoffRead(IN PRECORD rec, IN PSTRING name) {

	PMODULE          mod;
	COFF_FILE_HDR    hdr;
	COFF_SECTION_HDR sect;
	COFF_SYMBOL      sym;
	COFF_BASE_RELOCATION  reloc;
	index_t          idx;
	char*            strtab;
	char*            str;

	FileRead(rec, sizeof(COFF_FILE_HDR), &hdr);

	strtab = CoffGetStringTable(rec, &hdr);

	for (idx = 0; idx < hdr.numberOfSections; idx++) {
		FileRead(rec, sizeof(COFF_SECTION_HDR), &sect);
		if (sect.numberOfRelocations > 0)
			sect.pointerToRelocations;
	}

	FileSeek(rec, hdr.pointerToSymbolTable);
	for (idx = 0; idx < hdr.numberOfSymbols; idx++) {

		FileRead(rec, sizeof(COFF_SYMBOL), &sym);

		if (!sym.u.e.zero) {
			if (sym.u.e.offset) {
				str = strtab + sym.u.e.offset;
				// str => string of symbol
			}
		}
	}

	__debugbreak();


	// free(strtab)

	return NULL;

}

PUBLIC PMODULE CoffOpen(IN PSTRING name) {

	PRECORD          rec;

	rec = OpenFile(name);
	if (!rec) return NULL;

	return _CoffRead(rec, name);
}

/**
*	LibGetFileName
*
*	Description : Reads the file name for "hdr"
*
*	Output : File name
*/
PRIVATE PSTRING LibGetFileName(IN PARCHIVE arch, IN PARCHIVE_HDR hdr) {

	char        shortname[16];
	offset_t    nameoff;
	index_t     idx;

	if (hdr->name[0] == '/') {
		nameoff = strtoul(&hdr->name[1], NULL, 10);
		return InternString(&arch->longnames[nameoff]);
	}

	for (idx = 0; idx < 16; idx++) {
		if (hdr->name[idx] == '/') {
			shortname[idx] = '\0';
			break;
		}
		shortname[idx] = hdr->name[idx];
	}
	return InternString(shortname);
}

/**
*	_LibGetSymbol
*
*	Description : Test if symbol is defined in archive
*
*	Input : arch - Opened archive
*			symbol - Symbol to search for
*			idx - Optional output to set index if TRUE is returned
*
*	Output : TRUE if defined, FALSE if not
*/
PRIVATE BOOL _LibGetSymbol(IN PARCHIVE arch, IN PSTRING symbol, OUT OPTIONAL index_t* idx) {

	index_t start;
	index_t end;
	index_t mid;
	int     cmp;

	start = 0;
	end = arch->symcount;

	while (start < end) {
		mid = (start + end) / 2;
		cmp = strcmp(arch->symnames[mid], symbol->value);
		if (cmp < 0)
			start = mid + 1;
		else if (cmp > 0)
			end = mid;
		else {
			if (idx) *idx = mid;
			return TRUE;
		}
	}
	return FALSE;
}

/**
*	LibGetModuleOffset
*
*	Description : Given a symbol mapped to some module, this returns
*		the offset of the module in the specified archive
*
*	Input : arch - Opened archive
*			symbol - Symbol that exists in archive
*
*	Output : module offset ot 0 if not found
*/
PRIVATE offset_t LibGetModuleOffset(IN PARCHIVE arch, IN PSTRING symbol) {

	uint16_t    modindex;
	index_t     symidx;
	offset_t    offset;

	if (!_LibGetSymbol(arch, symbol, &symidx))
		return 0;

	modindex = arch->idxtab[symidx];

	// the symbol index to module index array
	// is an array of 1-based indices:
	assert(modindex > 0);

	// get module offset:
	return arch->modoffsets[modindex-1];
}

/**
*	LibIsDefined
*
*	Description : Test if symbol is defined in archive
*
*	Output : TRUE if defined, FALSE if not
*/
PUBLIC BOOL LibIsDefined(IN PARCHIVE arch, IN PSTRING symbol) {

	return _LibGetSymbol(arch, symbol, NULL);
}

/**
*	Given a symbol, ADD its module to the list if needed
*/
PUBLIC void* LibReadFile(IN PARCHIVE arch, IN PSTRING symbol) {

	ARCHIVE_HDR hdr;
	PSTRING     modname;
	offset_t    offset;

	offset = LibGetModuleOffset(arch, symbol);

	FileSeek(arch->file, offset);
	FileRead(arch->file, sizeof(ARCHIVE_HDR), &hdr);
	modname = LibGetFileName(arch, &hdr);

	_CoffRead(arch->file, modname);

	__debugbreak();
}

/**
*	LibOpen
*
*	Description : Opens archive and prepares it for reading
*
*	Output : Pointer to archive object or NULL
*/
PARCHIVE LibOpen(IN PSTRING name) {

	PRECORD     rec;
	PARCHIVE    archive;
	ARCHIVE_HDR hdr;
	long        fpos;
	size_t      size;
	char*       longnames;
	char        sig[8];
	index_t     idx;
	char*       cur;

	rec = OpenFile(name);
	if (!rec) return NULL;

	FileRead(rec, 8, sig);
	if (memcmp(sig, ARCHIVE_SIGNATURE, 8)) {
		CloseFile(rec);
		return NULL;
	}

	// read first linker member:

	FileRead(rec, sizeof(ARCHIVE_HDR), &hdr);
	if (memcmp(hdr.name, ARCHIVE_LINKER_MEMBER, 16)) {
		CloseFile(rec);
		return NULL;
	}
	size = strtoul((const char *)hdr.size, NULL, 10);
	FileSeek(rec, FileTell(rec) + ALIGN(size));

	// read second linker member:

	FileRead(rec, sizeof(ARCHIVE_HDR), &hdr);
	if (memcmp(hdr.name, ARCHIVE_LINKER_MEMBER, 16)) {
		CloseFile(rec);
		return NULL;
	}
	size = strtoul((const char *)hdr.size, NULL, 10);
	fpos = FileTell(rec);

	//
	// Second Linker Member format:
	//
	// +----------+--------------+----------+--------------+------------+
	// | modcount | modoffsets[] | symcount |   symidx[]   | symnames[] |
	// +----------+--------------+----------+--------------+------------+
	//   4 bytes    4 byte elmnt   4 byte     2 byte elmnt   CString array
	//
	// symnames[symIdx] = modoffsets[symIdx]
	// modoffsets[symIdx] is file offset of module
	// symnames is in lexagraphic sort order for binary search
	//

	archive = malloc(sizeof(ARCHIVE));
	memset(archive, 0, sizeof(ARCHIVE));

	archive->raw = malloc(size);
	FileRead(rec, size, archive->raw);
	FileSeek(rec, fpos);

	cur = archive->raw;
	archive->modcount = *(uint32_t*)cur;
	archive->modoffsets = cur + 4;

	cur += (archive->modcount * 4) + 4;
	archive->symcount = *(uint32_t*)cur;
	archive->idxtab = cur + 4;

	cur += (archive->symcount * 2) + 4;
	archive->symnames = malloc(sizeof(char*) * archive->symcount);
	for (idx = 0; idx < archive->symcount; idx++) {
		archive->symnames[idx] = cur;
		cur += strlen(cur) + 1;
	}

	// read long name linker member:

	FileSeek(rec, fpos + ALIGN(size));

	FileRead(rec, sizeof(ARCHIVE_HDR), &hdr);
	if (memcmp(hdr.name, ARCHIVE_LONGNAMES_MEMBER, 16)) {
		// FREE archive
		CloseFile(rec);
		return NULL;
	}
	size = strtoul((const char *)hdr.size, NULL, 10);
	longnames = malloc(size);
	fpos = FileTell(rec);
	FileRead(rec, size, longnames);
	FileSeek(rec, fpos + ALIGN(size));
	archive->longnames = longnames;
	archive->file = rec;

	return archive;
}

#if 0

// given an object file, return a list of EXTERN symbol names:
PUBLIC StringList* ExtractSymbols(IN File* object, IN int seekBase) {

	CoffFileHeader fh;
	Symbol         symbol;
	StringList*    symbols;
	uint32_t       numSymbols;
	char*          stringTable;
	uint32_t       currentSymbol;
	uint32_t       stringTableSize;

	stringTableSize = 0;
	stringTable = NULL;

	// get number of symbols:

	FileRead(object, sizeof(CoffFileHeader), &fh);
	numSymbols = fh.numberOfSymbols;

	if (!numSymbols)
		return NULL;

	//	if (fh.machine != IMAGE_FILE_MACHINE_I386 || fh.machine != IMAGE_FILE_MACHINE_IA64)
	//		return NULL;

	// read in string table. String table is located right after
	// symbol table:

	FileSeek(object, fh.pointerToSymbolTable + numSymbols * sizeof(Symbol)+seekBase);
	FileRead(object, sizeof(uint32_t), &stringTableSize);
	if (stringTableSize != 0) {
		// "stringTableSize" is size of symbol table + size of itself in the file.
		// so we subtract its size here:
		stringTableSize -= 4;
		stringTable = malloc(stringTableSize);
		FileRead(object, stringTableSize, stringTable);
	}

	// read symbols:

	symbols = NewStringList();
	FileSeek(object, fh.pointerToSymbolTable + seekBase);
	for (currentSymbol = 0; currentSymbol < numSymbols; currentSymbol++) {

		int currentAux;

		// read symbol:

		FileRead(object, sizeof(Symbol), &symbol);

		// only add symbols that are EXTERNAL and either VALUE or a valid SECTION number:

		if (symbol.storageClass == IMAGE_SYM_CLASS_EXTERNAL && (symbol.value || symbol.section)) {
			if (symbol.u.e.zero == 0) {
				char* realname = stringTable + symbol.u.e.offset - 4;
				AddString(symbols, _strdup(realname), TRUE);
			}
			else{
				char realname[9];
				memcpy(realname, symbol.u.shortname, 8);
				realname[8] = 0;
				AddString(symbols, _strdup(realname), TRUE);
			}
		}

		// skip over any auxiliary symbol enteries:

		for (currentAux = 0; currentAux < symbol.numAuxSymbols; currentAux++) {
			Symbol aux;
			FileRead(object, sizeof(Symbol), &aux);
			currentSymbol++;
		}
	}
	return symbols;
}


#endif

PRIVATE char _stub[] =
	"\x0e\x1f\xba\x0e\x00\xb4\x09\xcd\x21\xb8\x01\x4c\xcd\x21\x54\x68"
	"\x69\x73\x20\x70\x72\x6f\x67\x72\x61\x6d\x20\x63\x61\x6e\x6e\x6f"
	"\x74\x20\x62\x65\x20\x72\x75\x6e\x20\x69\x6e\x20\x44\x4f\x53\x20"
	"\x6d\x6f\x64\x65\x2e\x0d\x0d\x0a\x24\x00\x00\x00\x00\x00\x00\x00"
	"\xf3\x96\x03\xd1\xb7\xf7\x6d\x82\xb7\xf7\x6d\x82\xb7\xf7\x6d\x82"
	"\xf1\xa6\xb0\x82\xb4\xf7\x6d\x82\x29\x57\xaa\x82\xb6\xf7\x6d\x82"
	"\xf1\xa6\x8d\x82\xa4\xf7\x6d\x82\xf1\xa6\x8c\x82\xb0\xf7\x6d\x82"
	"\x6a\x08\xa6\x82\xb5\xf7\x6d\x82\xb7\xf7\x6c\x82\xf8\xf7\x6d\x82"
	"\xba\xa5\x8d\x82\xb0\xf7\x6d\x82\xba\xa5\xb6\x82\xb6\xf7\x6d\x82"
	"\xba\xa5\xb3\x82\xb6\xf7\x6d\x82\x52\x69\x63\x68\xb7\xf7\x6d\x82"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00";

#define COFF_FILE_ALIGN 0x200

void ImageBuildOptionalHdr(OUT PCOFF_OPTIONAL_HDR32 hdr) {

	memset(hdr, 0, sizeof(COFF_OPTIONAL_HDR32));

	hdr->magic = COFF_NT_OPTIONAL_HDR32_MAGIC;
	hdr->fileAlignment = COFF_FILE_ALIGN;
	hdr->numberOfDataDirectories = COFF_NUMBEROF_DIRECTORY_ENTRIES;
	//
	// ROUNDED to a MULTIPLE of the value FileAlignment
	//
	hdr->sizeOfHeaders = sizeof(COFF_OPTIONAL_HDR32)
		+sizeof(COFF_FILE_HDR);
		+4 // 4 byte signature
		+4 // dos e_lfanew field
		+0; // size of ALL section headers

	// linker command line
	hdr->imageBase;
	hdr->subsystem;
	hdr->sectionAlignment;
	hdr->dllCharacteristics;
	hdr->majorOperatingSystemVersion;
	hdr->majorSubsystemVersion;

	// other ---

	hdr->baseOfCode;
	hdr->baseOfData;

	hdr->addressOfEntryPoint;

	hdr->sizeOfCode; // size of raw data of all code sections
	hdr->sizeOfInitializedData;  // size of raw data of all code sections
	hdr->sizeOfUninitializedData;
	hdr->sizeOfImage;
	hdr->sizeOfStackReserve;
	hdr->sizeOfStackCommit;
	hdr->sizeOfHeapReserve;
	hdr->sizeOfHeapCommit;


}

void ImageBuildFileHdr(OUT PCOFF_FILE_HDR hdr) {

	memset(hdr, 0, sizeof(COFF_FILE_HDR));
	hdr->machine = COFF_FILE_MACHINE_I386;
	hdr->numberOfSections = 7; // obviously depends
	hdr->timeDateStamp = (uint32_t) time(NULL);
	hdr->sizeOfOptionalHeader = sizeof(COFF_OPTIONAL_HDR32);
	hdr->characteristics = COFF_FILE_EXECUTABLE_IMAGE | COFF_FILE_32BIT_MACHINE;
}

void ImageBuildDosHdr(OUT PCOFF_DOS_HDR dos) {
	//
	// values don't appear to change:
	//
	memset(dos, 0, sizeof(COFF_DOS_HDR));
	dos->magic = COFF_DOS_SIGNATURE;
	dos->cblp = 0x90; // bytes on last page of file
	dos->cp = 3; // pages in file
	dos->cparhdr = 4; // size of hdr in paragraphs
	dos->maxalloc = 0xffff; // max extra paragraphs
	dos->sp = 0xb8; // initial sp
	dos->crlc = 0x40; // offset to relocation table
	dos->lfanew = sizeof(_stub)+sizeof(COFF_DOS_HDR)-1;
}

void ImageWrite32(IN PTARGET name) {

	COFF_DOS_HDR        dos;
	COFF_HDRS32         hdr;
	COFF_SECTION_HDR    sect;
	COFF_SYMBOL         sym;
	COFF_BASE_RELOCATION     rel;

	ImageBuildDosHdr(&dos);

	hdr.signature = COFF_NT_OPTIONAL_HDR32_MAGIC;
	ImageBuildFileHdr(&hdr.fileHeader);
	ImageBuildOptionalHdr(&hdr.optionalHeader);

	__debugbreak();

	// everything else is 0

	// write "dos"

	// write "stub"

	// write "hdr"

	// write "section hdr array"

	// write sections

}

// https://dandylife.net/blog/archives/388

PUBLIC PMODULE AddModule(IN PSTRING fname) {

	PRECORD file;

#if 0
	file = OpenFile(fname);
	if (!file)
		return NULL;

	// read COFF file header:

	FileRead(file, sizeof(CoffFileHeader), &hdr);

	if (hdr.machine != IMAGE_FILE_MACHINE_I386)
		;
#endif

	return NULL;
}

#if 0


/* The string table follows the symbol table. */
offset_t CoffGetStringTable(IN uint32_t pointerToSymbolTable, IN uint32_t numberOfSymbols) {

	return pointerToSymbolTable + numberOfSymbols * sizeof(Symbol);
}

char* _stringTable;

/* Loads the string table. */
char* CoffLoadStringTable(IN FILE* file, IN CoffFileHeader header) {

	CoffStringTableHeader stringTable;
	offset_t symbolTableOffset;
	long location;
	char* strings;

	strings = NULL;
	symbolTableOffset = CoffGetStringTable(header.pointerToSymbolTable, header.numberOfSymbols);

	location = ftell(file);
	fseek(file, symbolTableOffset, SEEK_SET);
	fread(&stringTable, sizeof(CoffStringTableHeader), 1, file);

	strings = malloc(stringTable.totalSize);
	fread(strings, 1, stringTable.totalSize, file);
	fseek(file, location, SEEK_SET);

	_stringTable = strings;

	return strings;
}

/* Given an offset of a string, return the string from the String Table. */
char* CoffGetStringFromStringTable(IN offset_t offset) {

	/* the offset of the string is relative to the base of the entire string
	table, including header. */
	return _stringTable + offset - sizeof(CoffStringTableHeader);
}

/* Dump the current symbol information. */
Symbol dumpCoffSymbol(IN FILE* file) {

	Symbol symbol;
	fread(&symbol, sizeof(Symbol), 1, file);
	if (symbol.u.shortname[0] != 0) {
		char name[9];
		memcpy(name, symbol.u.shortname, 8);
		name[8] = 0;
		printf("\n\r%s", name);
	}
	else
		printf("\n\r%s", CoffGetStringFromStringTable(symbol.u.e.offset));
	printf("\n\r   Value: %x (%i)", symbol.value, symbol.value);
	printf("\n\r   Section number: %i", symbol.section);
	printf("\n\r   Base type: %x", symbol.type);
	printf("\n\r   Storage class: %x", symbol.storageClass);
	printf("\n\r   Number of aux symbols: %i", symbol.numAuxSymbols);
	return symbol;
}

/* Dump symbol table. */
void dumpFileSymbolTable(IN FILE* file, IN int pointerToSymbolTable, IN int numberOfSymbols) {

	Symbol symbol;

	printf("\n\n\rSymbol table ----------------\n\n\r");
	fseek(file, pointerToSymbolTable, SEEK_SET);
	while (numberOfSymbols--) {
		symbol = dumpCoffSymbol(file);
		/* skip over auxiliary symbol information. */
		while (symbol.numAuxSymbols--) {
			Symbol aux;
			fread(&aux, sizeof(Symbol), 1, file);
			numberOfSymbols--;
		}
	}
}

/* Dump relocation information. */
void CoffDumpRelocations(IN FILE* file, IN long relocCount, IN offset_t sectionRelocOffset) {

	long location;
	Relocation* reloc;
	long i;

	reloc = (Relocation*)malloc(relocCount * sizeof(Relocation));

	location = ftell(file);
	fseek(file, sectionRelocOffset, SEEK_SET);
	fread((void*)reloc, sizeof(Relocation), relocCount, file);
	fseek(file, location, SEEK_SET);

	for (i = 0; i < relocCount; i++) {

		printf("\n\r  Relocation:\n\r");
		printf("\n\r     Virtual Address: 0x%x", reloc[i].virtualAddress);
		printf("\n\r     Type: %i", reloc[i].type);
		printf("\n\r     Symbol Table Index: %i", reloc[i].symbolTableIndex);
	}
}

void dumpStabs(IN FILE* file, IN CoffSectionHeader* stab, IN CoffSectionHeader* stabstr) {

	// from stab.c
	typedef struct STABHDR {
		uint32_t  strx;
		uint8_t   type;
		uint8_t   other;
		uint16_t  desc;
		uint32_t  value;
	}STABHDR, *PSTABHDR;

	long     location;
	char*    strtable;
	STABHDR  hdr;
	offset_t offset;

	location = ftell(file);

	strtable = malloc(stabstr->sizeOfRawData);

	fseek(file, stabstr->pointerToRawData, SEEK_SET);
	fread(strtable, 1, stabstr->sizeOfRawData, file);

	printf("\n\rSTABS");
	printf("\n\r-------------------------\n\r");

	printf("\n\r%-8s%-8s%-8s%-10s%s", "TYPE", "DESC", "OTHER", "VALUE", "STRX");

	fseek(file, stab->pointerToRawData, SEEK_SET);
	for (offset = 0; offset < stab->sizeOfRawData; offset += sizeof(STABHDR)) {

		fread(&hdr, 1, sizeof(STABHDR), file);

		printf("\n\r%-8X%-8i%-8i%-10X%s",
			hdr.type, hdr.desc, hdr.other, hdr.value,
			hdr.strx ? &strtable[hdr.strx] : "0");
	}

	free(strtable);
	fseek(file, location, SEEK_SET);
}

/* Dump current section header. */
CoffSectionHeader dumpFileSection(IN FILE* file) {

	CoffSectionHeader header;

	fread(&header, sizeof(CoffSectionHeader), 1, file);

	printf("\n\rSection name: %s", header.name);
	printf("\n\r   Virtual size: %x", header.virtualSize);
	printf("\n\r   Virtual address: %x", header.virtualAddress);
	printf("\n\r   Size to raw data: %x", header.sizeOfRawData);
	printf("\n\r   Pointer to raw data: %x", header.pointerToRawData);
	printf("\n\r   Pointer to relocations: %x", header.pointerToRelocations);
	printf("\n\r   Pointer to line numbers: %x", header.pointerToLineNumbers);
	printf("\n\r   Number of relocations: %x", header.numberOfRelocations);
	printf("\n\r   Number of line numbers: %x", header.numberOfLineNumbers);
	printf("\n\r   Characteristics: %x", header.characteristics);
	if (header.characteristics & IMAGE_SCN_TYPE_NO_PAD)
		printf(" IMAGE_SCN_TYPE_NO_PAD");
	if ((header.characteristics & IMAGE_SCN_CNT_CODE) == IMAGE_SCN_CNT_CODE)
		printf(" IMAGE_SCN_CNT_CODE");
	if ((header.characteristics & IMAGE_SCN_CNT_INITIALIZED_DATA) == IMAGE_SCN_CNT_INITIALIZED_DATA)
		printf(" IMAGE_SCN_CNT_INITIALIZED_DATA");
	if ((header.characteristics & IMAGE_SCN_CNT_UNINITIALIZED_DATA) == IMAGE_SCN_CNT_UNINITIALIZED_DATA)
		printf(" IMAGE_SCN_CNT_UNINITIALIZED_DATA");
	if ((header.characteristics & IMAGE_SCN_LNK_INFO) == IMAGE_SCN_LNK_INFO)
		printf(" IMAGE_SCN_LNK_INFO");
	if ((header.characteristics & IMAGE_SCN_LNK_REMOVE) == IMAGE_SCN_LNK_REMOVE)
		printf(" IMAGE_SCN_CNT_CODE");
	if ((header.characteristics & IMAGE_SCN_LNK_COMDAT) == IMAGE_SCN_LNK_COMDAT)
		printf(" IMAGE_SCN_LNK_COMDAT");
	if ((header.characteristics & IMAGE_SCN_ALIGN_1BYTES) == IMAGE_SCN_ALIGN_1BYTES)
		printf(" IMAGE_SCN_ALIGN_1BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_2BYTES) == IMAGE_SCN_ALIGN_2BYTES)
		printf(" IMAGE_SCN_ALIGN_2BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_4BYTES) == IMAGE_SCN_ALIGN_4BYTES)
		printf(" IMAGE_SCN_ALIGN_4BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_8BYTES) == IMAGE_SCN_ALIGN_8BYTES)
		printf(" IMAGE_SCN_ALIGN_8BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_16BYTES) == IMAGE_SCN_ALIGN_16BYTES)
		printf(" IMAGE_SCN_ALIGN_16BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_32BYTES) == IMAGE_SCN_ALIGN_32BYTES)
		printf(" IMAGE_SCN_ALIGN_32BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_64BYTES) == IMAGE_SCN_ALIGN_64BYTES)
		printf(" IMAGE_SCN_ALIGN_64BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_128BYTES) == IMAGE_SCN_ALIGN_128BYTES)
		printf(" IMAGE_SCN_ALIGN_128BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_256BYTES) == IMAGE_SCN_ALIGN_256BYTES)
		printf(" IMAGE_SCN_ALIGN_256BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_1024BYTES) == IMAGE_SCN_ALIGN_1024BYTES)
		printf(" IMAGE_SCN_ALIGN_1024BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_2048BYTES) == IMAGE_SCN_ALIGN_2048BYTES)
		printf(" IMAGE_SCN_ALIGN_2048BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_4096BYTES) == IMAGE_SCN_ALIGN_4096BYTES)
		printf(" IMAGE_SCN_ALIGN_4096BYTES");
	if ((header.characteristics & IMAGE_SCN_ALIGN_8192BYTES) == IMAGE_SCN_ALIGN_8192BYTES)
		printf(" IMAGE_SCN_ALIGN_8192BYTES");
	if ((header.characteristics & IMAGE_SCN_LNK_NRELOC_OVFL) == IMAGE_SCN_LNK_NRELOC_OVFL)
		printf(" IMAGE_SCN_LNK_NRELOC_OVFL");
	if ((header.characteristics & IMAGE_SCN_MEM_DISCARDABLE) == IMAGE_SCN_MEM_DISCARDABLE)
		printf(" IMAGE_SCN_MEM_DISCARDABLE");
	if ((header.characteristics & IMAGE_SCN_MEM_NOT_CACHED) == IMAGE_SCN_MEM_NOT_CACHED)
		printf(" IMAGE_SCN_MEM_NOT_CACHED");
	if ((header.characteristics & IMAGE_SCN_MEM_NOT_PAGED) == IMAGE_SCN_MEM_NOT_PAGED)
		printf(" IMAGE_SCN_MEM_NOT_PAGED");
	if ((header.characteristics & IMAGE_SCN_MEM_SHARED) == IMAGE_SCN_MEM_SHARED)
		printf(" IMAGE_SCN_MEM_SHARED");
	if ((header.characteristics & IMAGE_SCN_MEM_EXECUTE) == IMAGE_SCN_MEM_EXECUTE)
		printf(" IMAGE_SCN_MEM_EXECUTE");
	if ((header.characteristics & IMAGE_SCN_MEM_READ) == IMAGE_SCN_MEM_READ)
		printf(" IMAGE_SCN_MEM_READ");
	if ((header.characteristics & IMAGE_SCN_MEM_WRITE) == IMAGE_SCN_MEM_WRITE)
		printf(" IMAGE_SCN_MEM_WRITE");

	if (header.numberOfRelocations > 0)
		CoffDumpRelocations(file, header.numberOfRelocations, header.pointerToRelocations);

	return header;
}

/* Dump all sections. */
BOOL dumpFileSections(IN FILE* file, IN int sectionCount, OUT CoffSectionHeader* stab,
	OUT CoffSectionHeader* stabstr) {

	CoffSectionHeader hdr;
	BOOL debug;

	debug = FALSE;
	while (sectionCount--) {
		hdr = dumpFileSection(file);
		if (!strcmp(hdr.name, ".stab")) {
			memcpy(stab, &hdr, sizeof(CoffSectionHeader));
			debug = TRUE;
		}
		if (!strcmp(hdr.name, ".stabstr")) {
			memcpy(stabstr, &hdr, sizeof(CoffSectionHeader));
			debug = TRUE;
		}
		printf("\n\r---------------------------\n\r");
	}
	return debug;
}

/* Dump COFF file header. */
CoffFileHeader dumpCoffFileHeader(IN FILE* file) {

	CoffFileHeader header;

	fread(&header, sizeof(CoffFileHeader), 1, file);
	printf("\n\rMachine: %x", header.machine);
	if (header.machine == 0) printf(" unknown");
	else if (header.machine == IMAGE_FILE_MACHINE_I386) printf(" i386");
	else if (header.machine == IMAGE_FILE_MACHINE_IA64) printf(" ia64");
	else printf("unsupported machine type.");
	printf("\n\rNumber of sections: %x", header.numberOfSections);
	printf("\n\rTime date stamp: %x", header.timeDateStamp);
	printf("\n\rPointer to symbol table: %x", header.pointerToSymbolTable);
	printf("\n\rNumber of symbols: %x", header.numberOfSymbols);
	printf("\n\rSize of optional header: %x", header.sizeOfOptionalHeader);
	printf("\n\rCharacteristics: %x", header.characteristics);
	if ((header.characteristics & IMAGE_FILE_RELOCS_STRIPPED) != 0)
		printf(" IMAGE_FILE_RELOCS_STRIPPED");
	if ((header.characteristics & IMAGE_FILE_EXECUTABLE_IMAGE) != 0)
		printf(" IMAGE_FILE_EXECUTABLE_IMAGE");
	if ((header.characteristics & IMAGE_FILE_LINE_NUMS_STRIPPED) != 0)
		printf(" IMAGE_FILE_LINE_NUMS_STRIPPED");
	if ((header.characteristics & IMAGE_FILE_LOCAL_SYMS_STRIPPED) != 0)
		printf(" IMAGE_FILE_LOCAL_SYMS_STRIPPED");
	if ((header.characteristics & IMAGE_FILE_AGGRESSIVE_WS_TRIM) != 0)
		printf(" IMAGE_FILE_AGGRESSIVE_WS_TRIM");
	if ((header.characteristics & IMAGE_FILE_LARGE_ADDRESS_AWARE) != 0)
		printf(" IMAGE_FILE_LARGE_ADDRESS_AWARE");
	if ((header.characteristics & IMAGE_FILE_16BIT_MACHINE) != 0)
		printf(" IMAGE_FILE_16BIT_MACHINE");
	if ((header.characteristics & IMAGE_FILE_BYTES_REVERSED_LO) != 0)
		printf(" IMAGE_FILE_BYTES_REVERSED_LO");
	if ((header.characteristics & IMAGE_FILE_32BIT_MACHINE) != 0)
		printf(" IMAGE_FILE_32BIT_MACHINE");
	if ((header.characteristics & IMAGE_FILE_DEBUG_STRIPPED) != 0)
		printf(" IMAGE_FILE_DEBUG_STRIPPED");
	if ((header.characteristics & IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP) != 0)
		printf(" IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP");
	if ((header.characteristics & IMAGE_FILE_SYSTEM) != 0)
		printf(" IMAGE_FILE_SYSTEM");
	if ((header.characteristics & IMAGE_FILE_DLL) != 0)
		printf(" IMAGE_FILE_DLL");
	if ((header.characteristics & IMAGE_FILE_UP_SYSTEM_ONLY) != 0)
		printf(" IMAGE_FILE_UP_SYSTEM_ONLY");
	if ((header.characteristics & IMAGE_FILE_BYTES_REVERSED_HI) != 0)
		printf(" IMAGE_FILE_BYTES_REVERSED_HI");

	return header;
}

/* Dump COFF imagge. */
void dumpCoffImage(IN FILE* file) {

	CoffFileHeader    coffFileHeader;
	CoffSectionHeader stab;
	CoffSectionHeader stabstr;
	BOOL              debug;

	if (!file)
		return;

	fseek(file, 0, SEEK_SET);
	coffFileHeader = dumpCoffFileHeader(file);
	/* comment for now since its a lot of info. */
	debug = dumpFileSections(file, coffFileHeader.numberOfSections, &stab, &stabstr);
	CoffLoadStringTable(file, coffFileHeader);
	dumpFileSymbolTable(file, coffFileHeader.pointerToSymbolTable, coffFileHeader.numberOfSymbols);
	if (debug) dumpStabs(file, &stab, &stabstr);

	printf("\n\r=== DONE ===\n\r");
	return;
}



#endif
