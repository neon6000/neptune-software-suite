/************************************************************************
*
*	nbuild.h - Neptune Build Utility.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <assert.h>

#define IN
#define OUT
#define OPTIONAL

typedef unsigned char uint8_t;
typedef signed char sint8_t;
typedef char int8_t;
typedef unsigned short uint16_t;
typedef signed short sint16_t;
typedef short int16_t;
typedef unsigned int uint32_t;
typedef signed int sint32_t;
typedef int int32_t;
typedef int bool_t;
typedef bool_t BOOL;
typedef uint32_t handle_t;
typedef uint32_t index_t;
typedef uint32_t status_t;
typedef unsigned long offset_t;
#define TRUE 1
#define FALSE 0

typedef struct _BUF {
	char* memory;
	size_t size;
}BUF;

typedef struct _FILEBUF {
	FILE*  file;
	char*  name;
	char*  buffer;
	size_t size;
	size_t line;
	char*  current;
	char*  end;
	time_t datetime;
	BOOL isMakefile;
}FILEBUF;

typedef struct TargetMachine {
	uint8_t sourcesMask;			// IMIDIR_I386
	char* description;				// "i386"
	char* commandLineSwitch;		// "i386"
	char* makeVariable;				// "386=1"
	char* sourceVariable;			// "i386_SOURCES"
	char* objectVariable;			// "386_OBJECTS"
	char* sourceDirectory;			// "i386"
	char* objectDirectory;          // "i386"
	char* objectMacro;              // don't initialize
}TargetMachine;

typedef struct _FileRecord;

typedef struct _IncludeRecord {
	unsigned long sig;
	struct _IncludeRecord* next;		// static list describes original arcs
	struct _IncludeRecord* nextTree;	// dynamic list - cycles are collapsed
	struct _FileRecord* cycleRoot;
	struct _FileRecord* include;
}IncludeRecord;

typedef struct _FileRecord {
	unsigned long sig;
	struct _FileRecord* next;
	struct _DirectoryRecord* dir;
	IncludeRecord* includeFiles;
	IncludeRecord* includeFilesTree;
	struct _FileRecord* newestDependency;
	char* commandToEOL;
	uint32_t dateTime;
	uint32_t dateTimeTree;
	uint32_t totalSourceLines;
	uint32_t fileFlags;
	uint32_t sourceLines;
	uint16_t attr;
	uint16_t subDirIndex;
	uint16_t version;
	uint16_t globalSequence;	// sequence number for dynamic include tree
	uint16_t localSequence;
	uint16_t idScan;
	uint16_t checksum;
	uint8_t dependActive;
	char name[1];
}FileRecord;

typedef struct _SourceRecord {
	unsigned long sig;
	struct _SourceRecord* next;
	FileRecord* source;
	uint8_t sourceSubDirMask;
	uint8_t srcFlags;
}SourceRecord;

/* util.c */
extern char* NbAppendString(IN char* destination, char* source, BOOL appendSpace);
extern char* NbFormatTime(IN unsigned long seconds);
extern char* NbFormatPath(IN char* dir, IN char* file);
extern time_t NbGetLastModifiedTime(IN OPTIONAL char* dir, IN char* file);
extern void NbPrintError(IN char* s, IN ...);
extern void NbCopyBuffer(IN BUF *src_buf,
	IN offset_t src_off, IN size_t src_len, IN BUF *dst_buf, IN offset_t dst_off, IN size_t dst_len);
extern BUF* NbWriteBuffer(IN const void *src_mem,
	IN size_t src_len, IN BUF* dst_buf, IN offset_t dst_off, IN size_t dst_len);
extern void NbReadBuffer(IN BUF* src_buf, IN offset_t src_off, IN size_t src_len, IN void *dst_mem);
extern void NbFreeBuffer(IN BUF* buf);

