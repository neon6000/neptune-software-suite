/************************************************************************
*
*	nbuild.c - Neptune Build Utility.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This component implements the parameter processing and entry point for nbuild. */

#include "nbuild.h"

#define MACHINE_X86 0

TargetMachine _targetMachineX86 = {
	MACHINE_X86, "x86", "x86", "X86=1", "X86_SOURCES", "X86_OBJECTS", "x86", "x86"
};

TargetMachine* _targetMachines[] = {
	&_targetMachineX86
};

char* _logFileName = "build.log";
char* _wrnFileName = "build.wrn";
char* _errFileName = "build.err";

char* _objDirectoryName = "obj";

/* Root directory. */
char _NeptuneRoot[256];

/* SDK Include Path. */
char _SdkIncludePath[256];

/* Make Utility. */
char* _MakeProgram;

/* System Include Path. */
char* _SystemIncludeDirectory;

BOOL NbAddTargetMachine(TargetMachine* machine){

	printf("\n\r** BbAddTarget: %s", machine->commandLineSwitch);
	return TRUE;
}


BOOL NbProcessParameters(IN int argc, char* argv[]) {

	char* arg;
	int i;

	for (i = 1; i < argc; i++) {

		arg = argv[i];
		if (arg[0] == '/' || arg[0] == '-') {

			printf("\n\rArgument: -%s", arg+1);

			if (strcmp(_targetMachineX86.commandLineSwitch, arg + 1) == 0)
				NbAddTargetMachine(&_targetMachineX86);

			switch (toupper(argv[1])){

			case '?':
				// show help.
				break;
			case 'C':
				// pass clean to make.
				break;
			case 'D':
				// enable debug.
				break;
			case 'O':
				// generate _objects.mac only.
				break;
			case 'L':
				// compile or link only.
				break;
			case 'N':
				// argument to nmake.
				break;
			};
		}
	}

	return FALSE;
}


int main(IN int argc, IN char* argv[]) {

	unsigned int i;
	char* s;
	char* s2;

	printf("\n\r\n\rNBUILD\n\r");

	test();
	return 0;

	s = getenv("_NROOT");
	s2 = getenv("_NDRIVE");

	NbPrintError("Test");
	NbPrintError("|Test2");
	NbPrintError(NULL);

	if (!s)
		s = "/neptune-software-suite";
	if (!s2)
		s2 = "";
	sprintf(_NeptuneRoot, "%s%s", s2, s);

	s = getenv("SDK_INC_PATH");
	if (!s)
		sprintf(_SdkIncludePath, "public/sdk/inc");
	else
		sprintf(_SdkIncludePath, s);

	_SystemIncludeDirectory = getenv("INCLUDE");

	_MakeProgram = getenv("MAKE");
	if (!_MakeProgram)
		_MakeProgram = "nmake.exe";

	if (!NbProcessParameters(argc, argv))
		;

	NbParseMakefile();

	return 0;
}
