/************************************************************************
*
*	src.c - Process makefile and dirs files.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <stdio.h>
#include <ctype.h>
#include "nbuild.h"

typedef struct _MACRO{
	BUF   value;
	char  name[1];
}MACRO;

/* given a pointer to a string, skip over
any whitespace on this line. */
char* skipws(IN char* s) {

	if (!s)
		return NULL;
	while (*s && (*s == ' ' || *s == '\t'))
		s++;
	return s;
}

/* given a pointer to a string, skip over
any non-whitespace characters. */
char* nexttok(IN char* s) {

	if (!s)
		return NULL;
	while (*s && isalnum(*s))
		s++;
	return s;
}

/* given a pointer to a string of format 'MACRO_NAME = VALUE",
return "VALUE" */
char* getvalue(IN char* macro) {

	char* f = macro;
	f = skipws(f);
	f = nexttok(f);
	f = skipws(f);
	if (!f)
		return NULL;
	if (*f != '=')
		return NULL;
	return skipws(f + 1);
}

void test() {

	char* f = "This is the path expansion: --$(PATH)-- ==$(PATH)== <<< all done. >>>";
	BUF* dst = NULL;
	char* start;
	char* end;
	offset_t offset;
	char* PATH;

	PATH = "";

	dst = NbWriteBuffer(f, strlen(f)+1, dst, 0, strlen(f)+1);

	printf("\n\rBEGIN WITH: '%s'\n\r", dst->memory);

	offset = 0;

	while ((start = strchr(dst->memory + offset, '$')) != NULL
		&& start[1] == '('
		&& (end = strchr(start, ')')) != NULL) {

		size_t length;

		printf("\n\n\r*** Found Macro ***\n\n\r");

		offset = start - dst->memory;
		length = end - start + 1;

		/* delete macro expression... */
		NbWriteBuffer(NULL, 0, dst, offset, length);

		/* ...and insert its expanded form. */
		NbWriteBuffer(PATH, strlen(PATH), dst, offset, 0);

		printf("\n\rExpanded: %s", dst->memory);

		offset += strlen(PATH);
	}

	printf("\n\rEND WITH: '%s'\n\r", dst->memory);

	return;


}

BOOL NbParseMakefile(void) {

	FILE* file = NULL;
	FILEBUF* buf;
	char* line;

	test();
	return FALSE;

	if (!NbOpenFile(NULL, "makefile", "rb", &file)) {
		printf("\n\r** Unable to open file");
		return FALSE;
	}

	buf = NbPrepareFileBuffer(file, "makefile");

	NbDumpFileBuffer(buf);

	printf("\n\n\r================\n\n\r");

	while (line = NbReadLine(buf))
		printf("\n\rLine: (%s)", line);

	printf("\n\n\r================\n\n\r");

	buf->current = buf->buffer;

	while (line = NbReadLine(buf))
		printf("\n\rLine: (%s)", line);

	printf("\n\n\r================\n\n\r");

	buf->current = buf->buffer;

	while (line = NbReadLine(buf))
		printf("\n\rLine: (%s)", line);

	return TRUE;
}

