/************************************************************************
*
*	util.c - Utility functions.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <stdarg.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include"nbuild.h"

#define NBUILD_MAX_PATH_LENGTH 128

char* _currentDirectory = "k:my/directory";

/* Given a string, display it as an error. */
void NbPrintError(IN char* s, IN ...) {

	char buf[128];
	va_list args;

	if (!s) {
		perror("Unknown error");
		return;
	}
	else{
		va_start(args, s);
		vsprintf(buf, s, args);
		va_end(args);
		perror(buf);
	}
}

/* append "source" to the end of "destination" */
char* NbAppendString(IN OUT char* destination, char* source, IN BOOL prependSpace) {

	if (!source)
		return NULL;
	if (!destination)
		return NULL;
	while (*destination)
		destination++;
	if (prependSpace)
		*destination++ = ' ';
	while (*source)
		*destination++ = *source++;
	return destination;
}

/* given a time in seconds, return a formatted string in hours:minutes:seconds. */
char* NbFormatTime(IN unsigned long seconds) {

	unsigned long hours;
	unsigned long minutes;
	static char buffer[16];

	hours = seconds / 3600;
	seconds %= 3600;
	minutes = seconds / 60;
	seconds %= 60;

	sprintf(buffer, "%2ld:%02ld:%02ld", hours, minutes, seconds);
	return buffer;
}

/* given a directory and filename, open it. */
BOOL NbOpenFile(IN char* dir, IN char* file, IN char* mode, OUT FILE** out) {

	char path[NBUILD_MAX_PATH_LENGTH];
	if (!dir)
		path[0] = '\0';
	else
		strcpy(path, dir);
	if (path[0] != '\0')
		strcat(path, '/');
	strcat(path, file);
	*out = fopen(path, mode);
	if (*out == NULL)
		return FALSE;
	return TRUE;
}

/* given a file object, close it. */
BOOL NbCloseFile(IN FILE* file) {

	if (!file)
		return FALSE;
	if (fclose(file) != 0)
		return FALSE;
	return TRUE;
}

void NbDumpFileBuffer(IN FILEBUF* buf) {

	if (!buf)
		return;
	printf("\n\n\r");
	if (buf->datetime) {
		char datetime[36];
		strftime(datetime, 36, "%d.%m.%Y %H:%M:%S", localtime(&buf->datetime));
		printf("%s (%s) %u bytes", buf->name, datetime, buf->size);
	}
	else
		printf("%s %u bytes", buf->name, buf->size);
	printf("\n\r\t%u", buf->line);
	printf("\n\r\t%s", buf->isMakefile ? "Is a makefile" : "Not a makefile");
	printf("\n\n\r");
}

FILEBUF* NbPrepareFileBuffer(IN FILE* file, IN char* name) {

	FILEBUF* buf;
	struct stat st;

	buf = (FILEBUF*)malloc(sizeof(FILEBUF));
	if (!buf)
		return NULL;

	buf->file = file;
	buf->name = name;
	buf->line = 0;
	buf->isMakefile = FALSE;
	if (stat(file, &st) == -1)
		buf->datetime = 0;
	else
		buf->datetime = st.st_mtime;

	if (strncmp(name, "makefile", strlen("makefile")) == 0)
		buf->isMakefile = TRUE;

	fseek(file, 0, SEEK_END);
	buf->size = ftell(file);
	rewind(file);

	buf->buffer = malloc(buf->size + 1);
	if (!buf->buffer){

		fclose(buf->file);
		free(buf);
		return NULL;
	}
	buf->current = buf->buffer;
	fread(buf->buffer, sizeof(uint8_t), buf->size, file);
	buf->end = &buf->buffer[buf->size];
	buf->buffer[buf->size + 1] = NULL;
	rewind(file);
	return buf;
}

/* given a file object, read the next line from the file. */
char* NbReadLine(IN FILEBUF* file) {

	char* lineStart;
	char* lineEnd = NULL;
	char* continueChar = NULL;

	lineStart = file->current;

	while (lineStart < file->end){

		switch (*lineStart) {

			case '\\':
				continueChar = lineStart;
				break;

			case ' ':
				break;

			case '\r':
			case '\t':
				*lineStart = ' ';
				break;

			case '\n':
			case '\0':

				if (!continueChar)
					goto eol;

				*continueChar = ' ';
				continueChar = NULL;

				*lineStart = ' ';
				break;

			default:
				continueChar = NULL;
				break;
		}
		lineStart++;
	}

eol:

	if (lineStart >= file->end)
		return NULL;

	/* last character position. */
	lineEnd = lineStart;
	lineStart = file->current;

	/* go to next line so next time this function gets called, it gets read. */
	file->current = lineEnd + 1;

	*lineEnd = '\0';
	return lineStart;
}


char _formatPathBuffer[NBUILD_MAX_PATH_LENGTH];

/* given a directory path and a file name relative to the directory,
return a new directory path to the file - relative to the current directory. */
char* NbFormatPath(IN char* dir, IN char* file) {



	return NULL;
}

/* get last modified datetime. */
time_t NbGetLastModifiedTime(IN OPTIONAL char* dir, IN char* file) {

	struct stat attr;
	char path[NBUILD_MAX_PATH_LENGTH];

	if (!file)
		return NULL;
	if (!dir || dir[0] == '/0')
		strcpy(path, file);
	else
		sprintf(path, "%s/%s", dir, file);

	if (stat(path, &attr) == -1)
		return 0;
	return attr.st_mtime;
}

/* test if file exists. */
BOOL NbTestFileExists(IN OPTIONAL char* dir, IN char* file) {

	struct stat attr;
	char path[NBUILD_MAX_PATH_LENGTH];

	if (!file)
		return NULL;
	if (!dir || dir[0] == '/0')
		strcpy(path, file);
	else
		sprintf(path, "%s/%s", dir, file);

	if (stat(path, &attr) == -1)
		return FALSE;
	return TRUE;
}

/* allocate a new buffer object of initial length "len". */
BUF* _NbBufferAlloc(IN uint8_t* src, IN size_t len) {

	BUF* dst = NULL;

	dst = malloc(sizeof (BUF));
	assert(dst != NULL);

	dst->size = len;
	dst->memory = realloc(NULL, len);
	assert(dst->memory != NULL);

	memset(dst->memory, len, 0);
	if (len != 0)
		memcpy(dst->memory, src, len);

	return dst;
}

/* free buffer object. */
void NbFreeBuffer(IN BUF* buf) {

	assert(buf != NULL);
	if (buf->memory)
		free(buf->memory);
	free(buf);
}

/* overwrite data starting at "dest_mem" of size "len" with data at "src_mem" */
void _NbBufferOverwrite(IN uint8_t* src_mem, IN uint8_t* dest_mem, IN size_t len) {

	memcpy(dest_mem, src_mem, len);
}

/* delete data from buffer object at "offset" of size "len." */
void _NbBufferDelete(IN BUF* dest, IN offset_t offset, IN size_t len) {

	offset_t start = dest->memory + offset;
	offset_t end = start + len;
	char* buffer_end = dest->size + dest->memory;

	memmove(start, end, buffer_end - end);
	dest->size -= len;
}

/* given "src" and "src_offset", insert "size" bytes from "src" into buffer object at "dst_offset". */
void _NbInsert(IN uint8_t* src, IN offset_t src_offset, IN BUF* dst, IN offset_t dst_offset, IN size_t size) {

	uint8_t* memory;
	uint8_t* from;
	uint8_t* to;
	size_t new_length;

	new_length = dst->size + size;

	memory = realloc(dst->memory, new_length);
	assert (memory != NULL);

	from = src + src_offset;
	to = memory + dst_offset;

	memmove(to + size, to, (memory + new_length) - (to + size));
	memcpy(to, from, size);

	dst->memory = memory;
	dst->size = new_length;
}

void NbCopyBuffer(IN BUF *src_buf, IN offset_t src_off, IN size_t src_len, IN BUF *dst_buf, IN offset_t dst_off, IN size_t dst_len) {

	uint8_t* buffer = NULL;

	assert(src_buf != NULL);
	assert(src_off >= 0 && src_off < src_buf->size);
	assert(src_len >= 0 && src_len <= (src_buf->size - src_off));
	assert(dst_buf != NULL);

	if (src_len == 0) {

		_NbBufferDelete(dst_buf, dst_off, dst_len);
	}
	else{

		buffer = realloc(NULL, src_buf->size);
		assert(buffer != NULL);
		NbReadBuffer(src_buf, src_off, src_buf->size, buffer);
		NbWriteBuffer(buffer, src_len, dst_buf, dst_off, dst_len);
		assert(dst_buf != NULL);
	}
}

#define MIN(a,b) ((a)<(b)) ? (a) : (b)

BUF* NbWriteBuffer(IN const void *src_mem, IN size_t src_len, IN BUF* dst_buf, IN offset_t dst_off, IN size_t dst_len) {

	size_t min;

	if (dst_buf == NULL)
		dst_buf = _NbBufferAlloc(src_mem, src_len);

	min = MIN(src_len, dst_len);

	if (min > 0)
		_NbBufferOverwrite(src_mem, dst_buf->memory, min);

	src_len -= min;
	dst_len -= min;

	if (src_len > 0)
		_NbInsert(src_mem, min, dst_buf, dst_off + min, src_len);
	else if (dst_len > 0)
		_NbBufferDelete(dst_buf, dst_off + min, dst_len);

	return dst_buf;
}

/* read "src_len" bytes from buffer object at 'src_off" into "dst_mem" */
void NbReadBuffer(IN BUF* src_buf, IN offset_t src_off, IN size_t src_len, IN void *dst_mem) {

	assert(src_buf != NULL);
	assert(dst_mem != NULL);
	assert(src_off >= 0 && src_off < src_buf->size);

	memcpy(dst_mem, src_buf->memory + src_off, src_len);
}
