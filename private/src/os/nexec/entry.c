/************************************************************************
*
*	entry.c - Neptune Executive Entry Point
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This component implements NEXEC.EXE entry point. */

#include <nexec.h>
#include <mm.h>
#include <image.h>

/* Note that the multiboot entry point is in mboot.c. entry.c only houses
the PE entry point if its executed directly. */

/*** PUBLIC Definitions **************************************/


/* Image file entry point. */
NORETURN PUBLIC void ExImageEntryPoint () {

	/* Not supported. */
	UNIMPLEMENTED;
	__halt();
}
