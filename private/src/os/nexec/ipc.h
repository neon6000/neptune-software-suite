/************************************************************************
*
*	ipc.h - Inter-Process Communication Services.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef _IPC_H
#define _IPC_H

#include <nexec.h>

/*** PUBLIC DECLARATIONS ************************************/

extern
PUBLIC
status_t
ExRaise(IN KPROCESS* in,
		int signal);

extern
PUBLIC
status_t
ExSend(IN int destination,
	   IN KMESSAGE* m);

extern
PUBLIC
status_t
ExReceive(IN int source,
		  OUT KMESSAGE* m);

extern
PUBLIC
status_t
ExSendReceive(IN int source,
			  IN KMESSAGE* in,
			  OUT KMESSAGE* out);

extern
PUBLIC
status_t
ExNotify(IN int source,
		 IN KMESSAGE* m);

extern
PUBLIC
status_t
ExEcho(IN int source,
	   IN KMESSAGE* m);

extern
PUBLIC
status_t
ExSystemRequest(IN KMESSAGE_TYPE function,
				IN int source,
				IN OUT OPTIONAL KMESSAGE* m);

#endif
