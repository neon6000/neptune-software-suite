/************************************************************************
*
*	mboot.c - Multiboot Support.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nexec.h>
#include <mboot.h>
#include <i386/hal.h>


/*** PRIVATE Definitions ****************************/

/* boot paramater information. */
MULTIBOOT_HEADER _kmbi;


/*** PUBLIC Definitions ******************************/

/**
*	Multiboot entry point. This is executed by NBOOT.
*/
NORETURN PUBLIC void ExMbEntryPoint () {

	MULTIBOOT_INFO* mbi;

	/* test for valid magic. */
	_asm {
		cmp eax, MULTIBOOT_BOOTLOADER_MAGIC
		je magic_ok
		jmp magic_bad
	}

	/* if the magic is bad, something is wrong. Either
	the mbi structure is corrupt, or the boot loader is wrong. */
	__asm magic_bad: jmp $
	__asm magic_ok:  mov mbi, ebx

	/* initialize serial debug port. */
	DbgSerialInitialize();

	/* copy mbi to global space since we are about to change stacks. */
	NrlCopyMemory(mbi,&_kmbi,sizeof(MULTIBOOT_INFO));

	/* scan memory map. */
	MmScanSystemMemoryMap(&_kmbi);

	/* build page frame database. */
	MmBuildPfnDatabase(&_kmbi);

	/* build kernel space. */
	MmBuildKernelSpace ();

	/* enable paging. */
	HalEnablePaging ();

	/* relocate nexec. */
	ExRelocateSelf ();

	/* jump to KERNEL_VIRTUAL. */
	_asm {
		lea eax, [flush_eip]
		call eax
flush_eip:
	}

	/* fix stack so its in kernel space. */
//	_asm {
//		mov esp, offset _kernel_stack+4096
//	;	mov esp, _kernel_stack+4096
//		mov ebp, esp
//	}

	/* call entry point. */
	ExMain (&_kmbi);
}

/* Multiboot information header. This must be defined
within the first 8k of the kernel image and aligned on
dword boundary. We achieve this by putting it into
its own section, and merging it at start of .text. */

#pragma code_seg(".a$0")
__declspec(allocate(".a$0"))
MULTIBOOT_HEADER _MultibootHeader = {
   MULTIBOOT_HEADER_MAGIC,
   MULTIBOOT_HEADER_FLAGS,
   MULTIBOOT_CHECKSUM,
   ((addr_t) &_MultibootHeader),
   KERNEL_PHYSICAL,
   0,
   0,
   (addr_t) ExMbEntryPoint,
   0,
   0,
   0,
   0
};
#pragma comment(linker, "/merge:.text=.a")
