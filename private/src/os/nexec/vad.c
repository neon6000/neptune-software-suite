/************************************************************************
*
*	vad.c - Virtual Address Space Management
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nexec.h>
#include <slab.h>

/*** PRIVATE Definitions ******************************/

/* returns MAX or MIN. */
#define MIN(a,b) ((a < b) ? a : b)
#define MAX(a,b) ((a > b) ? a : b)

/* KVAD cache. */
KCACHE* _cache = NULL;

/* KVAD reference type. */
typedef KVAD** KVAD_REF;

/* comparison function. */
PRIVATE
BOOL
VadCompare(IN addr_t start,
		   IN KVAD* right) {

	return (start < right->start);
}

/* creates new node. */
PRIVATE
KVAD*
VadNewNode(IN addr_t start,
		   IN size_t size) {

	KVAD* vad = ExAllocateCache(_cache, 0);
	if (!vad)
		return NULL;
			   
	vad->left = NULL;
	vad->right = NULL;
	vad->start = start;
	vad->size  = size;
	return vad;
}

/* releases node memory. */
PRIVATE
BOOL
VadFreeNode(IN KVAD* node) {

	if (!node)
		return FALSE;

	ExFreeCache(_cache, node);
	return TRUE;
}

/* initializes KVAD cache. */
PRIVATE
void
VadInitCache(void) {

	_cache = ExCreateCache("KVAD",sizeof(KVAD),0x1000,NULL);
}

/* gets height of node. */
PRIVATE
int
VadGetHeight(IN KVAD* node) {

	int leftHeight;
	int rightHeight;

	if (!node)
		return 0;

	leftHeight  = VadGetHeight(node->left);
	rightHeight = VadGetHeight(node->right);
	return MAX(leftHeight,rightHeight) + 1;
}

/* returns smallest valued node in tree. */
PRIVATE
KVAD*
VadGetMin(IN KVAD* node) {

	if (!node)
		return NULL;

	while (node->left)
		node = node->left;
	return node;
}

/* delete min node given root. Returns
new root. */
PRIVATE
void
VadDeleteMin(IN KVAD_REF node) {

	if (!(*node)->left)
		*node = (*node)->right;
	else
		VadDeleteMin(&(*node)->left);
}

/* get largest valued node in tree. */
PRIVATE
KVAD*
VadGetMax(IN KVAD* node) {

	if (!node)
		return NULL;

	while (node->right)
		node = node->right;
	return node;
}

/* do a left rotation. Return new root. */
PRIVATE
KVAD*
VadRotateLeft(IN KVAD* node) {
/*
            node                        p
			 V                          V
           [node]                     [left]
     [left]      [right]  -->  [right]      [node]
*/
	KVAD* p = node->left;
	node->left = p->right;
	p->right = node;
	return p;
}

/* do a right rotation. Return new root. */
PRIVATE
KVAD*
VadRotateRight(IN KVAD* node) {
/*
            node                       p
			 V                         V
           [node]                    [right]
     [left]      [right]  -->  [node]       [left]
*/
	KVAD* p = node->right;
	node->right = p->left;
	p->left = node;
	return p;
}

/* performs right followed by left rotate. */
PRIVATE
KVAD*
VadRotateRightLeft(IN KVAD* node) {

	node->left = VadRotateRight(node->left);
	return VadRotateLeft(node);
}

/* performs left followed by right rotate. */
PRIVATE
KVAD*
VadRotateLeftRight(IN KVAD* node) {

	node->right = VadRotateLeft(node->right);
	return VadRotateRight(node);
}

/* given a root node and value, find node in tree. */
PRIVATE
KVAD*
VadFind(IN KVAD* node,
		IN addr_t start) {

	if (! node)
		return FALSE;
	else if (start < node->start)
		return VadFind( node->left, start);
	else if (start > node->start)
		return VadFind( node->right, start);
	else
		return node;
}

/* delete node from tree. */
PRIVATE
BOOL
VadDelete(IN OUT KVAD_REF root,
		  IN addr_t start) {

	BOOL  result;
	KVAD* node;

	if (! (*root))
		return FALSE;
	else if (start < (*root)->start)
		result = VadDelete( &(*root)->left, start);
	else if (start > (*root)->start)
		result = VadDelete( &(*root)->right, start);
	else {

		/* we found the node to delete. */

		/* case 1 : this node has no children. */
		if (!((*root)->left) && !((*root)->right)) {

			VadFreeNode(*root);
			*root = NULL;
		}

		/* case 2 : this node has only a right child. */
		else if (!(*root)->left) {

			node = *root;
			VadFreeNode(node);
			*root = (*root)->right;
		}

		/* case 3 : this node has only a left child. */
		else if (!(*root)->right) {

			node = *root;
			*root = (*root)->left;
			VadFreeNode(node);
		}

		/* case 4 : this node has a left and right child. */
		else {

			/* get minimum value from right subtree. */

			node = VadGetMin((*root)->right);

			/* the minimum value becomes the new root.
			Note that we don't actually have to create
			a new node here. Just reuse it. */

			(*root)->start = node->start;
			(*root)->size  = node->size;

			/* delete node. */
			VadDeleteMin(&(*root)->right);
		}

		printf("\n\rDelete : %08X", start);
		return TRUE;
	}
	return FALSE;
}

/* insert node into tree. Note : We use KVAD_REF
here so we can update the root of the tree dynamically. */
PRIVATE
KVAD*
VadInsert(IN OUT KVAD_REF root,
		  IN addr_t start,
		  IN size_t size) {

	if (! (*root)) {
		*root = VadNewNode(start, size);
		return *root;
	}

	if (VadCompare (start, *root)) {

		(*root)->left = VadInsert(&(*root)->left, start, size);

		if(VadGetHeight((*root)->left) - VadGetHeight((*root)->right) == 2) {

			if (start < (*root)->left->start)
				*root = VadRotateLeft(*root);
			else
				*root = VadRotateRightLeft(*root);
		}
	}
	else {

		(*root)->right = VadInsert(&(*root)->right, start, size);

		if(VadGetHeight((*root)->right) - VadGetHeight((*root)->left) == 2) {

			if (start > (*root)->right->start)
				*root = VadRotateRight(*root);
			else
				*root = VadRotateLeftRight(*root);
		}
	}

	return *root;
}

PRIVATE
void
_VadPrint(IN KVAD* root,
		  IN KVAD *ptr,
		  IN int level) {

	int i;
    if (ptr!=NULL)
    {
        _VadPrint(root, ptr->right, level + 1);
        printf("\n");
        for (i = 0; i < level && ptr != root; i++)
            printf("        ");
		printf("(%08X - %08X)", ptr->start, ptr->start + ptr->size);
        _VadPrint(root, ptr->left, level + 1);
    }
}

PRIVATE
void 
VadPrint(IN KVAD* n) {
	printf("\n\r--------------------------");
	_VadPrint(n, n, 1);
	printf("\n\r--------------------------");
}

/*** PUBLIC Definitions ******************************/

/* reserve virtual addresses in a process. */
PUBLIC
status_t
ExAllocVirtual (IN KPROCESS* process,
				IN addr_t start,
				IN size_t size,
				IN MM_FLAGS flags,
				IN MM_PROTECT protect) {

	return 0;
}

/* free virtual addresses from a process. */
PUBLIC
status_t
ExFreeVirtual(IN KPROCESS* process,
			  IN addr_t start) {

	return 0;
}

/* run PUBLIC tests. */
PUBLIC
void
_ExRunTests () {

	KVAD* _root = NULL;

	VadInitCache();

	VadInsert(&_root, 150, 10);
	VadInsert(&_root, 350, 10);
	VadInsert(&_root, 250, 10);
	VadInsert(&_root, 450, 10);
	VadInsert(&_root, 550, 10);
	VadInsert(&_root, 5, 10);
	VadPrint(_root);
	VadDelete(&_root, 450);
	VadPrint(_root);
	VadDelete(&_root, 550);
	VadPrint(_root);
	VadDelete(&_root, 5);
	VadPrint(_root);
	{
		KVAD* f = VadFind(_root, 250);
		if(f)
			printf("\n\r*** found : %08X", f->start);
	}
	VadDelete(&_root, 250);
	VadPrint(_root);
	VadDelete(&_root, 350);
	VadPrint(_root);
	VadDelete(&_root, 150);
	VadPrint(_root);
	return;
}
