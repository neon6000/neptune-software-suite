/************************************************************************
*
*	image.c - Portable Executable (PE) Support.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "image.h"


/*** PUBLIC Definitions *************************************/

/**
*	Returns the data directory specified by index.
*/
PUBLIC IMAGE_DATA_DIRECTORY* PeGetDataDirectory (IN char* imageBase, IN int idx) {

	IMAGE_DOS_HEADER*     p;
	IMAGE_NT_HEADERS*     _ntHeaders;

	p = (IMAGE_DOS_HEADER*)imageBase;
	_ntHeaders = (IMAGE_NT_HEADERS*) (p->lfanew + imageBase);
	return &_ntHeaders->optionalHeader.dataDirectory [idx];
}

/**
*	Returns size of image file.
*/
PUBLIC size_t PeGetImageSize(IN char* imageBase) {

	IMAGE_DOS_HEADER*     p;
	IMAGE_NT_HEADERS*     ntHeaders;

	p = (IMAGE_DOS_HEADER*)imageBase;
	ntHeaders = (IMAGE_NT_HEADERS*) (p->lfanew + imageBase);

	return (size_t) ntHeaders->optionalHeader.sizeOfImage;
}

/**
*	Returns entry point address.
*/
PUBLIC VIRTUAL_ADDRESS PeGetEntryPoint (IN char* imageBase) {

	IMAGE_DOS_HEADER*     p;
	IMAGE_NT_HEADERS*     ntHeaders;

	p = (IMAGE_DOS_HEADER*)imageBase;
	ntHeaders = (IMAGE_NT_HEADERS*) (p->lfanew + imageBase);

	return (VIRTUAL_ADDRESS) ntHeaders->optionalHeader.addressOfEntryPoint;
}

/**
*	Returns address of function specified.
*/
PUBLIC VIRTUAL_ADDRESS PeGetProcAddress(IN char* imageBase, IN char* function) {

	IMAGE_DATA_DIRECTORY*   directory;
	IMAGE_EXPORT_DIRECTORY* exportDirectory;
	uint32_t*               functionNameAddressArray;
	uint16_t*               functionOrdinalAddressArray;
	uint32_t*               functionAddressArray;
	index_t                 current;

	directory = PeGetDataDirectory (imageBase, IMAGE_DIRECTORY_ENTRY_EXPORT);
	exportDirectory = (IMAGE_EXPORT_DIRECTORY*) (directory->virtualAddress + imageBase);

	functionNameAddressArray    = ((uint32_t)exportDirectory->addressOfNames)      + ((uint8_t*)imageBase);
	functionOrdinalAddressArray = (uint32_t) exportDirectory->addressOfNameOrdinal + (uint8_t*) imageBase;
	functionAddressArray        = (uint32_t) exportDirectory->addressOfFunctions   + (uint8_t)  imageBase;

	for ( current = 0; current < exportDirectory->numberOfFunctions; current++ )
	{
		char* functionName = functionNameAddressArray [current] + (uint8_t*)imageBase;

		if (NrlStrCompare (functionName, function) == 0) {

			uint16_t ordinal = functionOrdinalAddressArray [current];
			uint32_t functionAddress = functionAddressArray [ordinal];
			return (VIRTUAL_ADDRESS) (functionAddress + (uint8_t*)imageBase);
		}
	}

	return NULL;
}

/*
	Relocates image.
*/
PUBLIC void PeRelocate (IN IMAGE_DATA_DIRECTORY* reloc, IN char* imageBase, IN char* newBase) {

	IMAGE_BASE_RELOCATION*  baseReloc;
	IMAGE_RELOCATION_ENTRY* entry;
	uint32_t* memory;
	uint32_t  imageBaseDelta;

	/* calculate delta. */
	imageBaseDelta = newBase - imageBase;

	/* go through all base relocation blocks. */
	baseReloc = (IMAGE_BASE_RELOCATION*) (reloc->virtualAddress + (addr_t)imageBase);
	while ((uint32_t) baseReloc < reloc->virtualAddress + reloc->size + (addr_t)imageBase) {

		/* go through all relocations in this block. */
		entry = (IMAGE_RELOCATION_ENTRY*) (baseReloc+1);
		while ((char*)entry < (char*)baseReloc + baseReloc->sizeOfBlock) {

			/* write to memory the difference between 3gb - image base. */
			memory = (uint32_t*) (entry->offset + baseReloc->virtualAddress + imageBase);
			if (entry->type == IMAGE_REL_BASED_HIGHLOW)
				*memory += imageBaseDelta;

			/* go to next relocation. */
			entry++;
		}

		/* go to next block. */
		((char*) baseReloc) += baseReloc->sizeOfBlock;
	}
}

/**
*	Relocates NEXEC.EXE
*/
INIT_SECTION PUBLIC void ExRelocateSelf(void) {

	PeRelocate(
		PeGetDataDirectory((char*) KERNEL_PHYSICAL,IMAGE_DIRECTORY_ENTRY_BASERELOC),
		(char*) KERNEL_PHYSICAL, (char*) KERNEL_VIRTUAL);
}
