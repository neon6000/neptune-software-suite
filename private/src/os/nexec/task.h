/************************************************************************
*
*	task.h - Executive Task Services
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* Implementing Multi-Level Feedback Queue offline. */

#ifndef _TASK_H
#define _TASK_H

#include <types.h>
#include <nrtl.h>
#include <nexec.h>

/*** PUBLIC DECLARATIONS ************************************/

extern
PUBLIC
status_t
ExSetCurrentTask(IN int tid);

extern
PUBLIC
status_t
ExTaskSwitch (void);

extern
PUBLIC
status_t
ExTaskBlock(IN int reason);

extern
PUBLIC
status_t
ExTaskUnblock(IN int reason);

#endif
