/************************************************************************
*
*	mmpool.c - Memory Manager - System Pool
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This component implements the kernel mode system pool. The system pool
is used by kernel mode components for allocating system memory. */

#include <nexec.h>
#include <mm.h>

/*
    The System Pool is composed of a free list of system mapped
    page table entries (PTE) clusters that can be reserved or
    released back to the pool. A PTE cluster is composed of 1
    or more PTE's in System Space.

    The entries have the following format:

    Cluster of one PTE:

                  oneEntry   valid
    +------------+--------+------+
    | nextEntry  |   1    |   0  |
    +------------+--------+------+

    Cluster of "PTE Count" PTE's:

                  oneEntry   valid             oneEntry   valid
    +------------+--------+------+------------+--------+------+
    | nextEntry  |   0    |   0  | PTE Count  |   0    |   0  |
    +------------+--------+------+------------+--------+------+
*/

/* System PTE Pool starts here. */
#define MMPTE_POOL_START 0xd0000000

/* This marks the end of the pool. */
#define MMPTE_POOL_END 0xfffff

/* The system pool starts here. */
MMPTE* _MmSystemPoolBase;

/* The system pool ends here. */
MMPTE* _MmSystemPoolEnd;

/* The first free PTE in the system pool. */
MMPTE  _MmFirstFreeSystemPte;

/* Total amount of free PTE's in the pool. */
size_t _MmTotalFreeSystemPtes;

/* Total amount of PTE's in the pool. */
size_t _MmTotalSystemPtes;

/*
	Dump the system PTE pool.
*/
PUBLIC void MmDumpSystemPtes(void) {

	MMPTE*   previousPte;
	MMPTE*   nextPte;
	uint32_t clusterSize;

	DbgPrintf("\n");
	DbgPrintf("\nSystem PTE Pool");
	DbgPrintf("\n=================");

	previousPte = &_MmFirstFreeSystemPte;
	while (previousPte->u.list.nextEntry != MMPTE_POOL_END) {

		/* next pte is here. */
		nextPte = _MmSystemPoolBase + previousPte->u.list.nextEntry;

		/* get number of pte's in this cluster. */
		clusterSize = MmGetClusterSize (nextPte);

		DbgPrintf("\nPTE %x Cluster (%i pte's) --> %x", previousPte, clusterSize,
			nextPte);

		/* go to next pte. */
		previousPte = nextPte;
	}
	DbgPrintf("\n---End---\n");
}

/*
	Given a starting pte and the number of pte's,
	attempt to release them back to the pool.
*/
PUBLIC void MmReleaseSystemPtes(IN MMPTE* startPte, IN size_t numberOfPtes) {

	MMPTE*   previousPte;
	MMPTE*   nextPte;
	MMPTE*   insertPte;
	uint32_t clusterSize;

	/* clear them. */
	NrlZeroMemory(startPte, numberOfPtes * sizeof(MMPTE));

	/* need to insert this cluster back into the free list. */
	previousPte = &_MmFirstFreeSystemPte;
	insertPte = NULL;
	while (previousPte->u.list.nextEntry != MMPTE_POOL_END) {

		/* next pte is here. */
		nextPte = _MmSystemPoolBase + previousPte->u.list.nextEntry;

		/* get number of pte's in this cluster. */
		clusterSize = MmGetClusterSize (nextPte);

		if ((nextPte + clusterSize == startPte) || (startPte + numberOfPtes == nextPte)) {

			/* this cluster is adjacent to us. */

			numberOfPtes += clusterSize;
			if (nextPte < startPte)
				startPte = nextPte;

			previousPte->u.list.nextEntry = nextPte->u.list.nextEntry;
			if (nextPte->u.list.oneEntry == 0) {
				nextPte->u.data = 0;
				nextPte++;
			}
			nextPte->u.data = 0;
			insertPte = NULL;
		}
		else{

			if ((!insertPte) && (numberOfPtes <= clusterSize))
				insertPte = previousPte;

			previousPte = nextPte;
		}
	}

	/* No location was found, so insert at end. */
	if (!insertPte)
		insertPte = previousPte;

	/* Create a new cluster. */
	if (numberOfPtes == 1){
		startPte->u.list.oneEntry = 1;
	}else{
		startPte->u.list.oneEntry = 0;
		nextPte = startPte+1;
		nextPte->u.list.nextEntry = numberOfPtes;
	}

	/* add it to cluster list. */
	startPte->u.list.nextEntry = insertPte->u.list.nextEntry;
	insertPte->u.list.nextEntry = startPte - _MmSystemPoolBase;

	_MmTotalFreeSystemPtes += numberOfPtes;
}

/*
	Given the number of PTE's to reserve, try to find
	a free cluster that satisfies the request and return
	a pointer to the start of the cluster.
*/
PUBLIC MMPTE* MmReserveSystemPtes(IN uint32_t numberOfPtes) {

	MMPTE*   previousPte;
	MMPTE*   nextPte;
	MMPTE*   result;
	uint32_t clusterSize;

	/* make sure there are enough pte's. */
	if (_MmTotalFreeSystemPtes < numberOfPtes)
		return NULL;

	/* get the first free cluster. */
	previousPte = &_MmFirstFreeSystemPte;

	/* find a cluster that matches the requested size. */
	while (previousPte->u.list.nextEntry != MMPTE_POOL_END) {

		/* next pte is here. */
		nextPte = _MmSystemPoolBase + previousPte->u.list.nextEntry;

		/* get number of pte's in this cluster. */
		clusterSize = MmGetClusterSize (nextPte);

		/* if this satisfies the request, we are done. */
		if (numberOfPtes < clusterSize)
			break;

		/* go to next cluster. */
		previousPte = nextPte;
	}

	/* Watch for end of list. */
	if (previousPte->u.list.nextEntry == MMPTE_POOL_END){
		return NULL;
	}

	/* Remove the cluster from the free list. */
	previousPte->u.list.nextEntry = nextPte->u.list.nextEntry;

	/* Is this a perfect match? */
	if (clusterSize == numberOfPtes) {

		result = nextPte;
		if (nextPte->u.list.oneEntry == 0) {
			NrlZeroMemory(nextPte,sizeof(MMPTE));
			nextPte++;
		}
		NrlZeroMemory(nextPte,sizeof(MMPTE));
	}

	/* We need to split this cluster into two. */
	else {

		/* Divide this cluster. */
		clusterSize -= numberOfPtes;
		result = nextPte + clusterSize; /* keep the higher half. */

		/* Initialize new cluster. */
		if (clusterSize == 1) {

			/* This new cluster only has 1 PTE. */
			nextPte->u.list.oneEntry = 1;
			result->u.data = 0;

		}else{

			/* This new cluster has multiple PTE's, so
			set its size. */
			nextPte++;
			nextPte->u.list.nextEntry = clusterSize;
		}

		/* insert this new cluster. This is an ordered list by size. */
		previousPte = &_MmFirstFreeSystemPte;
		while (previousPte->u.list.nextEntry != MMPTE_POOL_END) {

			/* next pte is here. */
			nextPte = _MmSystemPoolBase + previousPte->u.list.nextEntry;
			if (clusterSize <= MmGetClusterSize(nextPte))
				break;
			previousPte = nextPte;
		}

		/* add it to the free list. */
		nextPte = result - clusterSize;
		nextPte->u.list.nextEntry = previousPte->u.list.nextEntry;
		previousPte->u.list.nextEntry = nextPte - _MmSystemPoolBase;
	}

	_MmTotalFreeSystemPtes--;

	DbgPrintf("\nMmReserveSystemPtes returning %x (maps to %x)", result,MmPteToAddress(result));
	return result;
}

/*
	Given the number of PTEs in the system pool,
	build the free PTE list.
*/
PUBLIC void MmBuildSystemPtePool(IN uint32_t numberOfPtes) {

	MMPTE* startingPte;
	MMPDE* kernelDirectory;

	/* We only reserve 1 page table right now. */
	ASSERT(numberOfPtes <= 1024);

	/* Initialize globals. */
	_MmSystemPoolBase      = MM_GET_PAGE_TABLE (MMPTE_POOL_START);
	_MmSystemPoolEnd       = _MmSystemPoolBase + numberOfPtes - 1;
	_MmTotalSystemPtes     = numberOfPtes;
	_MmTotalFreeSystemPtes = numberOfPtes;

	startingPte = _MmSystemPoolBase;

	MmPrepareAddressMapping(0xd0000000,0);

	/* Clear it. */
	NrlZeroMemory(startingPte, numberOfPtes * sizeof(MMPTE));

	/* This is a free cluster of numberOfPtes size. */
	startingPte->u.list.nextEntry = MMPTE_POOL_END;
	startingPte->u.list.oneEntry = 0;

	_MmFirstFreeSystemPte.u.data = 0;
	_MmFirstFreeSystemPte.u.list.nextEntry = startingPte - _MmSystemPoolBase;

	startingPte++;
	startingPte->u.data = 0;
	startingPte->u.list.nextEntry = numberOfPtes;

	DbgPrintf("\nSystem PTE Pool at %x (%i PTE's)", startingPte-1,
		MmGetClusterSize(startingPte-1));
}

/*
	Given a pointer to a System PTE, return the
	number of PTE's in the cluster.
*/
INLINE uint32_t MmGetClusterSize(IN MMPTE* pte) {

	/* If there is only one entry in this cluster, return 1. */
	if (pte->u.list.oneEntry)
		return 1;

	/* If there is more then one entry in this cluster,
	the number of entries in the cluster is in the next PTE. */
	pte++;
	return (uint32_t) pte->u.list.nextEntry;
}
