/************************************************************************
*
*	nrtl.h - Neptune RunTime Library.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This implements common runtime services for NEXEC. */

#include <types.h>

#ifndef _NRTL_H
#define _NRTL_H


/*** PUBLIC DEFINITIONS ****************************************/

/* returns offset of record in structure. */
#define NRL_OFFSETOF(t,f) ((unsigned long)(addr_t)&(((t*) 0)->f))

/* returns containing record in parent structure. */
#define NRL_CONTAINING_RECORD(address,type,field)  ((type *)(((addr_t)address) - (addr_t)(&(((type *)0)->field))))

/* bit map. */
typedef struct _RTLBITMAP {
	addr_t       base;
	size_t       size;
}RTLBITMAP;

/* double linked list. */
typedef struct _LIST_ENTRY {
	struct _LIST_ENTRY* next;
	struct _LIST_ENTRY* prev;
}LIST_ENTRY;

PRIVATE INLINE void NrlInitializeBitmap(IN OUT RTLBITMAP* in) {

	in->base = in->size = 0;
}

PRIVATE INLINE void NrlSetBit(IN RTLBITMAP* in, IN int index) {

	uint32_t* base = (uint32_t*) in->base;
	base [index / 32] |= (1 << (index % 32));
}

PRIVATE INLINE void NrlClearBit(IN RTLBITMAP* in, IN int index) {

	uint32_t* base = (uint32_t*) in->base;
	base [index / 32] &= ~(1 << (index % 32));
}

PRIVATE INLINE BOOL NrlBitTest(IN RTLBITMAP* in, IN int index) {

	uint32_t* base = (uint32_t*) in->base;
	if (base [index / 32] & (1 << (index % 32)))
		return TRUE;
   return FALSE;
}

PRIVATE INLINE void NrlInitializeListHead(IN LIST_ENTRY* head) {

	head->next = head->prev = head;
}

PRIVATE INLINE void NrlInsertHeadList(IN LIST_ENTRY* head, IN LIST_ENTRY* e) {

	LIST_ENTRY* oldNext = head->next;
	e->next = oldNext;
	e->prev = head;
	oldNext->prev = e;
	head->next = e;
}

PRIVATE INLINE void NrlInsertTailList(IN LIST_ENTRY* head, IN LIST_ENTRY* e) {

	LIST_ENTRY* oldPrev = head->prev;
	e->next = head;
	e->prev = oldPrev;
	head->prev = e;
	oldPrev->next = e;
}

PRIVATE INLINE bool_t NrlIsListEmpty (IN LIST_ENTRY* head) {

	return (head->next == head);
}

PRIVATE INLINE bool_t NrlRemoveEntryList(IN LIST_ENTRY* e) {

	LIST_ENTRY* oldPrev;
	LIST_ENTRY* oldNext;

	oldNext = e->next;
	oldPrev = e->prev;
	oldNext->prev = oldPrev;
	oldPrev->next = oldNext;
	return (oldNext == oldPrev);
}

PRIVATE INLINE LIST_ENTRY* NrlRemoveHeadList(IN LIST_ENTRY* head) {

	LIST_ENTRY* next;
	LIST_ENTRY* e;

	e = head->next;
	next = e->next;
	head->next = next;
	next->prev = head;
	return e;
}

PRIVATE INLINE LIST_ENTRY* NrlRemoveTailList(IN LIST_ENTRY* head) {

	LIST_ENTRY* prev;
	LIST_ENTRY* e;

	e = head->prev;
	prev = e->prev;
	head->prev = prev;
	prev->next = head;
	return e;
}

PRIVATE void NrlZeroMemory(IN void* dest, IN size_t s) {

	char* dp = (char*) dest;
	for (; s; s--, dp++) *dp = 0;
}

PRIVATE void* NrlCopyMemory(IN void* s1, IN void* s2, IN size_t n) {

	const char *sp = (const char *)s1;
    char *dp = (char *)s2;
    for(; n != 0; n--) *dp++ = *sp++;
    return (void*) s2;
}

PRIVATE unsigned long NlrCompareMemory (IN void* s, IN void* d, size_t length) {

	unsigned long i;
	for(i=0; i<length; i++) {
		if( ((char*)s)[i] != ((char*)d)[i] )
			break; 
	}
	return i;
}

PRIVATE int NrlStrCompare (IN char* s1, IN char* s2) {

	while (*s1 == *s2++) {
		if (*s1++ == 0)
			return (0);
	}
	return (*(const unsigned char *)s1 - *(const unsigned char *)(s2 - 1));
}


PUBLIC INLINE size_t NrlStringLength (IN char* s) {

	size_t len = 0;
	while (*s++)
		len++;
	return len;
}

extern PUBLIC long NrlInterlockedIncrement(IN OUT long volatile* addend);

extern PUBLIC long NrlInterlockedDecrement(IN OUT long volatile* addend);

extern PUBLIC long NrlInterlockedExchange(IN OUT long volatile* target, IN long value);

extern PUBLIC long NrlInterlockedExchangeAdd(IN OUT long volatile* addend, IN long value);

extern PUBLIC long NrlInterlockedCompareExchange(IN OUT long volatile* destination,
												 IN long exchange, IN long compareand);

extern PUBLIC int NrlWriteVaListToString(OUT char* buffer, IN size_t size,
										 IN char* format, va_list args);

extern PUBLIC void NrlSPrintf(IN char* s, IN size_t length, IN char* format, OUT ...);

#endif
