/************************************************************************
*
*	sched.c - Scheduler facilities
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nexec.h>

/* prority levels. */
#define PRIORITY_MAX 4

/* ready queues. */
LIST_ENTRY _ExReadyQueue [PRIORITY_MAX];

/* initializes scheduler. */
void
ExScheduleInitialize(void) {

	int i;
	for (i=0; i<PRIORITY_MAX; i++)
		NrlInitializeListHead(&_ExReadyQueue[i]);
}

/* inserts into ready queue at a queue priority level. */
void
ExScheduleInsertReady(IN LIST_ENTRY* current,
					  IN int priority) {

	ASSERT (priority < PRIORITY_MAX);
	NrlInsertTailList(&_ExReadyQueue[priority], current);
}

/* schedule in a round robin fashion. */
LIST_ENTRY*
ExScheduleRR(IN LIST_ENTRY* current) {

	return current->next;
}

/* first come first serve. */
LIST_ENTRY*
ExScheduleFCFS(IN LIST_ENTRY* current) {

	return current;
}

/* shortest job first. */
LIST_ENTRY*
ExScheduleSJF(IN LIST_ENTRY* current) {

	/* this one is an interesting case. */
	return NULL;
}

/* schedule highest priority. */
LIST_ENTRY*
ExSchedulePri(IN LIST_ENTRY* current) {

	KTHREAD* next;
	LIST_ENTRY* end;

	end  = current;
	next = NRL_CONTAINING_RECORD(current,KTHREAD,readyQueueEntry);

	for (current = current->next;
		current != end;
		current = current->next) {

		KTHREAD* task = NRL_CONTAINING_RECORD(current,KTHREAD,readyQueueEntry);

		if (task->priority > next->priority)
			next = task;
	}

	return &next->readyQueueEntry;
}

/* raise thread's queue priority level. */
void
ExSceduleRaiseLevel(IN KTHREAD* current) {

	/* if it is already highest priority, nothing to do. */
	if (current->readyQueuePriorityLevel == 0)
		return;

	/* add it to higher priority queue. */
	current->readyQueuePriorityLevel--;
	NrlRemoveEntryList(&current->readyQueueEntry);
	ExScheduleInsertReady(&current->readyQueueEntry, current->readyQueuePriorityLevel);
}

/* lower thread's queue priority level. */
void
ExSceduleLowerLevel(IN KTHREAD* current) {

	/* if it is already lowest priority, nothing to do. */
	if (current->readyQueuePriorityLevel == PRIORITY_MAX - 1)
		return;

	/* add it to lower priority queue. */
	current->readyQueuePriorityLevel++;
	NrlRemoveEntryList(&current->readyQueueEntry);
	ExScheduleInsertReady(&current->readyQueueEntry, current->readyQueuePriorityLevel);
}

/* schedule next task to run. */
LIST_ENTRY*
ExSchedule(void) {

	/* priority 0. */
	if (! NrlIsListEmpty(&_ExReadyQueue[0]))
		return ExScheduleFCFS(&_ExReadyQueue[0]);

	/* priority 1. */
	else if (! NrlIsListEmpty(&_ExReadyQueue[1]))
		return ExSchedulePri(&_ExReadyQueue[1]);

	/* priority 2. */
	else if (! NrlIsListEmpty(&_ExReadyQueue[2]))
		return ExScheduleRR(&_ExReadyQueue[2]);

	/* priority 3. Want to use SFJ here, for now use RR. */
	else if (! NrlIsListEmpty(&_ExReadyQueue[3]))
		return ExScheduleRR(&_ExReadyQueue[3]);

	/* nothing is ready. */
	else return NULL;
}
