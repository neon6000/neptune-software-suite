/************************************************************************
*
*	buddy.c - Buddy Allocator
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/*** This code is subject to change as zone allocation is worked out. ***/

/*
	Buddy Allocator.

	This is used to allocate from KZONE's that require contiguous
	physical memory blocks. E.g. DMA memory.

	The design uses the buddy system with free lists. For example,
	if a KZONE is 512 bytes in size,

	                +-----------------------------------------------+
	_exFreeList[0]  |                    512 bytes                  |
	                +------------------------+----------------------+
	_exFreeList[1]  |       256 bytes        |       256 bytes      |
	                +------------------------+----------------------+
	_exFreeList[2]  | 128 bytes | 128 bytes | 128 bytes | 128 bytes |
	                +-----------------------------------------------+
	_exFreeList[3]  |  64 | 64  | 64  |  64 | 64  | 64  | 64  | 64  |
	                +-----------------------------------------------+
	_exFreeList[4]  |32|32|32|32|32|32|32|32|32|32|32|32|32|32|32|32|
	                +-----------------------------------------------+

	The Free Lists are just a list of LIST_ENTRY, where each LIST_ENTRY is
	stored within the free block it manages. NEXEC keeps track of the first free
	page in the _exFreeList global array.
*/

typedef struct _KZONE {
	addr_t     start;
	size_t     size;
	uint32_t   attributes;
	LIST_ENTRY entry;
}KZONE;

/* returns number of allocations in a level. E.g. Level 1 in above example has 2 allocations. */
#define ALLOCS_LEVEL(level) (1<<(level))

/* returns number of bytes per block at a given level. E.g. Level 1 in above example is 256 bytes. */
#define SIZE_LEVEL(level,size) ((size) / (1<<(level)))

/* something arbitrary. We should never, ever get this high. */
#define MAX_LEVELS 32

/* index of pointer at a given level. */
#define INDEX_OF_POINTER(pointer,start,level,size) (((pointer)-(start)) / (SIZE_LEVEL(level,size)))

/*** DESIGN NOTE: These lists are ZONE specific. */

/* free lists. */
LIST_ENTRY _exFreeListHeads [MAX_LEVELS];

PRIVATE
void
ExBuddyPrint(IN KZONE* zone) {

	index_t level = 0;

	printf("\n\n\rPretty Print:");
	printf("\n\r------------------");

	while (level < MAX_LEVELS - 1) {

		LIST_ENTRY* e;

		if (! NrlIsListEmpty(&_exFreeListHeads[level])) {

			printf("\n\r[%u %u bytes]", level, SIZE_LEVEL(level, zone->size));

			for (e =_exFreeListHeads[level].next;
				e != &_exFreeListHeads[level];
				e = e->next) {

				printf(" 0x%X", e);
			}
		}
		level++;
	}
}

/* need to optimize. Rounds "value" to next power of 2. */
PRIVATE
size_t
ExBuddyRoundUp(IN size_t value) {
	size_t result = 1;
	while (result < value)
		result *= 2;
	return result;
}

/* converts size to _exFreeListHeads index. */
PRIVATE
index_t
ExBuddySizeToLevel(IN KZONE* zone,
				   IN size_t value) {

	index_t result = 0;
	while (result++ < MAX_LEVELS)
		if (SIZE_LEVEL(result,zone->size) == value)
			return result;
	return (index_t) MAX_LEVELS + 1;
}

PRIVATE
size_t
ExBuddyLevelToSize(IN KZONE* zone,
				   IN size_t value) {

	index_t result = zone->size;
	while (value--)
		result /= 2;
	return result;
}

PRIVATE
void*
ExBuddyAllocEx(IN KZONE* zone,
			   IN index_t level) {

	void* memory;
	LIST_ENTRY* e;
	LIST_ENTRY* left;
	LIST_ENTRY* right;

	/* sanity check. */
	if (level > MAX_LEVELS)
		return NULL;
	memory = NULL;

	/* if current level is empty, we cannot allocate from it yet. */
	if (NrlIsListEmpty(&_exFreeListHeads[level])) {

		size_t size;

		/* allocate memory block from larger free list. */
		memory = ExBuddyAllocEx(zone,level-1);
		if (!memory)
			return NULL; /* out of memory. */

		/* we are about to split this memory block. The size
		should be sizeof (memory block) / 2. */
		size = SIZE_LEVEL(level,zone->size);

		/* split memory into two blocks. */
		left  = (LIST_ENTRY*) memory;
		right = (LIST_ENTRY*) ((uint8_t*) memory + size);

		/* and initialize the entries. */
		NrlInitializeListHead(left);
		NrlInitializeListHead(right);

		/* insert the two blocks. */
		NrlInsertTailList(&_exFreeListHeads[level],left);
		NrlInsertTailList(&_exFreeListHeads[level],right);
	}

	/* allocate from current level. */
	e = NrlRemoveHeadList(&_exFreeListHeads[level]);
	memory = (void*) e;

	/* all done. */
	return memory;
}

PUBLIC
void*
ExBuddyAlloc (IN KZONE* zone,
			  IN size_t size) {

	/* adjust size so its a multiple of 2. */
	size_t adjustedSize = ExBuddyRoundUp(size);

	/* now get its level. */
	index_t level = ExBuddySizeToLevel(zone, adjustedSize);
	if(level > MAX_LEVELS)
		return NULL; /* requested size exceeds. */

	/* allocate. */
	return ExBuddyAllocEx(zone,level);
}

PRIVATE
void
ExBuddyFreeEx (IN KZONE* zone,
			   IN void* memory,
			   IN index_t level) {

	size_t adjustedSize;
	index_t index;
	addr_t  buddy;
	LIST_ENTRY* entry;
	LIST_ENTRY* memoryEntry;

	/* compute block size of level. */
	adjustedSize = ExBuddyLevelToSize(zone, level);

	/* compute index in a level. */
	index = INDEX_OF_POINTER((addr_t)memory,zone->start,level,zone->size);

	/* find the buddy. */
	if ((index & 1) == 0)
		buddy = (addr_t)memory + adjustedSize;
	else
		buddy = (addr_t)memory - adjustedSize;

	/* now we have to check if this buddy is also free. */
	entry = NULL;
	if (! NrlIsListEmpty(&_exFreeListHeads[level])) {
		for (entry =_exFreeListHeads[level].next;
			entry != &_exFreeListHeads[level];
			entry = entry->next)
		{
			if (entry == buddy)
				break;
		}
	}

	/* add it back to free list. */
	memoryEntry = (LIST_ENTRY*)memory;
	NrlInitializeListHead(memoryEntry);
	NrlInsertTailList(&_exFreeListHeads[level], memoryEntry);

	/* if buddy is also free, we can merge. */
	if (entry == buddy) {

		void* bufferStart;

		NrlRemoveEntryList(memoryEntry);
		NrlRemoveEntryList(buddy);

		/* always merge using the lowest address. */
		if ((index & 1) == 0)
			bufferStart = memoryEntry;
		else
			bufferStart = buddy;

		/* try to free this block as well. */
		ExBuddyFreeEx(zone,bufferStart,level - 1);
	}
}

PUBLIC
void
ExBuddyFree (IN KZONE* zone,
			 IN void* memory,
			 IN size_t size) {

	size_t adjustedSize;
	index_t level;
	index_t index;
	addr_t  buddy;
	LIST_ENTRY* entry;
	LIST_ENTRY* buddyEntry;
	LIST_ENTRY* memoryEntry;

	/* adjust size so its a multiple of 2. */
	adjustedSize = ExBuddyRoundUp(size);

	/* now get its level. */
	level = ExBuddySizeToLevel(zone, adjustedSize);

	/* sanity check. */
	ASSERT(level < MAX_LEVELS);

	/* compute index in a level. */
	index = INDEX_OF_POINTER((addr_t)memory,zone->start,level,zone->size);

	ExBuddyFreeEx(zone,memory,level);
}

PUBLIC
KZONE*
ExAllocateZone(void) {

	index_t     i;
	KZONE*      zone;
	LIST_ENTRY* e;
	
	/* create zone. */

	zone = malloc(sizeof(KZONE));
	zone->start = (addr_t) _aligned_malloc(1024,0x1000);
	zone->size  = 1024;

	/* need to initialize. */

	for (i = 0; i < MAX_LEVELS; i++)
		NrlInitializeListHead(&_exFreeListHeads[i]);

	/* _exFreeListHeads [0] can not be NULL.
	So here we store a LIST_ENTRY at the start of the buffer
	and make _exFreeListHeads [0] point to it. */

	e = (LIST_ENTRY*)zone->start;
	NrlInitializeListHead(e);
	NrlInsertTailList(&_exFreeListHeads[0], e);

	return zone;
}
