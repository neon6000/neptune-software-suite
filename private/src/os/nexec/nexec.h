/************************************************************************
*
*	nexec.h - NEXEC Public Definitions.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <types.h>
#include <nrtl.h>

#ifndef NEXEC_H
#define NEXEC_H


#define PAGE_SIZE 4096

#define TO_PFN(x) ((x)/PAGE_SIZE)
#define FROM_PFN(x) ((x)*PAGE_SIZE)

/* These are subject to being updated a lot as things progress,
so we define these right at the start of this file. */

/* boot module. */
typedef struct _KBOOTMODULE {
	addr_t base;
	struct KBOOTMODULE* next;
}KBOOTMODULE;

/* memory map entry. */
typedef struct _KMMAP {
	int    type;
	addr_t start;
	size_t size;
	struct KMMAP* next;
}KMMAP;

/* loader paramater block. */
typedef struct _KLPB {
	char*  loader;
	char*  command;
	addr_t apmTable;
	KMMAP  mmap;
	KBOOTMODULE modules;
}KLPB;

/* kernel status codes. */
typedef enum _KSTATUS {
	STAT_GENERAL_ERROR     = 0,
	STAT_SUCCESS,
	STAT_INVALID_ARGUMENT,
	STAT_OUT_OF_MEMORY,
	STAT_GENERAL_MEMORY,
}KSTATUS;

/* message operation codes. */
typedef enum _KMESSAGEOP {

	/* interrupt request. */
	MSG_HWIRQ,

}KMESSAGEOP;

/* message source types. */
typedef enum _KMESSAGE_SOURCE {

	MSG_SOURCE_ANY = -1

}KMESSAGE_SOURCE;

typedef struct _KPROCESSOR {
	uint8_t  id;
	uint8_t  type;
	uint8_t  family;
	uint8_t  majorStepping;
	uint8_t  minorStepping;
	uint8_t  model;
	/* might not be big enough. */
	uint64_t features;
	size_t   virtualAddressSize;
	size_t   physicalAddressSize;
}KPROCESSOR;

/*** NEXEC Global Definitions ********************************/

#define   KERNEL_VIRTUAL              0xc0000000
#define   KERNEL_PHYSICAL             0x100000

/* kernel event type. */
typedef unsigned long KEVENT;

/* kernel spinlock type. */
typedef long KSPIN_LOCK;

/* kernel clock type. */
typedef long KCLOCK;

/* message supporting integral types. */
typedef struct _KMESSAGE1 {
	int a,b,c,d,e,f;
} KMESSAGE1;

/* message supporting addresses. */
typedef struct _KMESSAGE2 {
	addr_t   a,b,c;
	uint32_t d,e,f;
}KMESSAGE2;

/* add more message types on a per-need basis. */

/* message type. */
typedef enum _KMESSAGE_TYPE {
	KM_SEND,
	KM_RECEIVE,
	KM_SEND_RECEIVE,
	KM_NOTIFY,
	KM_ECHO
}KMESSAGE_TYPE;

/* kernel message object. */
typedef struct _KMESSAGE {
	KMESSAGE_TYPE type;
	int           operation;
	LIST_ENTRY    next;
	union {
		KMESSAGE1 a;
		KMESSAGE2 b;
	}u;
}KMESSAGE;

/* used by usermode drivers to access io port space. */
typedef RTLBITMAP KIOBITMAP;


/* device object. */
typedef struct _KDEVICE {
	int i;
}KDEVICE;

/* volume paramater block. */
typedef struct _KVPB {
	int i;
}KVPB;

/* file descriptor. */
typedef struct _KFILE {
	size_t   size;
	KDEVICE* device;
	KVPB*    vpb;
	void*    ext;             /* driver specific information. */
	BOOL     readAccess;
	BOOL     writeAccess;
	BOOL     deleteAccess;
	BOOL     sharedRead;
	BOOL     sharedWrite;
	BOOL     sharedDelete;
	char*    fileName;        /* this needs to be UNICODE. */
	uint64_t currentOffset;
}KFILE;

typedef enum _MM_FLAGS {

	MM_RESERVE     = 0x00000200,
	MM_COMMIT      = 0x00001000,
	MM_WRITE_WATCH = 0x00200000,
	MM_LARGE_PAGES = 0x20000000

}MM_FLAGS;

typedef enum _MM_PROTECT {

	MM_PAGE_NO_ACCESS = 1,
	MM_PAGE_READONLY = 2,
	MM_PAGE_READWRITE = 4,
	MM_PAGE_WRITECOPY = 8,
	MM_PAGE_EXECUTE = 0x10,
	MM_PAGE_EXECUTE_READ = 0x20,
	MM_PAGE_EXECUTE_READWRITE = 0x40,
	MM_PAGE_EXECUTE_WRITECOPY = 0x80,
	MM_PAGE_GUARD = 0x100,
	MM_PAGE_NOCACHE = 0x200,
	MM_PAGE_WRITECOMBINE = 0x400

}MM_PROTECT;

/* virtual address descriptor. */
typedef struct _KVAD {
	struct _KVAD* left;
	struct _KVAD* right;
	addr_t        start;
	size_t        size;
	MM_FLAGS      flags;
	MM_PROTECT    protect;
	KSPIN_LOCK    lock;
	KFILE*        file;       /* mapped file information (if any). */
}KVAD;


/* process control block. */
typedef struct _KPROCESS {
	/* general info. */
	unsigned int     id;
	KVAD*            vad;
	/* used by servers. */
//	KGDTENTRY*       ldt;
	KIOBITMAP*       iobm;
	/* used by scheduler. */
	unsigned int     priority;
	PHYSICAL_ADDRESS directoryTable;
	LIST_ENTRY       threadListHead;
	/* points to next KPROCESS in global process list. */
	LIST_ENTRY       entry;
}KPROCESS;

/* thread control block. */
typedef struct _KTHREAD {
	/* general. */
	uint32_t      id;
	KPROCESS*     parent;
	KCLOCK*       userTime;
	KCLOCK*       kernelTime;
	LIST_ENTRY    listEntry;  /* ref: KPROCESS.threadListHead */
	/* scheduler. */
	unsigned int  priority;
	addr_t        stackBase;  /* user mode stack. */
	size_t        stackSize;
	addr_t        kstackBase; /* kernel mode stack. */
	addr_t        kstackTop;
	LIST_ENTRY    readyQueueEntry;
	index_t       readyQueuePriorityLevel;
	uint64_t      affinity; /* cpu affinity mask. */
	KCLOCK        quantum;
	/* message passing. */
	LIST_ENTRY    sendingProcessListHead;
	LIST_ENTRY    nextSender;
	LIST_ENTRY    irqMessageListHead;
	LIST_ENTRY    messageListHead;
	int           msgSource;
	int           msgDestination;
	/* signals. */
	RTLBITMAP     signals;
}KTHREAD;

/* interrupt service routine. */
typedef int (*KISR)(void);

/* logical interrupt. */
typedef struct _KHWINT {
	int           irq;
	KISR          request;
	LIST_ENTRY    entry;
}KHWINT;

/* kernel mutex type. */
typedef struct _KMUTEX {
	int filler;
}KMUTEX;

typedef uint32_t PFN_NUMBER;

extern PUBLIC void ExInitializeSpinLock(IN KSPIN_LOCK* lock);

extern PUBLIC void ExAcquireSpinLock(IN volatile KSPIN_LOCK* lock);

extern PUBLIC void ExReleaseSpinLock(IN volatile KSPIN_LOCK* lock);

#endif
