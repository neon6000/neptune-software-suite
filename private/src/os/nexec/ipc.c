/************************************************************************
*
*	ipc.c - Inter-Process Communication Services.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "ipc.h"
#include <slab.h>


/*** PUBLIC Definitions ***********************************/


PUBLIC status_t IpcRaise(IN KTHREAD* in, IN int signal) {

	if (NrlBitTest(&in->signals, signal))
		return FALSE;

	NrlSetBit(&in->signals, signal);
	return TRUE;
}

PUBLIC status_t IpcSend(IN int destination, IN KMESSAGE* m) {

	UNIMPLEMENTED;
	return 0;
}

PUBLIC status_t IpcReceive(IN int source, OUT KMESSAGE* m) {

	UNIMPLEMENTED;
	return 0;
}

PUBLIC status_t IpcSendReceive(IN int source, IN KMESSAGE* in, OUT KMESSAGE* out) {

	UNIMPLEMENTED;
	return 0;
}

PUBLIC status_t IpcNotify(IN int source, IN KMESSAGE* m) {

	UNIMPLEMENTED;
	return 0;
}

PUBLIC status_t IpcEcho(IN int source, IN KMESSAGE* m) {

	UNIMPLEMENTED;
	return 0;
}

PUBLIC status_t IpcSystemRequest(IN KMESSAGE_TYPE function, IN int source,
								 IN OUT OPTIONAL KMESSAGE* m) {

	UNIMPLEMENTED;
	return 0;
}
