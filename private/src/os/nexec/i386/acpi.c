/************************************************************************
*
*	i386/acpi.c - Advanced Configuration Power Interface
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This component implemenets ACPI services. */

#include <nexec.h>
#include <i386/hal.h>

/*** Private Definitions **********************************/


/**
*	Copy of RSDP.
*/
RSDP _rsdp;

/**
*	SDT Lookup Table
*/
typedef struct SDP_ENTRY {
	char tableName [SDT_SIGNATURE_LENGTH + 1];
	uint32_t identifier;
	size_t size;
	PHYSICAL_ADDRESS address;
}SDP_ENTRY;

SDP_ENTRY _SystemDescriptorTables [MAX_RSDT];

/**
*	Size of SDT lookup table.
*/
index_t _SystemDescriptorTablesCount;

/**
*	Given table name, return its base address.
*/
SDP_ENTRY* AcpiGetTable (IN uint32_t name) {

	unsigned int i;
	for (i = 0; i < _SystemDescriptorTablesCount; i++)
		if (_SystemDescriptorTables[i].identifier == name)
			return &_SystemDescriptorTables[i];

	return NULL;
}

/**
*	Test for a valid checksum.
*/
BOOL AcpiCheckChecksum(IN SDT_HEADER* header, IN size_t size) {

	uint8_t total;
	unsigned int i;
	for (i = 0; i < size; i++)
		total += ((uint8_t*)header)[i];
	if (total == 0)
		return TRUE;
	else
		return FALSE;
}

/**
*	Attempts to read the RSDP.
*/
INIT_SECTION BOOL AcpiReadRdsp (void) {

	uint8_t* biosArea;
	uint16_t* rsdp;

	/* rsdp is somewhere from 0xe0000 - 0x100000 */
	biosArea = MmMapIoSpace(0xe0000, 0x20000, FALSE);
	if (!biosArea) {
		DbgPrintf("\nAcpiReadRdsp: MmMapIoSpace failed");
		return FALSE; // read 0x40e
	}

	/* attempt to find rsdp table. */
	for (rsdp = (uint16_t*) biosArea; rsdp < biosArea + 0x20000; rsdp++) {

		if (NlrCompareMemory(rsdp, "RSD PTR ", 8) == 8) {

			/* keep a copy of the table. */
			NrlCopyMemory(rsdp, &_rsdp, sizeof(RSDP));
			MmUnmapIoSpace(biosArea, 0x20000);
			return TRUE;
		}
	}

	DbgPrintf("\nAcpiReadRdsp : Not found");
	MmUnmapIoSpace(biosArea, 0x20000);
	return FALSE;
}

/**
*	Compile SDT lookup table.
*/
INIT_SECTION void AcpiCompileRsdt(IN RSDP* rsdp) {

	index_t count;
	index_t i;
	index_t j;
	RSDT* sdt;

	sdt = (RSDT*) MmMapIoSpace(rsdp->rsdtAddr,0,FALSE);

	count = (sdt->header.length - sizeof (SDT_HEADER)) / sizeof (PHYSICAL_ADDRESS);
	for (i = 0; i < count; i++) {

		SDT_HEADER* sdtHeader;

		sdtHeader = (SDT_HEADER*) MmMapIoSpace (sdt->sdtPointers[i],0,FALSE);

		_SystemDescriptorTables[i].identifier = *(uint32_t*)sdtHeader->signature;
		_SystemDescriptorTables[i].size = sdtHeader->length;
		_SystemDescriptorTables[i].address = sdt->sdtPointers[i];

		MmUnmapIoSpace(sdtHeader,0);
	}

	MmUnmapIoSpace(sdt,0);
	_SystemDescriptorTablesCount = count;
}

/*** Public Definitions **********************************/

/**
*	Returns a list of DWORD table identifiers used by ACPI.
*/
PUBLIC size_t AcpiEnumerateTables(OUT void* buffer, IN size_t size) {

	uint32_t* id;
	int i;

	/* make sure there is enough space. */
	if (size < _SystemDescriptorTablesCount * SDT_SIGNATURE_LENGTH)
		return _SystemDescriptorTablesCount * SDT_SIGNATURE_LENGTH;

	/* the identifier is a 4 byte ASCII table name. */
	id = (uint32_t*) buffer;
	for (i = 0; i < _SystemDescriptorTablesCount; i++)
		NrlCopyMemory(_SystemDescriptorTables[i].tableName, id [i], SDT_SIGNATURE_LENGTH);
}

/**
*	Returns firmware table specified.
*/
PUBLIC size_t AcpiGetFirmwareTable(IN uint32_t signature, OUT void* buffer, IN size_t size) {

	SDP_ENTRY* sdp;
	void* table;

	sdp = AcpiGetTable(signature);
	if (!sdp)
		return 0;

	if (size < sdp->size)
		return sdp->size;

	if (!buffer)
		return 0;

	table = MmMapIoSpace(sdp->address, sdp->size, FALSE);
	NrlCopyMemory(table,buffer,sdp->size);
	MmUnmapIoSpace(table, sdp->size);

	return sdp->size;
}

/**
*	Initialize ACPI
*/
INIT_SECTION PUBLIC BOOL AcpiInitialize (void) {

	AcpiReadRdsp ();
	AcpiCompileRsdt (&_rsdp);
	return TRUE;
}

/**
*	Shutdown system.
*/
PUBLIC void AcpiShutdown (void) {

	UNIMPLEMENTED;
}

/**
*	Gets an element from the ACPI MADT table.
*/
PUBLIC MADT_ENTRY* ApicAcpiGetMadtEntry(IN MADT* madt, IN index_t idx) {

	uint8_t* current;
	uint8_t* end;
	int i;

	current = (uint8_t*) madt + sizeof (MADT);
	end = (uint8_t*) madt + madt->header.length;

	for (i = 0; i <= idx && current < end; i++) {
		if (idx == i)
			return (MADT_ENTRY*) current;
		current += ((MADT_ENTRY_HEADER*) current)->length;
	}

	return NULL;
}
