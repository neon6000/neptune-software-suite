/************************************************************************
*
*	i386/idt.c - Interrupt Descriptor Table.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nrtl.h>
#include "hal.h"

/*** PRIVATE Definitions **********************************/

typedef struct KIDTR {
	uint16_t limit;
	uint32_t base;
}KIDTR;

/* Global Interrupt Descriptor Table. */
#define KIDT_MAX 256
KIDT  _kidt[KIDT_MAX];
KIDTR _kidtr;

/**
*	Set IDTR register for current CPU
*/
PRIVATE void HalSetIdtr(void) {

	_kidtr.base  = (uint32_t) &_kidt[0];
	_kidtr.limit = (sizeof(KIDT) * KIDT_MAX) - 1;

	__asm lidt [_kidtr]
}

/**
*	Set IDT descriptor gate
*/
PRIVATE void IdtSetGate(IN index_t idx, IN addr_t irq,
						IN uint8_t sel, IN uint8_t gate, IN uint8_t dpl) {

	ASSERT(idx < KIDT_MAX);

	_kidt[idx].base   = (irq & 0x0000ffff);
	_kidt[idx].baseHi = (irq & 0xffff0000) >> 16;
	_kidt[idx].flags.gateType = gate;
	_kidt[idx].flags.dpl      = dpl;
	_kidt[idx].flags.storage  = 0;
	_kidt[idx].sel            = sel;
	_kidt[idx].reserved       = 0;
	_kidt[idx].flags.present  = 1;
}

/*** PUBLIC Definitions **********************************/


/**
*	Returns IDT gate descriptor.
*/
PUBLIC KIDT* HalGetIdtGate(IN index_t idx) {

	ASSERT(idx < KIDT_MAX);
	return &_kidt[idx];
}

/**
*	Exception service routines.
*/
extern void i386DivideByZero (void);
extern void i386DebugException (void);
extern void i386NMIException (void);
extern void i386Breakpoint (void);
extern void i386Overflow (void);
extern void i386BoundException (void);
extern void i386InvalidOpcode (void);
extern void i386FPUNotAvailable (void);
extern void i386DoubleFault (void);
extern void i386CoprocessorSegment (void);
extern void i386InvalidTss (void);
extern void i386SegmentNotPresent (void);
extern void i386StackException (void);
extern void i386GeneralProtectionFault (void);
extern void i386PageFault (void);
extern void i386CoprocessorError (void);
extern void i386AlignmentCheck (void);
extern void i386MachineCheck (void);

/**
*	Initializes interrupt descriptor table.
*/
PUBLIC void HalInitializeIdt(void) {

	NrlZeroMemory(_kidt,sizeof(KIDT) * KIDT_MAX);

	IdtSetGate (0,i386DivideByZero,            8,TrapGate32Bit,DplRing0);
	IdtSetGate (1,i386DebugException,          8,TrapGate32Bit,DplRing0);
	IdtSetGate (2,i386NMIException,            8,TrapGate32Bit,DplRing0);
	IdtSetGate (3,i386Breakpoint,              8,TrapGate32Bit,DplRing0);
	IdtSetGate (4,i386Overflow,                8,TrapGate32Bit,DplRing0);
	IdtSetGate (5,i386BoundException,          8,TrapGate32Bit,DplRing0);
	IdtSetGate (6,i386InvalidOpcode,           8,TrapGate32Bit,DplRing0);
	IdtSetGate (7,i386FPUNotAvailable,         8,TrapGate32Bit,DplRing0);
	IdtSetGate (8,i386DoubleFault,             8,TaskGate32Bit,DplRing0);
	IdtSetGate (9,i386CoprocessorSegment,      8,TrapGate32Bit,DplRing0);
	IdtSetGate (10,i386InvalidTss,             8,TrapGate32Bit,DplRing0);
	IdtSetGate (11,i386SegmentNotPresent,      8,TrapGate32Bit,DplRing0);
	IdtSetGate (12,i386StackException,         8,TrapGate32Bit,DplRing0);
	IdtSetGate (13,i386GeneralProtectionFault, 8,TrapGate32Bit,DplRing0);
	IdtSetGate (14,i386PageFault,              8,TrapGate32Bit,DplRing0);
	IdtSetGate (15,i386CoprocessorError,       8,TrapGate32Bit,DplRing0);
	IdtSetGate (16,i386AlignmentCheck,         8,TrapGate32Bit,DplRing0);
	IdtSetGate (17,i386MachineCheck,           8,TrapGate32Bit,DplRing0);

	HalSetIdtr ();
}
