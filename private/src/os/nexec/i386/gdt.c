/************************************************************************
*
*	i386/gdt.c - Global Descriptor Table.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "hal.h"

/*** EXTERNAL Definitions ********************************/

extern KTSS _ktss;

/*** PRIVATE Definitions *********************************/

/* Number of GDT Entries. */
#define GDT_MAX_ENTRIES 10

/* GDT Register Descriptor. */
typedef struct _KGDTR {
	uint16_t limit;
	uint32_t base;
}KGDTR;

/* Global Descriptor Table. */
KGDTENTRY _kgdt[GDT_MAX_ENTRIES];
KGDTR     _kgdtr;

/**
*	Sets GDT Register of current CPU.
*/
PRIVATE void HalSetGdtr(IN int limit) {

	_kgdtr.base  = &_kgdt[0];
	_kgdtr.limit = (sizeof (KGDTENTRY) * limit) - 1;

	_asm lgdt [_kgdtr]
}

/**
*	Set GDT descriptor.
*/
PRIVATE void HalSetDescriptor(IN index_t idx, IN uint32_t base, IN uint32_t limit,
							 IN uint8_t type, IN uint8_t dpl, IN uint8_t defaultBig) {

	ASSERT(idx <= GDT_MAX_ENTRIES);

	_kgdt[idx].type    = type;
	_kgdt[idx].dpl     = dpl;
	_kgdt[idx].defaultBig = defaultBig;

	_kgdt[idx].limitLo = (limit & 0x0ffff);
	_kgdt[idx].limitHi = (limit & 0xf0000) >> 16;

	_kgdt[idx].baseLo  = (base & 0x0000ffff);
	_kgdt[idx].baseMid = (base & 0x00ff0000) >> 16;
	_kgdt[idx].baseHi  = (base & 0xff000000) >> 24;

	_kgdt[idx].present     = TRUE;
	_kgdt[idx].reserved    = 0;
	_kgdt[idx].system      = 0;
	_kgdt[idx].granularity = PageGrandularity;
}

/*** PUBLIC Definitions *********************************/

/**
*	Get a GDT entry.
*/
PRIVATE KGDTENTRY* HalGetDescriptor(IN index_t idx) {

	ASSERT(idx <= GDT_MAX_ENTRIES);
	return &_kgdt[idx];
}

/**
*	Install system GDT.
*/
PUBLIC void HalInstallGdt(void) {

	HalSetDescriptor(0,0,0,0,0,0);
	HalSetDescriptor(1,0,0xffffffff,                     ExecutableCode,    DplRing0, ProtectedMode32Bit);
	HalSetDescriptor(2,0,0xffffffff,                     NonExecutableData, DplRing0, ProtectedMode32Bit);
	HalSetDescriptor(3,0,0xffffffff,                     ExecutableCode,    DplRing3, ProtectedMode32Bit);
	HalSetDescriptor(4,0,0xffffffff,                     NonExecutableData, DplRing3, ProtectedMode32Bit);
	HalSetDescriptor(5,0,0xffffffff,                     ExecutableCode,    DplRing3, ProtectedMode16Bit);
	HalSetDescriptor(6,0,0xffffffff,                     NonExecutableData, DplRing3, ProtectedMode16Bit);
	HalSetDescriptor(7,&_ktss,&_ktss + sizeof(KTSS),     NonExecutableData, DplRing0, ProtectedMode32Bit);
	HalSetGdtr (8);
}
