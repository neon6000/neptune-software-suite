/************************************************************************
*
*	i386/except.c - Exceptions.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nexec.h>
#include <i386/hal.h>

typedef struct _KEXCEPTION_CONTEXT {

	/* pushad */
	uint32_t eax, ecx, edx, ebx, esp2, ebp, esi, edi;

	/* cpu context */
	uint32_t vector;
	uint32_t errorCode;
	uint32_t eip;
	uint32_t cs;
	uint32_t eflags;

	/* only passed when called from user mode. */
	uint32_t esp;
	uint32_t ss;

}KEXCEPTION_CONTEXT;

/**
*	Process exception.
*/
PUBLIC void HalExceptionHandler(IN uint32_t nested, IN KEXCEPTION_CONTEXT* frame) {

	DbgPrintf("\n*** HalExceptionHandler (%x)", frame->vector);
	DbgPrintf("\nerrorCode = %x eflags = %x", frame->errorCode, frame->eflags);
	DbgPrintf("\ncs:eip = %x:%x", frame->cs,frame->eip);

	if (frame->vector == 14) {
		MmProcessPageFault(HalGetPageFaultAddress(), frame->errorCode);
	}

	DbgException (NULL);
}

/**
*	For now to make MSVC happy.
*/
PUBLIC void HalApEntryPoint (void) {

}
