/************************************************************************
*
*	i386/apic.c - Advanced PIC.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nexec.h>
#include <i386/hal.h>

/*** PRIVATE Definitions *********************************/

/**
*	IOAPIC Device.
*/
typedef struct _KIOAPIC {
	uint32_t         id;
	VIRTUAL_ADDRESS  base;
	PHYSICAL_ADDRESS phys;
	uint32_t         maxRedirectEntries;
	uint32_t         globalIntBase;
}KIOAPIC;

typedef struct _KIOAPICIRQ {
	KIOAPIC* apic;
	uint32_t redirectionEntry;
	uint32_t vector;
	uint32_t state;
}KIOAPICIRQ;

/* Maximum number of IOAPIC's supported. */
#define MAX_IOAPIC 32

/* Maximum number of IOAPIC IRQ's supported. */
#define MAX_IOAPIC_IRQ 255

/**
*	IOAPIC list.
*/
KIOAPIC _SystemIoApics [MAX_IOAPIC];

/**
*	IRQ list.
*/
KIOAPICIRQ _SystemApicIrqs [MAX_IOAPIC_IRQ];

/**
*	Number of IOAPIC's in the system.
*/
size_t _NumberOfIoApics;

/*
Each CPU has its own Local APIC (LAPIC). We get the location of
each LAPIC via the processor MSR. Note the physical address can
be relocated if needed.

      MAXPHYSADDR          12 11    8      0
+----------------+-----------+--+--+-+-----+
| Reserved       | APIC Base |G |  |B|     |
+----------------+-----------+--+--+-+-----+
	G - APIC Global Enable/Disable
	B - Processor is BSP
	APIC Base - Base of LAPIC Register Space
*/

/* lapic MSR. */
#define LAPIC_BASE_MSR       0x1b

/* all other bits in MSR reserved. */
#define LAPIC_MSR_BSP    0x00000100
#define LAPIC_MSR_ENABLE 0x00000800
#define LAPIC_MSR_BASE   0xfffff000

/* local APIC registers. */
typedef enum _LAPIC_REGISTERS {

	LAPIC_REG_ID                 = 0x20,
	LAPIC_REG_VERSION            = 0x30,
	LAPIC_REG_TPR                = 0x80,     /* task priority register */
	LAPIC_REG_APR                = 0x90,     /* arbitrartion priority register */
	LAPIC_REG_PPR                = 0xa0,     /* processor priority register */
	LAPIC_REG_EOI                = 0xb0,     /* end of interrupt */
	LAPIC_REG_RRD                = 0xc0,     /* remote read register */
	LAPIC_REG_LOGDEST            = 0xd0,     /* logical destination */
	LAPIC_REG_FMTDEST            = 0xe0,     /* destination format */
	LAPIC_REG_SPURIOUS_IV        = 0xf0,     /* spurious interrupt vector */
	LAPIC_REG_ISR                = 0x100,    /* in service register */
	LAPIC_REG_TMR                = 0x180,
	LAPIC_REG_TRR                = 0x200,
	LAPIC_REG_ERR                = 0x280,
	LAPIC_REG_LVT_CMCI           = 0x2f0,
	LAPIC_REG_ICR_LOW            = 0x300,
	LAPIC_REG_ICR_HIGH           = 0x310,
	LAPIC_REG_LVT_TIMER          = 0x320,
	LAPIC_REG_LVT_THERMAL_SENSOR = 0x330,
	LAPIC_REG_LVT_PERF_MONITOR_COUNTER = 0x340,
	LAPIC_REG_LVT_LINT0          = 0x350,
	LAPIC_REG_LVT_LINT1          = 0x360,
	LAPIC_REG_LVT_ERR            = 0x370,
	LAPIC_REG_INITIAL_COUNT      = 0x380,
	LAPIC_REG_CURRENT_COUNT      = 0x390,
	LAPIC_DIVIDE_CONFIG          = 0x3e0,

}LAPIC_REGISTERS;

/* bit format for LAPIC_ICR_LOW register. */
typedef struct _LAPIC_ICR_LOW {
	uint32_t vector            : 8;
	uint32_t destMode          : 3;
	uint32_t destPhysOrLogical : 1;
	uint32_t deliveryStatus    : 1;
	uint32_t reserved          : 1;
	uint32_t initLevelActive   : 1;
	uint32_t initLevelInactive : 1;
	uint32_t reserved2         : 2;
	uint32_t destType          : 2;
	uint32_t reserved3         : 12;
}LAPIC_ICR_LOW;

#define IOAPIC_ID             0
#define IOAPIC_VERSION        1
#define IOAPIC_ARB            2
#define IOAPIC_REDIR_TABLE 0x10

typedef union _IOAPIC_REDIRECT {
	struct {
		uint32_t vector        : 8;
		uint32_t deliveryMode  : 3;
		uint32_t destMode      : 1;
		uint32_t busy          : 1;
		uint32_t polarity      : 1;
		uint32_t remoteIRR     : 1;
		uint32_t triggerMode   : 1;
		uint32_t interruptMask : 1;
		uint32_t reserved      : 15;
	};
	uint32_t data;
}IOAPIC_REDIRECT;

typedef enum _IOAPIC_DELIVERY_MODE {
	IoApicFixedDeliveryMode          = 0,
	IoApicLowestPriorityDeliveryMode = 1,
	IoApicSmiDeliveryMode            = 2,
	IoApicNmiDeliveryMode            = 4,
	IoApicInitDeliveryMode           = 5,
	IoApicExtlInitDeliveryMode       = 7
}IOAPIC_DELIVERY_MODE;

typedef enum _IOAPIC_DESTINATION_MODE {
	IoApicPhysicalDestinationMode    = 0,
	IoApicLogicalDestinationMode     = 1
}IOAPIC_DESTINATION_MODE;


VIRTUAL_ADDRESS _LocalApicBase;

MADT* _ApicMADT;

/*
	Detect Local APIC (LAPIC) support for current processor.
*/
PRIVATE BOOL LapicDetect(void) {

	int info[4];
	HalCpuid(info,1);
	return info[3] & 0x200;
}

/*
	Test if we are the bootstrap processor.
*/
PRIVATE BOOL LapicIsBSP (void) {

	uint64_t value = HalReadMsr(LAPIC_BASE_MSR);
	return ((value & LAPIC_MSR_BSP) >> 8) == 1 ? TRUE : FALSE;
}

/*
	Returns physical address of Local APIC register space.
*/
PRIVATE addr_t LapicGetBaseAddress (void) {

	uint64_t value = HalReadMsr(LAPIC_BASE_MSR);
	return (addr_t) (value & LAPIC_MSR_BASE);
}

/*
	Sets Local APIC base address.
*/
PRIVATE addr_t LapicSetBaseAddress (IN addr_t base) {

	uint64_t value  = HalReadMsr(LAPIC_BASE_MSR);
	addr_t previous = value & LAPIC_MSR_BASE;

	value |= base & LAPIC_MSR_BASE;
	HalWriteMsr(LAPIC_BASE_MSR, value);

	return previous;
}

/*
	Enables or disables the local APIC.
*/
PRIVATE BOOL LapicEnable(IN BOOL enable) {

	uint64_t value;
	BOOL     previous;

	value    = HalReadMsr(LAPIC_BASE_MSR);
	previous = ((value & LAPIC_MSR_ENABLE) >> 11) == 1 ? TRUE : FALSE;

	if (enable)
		value |= LAPIC_MSR_ENABLE;
	else
		value &= ~LAPIC_MSR_ENABLE;

	HalWriteMsr(LAPIC_BASE_MSR, value);
	return previous;
}

/*
	Read from Local APIC register.
*/
PRIVATE uint32_t LapicRead(IN addr_t base, IN index_t reg) {

	return *(uint32_t*) (base + reg);
}

/*
	Write to Local APIC register.
*/
PRIVATE void LapicWrite(IN addr_t base, IN index_t reg, IN uint32_t value) {

	*(uint32_t*) (base + reg) = value;
}

/**
*	Write Interrupt Command Register
*/
PRIVATE void LapicWriteIcrLow(IN addr_t base, IN uint32_t value) {

	LapicWrite(base, LAPIC_REG_ICR_LOW, value);
}

/**
*	Write Interrupt Command Register
*/
PRIVATE uint32_t LapicWriteIcrHi(IN addr_t base, IN uint32_t value) {

	LapicWrite(base, LAPIC_REG_ICR_HIGH, value);
}

/**
*	Read Interrupt Command Register
*/
PRIVATE void LapicReadIcrLow(IN addr_t base, IN uint32_t value) {

	return LapicRead(base, LAPIC_REG_ICR_LOW);
}

/**
*	Read Interrupt Command Register
*/
PRIVATE void LapicReadIcrHi(IN addr_t base, IN uint32_t value) {

	return LapicRead(base, LAPIC_REG_ICR_HIGH);
}

/**
*	Get local APIC id.
*/
PRIVATE uint32_t LapicGetId (void) {

	return LapicRead(_LocalApicBase, LAPIC_REG_ID);
}

/**
*
*/
PRIVATE uint32_t LapicReadTmr(IN addr_t base, IN uint32_t vector) {

	return LapicRead(base, LAPIC_REG_TMR + 0x10 * (vector >> 5));
}

/**
*
*/
PRIVATE uint32_t LapicReadTrr(IN addr_t base, IN uint32_t vector) {

	return LapicRead(base, LAPIC_REG_TRR + 0x10 * (vector >> 5));
}

/**
*
*/
PRIVATE uint32_t LapicReadIsr(IN addr_t base, IN uint32_t vector) {

	return LapicRead(base, LAPIC_REG_ISR + 0x10 * (vector >> 5));
}

/**
*	Read register from IO Apic
*/
PUBLIC uint32_t IoapicRead(IN void volatile* base, IN uint8_t reg) {

	((uint32_t*)base)[0] = reg;
	return ((uint32_t*)base)[4];
}

/**
*	Write value to IO Apic register
*/
PUBLIC void IoapicWrite(IN void volatile* base, IN uint8_t reg, IN uint32_t value) {

	((uint32_t*)base)[0] = reg;
	((uint32_t*)base)[4] = value;
}

/**
*	Writes an entry into the redirection table
*/
PUBLIC void IoapicWriteRedirectionTableEntry(IN VIRTUAL_ADDRESS base, IN index_t entry, IN uint32_t low, IN uint32_t hi) {

	uint8_t reg = IOAPIC_REDIR_TABLE + entry * 2;

	IoapicWrite(base, reg,    low);
	IoapicWrite(base, reg + 1, hi);
}

/**
*	Mask IOAPIC interrupt signal.
*/
PUBLIC void IoapicMaskIntSignal(IN addr_t base, IN uint32_t signal) {

	uint8_t reg;
	IOAPIC_REDIRECT low;

	reg =  IOAPIC_REDIR_TABLE + signal * 2;
	low.data = IoapicRead (base, reg);
	low.interruptMask = 1;
	IoapicWrite(base, reg, low.data);
}

/**
*	Enable IOAPIC interrupt signal.
*/
PUBLIC void IoapicEnableIntSignal(IN addr_t base, IN uint32_t signal) {

	uint8_t reg;
	IOAPIC_REDIRECT low;

	reg =  IOAPIC_REDIR_TABLE + signal * 2;
	low.data = IoapicRead (base, reg);
	low.interruptMask = 0;
	IoapicWrite(base, reg, low.data);
}

/**
*	Gets IOAPIC identifier.
*/
PUBLIC uint8_t IoapicGetId(IN VIRTUAL_ADDRESS base) {

	return IoapicRead(base,IOAPIC_ID) >> 24;
}

/**
*	Sets IOAPIC identifier.
*/
PUBLIC void IoapicSetId(IN VIRTUAL_ADDRESS base, IN uint32_t id) {

	IoapicWrite(base, IOAPIC_ID, id << 24);
}

/**
*	Get IOAPIC version number.
*/
PUBLIC uint8_t IoapicGetVersion(IN VIRTUAL_ADDRESS base) {

	uint32_t reg = IoapicRead(base,IOAPIC_VERSION);
	return reg & 0xff;
}

/**
*	Gets IOAPIC maximum redirection entry.
*/
PUBLIC uint8_t IoapicGetMaximumRedirectionEntry(IN VIRTUAL_ADDRESS base) {

	uint32_t reg = IoapicRead(base,IOAPIC_VERSION);
	return (reg & 0xff0000) >> 16;
}

/**
*	Get IOAPIC bus arbitration priority.
*/
PUBLIC uint8_t IoapicGetArbId(IN VIRTUAL_ADDRESS base) {

	return IoapicRead(base,IOAPIC_ARB) >> 24;
}

/**
*	Initialize the Interrupt Descriptor Table
*/
PRIVATE BOOL ApicIdtInitialize (void) {


}

/**
*	Get ACPI APIC table
*/
PRIVATE BOOL ApicAcpiGetMADT (void) {

	size_t size;

	size = AcpiGetFirmwareTable('CIPA',NULL,0);
	if (!size)
		return FALSE;

	_ApicMADT = MmAlloc(size);
	AcpiGetFirmwareTable('CIPA',_ApicMADT,size);
	return TRUE;
}

/**
*	Get IO Apics from ACPI
*/
PRIVATE uint32_t ApicAcpiGetIoApics(void) {

	MADT_ENTRY* entry;
	index_t currentEntry;

	currentEntry     = 0;
	_NumberOfIoApics = 0;

	while (TRUE) {

		/* Get next entry from MADT. */
		entry = ApicAcpiGetMadtEntry(_ApicMADT, currentEntry++);
		if (!entry)
			break;

		/* Ignore non IOAPIC entries. */
		if (entry->header.type != MADT_TYPE_IOAPIC)
			continue;

		/* Store this IOAPIC. */
		_SystemIoApics[_NumberOfIoApics].phys          = entry->ioapic.address;
		_SystemIoApics[_NumberOfIoApics].globalIntBase = entry->ioapic.globalIntBase;
		_SystemIoApics[_NumberOfIoApics].id            = entry->ioapic.id;

		/* Map IOAPIC register space. */
		_SystemIoApics[_NumberOfIoApics].base 
 			= MmMapIoSpace(_SystemIoApics[_NumberOfIoApics].phys,0,TRUE);

		/* Get max redirection entries this IOAPIC supports. */
		_SystemIoApics[_NumberOfIoApics].maxRedirectEntries
			= IoapicGetMaximumRedirectionEntry (_SystemIoApics[_NumberOfIoApics].base);

		/* On to the next one. */
		_NumberOfIoApics++;
	}

	/* All done. */
	return _NumberOfIoApics;
}

/**
*	Enable all IO Apic's in the system.
*/
PRIVATE BOOL IoApicEnableAll (void) {

	return FALSE;
}

/**
*	Detect all IO Apic's in the system.
*/
PRIVATE BOOL IoApicDetectAll (void) {

	if (!ApicAcpiGetIoApics())
		return FALSE;
	return TRUE;
}

/**
*	Enable the local APIC for the specified CPU.
*/
PRIVATE BOOL LapicPrepareOnCpu (IN index_t cpu) {

	return TRUE;
}

/*** PUBLIC Definitions *********************************/

/**
*	Initialize APIC component.
*/
PUBLIC BOOL HalApicInitialize (void) {

	if (!LapicDetect())
		return FALSE;

	_LocalApicBase =  MmMapIoSpace(LapicGetBaseAddress(),0x400,FALSE);

	/* Assume 1 CPU. */
	if (!LapicPrepareOnCpu(0))
		return FALSE;

	DbgPrintf("\nBoot cpu Local APIC ID %x", LapicGetId());

	ApicAcpiGetMADT();

	if (!IoApicDetectAll()) {

		LapicEnable (FALSE);
		MmUnmapIoSpace (_LocalApicBase, 0x400);
		return FALSE;
	}

	IoApicEnableAll ();
	ApicIdtInitialize ();

	IoapicEnableIntSignal(_SystemIoApics[0].base, 0);

	return TRUE;
}

/**
*	Send End of Interrupt signal
*/
PUBLIC void HalEndOfInterrupt (void) {

}

/**
*	Send IPI to CPU specified
*/
PUBLIC void HalSendIpi (IN uint32_t vector, IN uint32_t cpu, IN int type) {


}

/**
*	Sends SIPI to specified cpu
*/
PUBLIC BOOL HalSendSIPI (IN uint32_t cpu, IN PHYSICAL_ADDRESS start) {

	return FALSE;
}

/**
*	Sends Init IPI to specified cpu
*/
PUBLIC BOOL HalSendInitIpi(IN uint32_t cpu, IN PHYSICAL_ADDRESS start) {

	return FALSE;
}
