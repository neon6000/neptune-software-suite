/************************************************************************
*
*	i386/hal.h - I386+ Public HAL Definitions.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef HAL_H
#define HAL_H

/*** PUBLIC Declarations ************************************/

/* HAL relies on NEXEC base types. */
#include <types.h>

/* bytes per page. */
#define MM_PAGE_SIZE  0x1000

/* shift to access page frame number. */
#define MM_PFN_SHIFT  12

/* bits in page frame number. */
#define MM_PFN_SIZE   20

#define MM_PDE_COUNT  1024

#define MM_PTE_COUNT  1024

/* system page. */
typedef struct _MMPTE_SYSTEM {
	uint32_t valid: 1;
	uint32_t pageFileLow: 4;
	uint32_t reserved: 7;
	uint32_t pageFileHi: 20;
}MMPTE_SYSTEM;

/* device page. */
typedef struct _MMPTE_I386 {
	uint32_t valid: 1;
	uint32_t write: 1;
	uint32_t user: 1;
	uint32_t writeThrough: 1;
	uint32_t cacheDisable: 1;
	uint32_t accessed: 1;
	uint32_t dirty: 1;
	uint32_t largePage: 1;
	uint32_t global: 1;
/* following 3 bits we can use for any purpose. */
	uint32_t copyOnWrite: 1;
	uint32_t reserved: 1;
	uint32_t reserved2: 1;
	uint32_t pageFrameNumber: 20;
}MMPTE_I386;

typedef struct _MMPTELIST {
	uint32_t valid: 2;
	uint32_t oneEntry : 1;
	uint32_t filler0 : 8;
	uint32_t nextEntry : 20;
	uint32_t prototype : 1;
}MMPTELIST;

/* page table entry. */
typedef struct _MMPTE {
	union {
		unsigned int data;
		MMPTE_I386   device;
		MMPTE_SYSTEM system;
		MMPTELIST    list;
	}u;
}MMPTE, MMPDE;

/* decriptor privilege level. */
typedef enum _KDPL {
	DplRing0,
	DplRing1,
	DplRing2,
	DplRing3
}KDPL;

typedef enum _KGDTENTRY_GRANDULARITY {
	ByteGrandularity,
	PageGrandularity
}KGDTENTRY_GRANDULARIT;

typedef enum _KGDTENTRY_TYPE {
	ExecutableCode    = 0x9a,
	NonExecutableData = 0x92
}KGDTENTRY_TYPE;

typedef enum _KGDTENTRY_SIZE {
	ProtectedMode16Bit,
	ProtectedMode32Bit
}KGDTENTRY_SIZE;

/* system GDT and LDT. */
typedef struct _KGDTENTRY {
	uint16_t limitLo;
	uint16_t baseLo;
	union {
		struct {
			uint32_t baseMid     : 8;
			uint32_t type        : 5;
			uint32_t dpl         : 2;
			uint32_t present     : 1;
			uint32_t limitHi     : 4;
			uint32_t system      : 1;
			uint32_t reserved    : 1;
			uint32_t defaultBig  : 1;
			uint32_t granularity : 1;
			uint32_t baseHi      : 8;
		};
		uint32_t data;
	};
}KGDTENTRY, KLDTENTRY;

/* IDT gate types. */
typedef enum _KIDT_GATE_TYPE {
	TaskGate32Bit      = 5,
	InterruptGate16Bit = 6,
	TrapGate16Bit      = 7,
	InterruptGate32Bit = 0xe,
	TrapGate32Bit      = 0xf
}KIDT_GATE_TYPE;

/* IDT Entry. */
typedef struct _KIDT {
	uint16_t base;
	uint16_t sel;
	uint8_t  reserved;
	struct {
		uint8_t gateType : 4;
		uint8_t storage  : 1;
		uint8_t dpl      : 2;
		uint8_t present  : 1;
	}flags;
	uint16_t baseHi;
}KIDT;


/* the order of this structure is important.
Used by dispatcher. */
typedef struct _KCONTEXT {
	uint32_t gs;
	uint32_t fs;
	uint32_t es;
	uint32_t ds;
	uint32_t eax;
	uint32_t ebx;
	uint32_t ecx;
	uint32_t edx;
	uint32_t esi;
	uint32_t edi;
	uint32_t esp;
	uint32_t ebp;
	uint32_t eip;
	uint32_t cs;
	uint32_t flags;
	uint32_t user_stack;
	uint32_t user_ss;
}KCONTEXT;

/* Task State Segment. */
typedef struct KTSS {
	uint32_t prevTss;
	uint32_t esp0;
	uint32_t ss0;
	uint32_t esp1;
	uint32_t ss1;
	uint32_t esp2;
	uint32_t ss2;
	uint32_t cr3;
	uint32_t eip;
	uint32_t eflags;
	uint32_t eax;
	uint32_t ecx;
	uint32_t edx;
	uint32_t ebx;
	uint32_t esp;
	uint32_t ebp;
	uint32_t esi;
	uint32_t edi;
	uint32_t es;
	uint32_t cs;
	uint32_t ss;
	uint32_t ds;
	uint32_t fs;
	uint32_t gs;
	uint32_t ldt;
	uint16_t trap;
	uint16_t iomap;
}KTSS;


/* i8259 PIC */

/* I/O ports for master and slave PIC. */

#define PIC1_CONTROL_PORT 0x20
#define PIC1_DATA_PORT    0x21
#define PIC2_CONTROL_PORT 0xa0
#define PIC2_DATA_PORT    0xa1

typedef enum _I8259_OCW1_OPERATING_MODE {
	Cascade,
	Single
}I8259_OCW1_OPERATING_MODE;

typedef enum _I8259_OCW1_INTERRUPT_MODE {
	EdgeTriggeredHw,
	LevelTriggeredHw
}I8259_OCW1_INTERRUPT_MODE;

typedef enum _I8259_OCW1_INTERVAL {
	Interval8,
	Interval4
}I8259_OCW1_INTERVAL;

typedef enum _I8259_OCW4_MODE {
	Mcs8086Mode,
	New8086Mode
}I8259_OCW4_MODE;

typedef enum _I8259_OCW4_EOI_MODE {
	NormalEoi,
	AutoEoi
}I8259_OCW4_EOI_MODE;

typedef enum _I8259_OCW4_BUFFERED_MODE {
	NonBuffered,
	NonBuffered2,
	BufferedSlave,
	BufferedMaster
}I8259_OCW4_BUFFERED_MODE;

typedef enum _I8259_READ_REQ {
	InvalidRequest,
	InvalidRequest2,
	Idr,
	Isr
}I8259_READ_REQ;

typedef enum _I8259_EOI_MODE {
	RotateAutoEoiClear,
	NonSpecificEoi,
	InvalidEoiMode,
	SpecificEoi,
	RotateAutoEoiSet,
	RotateNonSpecific,
	SetPriority,
	RotateSpecific
}I8259_EOI_MODE;

typedef union _I8259_ICW1 {
	struct {
		uint8_t needIcw4               : 1;
		uint8_t operatingMode          : 1;
		uint8_t interval               : 1;
		uint8_t interruptMode          : 1;
		uint8_t init                   : 1;
		uint8_t interruptVectorAddress : 3;
	};
	uint8_t data;
}I8259_ICW1;

typedef union _I8259_ICW2 {
	struct {
		uint8_t sbz             : 3;
		uint8_t interruptVector : 5;
	};
	uint8_t data;
}I8259_ICW2;

typedef union _I8259_ICW3 {
	union{
		struct{
			uint8_t slaveIrq0 : 1;
			uint8_t slaveIrq1 : 1;
			uint8_t slaveIrq2 : 1;
			uint8_t slaveIrq3 : 1;
			uint8_t slaveIrq4 : 1;
			uint8_t slaveIrq5 : 1;
			uint8_t slaveIrq6 : 1;
			uint8_t slaveIrq7 : 1;
		};
		struct{
			uint8_t slaveId   : 3;
			uint8_t reserved  : 5;
		};
	};
	uint8_t data;
}I8259_ICW3;

typedef struct _I8259_ICW4 {
	struct {
		uint8_t mode            : 1;
		uint8_t eoiMode         : 1;
		uint8_t bufferedMode    : 2;
		uint8_t fullyNestedMode : 1;
		uint8_t reserved        : 3;
	};
	uint8_t data;
}I8259_ICW4;

typedef union _I8259_OCW2 {
	struct {
		uint8_t irqNumber : 3;
		uint8_t sbz       : 2;
		uint8_t eoiMode   : 3;
	};
	uint8_t data;
}I8259_OCW2;

typedef union _I8259_OCW3 {
	struct {
		uint8_t readReq         : 2;
		uint8_t pollCommand     : 1;
		uint8_t sbo             : 1;
		uint8_t sbz             : 1;
		uint8_t specialMaskMode : 2;
		uint8_t reserved        : 1;
	};
	uint8_t data;
}I8259_OCW3;

#define EISA_ELCR_MASTER 0x4d0
#define EISA_ELCR_SLAVE  0x4d1

typedef union _EISA_ELCR {
	struct{
		struct{
			uint8_t irq0level : 1;
			uint8_t irq1level : 1;
			uint8_t irq2level : 1;
			uint8_t irq3level : 1;
			uint8_t irq4level : 1;
			uint8_t irq5level : 1;
			uint8_t irq6level : 1;
			uint8_t irq7level : 1;
		}master;
		struct{
			uint8_t irq8level  : 1;
			uint8_t irq9level  : 1;
			uint8_t irq10level : 1;
			uint8_t irq11level : 1;
			uint8_t irq12level : 1;
			uint8_t irq13level : 1;
			uint8_t irq14level : 1;
			uint8_t irq15level : 1;
		}slave;
	};
	uint8_t data;
};

/* ACPI definitions ***************************/

/* Root System Description Pointer */
typedef struct _RSDP {
	char signature[8]; /* "RSD PTR " */
	uint8_t checksum;
	char oemid[6];
	uint8_t revision;
	uint32_t rsdtAddr;
	/* only in version 2. */
	uint32_t length;
	uint64_t xsdtAddr;
	uint8_t extendedChecksum;
	uint8_t reserved[3];
}RSDP;

#define SDT_SIGNATURE_LENGTH 4

/* System Description Table Header */
typedef struct _SDT_HEADER {
	char signature[SDT_SIGNATURE_LENGTH];
	uint32_t length;
	uint8_t revision;
	uint8_t checksum;
	char oemid[6];
	char oemTableId[8];
	uint32_t oemRevision;
	uint32_t creatorId;
	uint32_t creatorRevision;
}SDT_HEADER;

/* ACPI defines 35 signatures. */
#define MAX_RSDT 35

/* Root System Descriptor Table */
typedef struct _RSDT {
	SDT_HEADER header;
	PHYSICAL_ADDRESS sdtPointers[MAX_RSDT];
}RSDT;

/* Address space types. */
typedef enum _ADDRESS_SPACE {
	SYSTEM_MEMORY = 0,
	SYSTEM_IO = 1,
	PCI_CONFIG = 2,
	EMBEDDED_CONTROLLER = 3,
	SMBUS = 4,
	FUNCTIONAL_FIXED_HARDWARE = 0x7f
}ADDRESS_SPACE;

/* Generic Address Structure. */
typedef struct _GENERIC_ADDRESS {
	uint8_t addressSpace;
	uint8_t bitWidth;
	uint8_t bitOffset;
	uint8_t addressize;
	uint64_t address;
}GENERIC_ADDRESS;

/* Fixed ACPI Description Table */
typedef struct _FADT {
	SDT_HEADER header;
	uint32_t firmwareControl;
	uint32_t dsdt;
	uint8_t model; /* reserved, acpi 1 only. */
	uint8_t preferredPMProfile;
	uint16_t sciInterrupt;
	uint32_t smiCommandPort;
	uint8_t acpiEnable;
	uint8_t acpiDisable;
	uint8_t s4biosReq;
	uint8_t pstateControl;
	uint32_t pm1aEventBlock;
	uint32_t pm1bEventBlock;
	uint32_t pm1aControlBlock;
	uint32_t pm1bControlBlock;
	uint32_t pm2ControlBlock;
	uint32_t pmTimerBlock;
	uint32_t gpe0Block;
	uint32_t gpe1Block;
	uint8_t pm1EventLength;
	uint8_t pm1ControlLength;
	uint8_t pm2ControlLength;
	uint8_t pmTimerLength;
	uint8_t gpe0BlockLength;
	uint8_t gpe1BlockLength;
	uint8_t gpe1Base;
	uint8_t cStateControl;
	uint16_t worstC2Latency;
	uint16_t worstC3Latency;
	uint16_t flushSize;
	uint16_t slushStride;
	uint8_t dutyOffset;
	uint8_t dutyWidth;
	uint8_t dayAlarm;
	uint8_t monthAlarm;
	uint8_t century;
	uint16_t bootArchitectureFlags;
	uint8_t reserved1;
	uint32_t flags;
	GENERIC_ADDRESS resetReg;
	uint8_t resetValue;
	uint8_t reserved2[3];
	uint64_t xafcs;
	uint64_t xdsdt;
	GENERIC_ADDRESS xpm1aEventBlock;
	GENERIC_ADDRESS xpm1bEventBlock;
	GENERIC_ADDRESS xpm1aControlBlock;
	GENERIC_ADDRESS xpm1bControlBlock;
	GENERIC_ADDRESS xpm2ControlBlock;
	GENERIC_ADDRESS xpmTimerBlock;
	GENERIC_ADDRESS xgpe0Block;
	GENERIC_ADDRESS xgpe1Block;
}FADT;

/* only flag in MADT. Indicates system
also has dual 8259 PIC. */
#define MADT_PCAT_COMPAT 1

typedef struct _MADT {
	SDT_HEADER header;
	uint32_t localApicAddress;
	uint32_t flags;
}MADT;

typedef enum _MADT_TYPE {
	MADT_TYPE_LAPIC = 0,
	MADT_TYPE_IOAPIC,
	MADT_TYPE_INT_SRC,
	MADT_TYPE_NMI_SRC,
	MADT_TYPE_LAPIC_NMI,
	MADT_TYPE_LAPIC_ADDRESS,
	MADT_TYPE_IOSAPIC,
	MADT_TYPE_LSAPIC,
	MADT_TYPE_PLATFORM_INT_SRC,
	MADT_TYPE_LX2APIC
}MADT_TYPE;

typedef struct _MADT_ENTRY_HEADER {
	uint8_t type;
	uint8_t length;
}MADT_ENTRY_HEADER;

typedef struct _MADT_LAPIC {
	uint8_t cpuId;
	uint8_t apicId;
	uint32_t flags;
}MADT_LAPIC;

typedef struct _MADT_IOAPIC {
	uint8_t id;
	uint8_t reserved;
	uint32_t address;
	uint32_t globalIntBase;
}MADT_IOAPIC;

typedef struct _MADT_INT_SRC {
	uint8_t bus;
	uint8_t busInt;
	uint32_t globalInt;
	uint16_t flags;
}MADT_INT_SRC;

typedef struct _MADT_NMI {
	uint16_t flags;
	uint32_t globalInt;
}MADT_NMI;

typedef struct _MADT_ENTRY {
	MADT_ENTRY_HEADER header;
	union {
		MADT_LAPIC   lapic;
		MADT_IOAPIC  ioapic;
		MADT_INT_SRC intSrc;
		MADT_NMI     nmi;
	};
}MADT_ENTRY;


/*** PUBLIC Definitions ************************************/

PUBLIC
INLINE
addr_t
HalAddressOfReturnAddress(void) {

	return (addr_t) _AddressOfReturnAddress();
}

PUBLIC
INLINE
void
HalInt80(void) {

	__asm int 0x80
}

PUBLIC
INLINE
void
HalWritePort(IN uint16_t port,
			 IN uint8_t data) {

	__outbyte(port,data);
}

PUBLIC
INLINE
void
HalWritePortString(IN uint16_t port,
				  IN uint8_t* buffer,
				  IN unsigned long count) {

	__outbytestring(port,buffer,count);
}

PUBLIC
INLINE
uint8_t
HalReadPort(IN uint16_t port) {

	return __inbyte(port);
}

PUBLIC
INLINE
void
HalReadPortString(IN uint16_t port,
				  IN uint8_t* buffer,
				  IN unsigned long count) {

	__inbytestring(port,buffer,count);
}

PUBLIC
INLINE
void
HalCpuid(IN int cpuInfo[4],
		IN int function) {

	__cpuid(cpuInfo,function);
}

PUBLIC
INLINE
void
HalCpuidEx(IN int cpuInfo[4],
		  IN int function,
		  IN int subfunction) {

	__cpuidex(cpuInfo,function,subfunction);
}

PUBLIC
INLINE
void
HalWriteMsr(IN uint32_t reg,
		   IN uint64_t value) {

	__writemsr(reg,value);
}

PUBLIC
INLINE
uint64_t
HalReadMsr(IN uint32_t reg) {

	return (uint64_t) __readmsr(reg);
}

PUBLIC
INLINE
void
HalFlushTlb(IN void* address) {

	__invlpg(address);
}

PUBLIC
INLINE
void
HalWritePdbr(IN void* address) {

	__writecr3(address);
}

PUBLIC
INLINE
void
HalEnablePaging (void) {

	__writecr0(__readcr0() | 0x80000000);
}

PUBLIC
INLINE
void*
HalGetPageFaultAddress(void) {

	return (void*) __readcr2();
}

extern
PUBLIC
void
HalSetContext (IN addr_t kstack);

extern
PUBLIC
void
HalBuildContext(IN addr_t base,
				IN addr_t entryPoint,
				IN BOOL user);

extern
PUBLIC
void
HalEndOfInterrupt (void);

extern
PUBLIC
void
HalRegisterInterruptReq (IN uint32_t req,
						 IN addr_t   callback);

/*
Should be able to have this at NEXEC level for
handling shared irq's.

extern
PUBLIC
void
HalChainInterruptReq (IN uint32_t req);
*/

extern
PUBLIC
void
HalYieldCpu (void);

extern
PUBLIC
void
HalEnableCpu (IN uint32_t cpu);

extern
PUBLIC
void
HalInitialize (void);

extern
PUBLIC
void
HalInstallGdt(void);

#endif
