;***********************************************************************
;
;  i386.asm - i386 back end code.
;
;   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
;
;***********************************************************************

bits 32
section .text

;*** Public Definitions ***********************************

extern _DbgExceptionHandler
extern _HalApEntryPoint
extern _HalExceptionHandler

;=== Exceptions ====================================

;
; Processor Generated Exceptions
;
global _i386DivideByZero
global _i386DebugException
global _i386NMIException
global _i386Breakpoint
global _i386Overflow
global _i386BoundException
global _i386InvalidOpcode
global _i386FPUNotAvailable
global _i386DoubleFault
global _i386CoprocessorSegment
global _i386InvalidTss
global _i386SegmentNotPresent
global _i386StackException
global _i386GeneralProtectionFault
global _i386PageFault
global _i386CoprocessorError
global _i386AlignmentCheck
global _i386MachineCheck

;
; Exception entry point
;
_i386ExceptionEntry:
	;
	; Call exception handler.
	;
	pushad
	push dword esp
	push dword 0
	call _HalExceptionHandler
	add esp, 8
	popad
	;
	; ExceptionWithErrorCode and ExceptionWithoutErrorCode
	; push an error code and vector number on stack, so
	; we pop them now.
	;
	add esp, 8
	iret

;
; Calls exception handler. It is
; assumed CPU already pushed error code.
;
%macro ExceptionWithErrorCode 1
	push dword %1
	jmp _i386ExceptionEntry
%endmacro

;
; Calls exception handler with
; a NULL error code.
;
%macro ExceptionWithoutErrorCode 1
	push dword 0
	push dword %1
	jmp _i386ExceptionEntry
%endmacro

;
; Exceptions.
;
_i386DivideByZero:           ExceptionWithoutErrorCode(0)
_i386DebugException:         ExceptionWithoutErrorCode(1)
_i386NMIException:           ExceptionWithoutErrorCode(2)
_i386Breakpoint:             ExceptionWithoutErrorCode(3)
_i386Overflow:               ExceptionWithoutErrorCode(4)
_i386BoundException:         ExceptionWithoutErrorCode(5)
_i386InvalidOpcode:          ExceptionWithoutErrorCode(6)
_i386FPUNotAvailable:        ExceptionWithoutErrorCode(7)
_i386DoubleFault:            ExceptionWithErrorCode(8)
_i386CoprocessorSegment:     ExceptionWithoutErrorCode(9)
_i386InvalidTss:             ExceptionWithErrorCode(10)
_i386SegmentNotPresent:      ExceptionWithErrorCode(11)
_i386StackException:         ExceptionWithErrorCode(12)
_i386GeneralProtectionFault: ExceptionWithErrorCode(13)
_i386PageFault:	             ExceptionWithErrorCode(14)
_i386CoprocessorError:       ExceptionWithoutErrorCode(15)
_i386AlignmentCheck:         ExceptionWithoutErrorCode(16)
_i386MachineCheck:           ExceptionWithoutErrorCode(17)

;=== AP Entry Point ================================

global _i386ApEntryPoint
global _i386ApEntryPointEnd

;
; AP data area block. Note if this is used before
; the call to ExRemoveIdentitySpace, then apInitialPdbr
; can just be the current cr3. apGdtBase and apIdtBase
; though must be < 1 MB.
;
struc ApDataArea
	.m_apGdtBase     resd 1
	.m_apIdtBase     resd 1
	.m_apInitialPdbr resd 1
endstruc
%define AP_DATA 0x500

;
; Application Processor Entry Point
;
bits 16
align 4096
_i386ApEntryPoint:
	;
	; disable interrupts and set ds
	;
	cli
	mov ax, cs
	mov ds, ax
	;
	; load gdt and idt
	;
	lgdt [AP_DATA + ApDataArea.m_apGdtBase]
	lidt [AP_DATA + ApDataArea.m_apIdtBase]
	;
	; enter protected mode
	;
	mov eax, cr0
	or eax, 1
	mov cr0, eax
bits 32
	jmp 8:.fix_cs
.fix_cs
	;
	; set up paging
	;
	mov eax, [AP_DATA + ApDataArea.m_apInitialPdbr]
	mov cr3, eax
	mov eax, cr0
	or eax, 0x80000000
	mov cr0, eax
	;
	; call nexec in kernel space
	;
	call _HalApEntryPoint
.halt:
	cli
	hlt
	jmp .halt

_i386ApEntryPointEnd: nop
