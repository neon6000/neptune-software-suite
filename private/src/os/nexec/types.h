/************************************************************************
*
*	types.c - Basic types.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef _TYPES_H
#define _TYPES_H

/* basic types. */
typedef unsigned long long uint64_t;
typedef unsigned int       uint32_t;
typedef unsigned short     uint16_t;
typedef unsigned char      uint8_t;
typedef unsigned long long sint64_t;
typedef signed int         sint32_t;
typedef signed short       sint16_t;
typedef signed char        sint8_t;
typedef int                bool_t;
//typedef wchar_t          char16_t;
typedef bool_t             BOOL;
#define TRUE               1
#define FALSE              0

typedef __int64            int64_t;

/* derived types. */
typedef uint32_t       size_t;
typedef uint32_t       addr_t;
typedef uint32_t       index_t;
typedef void*          handle_t;
typedef unsigned int   status_t;
typedef char*          va_list;

typedef unsigned long PHYSICAL_ADDRESS;
typedef unsigned int  VIRTUAL_ADDRESS;

/* operands. */
#define IN
#define OUT
#define OPTIONAL

/* compilier support. */
#define INLINE        __inline
#define CONST         const

#ifndef NULL
#define NULL          0
#endif

/* API directives. */
#define PRIVATE       static
#define PUBLIC

/* special define for .init section functions. */
#define INIT_SECTION

#define NORETURN __declspec(noreturn)

/* debugger macros. */
#define UNIMPLEMENTED    { DbgPrintf("\nUnimplemented: %s", __FUNCTION__); }
#define ASSERT(x)        { if (x == 0) {DbgAssert (__FUNCTION__,__LINE__); } }
#define DPRINT           DbgPrintf

/* variable argument macros. */
#define	STACKITEM	          int
#define	va_size(TYPE)         ((sizeof(TYPE) + sizeof(STACKITEM) - 1) & ~(sizeof(STACKITEM) - 1))

#ifndef va_start
#define	va_start(AP, LASTARG) (AP = ((va_list)&(LASTARG) + va_size(LASTARG)))
#define va_arg(AP, TYPE)      (AP += va_size(TYPE), *((TYPE *)(AP - va_size(TYPE))))
#define va_end(AP)
#endif

#define UNREFERENCED(x) (x=x);

extern PUBLIC void DbgAssert(char* function, uint64_t line);
extern PUBLIC void ExDebugPrintf(const char* s, ...);

#endif
