/************************************************************************
*
*	sync.c - Synchronization.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This component implements NEXEC synchronization primitives. */

#include <nexec.h>

/*** PUBLIC DEFINITIONS ********************************************/

PUBLIC
void
ExInitializeSpinLock(IN KSPIN_LOCK* lock) {

	*lock = 0;
}

PUBLIC
void
ExAcquireSpinLock(IN volatile KSPIN_LOCK* lock) {

	/* try to acquire lock. */
	while (NrlInterlockedIncrement((volatile long*) lock) != 1) {

		/* wait until lock is released. */
		while (*(volatile KSPIN_LOCK*) lock & 1)
			__asm pause
	}
}

PUBLIC
void
ExReleaseSpinLock(IN volatile KSPIN_LOCK* lock) {

	/* release lock. */
	NrlInterlockedDecrement((volatile long*) lock);
}

/*** Can't debug these now. ***/

PUBLIC
void
ExAcquireMutex(IN KPROCESS* process,
			   IN KMUTEX* mutex) {

	UNREFERENCED(process);
	UNREFERENCED(mutex);

	UNIMPLEMENTED;
}

PUBLIC
void
ExReleaseMutex(IN KMUTEX* mutex) {

	UNREFERENCED(mutex);

	UNIMPLEMENTED;
}
