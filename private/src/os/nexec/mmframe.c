/************************************************************************
*
*	mmframe.c - Memory Manager - Frame Allocator
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This implements the page frame allocator. */

#include <nrtl.h>
#include <i386/hal.h>
#include <mm.h>
#include <nexec.h>
#include <mboot.h>

/* lowest physical address. */
PHYSICAL_ADDRESS _MmLowestPhysicalAddress = -1;

/* highest physical address. */
PHYSICAL_ADDRESS _MmHighestPhysicalAddress = 0;

/* number of physical pages. */
PFN_NUMBER _MmNumberOfPhysicalPages = 0;

/* number of free pages. */
PFN_NUMBER _MmNumberOfFreePages = 0;

/* PFN database ------------------ */

/* all free stacks end like this. */
#define MM_FREE_STACK_END   ((PFN_NUMBER)0xffffffff)

/* currently, the database consists of one stack of
free pages. This can be expanded later to support
many stacks. */

/* Note to support cache coloring -- We can extend
this to many free stacks, such that each stack is
for a specific cache color. */

/* the free page stack. */
PFN_NUMBER _MmFreeStackTop = MM_FREE_STACK_END;

/* database. */
PFN_NUMBER* _MmPfnDatabase[] = {

	&_MmFreeStackTop
};

/**
*	Returns lowest physical address.
*/
PHYSICAL_ADDRESS MmGetLowestPhysicalAddress (void) {

	return _MmLowestPhysicalAddress;
}

/**
*	Returns highest physical address.
*/
PHYSICAL_ADDRESS MmGetHighestPhysicalAddress (void) {

	return _MmHighestPhysicalAddress;
}

/**
*	Returns total number of physical pages.
*/
PFN_NUMBER MmGetNumberOfPhysicalPages (void) {

	return _MmNumberOfPhysicalPages;
}

/**
*	Returns total number of free physical pages.
*/
PFN_NUMBER MmGetNumberOfFreePhysicalPages (void) {

	return _MmNumberOfFreePages;
}

/**
*	Returns a free frame.
*/
PFN_NUMBER MmGetFreeFrame (void) {

	/* no more memory! */
	if (_MmFreeStackTop == MM_FREE_STACK_END) {

		DbgPrintf("\nMmGetFreeFrame : No more frames");
		DbgException(NULL);
		return 0;
	}

	/* get it from free stack. */
	return _MmFreeStackTop;
}

/**
*	Allocates a frame that has been mapped.
*	Note that "page" must not have been written to yet.
*/
BOOL MmAllocateFrame (IN VIRTUAL_ADDRESS page) {

	PFN_NUMBER pfn;

	/* get page frame number. */
	pfn = *(PHYSICAL_ADDRESS*)page;

	DbgPrintf("\nMmAllocateFrame top = %x next = %x", _MmFreeStackTop, pfn);

	/* make sure we are not out of memory. */
	if (pfn == MM_FREE_STACK_END) {

		/* this is fatal! */
		DbgPrintf("\n*** MmAllocateFrame: Out of memory! page = %x stack top = %x",
			page,_MmFreeStackTop);
		DbgPrintf("\n---> free pages = %u", _MmNumberOfFreePages);
		return FALSE;
	}

	/* allocate it. */
	_MmNumberOfFreePages--;
	_MmFreeStackTop = pfn;

	/* clear it and return success. */
	*(PHYSICAL_ADDRESS*)page = 0;
	return TRUE;
}

/**
*	Places frame back on free stack.
*	"virt" must be mapped to "frame."
*/
void MmFreeFrame(IN VIRTUAL_ADDRESS virt, IN PFN_NUMBER frame) {

	/* add it back to free stack. */
	_MmNumberOfFreePages++;
	*(PHYSICAL_ADDRESS*) virt = _MmFreeStackTop;
	_MmFreeStackTop = frame;
}

/**
*	Adds free page to the PFN database.
*	This must be called with paging disabled.
*/
INIT_SECTION void MmAddFreePage (IN PHYSICAL_ADDRESS frame) {

//	DbgPrintf("\nFrame (%x) -> %x", MM_MAKE_PFN(frame), _MmFreeStackTop);

	/* add it to free stack. */
	*(PHYSICAL_ADDRESS*)frame = _MmFreeStackTop;
	_MmFreeStackTop = MM_MAKE_PFN (frame);
	_MmNumberOfFreePages++;
}

/**
*	Builds the page frame database.
*	This must be called with paging disabled.
*/
INIT_SECTION void MmBuildPfnDatabase (IN MULTIBOOT_INFO* lpb) {

	PHYSICAL_ADDRESS address;

	/* no free pages yet. */
	_MmNumberOfFreePages = 0;

	/* scan addresses. */
	for (address = 0; address < _MmHighestPhysicalAddress; address += MM_PAGE_SIZE) {

		/* Ignore non memory. */
		if (! MmIsMemory (lpb, address))
			continue;

		/* Memory below 2MB are reserved for the kernel
		and boot loader data structures. */
		if (address < 0x200000)
			continue;

		/* Add free frame. */
		MmAddFreePage(address);
	}

	/* Log it. */
	DbgPrintf("\nFree Page Stack = %x", _MmFreeStackTop);
}

/**
*	Tests if address is in RAM.
*/
INIT_SECTION BOOL MmIsMemory (IN MULTIBOOT_INFO* lpb, IN PHYSICAL_ADDRESS base) {

	uint8_t* memory;
	MULTIBOOT_MMAP* mmap;
	MULTIBOOT_MMAP* descriptor;
	size_t sizeInBytes;
	size_t errorCount;

	mmap = (MULTIBOOT_MMAP*) lpb->mmapAddr;
	sizeInBytes = lpb->mmapLength;
	errorCount = 0;
	memory = (uint8_t*) mmap;

	/* scan memory map. */
	while (memory < (uint8_t*)mmap + sizeInBytes) {

		/* this should not happen. */
		if (++errorCount == 30)
			break;

		/* prepare to go to next entry. */
		descriptor = (MULTIBOOT_MMAP*) memory;
		memory += descriptor->size;

		/* If this is not free memory, skip over it. */
		if (descriptor->type != 1)
			continue;

		/* test if in this block. */
		if (base >= descriptor->addr && base < descriptor->addr + descriptor->length) {
			return TRUE;
		}
	}

	return FALSE;
}

/**
*	Scans memory map. This sets min and max limits and
*	validates the intrigity of the provided map.
*/
INIT_SECTION void MmScanSystemMemoryMap (IN MULTIBOOT_INFO* lpb) {

	uint8_t* memory;
	MULTIBOOT_MMAP* mmap;
	MULTIBOOT_MMAP* descriptor;
	size_t sizeInBytes;
	size_t errorCount;

	mmap = (MULTIBOOT_MMAP*) lpb->mmapAddr;
	sizeInBytes = lpb->mmapLength;
	errorCount = 0;
	memory = (uint8_t*) mmap;

	/* scan memory map. */
	while (memory < (uint8_t*)mmap + sizeInBytes) {

		/* this should not happen. */
		if (++errorCount == 30) {
			DbgPrintf("\n*** MmScanSystemMemoryMap > 30, ignoring rest.");
			break;
		}

		/* prepare to go to next entry. */
		descriptor = (MULTIBOOT_MMAP*) memory;
		memory += descriptor->size;

		/* If this is not free memory, skip over it. */
		if (descriptor->type != 1)
			continue;

		/* Keep track of smallest physical address. */
		if (descriptor->addr < _MmLowestPhysicalAddress)
			_MmLowestPhysicalAddress = descriptor->addr;

		/* Keep track of highest physical address. */
		if (descriptor->addr + descriptor->length > _MmHighestPhysicalAddress)
			_MmHighestPhysicalAddress = descriptor->addr + descriptor->length;

		/* These are free pages. */
		_MmNumberOfFreePages += descriptor->length / PAGE_SIZE;
		_MmNumberOfPhysicalPages += descriptor->length / PAGE_SIZE;
	}
}
