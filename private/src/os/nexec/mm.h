/************************************************************************
*
*	mm.h - Memory Manager Definitions
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef MM_H
#define MM_H

#include <slab.h>

/* Start of System Pool. */
#define MM_POOL_START                 0xd0000000

/* System Pool is 64k. */
#define MM_POOL_END                   0xd0000000 + 0x10000

#define MM_KERNEL_PHYSICAL             0x100000

/* User space START. */
#define MM_USER_SPACE                  0x00000000

/* Kernel space START. */
#define MM_KERNEL_SPACE                0xc0000000
#define MM_KERNEL_BASE                 MM_KERNEL_SPACE

/* SUBJECT TO CHANGE. End of KERNEL. */
#define MM_KERNEL_END                  MM_KERNEL_SPACE + 0x10000

/* Page Frame Number Database. */
#define MM_PFN_DATABASE_BASE           MM_KERNEL_END

/* small page size. */
#define MM_PAGE_SIZE                   0x1000

/* virtual address of page directory. */
#define MM_PAGE_DIRECTORY_BASE         0xfffff000
#define MM_PAGE_DIRECTORY_END          0xffffffff

/* virtual address for accessing page tables. */
#define MM_PAGE_TABLE_BASE             0xffc00000
#define MM_PAGE_TABLE_END              0xffffffff

/* virtual address for accessing kernel page table. */
#define MM_KERNEL_PAGE_TABLE_BASE      0xfff00000
#define MM_KERNEL_PAGE_TABLE_END       0xfff00fff

/* extracts information from virtual address. */
#define MM_PAGE_DIRECTORY_INDEX(x)     (((x) >> 22) & 0x3ff)
#define MM_PAGE_TABLE_INDEX(x)         (((x) >> 12) & 0x3ff)
#define MM_PAGE_OFFSET(x)              ((x) & 0x3ff)

/* converts address into page frame number. */
#define MM_MAKE_PFN(x)                 ((x) >> 12)

/* converts page frame number into an address. */
#define MM_FROM_PFN(x)                 ((x) << 12)

/* size in bytes of a page table in memory. */
#define MM_PAGE_TABLE_SIZE             MM_PAGE_SIZE

/* given a virtual address, get a pointer to its page table (if it exists.) */
#define MM_GET_PAGE_TABLE(addr)        ((MM_PAGE_TABLE_BASE) + (MM_PAGE_TABLE_SIZE * MM_PAGE_DIRECTORY_INDEX((addr))))

/* mm types. */

typedef unsigned int  PFN;

/* defines MMPTE and MMPDE and a few others. */

#include <i386/hal.h>

/*** PUBLIC Definitions ****************************************/

PUBLIC
void*
ExMapIoSpace(IN addr_t physicalAddress,
			 IN size_t byteCount,
			 IN uint32_t cacheType);

PUBLIC
void
ExUnmapIoSpace(IN void* baseAddress,
			   IN size_t byteCount);

#endif


#if 0

Physical Memory Allocation

| Zero Page    |  16K - 1MB  |  1MB - 4GB |
+-----------+  +----------+  +------------+
| ZONE_ZERO |  | ZONE_DMA |  | ZONE_HMA   |
|     next -+->|    next -+->|      next -+--> Null
+-----------+  +----------+  +------------+
               |             |
			   +-> Buddy     +-> Free Stack

Virtual Memory Allocation

                 +----------------+
                 | KADDRESS_SPACE |
				 |    KVAD root   |
				 +----------------+
				 |
				 +-> KVAD AVL tree

Kernel Heap Allocation

				+-------------------+
				| Kernel Heap Pool  |
				+-------------------+
				|
				+-> Slab Allocator

ExAlloc
ExFree

**********************************************************

             +====================+
=============[ User Space         ]=========
             +====================+
             |                    |
0x100000     |      Process       |
             |                    |
             +-- --- --- --- -- --+
Varies       |  Stacks and Pool   |
             +====================+
=============[ Kernel Space       ]=========
             +====================+
             |                    |
0xc0000000   |     NEXEC.EXE      |
             |                    |
             +--------------------+
0xd0000000   |    PFN Database    |
             | +--------------------+
             | | PFN Allocation Map |
             | +--------------------+
             +--------------------+
             |                    |
             |    Kernel pool     |
             |                    |
             +--------------------+
             |                    |
0xffb00000   |  Interrupt Table   |
             |                    |
             +--------------------+
0xffc00000   |    Page Tables     |
             | +----------------+ |
0xfffff000   | |   Directory    | |  PT[1023] = Directory
             | +----------------+ |  
0xffffffff   +--------------------+

#endif
