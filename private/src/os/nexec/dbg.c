/************************************************************************
*
*	dbg.c - Neptune Executive Runtime Debug Services
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nrtl.h>
#include <i386/hal.h>

#define COM1 0x3f8

// temporary -- called from i386.asm
void DbgExceptionHandler(void) {

}

PRIVATE int DbgSerialReceived (void) {

	return HalReadPort(COM1 + 5) & 1;
}

PRIVATE int DbgSerialTransmitEmpty(void) {

	return HalReadPort(COM1 + 5) & 0x20;
}

PRIVATE char DbgReadSerial(void) {

	while (!DbgSerialReceived())
		;
	return HalReadPort(COM1);
}

PRIVATE void DbgWriteSerial(IN char c) {

	while (!DbgSerialTransmitEmpty())
		;
	HalWritePort(COM1,c);
}

PRIVATE void DbgWriteString(IN char* c) {

	size_t i;
	for (i = 0; i < NrlStringLength(c); i++)
		DbgWriteSerial(c[i]);
}

PRIVATE void DbgReadString(IN char* c, IN size_t length) {

	size_t i;
	for (i = 0; i < length; i++)
		c[i] = DbgReadSerial();
}

PUBLIC void DbgSerialInitialize (void) {

	HalWritePort(COM1 + 1, 0);
	HalWritePort(COM1 + 3, 0x80);
	HalWritePort(COM1 + 0, 3);
	HalWritePort(COM1 + 1, 0);
	HalWritePort(COM1 + 3, 3);
	HalWritePort(COM1 + 2, 0xc7);
	HalWritePort(COM1 + 4, 0x0b);
}

PUBLIC void DbgPrintf(IN char* format, ...) {

	char buffer[80];
	va_list args;

	NrlZeroMemory(buffer,80);

	va_start (args, format);
	NrlWriteVaListToString (buffer,80,format,args);
	va_end (args);

	DbgWriteString(buffer);
}

PUBLIC void DbgAssert(char* function, uint64_t line) {

	DbgPrintf ("\n*** Assertion failed in %s line %u", function, line);
}


/* Neptune Debugger Back End Services -------------------------- */


/* For compatibility, NDBG uses same packet interface as GDB. */

static const char _hexchars [] = "0123456789abcdef";

/* test if hex digit. */
static int hex(unsigned char c) {
	if (c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	if (c >= '0' && c <= '9')
		return c - '0';
	if (c >= 'A' && c <= 'F')
		return c - 'A' + 10;
	return -1;
}

/* scans sequence "$<data>#<checksum>" */
PRIVATE void DbgGetPacket (OUT char* memory, IN size_t size) {

	char c;
	unsigned char checksum;
	unsigned char xmitcsum;
	size_t count;
	index_t i;

	do {

		/* reads characters until start of packet. */
		while ((c = DbgReadSerial() & 0x7f) != '$')
			;

		checksum = 0;
		xmitcsum = -1;
		count = 0;

		/* read until '#' or end of buffer. */
		while (count < size) {
			c = DbgReadSerial() & 0x7f;
			if (c == '#')
				break;
			checksum += c;
			memory [count] = c;
			count++;
		}

		/* ignore this packet if it exceeds buffer size. */
		if (count >= size)
			continue;

		/* null terminate. */
		memory[count] = NULL;

		// "#<checksum>"
		if (c == '#') {

			xmitcsum = hex(DbgReadSerial() & 0x7f) << 4;
			xmitcsum |= hex(DbgReadSerial() & 0x7f);

			if (checksum != xmitcsum) {
				DbgWriteSerial('-'); /* failed. */
			}
			else {
				DbgWriteSerial('+'); /* success. */

				/* if sequence character is present, reply with
				sequence id. */
				if (memory[2] == ':') {

					/* send reply. */
					DbgWriteSerial(memory[0]);
					DbgWriteSerial(memory[1]);

					/* remove sequence characters. */
					count = NrlStringLength (memory);
					for (i = 3; i <= count; i++)
						memory [i-3] = memory[i];
				}
			}
		}
	}while (checksum != xmitcsum);
}

/* send packet "$<data>#<checksum>". */
PRIVATE void DbgPutPacket (IN char* memory) {

	unsigned char checksum;
	size_t count;
	char c;

	do {

		DbgWriteSerial('$');

		checksum = 0;
		count = 0;

		/* send data. */
		while ((c = memory[c]) != 0) {
			DbgWriteSerial(c);
			checksum += c;
			count++;
		}

		/* send checksum. */
		DbgWriteSerial('#');
		DbgWriteSerial(_hexchars[checksum >> 4]);
		DbgWriteSerial(_hexchars[checksum & 0xf]);

	/* wait for response so we know NDBG got packet. */
	}while ((DbgReadSerial() & 0x7f) != '+');
}


/* NDBG Services -------------------------------------------- */


/* READ byte at some location in the address space. */
PRIVATE BOOL DbgReadByte (unsigned char* address, unsigned char* destination) {

	*destination = *address;
	return TRUE;
}

/* WRITE byte to some location in the address space. */
PRIVATE BOOL DbgWriteByte (unsigned value, unsigned* destination) {

	*destination = value;
	return TRUE;
}

/* convert memory pointed by "memory" into a hex string.
Returns pointer to the last character put into result.
*/
PRIVATE uint8_t* DbgConvertMemoryToHex(IN unsigned char* memory, IN OUT char* result,
									   IN size_t count) {

	unsigned char c;

	while (count-- > 0) {

		if (! DbgReadByte(memory++, &c))
			return NULL;
		*result++ = _hexchars [c >> 4];
		*result++ = _hexchars [c & 0xf];
	}
	*result = NULL;
	return result;
}

/* convert hex array pointed by "memory" into binary to be placed in "result".
*/
PRIVATE uint8_t* DbgConvertHexToMemory(IN unsigned char* memory, IN OUT unsigned char* result,
									   IN size_t count) {

	unsigned char c;
	index_t i;

	for (i = 0; i < count; i++) {

		c = hex(*memory++) << 4;
		c |= hex(*memory++);
		if (!DbgWriteByte(c, result++))
			return NULL;
	}

	return result;
}

/* Register Context. */
typedef struct _NDBGR {
	uint32_t eip;
}NDBGR;

/* Set breakpoint instruction for single stepping. */
PRIVATE void DbgSingleStep(IN NDBGR* regs) {


}

/* process packet. */
PRIVATE void DbgProcessPacket (IN NDBGR* regs, IN unsigned char* packet,
							   OUT unsigned char* memory) {

	switch(packet[0]) {

		case '?':
			/* signal. */
			break;
		case 'd':
			/* toggle debug flag. */
			break;
		case 'g':
			/* get CPU registers. */
			break;
		case 'G':
			/* set CPU registers. */
			break;
		case 'm':
			/* mAA..AA,LLLL: read LLLL bytes at address AA..AA */
			break;
		case 'M':
			/* MAA..AA,LLLL: write LLLL bytes at address AA.AA */
			break;
		case 'c':
			/* cAA..AA: Continue at address AA..AA */
			break;
		case 'k':
			/* kill process. */
			break;
		case 'r':
			/* Reset machine. */
			break;
		case 's':
			/* Step to next instruction. */
			break;
		case 'b':
			/* bBB: Set baud rate. */
			break;
	};
}

/* This buffer is used to communicate with NDBG. */
#define DBG_BUFMAX 1024
PRIVATE unsigned char _DbgExceptionOutputBuffer [DBG_BUFMAX];
PRIVATE unsigned char _DbgExceptionInputBuffer [DBG_BUFMAX];

/* Process exception. */
PUBLIC void DbgException(IN NDBGR* regs) {

	char* memory;

	memory = _DbgExceptionOutputBuffer;

	DbgPrintf("\n*** EXCEPTION");

	/* Step 1: Send trap type. */

	/* Step 2: Send Program Counter. */

	/* Step 3: Send Frame Pointer. */

	/* Step 4: Send Stack Pointer. */

	DbgPrintf("\nWaiting for NDBG response...");

	/* Wait for NDBG response. */
	while (TRUE) {

		/* NULL terminate. */
		memory[0] = NULL;

		/* get packet. */
		DbgGetPacket(_DbgExceptionInputBuffer, DBG_BUFMAX);

		/* process it. */
		DbgProcessPacket (regs, _DbgExceptionInputBuffer, _DbgExceptionOutputBuffer);

		/* send packet. */
		DbgPutPacket(_DbgExceptionOutputBuffer);
	}
}

/* Generate breakpoint exception. This issues int 3
which calls DbgException. */
PRIVATE DbgBreakpoint(void) {

	__debugbreak();
}
