/************************************************************************
*
*	task.c - Executive Task Services
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "task.h"


/*** PRIVATE Definitions ************************************/


/* Pointer to currently running thread. */
KTHREAD* _ExCurrentThread = NULL;


/*** PUBLIC DEFINITIONS *************************************/

PUBLIC KTHREAD* ExGetCurrentThread(void) {

	return _ExCurrentThread;
}

PUBLIC status_t ExSetCurrentTask(IN int tid) {

	UNIMPLEMENTED;
	return 0;
}

PUBLIC status_t ExTaskSwitch (void) {

	/* set user address space. For now, ignore this. */
	return 0;
}

PUBLIC status_t ExTaskBlock(IN int reason) {

	UNIMPLEMENTED;
	return 0;
}

PUBLIC status_t ExTaskUnblock(IN int reason) {

	UNIMPLEMENTED;
	return 0;
}

/* Should thread creation stuff go in here? */

PUBLIC status_t ExTaskCreateThread(IN size_t stackSize,
				   IN addr_t entry,
				   IN OPTIONAL void* param,
				   OUT OPTIONAL uint32_t* tid) {

	UNIMPLEMENTED;
	return 0;
}

PUBLIC status_t ExTaskCreateRemoteThread(IN uint32_t pid,
						 IN size_t stackSize,
						 IN addr_t entry,
						 IN OPTIONAL void* param,
						 OUT OPTIONAL uint32_t* tid) {

	UNIMPLEMENTED;
	return 0;
}

PUBLIC status_t ExTaskTerminateThread(IN uint32_t tid,
					  IN int reason) {

	UNIMPLEMENTED;
	return 0;
}
