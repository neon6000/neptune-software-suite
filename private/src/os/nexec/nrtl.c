/************************************************************************
*
*	nrtl.c - Neptune RunTime Library.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This implements common runtime services for NEXEC. */

#include <nrtl.h>


/*** PRIVATE DEFINITIONS ********************************************/

/**
*	Qualifier types.
*/
typedef enum _QUALIFIER {
	QUALIFIER_NONE        = 0,
	QUALIFIER_SHORT       = 1,
	QUALIFIER_LONG        = 2,
	QUALIFIER_LONG_DOUBLE = 3,
	QUALIFIER_LONG_LONG   = 4
}QUALIFIER;

/**
*	Given a pointer to a string, the number to convert and
*	its base, whether it is signed and how to display it:
*
*	Convert "num" into a string, and write it to "str".
*	Return the number of bytes written.
*/
PRIVATE size_t _NrlParseNumber(IN char* str,IN uint64_t num,
							IN int base, IN BOOL notsigned,
							IN BOOL uppercase,
							IN int pad) {

	char* lower = "0123456789abcdefghijklmnopqrstuvwxyz";
	char* upper = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char* digits;
	char sign;
	index_t i;
	size_t result;
	char temp[64];

	if (uppercase)
		digits = upper;
	else
		digits = lower;

	sign = 0;

	/* is this number signed? */
	if (!notsigned) {
		if ((int64_t) num < 0) {
			sign = '-';
			num = -num;
		}
	}

	i = 0;
	if (num == 0)
		temp [i++] = '0';
	else{
		while (num) {
			temp[i++] = digits[num % base];
			num = num / base;
		}
	}

	result = i;
	if (sign) {
		*str++ = sign;
		result++;
	}

	if (base == 8) {
		*str++ = '0';
		result++;
	}
	else if (base == 16) {
		*str++ = '0';
		*str++ = digits [33];
		result+=2;
	}

	while (i-- > 0)
		*str++ = temp[i];

	return result;
}


/*** PUBLIC DEFINITIONS ********************************************/

PUBLIC long NrlInterlockedIncrement(IN OUT long volatile* addend) {

	return _InterlockedIncrement(addend);
}

PUBLIC long NrlInterlockedDecrement(IN OUT long volatile* addend) {

	return _InterlockedDecrement(addend);
}

PUBLIC long NrlInterlockedExchange(IN OUT long volatile* target, IN long value) {

	return _InterlockedExchange(target,value);
}

PUBLIC long NrlInterlockedExchangeAdd(IN OUT long volatile* addend, IN long value) {

	return _InterlockedExchangeAdd(addend,value);
}

PUBLIC long NrlInterlockedCompareExchange(IN OUT long volatile* destination,
										  IN long exchange,
										  IN long compareand) {

	return _InterlockedCompareExchange(destination,exchange,compareand);
}

PUBLIC int NrlWriteVaListToString(OUT char* buffer, IN size_t size,
								   IN char* format, va_list args) {

	int len;
	int i;
	int base;
	char *str;
	char *s;
	uint64_t num;
	QUALIFIER qualifier;
	BOOL uppercase;
	BOOL notsigned;

	for (str = buffer; *format; format++) {

		if (*format != '%') {
			*str++ = *format;
			continue;
		}

		/* default base. */
		base = 10;

		/* default lowercase. */
		uppercase = FALSE;

		/* default is unsigned. */
		notsigned = TRUE;

		/* default no qualifier. */
		qualifier = QUALIFIER_NONE;

		/* skip over '%' */
		format++;

		switch (*format) {
			case 'h':
				qualifier = QUALIFIER_SHORT;
				format++;
				break;
			case 'l':
				qualifier = QUALIFIER_LONG;
				format++;
				break;
			case 'L':
				qualifier = QUALIFIER_LONG_DOUBLE;
				format++;
				break;
		};
		switch (*format) {
			case 'l':
				qualifier = QUALIFIER_LONG_LONG;
				format++;
				break;
		};

		switch (*format) {

			case 'c':
				*str++ = (unsigned char) va_arg(args,int);
				continue;

			case 's':
				s = va_arg(args, char*);
				if (!s)
					s = "<null>";
				len = NrlStringLength(s);
				for (i = 0; i < len; ++i)
					*str++ = *s++;
				continue;

			case 'p':
				continue;

			case 'n':
				if (qualifier == QUALIFIER_LONG) {
					long* pointer = va_arg(args, long*);
					*pointer = (str - buffer);
				}else{
					int* pointer = va_arg(args, int*);
					*pointer = (str - buffer);
				}
				continue;

			case 'A':
				uppercase = TRUE;
			case 'a':
				str++;
				continue;

			case 'o':
				base = 8;
				break;

			case 'X':
				uppercase = TRUE;
			case 'x':
				base = 16;
				break;

			case 'd':
			case 'i':
				notsigned = FALSE;
				break;
			case 'u':
				notsigned = TRUE;
				break;

			default:
				if (*format != '%')
					*str++ = '%';
				if (*format)
					*str++ = *format;
				else
					--format;
				continue;
		}

		if (qualifier == QUALIFIER_LONG) {
			if (notsigned)
				num = va_arg(args,long);
			else
				num = va_arg(args,unsigned long);
		}
		else if (qualifier == QUALIFIER_SHORT) {
			if (notsigned)
				num = va_arg(args,short);
			else
				num = va_arg(args,unsigned short);
		}
		else if (qualifier == QUALIFIER_LONG_LONG) {
			if (notsigned)
				num = va_arg(args,long long);
			else
				num = va_arg(args,unsigned long long);
		}
		else {
			if (notsigned)
				num = 0xffffffff & va_arg(args,int);
			else
				num = 0xffffffff & va_arg(args,unsigned int);
		}

		str += _NrlParseNumber(str, num, base, notsigned, uppercase, 0);
	}

	return str - buffer;
}

PUBLIC void NrlSPrintf(IN char* s, IN size_t length, IN char* format, OUT ...) {

	va_list args;
	int result;

	va_start (args, format);
	result = NrlWriteVaListToString (s,length,format,args);
	va_end (args);

	return result;
}
