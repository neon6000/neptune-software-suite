/************************************************************************
*
*	slab.h - Slab Allocator.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This component implements the slab allocator. */

#ifndef _SLAB_H
#define _SLAB_H

#include <nexec.h>

/* experimental since we want to extend this to MP. */
/* ref: http://www.parrot.org/sites/www.parrot.org/files/vmem.pdf */

typedef struct _KMAGAZINE {
	void* next;
	void* round[1];
}KMAGAZINE;

typedef struct _KCPUCACHE {
	size_t     rounds;
	size_t     magsize;
	KMAGAZINE* loaded;
	KMAGAZINE* previous;
	void*      magRound[10];
	KSPIN_LOCK lock;
}KCPUCACHE;

/* object initialization callbacks. */
typedef int (*KCACHE_INITIALIZE) (void*, void*, int);
typedef int (*KCACHE_RELEASE)    (void*, void*);
typedef int (*KCACHE_RECLAIM)    (void*, void*, int);

/* Executive Cache Object. */
typedef struct _KCACHE {
	char*             name;
	size_t            size;
	int               align;
	int               flags;
	KCACHE_INITIALIZE initialize;
	KCACHE_RELEASE    destroy;
	KCACHE_RECLAIM    reclaim;
	size_t            pageSize;
	void*             privateData;
	int               color;
	int               colorOffset;
	KSPIN_LOCK        lock;
	LIST_ENTRY        fullSlabListHead;
	LIST_ENTRY        partialSlabListHead;
	LIST_ENTRY        emptySlabListHead;
	LIST_ENTRY        listEntry;
	KCPUCACHE         cpuCache[4];
}KCACHE;

/* Executive Slab Object. */
typedef struct _KSLAB {
	uint32_t        magic;
	KCACHE*         cache;
	LIST_ENTRY      listEntry;
	size_t          objectCount;
	size_t          usedObjects;
	size_t          bufferSize;
	union {
		/* small objects. */
		void*         freeList;
		/* large objects. */
		LIST_ENTRY    bufferControlFreeListHead;
	}u;
}KSLAB;

PUBLIC INIT_SECTION void MmInitializeGlobalCacheList (void);

PUBLIC void* MmAllocateCache (IN KCACHE* cache, IN uint32_t flags);

PUBLIC void MmFreeCache (IN KCACHE* cache, IN void* p);

PUBLIC status_t MmReleaseCache(IN KCACHE* cache);

PUBLIC KCACHE* MmCreateCache(IN OPTIONAL char* name,IN size_t size,
							 IN int align,
							 IN OPTIONAL KCACHE_INITIALIZE init);

PUBLIC status_t MmCacheReap(IN KCACHE* cache);

PUBLIC status_t MmCacheGrow(IN KCACHE* cache);

PUBLIC void* MmAlloc(IN size_t size);

PUBLIC void MmFree (IN void* memory);

PUBLIC void MmDumpCache(IN KCACHE* cache);

#endif
