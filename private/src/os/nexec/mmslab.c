/************************************************************************
*
*	mmslab.c - Memory Manager - Slab Allocator.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This component implements the slab allocator. */

#include <i386/hal.h>
#include <slab.h>

#if 0

SMALL slabs are defined as 1/8'th the size of a page.
They have the following format.

+----------------------------------------------------------------------------------+
| PAGE_SIZE memory block.                                                          |
|+======================+======================+===+======================+=======+|
|| BUFFER_1             | BUFFER_2             |   | BUFFER_N             | KSLAB ||
||+--------+----------+ |+--------+----------+ |   |+--------+----------+ |       ||
||| OBJECT | KBUFLINK | || OBJECT | KBUFLINK | |...|| OBJECT | KBUFLINK | |       ||
||+--------+----------+ |+--------+----------+ |   |+--------+----------+ |       ||
|+======================+======================+===+====================+=========+|
+-----------------------+--------------------------+-------------------------------+

#endif

/*** PRIVATE DECLARATIONS *********************************/

/* Buffer Controller Object Free List link. Only used with
small slabs. */
typedef struct _KBUFLINK {

	uint32_t   magic;
	void*      next;

}KBUFLINK;

/* Buffer Controller Object. Only used with large slabs. */
typedef struct _KBUFCTRL {

	void*             buffer;
	struct KSLAB*     parent;
	LIST_ENTRY        entry;

}KBUFCTRL;

/* Global cache list. */
LIST_ENTRY _ExCacheListHead;

/* Locks global cache list. */
KSPIN_LOCK _ExCacheListLock;

/* Cache of KCACHE objects. */
KCACHE     _ExCacheCache;

/* Cache of KSLAB objects. */
KCACHE     _ExSlabCache;

/* Cache of KBUFCTRL objects. */
KCACHE     _ExBufctrlCache;

/* General caches. */
#define    EX_GENERAL_CACHE_MAX 4
KCACHE     _ExGeneralCache[EX_GENERAL_CACHE_MAX];

#define ROUNDSIZE	             0x1000
#define ROUNDUP(s)	             (((s) + ROUNDSIZE - 1) & ~(ROUNDSIZE - 1))

//#define BUFFER_SIZE(object_size) (object_size + sizeof(addr_t))
#define BUFFER_SIZE(object_size) ((object_size) + sizeof(KBUFLINK))

#define SLAB_MAGIC               0x42414c53  /* "SLAB" */

#define KBUF_MAGIC               0x4655424b  /* "KBUF" */


/*** PRIVATE DEFINITIONS **********************************/

/**
*	Allocates a page from system pool memory.
*/
PUBLIC void* MmVirtualAlloc() {

	return MmAllocPage();
}

/**
*	Frees a frame from system pool memory.
*/
PUBLIC status_t MmVirtualFree(IN void* p) {

	MmFreePage (p);
	return STAT_SUCCESS;
}

/**
*	Initializes a cache object.
*/
PRIVATE KCACHE* MmInitializeCache(IN KCACHE* cache, IN OPTIONAL char* name,
				  IN size_t size, IN int align,
				  IN OPTIONAL KCACHE_INITIALIZE init) {

	ASSERT(size >= sizeof (KBUFLINK));

	/* initialize. */
	ExInitializeSpinLock (&cache->lock);
	cache->flags       = 0;
	cache->privateData = NULL;
	cache->name        = name;
	cache->size        = size;
	cache->align       = ROUNDUP (align);
	cache->pageSize    = 0x1000;
	cache->initialize  = init;
	NrlInitializeListHead (&cache->fullSlabListHead);
	NrlInitializeListHead (&cache->emptySlabListHead);
	NrlInitializeListHead (&cache->partialSlabListHead);
	NrlInitializeListHead (&cache->listEntry);

	/* add to global cache list. */
	NrlInsertTailList(&_ExCacheListHead,&cache->listEntry);

	/* return cache object. */
	return cache;
}

/**
*	Release a slab. This places it back
*	into the free list of the cache for which
*	it belongs.
*/
PRIVATE status_t MmReleaseSlab(IN KSLAB* slab) {

	KCACHE* cache;

	ASSERT(slab != NULL);

	cache = slab->cache;

	/* small slab buffers. */
	if (cache->size < cache->pageSize / 8) {

		/* should we align this down? */
		uint8_t* p = (uint8_t*) slab->u.freeList;
		index_t  i;

		if (cache->destroy) {
			for (i = 0; i < slab->objectCount; i++) {
				cache->destroy(p, cache->privateData);
				p += slab->bufferSize + sizeof (KBUFLINK);
			}
		}

	/* large slab buffers. */
	}else{

		KBUFCTRL*   bufctrl;
		LIST_ENTRY* entry;

		for (entry = slab->u.bufferControlFreeListHead.next;
			entry != &slab->u.bufferControlFreeListHead;
			entry = entry->next) {

			bufctrl = NRL_CONTAINING_RECORD(entry,KBUFCTRL,entry);
			if (!bufctrl) {
				DbgPrintf ("\n*** ExReleaseSlab invalid bufctrl, skipping");
				continue;
			}

			if(cache->destroy)
				cache->destroy(bufctrl->buffer,cache->privateData);

			/* TODO: Note that the SYSTEM knows how many bytes to free.
			We should probably store this in BUFCTRL for paging system. */



			MmVirtualFree (bufctrl->buffer);
		}
	}
	return STAT_SUCCESS;
}

/**
*	Allocates a buffer for an object within a slab.
*/
PRIVATE void* MmAllocateBuffer(IN KSLAB* slab) {

	void* buffer   = NULL;
	KBUFLINK* link = NULL;
	KCACHE* cache  = NULL;

	ASSERT (slab != NULL);
	cache = slab->cache;

	/* small objects. */
	if (cache->size < cache->pageSize / 8) {

		/* get first free buffer. */
		buffer = slab->u.freeList;
		if (! buffer)
			return NULL;

		/* go to next free buffer. */
		link = (KBUFLINK*) ((uint8_t*)slab->u.freeList + cache->size);
		if (link->magic != KBUF_MAGIC) {
			DbgPrintf ("\n\r*** ExAllocateBuffer : Corruption Detected Around %x", link);
		}

		/* go to next free buffer. */
		slab->u.freeList = link->next;
	}

	/* large objects. */
	else {

		/* get next bufctrl from free list. */
		KBUFCTRL* bufctrl = NRL_CONTAINING_RECORD(&slab->u.bufferControlFreeListHead,KBUFCTRL,entry);
		if (bufctrl) {

			/* we are going to use this buffer, so remove it from free list. */
			NrlRemoveEntryList(&bufctrl->entry);
			buffer = bufctrl->buffer;
		}
	}

	return buffer;
}


/*** PUBLIC DEFINITIONS ************************************/


/**
*	Releases memory allocated to unused slabs
*	in the cache back to the system.
*/
PUBLIC status_t MmCacheReap(IN KCACHE* cache) {

	KSLAB* slab;
	LIST_ENTRY* entry;

	/* release unused slabs. */
	for (entry = cache->emptySlabListHead.next;
		entry != &cache->emptySlabListHead;
		entry = entry->next) {

		slab = (KSLAB*) NRL_CONTAINING_RECORD(entry,KSLAB,listEntry);
		MmReleaseSlab(slab);
	}

	return STAT_SUCCESS;
}

/**
*	Attempts to grow a cache. This allocates
*	a new slab from the system and initializes
*	the objects within it for the cache.
*/
PUBLIC status_t MmCacheGrow(IN KCACHE* cache) {

	/* small slab. */
	if (cache->size < cache->pageSize / 8) {
	
		uint8_t*  memory;
		KSLAB*    slab;
		uint8_t*  buffer;
		index_t   i;
		KBUFLINK* link;

		/* get new page. */
		memory = (uint8_t*) MmVirtualAlloc();
		if (!memory)
			return STAT_OUT_OF_MEMORY;

		/* the slab descriptor is stored at end of page. */
		slab = (KSLAB*) ((memory + cache->pageSize) - sizeof (KSLAB));

		/* initialize. */
		slab->cache = cache;
		slab->magic = SLAB_MAGIC;
		slab->usedObjects = 0;
		slab->bufferSize  = BUFFER_SIZE(cache->size);
		slab->objectCount = (cache->pageSize - sizeof(KSLAB)) / slab->bufferSize;

		/* initialize object free list. */
		slab->u.freeList = memory;
		buffer = memory;
		for (i = 0; i < slab->objectCount - 1; i++) {

			/* initialize object. */
			if (cache->initialize)
				cache->initialize(buffer,cache->privateData,cache->size);

			/* create free list. */
			link = (KBUFLINK*) (buffer + cache->size);
			link->magic = KBUF_MAGIC;
			link->next = (KBUFLINK*) (buffer + slab->bufferSize);

			/* go to next buffer. */
			buffer += slab->bufferSize;
		}

		/* last entry in list points to NULL. */
		link = (KBUFLINK*) (buffer + cache->size);
		link->magic = KBUF_MAGIC;
		link->next = NULL;

		/* add it to empty list. */
		NrlInsertTailList(&cache->emptySlabListHead, &slab->listEntry);

	/* large slab. */
	}else{

		KSLAB*    slab;
		KBUFCTRL* bufctrl;
		uint8_t*  memory;
		index_t   bufcount;

		/* for now. */
		UNIMPLEMENTED;

		/* get new slab. */
		slab = (KSLAB*) MmAllocateCache(&_ExSlabCache,0);
		if (!slab)
			return STAT_OUT_OF_MEMORY;

		/* this should be rounded up to 2^n size. */
		memory = (uint8_t*) MmVirtualAlloc();
		if (!memory) {
			MmFreeCache(&_ExSlabCache, slab);
			return STAT_OUT_OF_MEMORY;
		}

		/* initialize. */
		slab->cache = cache;
		slab->magic = SLAB_MAGIC;
		slab->usedObjects = 0;

		slab->bufferSize  = cache->size;
		slab->objectCount = slab->bufferSize;

		/* init bufctrl's. */
		for (bufcount = 0; bufcount < slab->objectCount; bufcount++) {

			/* allocate bufctrl. */
			bufctrl = (KBUFCTRL*) MmAllocateCache(&_ExBufctrlCache,0);
			if (bufctrl) {

				/* initialize. */
				bufctrl->buffer = memory;
				bufctrl->parent = slab;
				NrlInitializeListHead(&bufctrl->entry);

				/* add it to bufctrl list. */
				NrlInsertTailList(&slab->u.bufferControlFreeListHead, &bufctrl->entry);
			}
		}
	}

	return STAT_SUCCESS;
}

/**
*	Initialize global cache list.
*/
PUBLIC INIT_SECTION void MmInitializeGlobalCacheList (void) {

	/* initialize global cache list. */
	NrlInitializeListHead (&_ExCacheListHead);
	ExInitializeSpinLock  (&_ExCacheListLock);

	/* initialize cache of cache objects. */
	MmInitializeCache     (&_ExCacheCache,"ExCacheCache",sizeof(KCACHE),0x1000,NULL);

	/* create general caches. */
	MmInitializeCache     (&_ExGeneralCache[0], "ExGenCache0",8, 0x1000,NULL);
	MmInitializeCache     (&_ExGeneralCache[1], "ExGenCache1",16,0x1000,NULL);
	MmInitializeCache     (&_ExGeneralCache[2], "ExGenCache2",32,0x1000,NULL);
	MmInitializeCache     (&_ExGeneralCache[3], "ExGenCache3",64,0x1000,NULL);
}

/**
*	Release cache object.
*/
PUBLIC status_t MmReleaseCache(IN KCACHE* cache) {

	ASSERT(cache != NULL);

	/* test if cache buffers still allocated. */
	if (! (NrlIsListEmpty(&cache->fullSlabListHead) || NrlIsListEmpty(&cache->partialSlabListHead))) {

		/* log it and ignore request. */
		DbgPrintf("\n*** ExReleaseCache: Not all slabs have been released!");
		return 0;
	}

	/* release slabs from cache. */
	MmCacheReap (cache);

	/* remove it from global cache list. */
	NrlRemoveEntryList(&cache->listEntry);

	/* and release cache memory. */
	MmFreeCache (&_ExCacheCache, cache);
	return 0;
}

/**
*	Create new cache.
*/
PUBLIC KCACHE* MmCreateCache(IN OPTIONAL char* name, IN size_t size,
							 IN int align, IN OPTIONAL KCACHE_INITIALIZE init) {

	KCACHE* cache;

	ASSERT(size >= sizeof (addr_t));

	/* allocate cache object. */
	cache = (KCACHE*) MmAllocateCache(&_ExCacheCache,0);
	if (!cache) {

		DbgPrintf("\n***ExCreateCache : ExAllocateCache fail");
		return NULL;
	}

	/* return cache object. */
	return MmInitializeCache (cache, name, size, align, init);
}

/**
*	Allocate a new buffer from a cache.
*/
PUBLIC void* MmAllocateCache (IN KCACHE* cache, IN uint32_t flags) {

	LIST_ENTRY* entry;
	KSLAB* slab;
	void*  buffer;

	ASSERT (cache != NULL);

	slab  = NULL;
	entry = cache->partialSlabListHead.next;

	/* try partial list. */
	if (entry == &cache->partialSlabListHead) {

		/* try free list. */
		entry = cache->emptySlabListHead.next;
		if (entry == &cache->emptySlabListHead) {

			/* allocate a new page for free slabs. */
			if (! MmCacheGrow(cache))
				return NULL;
		}

		/* add it to partial list. */
		entry = NrlRemoveTailList (&cache->emptySlabListHead);
		NrlInsertTailList (&cache->partialSlabListHead, entry);
	}

	/* get slab from partial list. Its at tail of list. */
	entry = cache->partialSlabListHead.prev;

	/* get slab. */
	slab = NRL_CONTAINING_RECORD(entry,KSLAB,listEntry);

	/* watch for null pointer. */
	if (!slab)
		return NULL;

	/* allocate buffer from slab. */
	buffer = MmAllocateBuffer(slab);

	/* add one more  slab. */
	++slab->usedObjects;

	/* if partial list is now full, move it to full list. */
	if (slab->usedObjects == slab->objectCount) {
		NrlRemoveEntryList(entry);
		NrlInsertTailList (&cache->fullSlabListHead, entry);
	}

	DbgPrintf("\nMmAllocCache returning : %x", buffer);

	/* we are done. */
	return buffer;
}

/**
*	Dump slab information to debugger.
*/
PUBLIC void MmDumpSlab(IN KSLAB* slab) {

	if (!slab) {
		DbgPrintf("\n\t\t*** Slab invalid");
		return;
	}
	DbgPrintf ("\n\t\tAddr %x", slab);
	DbgPrintf ("\n\t\tMagic %x", slab->magic);
	if (slab->magic == SLAB_MAGIC)
		DbgPrintf (" (valid)");
	else
		DbgPrintf (" *** (invalid) ***");
	DbgPrintf ("\n\t\tCache %x", slab->cache);
	if (slab->cache)
		DbgPrintf (" (%s)", slab->cache->name);
	DbgPrintf ("\n\t\tObject Count %i", slab->objectCount);
	DbgPrintf ("\n\t\tAllocated Objects %i", slab->usedObjects);
	DbgPrintf ("\n\t\tObject Buffer Size %i", slab->bufferSize);
	DbgPrintf ("\n\t\tKBUFCTRL %x", slab->u.freeList);
}

/**
*	Dump cache object to debugger.
*/
PUBLIC void MmDumpCache(IN KCACHE* cache) {

	LIST_ENTRY* entry;
	KSLAB*      slab;
	index_t     i;

	DbgPrintf("\nCache %s",              cache->name);
	DbgPrintf("\n\tObject Size %i",      cache->size);
	DbgPrintf("\n\tObject Alignment %i", cache->align);
	DbgPrintf("\n\tFlags %x",            cache->flags);
	DbgPrintf("\n\tInitialize: %x",      cache->initialize);
	DbgPrintf("\n\tPage Size: %x",       cache->pageSize);
	DbgPrintf("\n\tPrivate Data: %x",    cache->privateData);
	DbgPrintf("\n\tLock: %x",            cache->lock);

	DbgPrintf("\n\tEmpty slabs:");

	i=0;
	for (entry = cache->emptySlabListHead.next;
		entry != &cache->emptySlabListHead;
		entry = entry->next) {

			DbgPrintf("\n\tSlab %i", i++);
			slab = (KSLAB*) NRL_CONTAINING_RECORD(entry,KSLAB,listEntry);
			MmDumpSlab(slab);
	}

	DbgPrintf("\n\tPartial slabs:");

	i=0;
	for (entry = cache->partialSlabListHead.next;
		entry != &cache->partialSlabListHead;
		entry = entry->next) {

			DbgPrintf("\n\tSlab %i", i++);
			slab = (KSLAB*) NRL_CONTAINING_RECORD(entry,KSLAB,listEntry);
			MmDumpSlab(slab);
	}

	DbgPrintf("\n\tFull slabs:");

	i=0;
	for (entry = cache->fullSlabListHead.next;
		entry != &cache->fullSlabListHead;
		entry = entry->next) {

			DbgPrintf("\n\tSlab %i", i++);
			slab = (KSLAB*) NRL_CONTAINING_RECORD(entry,KSLAB,listEntry);
			MmDumpSlab(slab);
	}
	DbgPrintf("\n<done>\n");
}

/**
*	Release a buffer back to the cache.
*/
PUBLIC void MmFreeCache (IN KCACHE* cache, IN void* p) {

	KSLAB* slab;

	ASSERT (cache != NULL);
	ASSERT (p != NULL);

	/* small objects. */
	if (cache->size < cache->pageSize / 8) {

		KBUFLINK* link;

		/* slab is at end of page boundary. */
		slab = (KSLAB*) ROUNDUP((addr_t) p+1);
		slab--;

		/* validate slab. */
		if (slab->magic != SLAB_MAGIC) {

			DbgPrintf ("\n*** MmFreeCache : Corruption detected in slab at %x", slab);
			return;
		}

		/* get bufctrl link and make sure its valid. */
		link       = (KBUFLINK*) ((uint8_t*)p + cache->size);
		if (link->magic != KBUF_MAGIC) {

			DbgPrintf ("\n*** MmFreeCache : Buffer overflow detected from pointer %x", p);
		}

		/* add it to buffer free list. */
		link->next = (KBUFLINK*) slab->u.freeList;
		slab->u.freeList = (void*) p;
	}

	/* large objects. */
	else{

		KBUFCTRL* bufctrl;

		/* for now, until TODO is fixed. */
		UNIMPLEMENTED;

		/* TODO: need to convert buffer to bufctrl object. */
		bufctrl = NULL;
		slab    = (KSLAB*) bufctrl->parent;

		/* add it to list. */
		NrlInsertTailList(&slab->u.bufferControlFreeListHead, &bufctrl->entry);
		return;
	}

	/* if this slab was full, move it to partial. */
	if (slab->usedObjects == slab->objectCount) {

		NrlRemoveEntryList(&slab->listEntry);
		NrlInsertTailList(&cache->partialSlabListHead,&slab->listEntry);
	}

	/* one less object. */
	--slab->usedObjects;

	/* if this slab is now empty, move it to empty list. */
	if (slab->usedObjects == 0) {

		NrlRemoveEntryList(&slab->listEntry);
		NrlInsertTailList(&cache->emptySlabListHead,&slab->listEntry);
	}
}

/**
*	Allocate an object from a general cache.
*/
PUBLIC void* MmAlloc(IN size_t size) {

	KCACHE* cache = NULL;
	index_t i;

	/* find smallest cache allocation size can fit. */
	for (i = 0; i < EX_GENERAL_CACHE_MAX; i++) {
		if (_ExGeneralCache[i].size >= size) {
			cache = &_ExGeneralCache[i];
			break;
		}
	}

	if (!cache)
		return NULL;

	return MmAllocateCache(cache, 0);
}

/**
*	Releases a buffer back to the general cache.
*/
PUBLIC void MmFree (IN void* memory) {

	KSLAB*  slab  = NULL;

	ASSERT(memory != NULL);

	/* assuming small object, slab is at end of page boundary. */
	slab = (KSLAB*) ROUNDUP((addr_t) memory+1);
	slab--;

	if (slab->magic != SLAB_MAGIC) {

		DbgPrintf ("\n*** ExFree: Invalid SLAB_MAGIC!");
		return;
	}

	MmFreeCache(slab->cache, memory);
}
