/************************************************************************
*
*	main.c - Neptune Executive Entry Point
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This component implements main program. */

#include <types.h>
#include <mboot.h>
#include <nexec.h>
#include <mm.h>

/* kernel mode stack (temporary!). */

VIRTUAL_ADDRESS _kernel_esp;
VIRTUAL_ADDRESS _kernel_stack_base;

/**
*	Test if address is in kernel stack space.
*/
PUBLIC BOOL MmIsKernelStackAddress(IN VIRTUAL_ADDRESS address) {

	if (address >= _kernel_stack_base || address <= _kernel_stack_base + 8096)
		return TRUE;
	return FALSE;
}

/**
*	Allocates a kernel mode stack.
*	Currently used by NEXEC. We will be expanding
*	this to support any process.
*/
PUBLIC void MmBuildKernelStack(IN KPROCESS* process) {

	MMPTE* pte;
	PFN_NUMBER frame;

	/* two pages: one for kernel stack space, and a guard page. */
	pte = MmReserveSystemPtes(2);
	if (!pte)
		return 0;

	/* only map one page, first is guard page. */
	MmMapPte(pte+1,MmGetFreeFrame(),MM_PAGE_READWRITE);
	MmAllocateFrame(MmPteToAddress(pte+1));
	_kernel_esp = MmPteToAddress(pte+1);
	_kernel_stack_base = _kernel_esp;
	_kernel_esp += 4096;
}

/* freeze system with known good magic value for debugging. */
PRIVATE void ExFreeze(void) {

	_asm {
		mov eax, 0xabcdabcd
		cli
		hlt
	}
}

extern void __run_test_after_paging(void);

PUBLIC void ExDumpSystemMemoryDescriptor(IN MULTIBOOT_MMAP* descriptor) {

	DbgPrintf("\n %llx - %llx (%llu Bytes) Type (%i)", descriptor->addr, descriptor->addr + descriptor->length,
		descriptor->length, descriptor->type);
	if (descriptor->type == 1)
		DbgPrintf(" (RAM)");
}

PUBLIC void ExDumpModuleInformation(IN PHYSICAL_ADDRESS modules, IN size_t moduleCount) {

	MULTIBOOT_MODULE* moduleList;
	size_t count;

	moduleList = (MULTIBOOT_MODULE*)modules;
	DbgPrintf("\nModule List: 0x%x Count: %i", moduleList, moduleCount);

	for (count = 0; count < moduleCount; count++) {
		DbgPrintf("\n  Module (start: 0x%x end: 0x%x cmdLine: 0x%x):",
			moduleList[count].modStart,
			moduleList[count].modEnd,
			moduleList[count].cmdLine);
//		DbgPrintf("%s", moduleList[count].modStart);
	}
}

PUBLIC void ExDumpSystemMemoryMap(IN VIRTUAL_ADDRESS mmap, IN size_t sizeInBytes) {

	uint8_t* memory;
	MULTIBOOT_MMAP* descriptor;
	size_t errorCount;

	errorCount = 0;

	DbgPrintf("\nSystem Memory Map (At %x, Size %x (%u Bytes)):", mmap, sizeInBytes,  sizeInBytes);
	memory = (uint8_t*) mmap;
	while (memory < (uint8_t*)mmap + sizeInBytes) {

		if (++errorCount == 30) {
			DbgPrintf("\nExDumpSystemMemoryMap : Invalid system memory map detected!");
			break;
		}

		descriptor = (MULTIBOOT_MMAP*) memory;
		ExDumpSystemMemoryDescriptor(descriptor);
		memory += descriptor->size;
	}
}

typedef struct _ExVbeControlInfo {
	uint8_t  signiture[4];
	uint16_t version;
	uint32_t oemStrPtr;
	uint32_t caps;
	uint32_t videoModesPtr;
	uint16_t totalMemory;
	uint16_t oemSoftwareRev;
	uint32_t oemVenderNamePtr;
	uint32_t oemProductNamePtr;
	uint32_t oemProductRevPtr;
	uint8_t  reserved[222];
	uint8_t  oemData[256];
}ExVbeControlInfo;

typedef struct _ExVbeModeInfo {
	uint16_t modeAttrib;
	uint8_t winAttribA;
	uint8_t winAttribB;
	uint16_t winGranularity;
	uint16_t winSize;
	uint16_t winSegmentA;
	uint16_t winSegmentB;
	uint32_t winFuncPtr;
	uint16_t bytesPerScanLine;
	/**
	*	vbe 1.2 or higher fields
	*/
	uint16_t resolutionX;
	uint16_t resolutionY;
	uint8_t charSizeX;
	uint8_t charSizeY;
	uint8_t numberOfPlanes;
	uint8_t bitsPerPixel;
	uint8_t numberOfBanks;
	uint8_t memoryModel;
	uint8_t bankSize;
	uint8_t numberOfImagePages;
	uint8_t reserved;
	/**
	*	direct color fields
	*/
	uint8_t maskSizeRed;
	uint8_t fieldPositionRed;
	uint8_t maskSizeGreen;
	uint8_t fieldPositionGreen;
	uint8_t maskSizeBlue;
	uint8_t fieldPositionBlue;
	uint8_t maskSizeRsvd;
	uint8_t fieldPositionRsvd;
	uint8_t directColorModeInfo;
	/**
	*	vbe 2.0 and higher fields
	*/
	uint32_t physBasePtr;
	uint32_t reserved2;
	uint16_t reserved3;
	/**
	*	vbe 3.0 and higher fields
	*/
	uint16_t linBytesPerScanLine;
	uint8_t bnkNumberOfImagePages;
	uint8_t linNumberOfImagePages;
	uint8_t linRedMaskSize;
	uint8_t linRedFieldPosition;
	uint8_t linGreenMaskSize;
	uint8_t linGreenFieldPosition;
	uint8_t linBlueMaskSize;
	uint8_t linBlueFieldPosition;
	uint8_t linRsvdMaskSize;
	uint8_t linRsvdFieldPosition;
	uint32_t maxPixelClock;
}ExVbeModeInfo;

PRIVATE void ExDumpVbeControlInfo(IN ExVbeControlInfo* info) {
	if (info) {
		DbgPrintf("\n     Signature: %c%c%c%c", info->signiture[0], info->signiture[1], info->signiture[2], info->signiture[3]);
		DbgPrintf("\n     Version: %x", info->version);
		DbgPrintf("\n     OEM: %x", info->oemStrPtr);
		DbgPrintf("\n     Capabilities: %x", info->caps);
		DbgPrintf("\n     Video modes ptr: %x", info->videoModesPtr);
		DbgPrintf("\n     Total Memory: %i", info->totalMemory);
		DbgPrintf("\n     OEM Software Rev: %x", info->oemSoftwareRev);
		DbgPrintf("\n     OEM Product Name Ptr: %x", info->oemProductNamePtr);
		DbgPrintf("\n     OEM Product Rev Ptr: %x", info->oemProductRevPtr);
	}
}

PRIVATE void ExDumpVbeModeInfo(IN ExVbeModeInfo* info) {

	DbgPrintf("\n     bytesPerScanLine: %i", info->bytesPerScanLine);
	DbgPrintf("\n     resolutionX: %i", info->resolutionX);
	DbgPrintf("\n     resolutionY: %i", info->resolutionY);
	DbgPrintf("\n     charSizeX: %i", info->charSizeX);
	DbgPrintf("\n     charSizeY: %i", info->charSizeY);
	DbgPrintf("\n     numberOfPlanes: %i", info->numberOfPlanes);
	DbgPrintf("\n     bitsPerPixel: %i", info->bitsPerPixel);
	DbgPrintf("\n     numberOfBanks: %i", info->numberOfBanks);
	DbgPrintf("\n     memoryModel: %i", info->memoryModel);
	DbgPrintf("\n     bankSize: %i", info->bankSize);
	DbgPrintf("\n     physBasePtr: %x", info->physBasePtr);
	DbgPrintf("\n     linBytesPerScanLine: %i", info->linBytesPerScanLine);
}

PRIVATE void ExDumpVideoInformation(IN MULTIBOOT_INFO* mbi) {

	DbgPrintf("\nVBE Mode: %i", mbi->vbeMode);
	DbgPrintf("\n  VBE Interface %x:%x (%i bytes)", mbi->vbeInterfaceSeg, mbi->vbeInterfaceOff, mbi->vbeInterfaceLen);
	DbgPrintf("\n  VBE Control Info: %x", mbi->vbeControlInfo);
	ExDumpVbeControlInfo(mbi->vbeControlInfo);
	DbgPrintf("\n  VBE Mode Info: %x", mbi->vbeModeInfo);
	ExDumpVbeModeInfo(mbi->vbeModeInfo);
}

PRIVATE void ExDumpMbi(IN MULTIBOOT_INFO* mbi) {
	DbgPrintf("\nTHIS IS A TEST:");
	DbgPrintf("\nLoader Paramater Block:");
	DbgPrintf("\n Flags: %x", mbi->flags);
	DbgPrintf("\n Memory Low: %x", mbi->memLower);
	DbgPrintf("\n Memory High: %x", mbi->memUpper);
	DbgPrintf("\n Boot Device: %x", mbi->bootDevice);
	DbgPrintf("\n Command Line: %x", mbi->cmdLine);
	if (mbi->cmdLine)
		DbgPrintf(" -> '%s'", mbi->cmdLine);
	DbgPrintf("\n Module Count: %u", mbi->modsCount);
	DbgPrintf("\n Modules: %x", mbi->modsAddr);
	DbgPrintf("\n Memory Map Length: %u", mbi->mmapLength);
	DbgPrintf("\n Memory Map Address: %x", mbi->mmapAddr);

	ExDumpSystemMemoryMap(mbi->mmapAddr, mbi->mmapLength);
	ExDumpModuleInformation(mbi->modsAddr, mbi->modsCount);
	ExDumpVideoInformation(mbi);
}

PRIVATE void TestATA();

NORETURN PUBLIC void ExMain (IN MULTIBOOT_INFO* lpb) {;

	/* Note that by keeping the low 1MB identity mapped
	for the initial startup, the system will fall back
	to NSTART's exception handler. We only remove identity
	space when we are ready. */

	DbgPrintf("=================== OK");

	/* Dump loader paramater block. */
	ExDumpMbi(lpb);

	/* Builds system pool. */
	MmBuildSystemPtePool(1024);
	MmDumpSystemPtes();

	/* Initialize global caches. */
	MmInitializeGlobalCacheList ();

	/* Build kernel stack. */
	MmBuildKernelStack(NULL);
	_asm{
		mov esp, _kernel_esp
	}

	/* Remove identity space. */
	MmRemoveIdentitySpace ();

	/* Install system GDT. */
	HalInstallGdt();

	/* Install system IDT. */
	HalInitializeIdt();

	/* Initialize acpi. */
	AcpiInitialize ();

	TestATA();


	/* Initialize apic. */
//	HalApicInitialize();

//	__asm int 0

	/* for now, freeze system. */
	DbgPrintf("\nTests Done");
	ExFreeze();
}


/* Subject to change -- design -------------------- */


typedef enum _KINTERRUPTMODE {
	LevelTriggered,
	EdgeTriggered
}KINTERRUPTMODE;

typedef struct _KINTERRUPT {
	void*          userContext;
	KISR           isr;
	int            vector;
	int            irql;
	KINTERRUPTMODE mode;
	BOOL           share;
	KSPIN_LOCK     lock;
	LIST_ENTRY     entry;
}KINTERRUPT;

typedef BOOL (*KISR)(IN KINTERRUPT*, IN void*);


PUBLIC void ExInterruptEnd(IN KCONTEXT* frame, BOOL spurious) {

}


PUBLIC void ExDispatchInterrupt(IN KINTERRUPT* in, IN KCONTEXT* frame) {

	/* need to make sure interrupt is not spurious. */
	/* if (not_spurious) */
	{
		ExAcquireSpinLock(&in->lock);
		in->isr (in, in->userContext);
		ExReleaseSpinLock(&in->lock);
		ExInterruptEnd(frame, FALSE);
	}
//	else
		ExInterruptEnd(frame, TRUE);
}


PUBLIC void ExConnectInterrupt(IN KINTERRUPT* in) {


}


PUBLIC void ExDisconnectInterrupt(IN KINTERRUPT* in) {

}







/* allocate/free from buddy system. */

PUBLIC void* MmAllocContiguousMemory(IN size_t size, IN PHYSICAL_ADDRESS highestAddress /* <= 16MB */ );

PUBLIC BOOL MmFreeContiguousMemory(IN void*);

/*
KERNEL Space Address Map
----------------------------------------------
0xc0000000-0xc0009fff -> 0x00100000-0x00109fff - nexec.exe
0xfff00000-0xfff00fff -> 0x003ee000-0x003eefff - Page Tables
0xfff40000-0xfff40fff -> 0x003ec000-0x003ecfff
0xfffff000-0xffffffff -> 0x003ef000-0x003effff - Page Directory
*/


/*
	ATA Test
*/

PUBLIC
INLINE
void
HalWritePortLong(IN uint16_t port,
IN unsigned long data) {

	__outdword(port, data);
}

PUBLIC
INLINE
unsigned long
HalReadPortLong(IN uint16_t port) {

	return __indword(port);
}	

#define PCI_INVALID_VENDOR_ID 0xffffffff
#define PCI_ADDRESS uint32_t
#define PCI_ENABLE 0x80000000
#define CONFIG_ADDR 0xCF8
#define CONFIG_DATA 0xCFC

//
// +---------------------------------------------------+
// |Enable|Reserved|Bus|Device|Function|Register offset|
// +---------------------------------------------------+
// 31     30       23  15     10       7
//
PRIVATE PCI_ADDRESS PciGetAddress(uint8_t bus, uint8_t device, uint8_t func) {
	return ((PCI_ADDRESS)bus << 16) | ((PCI_ADDRESS)device << 11) | ((PCI_ADDRESS)func << 8) | (PCI_ENABLE);
}

PRIVATE uint32_t PciConfigRead(IN PCI_ADDRESS address, IN uint16_t offset) {
	HalWritePortLong(CONFIG_ADDR, address | offset);
	return HalReadPortLong(CONFIG_DATA);
}

#define CLASS_MSC 1
#define SUBCLASS_MSC_IDE 1

typedef struct DEVICE {
	int cls, subcls;
	uint32_t bar0, bar1, bar2, bar3, bar4;
	BOOL needIRQ;
}DEVICE, *PDEVICE;

PUBLIC BOOL PciScanDevices(IN int cls, IN int subcls, OUT PDEVICE device) {

	uint32_t data, bus, dev,func;

	for (bus = 0; bus < 256; bus++) {
		for (dev = 0; dev < 32; dev++) {
			for (func = 0; func < 256; func++) {

				data = PciConfigRead(PciGetAddress(bus, dev, func), 0);
				if (data == PCI_INVALID_VENDOR_ID)
					continue;

				data = PciConfigRead(PciGetAddress(bus, dev, func), 8) >> 16;
				if ((data >> 8) == cls && (data & 0xff) == subcls) {

					device->cls = cls;
					device->subcls = subcls;
					device->bar0 = PciConfigRead(PciGetAddress(bus, dev, func), 0x10);
					device->bar1 = PciConfigRead(PciGetAddress(bus, dev, func), 0x14);
					device->bar2 = PciConfigRead(PciGetAddress(bus, dev, func), 0x18);
					device->bar3 = PciConfigRead(PciGetAddress(bus, dev, func), 0x1C);
					device->bar4 = PciConfigRead(PciGetAddress(bus, dev, func), 0x20);

					HalWritePortLong(CONFIG_ADDR, PciGetAddress(bus, dev, func), 0x3C);
					HalWritePort(CONFIG_DATA, 0xFE);
					if ((HalReadPortLong(CONFIG_DATA) & 0xFF) == 0xFE)
						device->needIRQ = TRUE;
					else
						device->needIRQ = FALSE;
					return TRUE;
				}
			}
		}
	}
	return FALSE;
}

PRIVATE void TestATA() {

	DEVICE dev;
	if (PciScanDevices(CLASS_MSC, SUBCLASS_MSC_IDE, &dev)) {

		DbgPrintf("\nIDE CONTROLLER %x %x %x %i", dev.bar0, dev.bar1, dev.bar2, dev.needIRQ);
		
	}
}

