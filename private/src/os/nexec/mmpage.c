/************************************************************************
*
*	mmpage.c - Memory Manager - Paging System
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <nexec.h>
#include <mm.h>


/*** PUBLIC Definitions **************************************/


/* page frame number. */
typedef unsigned long MMPFN;

/* page frame descriptor. */
typedef struct _MMPAGE {
	/* used to link together pages (like a linked list.) */
	MMPFN  next;
	MMPFN  prev;
	/* pfn for this page. */
	MMPFN  pfn;
	/* pte for this page. */
	MMPTE* pte;
}MMPAGE;

/* page frame database. */
MMPAGE* _MmPageFrameDatabase;

/* these point inside of _MmPageFrameDatabase
to the starting entry of the list. */
MMPAGE* _MmPageFrameDatabaseFreeListHead;
MMPAGE* _MmPageFrameDatabaseUsedListHead;

/* range of physical addresses. */
typedef struct _MMADDRESS_RANGE {
	MMPFN  startPfn;
	size_t pfnCount;
}MMADDRESS_RANGE;

/* physical address map. */
typedef struct _MMADDRESS_MAP {
	/* actual size of ranges array. */
	size_t rangeCount;
	/* total number of pages. */
	size_t pageCount;
	/* range descriptors. */
	MMADDRESS_RANGE ranges[1];
}MMADDRESSP_MAP;

/* BIOS memory map entry types. */
typedef enum _BIOS_MMAP_ENTRY_TYPE {
	MMAP_AVAILABLE   = 1,
	MMAP_RESERVED    = 2,
	MMAP_ACPIRECLAIM = 3,
	MMAP_ACPINVS     = 4,
}BIOS_MMAP_ENTRY_TYPE;

/* BIOS int 15h function e820h structure.
This is a system memory map entry. */
typedef struct _BIOS_MMAP_ENTRY {
	uint64_t baseAddress;
	uint64_t length;
	BIOS_MMAP_ENTRY_TYPE type;
}BIOS_MMAP_ENTRY;

/**
*	Invalidate page that was mapped to specified address.
*/
PUBLIC void MmInvalidateAddress (VIRTUAL_ADDRESS address) {

	__invlpg(address);
}

/**
*	Gets system page directory table.
*/
PUBLIC MMPDE* MmGetSystemPageDirectoryTable (void) {

	return (MMPDE*) MM_PAGE_DIRECTORY_BASE;
}

/**
*	Gets PTE mapped to a virtual address.
*/
PUBLIC MMPTE* MmAddressToPte(IN VIRTUAL_ADDRESS address) {

	MMPTE* pte = (MMPTE*) MM_GET_PAGE_TABLE(address);
	return &pte [ MM_PAGE_TABLE_INDEX(address) ];
}

/**
*	Gets PDE mapped to a virtual address.
*/
PUBLIC MMPDE* MmAddressToPde(IN VIRTUAL_ADDRESS address) {

	MMPDE* pde = (MMPDE*) MM_PAGE_DIRECTORY_BASE;
	return &pde [ MM_PAGE_DIRECTORY_INDEX(address) ];
}

/**
*	Given a PTE, get its virtual address.
*/
PUBLIC VIRTUAL_ADDRESS MmPteToAddress(IN MMPTE* pte) {

	VIRTUAL_ADDRESS result;

	if (pte < MM_PAGE_TABLE_BASE || pte > MM_PAGE_TABLE_END)
		return NULL;

	result = pte - MM_PAGE_TABLE_BASE;
	result /= sizeof (MMPTE);

	return result * MM_PAGE_SIZE;
}

/**
*	Given a PDE, get its virtual address.
*/
PUBLIC VIRTUAL_ADDRESS MmPdeToAddress(IN MMPDE* pde) {

	VIRTUAL_ADDRESS result;

	if (pde < MM_PAGE_DIRECTORY_BASE || pde > MM_PAGE_DIRECTORY_END)
		return NULL;

	result = pde - MM_PAGE_DIRECTORY_BASE;
	result /= sizeof (MMPDE);

	/* each page table has 1024 pages in address space. */
	return result * (MM_PAGE_SIZE * 1024);
}

/**
*	Given a virtual address, test if the virtual address
*	maps to a physical page.
*/
PUBLIC BOOL MmIsAddressValid(IN VIRTUAL_ADDRESS address) {

	if (MmAddressToPde(address)->u.device.valid == 0)
		return FALSE;
	if (MmAddressToPte(address)->u.device.valid == 0)
		return FALSE;
	return TRUE;
}

/**
*	Given a virtual address, return the physical address it maps
*	to. If it is not mapped, return NULL.
*/
PUBLIC PHYSICAL_ADDRESS MmGetPhysicalAddress (IN VIRTUAL_ADDRESS address) {

	PHYSICAL_ADDRESS phys;
	MMPDE*  pde;
	MMPTE*  pageTable;
	MMPTE*  pte;

	if (!MmIsAddressValid(address))
		return 0;

	pde = MmAddressToPde(address);
	pte = MmAddressToPte(address);
	phys = MM_FROM_PFN(pte->u.device.pageFrameNumber) | MM_PAGE_OFFSET(address);
	return phys;
}

/**
*	Given a physical address, return the virtual address that maps
*	to this address or NULL.
*/
PUBLIC VIRTUAL_ADDRESS MmGetVirtualAddress(IN PHYSICAL_ADDRESS address) {

	UNIMPLEMENTED;
	return 0;
}

/**
*	Unmaps IDENTITY SPACE from the system address space.
*	This should be called during startup.
*/
INIT_SECTION PUBLIC void MmRemoveIdentitySpace (void) {

	MMPDE* page_directory;
	int c;

	/* clear zero page table. */
	page_directory = (MMPDE*) MM_PAGE_DIRECTORY_BASE;
	page_directory[0].u.device.valid = 0;
	page_directory[0].u.device.pageFrameNumber = 0;
	page_directory[0].u.device.accessed = 0;

	/* invalidate first 1024 pages. */
	for(c = 0; c < 0x400000; c += MM_PAGE_SIZE)
		HalFlushTlb(c);
}

/**
*	This is responsible for creating the kernel page tables.
*/
INIT_SECTION PUBLIC void MmBuildKernelSpace (void) {

	PFN_NUMBER   KernelPageDirectory;
	PFN_NUMBER   KernelPageTable;
	PFN_NUMBER   KernelPfnCount;
	PFN_NUMBER   IdentitySpacePageTable;
	MMPTE*       PageTableEntry;
	MMPDE*       PageDirectoryEntry;
	index_t      KernelPageTableIndex;
	index_t      Index;

	/* Note we can't call MmAllocPage since we don't have
	a pool at this stage. */

	KernelPageDirectory = MmGetFreeFrame();
	MmAllocateFrame( MM_FROM_PFN (KernelPageDirectory));

	KernelPageTable = MmGetFreeFrame();
	MmAllocateFrame(MM_FROM_PFN (KernelPageTable));

	IdentitySpacePageTable = MmGetFreeFrame();
	MmAllocateFrame (MM_FROM_PFN (IdentitySpacePageTable));

	/* Page Directory. */

	PageDirectoryEntry = (MMPDE*) MM_FROM_PFN (KernelPageDirectory);

	NrlZeroMemory (PageDirectoryEntry, MM_PAGE_SIZE);

	KernelPageTableIndex = MM_PAGE_DIRECTORY_INDEX(MM_KERNEL_BASE);

	PageDirectoryEntry [0].u.device.valid = 1;
	PageDirectoryEntry [0].u.device.pageFrameNumber = IdentitySpacePageTable;

	PageDirectoryEntry [KernelPageTableIndex].u.device.valid = 1;
	PageDirectoryEntry [KernelPageTableIndex].u.device.pageFrameNumber = KernelPageTable;

	PageDirectoryEntry [1023].u.device.valid = 1;
	PageDirectoryEntry [1023].u.device.pageFrameNumber = KernelPageDirectory;

	/* Kernel page table. */

	PageTableEntry = (MMPTE*) MM_FROM_PFN(KernelPageTable);

	NrlZeroMemory (PageTableEntry, MM_PAGE_SIZE);

	KernelPfnCount = (PeGetImageSize (KERNEL_PHYSICAL) / PAGE_SIZE) + 1;

	DbgPrintf("\nKernelPfnCount : %i", KernelPfnCount);
	ASSERT (KernelPfnCount < 1023);

	for (Index = 0; Index < KernelPfnCount; Index++) {
		PageTableEntry[Index].u.device.valid = 1;
		PageTableEntry[Index].u.device.pageFrameNumber = MM_MAKE_PFN(MM_KERNEL_PHYSICAL + MM_PAGE_SIZE * Index);
	}

	/* Identity Space. */

	PageTableEntry = (MMPTE*) MM_FROM_PFN(IdentitySpacePageTable);

	NrlZeroMemory (PageTableEntry, MM_PAGE_SIZE);

	for (Index = 0; Index < 1024; Index++) {
		PageTableEntry[Index].u.device.valid = 1;
		PageTableEntry[Index].u.device.pageFrameNumber = Index;
	}

	HalWritePdbr (PageDirectoryEntry);
}

/**
*	Unmap address.
*/
PUBLIC BOOL MmUnmapAddress (IN VIRTUAL_ADDRESS address) {

	MMPTE*          pte;
	MMPDE*          pde;
	PFN_NUMBER      pageTablePfn;
	MMPTE*          pageTableEntry;
	int             page;

	/* If already not mapped, nothing to do. */
	if (! MmIsAddressValid (address))
		return TRUE;

	pde = MmAddressToPde(address);
	pte = MmAddressToPte(address);

	pte->u.device.valid = 0;
	pte->u.device.pageFrameNumber = 0;

	MmInvalidateAddress (address);

	/* If any entry in the page table is still mapped, we are done. */
	pageTableEntry = (MMPTE*) MM_GET_PAGE_TABLE (address);
	for (page = 0; page < 1024; page++)
		if (pageTableEntry [page].u.device.valid == 1)
			return TRUE;

	DbgPrintf("\nMmUnmapAddress Freeing Page Table %x (%x)", pageTableEntry,
		pde->u.device.pageFrameNumber);

	/* Unmap page table. */
	MmFreeFrame(pageTableEntry, pde->u.device.pageFrameNumber);
	pde->u.device.valid = 0;

	MmInvalidateAddress (pageTableEntry);
}

/**
*	Build paging structures needed to map an address into the address space.
*/
PUBLIC BOOL MmPrepareAddressMapping(IN VIRTUAL_ADDRESS address, MM_PROTECT protect) {

	MMPTE*          pte;
	MMPDE*          pde;
	PFN_NUMBER      pageTablePfn;
	VIRTUAL_ADDRESS pageTableAddress;

	pde = MmAddressToPde(address);
	pte = MmAddressToPte(address);

	/* Map the page table in. */
	if (pde->u.device.valid == 0) {

		DbgPrintf("\nCreating page table for %x", address);

		/* We can't call MmAllocPage here since it relies
		on MmPrepareAddressMapping. */

		pageTablePfn = MmGetFreeFrame ();
		pageTableAddress = MM_GET_PAGE_TABLE (address);

		pde->u.device.valid = 1;
		pde->u.device.pageFrameNumber = pageTablePfn;

		MmAllocateFrame (pageTableAddress);

		MmInvalidateAddress (pageTableAddress);
	}

	return TRUE;
}

/**
*	Map page table entry.
*/
PUBLIC BOOL MmMapPte(IN MMPTE* pte, IN PFN_NUMBER frame, MM_PROTECT protect) {

	MmInvalidateAddress(MmPteToAddress(pte));
	pte->u.device.pageFrameNumber = frame;
	pte->u.device.valid = 1;
	return TRUE;
}

/**
*	Unmap PTE.
*/
PUBLIC BOOL MmUnmapPte(IN MMPTE* pte) {

	pte->u.device.valid = 0;
	MmInvalidateAddress(MmPteToAddress(pte));
	return TRUE;
}

/**
*	Map frame into the address space.
*/
PUBLIC BOOL MmMapAddress(IN VIRTUAL_ADDRESS address, IN PFN_NUMBER frame, MM_PROTECT protect) {

	MMPTE*          pte;
	MMPDE*          pde;
	PFN_NUMBER      pageTablePfn;
	VIRTUAL_ADDRESS pageTableAddress;

	if (! frame)
		return FALSE;

	pde = MmAddressToPde(address);
	pte = MmAddressToPte(address);

	/* This might not be an error. */
	if (pte->u.device.valid == 1) {

		DbgPrintf("\nAttempt to remap %x from %x to %x",
			address, pte->u.device.pageFrameNumber, frame);
		return FALSE;
	}

	/* Map it in. */
	pte->u.device.pageFrameNumber = frame;
	pte->u.device.valid = 1;

	MmInvalidateAddress (address);

	return TRUE;
}

/**
*	Modifies page protection fields.
*/
PUBLIC BOOL MmProtectPages(IN VIRTUAL_ADDRESS memory, IN size_t count, IN MM_PROTECT in) {

	VIRTUAL_ADDRESS address;
	MMPTE* pte;
	MMPDE* pde;
	size_t currentPage;

	address = memory & 0xfffff000;

	for (currentPage = 0; currentPage < count; currentPage++, address += MM_PAGE_SIZE) {

		pde = MmAddressToPde(address);
		pte = MmAddressToPte(address);

		if (pde->u.device.valid == 0)
			continue;
		if (pte->u.device.valid == 0)
			continue;

		/* need to check NX bit. */

		if (in & MM_PAGE_READWRITE)
			pte->u.device.write = 1;
		if (in & MM_PAGE_READONLY)
			pte->u.device.write = 0;
		if (in & MM_PAGE_WRITECOPY)
			pte->u.device.copyOnWrite = 1;
		else
			pte->u.device.copyOnWrite = 0;
	}

	return TRUE;
}

/**
*	Queries region of pages.
*/
PUBLIC BOOL MmQueryPages(IN VIRTUAL_ADDRESS memory, IN size_t count, OUT int a) {

	/* Return protection and map information about the region of virtual addresses. */
}

/**
*	Allocates a new frame and maps it in.
*/
PUBLIC VIRTUAL_ADDRESS MmAllocPage (void) {

	MMPTE* pte;
	PFN_NUMBER frame;
	VIRTUAL_ADDRESS result;

	pte = MmReserveSystemPtes(1);
	if (!pte)
		return 0;

	frame = MmGetFreeFrame ();
	if (!frame) {
		MmReleaseSystemPtes(pte,1);
		return 0;
	}

	MmMapPte(pte,frame,MM_PAGE_READWRITE);

	result = MmPteToAddress(pte);
	MmAllocateFrame(result);
	return (void*) result;
}

/**
*	Frees the frame associated with this page
*	and unmaps it.
*/
PUBLIC BOOL MmFreePage(IN VIRTUAL_ADDRESS address) {

	MMPTE* pte;
	PFN_NUMBER frame;

	if (!MmIsAddressValid(address))
		return FALSE;

	pte = MmAddressToPte (address);

	frame = MM_MAKE_PFN(MmGetPhysicalAddress(address));

	MmFreeFrame (address,frame);

	MmUnmapPte (pte);
	MmReleaseSystemPtes (pte,1);
	return TRUE;
}

/**
*	Unmaps I/O space.
*/
PUBLIC void MmUnmapIoSpace(IN VIRTUAL_ADDRESS memory, IN size_t byteCount) {

	MMPTE* pte;
	PFN_NUMBER pfn;
	PFN_NUMBER numberOfPageFrames;

	if (!MmIsAddressValid(memory))
		return;

	pte = MmAddressToPte(memory);
	numberOfPageFrames = (byteCount / MM_PAGE_SIZE) + 1;

	MmReleaseSystemPtes(pte, numberOfPageFrames);
	while (numberOfPageFrames--)
		MmUnmapPte(pte++);
}

/**
*	Maps I/O space.
*/
PUBLIC VIRTUAL_ADDRESS MmMapIoSpace(IN PHYSICAL_ADDRESS physicalAddress, IN size_t byteCount, IN BOOL crashOnError) {

	MMPTE* pte;
	PFN_NUMBER pfn;
	PFN_NUMBER numberOfPageFrames;
	uint32_t offsetInPageFrame;
	VIRTUAL_ADDRESS memory;

	numberOfPageFrames = (byteCount / MM_PAGE_SIZE) + 1;
	offsetInPageFrame = (physicalAddress % MM_PAGE_SIZE);

	pte = MmReserveSystemPtes(numberOfPageFrames);
	if (!pte)
		goto fail;

	memory = MmPteToAddress(pte);
	pfn = MM_MAKE_PFN(physicalAddress);

	while (numberOfPageFrames--)
		MmMapPte(pte++, pfn++, MM_PAGE_READWRITE);

	return memory + offsetInPageFrame;

fail:

	if (crashOnError) {
		DbgPrintf("\n*** ExMapIoSpace (addr %x, byteCount %i pfn count %i)", physicalAddress, byteCount,numberOfPageFrames);
		DbgException(NULL);
	}
	return NULL;
}

/**
*	Process page fault exception.
*/
PUBLIC BOOL MmProcessPageFault(IN VIRTUAL_ADDRESS faultAddress, IN uint32_t code) {

	if (MmIsKernelStackAddress(faultAddress)) {
		DbgPrintf("\n*** Attempt to write to kernel stack guard page! ***");
	}

	DbgPrintf("\n*** Page Fault at %x", faultAddress);
	return FALSE;
}
