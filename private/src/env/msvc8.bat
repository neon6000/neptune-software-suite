::
::	msvc8.bat
::		Sets up Neptune Build Environment variables in MSVC command shell
::
::	Copyright (c) BrokenThorn Entertainment Co
::

@title Neptune Build Environment

@if defined NEPTUNE_ENV goto :startEnvironment
@set DBGMSG=1
@set NEPTUNE_ENV=%~dp0
@set NEPTUNE_SRC=%CD%\..\os
@set NEPTUNE_BIN=%NEPTUNE_ENV%i386\bin
@set NEPTUNE_OUT=C:\Documents and Settings\Michael\Desktop\PROJECTS\Neptune\src\os\out-i386
@set NEPTUNE_INSTALL=C:\Documents and Settings\Michael\Desktop\Neptune\ins-i386
@set CC=MSVC8

:startEnvironment:
@call %comspec% /k "C:\Program Files\Microsoft Visual Studio 8\VC\vcvarsall.bat" x86
