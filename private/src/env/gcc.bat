::
::	gcc.bat
::		Sets up Neptune Build Environment variables in GCC command shell
::
::	Copyright (c) BrokenThorn Entertainment Co
::

@title Neptune Build Environment

@if defined NEPTUNE_ENV goto :startEnvironment
@set NEPTUNE_ENV=%~dp0
rem @set NEPTUNE_ENV:~0,-1%
@set NEPTUNE_SRC=%CD%\..\os
@set NEPTUNE_BIN=%NEPTUNE_ENV%i386\bin
@set NEPTUNE_OUT=C:\Documents and Settings\Michael.PC677134193111\Desktop\Neptune\out-i386
@set NEPTUNE_INSTALL=C:\Documents and Settings\Michael.PC677134193111\Desktop\Neptune\ins-i386
@set CC=GCC

:startEnvironment:
@set INCLUDE=%NEPTUNE_ENV%i386\mingw32\include
@set LIB=%NEPTUNE_ENV%i386\mingw32\lib
@set PATH=%PATH%;"%NEPTUNE_BIN%";"%NEPTUNE_ENV%i386\mingw32\bin"
echo PATH... %PATH%
@cmd
