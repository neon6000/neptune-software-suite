#------------------------------------------------------------------
#	targets.mak
#		Targets common to all components of Neptune
#
# Copyright (c) BrokenThorn Entertainment, Co. All Rights Reserved
#------------------------------------------------------------------

!ifndef MAKE_TARGETS
MAKE_TARGETS = 1

#
#	The following is a list of supported targets.
#
#--------------------------------------------------------------------------
# Target           | Description
#--------------------------------------------------------------------------
# make install     | Installs Neptune to a location specified by NEPTUNE_INSTALL variable
#--------------------------------------------------------------------------
# make clean       | Deletes all files that were generated when building Neptune
#==========================================================================
#	Not yet supported; these might be better handled by NBuild?
#==========================================================================
# make all         | Builds all of Neptune
#--------------------------------------------------------------------------
# make module      | Builds a single module. Replace module with the name of the module to build
#--------------------------------------------------------------------------
# make bootcd      | Builds an ISO from which Neptune can be booted and installed
#--------------------------------------------------------------------------
# make livecd      | Builds an ISO from which Neptune can be booted but not installed
#==========================================================================

!ifndef NEPTUNE_MAKE
!error targets.mak should not be included directly. Include makefile.def
!endif

#--------------------------------------------
#
#	Inference Rules
#
#=============================================

#
#	C targets to OBJ
#

.c.obj:
!ifdef DBGMSG
	@echo $(CC) $(CC_FLAGS) $<
!endif
	$(CC) $(CC_FLAGS) $<

#
#	C++ targets to OBJ
#

#.cpp.obj:
#!ifdef DBGMSG
#	@echo Building... $(CC) $(CC_FLAGS) $<
#!endif
#	$(CC) $(CC_FLAGS) $<

#
#	RC targets to RES
#

.rc.res:
!ifdef DBGMSG
	@echo $(RC) $(DIR_RC)\$(@B).rc
!endif
	$(RC) $(DIR_RC)\$(@B).rc

#
#	OBJ targets to LIB
#

.obj.lib:
!ifdef DBGMSG
	@echo $(LIB) $**
!endif
	$(LIB) $**

#
#	OBJ targets to EXE
#

.obj.exe:
!ifdef DBGMSG
	@echo $(LINKC) $(LIBS) $(LINK_FLAGS) $**
!endif
	$(LINKC) $(LIBS) $(LINK_FLAGS) $**

#
#	ASM targets to OBJ
#

.asm.obj:
!ifdef DBGMSG
	@echo $(ASM) $(ASM_FLAGS)
!endif
	$(ASM) $(ASM_FLAGS) $<

#
#	ASM targets to BIN
#

.asm.bin:
!ifdef DBGMSG
	@echo $(ASM) $(ASM_FLAGS)
!endif
	$(ASM) $(ASM_FLAGS) $<

#---------------------------------------------
#
#	Object files
#
#=============================================

OBJECTS = $(SOURCES:.cpp=.obj)
OBJECTS = $(OBJECTS:.c=.obj)

!if "$(ARCH)" == "ARCH_IA32"
!ifdef I386_SOURCES
ARCH_SOURCES = $(I386_SOURCES)
!endif
!endif

!if "$(ARCH)" == "ARCH_IA64"
!ifdef IA64_SOURCES
ARCH_SOURCES = $(IA64_SOURCES)
!endif
!endif

!if "$(ARCH_SOURCES)" != ""

ARCH_OBJECTS = $(ARCH_SOURCES:.cpp=.obj)
ARCH_OBJECTS = $(ARCH_OBJECTS:.c=.obj)

!if "$(ARCH)" == "ARCH_IA32"
!ifdef I386_SOURCES
ARCH_OBJECTS = $(ARCH_OBJECTS:i386/= )
ARCH_OBJECTS = $(ARCH_OBJECTS:i386\= )
!endif
!endif

!if "$(ARCH)" == "ARCH_IA64"
!ifdef IA64_SOURCES
ARCH_OBJECTS = $(ARCH_OBJECTS:ia64/= )
ARCH_OBJECTS = $(ARCH_OBJECTS:ia64\= )
!endif
!endif

!endif

!if "$(ARCH)" == "ARCH_IA32"
!if "$(I386_ASM_SOURCES)" != ""
!ifdef I386_ASM_SOURCES
ASM_OBJECTS = $(I386_ASM_SOURCES:.asm=.obj)
!endif
!endif
!endif

!if "$(ARCH)" == "ARCH_IA64"
!if "$(IA64_ASM_SOURCES)" != ""
!ifdef IA64_ASM_SOURCES
ASM_OBJECTS = $(IA64_ASM_SOURCES:.asm=.obj)
!endif
!endif
!endif



!ifdef DBGMSG
!message OBJECTS = $(OBJECTS)
!message ARCH_OBJECTS = $(ARCH_OBJECTS)
!message ASM_OBJECTS = $(ASM_OBJECTS)
!endif


#--------------------------------------------
#
#	Targets
#
#============================================

#--------------------------------------------
#
#	Static library
#
#--------------------------------------------

!ifdef SOURCES

!if "$(TARGET)" == "LIB"

$(TARGETNAME) : $(ARCH_OBJECTS) $(OBJECTS) $(ASM_OBJECTS)
!ifdef DBGMSG
	@echo $(LIBC) $** $(LIB_OPTIONS)
!endif
!if "$(HOST_OS)" == "HOST_WINDOWS"
	@if not exist "$(NEPTUNE_OUT)" \
	@mkdir "$(NEPTUNE_OUT)"
!endif
!if "$(HOST_OS)" == "HOST_UNIX"
	@!test "$(NEPTUNE_OUT)" \
	@mkdir "$(NEPTUNE_OUT)"
!endif
	@$(LIBC) $** $(LIB_OPTIONS)
	copy $(TARGETNAME) "$(NEPTUNE_OUT)/$(TARGETNAME)"
!else

#--------------------------------------------
#
#	Linker
#
#--------------------------------------------

$(TARGETNAME) : $(ARCH_OBJECTS) $(OBJECTS) $(ASM_OBJECTS)
!ifdef DBGMSG
	@echo $(LINKC) $(LIBS) $(LINK_FLAGS) $**
!endif
!if "$(HOST_OS)" == "HOST_WINDOWS"
	@if not exist "$(NEPTUNE_OUT)" \
	@mkdir "$(NEPTUNE_OUT)"
!endif
!if "$(HOST_OS)" == "HOST_UNIX"
	@!test "$(NEPTUNE_OUT)" \
	@mkdir"$(NEPTUNE_OUT)"
!endif
	@$(LINKC) $(LIBS) $(LINK_FLAGS) $**
!endif

#--------------------------------------------
#
#	C compiler
#
#--------------------------------------------

#
#	Note: ARCH_SOURCES must be first so
#	compiler doesnt get executed more then once
#	when using directories by nmake
#

$(OBJECTS) :: $(SOURCES)
!ifdef DBGMSG
!ifdef INCLUDES
	@echo $(CC) $(CC_FLAGS) -I $(INCLUDES:;= -I ) $**
!else
	@echo $(CC) $(CC_FLAGS) $**
!endif
!endif
!ifdef INCLUDES
	@$(CC) $(CC_FLAGS) -I $(INCLUDES:;= -I ) $**
!else
	@$(CC) $(CC_FLAGS) $**
!endif

!ifdef ARCH_OBJECTS

$(ARCH_OBJECTS) :: $(ARCH_SOURCES)
!ifdef DBGMSG
!ifdef INCLUDES
	@echo $(CC) $(CC_FLAGS) -I $(INCLUDES:;= -I ) $**
!else
	@echo $(CC) $(CC_FLAGS) $**
!endif
!endif
!ifdef INCLUDES
	@$(CC) $(CC_FLAGS) -I $(INCLUDES:;= -I ) $**
!else
	@$(CC) $(CC_FLAGS) $**
!endif

!endif

#
# end ELSE block above before Linker section
#

!endif

#--------------------------------------------
#
#	Assembler
#
#--------------------------------------------

!if "$(ASM_OBJECTS)" != ""

!ifdef IA64_ASM_SOURCES

$(ASM_OBJECTS) :: $(IA64_ASM_SOURCES)
!ifdef DBGMSG
	@echo $(ASM) $(ASM_FLAGS) $**
!endif
	@$(ASM) $(ASM_FLAGS) $**
!endif

!ifdef I386_ASM_SOURCES

$(ASM_OBJECTS) :: $(I386_ASM_SOURCES)
!ifdef DBGMSG
	@echo $(ASM) $(ASM_FLAGS) $**
!endif
	@$(ASM) $(ASM_FLAGS) $**
!if "$(TARGET)"=="BIN"
#	copy $** "$(NEPTUNE_OUT)/$(TARGETNAME)"
	copy "$(TARGETNAME)" "$(NEPTUNE_OUT)/$(TARGETNAME)"
!endif
!endif

!endif

#--------------------------------------------
#
#	NBE build engine
#
#--------------------------------------------

#NEPTUNE_BUILDENGINE = "..\..\nbuild.exe"
#NEPTUNE_NBUILDMAKE = nbuild.def

#!include $(NEPTUNE_NBUILDMAKE)

#$(NEPTUNE_NBUILDMAKE) : $(NEPTUNE_BUILDENGINE)
#   $(NEPTUNE_BUILDENGINE) > $(NEPTUNE_NBUILDMAKE)

#--------------------------------------------
#
#	clean
#
#--------------------------------------------

clean :
!if "$(HOST_OS)" == "HOST_WINDOWS"
	@del /Q /S *.obj
!if exist($(NEPTUNE_OUT))
	@del /Q "$(NEPTUNE_OUT)\$(TARGETNAME)"
!endif
!endif
!if "$(HOST_OS)" == "HOST_UNIX"
	@rm *.obj
!if exist($(NEPTUNE_OUT))
	@rm "$(NEPTUNE_OUT)\$(TARGETNAME)"
!endif
!endif

#--------------------------------------------
#
#	install
#
#--------------------------------------------

install : $(TARGETNAME)
!if "$(HOST_OS)" == "HOST_WINDOWS"
	@if not exist "$(NEPTUNE_INSTALL)" \
	@mkdir "$(NEPTUNE_INSTALL)"
	copy "$(NEPTUNE_OUT)\$(TARGETNAME)" "$(NEPTUNE_INSTALL)\$(TARGETNAME)"
!endif
!if "$(HOST_OS)" == "HOST_UNIX"
	@!test "$(NEPTUNE_INSTALL)" \
	@mkdir "$(NEPTUNE_INSTALL)"
	sh cp $(TARGETNAME) "$(NEPTUNE_INSTALL)/$(TARGETNAME)"
!if exist($(NEPTUNE_OUT))
!endif
!endif


!endif
